SSH Zugriff auf ext. DB

Passwort ändern: passwd

# auf Server
ssh nricherz@login01.kom.e-technik.tu-darmstadt.de
# auf MAKI015
ssh 10.0.30.16

# Port-Forwarding für pypmyadmin
ssh -L 8000:10.0.30.16:80 nricherz@login01.kom.e-technik.tu-darmstadt.de
im Browser
http://localhost:8000/phpmyadmin

# MATLAB Tunnel:
ssh -L 8001:10.0.30.16:22 nricherz@login01.kom.e-technik.tu-darmstadt.de
ssh -L 8002:localhost:3306 nricherz@localhost -p 8001

# in Matlab
auf Port 8002

Auf dem Server:
GIT-Commands:
# Repository holen
git clone

# Repository aktualisieren
git fetch origin

# Branch anlegen
git checkout -b test <origin/Remote/Branch> (ab git >1.6.5 auch einfach  git checkout test)

# Branch aktualisieren
git merge <origin/Remote/Branch> <Local/Branch>

# Branch wechseln
git checkout <Local/Branch>

# Status
git status

# im config file Bonnmotion Pfad 
/home/nricherz/bonnmotion-2.1a/bin/bm

# file bearbeiten
nano 

# file anschauen (no changes)
less path_to_file

screen
ctrl a 'zahl' (0 1 etc)

jvisualvm

# Verzeichnis löschen (ohne nachfrage und rekursiv)
rm -Rf

laufen lassen:
# .m2 löschen wenn komplett neu git clone
rm -Rf .m2/
# dann mvn Befehle

sonst wenn nur updates
# simrunner
ant clean
ant (fehler ignorieren)
ant build-project

# Bonnmotion Pfad setzten in default_config
/home/nricherz/bonnmotion-2.1a/bin/bm

# DB anlegen und name in config checken und auf active setzen

Multirunner:
#.jar file bauen
- simrunner export as runnable jar
- Haken bei extract
- jar extracten
- persistence.xml von src/META-INF nach 'root-Verzeichnis' META-INF
http://www.minecraftforum.net/forums/archive/tutorials/928367-mods-manually-modify-minecraft-jar-mac-29-10-10
- alles markieren compress
- archive.zip rausnehmen und umbennen in 'name'.jar

http://docs.oracle.com/javase/tutorial/deployment/jar/build.html


cd Documents/eclipse/simonstrator-simrunner/

java -jar MainGui.jar Variations config/adaptivemonitoring/default_config.xml -v1 config/adaptivemonitoring/variations/dataUploadInterval.txt -v2 config/adaptivemonitoring/variations/environment.txt -s 1 -p 1

java -jar MainGui.jar Variations config/adaptivemonitoring/default_config.xml -v1 config/adaptivemonitoring/variations/allCraterSpecificVariations.txt -v2 config/adaptivemonitoring/variations/environment.txt -s 1000,1100,1200,1300,1400 -p 2


Hey, an sich musst du "nur" die jar erstellen (aus Eclipse heraus), dann die persistence.xml verschieben (von src/meta_inf nach meta_inf im Hauptverzeichnis), die jar auf den Server kopieren und dort starten (mit java -jar name commands), wie auch lokal. Schau halt, dass du das auf dem server in einem "screen" machst, sonst beendet sich die Simulation sobald du die SSH-Verbindund schließt

Sorry, hatte bis grad business... wie du die jar am Besten kopierst (unter mac) weiß ich nicht. Remote-copy (command line) müsste aber gene


ANT Build:
# simrunner/build.xml
<arg line="${args}"/>
<jvmarg line="-Xms1G -Xmx8G"/>

cd Documents/eclipse/simonstrator-simrunner/

ant

ls -a

# System Parameter Eval - allValues
ant MultiRunner -Dargs='Variations config/adaptivemonitoring/default_config.xml -v1 config/adaptivemonitoring/variations/defaultEnvironment.txt -v2 config/adaptivemonitoring/variations/allCraterSpecificVariations.txt -s 111,222,333,444,555 -p 2'

# Scenario Parameter Eval - Config A - Max number of Connections/Sinks
ant MultiRunner -Dargs='Variations config/adaptivemonitoring/default_config.xml -v1 config/adaptivemonitoring/variations/defaultSystemValues.txt -v2 config/adaptivemonitoring/variations/eval_scenario_max_conn_to_BS.txt -s 111,222,333,444,555 -p 2'

# Scenario Parameter Eval - Config B - Ratio Back to Main Traffic
ant MultiRunner -Dargs='Variations config/adaptivemonitoring/default_config.xml -v1 config/adaptivemonitoring/variations/defaultSystemValues.txt -v2 config/adaptivemonitoring/variations/eval_scenario_ratio_back_to_main_var.txt -s 111,222,333,444,555 -p 2'

# Scenario Parameter Eval - Config C - Node Count
ant MultiRunner -Dargs='Variations config/adaptivemonitoring/default_config.xml -v1 config/adaptivemonitoring/variations/defaultSystemValues.txt -v2 config/adaptivemonitoring/variations/eval_scenario_number_nodes_var.txt -s 111,222,333,444,555 -p 2'

# Scenario Parameter Eval - Config D - Movement Speed
ant MultiRunner -Dargs='Variations config/adaptivemonitoring/default_config.xml -v1 config/adaptivemonitoring/variations/defaultSystemValues.txt -v2 config/adaptivemonitoring/variations/eval_scenario_movement_speed.txt -s 111,222,333,444,555 -p 2'

# Adaptiveness of Crater - Default with and without Crater
ant MultiRunner -Dargs='Variations config/adaptivemonitoring/default_config.xml -v1 config/adaptivemonitoring/variations/defaultSystemValues.txt -v2 config/adaptivemonitoring/variations/defaultEnvironment_withWithoutCrater.txt -s 111,222,333,444,555 -p 2'

# Adaptiveness of Crater - Max Conn BS with and without Crater
ant MultiRunner -Dargs='Variations config/adaptivemonitoring/default_config.xml -v1 config/adaptivemonitoring/variations/defaultSystemValues_withWithoutCrater.txt -v2 config/adaptivemonitoring/variations/eval_scenario_max_conn_to_BS.txt -s 111,222,333,444,555 -p 2'

# Adaptiveness of Crater with Scenario Configs