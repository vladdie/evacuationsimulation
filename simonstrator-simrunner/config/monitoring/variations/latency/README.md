# Latency Optimization Techniques (in Crater) Evaluation (partially used for BSc Jonas)

# Defaults (are used unless stated other) 
GW -- RANDOM
CRA_SINK_PREF -- DEFAULT
CRA_SEND_HISTORY_TYPE -- DEFAULT
CRA_CONTENTION_TYPE -- DEFAULT


## GW Selection Mechanism Variations
GW RANDOM (default) vs. D1C_LAT, DkC_LAT, CD_LAT_DBScan, CD_LAT_kMeans, CD_LAT_Grid
Gauss Movement
without APs (cloudlets)

vs CD_BECLEACH_Grid (comparison with Michael Walter GW Selektion Schemes)

java -jar MultiRunner.jar Variations config/monitoring/default.xml -v1 config/monitoring/variations/latency/defaultEnvironment.txt -v2 config/monitoring/variations/latency/eval_system_variation_gateway.txt -s 111 222 333 444 555 -p 2

## History Variations
GW RANDOM (default)
Gauss Movement
without APs (cloudlets)
History DEFAULT (default) vs. TIME,SIZE,NONE
Upload Interval 5s, 15s, 30s, 45s, 60s (default)

java -jar MultiRunner.jar Variations config/monitoring/default.xml -v1 config/monitoring/variations/latency/defaultEnvironment.txt -v2 config/monitoring/variations/latency/eval_system_variation_history.txt -s 111 222 333 444 555 -p 2

1. Plotting für feste History Auswahl mit Upload Variation
2. Plotting für feste Upload Variationen mit Variation der History 

## Contention Scheme Variations
GW RANDOM (default)
Gauss Movement
without APs (cloudlets)
Contention DEFAULT (default) vs. DIFI,MODIFI

java -jar MultiRunner.jar Variations config/monitoring/default.xml -v1 config/monitoring/variations/latency/defaultEnvironment.txt -v2 config/monitoring/variations/latency/eval_system_variation_contention_scheme.txt -s 111 222 333 444 555 -p 2

evtl TODO
sehr wenige SINKS

## Static Sink Preference Variations
with APs (10)
AP placement SOCIAL
Movement SOCIAL
SOCIAL_FACTOR 0.0 (Knoten sollen sich nicht allzu lange an den APs aufhalten) vs 0.2, 0.4
CRA_SINK_PREF GRADIENT (default) vs. QUALITY, STATIC_GRADIENT, STATIC_QUALITY

java -jar MultiRunner.jar Variations config/monitoring/default.xml -v1 config/monitoring/variations/latency/defaultEnvironment_withAP.txt -v2 config/monitoring/variations/latency/eval_system_variation_sinkpreference.txt -s 111 222 333 444 555 -p 2

## Initial Version vs. Improvements 
GW CD_Grid_Lat vs random (default)
History TimedCraterHistory vs default
Upload 5 Sekunden vs 5s, 60s (default)
Contention default
Sink Preference: default (also gradient)

java -jar MultiRunner.jar Variations config/monitoring/default.xml -v1 config/monitoring/variations/latency/defaultEnvironment.txt -v2 config/monitoring/variations/latency/eval_system_variation_general_improvement.txt -s 111 222 333 444 555 -p 2
