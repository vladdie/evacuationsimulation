#Default
dataRoutingSendingInterval=30s dataRoutingUploadInterval=60s initialTTL=3 noSinkAdvertisingInterval=10s sinkAdvertisingInterval=15s accuracyCompletenessCheckInterval=1m
#
#Variations
dataRoutingSendingInterval=15s
dataRoutingSendingInterval=60s
#
dataRoutingUploadInterval=30s
dataRoutingUploadInterval=120s
#
initialTTL=1
initialTTL=6
#
noSinkAdvertisingInterval=20s
noSinkAdvertisingInterval=40s
noSinkAdvertisingInterval=80s
#
sinkAdvertisingInterval=30s
sinkAdvertisingInterval=60s
#
accuracyCompletenessCheckInterval=30s
accuracyCompletenessCheckInterval=2m
accuracyCompletenessCheckInterval=3m