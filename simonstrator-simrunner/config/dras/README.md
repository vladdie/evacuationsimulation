# Disaster Region Analyzing Service

NOTE: DRAS is WORK IN PROGRESS and subject to change!

The Disaster-Region-Analyzing-Service (DRAS) can be used to determine and visualize so called disaster regions, i.e. regions without UMTS connection.

#### Usage
* GRID-CELLS: When using DRAS, make sure, the 5G Layer is activated. By shift-clicking on a grid-cell, you can destroy it and break down the UTMS connection. By shift-clicking on it again, UMTS becomes available again.

* Single node monitoring: If you want to monitor a single node, you can simply click on it. A seconds windows will pop up, which will start to draw black dots for positions without UMTS and red dots for positions where UMTS is available. To stop monitoring that node click on it again.

* Area monitoring: It is also possible to monitor a certain are. The data of all the nodes in that area will be collected and aggregated, as long as they stay within the area. To do so, click and drag the mouse from the upper left of the wished area to the lower right. A rectangle should appear. After releasing the mouse, the rectangle should turn blue, indicating, that monitoring of this area is active. Additionally a second windows should pop up again, showing an aggregated view of all the nodes in the area. Note the node ID in in upper left corner, which now doesn't correspond to a single node.
