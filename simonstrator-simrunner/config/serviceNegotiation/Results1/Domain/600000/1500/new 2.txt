_ECHO 1m "============================="
_ECHO 1m "Gateways Initialization!"
StaticGateways 1m-5m ServiceDiscoveryStaticComponent:initializateGateway RANDOM
MobileGateways 6m-10m ServiceDiscoveryMobileComponent:initializateGateway RANDOM
_ECHO 10m "Citizens Initialization!"
Citizens 11m-15m Consumer:initializateCitizen
_ECHO 15m "Providers Initialization!"
StaticProviders 15m-20m StaticProvider:initializateProvider
MobileProviders 15m-20m MobileProvider:initializateProvider
_ECHO 6m "Service Publishing!"
StaticProviders 21m-30m StaticProvider:publishServices RANDOM 1
MobileProviders 21m-30m MobileProvider:publishServices RANDOM 1
_ECHO 16m "Service Discovery!"
Citizens 31m-40m Consumer:startServiceRequests