#!/bin/bash

# Move to root of simrunner
cd ../../../

# Build project, if necessary

echo "\n\nRemember to rebuild the module with mvn package, if sources changed!!\n\n"
export JAVA_HOME=/usr/lib/jvm/java-8-oracle
mvn clean install -U
mvn package

echo "\n\nDO NOT change any configuration files while simulations are running!\n\n"

cp target/guirunner.jar ./runningSimulation.jar

echo "Starting baseline evaluations..."
java -cp runningSimulation.jar de.tudarmstadt.maki.simonstrator.peerfact.MultiRunner Variations  config/pubsub/bypass/debug_db.xml -v1 config/pubsub/bypass/variations/var_gateways_baseline.txt -s 11 22 33 44 55 -start

echo "Starting gateways evaluations..."
java -cp runningSimulation.jar de.tudarmstadt.maki.simonstrator.peerfact.MultiRunner Variations  config/pubsub/bypass/debug_db.xml -v2 config/pubsub/bypass/variations/var_gateways.txt -v1 config/pubsub/bypass/variations/var_gateways_env.txt -s 11 22 33 44 55 -start

echo "Done. Archiving used jar..."
mv runningSimulation.jar simulation_gateways_$(date +%F).jar
