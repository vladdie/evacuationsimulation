#
# Time, Target (Clients/Clouds), ProxyName, Mechanism
# 
# Just a chain of local dissemination transitions, triggered globally.
#
# 8 mins: GOSSIP -> PLANB
8m, Clients, BypassLocalDissemination, de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb.PlanBDistribution
# 12 mins: PLANB -> HyperG
12m, Clients, BypassLocalDissemination, de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip.HyperGossipDistribution
# 17 mins: HyperG -> Flooding
17m, Clients, BypassLocalDissemination, de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.flooding.FloodingDistribution
# 22 mins: aaand Broadcast
22m, Clients, BypassLocalDissemination, de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.broadcast.BroadcastDissemination