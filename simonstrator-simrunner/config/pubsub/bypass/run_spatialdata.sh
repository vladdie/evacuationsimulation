#!/bin/bash

# Move to root of simrunner
cd ../../../

# Build project, if necessary

echo "\n\nRemember to rebuild the module with mvn package, if sources changed!!\n\n"
export JAVA_HOME=/usr/lib/jvm/java-8-oracle
mvn clean install -U
mvn package

echo "\n\nDO NOT change any configuration files while simulations are running!\n\n"

echo "Starting bypass_spatialdata evaluations..."

cp target/guirunner.jar ./runningSimulation.jar

java -cp runningSimulation.jar de.tudarmstadt.maki.simonstrator.peerfact.MultiRunner Variations  config/pubsub/bypass/debug_db.xml -v1 config/pubsub/bypass/variations/var_spatialdata.txt -v2 config/pubsub/bypass/variations/var_spatialdata_env.txt -s 11 -start

echo "Done. Archiving used jar..."
mv runningSimulation.jar simulationRun_bypass_spatialdata_$(date +%F).jar
