let id = -1
$().ready(() => {
  $('.info').tooltip()
  let thresPrec = $('#metric1').data('threshold')
  let thresRec = $('#metric2').data('threshold')
  let lastPrec = 0.0
  let lastRec = 0.0
  let lastMsg = 0.0
  let lastWaypoints = -1
  let eventSource = new EventSource('/simonstrator/pubsub/client/bind'); 
  
  // Live plots
  let precPlot = $("#metric1chart").peity("line", { height: 12, width: 64, max: 1, min: 0 })
  precPlot.text('0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')
  let recPlot = $("#metric2chart").peity("line", { height: 12, width: 64, max: 1, min: 0 })
  recPlot.text('0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0')
  
  // Handle errors with eventSource
  eventSource.onerror = (error) => {
    $('#errorAlert').removeClass('hidden-xs-up')
    $('#errorAlert').addClass('in')
  }
  // Handle successful connection
  eventSource.onopen = () => {
    $('#errorAlert').removeClass('in')
    $('#errorAlert').addClass('hidden-xs-up')
  }
  // Handle new event
  eventSource.onmessage = (event) => {
    // Parse JSON
    let data = JSON.parse(event.data)

    console.log(data)

    // Set ID if it has changed
    if(id !== data.id) {
      id = data.id
      $('#idField').text(data.id)
      $('#idColor').css('color', data.colorHex)
    }

    // Set precision
    let prec = data.precision.toFixed(2)
    $('#metric1').text(prec)
    
    // Update liveplot
    if (prec != -1) {
      var precPlotValues = precPlot.text().split(",")
      precPlotValues.shift()
      precPlotValues.push(prec)
      precPlot.text(precPlotValues.join(",")).change()
    }
    // Change color for threshold
    if(prec == -1) {
      $('#metric1group').removeClass('tag-danger').removeClass('tag-success').addClass('tag-default')
      $('#metric1symbol').removeClass('fa-arrow-right').addClass('fa-times')
    } else {
      $('#metric1symbol').addClass('fa-arrow-right').removeClass('fa-times')
      if(prec < thresPrec) {
        $('#metric1group').removeClass('tag-success').addClass('tag-danger')
      } else {
        $('#metric1group').addClass('tag-success').removeClass('tag-danger')
      }
    }
    // Set arrow
    if(prec !== lastPrec) {
      if(prec > lastPrec) {
        $('#metric1symbol').addClass('ani-up').removeClass('ani-down')
      } else {
        $('#metric1symbol').addClass('ani-down').removeClass('ani-up')
      }
    }
    lastPrec = prec

    // Set recall
    let rec = data.recall.toFixed(2)
    $('#metric2').text(rec)
    
    // Update liveplot
    if (rec != -1) {
      var recPlotValues = recPlot.text().split(",")
      recPlotValues.shift()
      recPlotValues.push(rec)
      recPlot.text(recPlotValues.join(",")).change()
    }
    // Change color for threshold
    if(rec == -1) {
      $('#metric2group').removeClass('tag-danger').removeClass('tag-success').addClass('tag-default')
      $('#metric2symbol').removeClass('fa-arrow-right').addClass('fa-times')
    } else {
      $('#metric2symbol').addClass('fa-arrow-right').removeClass('fa-times')
      if(rec < thresRec) {
        $('#metric2group').removeClass('tag-success').addClass('tag-danger')
      } else {
        $('#metric2group').addClass('tag-success').removeClass('tag-danger')
      }
    }
    // Set arrow
    if(rec !== lastRec) {
      if(rec > lastRec) {
        $('#metric2symbol').addClass('ani-up').removeClass('ani-down')
      } else {
        $('#metric2symbol').addClass('ani-down').removeClass('ani-up')
      }
    }
    lastRec = rec

    // Set messages/min
    let msg = data.notificationsPerSecond.toFixed(0)
    $('#metric3').text(msg)

    // Set arrow
    if(msg !== lastMsg) {
      if(msg > lastMsg) {
        $('#metric3symbol').addClass('ani-up').removeClass('ani-down')
      } else {
        $('#metric3symbol').addClass('ani-down').removeClass('ani-up')
      }
    }
    lastMsg = msg

    // Scheme selector
    $('.scheme-select').each((idx, elem) => {
      elem = $(elem)
      if(elem.data('scheme') == data.currentScheme) {
        elem.removeClass('disabled')
      } else {
        elem.addClass('disabled')
      }
    })

    // Fill select with waypoints
    $('#location').text(data.currentAttractionPoint)

    if(data.attractionPoints.length > lastWaypoints) {
      $('#setting3').find('option').remove()


      $.each(data.attractionPoints, (point, elem) => {
        if(elem === data.currentAttractionPoint) {
          $('#setting3').append($("<option />").text(elem).prop('selected', true))
        } else {
          $('#setting3').append($("<option />").text(elem))
        }
        
      })
      lastWaypoints = data.attractionPoints.length
    }
  }
})

// Settings 1 increment click
$('#setting1incr').click(() => {
  let currentValue = parseFloat($('#setting1').val())
  let incr = parseFloat($('#setting1').data('incr'))
  currentValue += incr
  let min = parseFloat($('#setting1').data('min'))
  let max = parseFloat($('#setting1').data('max'))
  if(currentValue <= max) {
    $('#setting1').val(currentValue.toFixed($('#setting1').data('precision')))
    console.log(`GET Request with ${currentValue} to endpoint.`)
    $.get(`/simonstrator/pubsub/client/${id}/roi/${currentValue}`)
    $('#setting1decr').prop('disabled', true)
    $('#setting1incr').prop('disabled', true)
    setTimeout(function() {
      $('#setting1incr').prop('disabled', false)
      if(currentValue == max) {
        $('#setting1incr').prop('disabled', true)
      }
      if(currentValue > min) {
        $('#setting1decr').prop('disabled', false)
      }
    }, 1000);
  }
})

// Settings 1 decrement click
$('#setting1decr').click(() => {
  let currentValue = parseFloat($('#setting1').val())
  let decr = parseFloat($('#setting1').data('incr'))
  currentValue -= decr
  let min = parseFloat($('#setting1').data('min'))
  let max = parseFloat($('#setting1').data('max'))
  if(currentValue >= min) {
    $('#setting1').val(currentValue.toFixed($('#setting1').data('precision')))
    console.log(`GET Request with ${currentValue} to endpoint.`)
    $.get(`/simonstrator/pubsub/client/${id}/roi/${currentValue}`)
    $('#setting1decr').prop('disabled', true)
    $('#setting1incr').prop('disabled', true)
    setTimeout(function() {
      $('#setting1decr').prop('disabled', false)
      if(currentValue == min) {
        $('#setting1decr').prop('disabled', true)
      }
      if(currentValue < max) {
        $('#setting1incr').prop('disabled', false)
      }
    }, 1000);
  }
})

// Settings 2 increment click
$('#setting2incr').click(() => {
  let currentValue = parseFloat($('#setting2').val())
  let incr = parseFloat($('#setting2').data('incr'))
  currentValue += incr
  let min = parseFloat($('#setting2').data('min'))
  let max = parseFloat($('#setting2').data('max'))
  if(currentValue <= max) {
    $('#setting2').val(currentValue.toFixed($('#setting2').data('precision')))
    console.log(`GET Request with ${currentValue} to endpoint.`)
  }
  
  if(currentValue == max) {
    $('#setting2incr').prop('disabled', true)
  }

  if(currentValue > min) {
    $('#setting2decr').prop('disabled', false)
  }
})

// Settings 2 decrement click
$('#setting2decr').click(() => {
  let currentValue = parseFloat($('#setting2').val())
  let decr = parseFloat($('#setting2').data('incr'))
  currentValue -= decr
  let min = parseFloat($('#setting2').data('min'))
  let max = parseFloat($('#setting2').data('max'))
  if(currentValue >= min) {
    $('#setting2').val(currentValue.toFixed($('#setting2').data('precision')))
    console.log(`GET Request with ${currentValue} to endpoint.`)
  }
  
  if(currentValue == min) {
    $('#setting2decr').prop('disabled', true)
  }

  if(currentValue < max) {
    $('#setting2incr').prop('disabled', false)
  }
})

// Steting 3 selection (waypoint)
$('#setting3').change(() => {
  $.get(`/simonstrator/pubsub/client/${id}/waypoint/${$('#setting3').val()}`)
})

// Setting 3 button (gather)
$('#setting3gather').click(() => {
  console.log(`GET Request to gather at waypoint.`)
  $.get(`/simonstrator/pubsub/client/${id}/gather`)
  $('#setting3gather').prop('disabled', true)
  setTimeout(function() {
      $('#setting3gather').prop('disabled', false)
  }, 10000);
})
