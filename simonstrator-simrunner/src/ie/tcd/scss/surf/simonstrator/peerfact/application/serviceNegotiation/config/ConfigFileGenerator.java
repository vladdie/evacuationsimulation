package ie.tcd.scss.surf.simonstrator.peerfact.application.serviceNegotiation.config;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class ConfigFileGenerator {

	public void setUp(String inputPath, String outputPath) {

		System.out.println("Starting set up config files....");
		String sourceConfigFile = inputPath + "/serviceNegotiation_default.xml";
		String sourceTimeFile = inputPath + "/serviceNegotiation_eval_parameters.xml";
		String sourceDatFile = inputPath + "/serviceNegotiation.dat";

		String configFile = outputPath + "/serviceNegotiation_default.xml";
		String timeFile = outputPath + "/serviceNegotiation_eval_parameters.xml";
		String datFile = outputPath + "/serviceNegotiation.dat";

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document sourceDocumentConfig = db.parse(sourceConfigFile);
			org.w3c.dom.Document sourceDocumentTime = db.parse(sourceTimeFile);

			TransformerFactory transformerFactoryConfig = TransformerFactory.newInstance();
			Transformer transformerConfig = transformerFactoryConfig.newTransformer();
			DOMSource sourceConfig = new DOMSource(sourceDocumentConfig);
			StreamResult resultConfig = new StreamResult(new File(configFile));
			transformerConfig.transform(sourceConfig, resultConfig);

			TransformerFactory transformerFactoryTime = TransformerFactory.newInstance();
			Transformer transformerTime = transformerFactoryTime.newTransformer();
			DOMSource sourceTime = new DOMSource(sourceDocumentTime);
			StreamResult resultTime = new StreamResult(new File(timeFile));
			transformerTime.transform(sourceTime, resultTime);

			Path from = Paths.get(sourceDatFile); // convert from File to Path
			Path to = Paths.get(datFile); // convert from String to Path
			Files.copy(from, to, StandardCopyOption.REPLACE_EXISTING);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("config files set up successfully!");
	}

	public void generate(String[] args) {
		// System.setProperty("logfile.name", "simguilog.log");
		args = new String[1];
		String currentFolder = System.getProperty("user.dir");
		String fileFolder = new File(currentFolder + "").getParent();

		String configFile = fileFolder
				+ "/simonstrator-simrunner/NegotiatorData/testConfig/serviceNegotiation_default.xml";
		String timeFile = fileFolder
				+ "/simonstrator-simrunner/NegotiatorData/testConfig/serviceNegotiation_eval_parameters.xml";
		String actionsFile = fileFolder + "/simonstrator-simrunner/NegotiatorData/testConfig/template.dat";

		String churnVariable = "";
		String intervalVariable = "";
		String serviceAmountVariable = "";
		String gatewaysVariable = "";
		String speedVariable = "";
		String providersVariable = "";
		int time = 0;

		int file = 0;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			org.w3c.dom.Document documentConfig = db.parse(configFile);
			org.w3c.dom.Document documentTime = db.parse(timeFile);
			for (int churn = 0; churn <= 1; churn++) {
				if (churn == 0)
					churnVariable = "noChurn";
				else
					churnVariable = "churn";
				for (int i = 0; i < documentConfig.getElementsByTagName("Variable").getLength(); i++) {
					if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes().getNamedItem("name")
							.getNodeValue().equals("CHURN")) {
						if (churn == 0) {
							documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
									.getNamedItem("value").setNodeValue("false");
						}
						if (churn == 1) {
							documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
									.getNamedItem("value").setNodeValue("true");
						}
					}
				}

				for (int services = 10; services < 136; services = services + 60) {
					serviceAmountVariable = "" + services;
					for (int gateways = 75; gateways <= 375; gateways = gateways + 150) {
						gatewaysVariable = "" + gateways;
						int mobileGateways = gateways * 1 / 3;
						int staticGateways = gateways * 2 / 3;
						for (int i = 0; i < documentConfig.getElementsByTagName("Variable").getLength(); i++) {
							if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
									.getNamedItem("name").getNodeValue().equals("NUM_STATIC_GATEWAYS")) {
								documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
										.getNamedItem("value").setNodeValue("" + staticGateways);
							}
							if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
									.getNamedItem("name").getNodeValue().equals("NUM_MOBILE_GATEWAYS")) {
								documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
										.getNamedItem("value").setNodeValue("" + mobileGateways);
							}
							if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
									.getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) {
								documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
										.getNamedItem("value").setNodeValue("" + 100);
							}
						}

						for (int providerDistribution = 0; providerDistribution < 3; providerDistribution++) {
							for (int providers = 50; providers <= 250; providers = providers + 100) {
								providersVariable = "" + providers;
								int mobileProviders = 0;
								int staticProviders = 0;
								String distributionType = "";
								switch (providerDistribution) {
								case 0:
									// all mobile
									mobileProviders = providers;
									staticProviders = 0;
									distributionType = "0_static";
									break;
								case 1:
									// 50% mobile
									mobileProviders = providers / 2;
									staticProviders = providers - mobileProviders;
									distributionType = "50_static";
									break;
								case 2:
									// all static
									mobileProviders = 0;
									staticProviders = providers;
									distributionType = "100_static";
									break;
								}
								for (int i = 0; i < documentConfig.getElementsByTagName("Variable").getLength(); i++) {
									if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
											.getNamedItem("name").getNodeValue().equals("NUM_STATIC_PROVIDERS")) {
										documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
												.getNamedItem("value").setNodeValue("" + staticProviders);
									}
									if (documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
											.getNamedItem("name").getNodeValue().equals("NUM_MOBILE_PROVIDERS")) {
										documentConfig.getElementsByTagName("Variable").item(i).getAttributes()
												.getNamedItem("value").setNodeValue("" + mobileProviders);
									}
								}

								int pScale = 0;
								int sScale = 0;
								if (providers == 50) {
									pScale = 1;
								}
								if (providers == 150) {
									pScale = 3;
								}
								if (providers == 250) {
									pScale = 5;
								}
								if (services == 10) {
									sScale = 1;
								}
								if (services == 70) {
									sScale = 3;
								}
								if (services == 130) {
									sScale = 6;
								}

								int registerInterval = 40 * pScale * sScale;
								intervalVariable = "" + registerInterval;

								for (int speed = 0; speed < 3; speed++) {
									speedVariable = "";
									double minSpeed = 0;
									double maxSpeed = 0;
									switch (speed) {
									case 0:
										minSpeed = 0.42;
										maxSpeed = 0.5;
										speedVariable = "walk";
										break;
									case 1:
										minSpeed = 16.7;
										maxSpeed = 27.8;
										speedVariable = "car";
										break;
									case 2:
										minSpeed = 3.6;
										maxSpeed = 4.2;
										speedVariable = "bike";
										break;
									case 3:
										minSpeed = 0.42;
										maxSpeed = 27.8;
										speedVariable = "mix";
										break;
									}

									for (int i = 0; i < documentTime.getElementsByTagName("Variable")
											.getLength(); i++) {
										if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
												.getNamedItem("name").getNodeValue().equals("SPEED_MIN")) {
											documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("value").setNodeValue(Double.toString(minSpeed));
										}
										if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
												.getNamedItem("name").getNodeValue().equals("SPEED_MAX")) {
											documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("value").setNodeValue(Double.toString(maxSpeed));
										}
									}

									String folderName = churnVariable + "/" + speedVariable + "/" + gatewaysVariable
											+ "GWs/" + providersVariable + "SPs/" + distributionType + "/"
											+ serviceAmountVariable + "Services";

									try {
										TransformerFactory transformerFactoryConfig = TransformerFactory.newInstance();
										Transformer transformerConfig = transformerFactoryConfig.newTransformer();
										DOMSource sourceConfig = new DOMSource(documentConfig);
										StreamResult resultConfig = new StreamResult(new File(configFile));
										transformerConfig.transform(sourceConfig, resultConfig);

										String[] linesConfig = Files.readAllLines(new File(configFile).toPath())
												.toArray(new String[0]);

										try {
											// String basePath = "P:/test/";
											String basePath = fileFolder
													+ "/simonstrator-simrunner/NegotiatorData/testConfig/";
											File directory = new File(basePath + folderName);
											if (!directory.exists()) {
												directory.mkdirs();
											}
											System.out.println(file + "-th file: creating ->" + basePath + folderName
													+ "/serviceNegotiation_default.xml");
											BufferedWriter out = new BufferedWriter(new FileWriter(
													basePath + folderName + "/serviceNegotiation_default.xml", false));
											for (int i = 0; i < linesConfig.length; i++) {
												out.write(linesConfig[i]);
												out.newLine();
											}
											out.close();
										} catch (IOException e) {
											e.printStackTrace();
										}

										String[] lines = Files.readAllLines(new File(actionsFile).toPath())
												.toArray(new String[0]);

										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<PATH_INTIALIZATION>")) {
												lines[i] = lines[i].replaceAll("<PATH_INTIALIZATION>",
														time + "m-" + (time + 1) + "m");
											}
										}
										time = time + 1;

										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<GATEWAY_INTIALIZATION>")) {
												lines[i] = lines[i].replaceAll("<GATEWAY_INTIALIZATION>",
														time + "m-" + (time + 100) + "m");
											}
										}
										time = time + 100;

										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<PROVIDER_INTIALIZATION>")) {
												lines[i] = lines[i].replaceAll("<PROVIDER_INTIALIZATION>",
														time + "m-" + (time + 100) + "m");
											}
										}
										time = time + 100;

										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<CITIZEN_INTIALIZATION>")) {
												lines[i] = lines[i].replaceAll("<CITIZEN_INTIALIZATION>",
														time + "m-" + (time + 100) + "m");
											}
										}
										time = time + 100;

										int publishTime = 60 * pScale * sScale;
										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<START_PUBLISHING>")) {
												lines[i] = lines[i].replaceAll("<START_PUBLISHING>",
														time + "m-" + (time + publishTime) + "m");
											}
										}
										time = time + publishTime;

										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<STOP_PUBLISHING>")) {
												lines[i] = lines[i].replaceAll("<STOP_PUBLISHING>",
														time + "m-" + (time + 60) + "m");
											}
										}
										time = time + 60;

										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<SHOW_SERVICES>")) {
												lines[i] = lines[i].replaceAll("<SHOW_SERVICES>",
														time + "m-" + (time + staticGateways) + "m");
											}
										}
										time = time + staticGateways;

										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<START_REQUESTS>")) {
												lines[i] = lines[i].replaceAll("<START_REQUESTS>",
														time + "m-" + (time + 150) + "m");
											}
										}
										time = time + 150;

										// time = time + 800;
										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<STOP_REQUESTS>")) {
												lines[i] = lines[i].replaceAll("<STOP_REQUESTS>",
														time + "m-" + (time + 60) + "m");
											}
										}
										time = time + 60;
										for (int i = 0; i < lines.length; i++) {
											if (lines[i].contains("<PRINT_RESULTS>")) {
												lines[i] = lines[i].replaceAll("<PRINT_RESULTS>",
														time + "m-" + (time + 5) + "m");
											}
										}
										time = time + 5;

										String logPath = fileFolder
												+ "/simonstrator-simrunner/NegotiatorData/testConfig";

										for (int i = 0; i < lines.length; i++) {
											lines[i] = lines[i].replaceAll("<CHURN>", churnVariable)
													.replaceAll("<SPEED>", speedVariable)
													.replaceAll("<GATEWAYS>", gatewaysVariable)
													.replaceAll("<PROVIDERS>", providersVariable)
													.replaceAll("<INTERVAL>", intervalVariable)
													.replaceAll("<SERVICESAMOUNT>", serviceAmountVariable)
													.replaceAll("<GATEWAYSPATH>", gatewaysVariable + "GWs")
													.replaceAll("<PROVIDERSPATH>", providersVariable + "SPs")
													.replaceAll("<DISTRIBUTIONPATH>", distributionType)
													.replaceAll("<LOGDIR>", logPath).replaceAll("<SERVICESAMOUNTPATH>",
															serviceAmountVariable + "Services");

										}
										// String folderName =
										// churnVariable+"/"+speedVariable+"/"+gatewaysVariable+"GWs/"+providersVariable+"SPs/"+distributionType+"/"+serviceAmountVariable+"Services";
										try {
											file++;
											// String basePath = "P:/test/";
											String basePath = fileFolder
													+ "/simonstrator-simrunner/NegotiatorData/testConfig/";
											File directory = new File(basePath + folderName);
											if (!directory.exists()) {
												directory.mkdirs();
											}
											System.out.println(file + "-th file: creating ->" + basePath + folderName
													+ "/serviceNegotiation.dat");
											BufferedWriter out = new BufferedWriter(new FileWriter(
													basePath + folderName + "/serviceNegotiation.dat", false));
											for (int i = 0; i < lines.length; i++) {
												out.write(lines[i]);
												out.newLine();
											}
											out.close();
										} catch (IOException e) {
											e.printStackTrace();
										}

										for (int i = 0; i < documentTime.getElementsByTagName("Variable")
												.getLength(); i++) {
											if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("name").getNodeValue().equals("finishTime")) {
												documentTime.getElementsByTagName("Variable").item(i).getAttributes()
														.getNamedItem("value").setNodeValue("" + time + "m");
											}
											if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("name").getNodeValue().equals("CHURN")) {
												if (churn == 0) {
													documentTime.getElementsByTagName("Variable").item(i)
															.getAttributes().getNamedItem("value")
															.setNodeValue("false");
												}
												if (churn == 1) {
													documentTime.getElementsByTagName("Variable").item(i)
															.getAttributes().getNamedItem("value").setNodeValue("true");
												}
											}
											if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("name").getNodeValue().equals("CHURN_START")) {
												documentTime.getElementsByTagName("Variable").item(i).getAttributes()
														.getNamedItem("value").setNodeValue("60m");
											}
											if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("name").getNodeValue()
													.equals("NUM_STATIC_PROVIDERS")) {
												documentTime.getElementsByTagName("Variable").item(i).getAttributes()
														.getNamedItem("value").setNodeValue("" + staticProviders);
											}
											if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("name").getNodeValue()
													.equals("NUM_MOBILE_PROVIDERS")) {
												documentTime.getElementsByTagName("Variable").item(i).getAttributes()
														.getNamedItem("value").setNodeValue("" + mobileProviders);
											}
											if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("name").getNodeValue()
													.equals("NUM_STATIC_GATEWAYS")) {
												documentTime.getElementsByTagName("Variable").item(i).getAttributes()
														.getNamedItem("value").setNodeValue("" + staticGateways);
											}
											if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("name").getNodeValue()
													.equals("NUM_MOBILE_GATEWAYS")) {
												documentTime.getElementsByTagName("Variable").item(i).getAttributes()
														.getNamedItem("value").setNodeValue("" + mobileGateways);
											}
											if (documentTime.getElementsByTagName("Variable").item(i).getAttributes()
													.getNamedItem("name").getNodeValue().equals("NUM_CITIZENS")) {
												documentTime.getElementsByTagName("Variable").item(i).getAttributes()
														.getNamedItem("value").setNodeValue("" + 100);
											}

										}

										TransformerFactory transformerFactoryTime = TransformerFactory.newInstance();
										Transformer transformerTime = transformerFactoryTime.newTransformer();
										DOMSource sourceTime = new DOMSource(documentTime);
										StreamResult resultTime = new StreamResult(new File(timeFile));
										transformerTime.transform(sourceTime, resultTime);

										String[] linesTime = Files.readAllLines(new File(timeFile).toPath())
												.toArray(new String[0]);

										try {
											// String basePath = "P:/test/";
											String basePath = fileFolder
													+ "/simonstrator-simrunner/NegotiatorData/testConfig/";
											File directory = new File(basePath + folderName);
											if (!directory.exists()) {
												directory.mkdirs();
											}
											System.out.println(file + "-th file: creating ->" + basePath + folderName
													+ "/serviceNegotiation_eval_parameters.xml");
											BufferedWriter out = new BufferedWriter(new FileWriter(
													basePath + folderName + "/serviceNegotiation_eval_parameters.xml",
													false));
											for (int i = 0; i < linesTime.length; i++) {
												out.write(linesTime[i]);
												out.newLine();
											}
											out.close();
										} catch (IOException e) {
											e.printStackTrace();
										}

										args[0] = configFile;

										time = 0;
									} catch (Exception ex) {
										ex.printStackTrace();
									}

								}
							}
						}

					}
				}
			}
			System.out.println("Total file number: " + file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
