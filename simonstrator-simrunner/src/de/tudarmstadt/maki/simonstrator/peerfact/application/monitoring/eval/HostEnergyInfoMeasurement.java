package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;

@Entity
@Table(name = "host_energy_info")
public class HostEnergyInfoMeasurement extends CustomMeasurement{
	public static final MetricDescription HOST_ENERGY_INFO_METRIC = new MetricDescription(
			HostEnergyAnalyzer.class,
			"HostEnergyInfo",
			"Provides the session time [us], energy consumption [uJ], and power [uW] per host",
			"online time [us], energy consumption [uJ], power [uW]");
	
	@SuppressWarnings("unused")
	private long nodeId;
	
	@SuppressWarnings("unused")
	private long sessionTime;
	
	@SuppressWarnings("unused")
	private double energyConsumption;
	
	@SuppressWarnings("unused")
	private double power;
	
	@SuppressWarnings("unused")
	private String netInterfaceName;

	public HostEnergyInfoMeasurement(long nodeId, long sessionTime, double energyConsumption,
			double power, NetInterfaceName netInterfaceName) {
		super();
		this.nodeId = nodeId;
		this.sessionTime = sessionTime;
		this.energyConsumption = energyConsumption;
		this.power = power;
		this.netInterfaceName = netInterfaceName.toString();
	}
}
