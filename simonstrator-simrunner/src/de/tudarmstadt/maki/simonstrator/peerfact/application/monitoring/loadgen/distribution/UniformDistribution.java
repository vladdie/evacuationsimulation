package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.distribution;

import de.tud.kom.p2psim.impl.scenario.DefaultConfigurator;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

/**
 * just returns a fix percentage given from the config.
 * 
 * @author Simon
 * @version 1.0, 16.06.2016
 */
public final class UniformDistribution implements AbstractDistribution {

	private final double percentOfRequestingNumOfNodes;

	private final int minNumNodes;

	@XMLConfigurableConstructor({ "percentage", "minNodes" })
	public UniformDistribution(String percentage, String minNodes) {
		double percentOfRequestingNumOfNodes = DefaultConfigurator
				.parseNumber(percentage.replaceAll("\\s+", ""), Double.class);
		int minNumNodes = DefaultConfigurator
				.parseNumber(minNodes.replaceAll("\\s+", ""), Integer.class);

		// if invalid percentage number, everybody is sending
		if (percentOfRequestingNumOfNodes < 0
				|| percentOfRequestingNumOfNodes > 1 || minNumNodes < 0) {
			System.err.println(
					"Wrong inputs for percentageOfSendingClients or minNumNodes");
			this.percentOfRequestingNumOfNodes = 1;
			this.minNumNodes = 1;
		} else {
			this.percentOfRequestingNumOfNodes = percentOfRequestingNumOfNodes;
			this.minNumNodes = minNumNodes;
		}
	}

	@Override
	public int getCount(long timePoint, int numberOfFilteredNodes) {
		int uniformOutput = (int) Math
				.round((numberOfFilteredNodes) * percentOfRequestingNumOfNodes);

		if (uniformOutput < minNumNodes)
			return minNumNodes;
		else
			return uniformOutput;
	}
}
