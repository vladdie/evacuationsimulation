package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;


@Entity
@Table(name = "per_node_role_time_measurements")
public class PerNodeRoleTimeMeasurement extends CustomMeasurement{

	public static final MetricDescription PER_NODE_ROLE_TIME_MEASUREMENT_METRIC = new MetricDescription(
			PerNodeRoleTimesAnalyzerImpl.class, "PerNodeRoleTimeMeasurement",
			"Provides the times nodes have been in their respective roles", "us");
	
	@SuppressWarnings("unused")
	private long nodeID;
	
	// @SuppressWarnings("unused")
	// private long sinkTime;
	//
	// @SuppressWarnings("unused")
	// private long leafTime;
	//
	// @SuppressWarnings("unused")
	// private long offlineTime;
	//
	// @SuppressWarnings("unused")
	// private long sinkActiveTime;
	//
	// @SuppressWarnings("unused")
	// private long leafActiveTime;

	@SuppressWarnings("unused")
	private double sinkTimeRatio;

	@SuppressWarnings("unused")
	private double leafTimeRatio;

	@SuppressWarnings("unused")
	private double sinkActiveTimeRatio;
	
	@SuppressWarnings("unused")
	private double leafActiveTimeRatio;
	
	public PerNodeRoleTimeMeasurement(long nodeID, double sinkTimeRatio, double leafTimeRatio, double sinkActiveTimeRatio,
			double leafActiveTimeRatio) {
		super();
		this.nodeID = nodeID;
		// this.sinkTime = sinkTime;
		// this.leafTime = leafTime;
		// this.sinkActiveTime = sinkActiveTime;
		// this.leafActiveTime = leafActiveTime;
		// this.offlineTime = offlineTime;

		this.sinkTimeRatio = sinkTimeRatio;
		this.leafTimeRatio = leafTimeRatio;
		this.sinkActiveTimeRatio = sinkActiveTimeRatio;
		this.leafActiveTimeRatio = leafActiveTimeRatio;

	}
}
