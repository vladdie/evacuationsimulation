package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.nodeselection;

import java.util.ArrayList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * Selects the first nodeCount nodes from AllNodeIDs.
 * 
 * @author ConstiZ
 *
 */
public class StaticNodeSelection extends AbstractNodeSelection {

	public StaticNodeSelection() {
		super();
	}

	@Override
	public List<INodeID> selectNodes(int nodeCount, List<INodeID> allNodeIDs)
			throws IllegalSelectionException {
		validateSelection(nodeCount, allNodeIDs);

		List<INodeID> selection = new ArrayList<INodeID>(allNodeIDs);
		selection.subList(nodeCount, allNodeIDs.size()).clear();
		return selection;
	}
}
