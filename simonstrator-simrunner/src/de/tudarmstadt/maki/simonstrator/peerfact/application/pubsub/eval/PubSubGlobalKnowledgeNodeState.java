/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import java.util.LinkedList;
import java.util.List;

import de.tud.kom.p2psim.impl.topology.PositionVector;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubInfoComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubAppListener;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubWorkloadApp;

/**
 * State of a specific node (i.e., its subscriptions and publications)
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Sep 17, 2013
 */
public class PubSubGlobalKnowledgeNodeState implements PubSubAppListener {

	private List<Subscription> subs = new LinkedList<Subscription>();

	private final PubSubComponent component;

	private final PubSubInfoComponent infoComponent;

	private final PubSubWorkloadApp app;

	public final INodeID hostId;

	public PubSubGlobalKnowledgeNodeState(PubSubWorkloadApp app,
			PubSubComponent component) {
		this.component = component;
		if (component instanceof PubSubInfoComponent) {
			this.infoComponent = (PubSubInfoComponent) component;
		} else {
			throw new AssertionError("Overlay must implement the PubSubInfoComponent interface!");
		}
		this.app = app;
		this.app.addPubSubListener(this);
		this.hostId = app.getHost().getId();
	}

	@Override
	public void onNewSubscription(Subscription sub) {
		subs.add(sub);
		PubSubGlobalKnowledge.observeSubscription(component, sub);
	}

	@Override
	public void onNewPublication(Notification publication) {
		PubSubGlobalKnowledge.observePublication(component, publication);
	}

	@Override
	public void onUnsubscribe(Subscription sub) {
		boolean removed = subs.remove(sub);
		if (!removed) {
			Monitor.log(PubSubGlobalKnowledge.class, Level.WARN,
					"Unsubscribe on a non-existant Subscription...");
		}
	}

	@Override
	public void onReceivedNotification(Subscription matchedSubscription,
			Notification notification) {
		PubSubGlobalKnowledge.receivedNotification(component, notification,
				matchedSubscription);
	}

	/**
	 * Has to return true, if the node has at least one active Subscription that
	 * matches the given publication.
	 * 
	 * @param publication
	 * @return
	 */
	public boolean hasSubscriptionMatching(Notification publication) {
		return infoComponent.isSubscribedTo(publication);
	}

	public PubSubComponent getComponent() {
		return component;
	}

	public PubSubWorkloadApp getApp() {
		return app;
	}

	public PositionVector getPosition() {
		return app.getHost().getTopologyComponent().getRealPosition();
	}

	@Override
	public String toString() {
		return "H" + app.getHost().getHostId();
	}

}
