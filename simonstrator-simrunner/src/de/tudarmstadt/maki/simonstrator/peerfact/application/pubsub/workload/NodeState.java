/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubWorkloadApp;

/**
 * Container keeping track of a node's state
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Oct 14, 2013
 */
public class NodeState {

	protected final PubSubWorkloadApp app;

	private final INodeID id;

	public boolean enablePublications = false;

	public boolean enableSubscriptions = false;

	public NodeState(PubSubWorkloadApp app) {
		this.app = app;
		this.id = app.getHost().getId();
	}

	public INodeID getId() {
		return this.id;
	}

	public PubSubWorkloadApp getApp() {
		return app;
	}
	
	/**
	 * Actual publishing process. Should invoke onPublish on the {@link PubSubWorkloadApp} to trigger analyzers.
	 * @param n
	 */
	public void doPublish(Notification n) {
		app.onPublish(n);
		app.getPubsub().publish(n);
	}

	/**
	 * Use this method to dispatch Subscriptions to the overlay.
	 * 
	 * @param s
	 * @param l
	 *            listener, can be null
	 */
	public void doSubscribe(Subscription s, final PubSubListener l) {
		PubSubListener innerListener = app.onSubscribe(s, l);
		app.getPubsub().subscribe(s, innerListener);
		((SubscriptionInfoImpl) (s._getSubscriptionInfo(null))).markAsActive();
	}

	/**
	 * Dispatcher for the unsubscribe-operation. Use this method to ensure
	 * correct updates of the global knowledge!
	 * 
	 * @param sub
	 */
	public void doUnsubscribe(Subscription sub) {
		app.onUnsubscribe(sub);
		app.getPubsub().unsubscribe(sub, null);
		((SubscriptionInfoImpl) (sub._getSubscriptionInfo(null))).markAsInactive();
	}

}