package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.transitions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import de.tud.kom.p2psim.impl.scenario.DefaultConfigurator;
import de.tudarmstadt.maki.simonstrator.api.Binder;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.GlobalComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.NodeType;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PubSubGlobalKnowledge;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PubSubResultPersistingMetrics;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlan;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.AtomicTransitionAction;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.StateChangeAction;

/**
 * Controller for Filter Scheme transitions within Bypass.
 * 
 * Solve bootstrap-issue by listening to client activity, and trigger a
 * transition at the client once it becomes active. Automagically enables raw
 * data sampling for publication-related metrics during a transition.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public abstract class AbstractTransitionController implements GlobalComponent, EventHandler, IPeerStatusListener {

	private static final int EVENT_INITIALIZE = 1;

	private static final int EVENT_TRANSITION = 2;

	private static final int EVENT_ENABLE_RAW_SAMPLING = 5;

	private static final int EVENT_DISABLE_RAW_SAMPLING = 6;

	private static final String TARGET_CLIENTS = "Clients";

	private static final String TARGET_CLOUDS = "Clouds";

	private BypassCloudComponent cloud;

	private TransitionCoordinator coordinator;

	private Map<INodeID, BypassClientComponent> clients = new LinkedHashMap<>();

	private Set<BypassClientComponent> activeClients = new LinkedHashSet<>();

	/**
	 * This is used to bootstrap joining clients. It stores a reference to the
	 * last event for each proxy (only for clients).
	 */
	private Map<String, TransitionEvent> lastEventPerProxy = new LinkedHashMap<>();

	private List<OverlayContact> activeClientContacts = new LinkedList<>();

	private long oracleDisseminationDelay = 0;

	private long oracleDisseminationDelayVar = 0;

	private boolean triggerOnlyOneClient = false;

	private final Random rnd = Randoms.getRandom(AbstractTransitionController.class);

	public AbstractTransitionController() {
		Binder.registerComponent(this);
		Event.scheduleImmediately(this, null, EVENT_INITIALIZE);
	}

	/**
	 * Called on simulation startup
	 */
	public void initialize() {
		for (Host host : Oracle.getAllHosts()) {
			try {
				BypassPubSubComponent bypass = host.getComponent(BypassPubSubComponent.class);
				if (bypass.getNodeType() == NodeType.CLOUD) {
					assert cloud == null;
					cloud = (BypassCloudComponent) bypass;
					coordinator = host.getComponent(TransitionCoordinator.class);
				} else {
					assert bypass.getNodeType() == NodeType.CLIENT : "Cloudlets do no longer exist in Bypass...";
					clients.put(host.getId(), (BypassClientComponent) bypass);
					bypass.addPeerStatusListener(this);
				}
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError("A non-bypass host was found?");
			}
		}
		registerCustomTransitions();
	}

	public BypassCloudComponent getCloud() {
		return cloud;
	}

	public Map<INodeID, BypassClientComponent> getClients() {
		return clients;
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		if (clients.containsKey(source.getHost().getId())) {
			BypassClientComponent client = clients.get(source.getHost().getId());
			if (peerStatus == PeerStatus.PRESENT) {
				/*
				 * Replay last transition per proxy
				 */
				for (TransitionEvent event : lastEventPerProxy.values()) {
					if (coordinator != null) {
						// use coordinator
						TransitionExecutionPlan plan = coordinator.createExecutionPlan();
						plan.addAction(new AtomicTransitionAction(event.proxyName, event.targetClass));
						for (Entry<String, Object> state : event.state.entrySet()) {
							plan.addAction(new StateChangeAction(event.proxyName, event.targetClass, state.getKey(),
									state.getValue()));
						}
						coordinator.executeTransitionPlan(Collections.singleton(client.getLocalOverlayContact()), plan);
					} else {
						// use oracle
						client.getTransitionEngine().executeAtomicTransition(event.proxyName, event.targetClass);
						for (Entry<String, Object> state : event.state.entrySet()) {
							client.getTransitionEngine().alterLocalState(event.proxyName, event.targetClass,
									state.getKey(), state.getValue());
						}
					}
				}
				activeClients.add(client);
				activeClientContacts.add(client.getLocalOverlayContact());
			} else if (peerStatus == PeerStatus.ABSENT) {
				activeClients.remove(client);
				activeClientContacts.remove(client.getLocalOverlayContact());
			}
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		switch (type) {
		case EVENT_INITIALIZE:
			initialize();
			break;

		case EVENT_TRANSITION:
			TransitionEvent event = (TransitionEvent) content;
			switch (event.targetGroup) {
			case TARGET_CLIENTS:
				if (coordinator != null) {
					// use coordinator
					TransitionExecutionPlan plan = coordinator.createExecutionPlan();
					plan.addAction(new AtomicTransitionAction(event.proxyName, event.targetClass));
					for (Entry<String, Object> state : event.state.entrySet()) {
						plan.addAction(new StateChangeAction(event.proxyName, event.targetClass, state.getKey(),
								state.getValue()));
					}
					coordinator.executeTransitionPlan(activeClientContacts, plan);
				} else {
					// use oracle
					for (BypassClientComponent client : activeClients) {
						if (oracleDisseminationDelay > 0) {
							long delay = (long) (oracleDisseminationDelay
									+ rnd.nextDouble() * 2 * oracleDisseminationDelayVar - oracleDisseminationDelayVar);
							Event.scheduleWithDelay(delay, new EventHandler() {
								@Override
								public void eventOccurred(Object content, int type) {
									client.getTransitionEngine().executeAtomicTransition(event.proxyName,
											event.targetClass);
									for (Entry<String, Object> state : event.state.entrySet()) {
										client.getTransitionEngine().alterLocalState(event.proxyName, event.targetClass,
												state.getKey(), state.getValue());
									}
								}
							}, null, 0);
						} else { // Instant
							client.getTransitionEngine().executeAtomicTransition(event.proxyName, event.targetClass);
							for (Entry<String, Object> state : event.state.entrySet()) {
								client.getTransitionEngine().alterLocalState(event.proxyName, event.targetClass,
										state.getKey(), state.getValue());
							}
						}
						if (triggerOnlyOneClient) {
							break;
						}
					}
				}
				lastEventPerProxy.put(event.proxyName, event);
				break;
				
			case TARGET_CLOUDS:
				cloud.getTransitionEngine().executeAtomicTransition(event.proxyName, event.targetClass);
				for (Entry<String, Object> state : event.state.entrySet()) {
					cloud.getTransitionEngine().alterLocalState(event.proxyName, event.targetClass, state.getKey(),
							state.getValue());
				}
				break;

			default:
				throw new AssertionError("Unknown Target Group "+event.targetGroup);
			}
			break;

		case EVENT_ENABLE_RAW_SAMPLING:
			PubSubResultPersistingMetrics.ENABLE_RAW_SAMPLING = true;
			break;

		case EVENT_DISABLE_RAW_SAMPLING:
			PubSubResultPersistingMetrics.ENABLE_RAW_SAMPLING = false;
			break;

		default:
			break;
		}
	}

	/**
	 * Sets the delay between transition decision and triggering at the client.
	 * 
	 * @param delay
	 */
	public void setOracleDisseminationDelay(long delay) {
		this.oracleDisseminationDelay = delay;
	}

	/**
	 * IFF the oracle is used (no coordinator), this varies the average delay.
	 * 
	 * @param variance
	 */
	public void setOracleDisseminationVariance(long variance) {
		this.oracleDisseminationDelayVar = variance;
	}

	/**
	 * If true, only one client is triggered in the ORACLE case. Only really
	 * usable for the self-repair mechanism of the ad hoc dissemination
	 * protocols.
	 * 
	 * @param triggerOnlyOneClient
	 */
	public void setTriggerOnlyOneClient(boolean triggerOnlyOneClient) {
		this.triggerOnlyOneClient = triggerOnlyOneClient;
	}

	public void setTransitionsFile(String filename) {
		/*
		 * This parser works for the following csv file structure.
		 * 
		 * timestamp, proxyName, targetMechanism (class)
		 * 
		 */
		BufferedReader csv = null;

		try {
			csv = new BufferedReader(new FileReader(filename));
			while (csv.ready()) {
				String line = csv.readLine();
				if (line.length() == 0 || line.startsWith("#"))
					continue;

				if (line.indexOf(",") > -1) {
					String[] parts = line.split(",");
					long time = DefaultConfigurator.parseNumber(parts[0].trim(), Long.class);
					String targetGroup = parts[1].trim();
					String proxyName = parts[2].trim();
					@SuppressWarnings("unchecked")
					Class<? extends TransitionEnabled> clazz = (Class<? extends TransitionEnabled>) Class
							.forName(parts[3].trim());
					TransitionEvent event = createTransitionEvent(time, targetGroup, clazz, proxyName);
					
					// Mechanism state variables?
					if (parts.length > 4) {
						for (int i = 4; i < parts.length; i++) {
							String[] keyVal = parts[i].trim().split("=");
							event.addState(keyVal[0], keyVal[1]);
						}
					}
					// The actual transition
					Event.scheduleWithDelay(time, this, event, EVENT_TRANSITION);
					/*
					 * Enable sampling before (2 Seconds). We need to wait for
					 * the PublicationDelivery Timeout as well, which is
					 * therefore added as an offset.
					 */
					Event.scheduleWithDelay(time - 2 * Time.SECOND + PubSubGlobalKnowledge.TIMEOUT_PUBLICATION_DELIVERY,
							this, event, EVENT_ENABLE_RAW_SAMPLING);
					// disable afterwards (5 Seconds)
					Event.scheduleWithDelay(time + 5 * Time.SECOND + PubSubGlobalKnowledge.TIMEOUT_PUBLICATION_DELIVERY,
							this, event, EVENT_DISABLE_RAW_SAMPLING);
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Could not open " + filename);
		} finally {
			if (csv != null) {
				try {
					csv.close();
				} catch (IOException e) {
					//
				}
			}
		}
	}

	/**
	 * Create a {@link TransitionEvent} object
	 * 
	 * @param time
	 * @param targetGroup
	 * @param targetClass
	 * @param proxyName
	 * @return
	 */
	protected TransitionEvent createTransitionEvent(long time, String targetGroup,
			Class<? extends TransitionEnabled> targetClass, String proxyName) {
		return new TransitionEvent(time, proxyName, targetClass, targetGroup);
	}

	/**
	 * Hook to register custom transitions
	 */
	protected abstract void registerCustomTransitions();

	/**
	 * Maybe someone wants to extend this to add more features that can be
	 * configured for a transition?
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class TransitionEvent {

		public final long time;

		public final Class<? extends TransitionEnabled> targetClass;

		public final String proxyName;

		public final String targetGroup;

		public final Map<String, Object> state;

		public TransitionEvent(long time, String proxyName, Class<? extends TransitionEnabled> targetClass,
				String targetGroup) {
			this.time = time;
			this.proxyName = proxyName;
			this.targetClass = targetClass;
			this.targetGroup = targetGroup;
			this.state = new LinkedHashMap<>();
		}

		public void addState(String name, String value) {
			try {
				if (value.contains(".")) {
					Double val = Double.valueOf(value);
					state.put(name, val);
				} else {
					Integer val = Integer.valueOf(value);
					state.put(name, val);
				}
			} catch (NumberFormatException e) {
				// just store the String
				state.put(name, value);
			}
		}

	}

}
