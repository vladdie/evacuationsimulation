package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;


@Entity
@Table(name = "per_node_no_sink_time_measurements")
public class PerNodeNoSinkTimeMeasurement extends CustomMeasurement{

	public static final MetricDescription PER_NODE_NO_SINK_TIME_METRIC = new MetricDescription(
			PerNodeHasNoSinkTimesAnalyzerImpl.class, "PerNodeNoSinkTimeMeasurement",
			"Provides the times nodes have been in their respective states", "us");
	
	@SuppressWarnings("unused")
	private long nodeID;
	
	@SuppressWarnings("unused")
	private long timeWithoutSink;
	
	@SuppressWarnings("unused")
	private long onlineTime;
	
	public PerNodeNoSinkTimeMeasurement(long nodeID, long timeWithoutSink, long onlineTime) {
		super();
		this.nodeID = nodeID;
		this.timeWithoutSink = timeWithoutSink;
		this.onlineTime = onlineTime;
	}
}
