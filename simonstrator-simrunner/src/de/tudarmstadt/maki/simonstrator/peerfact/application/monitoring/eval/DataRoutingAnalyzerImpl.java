package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.impl.simengine.Simulator;
import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tud.kom.p2psim.impl.util.oracle.GlobalOracle;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.cloudlet.CraterCloudletComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.AggregationFunctionNotKnownException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerUploadImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObject;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupInsensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupSensitiveImpl;

public class DataRoutingAnalyzerImpl implements DataRoutingAnalyzer, EventHandler {

	private CraterCentralComponent cloud;

	private final int _EVENT_ENABLE_DAO_AT = 2001;

	private final int _EVENT_WRITE_SINGLE_LATENCIES = 2002;

	private final long WRITE_SINGLE_LATENCIES_INTERVAL = 3 * Time.MINUTE;

	/*************************************************************************************************************************
	 ******************************************** Delivery & Duplicate Ratios Variables ***********************************
	 *************************************************************************************************************************/

	/*
	 * Container Messages
	 */
	@SuppressWarnings("serial")
	private HashMap<UniqueID, INodeID> sendInNetworkContainerMessages = new LinkedHashMap<UniqueID, INodeID>(10000 * 3 / 2, 0.7f,
			true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<UniqueID, INodeID> eldest) {
			return size() > 80000;
		}
	};
	private long sendCount_inNetworkContainerMessage = 0;
	private long receivedCount_atSink_containerMessage = 0;
	private long duplicateCount_atSink_containerMessage = 0;

	/*
	 * Upload Messages
	 */
	@SuppressWarnings("serial")
	private HashMap<UniqueID, Short> sendUploadMessages = new LinkedHashMap<UniqueID, Short>(80000 * 3 / 2, 0.7f, true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<UniqueID, Short> eldest) {
			return size() > 80000;
		}
	};
	@SuppressWarnings("serial")
	private HashMap<UniqueID, Short> receivedUploadMessages = new LinkedHashMap<UniqueID, Short>(80000 * 3 / 2, 0.7f, true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<UniqueID, Short> eldest) {
			return size() > 80000;
		}
	};
	private long sendCount_sinkUploadContainerMessage = 0;
	private long sendCount_cloudletUploadContainerMessage = 0;
	private long receivedCount_uploadContainerMessage = 0;
	private long duplicateCount_uploadContainerMessage = 0;

	/*
	 * Normal Data
	 */
	@SuppressWarnings("serial")
	private HashMap<UniqueID, Short> sendNormalData = new LinkedHashMap<UniqueID, Short>(80000 * 3 / 2, 0.7f, true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<UniqueID, Short> eldest) {
			return size() > 80000;
		}
	};
	@SuppressWarnings("serial")
	private HashMap<UniqueID, Short> received_atSink_NormalData = new LinkedHashMap<UniqueID, Short>(80000 * 3 / 2, 0.7f, true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<UniqueID, Short> eldest) {
			return size() > 80000;
		}
	};
	@SuppressWarnings("serial")
	private HashMap<UniqueID, Short> received_atCentral_NormalData = new LinkedHashMap<UniqueID, Short>(80000 * 3 / 2, 0.7f,
			true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<UniqueID, Short> eldest) {
			return size() > 80000;
		}
	};
	private long sendCount_inNetwork_NormalData = 0;
	private long sendCount_inUpload_NormalData = 0;
	private long receivedCount_atSink_NormalData = 0;
	private long receivedCount_atCentral_NormalData = 0;
	private long duplicateCount_atSink_NormalData = 0;
	private long duplicateCount_atCentral_NormalData = 0;

	private long duplicateCountDetectedAtSink_NormalData = 0;

	/*
	 * Duplicate Sensitive Data
	 */
	@SuppressWarnings("serial")
	private HashMap<UniqueID, Short> sendDupSensData = new LinkedHashMap<UniqueID, Short>(80000 * 3 / 2, 0.7f, true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<UniqueID, Short> eldest) {
			return size() > 80000;
		}
	};
	// @SuppressWarnings("serial")
	// private HashMap<UniqueID, Short> sendDupSensData_inNetwork = new
	// LinkedHashMap<UniqueID, Short>(80000 * 3 / 2, 0.7f, true) {
	// @Override
	// protected boolean removeEldestEntry(Map.Entry<UniqueID, Short> eldest) {
	// return size() > 80000;
	// }
	// };
	@SuppressWarnings("serial")
	private HashMap<UniqueID, Short> received_atSink_DupSensData = new LinkedHashMap<UniqueID, Short>(80000 * 3 / 2, 0.7f, true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<UniqueID, Short> eldest) {
			return size() > 80000;
		}
	};
	@SuppressWarnings("serial")
	private HashMap<UniqueID, Short> received_atCentral_DupSensData = new LinkedHashMap<UniqueID, Short>(80000 * 3 / 2, 0.7f,
			true) {
		@Override
		protected boolean removeEldestEntry(Map.Entry<UniqueID, Short> eldest) {
			return size() > 80000;
		}
	};
	private long sendCount_inNetwork_DupSensData = 0;
	private long sendCount_inUpload_DupSensData = 0;
	private long receivedCount_atSink_DupSensData = 0;
	private long receivedCount_atCentral_DupSensData = 0;
	private long duplicateCount_atSink_DubSensData = 0;
	private long duplicateCount_atCentral_DubSensData = 0;

	private long duplicateCountDetectedAtSink_DupSensData = 0;
	private long duplicateCountDetectedAtCentral_DupSensData = 0;

	private HashMap<UniqueID, Integer> numberOfDuplicateOfSameTypeAtSink_ContainerMessage = new HashMap<UniqueID, Integer>();

	/**
	 * Data send out in {@link DataMessageContainerLocalImpl}'s.
	 * 
	 * Overall data, including all {@link DataObject}'s ever send out (added).
	 * 
	 * Ground-truth data, which was actually send out. Data send in the network!
	 */
	private LinkedList<DataObjectAggregationDupInsensitiveImpl> sendAggregationDupInsensitiveData = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();

	/**
	 * Data send out in {@link DataMessageContainerUploadImpl}'s.
	 * 
	 * Ground-truth data, which was actually added by sinks! Caution, this data has to be merged with the send in
	 * network data!
	 */
	private LinkedList<DataObjectAggregationDupInsensitiveImpl> sendAggregationDupInsensitiveDataAtSink = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();

	/**
	 * Received data at sinks. After duplicate resolving!
	 * 
	 * So this is the data that is being uploaded to the central entity! (But maybe not received from the central
	 * entity.)
	 */
	private LinkedList<DataObjectAggregationDupInsensitiveImpl> receivedAtSink_AggregationDupInsensitiveData = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();

	/**
	 * Received data at central entity. After again duplicate resolving there.
	 * 
	 * So this is the final data that would be used for further analysis.
	 */
	private LinkedList<DataObjectAggregationDupInsensitiveImpl> receivedAtCentral_AggregationDupInsensitiveData = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();

	/*************************************************************************************************************************
	 ************************************************* Latency Analyzing Variables *******************************************
	 ************************************************************************************************************************* 
	 * 
	 * InNetwork describes latency from origin to sink.
	 * 
	 * !Currently not considered! InUpload describes the latency from sink (sending) to central entity.
	 * 
	 * EndToEnd describes the latency from origin to central entity.
	 * 
	 * Why three different latencies? As the upload interval of the sinks is changeable, thus it is interesting how long
	 * the data needs overall but also how long data needs in network.
	 */
	private LinkedHashMap<UniqueID, Long> sendInNetworkContainerMessagesTimestamps = new LinkedHashMap<UniqueID, Long>();
	private long latency_containerMessagesInNetwork = 0;
	private long latencyCount_containerMessagesInNetwork = 0;
	private LinkedList<Long> sendInNetworkContainerMessagesTimestamps_all = new LinkedList<Long>();

	private LinkedHashMap<UniqueID, Long> sendInUploadContainerMessagesTimestamps = new LinkedHashMap<UniqueID, Long>();
	private long latency_containerMessagesInUpload = 0;
	private long latencyCount_containerMessagesInUpload = 0;
	private LinkedList<Long> sendInUploadContainerMessagesTimestamps_all = new LinkedList<Long>();

	private LinkedHashMap<UniqueID, Long> sendInNetworkNormalDataObjectTimestamps = new LinkedHashMap<UniqueID, Long>();
	private long latency_NormalDataObjectInNetwork = 0;
	private long latencyCount_NormalDataObjectInNetwork = 0;
	private LinkedList<UniqueID> latency_alreadyConsideredNormalDataObjectInNetwork = new LinkedList<UniqueID>();
	private long latency_NormalDataObjectEndToEnd = 0;
	private long latencyCount_NormalDataObjectEndToEnd = 0;
	private LinkedList<Long> latencies_NormalDataObjectEndToEnd_all = new LinkedList<Long>();
	private LinkedList<UniqueID> latency_alreadyConsideredNormalDataObjectEndToEnd = new LinkedList<UniqueID>();

	private LinkedHashMap<UniqueID, Long> sendInNetworkDupSensEntryTimestamps = new LinkedHashMap<UniqueID, Long>();
	private long latency_DupSensEntryInNetwork = 0;
	private long latencyCount_DupSensEntryInNetwork = 0;
	private LinkedList<UniqueID> latency_alreadyConsideredDupSensEntryInNetwork = new LinkedList<UniqueID>();
	private long latency_DupSensEntryEndToEnd = 0;
	private long latencyCount_DupSensEntryEndToEnd = 0;
	private LinkedList<Long> latencies_DubSensEntryEndToEnd_all = new LinkedList<Long>();
	private LinkedList<UniqueID> latency_alreadyConsideredDupSensEntryEndToEnd = new LinkedList<UniqueID>();


	private boolean started = false;

	private boolean ENABLE_DAO = false;

	// private long centralID;

	private boolean withCraterFunctionality;

	@XMLConfigurableConstructor({ "withCraterFunctionality" })
	public DataRoutingAnalyzerImpl(boolean withCraterFunctionality) {
		this.withCraterFunctionality = withCraterFunctionality;

		DAO.addEntityClass(DataRoutingMeasurement.class);
		DAO.addEntityClass(LatencyMeasurement.class);
		DAO.addEntityClass(LatencySingleMeasurements.class);
	}

	public void setEnableDao(boolean enableDao) {
		this.ENABLE_DAO = enableDao;
	}

	public void setTimeEnableDao(long timeEnableDao) {
		if (ENABLE_DAO) {
			Event.scheduleWithDelay(timeEnableDao, this, null, _EVENT_ENABLE_DAO_AT);
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// _EVENT_ENABLE_DAO_AT
		assert ENABLE_DAO;
		if (type == _EVENT_ENABLE_DAO_AT) {
			for (SimHost host : GlobalOracle.getHosts()) {
				try {
					CraterCentralComponent cloud = host.getComponent(CraterCentralComponent.class);
					this.cloud = cloud;
				} catch (ComponentNotAvailableException e) {
					// keine cloud
				}
			}
			assert cloud != null : "Es wird keine Cloud Instanz in dieser Simulation genutzt.";
			// centralID =
			// Simulator.getInstance().getScenario().getHosts().get("central_entity").iterator().next().getHostId();
			started = true;

			Event.scheduleWithDelay(WRITE_SINGLE_LATENCIES_INTERVAL, this, null, _EVENT_WRITE_SINGLE_LATENCIES);

		} else if (type == _EVENT_WRITE_SINGLE_LATENCIES) {

			writeSingleMeasurementsToDB();
			Event.scheduleWithDelay(WRITE_SINGLE_LATENCIES_INTERVAL, this, null, _EVENT_WRITE_SINGLE_LATENCIES);
		} else {
			throw new Error("Unknown Event Type");
		}
	}

	@Override
	public void start() {
		// if (!ENABLE_DAO) {
		// return;
		// }
		//
		// for (SimHost host : GlobalOracle.getHosts()) {
		// try {
		// CraterCentralComponent cloud = host.getComponent(CraterCentralComponent.class);
		// this.cloud = cloud;
		// } catch (ComponentNotAvailableException e) {
		// // keine cloud
		// }
		// }
		// assert cloud != null : "Es wird keine Cloud Instanz in dieser Simulation genutzt.";
		// // centralID =
		// // Simulator.getInstance().getScenario().getHosts().get("central_entity").iterator().next().getHostId();
		// started = true;
	}

	public void writeSingleMeasurementsToDB() {
		/*
		 * Go through all lists
		 */
		for (Long actLatency : sendInNetworkContainerMessagesTimestamps_all) {
			LatencySingleMeasurements latencyMeasurement = new LatencySingleMeasurements(LatencyOrigin.Container_Local.toString(),
					actLatency);
			MeasurementDAO.storeCustomMeasurement(LatencySingleMeasurements.LATENCY_METRIC, cloud.getHost().getId().value(),
					Simulator.getCurrentTime(), latencyMeasurement);
		}
		sendInNetworkContainerMessagesTimestamps_all.clear();

		for (Long actLatency : sendInUploadContainerMessagesTimestamps_all) {
			LatencySingleMeasurements latencyMeasurement = new LatencySingleMeasurements(
					LatencyOrigin.Container_Upload.toString(), actLatency);
			MeasurementDAO.storeCustomMeasurement(LatencySingleMeasurements.LATENCY_METRIC, cloud.getHost().getId().value(),
					Simulator.getCurrentTime(), latencyMeasurement);
		}
		sendInUploadContainerMessagesTimestamps_all.clear();

		for (Long actLatency : latencies_NormalDataObjectEndToEnd_all) {
			LatencySingleMeasurements latencyMeasurement = new LatencySingleMeasurements(LatencyOrigin.EndToEnd_Normal.toString(),
					actLatency);
			MeasurementDAO.storeCustomMeasurement(LatencySingleMeasurements.LATENCY_METRIC, cloud.getHost().getId().value(),
					Simulator.getCurrentTime(), latencyMeasurement);
		}
		latencies_NormalDataObjectEndToEnd_all.clear();

		for (Long actLatency : latencies_DubSensEntryEndToEnd_all) {
			LatencySingleMeasurements latencyMeasurement = new LatencySingleMeasurements(
					LatencyOrigin.EndToEnd_DubSens.toString(), actLatency);
			MeasurementDAO.storeCustomMeasurement(LatencySingleMeasurements.LATENCY_METRIC, cloud.getHost().getId().value(),
					Simulator.getCurrentTime(), latencyMeasurement);
		}
		latencies_DubSensEntryEndToEnd_all.clear();

	}

	/**
	 * Enumeration used to distinguish between the different latencies in the DB.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	private enum LatencyOrigin {
		Container_Local,

		Container_Upload,

		EndToEnd_Normal,

		EndToEnd_DubSens;
	}

	@Override
	public void stop(Writer out) {
		if (withCraterFunctionality) {
			statisticsAtSinkLevel();
			statisticsAtSinkLevel_NormalData();
			statisticsAtSinkLevel_AggregationDupInsensitiveData();
			statisticsAtSinkLevel_AggregationDupSensitiveData();
		}
		statisticsAtCentralLevel();
		statisticsAtCentralLevel_NormalData();
		statisticsAtCentralLevel_AggregationDupInsensitiveData();
		statisticsAtCentralLevel_AggregationDupSensitiveData();

		statisticsForLatency();

		if (ENABLE_DAO) {
			writeToDB();
			writeSingleMeasurementsToDB();
		}
	}

	@Override
	public void onContainerMessageSend(UniqueID uniqueMsgId, INodeID sinkId) {
		if (!started)
			return;

		sendInNetworkContainerMessages.put(uniqueMsgId, sinkId);

		sendCount_inNetworkContainerMessage++;

		sendInNetworkContainerMessagesTimestamps.put(uniqueMsgId, Simulator.getCurrentTime());

	}

	@Override
	public void onContainerMessageReceivedBySink(UniqueID uniqueMsgId, INodeID sinkId) {
		if (!started) {
			return;
		}

		if (!sendInNetworkContainerMessages.containsKey(uniqueMsgId)) {
			return;
		}

		receivedCount_atSink_containerMessage++;

		// Latencies
		long currentLatency = Simulator.getCurrentTime() - sendInNetworkContainerMessagesTimestamps.remove(uniqueMsgId);
		latency_containerMessagesInNetwork += currentLatency;
		sendInNetworkContainerMessagesTimestamps_all.add(currentLatency);
		latencyCount_containerMessagesInNetwork++;
	}

	@Override
	public void onSuccessiveContainerMessageReceivedBySink(UniqueID uniqueMsgId) {
		if (!started)
			return;

		if (numberOfDuplicateOfSameTypeAtSink_ContainerMessage.containsKey(uniqueMsgId)) {
			numberOfDuplicateOfSameTypeAtSink_ContainerMessage.put(uniqueMsgId,
					numberOfDuplicateOfSameTypeAtSink_ContainerMessage.get(uniqueMsgId) + 1);
		} else {
			numberOfDuplicateOfSameTypeAtSink_ContainerMessage.put(uniqueMsgId, 1);
		}
		duplicateCount_atSink_containerMessage++;
	}

	@Override
	public void onNormalDataObjectAddedFirstTime(UniqueID dataObjectID) {
		if (!started)
			return;

		// sendInitialNormalDataObjects.add(dataObjectID);
	}

	@Override
	public void onNormalDataObjectAddedInNetwork(UniqueID dataObjectID) {
		if (!started)
			return;

		sendNormalData.put(dataObjectID, null);
		sendCount_inNetwork_NormalData++;

		sendInNetworkNormalDataObjectTimestamps.put(dataObjectID, Simulator.getCurrentTime());
	}

	@Override
	public void onNormalDataObjectReceivedAtSink(UniqueID dataObjectID) {
		if (!started)
			return;

		// Timestamping InNetwork NormalData.
		if (sendInNetworkNormalDataObjectTimestamps.containsKey(dataObjectID)
				&& !latency_alreadyConsideredNormalDataObjectInNetwork.contains(dataObjectID)) {
			latencyCount_NormalDataObjectInNetwork++;
			latency_NormalDataObjectInNetwork += Simulator.getCurrentTime()
					- sendInNetworkNormalDataObjectTimestamps.get(dataObjectID);
			latency_alreadyConsideredNormalDataObjectInNetwork.add(dataObjectID);
		}
	}

	@Override
	public void onDupSensEntriesReceivedAtSink(LinkedList<UniqueID> includedEntriesIDs) {
		if (!started)
			return;

		for (UniqueID actEntryID : includedEntriesIDs) {
			// Timestamping InNetwork NormalData.
			if (sendInNetworkDupSensEntryTimestamps.containsKey(actEntryID)
					&& !latency_alreadyConsideredDupSensEntryInNetwork.contains(actEntryID)) {
				latencyCount_DupSensEntryInNetwork++;
				latency_DupSensEntryInNetwork += Simulator.getCurrentTime() - sendInNetworkDupSensEntryTimestamps.get(actEntryID);
				latency_alreadyConsideredDupSensEntryInNetwork.add(actEntryID);
			}
		}
	}

	@Override
	public void onDuplicateInsensitiveAggregationDataAddedInNetwork(String aggregationFunctionId, int attributeId, double value) {
		if (!started)
			return;

		boolean entryExisted = false;
		for (DataObjectAggregationDupInsensitiveImpl actDataObject : sendAggregationDupInsensitiveData) {
			if (actDataObject.getAggregationFunctionId().equals(aggregationFunctionId)
					&& actDataObject.getAttributeId() == attributeId) {
				actDataObject.putValue(value);
				entryExisted = true;
			}
		}

		if (!entryExisted) {
			DataObjectAggregationDupInsensitiveImpl aggregationDupInsensitvieDataObejct;
			try {
				aggregationDupInsensitvieDataObejct = new DataObjectAggregationDupInsensitiveImpl(aggregationFunctionId,
						attributeId, value);
				sendAggregationDupInsensitiveData.add(aggregationDupInsensitvieDataObejct);
			} catch (AggregationFunctionNotKnownException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onDuplicateSensitiveAggregationDataAddedInNetwork(UniqueID addedEntry) {
		if (!started)
			return;

		sendDupSensData.put(addedEntry, null);
		// sendDupSensData_inNetwork.put(addedEntry, null);
		sendCount_inNetwork_DupSensData++;

		sendInNetworkDupSensEntryTimestamps.put(addedEntry, Simulator.getCurrentTime());
	}

	@Override
	public void onNormalDataObjectAddedAtSink(UniqueID dataObjectID) {
		if (!started)
			return;

		sendNormalData.put(dataObjectID, null);
		sendCount_inUpload_NormalData++;
	}

	@Override
	public void onDuplicateInsensitiveAggregationDataAddedAtSink(String aggregationFunctionId, int attributeId, double value) {
		if (!started)
			return;

		boolean entryExisted = false;
		for (DataObjectAggregationDupInsensitiveImpl actDataObject : sendAggregationDupInsensitiveDataAtSink) {
			if (actDataObject.getAggregationFunctionId().equals(aggregationFunctionId)
					&& actDataObject.getAttributeId() == attributeId) {
				actDataObject.putValue(value);
				entryExisted = true;
			}
		}

		if (!entryExisted) {
			DataObjectAggregationDupInsensitiveImpl aggregationDupInsensitvieDataObejct;
			try {
				aggregationDupInsensitvieDataObejct = new DataObjectAggregationDupInsensitiveImpl(aggregationFunctionId,
						attributeId, value);
				sendAggregationDupInsensitiveDataAtSink.add(aggregationDupInsensitvieDataObejct);
			} catch (AggregationFunctionNotKnownException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void onDuplicateSensitiveAggregationEntryAddedAtSink(UniqueID addedEntryID) {
		if (!started)
			return;

		sendDupSensData.put(addedEntryID, null);
		sendCount_inUpload_DupSensData++;
	}

	@Override
	public void onMessageContentUploadedAtSink(LinkedList<UniqueID> normalDataList,
			LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveDataList,
			LinkedList<LinkedList<UniqueID>> aggregationDupSensitiveEntryIDList) {
		if (!started)
			return;

		for (UniqueID actNormalDataId : normalDataList) {
			if (sendNormalData.containsKey(actNormalDataId)) {
				if (received_atSink_NormalData.containsKey(actNormalDataId))
					duplicateCount_atSink_NormalData++;
				else {
					received_atSink_NormalData.put(actNormalDataId, null);
					receivedCount_atSink_NormalData++;
				}
			}

		}

		receivedAtSink_AggregationDupInsensitiveData.addAll(aggregationDupInsensitiveDataList);

		for (LinkedList<UniqueID> listOfDupSensEntries : aggregationDupSensitiveEntryIDList) {
			for (UniqueID actDupSensDataId : listOfDupSensEntries) {
				// if (sendDupSensData_inNetwork.containsKey(actDupSensDataId))
				// {
				if (sendDupSensData.containsKey(actDupSensDataId)) {
					if (received_atSink_DupSensData.containsKey(actDupSensDataId))
						duplicateCount_atSink_DubSensData++;
					else {
						received_atSink_DupSensData.put(actDupSensDataId, null);
						receivedCount_atSink_DupSensData++;
					}
				}
			}

		}

	}

	@Override
	public void onMessageContentReceivedAtServer(LinkedList<UniqueID> normalDataList,
			LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveDataList,
			LinkedList<DataObjectAggregationDupSensitiveImpl> aggregationDupSensitiveDataList) {
		if (!started)
			return;

		// Timestamping EndToEnd NormalData.
		for (UniqueID actNormalDataID : normalDataList) {
			if (sendInNetworkNormalDataObjectTimestamps.containsKey(actNormalDataID)
					&& !latency_alreadyConsideredNormalDataObjectEndToEnd.contains(actNormalDataID)) {
				latencyCount_NormalDataObjectEndToEnd++;

				long currentLatency = Simulator.getCurrentTime()
						- sendInNetworkNormalDataObjectTimestamps.remove(actNormalDataID);
				latency_NormalDataObjectEndToEnd += currentLatency;
				latencies_NormalDataObjectEndToEnd_all.add(currentLatency);
				latency_alreadyConsideredNormalDataObjectEndToEnd.add(actNormalDataID);
				// latency_alreadyConsideredNormalDataObjectInNetwork.remove(actNormalDataID);
			}
		}

		for (UniqueID uniqueID : normalDataList) {
			if (sendNormalData.containsKey(uniqueID)) {
				if (received_atCentral_NormalData.containsKey(uniqueID))
					duplicateCount_atCentral_NormalData++;
				else {
					received_atCentral_NormalData.put(uniqueID, null);
					receivedCount_atCentral_NormalData++;
				}
			}

		}

		receivedAtCentral_AggregationDupInsensitiveData.addAll(aggregationDupInsensitiveDataList);

		/*
		 * Add duplicate sensitive data to alreadyRec data.
		 */
		// go through msg content
		for (DataObjectAggregationDupSensitiveImpl actReceivedDupSensAggDataObject : aggregationDupSensitiveDataList) {

			// Timestamping EndToEnd DupSensData
			for (UniqueID actEntryID : actReceivedDupSensAggDataObject.getIncludedEntriesIDs()) {
				if (sendInNetworkDupSensEntryTimestamps.containsKey(actEntryID)
						&& !latency_alreadyConsideredDupSensEntryEndToEnd.contains(actEntryID)) {
					latencyCount_DupSensEntryEndToEnd++;
					long currentLatency = Simulator.getCurrentTime() - sendInNetworkDupSensEntryTimestamps.remove(actEntryID);
					latency_DupSensEntryEndToEnd += currentLatency;
					latencies_DubSensEntryEndToEnd_all.add(currentLatency);
					latency_alreadyConsideredDupSensEntryEndToEnd.add(actEntryID);
					// latency_alreadyConsideredDupSensEntryInNetwork.remove(actEntryID);
				}

				if (sendDupSensData.containsKey(actEntryID)) {
					if (received_atCentral_DupSensData.containsKey(actEntryID))
						duplicateCount_atCentral_DubSensData++;
					else {
						received_atCentral_DupSensData.put(actEntryID, null);
						receivedCount_atCentral_DupSensData++;
					}
				}
			}

		}
	}

	@Override
	public void onAggregationDupSensitiveEntryAddedFirstTime(AggregationFunctionEntry addedEntry) {
		if (!started)
			return;

		// sendInitialAggregationDupSensitiveEntryIDs.add(addedEntry.getUniqueEntryID());
	}

	@Override
	public void onAggregationDupSensitiveDuplicateDetectedAtSink() {
		if (!started)
			return;

		duplicateCountDetectedAtSink_DupSensData++;
	}

	@Override
	public void onAggregationDupSensitiveDuplicateDetectedAtServer() {
		if (!started)
			return;

		duplicateCountDetectedAtCentral_DupSensData++;
	}

	@Override
	public void onNormalDataDuplicateDetectedAtSink() {
		if (!started)
			return;

		duplicateCountDetectedAtSink_NormalData++;
	}

	@Override
	public void uploadMsgSend(UniqueID uniqueId, CraterComponent comp) {
		if (!started)
			return;

		if (sendUploadMessages.keySet().contains(uniqueId)) {
			throw new Error("Normally messages id's should be UNIQUE!!");
		}
		sendUploadMessages.put(uniqueId, null);
		if (comp instanceof CraterNodeComponent) {
			sendCount_sinkUploadContainerMessage++;
		} else if (comp instanceof CraterCloudletComponent) {
			sendCount_cloudletUploadContainerMessage++;
		} else {
			throw new InstantiationError();
		}

		sendInUploadContainerMessagesTimestamps.put(uniqueId, Simulator.getCurrentTime());
	}

	@Override
	public void uploadMsgReceived(UniqueID uniqueId) {
		if (!started)
			return;

		if (!sendUploadMessages.keySet().contains(uniqueId))
			return;

		if (receivedUploadMessages.keySet().contains(uniqueId)) {
			duplicateCount_uploadContainerMessage++;
		} else {
			receivedUploadMessages.put(uniqueId, null);
			receivedCount_uploadContainerMessage++;

			// Latencies
			latencyCount_containerMessagesInUpload++;
			long currentLatency = Simulator.getCurrentTime() - sendInUploadContainerMessagesTimestamps.remove(uniqueId);
			latency_containerMessagesInUpload += currentLatency;
			sendInUploadContainerMessagesTimestamps_all.add(currentLatency);
		}
	}

	/*
	 * Variables for DB Output!
	 */
	private double deliveryRatioAtSink_ContainerMsg;
	private double duplicateRatioAtSink_ContainerMsg;
	private double averageSameContainerMsgRec;
	private double singleMaxSameContainerMsgRec;
	private double deliveryRatioAtSink_normalData;
	private double duplicateRatioAtSinkWithoutResolving_normalData;
	private double duplicateRatioAtSinkWithResolving_normalData;
	private double deliveryRatioAtSink_dupSensData;
	private double duplicateRatioAtSinkWithoutResolving_dupSensData;
	private double duplicateRatioAtSinkWithResolving_dupSensData;
	private double deliveryRatioAtCentral_UploadMsg;
	private double duplicateRatioAtCentral_UploadMsg;
	private double deliveryRatioAtCentral_normalData;
	private double duplicateRatioAtCentral_normalData;
	private double deliveryRatioAtCentral_dupSensData;
	private double duplicateRatioAtCentral_dupSensData;

	private void statisticsAtSinkLevel() {
		// long measurementTime = (Simulator.getEndTime() - 1 * Time.HOUR) /
		// Time.SECOND;

		deliveryRatioAtSink_ContainerMsg = (double) receivedCount_atSink_containerMessage
				/ (double) sendCount_inNetworkContainerMessage;

		if (Double.isNaN(deliveryRatioAtSink_ContainerMsg))
			deliveryRatioAtSink_ContainerMsg = 0;

		// double deliveryRatio = ((double)
		// Math.round((deliveryRatioAtSink_ContainerMsg) * 10000)) / 100;
		//
		// double sendPerSec = ((double) Math.round(((double)
		// sendCount_inNetworkContainerMessage / (double) measurementTime) *
		// 100)) / 100;
		// double recPerSec = ((double) Math
		// .round(((double) receivedCount_atSink_containerMessage / (double)
		// measurementTime) * 100)) / 100;

		duplicateRatioAtSink_ContainerMsg = (double) duplicateCount_atSink_containerMessage
				/ (double) (receivedCount_atSink_containerMessage + duplicateCount_atSink_containerMessage);
		if (Double.isNaN(duplicateRatioAtSink_ContainerMsg))
			duplicateRatioAtSink_ContainerMsg = 0;

		// double dupRatio = ((double)
		// Math.round((duplicateRatioAtSink_ContainerMsg) * 10000)) / 100;

		// log.debug("SINK LEVEL: ContainerMessages send per second " +
		// sendPerSec + " ; rec per second " + recPerSec
		// + " ; Delivery ratio " + deliveryRatio + " % duplicate ratio " +
		// dupRatio + " %");

		if (duplicateCount_atSink_containerMessage == 0) {
			averageSameContainerMsgRec = 0;
		} else {
			averageSameContainerMsgRec = (double) duplicateCount_atSink_containerMessage
					/ (double) numberOfDuplicateOfSameTypeAtSink_ContainerMessage.size();
		}

		// double averageNumberOfDupContainerReceived = ((double)
		// Math.round((averageSameContainerMsgRec) * 100)) / 100;

		int maxNumberOfDup = 0;
		for (int actNumberOfDupRec : numberOfDuplicateOfSameTypeAtSink_ContainerMessage.values()) {
			if (maxNumberOfDup < actNumberOfDupRec)
				maxNumberOfDup = actNumberOfDupRec;
		}

		singleMaxSameContainerMsgRec = maxNumberOfDup;
	}

	private void statisticsAtSinkLevel_NormalData() {
		deliveryRatioAtSink_normalData = (double) receivedCount_atSink_NormalData / (double) sendCount_inNetwork_NormalData;
		if (Double.isNaN(deliveryRatioAtSink_normalData))
			deliveryRatioAtSink_normalData = 0;

		// double deliveryRatio = ((double)
		// Math.round((deliveryRatioAtSink_normalData) * 10000)) / 100;

		duplicateRatioAtSinkWithoutResolving_normalData = ((double) duplicateCount_atSink_NormalData
				+ duplicateCountDetectedAtSink_NormalData)
				/ (double) (receivedCount_atSink_NormalData + duplicateCount_atSink_NormalData
						+ duplicateCountDetectedAtSink_NormalData);
		if (Double.isNaN(duplicateRatioAtSinkWithoutResolving_normalData))
			duplicateRatioAtSinkWithoutResolving_normalData = 0;

		duplicateRatioAtSinkWithResolving_normalData = (double) duplicateCount_atSink_NormalData
				/ (double) (receivedCount_atSink_NormalData + duplicateCount_atSink_NormalData);
		if (Double.isNaN(duplicateRatioAtSinkWithResolving_normalData))
			duplicateRatioAtSinkWithResolving_normalData = 0;

		// double duplicateRatioOfDelivered = ((double)
		// Math.round(duplicateRatioAtSinkWithResolving_normalData * 10000)) /
		// 100;

		// double duplicateRatioResolvedDuplicates = ((double) Math
		// .round(((double) duplicateCountDetectedAtSink_NormalData / (double)
		// (duplicateCountDetectedAtSink_NormalData +
		// duplicateCount_atSink_NormalData)) * 10000)) / 100;

	}

	private void statisticsAtSinkLevel_AggregationDupInsensitiveData() {
		LinkedList<DataObjectAggregationDupInsensitiveImpl> alreadyReceivedAggregationDupInsensitiveData = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();

		for (DataObjectAggregationDupInsensitiveImpl actReceivedObject : receivedAtSink_AggregationDupInsensitiveData) {
			boolean entryExisted = false;
			for (DataObjectAggregationDupInsensitiveImpl actDataObject : alreadyReceivedAggregationDupInsensitiveData) {
				if (actReceivedObject.ofSameType(actDataObject)) {
					actDataObject.putValue(actReceivedObject.getAggregateValue());
					entryExisted = true;
				}
			}

			if (!entryExisted) {
				DataObjectAggregationDupInsensitiveImpl aggregationDupInsensitvieDataObejct;
				try {
					aggregationDupInsensitvieDataObejct = new DataObjectAggregationDupInsensitiveImpl(
							actReceivedObject.getAggregationFunctionId(), actReceivedObject.getAttributeId(),
							actReceivedObject.getAggregateValue());
					alreadyReceivedAggregationDupInsensitiveData.add(aggregationDupInsensitvieDataObejct);
				} catch (AggregationFunctionNotKnownException e) {
					e.printStackTrace();
				}
			}
		}

		// for (DataObjectAggregationDupInsensitiveImpl actSendObject :
		// sendAggregationDupInsensitiveData) {
		// for (DataObjectAggregationDupInsensitiveImpl actRecObject :
		// alreadyReceivedAggregationDupInsensitiveData) {
		// // if (actRecObject.ofSameType(actRecObject)) {
		// //
		// log.debug("SINK LEVEL - DataRouting_DupInsensitive: Aggregation
		// Function '"
		// // + actSendObject.getAggregationFunctionId() + "' with Attribute " +
		// actSendObject.getAttributeId()
		// // + " value GroundTruth " + actSendObject.getAggregateValue() +
		// " receivedValue "
		// // + actRecObject.getAggregateValue() + " are the same "
		// // +
		// (actRecObject.getAggregateValue().equals(actSendObject.getAggregateValue())));
		// // }
		// }
		// }
	}

	private void statisticsAtSinkLevel_AggregationDupSensitiveData() {
		deliveryRatioAtSink_dupSensData = (double) receivedCount_atSink_DupSensData / (double) sendCount_inNetwork_DupSensData;
		if (Double.isNaN(deliveryRatioAtSink_dupSensData))
			deliveryRatioAtSink_dupSensData = 0;

		// double deliveryRatio = ((double)
		// Math.round((deliveryRatioAtSink_dupSensData) * 10000)) / 100;

		duplicateRatioAtSinkWithoutResolving_dupSensData = (double) (duplicateCount_atSink_DubSensData
				+ duplicateCountDetectedAtSink_DupSensData)
				/ (double) (receivedCount_atSink_DupSensData + duplicateCount_atSink_DubSensData
						+ duplicateCountDetectedAtSink_DupSensData);
		if (Double.isNaN(duplicateRatioAtSinkWithoutResolving_dupSensData))
			duplicateRatioAtSinkWithoutResolving_dupSensData = 0;

		duplicateRatioAtSinkWithResolving_dupSensData = (double) duplicateCount_atSink_DubSensData
				/ (double) (receivedCount_atSink_DupSensData + duplicateCount_atSink_DubSensData);
		if (Double.isNaN(duplicateRatioAtSinkWithResolving_dupSensData))
			duplicateRatioAtSinkWithResolving_dupSensData = 0;

		// double duplicateRatioOfDelivered = ((double)
		// Math.round(duplicateRatioAtSinkWithResolving_dupSensData * 10000)) /
		// 100;

		// double duplicateRatioResolvedDuplicates = ((double) Math
		// .round(((double) duplicateCountDetectedAtSink_DupSensData / (double)
		// (duplicateCountDetectedAtSink_DupSensData +
		// duplicateCount_atSink_DubSensData)) * 10000)) / 100;
		//
		// log.debug("SINK LEVEL - DataRouting_DupSensitive: Duplicate clean
		// delivery ratio at sink nodes "
		// + deliveryRatio
		// + " % with duplicate ratio of " + duplicateRatioOfDelivered +
		// " % of overall received.");
		// log.debug("SINK LEVEL - DataRouting_DupSensitive: " +
		// duplicateRatioResolvedDuplicates
		// + " % of duplicates resolved at SINK LEVEL");
	}

	private void statisticsAtCentralLevel() {

		deliveryRatioAtCentral_UploadMsg = (double) receivedCount_uploadContainerMessage
				/ ((double) sendCount_sinkUploadContainerMessage + sendCount_cloudletUploadContainerMessage);

		// double deliveryRatio = ((double)
		// Math.round((deliveryRatioAtCentral_UploadMsg) * 10000)) / 100;

		duplicateRatioAtCentral_UploadMsg = (double) duplicateCount_uploadContainerMessage
				/ (double) (receivedCount_uploadContainerMessage + duplicateCount_uploadContainerMessage);

		// double dupRatio = ((double)
		// Math.round((duplicateRatioAtCentral_UploadMsg) * 10000)) / 100;

		// log.debug("CENTRAL LEVEL - DataRouting: DDR of ContainerUploadMessges
		// "
		// + deliveryRatio + " % with " + dupRatio
		// + " duplicates.");
	}

	private void statisticsAtCentralLevel_NormalData() {
		deliveryRatioAtCentral_normalData = (double) receivedCount_atCentral_NormalData
				/ (double) (sendCount_inNetwork_NormalData + sendCount_inUpload_NormalData);

		// double deliveryRatio = ((double)
		// Math.round((deliveryRatioAtCentral_normalData) * 10000)) / 100;

		duplicateRatioAtCentral_normalData = (double) duplicateCount_atCentral_NormalData
				/ (double) (receivedCount_atCentral_NormalData + duplicateCount_atCentral_NormalData);

		// double duplicateRatioOfDelivered = ((double)
		// Math.round((duplicateRatioAtCentral_normalData) * 10000)) / 100;

		// log.debug("CENTRAL LEVEL - DataRouting_NormalData: Delivery ratio of
		// 'normal' DataObjects at sink nodes "
		// + deliveryRatio
		// + " % with duplicate ratio of " + duplicateRatioOfDelivered +
		// " % of overall received.");
	}

	private void statisticsAtCentralLevel_AggregationDupInsensitiveData() {
		// Merge of send ground truth (in network + at sink)
		LinkedList<DataObjectAggregationDupInsensitiveImpl> sendGroundTruth = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();
		sendGroundTruth.addAll(sendAggregationDupInsensitiveData);

		for (DataObjectAggregationDupInsensitiveImpl actSendAtSink : sendAggregationDupInsensitiveDataAtSink) {
			boolean entryExisted = false;
			for (DataObjectAggregationDupInsensitiveImpl actDataObject : sendGroundTruth) {
				if (actDataObject.getAggregationFunctionId().equals(actSendAtSink.getAggregationFunctionId())
						&& actDataObject.getAttributeId() == actSendAtSink.getAttributeId()) {
					actSendAtSink.putValue(actDataObject.getAggregateValue());
					entryExisted = true;
				}
			}

			if (!entryExisted) {
				sendGroundTruth.add(actSendAtSink);
			}
		}

		LinkedList<DataObjectAggregationDupInsensitiveImpl> alreadyReceivedAggregationDupInsensitiveData = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();

		for (DataObjectAggregationDupInsensitiveImpl actReceivedObject : receivedAtCentral_AggregationDupInsensitiveData) {
			boolean entryExisted = false;
			for (DataObjectAggregationDupInsensitiveImpl actDataObject : alreadyReceivedAggregationDupInsensitiveData) {
				if (actReceivedObject.ofSameType(actDataObject)) {
					actDataObject.putValue(actReceivedObject.getAggregateValue());
					entryExisted = true;
				}
			}

			if (!entryExisted) {
				DataObjectAggregationDupInsensitiveImpl aggregationDupInsensitvieDataObejct;
				try {
					aggregationDupInsensitvieDataObejct = new DataObjectAggregationDupInsensitiveImpl(
							actReceivedObject.getAggregationFunctionId(), actReceivedObject.getAttributeId(),
							actReceivedObject.getAggregateValue());
					alreadyReceivedAggregationDupInsensitiveData.add(aggregationDupInsensitvieDataObejct);
				} catch (AggregationFunctionNotKnownException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void statisticsAtCentralLevel_AggregationDupSensitiveData() {

		deliveryRatioAtCentral_dupSensData = (double) receivedCount_atCentral_DupSensData
				/ (double) (sendCount_inNetwork_DupSensData + sendCount_inUpload_DupSensData);

		// double deliveryRatio = ((double)
		// Math.round((deliveryRatioAtCentral_dupSensData) * 10000)) / 100;

		duplicateRatioAtCentral_dupSensData = (double) duplicateCount_atCentral_DubSensData
				/ (double) (receivedCount_atCentral_DupSensData + duplicateCount_atCentral_DubSensData);

		// double duplicateRatioResolvedOfDelivered = ((double)
		// Math.round((duplicateRatioAtCentral_dupSensData) * 10000)) / 100;

	}

	private void statisticsForLatency() {
		double avgLatency_containerMessagesInNetwork = (double) latency_containerMessagesInNetwork
				/ (double) latencyCount_containerMessagesInNetwork;
		if (Double.isNaN(avgLatency_containerMessagesInNetwork))
			avgLatency_containerMessagesInNetwork = 0;

		double avgLatency_normalDataInNetwork = (double) latency_NormalDataObjectInNetwork
				/ (double) latencyCount_NormalDataObjectInNetwork;
		if (Double.isNaN(avgLatency_normalDataInNetwork))
			avgLatency_normalDataInNetwork = 0;

		double avgLatency_dupSensDataInNetwork = (double) latency_DupSensEntryInNetwork
				/ (double) latencyCount_DupSensEntryInNetwork;
		if (Double.isNaN(avgLatency_dupSensDataInNetwork))
			avgLatency_dupSensDataInNetwork = 0;

		double avgLatency_containerMessagesInUpload = (double) latency_containerMessagesInUpload
				/ (double) latencyCount_containerMessagesInUpload;

		double avgLatency_normalDataEndToEnd = (double) latency_NormalDataObjectEndToEnd
				/ (double) latencyCount_NormalDataObjectEndToEnd;
		if (Double.isNaN(avgLatency_normalDataEndToEnd))
			avgLatency_normalDataEndToEnd = 0;

		double avgLatency_dupSensDataEndToEnd = (double) latency_DupSensEntryEndToEnd
				/ (double) latencyCount_DupSensEntryEndToEnd;
		if (Double.isNaN(avgLatency_dupSensDataEndToEnd))
			avgLatency_dupSensDataEndToEnd = 0;

		if (ENABLE_DAO) {
			LatencyMeasurement latencyMeasurement = new LatencyMeasurement(avgLatency_containerMessagesInNetwork,
					avgLatency_normalDataInNetwork, avgLatency_dupSensDataInNetwork, avgLatency_containerMessagesInUpload,
					avgLatency_normalDataEndToEnd, avgLatency_dupSensDataEndToEnd);
			MeasurementDAO.storeCustomMeasurement(LatencyMeasurement.LATENCY_METRIC, cloud.getHost().getId().value(),
					Simulator.getCurrentTime(), latencyMeasurement);
		}

	}

	private void writeToDB() {

		DataRoutingMeasurement measurement = new DataRoutingMeasurement(deliveryRatioAtSink_ContainerMsg,
				duplicateRatioAtSink_ContainerMsg, averageSameContainerMsgRec, singleMaxSameContainerMsgRec,
				deliveryRatioAtSink_normalData, duplicateRatioAtSinkWithoutResolving_normalData,
				duplicateRatioAtSinkWithResolving_normalData, deliveryRatioAtSink_dupSensData,
				duplicateRatioAtSinkWithoutResolving_dupSensData, duplicateRatioAtSinkWithResolving_dupSensData,
				deliveryRatioAtCentral_UploadMsg, duplicateRatioAtCentral_UploadMsg, deliveryRatioAtCentral_normalData,
				duplicateRatioAtCentral_normalData, deliveryRatioAtCentral_dupSensData, duplicateRatioAtCentral_dupSensData);
		MeasurementDAO.storeCustomMeasurement(DataRoutingMeasurement.DATA_ROUTING_METRIC, cloud.getHost().getId().value(),
				Simulator.getCurrentTime(), measurement);
	}
}
