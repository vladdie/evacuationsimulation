package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue;
import de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval.MCompleteness.MCompletenessValue;

/**
 * Completeness measure within Crater.
 * 
 * Measured by checking the participation of nodes in the monitoring effort over
 * time. Interval-based metric.
 * 
 * 
 * @author Nils Richerzhagen
 *
 */
public class MCompleteness extends AbstractMetric<MCompletenessValue> implements Metric<MCompletenessValue> {

	public MCompleteness() {
		super("Crater: Accuracy Measurement (Participating Hosts in the monitoring effort)", MetricUnit.NONE);
	}

	protected class MCompletenessValue implements MetricValue<Double> {

		@Override
		public Double getValue() {
			return AccuracyAndCompletenessAnalyzerImpl.getInstance().getCompleteness();
		}

		@Override
		public boolean isValid() {
			return AccuracyAndCompletenessAnalyzerImpl.getInstance().isValid();
		}

	}

	@Override
	public void initialize(List<Host> hosts) {
		setOverallMetric(new MCompletenessValue());
	}

}
