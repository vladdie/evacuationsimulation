package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;

@Entity
@Table(name = "latency_measurements")
public class LatencyMeasurement extends CustomMeasurement {

	public static final MetricDescription LATENCY_METRIC = new MetricDescription(DataRoutingAnalyzerImpl.class,
			"LatencyMeasurement", "Includes the measured average packet or entry latencies of ContainerMsg, normal data, and dup sens data for orig to sink and orig to server.", "us");

    @SuppressWarnings("unused")
	private double avgLatency_containerMessagesInNetwork;
    @SuppressWarnings("unused")
	private double avgLatency_normalDataInNetwork;
    @SuppressWarnings("unused")
	private double avgLatency_dupSensDataInNetwork;

    @SuppressWarnings("unused")
	private double avgLatency_containerMessagesInUpload;
    @SuppressWarnings("unused")
	private double avgLatency_normalDataEndToEnd;
    @SuppressWarnings("unused")
	private double avgLatency_dupSensDataEndToEnd;

	public LatencyMeasurement(double avgLatency_containerMessagesInNetwork, double avgLatency_normalDataInNetwork, double avgLatency_dupSensDataInNetwork,
			double avgLatency_containerMessagesInUpload, double avgLatency_normalDataEndToEnd, double avgLatency_dupSensDataEndToEnd) {
		super();
		this.avgLatency_containerMessagesInNetwork = avgLatency_containerMessagesInNetwork;
		this.avgLatency_normalDataInNetwork = avgLatency_normalDataInNetwork;
		this.avgLatency_dupSensDataInNetwork = avgLatency_dupSensDataInNetwork;

		this.avgLatency_containerMessagesInUpload = avgLatency_containerMessagesInUpload;
		this.avgLatency_normalDataEndToEnd = avgLatency_normalDataEndToEnd;
		this.avgLatency_dupSensDataEndToEnd = avgLatency_dupSensDataEndToEnd;
	}
}
