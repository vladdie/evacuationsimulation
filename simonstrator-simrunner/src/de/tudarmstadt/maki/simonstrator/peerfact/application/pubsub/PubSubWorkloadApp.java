package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import de.tud.kom.p2psim.api.application.Application;
import de.tud.kom.p2psim.api.application.WorkloadGenerator;
import de.tud.kom.p2psim.api.application.WorkloadListener;
import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.impl.topology.PositionVector;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PubSubGlobalKnowledge;

/**
 * Pub/Sub workload-generator with evaluation capabilities. All settings are to
 * be set in the Factory, NOT by changing any of the booleans in this class and
 * thereby changing the default behavior for all users.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class PubSubWorkloadApp implements Application, WorkloadListener {

	private final SimHost host;

	protected final List<PubSubAppListener> pubSubListeners = new LinkedList<PubSubAppListener>();

	private PubSubComponent pubsub;

	protected static boolean ENABLE_EVAL = true;

	private final WorkloadGenerator workload;

	public PubSubWorkloadApp(SimHost host) {
		this.host = host;
		this.workload = null;
	}

	public PubSubWorkloadApp(SimHost host, WorkloadGenerator workload) {
		this.host = host;
		this.workload = workload;
	}

	@Override
	public SimHost getHost() {
		return host;
	}

	@Override
	public void initialize() {
		try {
			pubsub = host.getComponent(PubSubComponent.class);
			if (ENABLE_EVAL) {
				PubSubGlobalKnowledge.addPubSubComponent(pubsub);
			}
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("This app requires a pub/sub component!");
		}

		if (workload != null) {
			workload.addWorkloadListener(this);
		}

	}

	@Override
	public void shutdown() {
		// not used
	}

	public void addPubSubListener(PubSubAppListener listener) {
		this.pubSubListeners.add(listener);
	}

	public void removePubSubListener(PubSubAppListener listener) {
		this.pubSubListeners.remove(listener);
	}

	/**
	 * Use this method to dispatch Notifications to the overlay. Otherwise,
	 * Evaluation results might be wrong...
	 * 
	 * @param n
	 */
	public void onPublish(Notification n) {
		// Notify Listeners
		for (PubSubAppListener listener : pubSubListeners) {
			listener.onNewPublication(n);
		}
	}

	/**
	 * Call, when dispatching subscriptions to the overlay. Wraps the listener
	 * <code>l</code> into another listener which is returned by this method.
	 * 
	 * @param s
	 * @param l
	 *            listener, can be null (inner)
	 * 
	 * @return listener to be used the the overlay (wraps provided listener
	 *         <code>l</code>
	 */
	public PubSubListener onSubscribe(Subscription s, final PubSubListener l) {
		// Notify Listeners
		for (PubSubAppListener listener : pubSubListeners) {
			listener.onNewSubscription(s);
		}
		/*
		 * Trigger subscription process in the overlay, and encapsulate the
		 * given listener for analyzing.
		 */
		PubSubListener subListener = new PubSubListener() {
			@Override
			public void onNotificationArrived(Subscription matchedSubscription,
					Notification notification) {
				// Notify Listeners
				for (PubSubAppListener listener : pubSubListeners) {
					listener.onReceivedNotification(matchedSubscription,
							notification);
				}
				if (l != null) {
					l.onNotificationArrived(matchedSubscription, notification);
				}
			}
		};
		return subListener;
	}

	/**
	 * Dispatcher for the unsubscribe-operation. Use this method to ensure
	 * correct updates of the global knowledge!
	 * 
	 * @param sub
	 */
	public void onUnsubscribe(Subscription sub) {
		// Notify Listeners
		for (PubSubAppListener listener : pubSubListeners) {
			listener.onUnsubscribe(sub);
		}
	}

	@Override
	public PubSubWorkloadApp getApplication() {
		return this;
	}

	/**
	 * The Pub/Sub-Component
	 * 
	 * @return
	 */
	public PubSubComponent getPubsub() {
		return pubsub;
	}

	/**
	 * Currently running workload
	 * 
	 * @return
	 */
	public WorkloadGenerator getWorkload() {
		return workload;
	}

	/*
	 * ======= The following code aids in the visualization process
	 */

	private LocationSensor locSensor = null;

	public PositionVector getLocation() {
		if (locSensor == null) {
			try {
				locSensor = getHost().getComponent(LocationSensor.class);
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError(
						"Locations are required for this method.");
			}
		}
		return (PositionVector) locSensor.getLastLocation();
	}

	public Point getLocationAsPoint() {
		Location loc = getLocation();
		return new Point((int) loc.getLongitude(), (int) loc.getLatitude());
	}

}
