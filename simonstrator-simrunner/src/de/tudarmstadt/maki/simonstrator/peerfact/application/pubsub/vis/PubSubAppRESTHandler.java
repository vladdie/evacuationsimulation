package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.vis;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseFeature;

import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubWorkloadAppFactory;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.vis.PubSubAppVisSidebar.DemoClientController;

/**
 * Access to pub/sub-app and the simulator via REST
 * 
 * @author Bjoern Richerzhagen
 *
 */
@Path("pubsub")
public class PubSubAppRESTHandler {

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to
	 * the client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getHelp() {
		if (!Oracle.isSimulation()) {
			return "This is not a simulation.";
		}

		return "Currently simulating " + Oracle.getAllHosts().size() + " hosts.";
	}

	@GET
	@Path("/client/{clientId}/info")
	@Produces(MediaType.TEXT_PLAIN)
	public String getClientInfo(@PathParam("clientId") int clientId) {
		DemoClientController client = PubSubAppVisSidebar.getInstance().getClientController(clientId);
		if (client == null) {
			return "404 for " + clientId;
		}
		return client.toString();
	}

	@GET
	@Path("/client/{clientId}/waypoint/{waypoint}")
	public Response setTargetWaypoint(@PathParam("clientId") int clientId, @PathParam("waypoint") String waypoint) {
		DemoClientController client = PubSubAppVisSidebar.getInstance().getClientController(clientId);
		if (client == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		client.setTargetWaypoint(waypoint);
		return Response.status(Status.OK).build();
	}

	@GET
	@Path("/client/{clientId}/gather")
	public Response setTargetWaypoint(@PathParam("clientId") int clientId) {
		DemoClientController client = PubSubAppVisSidebar.getInstance().getClientController(clientId);
		if (client == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		PubSubAppVisSidebar.getInstance().gatherClientsAt(client.currentAttractionPoint);
		return Response.status(Status.OK).build();
	}

	// @GET
	// @Path("/client/gather/{waypoint}")
	// public Response setGatherAtWaypoint(@PathParam("waypoint") String
	// waypoint) {
	// PubSubAppVisSidebar.getInstance().gatherClientsAt(waypoint);
	// return Response.status(Status.OK).build();
	// }

	@GET
	@Path("/client/{clientId}/roi/{roi}")
	public Response setRadiusOfInterest(@PathParam("clientId") int clientId, @PathParam("roi") int roi) {
		DemoClientController client = PubSubAppVisSidebar.getInstance().getClientController(clientId);
		if (client == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		client.updateRadiusOfInterest(roi);
		return Response.status(Status.OK).build();
	}

	/**
	 * Keep track of attached clients.
	 * 
	 * @return
	 */
	@GET
	@Path("/client/bind")
	@Produces(SseFeature.SERVER_SENT_EVENTS)
	public EventOutput getClient() {
		if (!PubSubWorkloadAppFactory.SIMULATION_STARTED) {
			System.err.println("Simulation not started yet.");
			return null;
		}
		final EventOutput eventOutput = new EventOutput();
		new Thread(new Runnable() {
			@Override
			public void run() {
				DemoClientController clientCtrl = PubSubAppVisSidebar.getInstance().createClientController();
				if (clientCtrl == null) {
					System.err.println("Client null");
					return;
				}
				try {
					while (true) {
						clientCtrl.update();
						final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
						eventBuilder.mediaType(MediaType.APPLICATION_JSON_TYPE);
						eventBuilder.data(DemoClientController.class, clientCtrl);
						final OutboundEvent event = eventBuilder.build();
						eventOutput.write(event);
						try {
							Thread.sleep(250);
						} catch (InterruptedException e) {
							//
						}
					}
				} catch (IOException e) {
					/*
					 * Client terminated the session!
					 */
					System.err.println("Client Disconnect! " + e.getMessage());
					e.printStackTrace(System.err);
					// Fail silently!
				} catch (WebApplicationException e) {
					// Fail silently
				} finally {
					PubSubAppVisSidebar.getInstance().destroyClientController(clientCtrl);
					try {
						eventOutput.close();
					} catch (IOException ioClose) {
						throw new RuntimeException("Error when closing the event output.", ioClose);
					}
				}
			}
		}).start();
		return eventOutput;
	}

}
