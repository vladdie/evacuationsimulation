package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.nodeselection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * Determines initially random order of nodes ID's and tries to select the same
 * ones in the same order on consecutive calls. A node is skipped if it was
 * offline, to follow the distribution.
 * 
 * @author ConstiZ
 *
 */
public class InitialRandomNodeSelection extends AbstractNodeSelection {
	Random rnd;

	List<INodeID> initialSelection;

	public InitialRandomNodeSelection() {
		super();
		rnd = new Random();

		initialSelection = new ArrayList<INodeID>(allNodeIDs);
		Collections.shuffle(initialSelection, rnd);
		System.out.println(initialSelection.toString());
	}

	@Override
	public List<INodeID> selectNodes(int nodeCount, List<INodeID> allNodeIDs)
			throws IllegalSelectionException {
		validateSelection(nodeCount, allNodeIDs);

		List<INodeID> selection = new ArrayList<INodeID>();

		// Select nodes from initialSelection, if they are in allNodes
		for (Iterator<INodeID> iterator = initialSelection.iterator(); iterator
				.hasNext();) {
			INodeID id = iterator.next();
			if (allNodeIDs.contains(id)) {
				selection.add(id);
			}
		}

		if (selection.size() < nodeCount) {
			System.err.println("Initial selection too short!");
		}

		selection.subList(nodeCount, selection.size()).clear();
		return selection;
	}

}
