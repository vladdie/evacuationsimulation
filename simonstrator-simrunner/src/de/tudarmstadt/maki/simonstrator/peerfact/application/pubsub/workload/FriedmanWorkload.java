/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubWorkloadApp;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.FriedmanWorkload.TopicState;

/**
 * This workload generator creates workloads as described in Friedman, R., &
 * Kaplun Shulman, A. (2012). A density-driven publish subscribe service for
 * mobile ad-hoc networks. Ad Hoc Networks, 11(1).
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Apr 11, 2014
 */
public class FriedmanWorkload extends
		PubSubWorkloadGenerator<TopicState> implements EventHandler {

	private final static int EVENT_SUBSCRIBE = 2;

	private final static int EVENT_PUBLISH = 3;

	private final static String TOPIC = "/pubsub/workload/";

	private long lastScheduledPublication = 0;

	private double percentageSubscribers = 20.0;

	private double percentagePublishers = 100.0;

	private double percentageSubscriptionsMatchingSinglePub = 14.3;

	private int numberOfPublishers;

	private List<Topic> topics = new LinkedList<Topic>();

	// Just a very simple container
	public static class TopicState extends NodeState {

		public TopicState(PubSubWorkloadApp app) {
			super(app);
		}

		public Topic subTopic = null;

		public Topic pubTopic = null;
	}

	/**
	 * Delay between two consecutive publications of ONE publisher
	 * 
	 * @return
	 */
	private long getDelayBetweenPublications() {
		// needs to be dynamic
		return (long) (Time.SECOND * (numberOfPublishers / publicationsPerSecond
				.getCurrentValue()));
	}

	@Override
	protected void initialize() {
		assert percentagePublishers <= 100 && percentagePublishers >= 0;
		assert percentageSubscriptionsMatchingSinglePub > 0
				&& percentageSubscriptionsMatchingSinglePub <= 100;
		assert percentageSubscribers <= 100 && percentageSubscribers >= 0;

		// Assign Publishers and Subscribers
		int nodesTotal = states.size();
		numberOfPublishers = (int) Math.ceil(nodesTotal
				* (percentagePublishers / 100d));
		assert numberOfPublishers <= nodesTotal;
		int numberSubscribers = (int) Math.ceil(nodesTotal
				* (percentageSubscribers / 100d));
		assert numberSubscribers <= nodesTotal;

		// int numberOfSubsPerPub = (int) Math.ceil(numberSubscribers *
		// subscriptionsPerSubscriber
		// * (percentageSubscriptionsMatchingSinglePub / 100d));

		int numTopics = (int) Math
				.ceil(100d / percentageSubscriptionsMatchingSinglePub);
		int subscribersPerTopic = (int) Math.ceil(numberSubscribers
				/ (double) numTopics);
		for (int topicNum = 1; topicNum <= numTopics; topicNum++) {
			topics.add(states.get(0).getApp().getPubsub()
					.createTopic(TOPIC + topicNum));
		}

		List<TopicState> statesToShuffle = new LinkedList<TopicState>(states);

		Collections.shuffle(statesToShuffle, rnd);
		List<TopicState> subscribers = statesToShuffle.subList(0,
				numberSubscribers);
		int subCt = 0;
		int currTopic = 0;
		for (TopicState nodeState : subscribers) {
			nodeState.enableSubscriptions = true;
			assert currTopic < topics.size();
			assignTopic(nodeState, topics.get(currTopic), true);
			subCt++;
			if (subCt % subscribersPerTopic == 0) {
				currTopic++;
			}
		}

		Collections.shuffle(statesToShuffle, rnd);
		List<TopicState> publishers = statesToShuffle.subList(0,
				numberOfPublishers);
		for (TopicState nodeState : publishers) {
			int topicIdx = rnd.nextInt(topics.size());
			nodeState.enablePublications = true;
			assignTopic(nodeState, topics.get(topicIdx), false);
		}

	}

	private void assignTopic(TopicState state, Topic t, boolean subscription) {
		if (subscription) {
			assert state.enableSubscriptions;
			state.subTopic = t;
		} else {
			assert state.enablePublications;
			state.pubTopic = t;
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		TopicState state = (TopicState) content;
		switch (type) {
		case EVENT_PUBLISH:
			Notification n = state
					.getApp()
					.getPubsub()
					.createNotification(state.pubTopic, null);
			n._getNotificationInfo(new NotificationInfoImpl(
					(long) publicationSize.getCurrentValue(), state.app
							.getHost().getHostId()));
			state.doPublish(n);
			long delay = getDelayBetweenPublications();
			lastScheduledPublication = Time.getCurrentTime() + delay;
			Event.scheduleWithDelay(delay, this, state, EVENT_PUBLISH);
			break;

		case EVENT_SUBSCRIBE:
			Subscription s = state
					.getApp()
					.getPubsub()
					.createSubscription(state.subTopic);
			s._getSubscriptionInfo(new SubscriptionInfoImpl(state.getApp().getHost().getId()));
			state.doSubscribe(s, new PubSubListener() {
				@Override
				public void onNotificationArrived(
						Subscription matchedSubscription,
						Notification notification) {
					// not interested
				}
			});
			break;

		default:
			break;
		}
	}

	@Override
	protected void onPeerStatusChanged(TopicState state, PeerStatus newStatus) {
		if (newStatus == PeerStatus.PRESENT) {
			// Subscribers subscribe
			if (state.enableSubscriptions) {
				Event.scheduleWithDelay(2 * Time.MINUTE, this, state,
						EVENT_SUBSCRIBE);
			}
			if (state.enablePublications) {
				/*
				 * Publishers perish ;) - ensure the limit (pub/s)
				 */
				long delay = getDelayBetweenPublications();
				if (lastScheduledPublication > Time.getCurrentTime()) {
					assert Time.getCurrentTime() - lastScheduledPublication <= delay;
					delay = lastScheduledPublication - Time.getCurrentTime()
							+ delay / numberOfPublishers;
				}
				Event.scheduleWithDelay(delay, this, state, EVENT_PUBLISH);
				lastScheduledPublication = Time.getCurrentTime() + delay;
			}
		}
	}

	@Override
	protected TopicState createNodeStateObject(PubSubWorkloadApp app) {
		return new TopicState(app);
	}

}