package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.io.Writer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeMap;

import de.tud.kom.p2psim.impl.simengine.Simulator;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.RequestResponseAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.ResponseObject;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.ResponseEntry;

public class RequestResponseAnalyzerImpl implements RequestResponseAnalyzer,
		IPeerStatusListener, EventHandler {

	private final int cleanup = 4512;

	private boolean ENABLE_DAO = false;

	// Debug setting
	private boolean logout = true;
	private boolean printout = false;

	private boolean doAnalyze = false;

	/**
	 * List of all mobile crater nodes.
	 */
	private LinkedList<INodeID> allCraterMobileNodes;

	private LinkedList<INodeID> currentOnlineNodes;
	private LinkedList<INodeID> currentOfflineNodes;

	private INodeID cloudID;
	//////////////////////////////////////////////////
	// Request stuff

	/**
	 * Stores information about a request at creation time, mapped to the
	 * request hash
	 */
	private HashMap<Integer, requestCreatedInfo> infosAtRequestCreation = new HashMap<Integer, requestCreatedInfo>();

	/**
	 * Stores the list of nodes that should receive a request
	 */
	private HashMap<Integer, LinkedList<INodeID>> expectedReceiversForRequest = new HashMap<Integer, LinkedList<INodeID>>();

	/**
	 * Stores information about when a request was received by a node. <br>
	 * Uses a requestReceivedInfo class instead of a map, as the same Request
	 * may be received multiple times by the same node, IF that node went
	 * offline in between -due to churn.
	 */
	private HashMap<Integer, LinkedList<requestReceivedInfo>> perRequestReceivedInfo = new HashMap<Integer, LinkedList<requestReceivedInfo>>();

	/**
	 * Stores information where requests where received
	 */
	private HashMap<Integer, LinkedList<INodeID>> perRequestReachedNodes = new HashMap<Integer, LinkedList<INodeID>>();
	
	/**
	 * Includes the hashes of still valid Requests, mapped to their validity
	 * time. Requests that are no longer valid will be removed from this map.
	 */
	private TreeMap<Long, HashSet<Integer>> validRequests = new TreeMap<Long, HashSet<Integer>>();

	/////////////////////////////////////////////////////////////////
	// Response stuff
	
	/**
	 * Stores per request and responder a list of timestamps, for when responses
	 * were generated. <br>
	 * It is a list of timestamps, because of possible multiple answers for
	 * periodic requests
	 */
	HashMap<Integer, HashMap<INodeID, LinkedList<Long>>> responseCreationTimestampsPerRequest = new HashMap<Integer, HashMap<INodeID, LinkedList<Long>>>();

	/**
	 * For the Receive responses, per Request: a Map of NodeIDs with a map of
	 * responses send and the time it was received first.
	 */
	HashMap<Integer, HashMap<INodeID, HashMap<Long, Long>>> responseReceivedTimestampsPerRequest = new HashMap<Integer, HashMap<INodeID, HashMap<Long, Long>>>();

	/**
	 * For responses received at the central Entity. This maps a list of entry
	 * creationTimes (asUID) to the creation Node and the request.
	 */
	HashMap<Integer, HashMap<INodeID, HashSet<Long>>> responsesReceivedAtCentralEntity = new HashMap<Integer, HashMap<INodeID, HashSet<Long>>>();

	HashMap<Integer, Integer> duplicatesPerRequest = new HashMap<Integer, Integer>();

	public RequestResponseAnalyzerImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void start() {

		doAnalyze = printout || ENABLE_DAO || logout;

		if (!doAnalyze) {
			return;
		}

		allCraterMobileNodes = new LinkedList<INodeID>();
		for (Host host : Oracle.getAllHosts()) {
			try {
				CraterComponent craterNode = host.getComponent(CraterComponent.class);

				if (craterNode instanceof CraterNodeComponent) {
					// This is a crater node, so add its ID to the list of valid
					// mobile nodes.
					allCraterMobileNodes.add(host.getId());

					craterNode.addPeerStatusListener(this);
				} else if (craterNode instanceof CraterCentralComponent) {
					cloudID = host.getId();
				}

			} catch (Exception e) {
				// Not a crater node, do nothing
			}
		}

		currentOnlineNodes = new LinkedList<INodeID>();
		currentOfflineNodes = new LinkedList<INodeID>(allCraterMobileNodes);
	}

	@Override
	public void stop(Writer out) {
		if (!doAnalyze) {
			return;
		}
		Iterator<Long> timeline = validRequests.keySet().iterator();
		while (timeline.hasNext()) {
			long time = timeline.next();
			HashSet<Integer> requests = validRequests.get(time);
			for (int request : requests) {
				generateRequestStats(request);
			}
			timeline.remove();
		}
	}

	public void setEnableDao(boolean enableDao) {
		this.ENABLE_DAO = enableDao;
	}


	@Override
	public void onRequestInvoked(Request request) {
		if (!doAnalyze) {
			return;
		}

		int requestHash = request.hashCode();

		requestCreatedInfo info = new requestCreatedInfo();
		info.requester = request.getSource();
		info.creationTime = Time.getCurrentTime();
		info.validity = request.getValidity();
		info.type = request.getType();
		info.expectedReceivers = new LinkedList<INodeID>(currentOnlineNodes);
		info.intendedReceivers = new LinkedList<INodeID>();

		if (!Arrays.asList(request.getReceiver()).contains("ALL")) {
			// TODO is this still correct?
			for (String actReceiver : request.getReceiver()) {
				info.intendedReceivers.add(INodeID.get(actReceiver));
			}
		}

		int expectedAnswers = 1;
		if (request.getType().equals("PERIODIC")) {
			expectedAnswers = (int) ((request.getValidity() - Time.getCurrentTime()) / request.getInterval());
		}
		info.expectedNumberOfAnswersPerNode = expectedAnswers;

		if (infosAtRequestCreation.containsKey(requestHash)) {
			System.err.println("The same Request was generated multiple times!");

			return;
		}
		infosAtRequestCreation.put(requestHash, info);

		expectedReceiversForRequest.put(requestHash, new LinkedList<INodeID>(currentOnlineNodes));

		HashSet<Integer> requestsForTime;
		if (validRequests.containsKey(request.getValidity())) {
			requestsForTime = validRequests.get(request.getValidity());
		} else {
			requestsForTime = new HashSet<Integer>();
			validRequests.put(request.getValidity(), requestsForTime);
		}
		requestsForTime.add(requestHash);

		// Prepare the response maps to prevent later nullPointerExceptions,
		// incase no response was generated
		responseCreationTimestampsPerRequest.put(requestHash, new HashMap<INodeID, LinkedList<Long>>());
		responseReceivedTimestampsPerRequest.put(requestHash, new HashMap<INodeID, HashMap<Long, Long>>());
		responsesReceivedAtCentralEntity.put(requestHash, new HashMap<INodeID, HashSet<Long>>());
		duplicatesPerRequest.put(requestHash, 0);

		// Remove the request from valid requests once it is timed out!
		long timeout = (request.getValidity() - Time.getCurrentTime()) + 1L;
		Event.scheduleWithDelay(timeout, this, null, cleanup);
	}

	@Override
	public void onRequestMessageReceived(Request request, INodeID receiver) {
		if (!doAnalyze) {
			return;
		}

		if (Time.getCurrentTime() > request.getValidity()) {
			// This shouldn't really happen
			return;
		}

		int requestHash = request.hashCode();
		
		requestReceivedInfo info = new requestReceivedInfo();
		info.nodeID = receiver;
		info.receivedAt = Time.getCurrentTime();

		LinkedList<requestReceivedInfo> infoList;
		if (perRequestReceivedInfo.containsKey(requestHash)) {
			infoList = perRequestReceivedInfo.get(requestHash);
		} else {
			infoList = new LinkedList<requestReceivedInfo>();
			perRequestReceivedInfo.put(requestHash, infoList);
		}
		infoList.add(info);

		
		LinkedList<INodeID> receiverList;
		if(perRequestReachedNodes.containsKey(requestHash)){
			receiverList = perRequestReachedNodes.get(requestHash);
		} else {
			receiverList = new LinkedList<INodeID>();
			perRequestReachedNodes.put(requestHash, receiverList);
		}
		receiverList.add(receiver);
	}

	@Override
	public void onResponseGeneratedForRequest(ResponseObject response, INodeID responder) {
		if (!doAnalyze) {
			return;
		}

		int requestHash = response.getRequestHash();
		LinkedList<Long> list;
		if (responseCreationTimestampsPerRequest.containsKey(requestHash)) {
			HashMap<INodeID, LinkedList<Long>> map = responseCreationTimestampsPerRequest
					.get(requestHash);
			if (map.containsKey(responder)) {
				list = map.get(responder);
			} else {
				list = new LinkedList<Long>();
				map.put(responder, list);
			}
		} else {
			HashMap<INodeID, LinkedList<Long>> map = new HashMap<INodeID, LinkedList<Long>>();
			list = new LinkedList<Long>();
			map.put(responder, list);
			responseCreationTimestampsPerRequest.put(requestHash, map);
		}
		list.add(Time.getCurrentTime());
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onResponseReceivedAtRequester(ResponseObject response) {
		if (!doAnalyze) {
			return;
		}
		long currentTime = Time.getCurrentTime();
		if (currentTime > response.getValidity()) {
			// should only happen very very rarely.
			// We are only interested in Responses that arrived in time, are we?
			return;
		}
		int requestHash = response.getRequestHash();

		// get the map
		HashMap<INodeID, HashMap<Long, Long>> mapForNodes;
		if (responseReceivedTimestampsPerRequest.containsKey(requestHash)) {
			mapForNodes = responseReceivedTimestampsPerRequest.get(requestHash);
		} else {
			mapForNodes = new HashMap<INodeID, HashMap<Long, Long>>();
			responseReceivedTimestampsPerRequest.put(requestHash,
					mapForNodes);
		}

		// get all the responders timestamps
		// This is awfully inefficient...
		for (Set<ResponseEntry> set : response.getAllResponseValues().values()) {
			// There is currently only one set!
			for (ResponseEntry entry : set) {
				INodeID nodeID = entry.getIdentifier();
				
				HashMap<Long, Long> timeForTime;
				if(mapForNodes.containsKey(nodeID)){
					timeForTime = mapForNodes.get(nodeID);
					
					if(timeForTime.containsKey(entry.getCreationTime())){
						// was already present. Duplicate!
						int duplicates = duplicatesPerRequest.get(requestHash);
						duplicatesPerRequest.put(requestHash,
								duplicates + 1);
					} else {
						timeForTime.put(entry.getCreationTime(), currentTime);
					}
					
				} else{
					timeForTime = new HashMap<Long, Long>();
					timeForTime.put(entry.getCreationTime(), currentTime);
					mapForNodes.put(nodeID, timeForTime);
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onResponseReceivedAtCentralEnetity(ResponseObject response) {
		if (!doAnalyze) {
			return;
		}
		int requestHash = response.getRequestHash();
		for (Set<ResponseEntry> set : response.getAllResponseValues().values()) {
			for (ResponseEntry entry : set) {
				INodeID nodeID = entry.getIdentifier();
				long creationTime = entry.getCreationTime();

				HashMap<INodeID, HashSet<Long>> nodeEntries = responsesReceivedAtCentralEntity.get(requestHash);
				HashSet<Long> sentTimes;
				if (nodeEntries.containsKey(nodeID)) {
					sentTimes = nodeEntries.get(nodeID);
				} else {
					sentTimes = new HashSet<Long>();
					nodeEntries.put(nodeID, sentTimes);
				}
				sentTimes.add(creationTime);
			}
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		if (type == cleanup) {
			removeInvalidRequests();
		}

	}

	private void removeInvalidRequests() {
		Iterator<Long> validityIterator = validRequests.navigableKeySet().iterator();
		long currentTime = Time.getCurrentTime();
		while (validityIterator.hasNext()) {
			long validTime = validityIterator.next();
			if (currentTime > validTime) {

				for (int req : validRequests.get(validTime)) {
					generateRequestStats(req);
				}

				validityIterator.remove();
			} else {
				// this was a sorted set, so we already passed all invalid
				// entries
				return;
			}
		}
	}

	private void generateRequestStats(int requestHash) {

		if (!infosAtRequestCreation.containsKey(requestHash)) {
			System.err.println("Can't find Request #" + requestHash);
		}

		// /////////////////////////////////////////////////////////////////////////////
		// At fist a cleanup, remove the requester from all lists that it snuck
		// into
		// /////////////////////////////////////////////////////////////////////////////
		INodeID requester = infosAtRequestCreation.get(requestHash).requester;

		infosAtRequestCreation.get(requestHash).expectedReceivers
				.remove(requester);

		Iterator<INodeID> removeRequester = expectedReceiversForRequest.get(
				requestHash).iterator();
		while (removeRequester.hasNext()) {
			INodeID next = removeRequester.next();
			if (next.equals(requester)) {
				removeRequester.remove();
			}
		}

		/////////////////////////////////////////////////////
		// Request Values!
		/////////////////////////////////////////////////////

		// Important
		boolean periodic = infosAtRequestCreation.get(requestHash).type.equals("PERIODIC");
		long requestCreationTime = infosAtRequestCreation.get(requestHash).creationTime;
		long validTime = infosAtRequestCreation.get(requestHash).validity;

		// needed Later
		HashSet<INodeID> nodesAtCreation = new HashSet<INodeID>(
				infosAtRequestCreation.get(requestHash).expectedReceivers);
		int updatesPerNode = infosAtRequestCreation.get(requestHash).expectedNumberOfAnswersPerNode;

		int numOfNodesAtCreation = nodesAtCreation.size();

		LinkedList<INodeID> allExpectedReceivers = expectedReceiversForRequest.get(requestHash);
		int numOfAllExpectedReceivers = allExpectedReceivers.size();

		// Update: Now logs number of Request's online 'targets' (NodeIDs of
		// reqeust.getReciver)
		HashSet<INodeID> initialyIntendedReceivers = new HashSet<INodeID>(
				infosAtRequestCreation.get(requestHash).expectedReceivers);
		LinkedList<INodeID> allIntendedReceivers = new LinkedList<INodeID>(allExpectedReceivers);

		if (!infosAtRequestCreation.get(requestHash).intendedReceivers.isEmpty()) {
			initialyIntendedReceivers.retainAll(infosAtRequestCreation.get(requestHash).intendedReceivers);
			allIntendedReceivers.retainAll(infosAtRequestCreation.get(requestHash).intendedReceivers);
		}
		int numOfInitiallyIntendedReceivers = initialyIntendedReceivers.size();
		int numOfAllIntendedReceivers = allIntendedReceivers.size();

		// Retain all nodes that have been reached, remove ones that were
		// not.
		HashSet<INodeID> initialNodesReached = new HashSet<INodeID>(nodesAtCreation);
		LinkedList<INodeID> allNodesReached = new LinkedList<INodeID>(allExpectedReceivers);
		if (perRequestReachedNodes.containsKey(requestHash)) {
			initialNodesReached.retainAll(perRequestReachedNodes.get(requestHash));
			allNodesReached.retainAll(perRequestReachedNodes
					.get(requestHash));

			initialyIntendedReceivers.retainAll(perRequestReachedNodes.get(requestHash));
			allIntendedReceivers.retainAll(perRequestReachedNodes.get(requestHash));
		} else {
			initialNodesReached.clear();
			allNodesReached.clear();
			initialyIntendedReceivers.clear();
			allIntendedReceivers.clear();
		}

		// needed later
		int numOfInitialRequestReceivers = 0;
		// important!
		long meanTimeToRequestReceivedInitialNodes = 0L;
		long meanTimeToRequestReceivedAllNodes = 0L;
		if (perRequestReceivedInfo.containsKey(requestHash)) {
			for (requestReceivedInfo info : perRequestReceivedInfo.get(requestHash)) {

				long time = info.receivedAt - requestCreationTime;

				meanTimeToRequestReceivedAllNodes += time;

				if (nodesAtCreation.contains(info.nodeID)) {
					numOfInitialRequestReceivers++;
					meanTimeToRequestReceivedInitialNodes += time;
				}
			}
			meanTimeToRequestReceivedAllNodes = meanTimeToRequestReceivedAllNodes / perRequestReceivedInfo.get(requestHash).size();

			if (numOfInitialRequestReceivers > 0) {
				meanTimeToRequestReceivedInitialNodes = meanTimeToRequestReceivedInitialNodes
						/ numOfInitialRequestReceivers;
			}
		}

		////////////////////////////////////
		// response Values
		////////////////////////////////////
		HashMap<INodeID, LinkedList<Long>> responseSenders = responseCreationTimestampsPerRequest.get(requestHash);
		HashMap<INodeID, HashMap<Long, Long>> responsesReceived = responseReceivedTimestampsPerRequest
				.get(requestHash);
		HashMap<INodeID, HashSet<Long>> responsesAtCloud = responsesReceivedAtCentralEntity.get(requestHash);

		int numOfSentResponses = 0;
		int numOfReceivedResponses = 0;
		int numOfResponsesAtCloud = 0;

		// These are averaged over the average message travelTime per Node!
		// Important!
		long meanResponseTravelTimePerNode = 0L;

		int numOfFirstResponders = 0;
		long meanTimeRequestToResponseAllNodes = 0L;

		int numOfInitialResponders = 0;
		long meanTimeRequestToResponseInitialNodes = 0L;
		
		HashSet<INodeID> initialResponders = new HashSet<INodeID>(responsesReceived.keySet());
		initialResponders.retainAll(infosAtRequestCreation.get(requestHash).expectedReceivers);


		// iterate over every sender
		for (INodeID responder : responseSenders.keySet()) {
			LinkedList<Long> senderList = responseSenders.get(responder);
			numOfSentResponses += senderList.size();

			// Received at requester
			if (responsesReceived.containsKey(responder)) {
				HashMap<Long, Long> receiverMap = responsesReceived
						.get(responder);
				numOfReceivedResponses += receiverMap.size();

				meanResponseTravelTimePerNode += getMeanTravelTime(receiverMap);

				if (senderList.size() > 0) {
					if (receiverMap.containsKey(senderList.get(0))) {

						long responseArrivalTime = receiverMap.get(senderList
								.get(0));
						long time = responseArrivalTime - requestCreationTime;
						meanTimeRequestToResponseAllNodes += time;
						numOfFirstResponders++;

						if (initialResponders.contains(responder)) {
							numOfInitialResponders++;
							meanTimeRequestToResponseInitialNodes += time;
						}
					}
				}

			}
			// Received at Central
			if (responsesAtCloud.containsKey(responder)) {
				numOfResponsesAtCloud += responsesAtCloud.get(responder).size();
			} else {
				// no responses of this node made it through...
			}
		}
		
		// Now scale all the traveltimes with nodeNumbers!
		if (responsesReceived.size() > 0) {
			meanResponseTravelTimePerNode = meanResponseTravelTimePerNode / responsesReceived.size();
		}

		if (numOfFirstResponders > 0) {
			meanTimeRequestToResponseAllNodes = meanTimeRequestToResponseAllNodes
					/ numOfFirstResponders;
		}

		if (numOfInitialResponders > 0) {
			meanTimeRequestToResponseInitialNodes = meanTimeRequestToResponseInitialNodes / numOfInitialResponders;
		}

		int numOfDuplicates = duplicatesPerRequest.get(requestHash);

		if (ENABLE_DAO) {
			// generate metric
			RequestResponseMeasurement measurement = new RequestResponseMeasurement(requestHash, periodic,
 requestCreationTime, validTime,
					initialNodesReached.size(), numOfNodesAtCreation, initialyIntendedReceivers.size(),
					numOfInitiallyIntendedReceivers, allNodesReached.size(), numOfAllExpectedReceivers,
					allIntendedReceivers.size(), numOfAllIntendedReceivers,
					meanTimeToRequestReceivedInitialNodes, meanTimeToRequestReceivedAllNodes,
 numOfReceivedResponses,
					numOfResponsesAtCloud, numOfSentResponses,
					meanResponseTravelTimePerNode,
					meanTimeRequestToResponseInitialNodes, meanTimeRequestToResponseAllNodes);

			// Store the measuremen
			MeasurementDAO.storeCustomMeasurement(RequestResponseMeasurement.REQUEST_RESPONSE_METRIC, cloudID.value(),
					Simulator.getCurrentTime(), measurement);
		}

		if (logout || printout) {

			String statement1 = "Request " + requestHash + ": Reached " + initialNodesReached.size() + " of initial "
					+ numOfNodesAtCreation + " Nodes. In an average of "
					+ Time.getFormattedTime(meanTimeToRequestReceivedInitialNodes);

			String statement2 = "Request " + requestHash + ": This includes " + initialyIntendedReceivers.size()
					+ " inteded receivers out of " + numOfInitiallyIntendedReceivers
					+ " initialy online RequestTargets";

			String statement3 = "Request " + requestHash + ": Reached "
					+ allNodesReached.size() + " of all " + numOfAllExpectedReceivers
					+ " Nodes. In an average of "
					+ Time.getFormattedTime(meanTimeToRequestReceivedAllNodes);

			String statement4 = "Request " + requestHash + ": This includes " + allIntendedReceivers.size()
					+ " inteded receivers out of " + numOfAllIntendedReceivers + " all RequestTargets";

			String statement5 = "Request " + requestHash
					+ ": Requester was reached by " + numOfReceivedResponses
					+ " of all " + numOfSentResponses + " Responses.";

			String statement6 = "Request " + requestHash
					+ ": Cloud was reached by " + numOfResponsesAtCloud
					+ " of all " + numOfSentResponses + " Responses.";

			String statement7 = "Request " + requestHash + ": Average Response travelTime was "
					+ Time.getFormattedTime(meanResponseTravelTimePerNode);

			String statement8 = "Request " + requestHash + ": Average delay Request to first response was "
					+ Time.getFormattedTime(meanTimeRequestToResponseInitialNodes) + " for initial nodes.";

			String statement9 = "Request " + requestHash + ": Average delay Request to first response was "
					+ Time.getFormattedTime(meanTimeRequestToResponseAllNodes) + " for all nodes.";

			if (logout) {
				Monitor.log(RequestResponseAnalyzerImpl.class, Level.INFO,
						"%s: Request: " + requestHash
								+ "\r\n%s \r\n%s \r\n%s \r\n%s \r\n%s \r\n%s \r\n%s \r\n%s \r\n%s",
						Time.getFormattedTime(), statement1, statement2, statement3, statement4, statement5, statement6,
						statement7, statement8, statement9);
			}

			if (printout) {
				System.out.println("-------- Request: " + requestHash + " --------");

				System.out.println(statement1);
				System.out.println(statement2);

				System.out.println(statement3);
				System.out.println(statement4);

				System.out.println(statement5);

				System.out.println(statement6);

				System.out.println(statement7);

				System.out.println(statement8);

				System.out.println(statement9);

				System.out.println("------------------------------------");
			}
		}

		// delete all entries for this request in here...
		infosAtRequestCreation.remove(requestHash);
		expectedReceiversForRequest.remove(requestHash);
		perRequestReceivedInfo.remove(requestHash);
		perRequestReachedNodes.remove(requestHash);
	}

	/**
	 * Computes the mean travel time between transmitting events and receiving
	 * events <br>
	 * <b>Needs both lists to have the same length!</b>
	 * 
	 * @param sen
	 *            -list of sending times
	 * @param rec
	 *            -list of receiving times
	 */
	private long getMeanTravelTime(HashMap<Long, Long> sentAndReceived) {

		long meanTravelTime = 0L;

		for (long send : sentAndReceived.keySet()) {
			long received = sentAndReceived.get(send);
			meanTravelTime += received - send;
		}
		meanTravelTime = meanTravelTime / sentAndReceived.size();

		return meanTravelTime;
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {

		INodeID node = source.getHost().getId();

		switch (peerStatus) {
		case ABSENT:
			currentOnlineNodes.remove(node);
			currentOfflineNodes.add(node);
			break;
		case PRESENT:
			currentOnlineNodes.add(node);
			currentOfflineNodes.remove(node);

			// get all currently valid requests and add this node the list of
			// expected receivers!
			// removeInvalidRequests();

			for (HashSet<Integer> set : validRequests.values()) {
				for (int requestHash : set) {
					expectedReceiversForRequest.get(requestHash).add(node);
				}
			}

			break;
		default:
			return;
		}
	}

	// stores all important information per request-generation (this is
	// different from request received!)
	private class requestCreatedInfo {
		INodeID requester;
		long creationTime;
		long validity;
		String type;
		LinkedList<INodeID> expectedReceivers;
		int expectedNumberOfAnswersPerNode;
		LinkedList<INodeID> intendedReceivers;
	}
	
	// stores per node info about received requests
	private class requestReceivedInfo {
		INodeID nodeID;
		long receivedAt;
	}

}
