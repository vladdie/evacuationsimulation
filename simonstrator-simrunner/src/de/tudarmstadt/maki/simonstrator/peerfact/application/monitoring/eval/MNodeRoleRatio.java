package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval.MNodeRoleRatio.MNodeRoleRatioValue;

/**
 * 
 * 
 * @author Nils Richerzhagen
 *
 */
public class MNodeRoleRatio extends AbstractMetric<MNodeRoleRatioValue> implements Metric<MNodeRoleRatioValue> {

	public MNodeRoleRatio() {
		super("Ratio of Sinks over Nodes", MetricUnit.NONE);
	}

	/**
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	protected class MNodeRoleRatioValue implements de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue<Double> {

		@Override
		public Double getValue() {
			double ratio = -1;
			double leaves = 0;
			double sinks = 0;
			for (Host host : Oracle.getAllHosts()) {
				try {
					CraterNodeComponent comp = (CraterNodeComponent) host.getComponent(CraterNodeComponent.class);
					if (comp.isPresent()) {
						switch (comp.getCraterNodeRole()) {
						case LEAF:
							leaves++;
							break;
						case SINK:
							sinks++;
							break;
						default:
							throw new AssertionError(
									"Should not happen that a node is present and does not have the LEAF or SINK role.");
						}
					}
				} catch (ComponentNotAvailableException e) {
					// don't care
				}
			}
			if (sinks != 0) {
				ratio = sinks / (leaves + sinks);
			}
			return ratio;
		}

		@Override
		public boolean isValid() {
			return getValue() != -1;
		}

	}

	@Override
	public void initialize(List<Host> hosts) {
		setOverallMetric(new MNodeRoleRatioValue());
	}
}
