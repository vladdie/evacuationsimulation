package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub;

import de.tud.kom.p2psim.api.application.WorkloadGenerator;
import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView.VisualizationInjector;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.vis.PubSubAppVisualization;

/**
 * Workload-App for Pub/Sub overlays.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Sep 18, 2013
 */
public class PubSubWorkloadAppFactory implements HostComponentFactory {

	private boolean enableVisualization = false;

	private boolean enableDemoMode = false;

	private boolean firstHost = true;

	private WorkloadGenerator workload = null;

	public static boolean SIMULATION_STARTED = false;

	@Override
	public HostComponent createComponent(Host host) {
		if (enableVisualization && firstHost) {
			PubSubWorkloadApp.ENABLE_EVAL = true;
			firstHost = false;
			if (enableDemoMode) {
				PubSubAppVisualization.ENABLE_DEMO_MODE = true;
			}
			VisualizationInjector.injectComponent(PubSubAppVisualization.getInstance());
			PubSubWorkloadAppFactory.SIMULATION_STARTED = true;
		}
		if (workload != null) {
			return new PubSubWorkloadApp((SimHost) host, workload);
		} else {
			return new PubSubWorkloadApp((SimHost) host);
		}
	}

	/**
	 * Add a workload-generator to the application
	 * 
	 * @param workload
	 */
	public void setWorkload(WorkloadGenerator workload) {
		this.workload = workload;
	}

	/**
	 * If set to true, the evaluation mode is disabled (i.e., delivery ratio and
	 * other metrics are not computed).
	 * 
	 * @param enableEval
	 */
	public void setDisableEval(boolean disableEval) {
		PubSubWorkloadApp.ENABLE_EVAL = !disableEval;
	}

	/**
	 * If set to true, the Workload app will draw a visualization layer
	 * (requires the VisTopologyView!) for the pub/sub workload.
	 * 
	 * @param enableVis
	 */
	public void setEnableVis(boolean enableVis) {
		this.enableVisualization = enableVis;
	}

	/**
	 * If set to true, a side panel with controls is added.
	 * 
	 * @param enableDemoMode
	 */
	public void setEnableDemoMode(boolean enableDemoMode) {
		this.enableDemoMode = enableDemoMode;
	}
}
