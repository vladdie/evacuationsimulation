package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;

@Entity
@Table(name = "data_routing_measurements")
public class DataRoutingMeasurement extends CustomMeasurement {

	public static final MetricDescription DATA_ROUTING_METRIC = new MetricDescription(DataRoutingAnalyzerImpl.class,
			"DataRoutingMeasurement", "Provides delivery and duplicate ratios from the sinks and central entity", "");

	@SuppressWarnings("unused")
	private double deliveryRatioAtSink_ContainerMsg;

	@SuppressWarnings("unused")
	private double duplicateRatioAtSink_ContainerMsg;

	@SuppressWarnings("unused")
	private double averageSameContainerMsgRec;

	@SuppressWarnings("unused")
	private double singleMaxSameContainerMsgRec;

	@SuppressWarnings("unused")
	private double deliveryRatioAtSink_normalData;

	@SuppressWarnings("unused")
	private double duplicateRatioAtSinkWithoutResolving_normalData;

	@SuppressWarnings("unused")
	private double duplicateRatioAtSinkWithResolving_normalData;

	@SuppressWarnings("unused")
	private double deliveryRatioAtSink_dupSensData;

	@SuppressWarnings("unused")
	private double duplicateRatioAtSinkWithoutResolving_dupSensData;

	@SuppressWarnings("unused")
	private double duplicateRatioAtSinkWithResolving_dupSensData;

	@SuppressWarnings("unused")
	private double deliveryRatioAtCentral_UploadMsg;

	@SuppressWarnings("unused")
	private double duplicateRatioAtCentral_UploadMsg;

	@SuppressWarnings("unused")
	private double deliveryRatioAtCentral_normalData;

	@SuppressWarnings("unused")
	private double duplicateRatioAtCentral_normalData;

	@SuppressWarnings("unused")
	private double deliveryRatioAtCentral_dupSensData;

	@SuppressWarnings("unused")
	private double duplicateRatioAtCentral_dupSensData;

	public DataRoutingMeasurement(double deliveryRatioAtSink_ContainerMsg, double duplicateRatioAtSink_ContainerMsg,
			double averageSameContainerMsgRec, double singleMaxSameContainerMsgRec, double deliveryRatioAtSink_normalData,
			double duplicateRatioAtSinkWithoutResolving_normalData, double duplicateRatioAtSinkWithResolving_normalData,
			double deliveryRatioAtSink_dupSensData, double duplicateRatioAtSinkWithoutResolving_dupSensData,
			double duplicateRatioAtSinkWithResolving_dupSensData, double deliveryRatioAtCentral_UploadMsg,
			double duplicateRatioAtCentral_UploadMsg, double deliveryRatioAtCentral_normalData,
			double duplicateRatioAtCentral_normalData, double deliveryRatioAtCentral_dupSensData, double duplicateRatioAtCentral_dupSensData) {
		super();
		this.deliveryRatioAtSink_ContainerMsg = deliveryRatioAtSink_ContainerMsg;
		this.duplicateRatioAtSink_ContainerMsg = duplicateRatioAtSink_ContainerMsg;
		this.averageSameContainerMsgRec = averageSameContainerMsgRec;
		this.singleMaxSameContainerMsgRec = singleMaxSameContainerMsgRec;
		this.deliveryRatioAtSink_normalData = deliveryRatioAtSink_normalData;
		this.duplicateRatioAtSinkWithoutResolving_normalData = duplicateRatioAtSinkWithoutResolving_normalData;
		this.duplicateRatioAtSinkWithResolving_normalData = duplicateRatioAtSinkWithResolving_normalData;
		this.deliveryRatioAtSink_dupSensData = deliveryRatioAtSink_dupSensData;
		this.duplicateRatioAtSinkWithoutResolving_dupSensData = duplicateRatioAtSinkWithoutResolving_dupSensData;
		this.duplicateRatioAtSinkWithResolving_dupSensData = duplicateRatioAtSinkWithResolving_dupSensData;
		this.deliveryRatioAtCentral_UploadMsg = deliveryRatioAtCentral_UploadMsg;
		this.duplicateRatioAtCentral_UploadMsg = duplicateRatioAtCentral_UploadMsg;
		this.deliveryRatioAtCentral_normalData = deliveryRatioAtCentral_normalData;
		this.duplicateRatioAtCentral_normalData = duplicateRatioAtCentral_normalData;
		this.deliveryRatioAtCentral_dupSensData = deliveryRatioAtCentral_dupSensData;
		this.duplicateRatioAtCentral_dupSensData = duplicateRatioAtCentral_dupSensData;
	}
}
