package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.vis;

import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView.VisualizationInjector;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;

/**
 * App for Visualization of Crater.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CraterVizualizationAppFactory implements
		HostComponentFactory {

	private boolean firstHost = true;

	@Override
	public HostComponent createComponent(Host host) {
		if (firstHost) {
			firstHost = false;
			VisualizationInjector.injectComponent(CraterVisualization.getInstance());
		}

		return new CraterVisualizationApp((SimHost) host);
	}

}
