package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.math.stat.descriptive.DescriptiveStatistics;

import de.tud.kom.p2psim.api.topology.Topology;
import de.tud.kom.p2psim.impl.topology.PositionVector;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;
import de.tud.kom.p2psim.impl.util.oracle.GlobalOracle;
import de.tudarmstadt.maki.simonstrator.api.Binder;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.transitions.AbstractTransitionController;

/**
 * Writing per-publication results via the default metrics interface. Supports
 * writing of aggregated results - in this case, in addition to periodic output,
 * we also write a total aggregate once at the end of the simulation.
 * 
 * TODO additionally, we can sample metrics for grid-cells on the simulated
 * area. In this case, we "bin" the area into 10 m^2 cells for which we then
 * calculate aggregates - however, this is only done for the whole simulation,
 * not sampled - otherwise, this leads to database explosions.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class PubSubResultPersistingMetrics implements PubSubResultPersisting, EventHandler {

	private boolean writeListMetrics = false;

	private long startDaoAt = 0;

	private long stopDaoAt = Long.MAX_VALUE;

	private long aggregationInterval = Time.MINUTE;

	private boolean aggregateValues = false;

	private boolean enableSpatialSampling = false;

	private Map<String, Map<MetricDescription, DescriptiveStatistics>> aggregates = new LinkedHashMap<>();

	private Map<String, Map<MetricDescription, DescriptiveStatistics>> overallAggregates = new LinkedHashMap<>();

	private Map<String, Map<MetricDescription, Map<Integer, DescriptiveStatistics>>> spatialAggregates = new LinkedHashMap<>();

	private Map<Long, PositionVector> positionVectors = new LinkedHashMap<>();

	private int worldSizeX;

	private double spatialResolution = 20;

	private Map<Long, String> hostIdToGroup = new LinkedHashMap<>();

	private static final int EVENT_PERSIST_AGGREGATES = 1;

	/**
	 * This switch is used to enable sampling of raw (per-publication) data
	 * points programmatically. It is used in conjunction with a
	 * {@link AbstractTransitionController} to measure metrics at and around the
	 * occurrence of a transition.
	 */
	public static boolean ENABLE_RAW_SAMPLING = false;

	private long timestampStart = -1;

	public PubSubResultPersistingMetrics() {
		PubSubGlobalKnowledge.OBSERVE_PUBLICATIONS = true;
	}

	/**
	 * Recall, Delivery ratio of a single publication (ratio of reached
	 * subscribers vs. all interested subscribers).
	 */
	protected static final MetricDescription PUB_DELIVERY_RECALL = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliveryRecall",
			"Recall of a single publication: number of notified subscribers divided by number of all subscribers",
			"none");

	/**
	 * Precision of a single publication (ratio of reached subscribers vs.
	 * reached subscribers + wrongly notified nodes)
	 */
	protected static final MetricDescription PUB_DELIVERY_PRECISION = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliveryPrecision",
			"Precision of a single publication: number of notified subscribers divided by number of all notified nodes",
			"none");

	/**
	 * Complexity of matching involved with the publication (e.g., number of
	 * match function calls, potentially weighted)
	 */
	protected static final MetricDescription PUB_DELIVERY_MATCHING_COMPLEXITY = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliveryMatchingComplexity",
			"Complexity of matching involved with the publication (e.g., number of match function calls, potentially weighted)",
			"none");

	/**
	 * Number of subscribers for this notification.
	 */
	protected static final MetricDescription PUB_DELIVERY_SUBSCRIBERS_TOTAL = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliverySubscribersTotal",
			"Global Knowledge: number of receivers that should have received the publication", "none");

	/**
	 * Actually reached subscribers.
	 */
	protected static final MetricDescription PUB_DELIVERY_SUBSCRIBERS_REACHED = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliverySubscribersReached",
			"Global Knowledge: number of receivers that did indeed receive the publication", "none");

	/**
	 * Nodes that were notified, even if they did not subscribe. This is
	 * measured on the Application layer, so a pub/sub overlay might still
	 * filter irrelevant notifications at the client before notifying the app.
	 */
	protected static final MetricDescription PUB_DELIVERY_UNINTERESTED_REACHED = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliveryUninterestedReached",
			"Global Knowledge: nodes that received the publication but did not subscribe to it", "none");

	/**
	 * Number of gateways involved in sending this message from the cloud to
	 * clients.
	 */
	protected static final MetricDescription PUB_DELIVERY_GATEWAYS_USED = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliveryGatewaysUsed",
			"Number of gateways used during delivery of this publication", "none");

	/**
	 * Average delivery delay of this publication to all subscribers.
	 */
	protected static final MetricDescription PUB_DELIVERY_DELAY_AVG = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliveryDelayAvg",
			"Average Delivery Delay of a single publication", "us");

	protected static final MetricDescription PUB_DELIVERY_DELAYS = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliveryDelays",
			"Combined Metric of all delivery delays for a single publication", "us");

	protected static final MetricDescription PUB_NODE_DISTANCES = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubNodeDistances",
			"Combined Metric of all distances between publisher and subscribers of this publication", "m");

	protected static final MetricDescription PUB_DELIVERY_COUNT = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliveryCount",
			"Combined Metric of the number of times a publication was delivered to the app layer of subscribed hosts. Ideally, this metric is ==1",
			"none");

	protected static final MetricDescription PUB_DELIVERY_COUNT_AVG = new MetricDescription(
			PubSubResultPersistingMetrics.class, "PubDeliveryCountAvg",
			"Average number of times the publication is delivered to a single subscriber's application", "none");

	@Override
	public void start() {
		// don't care
		if (aggregateValues) {
			Event.scheduleWithDelay(aggregationInterval, this, null, EVENT_PERSIST_AGGREGATES);
		}
		worldSizeX = (int) Binder.getComponentOrNull(Topology.class).getWorldDimensions().getX();
		timestampStart = Time.getCurrentTime();
	}

	@Override
	public void stop(Writer out) {
		// write global aggregates
		if (aggregateValues) {
			for (Entry<String, Map<MetricDescription, DescriptiveStatistics>> byGroup : overallAggregates.entrySet()) {
				for (Entry<MetricDescription, DescriptiveStatistics> byMetric : byGroup.getValue().entrySet()) {
					MeasurementDAO.storeGroupStatisticsMeasurement(byMetric.getKey(), byGroup.getKey(),
							Time.getCurrentTime(), byMetric.getValue(), Time.getCurrentTime() - timestampStart, true);
				}
				byGroup.getValue().clear();
			}
		}
		// Write spatial samples (only if values > 0)
		if (enableSpatialSampling) {
			for (Entry<String, Map<MetricDescription, Map<Integer, DescriptiveStatistics>>> byGroup : spatialAggregates
					.entrySet()) {
				for (Entry<MetricDescription, Map<Integer, DescriptiveStatistics>> byMetric : byGroup.getValue()
						.entrySet()) {
					for (Entry<Integer, DescriptiveStatistics> spatialCell : byMetric.getValue().entrySet()) {
						int[] location = getCoordinateForKey(spatialCell.getKey());
						MeasurementDAO.storeSpatialGroupStatisticsMeasurement(byMetric.getKey(), byGroup.getKey(),
								Time.getCurrentTime(), spatialCell.getValue(), Time.getCurrentTime() - timestampStart,
								true, location[0], location[1]);
					}
					byMetric.getValue().clear();
				}
			}
		}
		// stop aggregation events, if any.
		aggregateValues = false;
	}

	/**
	 * Store a value inside an aggregate.
	 * 
	 * @param metric
	 * @param value
	 */
	private void addToAggregate(String group, MetricDescription metric, double value, long hostId, int spatialKey) {
		if (!aggregates.containsKey(group)) {
			aggregates.put(group, new LinkedHashMap<>());
		}
		if (!overallAggregates.containsKey(group)) {
			overallAggregates.put(group, new LinkedHashMap<>());
		}
		Map<MetricDescription, DescriptiveStatistics> stats = aggregates.get(group);
		Map<MetricDescription, DescriptiveStatistics> overall = overallAggregates.get(group);
		if (!stats.containsKey(metric)) {
			stats.put(metric, new DescriptiveStatistics());
		}
		if (!overall.containsKey(metric)) {
			overall.put(metric, new DescriptiveStatistics());
		}
		stats.get(metric).addValue(value);
		overall.get(metric).addValue(value);

		if (enableSpatialSampling) {
			if (!spatialAggregates.containsKey(group)) {
				spatialAggregates.put(group, new LinkedHashMap<>());
			}
			Map<MetricDescription, Map<Integer, DescriptiveStatistics>> spatialByMetric = spatialAggregates.get(group);
			if (!spatialByMetric.containsKey(metric)) {
				spatialByMetric.put(metric, new LinkedHashMap<Integer, DescriptiveStatistics>());
			}
			Map<Integer, DescriptiveStatistics> spatial = spatialByMetric.get(metric);
			if (!spatial.containsKey(spatialKey)) {
				spatial.put(spatialKey, new DescriptiveStatistics());
			}
			spatial.get(spatialKey).addValue(value);
		}
	}

	/**
	 * Mapping (location -> int)
	 * 
	 * @param hostId
	 * @return
	 */
	private int getSpatialKeyFor(Location location) {
		int key = (int) (((int) (location.getLongitude() / spatialResolution)) * worldSizeX
				+ (int) (location.getLatitude() / spatialResolution));
		return key;
	}

	/**
	 * Reverse mapping
	 * 
	 * @param key
	 * @return
	 */
	private int[] getCoordinateForKey(int key) {
		int[] coordinate = new int[2];
		coordinate[0] = (int) (key / worldSizeX);
		coordinate[1] = key % worldSizeX;
		return coordinate;
	}

	/**
	 * Fork for aggregates vs. direct measurements
	 * 
	 * @param metric
	 * @param hostId
	 * @param time
	 * @param value
	 */
	private void storeValue(MetricDescription metric, long hostId, long time, double value, int spatialKey) {
		if (aggregateValues) {
			String group = hostIdToGroup.get(hostId);
			if (group == null) {
				group = GlobalOracle.getHostForHostID(hostId).getProperties().getGroupID();
				hostIdToGroup.put(hostId, group);
			}
			addToAggregate(group, metric, value, hostId, spatialKey);
			if (ENABLE_RAW_SAMPLING) {
				/*
				 * RAW sampling, programmatically during and after a transition
				 * took place
				 */
				MeasurementDAO.storeSingleMeasurement(metric, hostId, time, value);
			}
		} else {
			MeasurementDAO.storeSingleMeasurement(metric, hostId, time, value);
		}
	}

	/**
	 * Periodically persist aggregates.
	 */
	private void persistAggregates() {
		long time = Time.getCurrentTime();
		for (Entry<String, Map<MetricDescription, DescriptiveStatistics>> byGroup : aggregates.entrySet()) {
			for (Entry<MetricDescription, DescriptiveStatistics> byMetric : byGroup.getValue().entrySet()) {
				MeasurementDAO.storeGroupStatisticsMeasurement(byMetric.getKey(), byGroup.getKey(), time,
						byMetric.getValue(), aggregationInterval, false);
			}
			byGroup.getValue().clear();
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		assert type == EVENT_PERSIST_AGGREGATES;
		persistAggregates();
		if (aggregateValues) {
			Event.scheduleWithDelay(aggregationInterval, this, null, EVENT_PERSIST_AGGREGATES);
		}
	}

	@Override
	public void persistPublication(PublicationResultCollector collector) {
		// We use the time when the publication was created
		long time = collector.startedTimestamp;
		if (time < startDaoAt || time > stopDaoAt) {
			// not interested
			return;
		}
		
		int spatialKey = -1;
		if (enableSpatialSampling) {
			if (collector.publication instanceof LocationBasedNotification) {
				Location loc = ((LocationBasedNotification) collector.publication).getLocation();
				spatialKey = getSpatialKeyFor(loc);
			}
		}

		/*
		 * Might be invalid
		 */
		if (collector.getAverageDeliveryDelay() != -1) {
			storeValue(PUB_DELIVERY_DELAY_AVG, collector.initiatorHostID.value(), time,
					collector.getAverageDeliveryDelay(), spatialKey);
		}
		storeValue(PUB_DELIVERY_RECALL, collector.initiatorHostID.value(), time, collector.getDeliveryRecall(),
				spatialKey);

		/*
		 * Precision might be invalid (-1)
		 */
		if (collector.getDeliveryPrecision() != -1) {
			storeValue(PUB_DELIVERY_PRECISION, collector.initiatorHostID.value(), time,
					collector.getDeliveryPrecision(), spatialKey);
		}

		/*
		 * Only values > 0 are relevant.
		 */
		if (collector.getNumberOfGatewaysUsed() > 0) {
			storeValue(PUB_DELIVERY_GATEWAYS_USED, collector.initiatorHostID.value(), time,
					collector.getNumberOfGatewaysUsed(), spatialKey);
		}

		/*
		 * # of match OPs might be invalid
		 */
		if (collector.getMatchComplexity() != -1) {
			storeValue(PUB_DELIVERY_MATCHING_COMPLEXITY, collector.initiatorHostID.value(), time,
					collector.getMatchComplexity(), spatialKey);
		}

		/*
		 * Avg. Number of deliveries
		 */
		storeValue(PUB_DELIVERY_COUNT_AVG, collector.initiatorHostID.value(), time,
				collector.getAverageNumberOfDeliveriesPerSubscriber(), spatialKey);

		storeValue(PUB_DELIVERY_SUBSCRIBERS_REACHED, collector.initiatorHostID.value(), time,
				collector.getReachedReceivers().size(), spatialKey);
		storeValue(PUB_DELIVERY_SUBSCRIBERS_TOTAL, collector.initiatorHostID.value(), time,
				collector.getTotalNumberOfInterestedReceivers(), spatialKey);
		storeValue(PUB_DELIVERY_UNINTERESTED_REACHED, collector.initiatorHostID.value(), time,
				collector.getUnintendedReachedReceivers().size(), spatialKey);

		if (writeListMetrics) {
			MeasurementDAO.storeListMeasurement(PUB_DELIVERY_DELAYS, collector.initiatorHostID.value(), time,
					collector.getDeliveryDelaysAsList());
			MeasurementDAO.storeListMeasurement(PUB_DELIVERY_COUNT, collector.initiatorHostID.value(), time,
					collector.getNumberOfDeliveriesAsList());
		}
	}

	@Override
	public void persistSubscription(SubscriptionResultCollector collector) {
		/*
		 * Simply not supported - in favor of custom table as implemented in the
		 * CustomMetric-variant of this class.
		 */
		throw new UnsupportedOperationException();
	}

	/**
	 * If set to true, the analyzer will also write list metrics to the
	 * database.
	 * 
	 * @param writeListMetrics
	 */
	public void setWriteListMetrics(boolean writeListMetrics) {
		this.writeListMetrics = writeListMetrics;
	}

	/**
	 * If set to true, publication-related metrics are aggregated and then
	 * written to the STATISTICS-DAO at a given frequency.
	 * 
	 * @param aggregateValues
	 */
	public void setAggregateValues(boolean aggregateValues) {
		this.aggregateValues = aggregateValues;
	}

	/**
	 * Duration of a single aggregation interval
	 * 
	 * @param aggregationInterval
	 */
	public void setAggregationInterval(long aggregationInterval) {
		this.aggregationInterval = aggregationInterval;
	}

	/**
	 * Timestamp at which we start writing to the database (the creation time of
	 * the publication is used as indicator)
	 * 
	 * @param startDaoAt
	 */
	public void setStartDaoAt(long startDaoAt) {
		this.startDaoAt = startDaoAt;
	}

	/**
	 * Timestamp at which we start writing to the database (the creation time of
	 * the publication is used as indicator)
	 * 
	 * @param stopDaoAt
	 */
	public void setStopDaoAt(long stopDaoAt) {
		this.stopDaoAt = stopDaoAt;
	}

	/**
	 * If true, metrics are additionally grouped into bins depending on the
	 * host's current location, for later spatial plotting.
	 * 
	 * @param enableSpatialSampling
	 */
	public void setEnableSpatialSampling(boolean enableSpatialSampling) {
		this.enableSpatialSampling = enableSpatialSampling;
	}

}
