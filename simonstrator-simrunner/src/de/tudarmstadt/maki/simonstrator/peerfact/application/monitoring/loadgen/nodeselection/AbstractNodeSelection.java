package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.nodeselection;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * Abstract class for the node selection strategy. Defines which nodes are
 * picked to send a request, in order to follow the given load distribution or
 * trace.
 * 
 * @author ConstiZ
 *
 */
public abstract class AbstractNodeSelection {
	List<INodeID> allNodeIDs;

	/**
	 * Initialize NodeSelection.
	 */
	public AbstractNodeSelection() {
	}

	public void setAllNodeIDs(List<INodeID> allIDs) {
		this.allNodeIDs = allIDs;
	}

	/**
	 * Selects nodes ID's according to a strategy.
	 * 
	 * @param nodeCount
	 *            Amount of nodes that should be selected. (given by
	 *            distribution)
	 * @param activeNodeIDs
	 *            List of all active Nodes ID's to select from.
	 * @return List of selected Node ID's according to strategy
	 * @throws IllegalSelectionException
	 */
	public abstract List<INodeID> selectNodes(int nodeCount,
			List<INodeID> activeNodeIDs) throws IllegalSelectionException;

	/**
	 * Validates input for {@link #selectNodes(int, List) selectNodes()}
	 * 
	 * @param nodeCount
	 * @param allNodeIDs2
	 * @return
	 * @throws IllegalSelectionException
	 */
	protected boolean validateSelection(int nodeCount,
			List<INodeID> allNodeIDs2) throws IllegalSelectionException {
		if (nodeCount < 0)
			throw new IllegalSelectionException("Negative nodeCount");

		if (nodeCount > allNodeIDs2.size())
			throw new IllegalSelectionException("List too short");

		return true;
	}

	public class IllegalSelectionException extends Exception {
		public IllegalSelectionException(String message) {
			super(message);
		}
	}
}
