package de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import de.tud.kom.p2psim.impl.topology.movement.modularosm.mapvisualization.IMapVisualization;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import javafx.util.Pair;

/**
 * Visualisation of the disaster region analysing.
 *
 * @author Clemens Krug
 */
public class DisasterRegionCentralView extends JFrame
{
    private BufferedImage image;


    /**
     * Initialises the UI
     * @param mapVisualization The {@link IMapVisualization} used to visualize the OSM-Maps. Can be null.
     */
    public DisasterRegionCentralView(IMapVisualization mapVisualization)
    {
        image = new BufferedImage(VisualizationTopologyView.VisualizationInjector.getWorldX(),
                VisualizationTopologyView.VisualizationInjector.getWorldY(), BufferedImage.TYPE_INT_ARGB);

        JComponent comp = new JComponent()
        {
            @Override
            protected void paintComponent(Graphics g)
            {
                super.paintComponent(g);
                Graphics2D g2 = (Graphics2D) g;
                g2.drawImage(image, 0, 0, null);
            }
        };
        comp.setBounds(0, 0, VisualizationTopologyView.VisualizationInjector.getWorldX(),
                VisualizationTopologyView.VisualizationInjector.getWorldY());

        JLayeredPane layeredPane = new JLayeredPane();
        layeredPane.setPreferredSize(new Dimension(VisualizationTopologyView.VisualizationInjector.getWorldX(),
                VisualizationTopologyView.VisualizationInjector.getWorldY()));
        layeredPane.add(comp);
        if(mapVisualization != null)
        {
            ((JComponent) mapVisualization).setBounds(0, 0, VisualizationTopologyView.VisualizationInjector.getWorldX(),
                    VisualizationTopologyView.VisualizationInjector.getWorldY());
            layeredPane.add((JComponent) mapVisualization);
        }

        JScrollPane scrollPanel = new JScrollPane(layeredPane,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPanel.setPreferredSize(new Dimension(800, 600));

        getContentPane().add(scrollPanel);

        setTitle("Disaster Region Central Analyzer");
        pack();
        setVisible(true);
    }


    /**
     * Called when the corresponding DisasterRegionAnalyzer has new data available.
     * @param locations The updated locations with no connection.
     */
    public void onDataUpdated(List locations)
    {
        Graphics2D g2 = (Graphics2D) image.getGraphics();
        g2.setBackground(new Color(255, 255, 255, 0));
        g2.clearRect(0, 0, VisualizationTopologyView.VisualizationInjector.getWorldX(), VisualizationTopologyView.VisualizationInjector.getWorldY());

		g2.setColor(Color.BLUE);
        locations.stream().forEach(l -> g2.fillOval(
                                            (int) ((Pair<UniqueID, Location>) l).getValue().getLongitude(),
                                            (int)((Pair<UniqueID, Location>) l).getValue().getLatitude(),
                                            5, 5));
        g2.dispose();
        repaint();
    }




}
