/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.Component;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.PubSubNotificationForwardingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer.BypassSubscriptionSchemeAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubWorkloadApp;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.NotificationInfoImpl;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.SubscriptionInfoImpl;

/**
 * Global Knowledge used to assess the success and recall of publication
 * delivery.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Sep 17, 2013
 */
public class PubSubGlobalKnowledge implements Component, EventHandler,
		PubSubNotificationForwardingAnalyzer, BypassSubscriptionSchemeAnalyzer {

	private static final int EVENT_PUBLICATION_TIMEOUT = 12;

	private static final int EVENT_SUBSCRIPTION_PERSIST = 15;

	public static final long TIMEOUT_PUBLICATION_DELIVERY = 2 * Time.SECOND;

	public static final long TIMEOUT_NOTIFICATION_AGE = 2 * Time.SECOND;

	private static final Map<PubSubComponent, PubSubGlobalKnowledgeNodeState> nodeStates = new LinkedHashMap<PubSubComponent, PubSubGlobalKnowledgeNodeState>();

	private static final PubSubGlobalKnowledge instance = new PubSubGlobalKnowledge();

	protected static long averageDeliveryDelay = -1;

	protected static double averageDeliveryCounter = -1;

	protected static double averageDeliveryToUninterested = -1;

	protected static double averageRecall = -1;

	protected static double averagePrecision = -1;

	protected static double averageMatchComplexity = -1;

	private static boolean first = true;

	public static boolean OBSERVE_PUBLICATIONS = false;

	public static boolean OBSERVE_SUBSCRIPTIONS = false;

	public static long OBSERVE_SUBSCRIPTIONS_INTERVAL = -1;

	private PubSubGlobalKnowledge() {
		// limit access
	}

	/**
	 * To be called within the initialize-method of the PubSubWorkloadApp
	 * 
	 * @param host
	 */
	public static void addPubSubComponent(PubSubComponent component) {
		if (first) {
			Monitor.registerAnalyzer(instance);
			first = false;
		}
		try {
			PubSubWorkloadApp app = component.getHost().getComponent(
					PubSubWorkloadApp.class);
			PubSubGlobalKnowledgeNodeState nState = new PubSubGlobalKnowledgeNodeState(
					app, component);
			nodeStates.put(component, nState);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError();
		}
	}

	/**
	 * Called from within the NodeState as soon as a Publication is issued
	 * 
	 * @param from
	 * @param pub
	 */
	public static void observePublication(PubSubComponent from,
			Notification pub) {

		if (!OBSERVE_PUBLICATIONS) {
			return;
		}

		/*
		 * Find all nodes that have subscribed to this publication - we exclude
		 * the publisher itself, as local delivery leads to skewed evaluation
		 * results (delay, success)
		 */
		List<PubSubGlobalKnowledgeNodeState> matchingNodes = new LinkedList<PubSubGlobalKnowledgeNodeState>();
		for (PubSubGlobalKnowledgeNodeState nodeState : nodeStates.values()) {
			if (nodeState.getComponent().equals(from)) {
				// see comment above
				continue;
			}
			if (nodeState.hasSubscriptionMatching(pub)) {
				matchingNodes.add(nodeState);
			}
		}

		PublicationResultCollector pres = new PublicationResultCollector(from, pub, matchingNodes);
		((NotificationInfoImpl) (pub._getNotificationInfo(null))).resultCollector = pres;

		Event.scheduleWithDelay(TIMEOUT_PUBLICATION_DELIVERY, instance, pub,
				EVENT_PUBLICATION_TIMEOUT);
	}

	/**
	 * Called from within the NodeState as soon as a Subscription is issued.
	 * 
	 * @param from
	 * @param sub
	 */
	public static void observeSubscription(PubSubComponent from, Subscription sub) {
		/*
		 * Just attach the result collector. If this is the first subscription,
		 * and if we have an interested ResultPersist-Pipeline, start periodic
		 * subscription metric collection.
		 */
		if (!OBSERVE_SUBSCRIPTIONS || OBSERVE_SUBSCRIPTIONS_INTERVAL <= 0) {
			return;
		}

		Event.scheduleWithDelay(OBSERVE_SUBSCRIPTIONS_INTERVAL, instance, sub, EVENT_SUBSCRIPTION_PERSIST);
		SubscriptionResultCollector sres = new SubscriptionResultCollector(getNodeState(from), sub,
				TIMEOUT_NOTIFICATION_AGE);
		((SubscriptionInfoImpl) (sub._getSubscriptionInfo(null))).resultCollector = sres;
	}

	/**
	 * Access for the visualization and inner classes
	 * 
	 * @param notification
	 * @return the PublicationResultCollector or null, if the timeout has
	 *         already fired
	 */
	public static PublicationResultCollector getPublicationResultCollectorFor(
			Notification notification) {
		return ((NotificationInfoImpl) (notification._getNotificationInfo(null))).resultCollector;
	}

	/**
	 * Result collector attached to this subscription
	 * 
	 * @param subscription
	 * @return the {@link SubscriptionResultCollector} or null, if none is
	 *         attached to this subscription.
	 */
	public static SubscriptionResultCollector getSubscriptionResultCollectorFor(Subscription subscription) {
		return ((SubscriptionInfoImpl) (subscription._getSubscriptionInfo(null))).resultCollector;
	}

	/**
	 * 
	 * @param on
	 * @param notification
	 */
	public static void receivedNotification(PubSubComponent on,
			Notification notification, Subscription matchedSubscription) {
		// Log notification-related data
		PublicationResultCollector collector = getPublicationResultCollectorFor(notification);
		if (collector != null) {
			collector.notificationArrivedAt(nodeStates.get(on));
		}
		// Log subscription-related data
		SubscriptionResultCollector subCollector = getSubscriptionResultCollectorFor(matchedSubscription);
		if (subCollector != null) {
			subCollector.notificationArrived(notification);
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		switch (type) {
		case EVENT_PUBLICATION_TIMEOUT:
			handlePublicationTimeout((Notification) content);
			break;
			
		case EVENT_SUBSCRIPTION_PERSIST:
			Subscription sub = (Subscription) content;
			persistSubscriptionMetrics(sub);
			if (sub._getSubscriptionInfo(null).isActive() && OBSERVE_SUBSCRIPTIONS_INTERVAL > 0) {
				/*
				 * reschedule if the subscription is still active
				 */
				Event.scheduleWithDelay(OBSERVE_SUBSCRIPTIONS_INTERVAL, instance, sub, EVENT_SUBSCRIPTION_PERSIST);
			}
			break;

		default:
			throw new AssertionError();
		}
	}

	/**
	 * Persist information related to the subscription. Invoked periodically for
	 * each subscription, according to the interval defined in the
	 * {@link PubSubResultPersisting} analyzer.
	 * 
	 * @param subscription
	 */
	private static void persistSubscriptionMetrics(Subscription subscription) {
		SubscriptionResultCollector collector = getSubscriptionResultCollectorFor(subscription);
		if (Monitor.hasAnalyzer(PubSubResultPersisting.class)) {
			Monitor.getOrNull(PubSubResultPersisting.class).persistSubscription(collector);
		}
	}

	/**
	 * 
	 * @param collector
	 */
	private static void handlePublicationTimeout(Notification publication) {
		PublicationResultCollector collector = getPublicationResultCollectorFor(publication);
		collector.finalizeMeasurement();

		/*
		 * With this new concept, each persisting step is done via an instance
		 * of a PublicationResultPersisting Analyzer.
		 */
		if (collector.isValid() && Monitor.hasAnalyzer(PubSubResultPersisting.class)) {
			Monitor.getOrNull(PubSubResultPersisting.class).persistPublication(collector);
		}
		updateAverageMetrics(collector);
	}

	/*
	 * Access to performance metrics
	 */

	public static double getMAverageRecall() {
		return averageRecall;
	}

	public static double getMAveragePrecision() {
		return averagePrecision;
	}

	public static long getMAverageDeliveryDelay() {
		return averageDeliveryDelay;
	}

	public static double getMAverageDeliveryCounter() {
		return averageDeliveryCounter;
	}

	public static double getMDeliveryToUninterestedCounter() {
		return averageDeliveryToUninterested;
	}

	public static double getMAverageMatchComplexity() {
		return averageMatchComplexity;
	}

	public static double getMDeliveryRecall() {
		return averageRecall;
	}

	private static void updateAverageMetrics(
			PublicationResultCollector newestCollector) {
		if (!newestCollector.isValid()) {
			return;
		}

		double alpha = 0.99;
		double alphaN = 1 - alpha;

		// Avg. Recall (prev. Delivery Ratio) (exp. smoothed)
		if (averageRecall == -1) {
			averageRecall = newestCollector.getDeliveryRecall();
		}
		averageRecall = averageRecall * alpha + newestCollector.getDeliveryRecall() * alphaN;
		assert averageRecall >= 0 && averageRecall <= 1;

		// Avg. Precision (exp. smoothed), may be invalid (-1)
		if (newestCollector.getDeliveryPrecision() != -1) {
			if (averagePrecision == -1) {
				averagePrecision = newestCollector.getDeliveryPrecision();
			}
			averagePrecision = averagePrecision * alpha + newestCollector.getDeliveryPrecision() * alphaN;
			assert averagePrecision >= 0 && averagePrecision <= 1;
		}

		// Avg. Delivery Delay (exp. smoothed), may be invalid
		if (newestCollector.getAverageDeliveryDelay() != -1) {
			if (averageDeliveryDelay == -1) {
				averageDeliveryDelay = newestCollector.getAverageDeliveryDelay();
			}
			averageDeliveryDelay = (long) (averageDeliveryDelay * alpha
					+ newestCollector.getAverageDeliveryDelay() * alphaN);
			assert averageDeliveryDelay >= 0;
		}

		// Avg. Match complexity (exp. smoothed), may be invalid
		if (newestCollector.getMatchComplexity() != -1) {
			if (averageMatchComplexity == -1) {
				averageMatchComplexity = newestCollector.getMatchComplexity();
			}
			averageMatchComplexity = (double) (averageMatchComplexity * alpha
					+ newestCollector.getMatchComplexity() * alphaN);
			assert averageMatchComplexity >= 0;
		}

		// Avg. Delivery Counter (exp. smoothed)
		if (averageDeliveryCounter == -1) {
			averageDeliveryCounter = newestCollector.getAverageNumberOfDeliveriesPerSubscriber();
		}
		averageDeliveryCounter = averageDeliveryCounter * alpha
				+ newestCollector.getAverageNumberOfDeliveriesPerSubscriber() * alphaN;

		// Avg. Delivery to uninterested (exp. smoothed)
		if (averageDeliveryToUninterested == -1) {
			averageDeliveryToUninterested = newestCollector.getUnintendedReachedReceivers().size();
		}
		averageDeliveryToUninterested = averageDeliveryToUninterested * alpha
				+ ((double) newestCollector.getUnintendedReachedReceivers().size()) * alphaN;

		// Debugging: print statistics for each publication
		// if (!ENABLE_DAO)
		// System.out.println(newestCollector.toString());
	}

	@Override
	public void onForwardNotification(Host localhost,
			Notification notification, INodeID receivedFrom, int color) {
		PublicationResultCollector collector = getPublicationResultCollectorFor(notification);
		if (collector != null) {
			collector.onForwardNotification(localhost, notification, receivedFrom, color);
		}
	}

	@Override
	public void onDropNotification(Host localhost, Notification notification, INodeID receivedFrom, int color) {
		PublicationResultCollector collector = getPublicationResultCollectorFor(notification);
		if (collector != null) {
			collector.onDropNotification(localhost, notification, receivedFrom, color);
		}
	}

	@Override
	public void onMatchNotification(Host host, Notification n, SchemeName scheme, int numberOfSubscriptions) {
		PublicationResultCollector collector = getPublicationResultCollectorFor(n);
		if (collector != null) {
			collector.notificationWasMatched(scheme, numberOfSubscriptions);
		}
	}

	/**
	 * Access to the {@link PubSubGlobalKnowledgeNodeState} object of a given
	 * host.
	 * 
	 * @param comp
	 * @return
	 */
	public static PubSubGlobalKnowledgeNodeState getNodeState(
			PubSubComponent comp) {
		return nodeStates.get(comp);
	}

	@Override
	public void start() {
		// not needed
	}

	@Override
	public void stop(Writer out) {
		// not needed
	}

}
