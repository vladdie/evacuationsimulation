package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.argame;

import de.tud.kom.p2psim.api.application.Application;
import de.tud.kom.p2psim.api.common.SimHost;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.ARGameWorkload;

/**
 * Server-side application of the AR-GAME, including analayzing. Please note,
 * that this app does not publish. Furthermore, it is not included in the global
 * knowledge state of the usual pubsub workload app, which is why the
 * subscriptions issued by this app are not analyzed in the delivery ratio of a
 * notification.
 * 
 * @author Bjoern Richerzhagen
 * @version May 4, 2014
 */
public class ARGameServerApp implements Application, IPeerStatusListener,
		PubSubListener {
	
	private final SimHost host;
	
	private PubSubComponent cloud;

	private Subscription cloudSub;

	public ARGameServerApp(SimHost host) {
		this.host = host;
	}

	@Override
	public void initialize() {
		// Get and Bind Cloud-Component
		try {
			cloud = host.getComponent(PubSubComponent.class);
			cloud.addPeerStatusListener(this);

		} catch (ComponentNotAvailableException e) {
			throw new AssertionError(
					"This app has to run on a node providing a BypassCloudComponent!");
		}

		/*
		 * Register the areaIntersects-operator on all pubsub-nodes!
		 */
		for (Host host : Oracle.getAllHosts()) {
			try {
				host.getComponent(PubSubComponent.class).setOperatorImplementation(FilterOperator.INTERSECTS,
						AreaAttributeValue.class, new AreaIntersectsOperator(0.0, true));
			} catch (ComponentNotAvailableException e) {
				continue;
			}
		}
	}

	@Override
	public void shutdown() {
		// not used
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		assert source.equals(cloud);
		if (peerStatus == PeerStatus.PRESENT) {
			// Register as Subscriber for ALL Position Updates (no AREA filter)
			Topic posUpdates = cloud
					.createTopic(ARGameWorkload.TOPIC_POSITION_UPDATES_STRING);
			cloudSub = cloud.createSubscription(posUpdates);
			cloud.subscribe(cloudSub, this);
		}
	}

	@Override
	public void onNotificationArrived(Subscription matchedSubscription,
			Notification notification) {
		//
	}

	@Override
	public SimHost getHost() {
		return host;
	}

	/**
	 * Factory for the game server
	 * 
	 * @author Bjoern Richerzhagen
	 * @version May 4, 2014
	 */
	public static class Factory implements HostComponentFactory {

		@Override
		public HostComponent createComponent(Host host) {
			return new ARGameServerApp((SimHost) host);
		}

	}

}
