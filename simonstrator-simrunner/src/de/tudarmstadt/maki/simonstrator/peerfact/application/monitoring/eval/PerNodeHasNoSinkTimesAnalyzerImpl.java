package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedHashMap;

import de.tud.kom.p2psim.impl.simengine.Simulator;
import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.PerNodeHasNoSinkTimesAnalyzer;

/**
 * Analyzer to measure the time {@link CraterNodeComponent} is being a LEAF without knowing a sink (empty sink table).
 * Beside this this analyzer measures the average time the sink table of a LEAF needs to be refilled.
 * 
 * Metrics:
 * 
 * - Online Time per Node
 * 
 * - NoSinkTime per Node
 * 
 * - Average Refill Delay for sink table
 * 
 * 
 * @author Nils Richerzhagen
 * 
 */
public class PerNodeHasNoSinkTimesAnalyzerImpl implements PerNodeHasNoSinkTimesAnalyzer, EventHandler {

	/**
	 * Leaf without sink times; Refill Time
	 * 
	 * LEAF && isEmpty || LEAF && gotEmpty ==> startTimestamp; startTimestamp
	 * 
	 * LEAF && gotFilled ==> endTimestamp; endTimestamp
	 * 
	 * became SINK ==> endTimestamp if there; discard
	 * 
	 * went OFFLINE ==> endTimestamp if there; discard
	 */

	/*
	 * Intervals for "no sink times" and "online times"
	 */
	private HashMap<INodeID, Long> _currentOpenNoSinkTimeIntervals = new LinkedHashMap<INodeID, Long>();

	private HashMap<INodeID, Long> _currentOpenOnlineTimeIntervals = new LinkedHashMap<INodeID, Long>();

	private HashMap<INodeID, Long> perNodeNoSinkTime = new HashMap<INodeID, Long>();

	private HashMap<INodeID, Long> perNodeOnlineTime = new HashMap<INodeID, Long>();

	/*
	 * Intervals for refill delays per leaf
	 */
	private HashMap<INodeID, Long> _currentOpenRefillTimeIntervals = new LinkedHashMap<INodeID, Long>();

	private HashMap<INodeID, RefillDelayInfo> perNodeRefillTime = new HashMap<INodeID, RefillDelayInfo>();

	private final int _EVENT_ENABLE_DAO_AT = 2001;

	private boolean started = false;

	private boolean ENABLE_DAO = false;

	public PerNodeHasNoSinkTimesAnalyzerImpl() {
		DAO.addEntityClass(PerNodeNoSinkTimeMeasurement.class);
		DAO.addEntityClass(SinkTableRefillDelayMeasurement.class);
	}

	public void setEnableDao(boolean enableDao) {
		this.ENABLE_DAO = enableDao;
	}

	public void setTimeEnableDao(long timeEnableDao) {
		if (ENABLE_DAO) {
			Event.scheduleWithDelay(timeEnableDao, this, null, _EVENT_ENABLE_DAO_AT);
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// _EVENT_ENABLE_DAO_AT
		assert ENABLE_DAO;
		if (type == _EVENT_ENABLE_DAO_AT) {
			started = true;

			// As this can happen anytime in the simulation we have to iterate through the hosts and add the current
			// open intervals.
			for (Host host : Oracle.getAllHosts()) {
				try {
					CraterNodeComponent comp = host.getComponent(CraterNodeComponent.class);

					INodeID hostID = comp.getHost().getId();
					long currentTime = Time.getCurrentTime();
					/*
					 * Online empty sink table and LEAF.
					 */
					if (comp.sinkTableIsEmpty() && comp.getCraterNodeRole() == CraterNodeRole.LEAF) {
						// Interval for "no sink times"
						_currentOpenNoSinkTimeIntervals.put(hostID, currentTime);

						// Interval for refill delays per leaf
						_currentOpenRefillTimeIntervals.put(hostID, currentTime);
					}
					/*
					 * Online Time.
					 */
					if (comp.getCraterNodeRole() != CraterNodeRole.OFFLINE && comp.isPresent()) {
						_currentOpenOnlineTimeIntervals.put(hostID, currentTime);
					}
				} catch (ComponentNotAvailableException e) {
					// Cloud or other don't care.
				}
			}
		}
	}

	@Override
	public void start() {
		// Not needed! Done by scheduled event.
	}

	@Override
	public void onSinkTableGotEmpty(CraterNodeComponent comp) {
		if (!started)
			return;

		INodeID hostID = comp.getHost().getId();

		if (comp.getCraterNodeRole() == CraterNodeRole.LEAF) {
			long currentTime = Time.getCurrentTime();
			assert !_currentOpenNoSinkTimeIntervals.containsKey(hostID) : "There must be no currently open Interval!";

			_currentOpenNoSinkTimeIntervals.put(hostID, currentTime);

			assert !_currentOpenRefillTimeIntervals.containsKey(hostID);

			_currentOpenRefillTimeIntervals.put(hostID, currentTime);
		}
	}

	@Override
	public void onSinkTableGotFilled(CraterNodeComponent comp) {
		if (!started)
			return;

		/*
		 * Sink Table got filled and node is a LEAF - end timestamp
		 * 
		 * There should be an entry in the currentOpenTimeIntervals as once the node became a Leaf the interval should
		 * be added when the sink table was empty back then.
		 * 
		 * There should be an entry in the currentOpenRefillIntervals as once the node became a Leaf the interval should
		 * be added when the sink table was empty back then.
		 * 
		 */
		if (comp.getCraterNodeRole() == CraterNodeRole.LEAF) {
			INodeID hostID = comp.getHost().getId();
			assert _currentOpenNoSinkTimeIntervals.containsKey(hostID);

			long startTime = _currentOpenNoSinkTimeIntervals.remove(hostID);
			long intervalDuration = Simulator.getCurrentTime() - startTime;

			if (perNodeNoSinkTime.containsKey(hostID)) {
				long currentNoSinkTime = perNodeNoSinkTime.get(hostID);
				currentNoSinkTime += intervalDuration;
				perNodeNoSinkTime.put(hostID, currentNoSinkTime);
			} else {
				perNodeNoSinkTime.put(hostID, intervalDuration);
			}


			assert _currentOpenRefillTimeIntervals.containsKey(hostID);

			startTime = _currentOpenRefillTimeIntervals.remove(hostID);
			intervalDuration = Simulator.getCurrentTime() - startTime;

			if (perNodeRefillTime.containsKey(hostID)) {
				RefillDelayInfo currentRefillInfo = perNodeRefillTime.get(hostID);
				currentRefillInfo.addRefillDelayInterval(intervalDuration);
				perNodeRefillTime.put(hostID, currentRefillInfo);
			} else {
				perNodeRefillTime.put(hostID, new RefillDelayInfo(intervalDuration));
			}
		}
	}

	@Override
	public void onCraterNodeRoleChanged(CraterNodeComponent comp, CraterNodeRole craterNodeRole) {
		if (!started) {
			return;
		}

		INodeID hostID = comp.getHost().getId();

		switch (craterNodeRole) {
		case LEAF:
			assert !_currentOpenNoSinkTimeIntervals
					.containsKey(hostID) : "There must not be an open interval for the no sink times at this point!";

			assert !_currentOpenRefillTimeIntervals
					.containsKey(hostID) : "There must not be an open interval for the refill times at this point!";

			long currentTime = Time.getCurrentTime();

			// Became Leaf and is Empty
			if (comp.sinkTableIsEmpty()) {
				_currentOpenNoSinkTimeIntervals.put(hostID, currentTime);

				_currentOpenRefillTimeIntervals.put(hostID, currentTime);
			}

			// Maybe went online (check for included) - if not start offline time.
			if (!_currentOpenOnlineTimeIntervals.containsKey(hostID)) {
				_currentOpenOnlineTimeIntervals.put(hostID, currentTime);
			}

			break;
		case SINK:
			// Became Sink --> remove if contained
			if (_currentOpenNoSinkTimeIntervals.containsKey(hostID)) {
				long startTime = _currentOpenNoSinkTimeIntervals.remove(hostID);
				long intervalDuration = Time.getCurrentTime() - startTime;

				if (perNodeNoSinkTime.containsKey(hostID)) {
					long currentNoSinkTime = perNodeNoSinkTime.get(hostID);
					currentNoSinkTime += intervalDuration;
					perNodeNoSinkTime.put(hostID, currentNoSinkTime);
				} else {
					perNodeNoSinkTime.put(hostID, intervalDuration);
				}
			}

			// Maybe went online (check for included) - if not start offline time.
			if (!_currentOpenOnlineTimeIntervals.containsKey(hostID)) {
				_currentOpenOnlineTimeIntervals.put(hostID, Time.getCurrentTime());
			}

			_currentOpenRefillTimeIntervals.remove(hostID);

			break;
		case OFFLINE:
			// Went Offline --> remove if contained
			if (_currentOpenNoSinkTimeIntervals.containsKey(hostID)) {
				long startTime = _currentOpenNoSinkTimeIntervals.remove(hostID);
				long intervalDuration = Time.getCurrentTime() - startTime;

				if (perNodeNoSinkTime.containsKey(hostID)) {
					long currentNoSinkTime = perNodeNoSinkTime.get(hostID);
					currentNoSinkTime += intervalDuration;
					perNodeNoSinkTime.put(hostID, currentNoSinkTime);
				} else {
					perNodeNoSinkTime.put(hostID, intervalDuration);
				}
			}

			assert _currentOpenOnlineTimeIntervals
					.containsKey(hostID) : "There must be an open interval, as the node must be online before.";

			long startTime = _currentOpenOnlineTimeIntervals.remove(hostID);
			long intervalDuration = Time.getCurrentTime() - startTime;
			if (perNodeOnlineTime.containsKey(hostID)) {
				long currentOnlineTime = perNodeOnlineTime.get(hostID);
				currentOnlineTime += intervalDuration;
				perNodeOnlineTime.put(hostID, currentOnlineTime);
			} else {
				perNodeOnlineTime.put(hostID, intervalDuration);
			}

			_currentOpenRefillTimeIntervals.remove(hostID);

			break;
		default:
			throw new AssertionError("Unknown case.");
		}
	}

	@Override
	public void stop(Writer out) {
		if (ENABLE_DAO) {

			/*
			 * Put last elements
			 */
			for (INodeID actID : _currentOpenNoSinkTimeIntervals.keySet()) {
				long startTime = _currentOpenNoSinkTimeIntervals.get(actID);
				long intervalDuration = Simulator.getCurrentTime() - startTime;

				if (perNodeNoSinkTime.containsKey(actID)) {
					long currentNoSinkTime = perNodeNoSinkTime.get(actID);
					currentNoSinkTime += intervalDuration;
					perNodeNoSinkTime.put(actID, currentNoSinkTime);
				} else {
					perNodeNoSinkTime.put(actID, intervalDuration);
				}
			}

			/*
			 * Put last elements
			 */
			for (INodeID actID : _currentOpenOnlineTimeIntervals.keySet()) {
				long startTime = _currentOpenOnlineTimeIntervals.get(actID);
				long intervalDuration = Simulator.getCurrentTime() - startTime;

				if (perNodeOnlineTime.containsKey(actID)) {
					long currentOnlineTime = perNodeOnlineTime.get(actID);
					currentOnlineTime += intervalDuration;
					perNodeOnlineTime.put(actID, currentOnlineTime);
				} else {
					perNodeOnlineTime.put(actID, intervalDuration);
				}
			}

			/*
			 * Put last elements
			 */
			for (INodeID actID : _currentOpenRefillTimeIntervals.keySet()) {
				long startTime = _currentOpenRefillTimeIntervals.get(actID);
				long intervalDuration = Simulator.getCurrentTime() - startTime;

				if (perNodeRefillTime.containsKey(actID)) {
					RefillDelayInfo currentRefillInfo = perNodeRefillTime.get(actID);
					currentRefillInfo.addRefillDelayInterval(intervalDuration);
					perNodeRefillTime.put(actID, currentRefillInfo);
				} else {
					perNodeRefillTime.put(actID, new RefillDelayInfo(intervalDuration));
				}
			}

			for (INodeID actId : perNodeNoSinkTime.keySet()) {

				/*
				 * Per Node Time being online without sink!
				 */
				long timeOnlineWithoutSink = perNodeNoSinkTime.get(actId);
				long timeOnline = perNodeOnlineTime.remove(actId);

				assert timeOnline >= timeOnlineWithoutSink;

				PerNodeNoSinkTimeMeasurement currentMeasurement = new PerNodeNoSinkTimeMeasurement(actId.value(),
						timeOnlineWithoutSink, timeOnline);
				MeasurementDAO.storeCustomMeasurement(PerNodeNoSinkTimeMeasurement.PER_NODE_NO_SINK_TIME_METRIC, actId.value(),
						Simulator.getCurrentTime(), currentMeasurement);

				// System.out.println("Node " + actId.value() + " noSinkTime: " +
				// Time.getFormattedTime(timeOnlineWithoutSink)
				// + " online Time " + Time.getFormattedTime(timeOnline));
				Monitor.log(PerNodeHasNoSinkTimesAnalyzer.class, Level.INFO, "Node " + actId.value() + " noSinkTime: "
						+ Time.getFormattedTime(timeOnlineWithoutSink) + " online Time " + Time.getFormattedTime(timeOnline));
			}

			/*
			 * Intervals left for online time?
			 */
			if (!perNodeOnlineTime.isEmpty()) {
				for (INodeID actId : perNodeOnlineTime.keySet()) {
					assert !perNodeNoSinkTime.containsKey(actId);

					PerNodeNoSinkTimeMeasurement currentMeasurement = new PerNodeNoSinkTimeMeasurement(actId.value(), 0,
							perNodeOnlineTime.get(actId));
					MeasurementDAO.storeCustomMeasurement(PerNodeNoSinkTimeMeasurement.PER_NODE_NO_SINK_TIME_METRIC,
							actId.value(), Simulator.getCurrentTime(), currentMeasurement);

					// System.out.println("Node " + actId.value() + " noSinkTime: 0 online Time "
					// + Time.getFormattedTime(perNodeOnlineTime.get(actId)));
					Monitor.log(PerNodeHasNoSinkTimesAnalyzer.class, Level.INFO, "Node " + actId.value()
							+ " noSinkTime: 0 online Time " + Time.getFormattedTime(perNodeOnlineTime.get(actId)));
				}
			}

			/*
			 * Intervals for refill times
			 */

			for (INodeID actId : perNodeRefillTime.keySet()) {
				/*
				 * Per Node refill time!
				 */
				RefillDelayInfo actRefillDelayInfo = perNodeRefillTime.get(actId);

				SinkTableRefillDelayMeasurement currentMeasurement = new SinkTableRefillDelayMeasurement(actId.value(),
						actRefillDelayInfo.getAverageRefillDelay());
				MeasurementDAO.storeCustomMeasurement(SinkTableRefillDelayMeasurement.SINK_TABLE_REFILL_DELAY_METRIC,
						actId.value(), Simulator.getCurrentTime(), currentMeasurement);

				// System.out.println("Node " + actId.value() + " avg. refill delay: "
				// + Time.getFormattedTime(actRefillDelayInfo.getAverageRefillDelay()));
				Monitor.log(PerNodeHasNoSinkTimesAnalyzer.class, Level.INFO, "Node " + actId.value() + " avg. refill delay: "
						+ Time.getFormattedTime(actRefillDelayInfo.getAverageRefillDelay()));
			}

		}

	}

	/**
	 * Used for average refill delay estimations. By adding finalized intervals the class counts the number of added
	 * intervals and returns the average interval.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	private class RefillDelayInfo {
		private long overallRefillDelayTime = 0;
		private int countRefillDelays = 0;

		public RefillDelayInfo(long refillDelayInterval) {
			this.overallRefillDelayTime = refillDelayInterval;
			this.countRefillDelays++;
		}

		public void addRefillDelayInterval(long refillDelayInterval) {
			this.overallRefillDelayTime += refillDelayInterval;
			this.countRefillDelays++;
		}

		public long getAverageRefillDelay() {
			return overallRefillDelayTime / countRefillDelays;
		}
	}

}
