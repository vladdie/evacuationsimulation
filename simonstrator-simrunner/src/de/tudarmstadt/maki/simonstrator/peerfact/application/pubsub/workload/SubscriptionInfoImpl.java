package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.SubscriptionInfo;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.SubscriptionResultCollector;

/**
 * Simple {@link SubscriptionInfo} container to hold a
 * {@link SubscriptionResultCollector}.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class SubscriptionInfoImpl implements SubscriptionInfo {

	/**
	 * Allows the evaluation toolchain to store state with a subscription.
	 */
	public SubscriptionResultCollector resultCollector;

	private boolean isActive = false;

	private final INodeID originatorId;

	private final long timestampOfCreation;

	public SubscriptionInfoImpl(INodeID originatorId) {
		this.originatorId = originatorId;
		this.timestampOfCreation = Time.getCurrentTime();
	}

	@Override
	public boolean isActive() {
		return isActive;
	}

	public void markAsActive() {
		assert !isActive;
		isActive = true;
	}

	public void markAsInactive() {
		assert isActive;
		isActive = false;
	}

	@Override
	public long getOriginatorHostId() {
		return originatorId.value();
	}

	@Override
	public long getTimestampOfCreation() {
		return timestampOfCreation;
	}

}