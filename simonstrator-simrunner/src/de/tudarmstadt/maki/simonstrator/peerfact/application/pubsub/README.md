# Pub/Sub Workloads

(work in progress)

## REST Interface for Demonstrations

This version of the simrunner provides a REST-interface that allows manipulation of relevant simulation data.
It is intended for usage within demonstrations, such as the [Subscription-Scheme Demo](http://www.kom.tu-darmstadt.de/research-results/publications/publications-details/publications/RRZ%2B16-2/).

To enable a local webserver listening on port `8080`, just start simulations via the `GUIRestRunner` instead of the usual `GuiRunner`.

The base URL of the service is `http://localhost:8080/simonstrator/`, with applications and services residing in their own namespaces at this path.
To interact with the pub/sub REST API as offered in `PubSubAppRESTHandler`, simply open `http://localhost:8080/simonstrator/pubsub/`.

### API Documentation

All API calls assume the base URL of `http://localhost:8080/simonstrator`.

**/status**: A test-case for [Server Sent Events](https://jersey.java.net/documentation/latest/sse.html) to allow frequent client updates (later, we need to export live stats via sub APIs of this call).

**/hosts/[ID]**: Allows interaction with the specific host. The reserved ID `all` triggers the respective action for all hosts. Current implementations are just a proof of concept.

