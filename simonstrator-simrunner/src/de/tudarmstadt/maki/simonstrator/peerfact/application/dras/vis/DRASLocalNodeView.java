package de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.UIManager;

import de.tud.kom.p2psim.impl.topology.movement.modularosm.mapvisualization.IMapVisualization;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.peerfact.application.dras.eval.DRASLocalAnalyzerImpl;

/**
 * Visualisation class for the monitoring of a single node
 */
public class DRASLocalNodeView extends JComponent {
	private BufferedImage image;

	private long id;
	private static int panelSize = 400;

	private LocationSensor locSensor;

	/**
	 * The map visualisation class which is used
	 */
	private static IMapVisualization mapVisualization;

	/**
	 * Used to display the map for this component. Contains only the image of the map.
	 */
	private JComponent localMapVis;

	private boolean threadFlag = false;
	private Point lastDif = new Point(0, 0);

	/**
	 * Create a new DRASLocalNodeView
	 * 
	 * @param id
	 *            ID of the node which is monitored
	 * @param parent
	 *            {@link DRASLocalMonitor} which contains this view.
	 */
	public DRASLocalNodeView(long id, DRASLocalMonitor parent) {
		this.id = id;
		setSize(panelSize, panelSize);

		JButton close = new JButton(UIManager.getIcon("InternalFrame.closeIcon"));
		close.setBounds(panelSize - 30, 10, 20, 20);
		close.addActionListener(e -> {
			try {
				// Single Node is removed from monitoring
				VisualizationTopologyView.VisualizationInjector.getTopologyView().toggleNodeClicked(INodeID.get(id), false);
				parent.removeNodeOnClick(id);
			} catch (NullPointerException npe) {
				// Area has been removed and TopologyView is (naturally) not available.
				// Don't worry, we dont need it.
				DRASLocalAnalyzerImpl.selectComp.areaOnClickRemoved(id);
			}

		});

		try {
			locSensor = Oracle.getHostByID(INodeID.get(id)).getComponent(LocationSensor.class);
		} catch (ComponentNotAvailableException e1) {
			throw new AssertionError(e1);
		} catch (NullPointerException npe)
		{
			//Location sensor is not available for areas
			locSensor = null;
		}

		// Thread used to trigger update of the visualisation of the movement of the monitored node
		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				// TopologyComponentVis nodeVis = VisualizationTopologyView.VisualizationInjector.getTopologyView().
				// .getCompVisOfHost(INodeID.get(id));

				// Point pos = new Point(0, 0);
				double oldX = 0;
				double oldY = 0;
				threadFlag = locSensor != null;
				while (threadFlag) {
					Location newPos = locSensor.getLastLocation();

					// Only if the position changed
					if (newPos.getLongitude() != oldX || newPos.getLatitude() != oldY) {
						oldX = newPos.getLongitude();
						oldY = newPos.getLatitude();
						// pos = newPos.asPoint();
						repaint();
					}
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		t.start();

		localMapVis = new JComponent() {
			BufferedImage localMap = mapVisualization.getMapImage();

			@Override
			protected void paintComponent(Graphics g) {
				assert localMap != null : "Used implementation of IMapVisualization does not support getMapImage() Method!";
				super.paintComponent(g);
				g.drawImage(localMap, 0, 0, null);

			}
		};

		localMapVis.setSize(VisualizationTopologyView.VisualizationInjector.getWorldX(),
				VisualizationTopologyView.VisualizationInjector.getWorldY());

		add(localMapVis);
		add(close);

		image = new BufferedImage(panelSize, panelSize, BufferedImage.TYPE_INT_ARGB);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2 = (Graphics2D) image.getGraphics();

		g2.setColor(Color.RED);
		g2.drawString("Node " + id, 15, 15);
		g2.drawRect(0, 0, panelSize - 1, panelSize - 1);

		g.drawImage(image, 0, 0, null);
		g2.dispose();

		// Draw movement of the monitored node

		if(locSensor != null)
		{
			Location pos = locSensor.getLastLocation();
			g.setColor(Color.MAGENTA);
			g.fillOval((int) pos.getLongitude() - lastDif.x - 4, (int) pos.getLatitude() - lastDif.y - 4, 8, 8);
		}

	}

	/**
	 * Updates the view of this node.
	 * 
	 * @param noUMTSLocations
	 *            updated locations without connectivity
	 * @param netAvailableLocations
	 *            updated locations with connectivity
	 */
	public void update(List<Map.Entry<UniqueID, Location>> noUMTSLocations,
			List<Map.Entry<UniqueID, Location>> netAvailableLocations) {
		Graphics2D g2 = image.createGraphics();
		g2.setBackground(new Color(255, 255, 255, 0));
		g2.clearRect(0, 0, panelSize, panelSize);
		g2.setColor(Color.BLACK);

		Point center = noUMTSLocations != null && noUMTSLocations.size() > 0 ? calculateCenter(noUMTSLocations)
				: calculateCenter(netAvailableLocations);
		int xDif = 0, yDif = 0;

		if (center.x > panelSize / 2)
			xDif = center.x - panelSize / 2;
		if (center.y > panelSize / 2)
			yDif = center.y - panelSize / 2;

		lastDif = new Point(xDif, yDif);

		// Shift the map visualisation to match the monitored area
		localMapVis.setLocation(-xDif, -yDif);
		revalidate();

		// Drawing of the location, centered to the current view, so we don't have
		// have to visualise the whole world.
		final int finalXDif = xDif;
		final int finalYDif = yDif;
		if (noUMTSLocations != null) {
			noUMTSLocations.stream().forEach(l -> g2.fillOval((int) l.getValue().getLongitude() - finalXDif - 3,
					(int) l.getValue().getLatitude() - finalYDif - 3, 6, 6));
		}

		g2.setColor(Color.RED);
		if (netAvailableLocations != null) {
			netAvailableLocations.stream().forEach(l -> g2.fillOval((int) l.getValue().getLongitude() - finalXDif - 3,
					(int) l.getValue().getLatitude() - finalYDif - 3, 6, 6));
		}
		repaint();

	}

	/**
	 * Calculates the center of the given data set. Center is this context means the location in the middle of the
	 * outermost locations of this data set
	 * 
	 * @param locations
	 *            positions of which the center should be calculated
	 * @return the center of the given data set
	 */
	private Point calculateCenter(List<Map.Entry<UniqueID, Location>> locations) {
		try {
			List<Location> loc = locations.stream().map(Map.Entry::getValue).collect(Collectors.toList());
			double xMin = loc.stream().mapToDouble(Location::getLongitude).min().getAsDouble();
			double xMax = loc.stream().mapToDouble(Location::getLongitude).max().getAsDouble();

			double yMin = loc.stream().mapToDouble(Location::getLatitude).min().getAsDouble();
			double yMax = loc.stream().mapToDouble(Location::getLatitude).max().getAsDouble();

			return new Point((int) (xMax + xMin) / 2, (int) (yMax + yMin) / 2);
		} catch (NoSuchElementException nse) {
			return new Point(0, 0);
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(panelSize, panelSize);
	}

	public static void setPanelSize(int size) {
		panelSize = size;
	}

	public static void setIMapVisualization(IMapVisualization map) {
		mapVisualization = map;
	}

	/**
	 * Stops this component. Should be used before removing and therefore dereferencing this component to prevent leaks!
	 */
	public void onStop() {
		threadFlag = false;
	}

}
