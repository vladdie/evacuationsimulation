/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.vis;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Path2D;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.dyn4j.geometry.Vector2;

import de.tud.kom.p2psim.impl.topology.PositionVector;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView.VisualizationInjector;
import de.tud.kom.p2psim.impl.topology.views.visualization.ui.InteractiveVisualizationComponent;
import de.tud.kom.p2psim.impl.topology.views.visualization.ui.VisHelper;
import de.tud.kom.p2psim.impl.topology.views.visualization.world.NodeInfoComponentVis;
import de.tud.kom.p2psim.impl.topology.views.visualization.world.NodeVisInteractionListener;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BrokerSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction.AttractionSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction.AttractionSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid.ExtGridSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.SubscriptionGridCell;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste.SpaceTimeEnvelope;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste.SpaceTimeEnvelopeSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste.SpaceTimeEnvelopeSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubAppListener;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubWorkloadApp;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PubSubGlobalKnowledge;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PubSubGlobalKnowledgeNodeState;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PubSubResultPersisting;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PubSubResultPersistingLiveStats;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PublicationResultCollector;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PublicationResultCollector.ForwarderInfo;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.SubscriptionResultCollector.SubscriptionResultSnapshot;
import de.tudarmstadt.maki.simonstrator.service.transition.local.LocalTransitionEngine;

/**
 * This component draws the visualization layer for the Pub/Sub Workload-App
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Sep 18, 2013
 */
public class PubSubAppVisualization extends JComponent
		implements NodeVisInteractionListener, InteractiveVisualizationComponent {

	private static final long serialVersionUID = 1L;

	private static PubSubAppVisualization instance = null;

	/**
	 * Options for the menu
	 */
	private final JMenu menu = new JMenu("Pub/Sub App");

	private final ConcurrentHashMap<Long, PublicationFlowVis> activePubVis = new ConcurrentHashMap<Long, PublicationFlowVis>();

	private final ConcurrentHashMap<Long, SubscriptionMetricVis> activeSubVis = new ConcurrentHashMap<>();
	
	private final LinkedHashSet<INodeID> activeHosts = new LinkedHashSet<>();

	private Map<Long, PubSubWorkloadApp> hostIdToApp = null;

	private List<PubSubWorkloadApp> apps = null;

	protected boolean showNodes = false;

	protected boolean showNodeType = false;

	protected boolean showActions = false;

	protected boolean showPublicationTargets = true;

	protected boolean showForwardedMessages = true;

	protected boolean showDiscardedMessages = false;

	private Map<Long, NodeVis> nodeVisualizations = null;

	private BrokerVis brokerVisualization = null;

	public static boolean ENABLE_DEMO_MODE = false;

	protected final static Color COLOR_PUB_OK = Color.GREEN;

	protected final static Color COLOR_PUB_DROP = Color.GRAY;

	protected final static Color COLOR_PUB_WRONG = Color.RED;

	private PubSubAppVisualization() {
		setBounds(0, 0, VisualizationInjector.getWorldX(), VisualizationInjector.getWorldY());
		setOpaque(true);
		setVisible(true);
		// Singleton
		// checkBoxes.add(createCheckboxNodeType()); // unused
		menu.add(createCheckboxActions());
		menu.add(createCheckboxForwardedMessages());
		menu.add(createCheckboxDiscardedMessages());
		menu.add(createCheckboxNodes());
		menu.add(createCheckboxPublicationTargets());
		VisualizationInjector.addInteractionListener(this);

		NodeInfoComponentVis vis = new NodeInfoComponentVis(BypassClientComponent.class);
		vis.setHideInactiveNodes(true);
		VisualizationInjector.injectComponent(vis);
	}

	public static PubSubAppVisualization getInstance() {
		if (instance == null) {
			instance = new PubSubAppVisualization();
		}
		return instance;
	}

	/**
	 * This checkbox enables the visualization of node interactions with the
	 * overlay (i.e., if a node publishes/subscribes or unsubscribes)
	 * 
	 * @return
	 */
	private JCheckBoxMenuItem createCheckboxNodes() {
		final JCheckBoxMenuItem checkBox = new JCheckBoxMenuItem("show nodes", showNodes);
		checkBox.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				showNodes = checkBox.isSelected();
				VisualizationInjector.invalidate();
			}
		});
		return checkBox;
	}

	/**
	 * Show target locations of publications.
	 * 
	 * @return
	 */
	private JCheckBoxMenuItem createCheckboxPublicationTargets() {
		final JCheckBoxMenuItem checkBox = new JCheckBoxMenuItem("show targets", showPublicationTargets);
		checkBox.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				showPublicationTargets = checkBox.isSelected();
				VisualizationInjector.invalidate();
			}
		});
		return checkBox;
	}

	/**
	 * This checkbox enables the visualization of node interactions with the
	 * overlay (i.e., if a node publishes/subscribes or unsubscribes)
	 * 
	 * @return
	 */
	private JCheckBoxMenuItem createCheckboxActions() {
		final JCheckBoxMenuItem checkBox = new JCheckBoxMenuItem("show actions", showActions);
		checkBox.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				showActions = checkBox.isSelected();
				VisualizationInjector.invalidate();
			}
		});
		return checkBox;
	}

	/**
	 * This checkbox enables the visualization of node interactions with the
	 * overlay (i.e., if a node publishes/subscribes or unsubscribes)
	 * 
	 * @return
	 */
	private JCheckBoxMenuItem createCheckboxForwardedMessages() {
		final JCheckBoxMenuItem checkBox = new JCheckBoxMenuItem("show forwarded messages", showForwardedMessages);
		checkBox.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				showForwardedMessages = checkBox.isSelected();
				VisualizationInjector.invalidate();
			}
		});
		return checkBox;
	}

	/**
	 * This checkbox enables the visualization of node interactions with the
	 * overlay (i.e., if a node publishes/subscribes or unsubscribes)
	 * 
	 * @return
	 */
	private JCheckBox createCheckboxDiscardedMessages() {
		final JCheckBox checkBox = new JCheckBox("show discarded messages", showDiscardedMessages);
		checkBox.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				showDiscardedMessages = checkBox.isSelected();
				VisualizationInjector.invalidate();
			}
		});
		return checkBox;
	}

	protected List<PubSubWorkloadApp> getAllApps() {
		if (apps == null || apps.isEmpty()) {
			apps = new LinkedList<PubSubWorkloadApp>();
			for (Host host : Oracle.getAllHosts()) {
				try {
					PubSubWorkloadApp app = host
							.getComponent(PubSubWorkloadApp.class);
					apps.add(app);
				} catch (ComponentNotAvailableException e) {
					continue;
				}
			}
		}
		return apps;
	}

	protected PubSubWorkloadApp getAppForHostId(long hostId) {
		if (hostIdToApp == null) {
			hostIdToApp = new LinkedHashMap<Long, PubSubWorkloadApp>();
			for (Host host : Oracle.getAllHosts()) {
				try {
					PubSubWorkloadApp app = host
							.getComponent(PubSubWorkloadApp.class);
					hostIdToApp.put(host.getId().value(), app);
				} catch (ComponentNotAvailableException e) {
					continue;
				}
			}
		}
		return hostIdToApp.get(hostId);
	}

	protected PubSubWorkloadApp getAppForHostId(INodeID hostId) {
		return getAppForHostId(hostId.value());
	}

	protected Collection<NodeVis> getNodeVisualizations() {
		if (nodeVisualizations == null || nodeVisualizations.isEmpty()) {
			nodeVisualizations = new LinkedHashMap<Long, NodeVis>();
			for (PubSubWorkloadApp app : getAllApps()) {
				nodeVisualizations.put(app.getHost().getHostId(), new NodeVis(
						app));
			}
		}
		return nodeVisualizations.values();
	}

	/**
	 * The {@link NodeVis} object for a given host Id
	 * 
	 * @param hostId
	 *            as long
	 * @return
	 */
	public NodeVis getNodeVisualizationFor(long hostId) {
		if (nodeVisualizations == null || nodeVisualizations.isEmpty()) {
			getNodeVisualizations();
		}
		return nodeVisualizations.get(hostId);
	}

	protected BrokerVis getBrokerVisualization() {
		if (brokerVisualization == null) {
			for (Host host : Oracle.getAllHosts()) {
				try {
					BypassCloudComponent broker = host.getComponent(BypassCloudComponent.class);
					brokerVisualization = new BrokerVis(broker);
				} catch (ComponentNotAvailableException e) {
					//
				}

			}
		}
		return brokerVisualization;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		/*
		 * Show currently active publication flows for a given (selected) node
		 */
		for (PublicationFlowVis pVis : activePubVis.values()) {
			pVis.draw(g2);
		}
		
		for (SubscriptionMetricVis sVis : activeSubVis.values()) {
			sVis.draw(g2);
		}

		// Legend
		if (!activePubVis.isEmpty()) {
			g2.setColor(COLOR_PUB_OK);
			g2.fillRect(10, 1, 8, 8);
			g2.setColor(Color.DARK_GRAY);
			g2.drawString("Publication delivered", 20, 10);

			g2.setColor(COLOR_PUB_DROP);
			g2.fillRect(10, 11, 8, 8);
			g2.setColor(Color.DARK_GRAY);
			g2.drawString("Publication not delivered", 20, 20);

			g2.setColor(COLOR_PUB_WRONG);
			g2.fillRect(10, 21, 8, 8);
			g2.setColor(Color.DARK_GRAY);
			g2.drawString("Publication delivered to uninterested node", 20, 30);
		}

		// Include all per-node switches here and filter within NodeVis.draw
		Collection<NodeVis> nodes = getNodeVisualizations();
		for (NodeVis vis : nodes) {
			vis.draw(g2);
		}

		BrokerVis broker = getBrokerVisualization();
		if (broker != null) {
			broker.draw(g2);
		}

	}

	@Override
	public void onHostClick(long hostID, boolean isActive) {
		PubSubWorkloadApp app = getAppForHostId(hostID);
		if (app == null) {
			return;
		}
		/*
		 * Used to (for example) show the publications currently floating for a
		 * node.
		 */

		/*
		 * Create the flowVis and AppListener and bind it to the given host
		 */
		if (isActive) {
			PublicationFlowVis pubVis = new PublicationFlowVis(app);
			activePubVis.put(hostID, pubVis);
			activeSubVis.put(hostID, new SubscriptionMetricVis(INodeID.get(hostID)));
			activeHosts.add(app.getHost().getId());
			getNodeVisualizationFor(hostID).clicked = true;
			pubVis.activateListener();
		} else {
			PublicationFlowVis pubVis = activePubVis.remove(hostID);
			activeSubVis.remove(hostID);
			activeHosts.remove(app.getHost().getId());
			getNodeVisualizationFor(hostID).clicked = false;
			if (pubVis != null) {
				pubVis.removeListener();
			}
		}
	}

	public LinkedHashSet<INodeID> getActiveHosts() {
		return activeHosts;
	}

	@Override
	public JComponent getComponent() {
		return this;
	}

	@Override
	public JMenu getCustomMenu() {
		return menu;
	}

	@Override
	public JComponent getSidebarComponent() {
		if (!ENABLE_DEMO_MODE) {
			return null;
		}
		return PubSubAppVisSidebar.getInstance();
	}

	@Override
	public String getDisplayName() {
		return "Pub/Sub";
	}

	@Override
	public boolean isHidden() {
		return false;
	}

	/**
	 * Visualization-fragments for Node-centric visualization-information.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, Sep 22, 2013
	 */
	protected class NodeVis implements PubSubAppListener {

		private final PubSubWorkloadApp app;

		private long lastSubscription;

		private long lastPublication;

		private Location lastPublishedLocation;

		private long lastUnsubscribe;

		private long lastNotification;

		private boolean hasRestClient = false;

		private String restClientColorHex = "#0C7C59";

		private Color restClientColor = Color.decode("#0C7C59");

		private static final long DRAW_ACTIONS_TIMEOUT = 10 * Time.SECOND;

		private static final int CROSS_PADDING = 10;

		private Font font = new Font("Verdana", Font.BOLD, 12);

		protected boolean clicked = false;
				
		public NodeVis(PubSubWorkloadApp app) {
			this.app = app;
			this.app.addPubSubListener(this);
		}

		public void setRestClient(String colorHex) {
			this.hasRestClient = true;
			this.restClientColor = java.awt.Color.decode(colorHex);
			this.restClientColorHex = colorHex;
		}

		public void unsetRestClient() {
			this.hasRestClient = false;
			this.restClientColor = Color.decode("#0C7C59");
			this.restClientColorHex = "#0C7C59";
		}


		public void draw(Graphics2D g2) {
			if (app == null || app.getPubsub() == null || !app.getPubsub().isPresent()) {
				return;
			}
			
			Point center = app.getLocationAsPoint();
			long now = Time.getCurrentTime();
			
			if (hasRestClient) {
				g2.setColor(restClientColor);
				g2.setStroke(new BasicStroke(4.0f));
				g2.drawOval(center.x - 20, center.y - 20, 41, 41);
				g2.setFont(font);
				g2.drawString("" + app.getHost().getHostId(), center.x + 10, center.y - 20);
			}

			if (showNodes) {
				g2.setColor(restClientColor);
				g2.fillOval(center.x - 6, center.y - 6, 13, 13);
				g2.setColor(Color.WHITE);
				g2.setStroke(new BasicStroke(2.0f));
				g2.drawOval(center.x - 6, center.y - 6, 13, 13);
			}

			if (clicked) {
				if (lastPublishedLocation != null) {
					int x = (int) lastPublishedLocation.getLongitude();
					int y = (int) lastPublishedLocation.getLatitude();
					g2.setColor(Color.DARK_GRAY);
					g2.setStroke(new BasicStroke(4.0f));
					g2.drawLine(x - CROSS_PADDING, y - CROSS_PADDING, x + CROSS_PADDING, y + CROSS_PADDING);
					g2.drawLine(x - CROSS_PADDING, y + CROSS_PADDING, x + CROSS_PADDING, y - CROSS_PADDING);
					g2.setStroke(new BasicStroke(2.0f));
				}
			}

			if (showActions) {
				// Publications
				if (now - lastPublication <= DRAW_ACTIONS_TIMEOUT) {
					float percent = getPercentage(lastPublication, now);
					int radius = 8 + (int) (percent * 16);

					g2.setStroke(new BasicStroke(3 + percent * 16));
					g2.setColor(new Color(0.25f, 0.41f, 0.88f, Math.max(
							1 - percent, 0))); // blue
					g2.drawOval(center.x - radius, center.y - radius,
							radius * 2, radius * 2);
				}

				// Subscriptions
				if (now - lastSubscription <= DRAW_ACTIONS_TIMEOUT) {
					float percent = getPercentage(lastSubscription, now);
					int radius = 8 + (int) (percent * 16);

					g2.setStroke(new BasicStroke(3 + percent * 16));
					g2.setColor(new Color(1f, 0.50f, 0f, Math.max(
							0.7f - percent, 0))); // orange
					g2.drawOval(center.x - radius, center.y - radius,
							radius * 2, radius * 2);
				}

				// Received Notifications
				if (now - lastNotification <= DRAW_ACTIONS_TIMEOUT) {
					float percent = getPercentage(lastNotification, now);
					int radius = 8 + (int) (percent * 16);

					g2.setStroke(new BasicStroke(3 + percent * 16));
					g2.setColor(new Color(1f, 0.41f, 0.41f, Math.max(
							1 - percent, 0))); // red
					g2.drawOval(center.x - radius, center.y - radius,
							radius * 2, radius * 2);
				}
			}
		}

		private float getPercentage(long lastX, long now) {
			return Math.max(0,
					((float) (now - lastX) / (float) DRAW_ACTIONS_TIMEOUT));
		}

		@Override
		public void onNewSubscription(Subscription sub) {
			lastSubscription = Time.getCurrentTime();
		}

		@Override
		public void onNewPublication(Notification publication) {
			lastPublication = Time.getCurrentTime();
			if (publication instanceof LocationBasedNotification) {
				lastPublishedLocation = ((LocationBasedNotification) publication).getLocation();
			}
		}

		@Override
		public void onUnsubscribe(Subscription sub) {
			lastUnsubscribe = Time.getCurrentTime();
		}

		@Override
		public void onReceivedNotification(Subscription matchedSubscription,
				Notification notification) {
			lastNotification = Time.getCurrentTime();
		}

	}
	
	/**
	 * Visualizes information for a single node's subscription
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private class SubscriptionMetricVis {
		
		private final INodeID nodeId;

		public SubscriptionMetricVis(INodeID nodeId) {
			this.nodeId = nodeId;
			assert Monitor.hasAnalyzer(PubSubResultPersisting.class);
		}

		public void draw(Graphics2D g2) {
			SubscriptionResultSnapshot snapshot = PubSubResultPersistingLiveStats.getSnapshot(nodeId);
			if (snapshot != null) {
				markNodes(g2, snapshot.getTruePositivesSet(), new Color(0.25f, 0.41f, 0.88f, 0.8f));
				markNodes(g2, snapshot.getFalsePositivesSet(), new Color(1f, 0.41f, 0.41f, 0.8f));
				markNodes(g2, snapshot.getFalseNegativesSet(), new Color(1f, 0.50f, 0f, 0.8f));
			}
		}

		private void markNodes(Graphics2D g2, Set<INodeID> nodes, Color color) {
			g2.setColor(color);
			for (INodeID node : nodes) {
				Point loc = getAppForHostId(node).getLocationAsPoint();
				g2.fillRect(loc.x, loc.y, 10, 10);
			}
		}

	}

	/**
	 * Responsible for drawing the current state of a publication issued by a
	 * specific node.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, Sep 18, 2013
	 */
	private class PublicationFlowVis implements PubSubAppListener {

		private PublicationResultCollector collector = null;

		private long lastCollectorUpdate;

		private static final long COLLECTOR_DRAW_TIMEOUT = 2 * Time.MINUTE;

		private final PubSubWorkloadApp initiator;

		private final Color[] colors = { Color.BLUE, Color.GREEN, Color.RED,
				Color.CYAN, Color.DARK_GRAY };

		public PublicationFlowVis(PubSubWorkloadApp initiator) {
			this.initiator = initiator;
		}

		public void removeListener() {
			initiator.removePubSubListener(this);
		}

		public void activateListener() {
			initiator.addPubSubListener(this);
		}

		public void draw(Graphics2D g2) {
			if (collector != null
					&& lastCollectorUpdate + COLLECTOR_DRAW_TIMEOUT > Time
							.getCurrentTime()) {
				List<PubSubGlobalKnowledgeNodeState> gkReceivers = new LinkedList<>(
						collector.getGlobalKnowledgeReceivers());
				List<PubSubGlobalKnowledgeNodeState> reachedReceivers = new LinkedList<>(
						collector.getReachedReceivers());
				List<PubSubGlobalKnowledgeNodeState> unintendedReceivers = new LinkedList<>(
						collector.getUnintendedReachedReceivers());

				Map<PubSubGlobalKnowledgeNodeState, Double> delays = new LinkedHashMap<>(collector.getDeliveryDelays());

				Point pos;
				for (PubSubGlobalKnowledgeNodeState gkReceiver : gkReceivers) {
					pos = gkReceiver.getApp().getLocationAsPoint();
					if (reachedReceivers.contains(gkReceiver)) {
						g2.setColor(COLOR_PUB_OK);
						VisHelper.drawArrow(g2, initiator.getLocationAsPoint(),
								pos, 1);
						double deliveryDelay = delays.get(gkReceiver);
						PositionVector ptForText = gkReceiver.getApp()
								.getLocation()
								.moveStep(initiator.getLocation(), 10);
						g2.drawString(" "
								+ (int) (deliveryDelay / Time.MILLISECOND)
								+ " ms", (int) ptForText.getX(),
								(int) ptForText.getY());
					} else {
						g2.setColor(COLOR_PUB_DROP);
						VisHelper.drawArrow(g2, initiator.getLocationAsPoint(),
								pos, 1);
					}
				}
				for (PubSubGlobalKnowledgeNodeState unintended : unintendedReceivers) {
					pos = unintended.getApp().getLocationAsPoint();
					g2.setColor(COLOR_PUB_WRONG);
					VisHelper.drawArrow(g2, initiator.getLocationAsPoint(),
							pos, 1);
				}

				/*
				 * Draws all messages associated with the notification.
				 */
				if (showForwardedMessages || showDiscardedMessages) {
					LinkedList<ForwarderInfo> forwarders = new LinkedList<ForwarderInfo>(collector.getForwarders());
					PubSubWorkloadApp lastHop = null;
					for (ForwarderInfo hop : forwarders) {
						PubSubWorkloadApp currentHop = getAppForHostId(hop.to.getId());
						if (hop.from != null) {
							lastHop = getAppForHostId(hop.from);
						} else {
							lastHop = initiator;
						}
						// draw connection
						if (showForwardedMessages && !hop.drop) {
							g2.setColor(colors[hop.color]);
							VisHelper.drawArrow(g2, lastHop.getLocationAsPoint(), currentHop.getLocationAsPoint(),
									1.5f);
						}
						if (showDiscardedMessages && hop.drop) {
							g2.setColor(Color.LIGHT_GRAY);
							VisHelper.drawArrow(g2, lastHop.getLocationAsPoint(), currentHop.getLocationAsPoint(),
									0.5f);
						}
					}
				}
			}
		}

		@Override
		public void onNewSubscription(Subscription sub) {
			// don't care
		}

		@Override
		public void onNewPublication(Notification publication) {
			/*
			 * Possible source of a bug: this requires the global knowledge to
			 * be the first listener - this is a reasonable assumption.
			 */
			collector = PubSubGlobalKnowledge
					.getPublicationResultCollectorFor(publication);
			lastCollectorUpdate = Time.getCurrentTime();
			// Here, the collector is NEVER allowed to be null
			assert collector != null;
		}

		@Override
		public void onUnsubscribe(Subscription sub) {
			// don't care
		}

		@Override
		public void onReceivedNotification(Subscription matchedSubscription,
				Notification notification) {
			// don't care
		}
	}

	/**
	 * Visualization for subscription schemes
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private class BrokerVis {

		private final BypassCloudComponent broker;

		private LocalTransitionEngine tEngine;

		public BrokerVis(BypassCloudComponent broker) {
			this.broker = broker;
		}

		public void draw(Graphics2D g2) {

			if (!broker.isActive()) {
				return;
			}
			if (tEngine == null) {
				tEngine = (LocalTransitionEngine) broker.getTransitionEngine();
			}

			BrokerSubscriptionScheme scheme = tEngine._getMechanismInstance(
					BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER, BrokerSubscriptionScheme.class);


			switch (scheme.getName()) {
			case GRID:
				drawGrid(g2, tEngine._getMechanismInstance(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
						GridSchemeBroker.class).getStorage());
				break;

			case EXT_GRID:
				drawGrid(g2, tEngine._getMechanismInstance(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
						ExtGridSchemeBroker.class).getStorage());
				break;

			case EXT_GRID_RECT:
				drawGrid(g2, tEngine._getMechanismInstance(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
						ExtGridSchemeBroker.class).getStorage());
				break;

			case STE:
				drawSTE(g2, tEngine._getMechanismInstance(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
						SpaceTimeEnvelopeSchemeBroker.class).getStorage());
				break;
				
			case LBS:
				drawLBS(g2, tEngine._getMechanismInstance(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
						LocationBasedSchemeBroker.class).getStorage());
				break;

			case ATTRACTION:
				drawAttraction(g2, tEngine._getMechanismInstance(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
						AttractionSchemeBroker.class).getStorage());
				break;

			default:
				break;
			}
		}

		public void drawAttraction(Graphics2D g2, AttractionSubscriptionStorage storage) {
			for (OverlayContact subscriber : storage.getSubscribers()) {
				if (getActiveHosts().contains(subscriber.getNodeID())) {
					Map<AttractionPoint, Set<OverlayContact>> subs = storage.getAttractionPointSubscribers();
					for (Entry<AttractionPoint, Set<OverlayContact>> entry : subs.entrySet()) {
						if (entry.getValue().contains(subscriber)) {
							drawCircle(g2, entry.getKey(), (int) entry.getKey().getRadius(), Color.GREEN);
						}
					}
				}
			}
		}

		public void drawLBS(Graphics2D g2, LocationBasedSubscriptionStorage storage) {
			for (OverlayContact subscriber : storage.getSubscribers()) {
				if (getActiveHosts().contains(subscriber.getNodeID())) {
					double roi = 0;
					for (LocationBasedSubscription sub : storage.getLocationBasedSubscriptionsBy(subscriber)) {
						if (sub.getRadiusOfInterest() > roi) {
							roi = sub.getRadiusOfInterest();
						}
					}
					drawCircle(g2, storage.getLastKnownLocation(subscriber), (int) roi, Color.DARK_GRAY);
					drawCircle(g2, getAppForHostId(subscriber.getNodeID()).getLocation(), (int) roi, Color.LIGHT_GRAY);
				}
			}
		}

		public void drawSTE(Graphics2D g2, SpaceTimeEnvelopeSubscriptionStorage storage) {
			for (OverlayContact subscriber : storage.getSubscribers()) {
				if (getActiveHosts().contains(subscriber.getNodeID())) {
					SpaceTimeEnvelope ste = storage.getCachedSTE(subscriber);
					if (ste != null) {
						if (ste.getEnvelopeCone() != null) {
							Vector2[] vertices = ste.getEnvelopeCone().getVertices();
							int l = vertices.length;

							// create the awt polygon
							Path2D.Double p = new Path2D.Double();
							p.moveTo(vertices[0].x, vertices[0].y);
							for (int i = 1; i < l; i++) {
								p.lineTo(vertices[i].x, vertices[i].y);
							}
							p.closePath();

							// fill the shape
							g2.setColor(new Color(0, 100, 0, 50));
							g2.fill(p);
						}
						if (ste.getCenterCircle() != null) {
							drawCircle(g2, storage.getLastKnownLocation(subscriber),
									(int) ste.getCenterCircle().getRadius(), Color.DARK_GRAY);
							drawCircle(g2, getAppForHostId(subscriber.getNodeID()).getLocation(),
									(int) ste.getCenterCircle().getRadius(), Color.LIGHT_GRAY);
						}
					} else {
						drawCircle(g2, storage.getLastKnownLocation(subscriber), 4, Color.DARK_GRAY);
					}

				}
			}
		}

		public void drawCircle(Graphics2D g2, Location center, int radiusOfInterest, Color color) {
			if (center == null) {
				return;
			}
			g2.setColor(color);
			g2.drawOval((int) center.getLongitude() - radiusOfInterest, (int) center.getLatitude() - radiusOfInterest,
					radiusOfInterest * 2, radiusOfInterest * 2);
		}

		public void drawGrid(Graphics2D g2, GridBasedSubscriptionStorage storage) {
			for (SubscriptionGridCell cell : storage.getGridCells()) {
				BoundingRectangle rect = cell.getMBR();
				boolean subscribed = false;
				Set<OverlayContact> subscribers = cell.getSubscribers();
				for (OverlayContact subscriber : subscribers) {
					if (getActiveHosts().contains(subscriber.getNodeID())) {
						subscribed = true;
					}
				}
				drawBoundingRectangle(g2, rect, subscribed);
			}
		}

		public void drawBoundingRectangle(Graphics2D g2, BoundingRectangle rect, boolean subscribed) {
			int width = (int) rect.getWidth();
			int height = (int) rect.getHeight();
			if (subscribed) {
				g2.setColor(new Color(0, 100, 0, 50));
				g2.fillRect((int) rect.getCenterLong() - height / 2, (int) rect.getCenterLat() - width / 2, height,
						width);
			}
			g2.setColor(Color.GRAY);
			g2.drawRect((int) rect.getCenterLong() - height / 2, (int) rect.getCenterLat() - width / 2, height, width);
		}

	}

}
