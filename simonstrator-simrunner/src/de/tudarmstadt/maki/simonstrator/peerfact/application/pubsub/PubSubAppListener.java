/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;

/**
 * This Listener is informed, whenever an action happens at the application.
 * Actions include new publications and subscriptions as well as received
 * notifications.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Sep 17, 2013
 */
public interface PubSubAppListener {

	public void onNewSubscription(Subscription sub);

	public void onNewPublication(Notification publication);

	public void onUnsubscribe(Subscription sub);

	public void onReceivedNotification(Subscription matchedSubscription,
			Notification notification);

}
