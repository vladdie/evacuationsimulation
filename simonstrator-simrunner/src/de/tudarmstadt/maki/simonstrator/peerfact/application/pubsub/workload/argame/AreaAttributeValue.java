package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.argame;

import de.tud.kom.p2psim.impl.topology.PositionVector;
import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;

/**
 * Type for a location/area attribute with context-updates enabled (i.e.,
 * incoming notifications with the same nodeID update the subscription on
 * filtering)
 * 
 * FIXME -> current implementation of matches() requires those filters to be
 * executed as early as possible, as we fail fast.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, May 2, 2014
 */
public class AreaAttributeValue implements Transmitable, Cloneable {

	private static final long serialVersionUID = 1L;

	private double x;

	private double y;

	// Maybe needed for analyzing lateron?
	private transient PositionVector realPosition;

	private double radius;

	/**
	 * Until we have a generic API for context-based pub/sub, we update
	 * application-based using the Operation defined in
	 * {@link IntersectsOperator}. Therefore, this Attribute Value has to
	 * contain the hostID.
	 */
	private long hostId;

	@SuppressWarnings("unused")
	private AreaAttributeValue() {
		// for Kryo
	}

	public AreaAttributeValue(PositionVector pos, double radius, long hostId) {
		this.x = pos.getX();
		this.y = pos.getY();
		this.realPosition = pos;
		this.radius = radius;
		this.hostId = hostId;
	}

	public PositionVector getRealPosition() {
		return realPosition;
	}

	public double getRadius() {
		return radius;
	}

	public long getHostId() {
		return hostId;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void updatePosition(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int getTransmissionSize() {
		return 8 + 4 + 8; // position.getTransmissionSize();
	}

	@Override
	public AreaAttributeValue clone() {
		return new AreaAttributeValue(realPosition, radius, hostId);
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

}