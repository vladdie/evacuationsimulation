package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.NotificationInfo;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PublicationResultCollector;

/**
 * Notification Info - this object determines the size of a Notification in
 * simulation scenarios. Furthermore, it is heavily used for analyzing.
 * 
 * @author Bjoern Richerzhagen
 * @version May 4, 2014
 */
public class NotificationInfoImpl implements NotificationInfo {

	private final long payloadSize;

	private final long originatorHostId;

	private final long timestampOfCreation;

	private final Set<Long> hostIdsDeliveredTo;

	private final Map<Class<?>, Set<Long>> markers;

	private final long sequenceNumber;

	private static Map<Long, Long> hostSequenceNumbers = new LinkedHashMap<Long, Long>();

	/**
	 * Allows the evaluation toolchain to store state with a notification.
	 */
	public PublicationResultCollector resultCollector;

	public NotificationInfoImpl(long payloadSize, long originatorHostId) {
		this.payloadSize = payloadSize;
		this.originatorHostId = originatorHostId;
		this.timestampOfCreation = Time.getCurrentTime();
		this.hostIdsDeliveredTo = new HashSet<Long>();
		this.markers = new HashMap<Class<?>, Set<Long>>();
		if (hostSequenceNumbers.containsKey(originatorHostId)) {
			this.sequenceNumber = hostSequenceNumbers.get(originatorHostId);
		} else {
			this.sequenceNumber = 0;
		}
		hostSequenceNumbers.put(originatorHostId, this.sequenceNumber + 1);
	}

	/**
	 * Marks the notification as being delivered to the given host. Method
	 * returns false, if the notification is a duplicate.
	 * 
	 * @param hostId
	 * @return
	 */
	public boolean markAsDelivered(long hostId) {
		return this.hostIdsDeliveredTo.add(hostId);
	}

	@Override
	public void setMarker(Class<?> markerClass, long marker) {
		Set<Long> cMarkers = markers.get(markerClass);
		if (cMarkers == null) {
			cMarkers = new HashSet<Long>();
			markers.put(markerClass, cMarkers);
		}
		cMarkers.add(marker);
	}

	@Override
	public boolean hasMarker(Class<?> markerClass, long number) {
		return this.markers.containsKey(markerClass)
				&& this.markers.get(markerClass).contains(number);
	}

	@Override
	public long getNotificationPayloadSize() {
		return payloadSize;
	}

	@Override
	public long getOriginatorHostId() {
		return originatorHostId;
	}

	@Override
	public long getTimestampOfCreation() {
		return timestampOfCreation;
	}

	@Override
	public long getSequenceNumber() {
		return sequenceNumber;
	}

}
