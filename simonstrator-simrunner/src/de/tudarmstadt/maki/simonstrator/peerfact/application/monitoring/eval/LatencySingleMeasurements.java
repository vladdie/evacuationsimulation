package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;

@Entity
@Table(name = "latency_single_measurements")
public class LatencySingleMeasurements extends CustomMeasurement {

	public static final MetricDescription LATENCY_METRIC = new MetricDescription(DataRoutingAnalyzerImpl.class,
			"LatencySingleMeasurements",
			"Includes the single latency per packet or entry latencies of ContainerMsg, normal data, and dup sens data for orig to sink and orig to server.",
			"us");

	@SuppressWarnings("unused")
	private String origin;

	@SuppressWarnings("unused")
	private long latency;

	public LatencySingleMeasurements(String origin, long latency) {
		super();
		this.origin = origin;
		this.latency = latency;
	}
}
