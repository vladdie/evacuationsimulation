package de.tudarmstadt.maki.simonstrator.peerfact.application.dras.eval;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.stream.Collectors;

import de.tud.kom.p2psim.impl.topology.movement.modularosm.mapvisualization.IMapVisualization;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView;
import de.tud.kom.p2psim.impl.topology.views.visualization.world.NodeVisInteractionListener;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis.AreaSelectionListener;
import de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis.DRASLocalMonitor;
import de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis.DRASLocalNodeView;
import de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis.SelectionComponent;
import de.tudarmstadt.maki.simonstrator.service.disasterRegion.DRASLocalAnalyzer;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutPairedList;

/**
 * Implementation of the {@link DRASLocalAnalyzer}.
 */
public class DRASLocalAnalyzerImpl implements DRASLocalAnalyzer, NodeVisInteractionListener, EventHandler, AreaSelectionListener {
	private static DRASLocalAnalyzerImpl singleton;
	private DRASLocalMonitor view;

	private long updateInterval;
	private final int LOCAL_EVENT_UPDATE_ID = 2409;

	private HashMap<Long, Boolean> monitoredIDs;
	private HashMap<Rectangle, ArrayList<Long>> monitoredAreas;
	private HashMap<Long, TimeoutPairedList<UniqueID, Location>> noUMTSdataMap;
	private HashMap<Long, TimeoutPairedList<UniqueID, Location>> netAvailableDataMap;

	public static SelectionComponent selectComp;

	/**
	 * Since this analyzer is a singleton, this method can be used to retrieve a reference to the analyzer-object.
	 * 
	 * @return that analyzer singleton
	 */
	public static DRASLocalAnalyzerImpl getInstance() {
		if (singleton == null)
			singleton = new DRASLocalAnalyzerImpl();
		return singleton;
	}

	private DRASLocalAnalyzerImpl() {
		VisualizationTopologyView.VisualizationInjector.addInteractionListener(this);
		view = new DRASLocalMonitor(this);
		monitoredIDs = new HashMap<>();
		monitoredAreas = new HashMap<>();
		noUMTSdataMap = new HashMap<>();
		netAvailableDataMap = new HashMap<>();
	}

	@Override
	public void start() {
		Event.scheduleWithDelay(updateInterval, this, null, LOCAL_EVENT_UPDATE_ID);
	}

	@Override
	public void onNoUTMSLocationsChanged(TimeoutPairedList<UniqueID, Location> locations, UniqueID nodeID) {
		// ignore if this node is currently not monitored
		if (monitoredIDs.containsKey(nodeID.value())) {
			noUMTSdataMap.put(nodeID.value(), locations);

			// Set entry to true to indicate a change
			monitoredIDs.put(nodeID.value(), true);
		}

		Iterator<Map.Entry<Rectangle, ArrayList<Long>>> iter = monitoredAreas.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<Rectangle, ArrayList<Long>> elem = iter.next();
			if (elem.getValue().contains(nodeID.value())) {
				Long key = Integer.toUnsignedLong(elem.getKey().hashCode());

				TimeoutPairedList<UniqueID, Location> old = noUMTSdataMap.get(key);
				TimeoutPairedList<UniqueID, Location> aggregate = new TimeoutPairedList<>(locations.getTimeout());
				aggregate.merge(old);
				aggregate.merge(locations);
				noUMTSdataMap.put(key, aggregate);
				monitoredIDs.put(key, true);
			}
		}

	}

	@Override
	public void onNetAvailableLocationsChanged(TimeoutPairedList<UniqueID, Location> locations, UniqueID nodeID) {
		// ignore if this node is currently not monitored
		if (monitoredIDs.containsKey(nodeID.value())) {
			netAvailableDataMap.put(nodeID.value(), locations);

			// Set entry to true to indicate a change
			monitoredIDs.put(nodeID.value(), true);
		}

		Iterator<Map.Entry<Rectangle, ArrayList<Long>>> iter = monitoredAreas.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<Rectangle, ArrayList<Long>> elem = iter.next();
			if (elem.getValue().contains(nodeID.value())) {
				Long key = Integer.toUnsignedLong(elem.getKey().hashCode());

				TimeoutPairedList<UniqueID, Location> old = netAvailableDataMap.get(key);
				TimeoutPairedList<UniqueID, Location> aggregate = new TimeoutPairedList<>(locations.getTimeout());
				aggregate.merge(old);
				aggregate.merge(locations);
				netAvailableDataMap.put(key, aggregate);
				monitoredIDs.put(key, true);
			}
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// Update of the single monitored nodes
		Iterator<Map.Entry<Long, Boolean>> iter = monitoredIDs.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<Long, Boolean> elem = iter.next();
			TimeoutPairedList<UniqueID, Location> noUMTSElem = noUMTSdataMap.get(elem.getKey());
			TimeoutPairedList<UniqueID, Location> netAvailableElem = netAvailableDataMap.get(elem.getKey());

			boolean noUMTSChanged = noUMTSElem != null && noUMTSElem.hasChanged();
			boolean netAvailableChanged = netAvailableElem != null && netAvailableElem.hasChanged();

			if (elem.getValue() || netAvailableChanged || noUMTSChanged) {
				view.updateView(elem.getKey(), noUMTSElem != null ? noUMTSElem.getUnmodifiableList() : null,
						netAvailableElem != null ? netAvailableElem.getUnmodifiableList() : null);
			}
			elem.setValue(false);
		}

		// Update which nodes are inside the monitored rectangles
		Iterator<Map.Entry<Rectangle, ArrayList<Long>>> areaIterator = monitoredAreas.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry<Rectangle, ArrayList<Long>> elem = areaIterator.next();
			ArrayList<Long> old = elem.getValue();
			ArrayList<Long> newNodes = getNodesInRectangle(elem.getKey());
			ArrayList<Long> toRemove = old.stream().filter(l -> !newNodes.contains(l))
					.collect(Collectors.toCollection(ArrayList::new));
			monitoredAreas.put(elem.getKey(), newNodes);

			toRemove.forEach(l -> {
				if (!view.isMonitored(l)) {
					monitoredIDs.remove(l);
				}
			});
		}
		Event.scheduleWithDelay(updateInterval, this, null, LOCAL_EVENT_UPDATE_ID);
	}

	@Override
	public void onHostClick(long hostID, boolean isActive) {
		if (isActive) {
			monitoredIDs.put(hostID, false);
			view.addNodeView(hostID);
		} else {
			monitoredIDs.remove(hostID);
			view.removeNodeView(hostID);
		}
	}

	@Override
	public void areaAdded(Rectangle r) {
		ArrayList<Long> nodes = getNodesInRectangle(r);
		monitoredAreas.put(r, nodes);
		// Start monitoring the nodes in the specified area
		nodes.forEach(n -> {
			if (!monitoredIDs.containsKey(n)) {
				monitoredIDs.put(n, false);
			}
		});

		monitoredIDs.put(Integer.toUnsignedLong(r.hashCode()), false);
		view.addNodeView(r.hashCode());

	}

	@Override
	public void areaRemoved(Rectangle r) {
		monitoredAreas.get(r).forEach(n -> {
			// Remove nodes from monitoring only if they are not monitored by themself, too
			if (!view.isMonitored(n)) {
				monitoredIDs.remove(n);
			}
		});

		monitoredAreas.remove(r);
		monitoredIDs.remove(Integer.toUnsignedLong(r.hashCode()));
		view.removeNodeView(r.hashCode());
	}

	/**
	 * Return the node which positions are currently in the specified area.
	 * 
	 * @param r
	 *            the area to check
	 * @return list of the collected nodes
	 */
	private ArrayList<Long> getNodesInRectangle(Rectangle r) {

		ArrayList<Long> ids = new ArrayList<>();
		for (Host curHost : Oracle.getAllHosts()) {
			LocationSensor locSensor;
			try {
				locSensor = curHost.getComponent(LocationSensor.class);
			} catch (ComponentNotAvailableException e) {
				continue;
			}

			if (r.contains(locSensor.getLastLocation().getLongitude(), locSensor.getLastLocation().getLatitude())) {
				ids.add(curHost.getHostId());
			}
		}
		return ids;
	}

	/**
	 * Used by XML configuration file
	 * 
	 * @param size
	 *            size the {@link DRASLocalNodeView}s should have
	 */
	public void setPanelSize(int size) {
		view.setPanelSize(size);
	}

	/**
	 * Used by XML configuration file
	 * 
	 * @param interval
	 *            update interval
	 */
	public void setInterval(long interval) {
		updateInterval = interval;
	}

	/**
	 * Used by XML configuration file
	 * 
	 * @param mapVisualization
	 *            map visualisation to use
	 */
	public void setIMapVisualization(IMapVisualization mapVisualization) {
		DRASLocalNodeView.setIMapVisualization(mapVisualization);
	}

	/**
	 * Used by XML configuration file
	 * 
	 * @param selectionComp
	 *            the component which should monitor area selections
	 */
	public void setSelectionComponent(SelectionComponent selectionComp) {
		selectComp = selectionComp;
		selectComp.addListener(this);
	}
}
