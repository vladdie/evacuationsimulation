package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.nodeselection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * Selects random node ID's on every call.
 * 
 * @author ConstiZ
 *
 */
public class FullyRandomNodeSelection extends AbstractNodeSelection {
	Random rnd;

	public FullyRandomNodeSelection() {
		super();
		rnd = new Random(); // TODO seed for repeated simulation runs?
	}

	@Override
	public List<INodeID> selectNodes(int nodeCount, List<INodeID> allNodeIDs)
			throws IllegalSelectionException {
		validateSelection(nodeCount, allNodeIDs);

		List<INodeID> selection = new ArrayList<INodeID>(allNodeIDs);

		rnd = new Random();
		Collections.shuffle(selection, rnd);
		selection.subList(nodeCount, allNodeIDs.size()).clear();
		return selection;
	}

}
