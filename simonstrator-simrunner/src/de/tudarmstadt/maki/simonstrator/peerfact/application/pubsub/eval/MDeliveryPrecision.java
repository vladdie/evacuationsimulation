/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.MDeliveryPrecision.MDeliveryPrecisionValue;

/**
 * Precision: the number of true positives (e.g., valid notifications) divided
 * by the sum of true positives and falsly notified clients (e.g., clients that
 * were not subscribers)
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Nov 6, 2013
 */
public class MDeliveryPrecision extends AbstractMetric<MDeliveryPrecisionValue>
		implements Metric<MDeliveryPrecisionValue> {

	public MDeliveryPrecision() {
		super("Precision",
				MetricUnit.NONE);
	}

	/**
	 * The value
	 * 
	 * @author Bjoern
	 * @version 1.0, Sep 22, 2013
	 */
	protected class MDeliveryPrecisionValue
			implements de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue<Double> {

		@Override
		public Double getValue() {
			return PubSubGlobalKnowledge.getMAveragePrecision();
		}

		@Override
		public boolean isValid() {
			return PubSubGlobalKnowledge.getMAveragePrecision() != -1;
		}

	}

	@Override
	public void initialize(List<Host> hosts) {
		setOverallMetric(new MDeliveryPrecisionValue());
	}

}
