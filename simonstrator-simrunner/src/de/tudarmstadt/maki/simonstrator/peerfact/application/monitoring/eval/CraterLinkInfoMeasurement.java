package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;

@Entity
@Table(name = "crater_link_info")
public class CraterLinkInfoMeasurement extends CustomMeasurement {

	public static final MetricDescription CRATER_HOST_LINK_INFO_METRIC = new MetricDescription(CraterLinkAnalyzer.class,
			"CraterHostLinkInfoBean",
			"Provides # of msg sent and received as well as their sizes during the stored time interval", "");

	@SuppressWarnings("unused")
	private long dataID;

	@SuppressWarnings("unused")
	private long timeInterval;

	// the variables to measure the size of the sent messages
	@SuppressWarnings("unused")
	private long upAdHocTraffic;

	@SuppressWarnings("unused")
	private long upCellularTraffic;

	@SuppressWarnings("unused")
	private long upSinkTraffic;

	@SuppressWarnings("unused")
	private long upCloudletTraffic;

	@SuppressWarnings("unused")
	private long upNoSinkAdvTraffic;

	@SuppressWarnings("unused")
	private long upSinkAdvTraffic;

	@SuppressWarnings("unused")
	private long upDataTraffic;

	// the variables to measure the size of the received messages
	@SuppressWarnings("unused")
	private long downAdHocTraffic;

	@SuppressWarnings("unused")
	private long downCellularTraffic;

	@SuppressWarnings("unused")
	private long downNoSinkAdvTraffic;

	@SuppressWarnings("unused")
	private long downSinkAdvTraffic;

	@SuppressWarnings("unused")
	private long downDataTraffic;

	// the variables to count the received messages
	@SuppressWarnings("unused")
	private int countRec_AdHocMsg;

	@SuppressWarnings("unused")
	private int countRec_CellularMsg;

	@SuppressWarnings("unused")
	private int countRec_NoSinkAdvMsg;

	@SuppressWarnings("unused")
	private int countRec_SinkAdvMsg;

	@SuppressWarnings("unused")
	private int countRec_DataMsg;

	// the variables to count the sent messages
	@SuppressWarnings("unused")
	private int countSend_AdHocMsg;

	@SuppressWarnings("unused")
	private int countSend_CellularMsg;

	@SuppressWarnings("unused")
	private int countSend_SinkUploadMsg;

	@SuppressWarnings("unused")
	private int countSend_CloudletUploadMsg;

	@SuppressWarnings("unused")
	private int countSend_NoSinkAdvMsg;

	@SuppressWarnings("unused")
	private int countSend_SinkAdvMsg;

	@SuppressWarnings("unused")
	private int countSend_DataMsg;

	public CraterLinkInfoMeasurement(long dataID, long timeInterval, long upAdHocTraffic, long upCellularTraffic,
			long upSinkTraffic, long upCloudletTraffic, long upNoSinkAdvTraffic, long upSinkAdvTraffic, long upDataTraffic,
			long downAdHocTraffic, long downCellularTraffic, long downNoSinkAdvTraffic, long downSinkAdvTraffic,
			long downDataTraffic, int countRec_AdHocMsg, int countRec_CellularMsg, int countRec_NoSinkAdvMsg,
			int countRec_SinkAdvMsg, int countRec_DataMsg, int countSend_AdHocMsg, int countSend_CellularMsg,
			int countSend_SinkUploadMsg, int countSend_CloudletUploadMsg, int countSend_NoSinkAdvMsg, int countSend_SinkAdvMsg,
			int countSend_DataMsg) {
		super();
		this.dataID = dataID;
		this.timeInterval = timeInterval;
		this.upAdHocTraffic = upAdHocTraffic;
		this.upCellularTraffic = upCellularTraffic;
		this.upSinkTraffic = upSinkTraffic;
		this.upCloudletTraffic = upCloudletTraffic;
		this.upNoSinkAdvTraffic = upNoSinkAdvTraffic;
		this.upSinkAdvTraffic = upSinkAdvTraffic;
		this.upDataTraffic = upDataTraffic;
		this.downAdHocTraffic = downAdHocTraffic;
		this.downCellularTraffic = downCellularTraffic;
		this.downNoSinkAdvTraffic = downNoSinkAdvTraffic;
		this.downSinkAdvTraffic = downSinkAdvTraffic;
		this.downDataTraffic = downDataTraffic;
		this.countRec_AdHocMsg = countRec_AdHocMsg;
		this.countRec_CellularMsg = countRec_CellularMsg;
		this.countRec_NoSinkAdvMsg = countRec_NoSinkAdvMsg;
		this.countRec_SinkAdvMsg = countRec_SinkAdvMsg;
		this.countRec_DataMsg = countRec_DataMsg;
		this.countSend_AdHocMsg = countSend_AdHocMsg;
		this.countSend_CellularMsg = countSend_CellularMsg;
		this.countSend_SinkUploadMsg = countSend_SinkUploadMsg;
		this.countSend_CloudletUploadMsg = countSend_CloudletUploadMsg;
		this.countSend_NoSinkAdvMsg = countSend_NoSinkAdvMsg;
		this.countSend_SinkAdvMsg = countSend_SinkAdvMsg;
		this.countSend_DataMsg = countSend_DataMsg;
	}
}
