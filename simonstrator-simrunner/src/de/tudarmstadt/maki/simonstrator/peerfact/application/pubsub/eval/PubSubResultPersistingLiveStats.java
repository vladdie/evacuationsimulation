package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import java.io.Writer;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.SubscriptionResultCollector.SubscriptionResultSnapshot;

/**
 * Provides access to data via live metrics, also on a per-host basis
 * (interactive). This is usually only needed in visualization/demo-mode.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class PubSubResultPersistingLiveStats implements PubSubResultPersisting {

	private static Map<INodeID, SubscriptionResultSnapshot> lastSnapshotByNode = new ConcurrentHashMap<>();

	public PubSubResultPersistingLiveStats() {
		PubSubGlobalKnowledge.OBSERVE_PUBLICATIONS = true;
		PubSubGlobalKnowledge.OBSERVE_SUBSCRIPTIONS = true;
		PubSubGlobalKnowledge.OBSERVE_SUBSCRIPTIONS_INTERVAL = 1 * Time.SECOND;
	}

	public static SubscriptionResultSnapshot getSnapshot(INodeID from) {
		return lastSnapshotByNode.get(from);
	}

	@Override
	public void start() {
		// don't care.
	}

	@Override
	public void stop(Writer out) {
		// dont't care
	}

	@Override
	public void persistPublication(PublicationResultCollector collector) {
		// don't care. We still observe them for the server-side metrics
		Monitor.log(PubSubResultPersistingLiveStats.class, Level.DEBUG, "%s", collector);
	}

	@Override
	public void persistSubscription(SubscriptionResultCollector collector) {
		SubscriptionResultSnapshot snapshot = collector.createSnapshot();
		lastSnapshotByNode.put(collector.subscriberNodeId, snapshot);
	}

}
