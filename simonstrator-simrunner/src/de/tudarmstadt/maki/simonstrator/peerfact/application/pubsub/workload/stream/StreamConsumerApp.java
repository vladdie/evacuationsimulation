package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream.StreamModel.Chunk;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream.StreamModel.Statistics;

/**
 * Consumer of a stream. In most configurations, this might just as well be the
 * Director node.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class StreamConsumerApp extends StreamApp implements PubSubListener {

	private Subscription sub = null;

	private StreamConsumerOperation streamOp = null;

	public StreamConsumerApp(Host host) {
		super(host);
	}

	@Override
	public void initialize() {
		super.initialize();
		StreamDirectorApp.registerConsumer(this);
	}

	public void startViewing(StreamModel streamModel) {
		// start viewing == subscribe
		assert sub == null;
		assert streamOp == null;
		sub = getPubSub().createSubscription(ATTR_TOPIC);
		getPubSub().subscribe(sub, this);
		streamOp = new StreamConsumerOperation(streamModel);
		streamOp.start();
	}

	public void stopViewing() {
		assert sub != null;
		streamOp.stop();
		streamOp = null;
		getPubSub().unsubscribe(sub, null);
		sub = null;
	}

	@Override
	public void onNotificationArrived(Subscription matchedSubscription, Notification notification) {
		if (streamOp == null) {
			// No active streaming
			return;
		}

		assert sub != null && sub.equals(matchedSubscription);

		int chunkId = notification.getAttribute(ATTR_CHUNK_ID.getName(), ATTR_CHUNK_ID.getType()).getValue();
		short blockId = notification.getAttribute(ATTR_BLOCK_ID.getName(), ATTR_BLOCK_ID.getType()).getValue();
		streamOp.onReceivedBlock(chunkId, blockId);


		// long timestampCreated =
		// notification._getNotificationInfo(null).getTimestampOfCreation();
		// long duration = Time.getCurrentTime() - timestampCreated;
		//
		// System.out.println();
		// System.out.println("StreamAPP Consumer: received chunk " + chunkId +
		// ". Took " + Time.getFormattedTime(duration)
		// + " " + notification.toString());
		// System.out.println(" - Throughput: "
		// + Rate.getFormattedRate(notification.getTransmissionSize() * 8 *
		// Time.SECOND / duration));
		// System.out.println(" - Effective Goodput: " + Rate.getFormattedRate(
		// notification._getNotificationInfo(null).getNotificationPayloadSize()
		// * 8 * Time.SECOND / duration));
		// System.out.println(" - Delay: " + Time.getFormattedTime(duration));
		// System.out.println(" - Size: " + notification.getTransmissionSize() *
		// 8 + " bit");
	}


	/**
	 * Consumer operation
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private static class StreamConsumerOperation implements EventHandler {

		public static final int EVENT_NEXT_CHUNK = 23;

		public static final int EVENT_CHECK_BUFFER = 34;

		private StreamModel model;

		private boolean active = false;

		private int chunkToPlay = -1;

		private State currentState = null;

		private long timestampPlaybackStart = -1;

		public static long initialBufferWaitingTime = 0 * Time.SECOND;

		/**
		 * When we stall, we buffer this length of video playback before
		 * continuing again.
		 */
		public static long minBufferLength = 1 * Time.SECOND;

		private long timestampStartStalling = 0;

		private long durationCurrentStall = 0;

		private enum State {
			STALLING, PLAYING, BUFFERING_INIT
		}

		public StreamConsumerOperation(StreamModel model) {
			this.model = model;
		}

		public void start() {
			this.active = true;
			this.timestampPlaybackStart = Time.getCurrentTime();
			switchState(State.BUFFERING_INIT);
			Event.scheduleWithDelay(initialBufferWaitingTime, this, null, EVENT_CHECK_BUFFER);
		}

		public void stop() {
			// Statistics!
			Statistics stats = model.getStatistics();
			System.out.println(stats.toString());
			stats.appendToFile();
			this.model = null;
			this.active = false;
		}

		public void onReceivedBlock(int chunkId, short blockId) {
			model.onReceivedBlock(chunkId, blockId);
			if (currentState == State.STALLING) {
				//
				Event.scheduleImmediately(this, null, EVENT_CHECK_BUFFER);
			} else if (currentState == State.BUFFERING_INIT
					&& timestampPlaybackStart + initialBufferWaitingTime < Time.getCurrentTime()) {
				Event.scheduleImmediately(this, null, EVENT_CHECK_BUFFER);
			}
		}

		private void switchState(State newState) {
			System.out.println("StreamConsumer: Switched State to " + newState.toString());
			if (newState == State.STALLING || newState == State.BUFFERING_INIT) {
				assert currentState == null || currentState == State.PLAYING;
				timestampStartStalling = Time.getCurrentTime();
			} else if (newState == State.PLAYING) {
				durationCurrentStall = Time.getCurrentTime() - timestampStartStalling;
			}
			currentState = newState;
		}

		@Override
		public void eventOccurred(Object content, int type) {
			if (!active) {
				return;
			}
			
			if (type == EVENT_CHECK_BUFFER) {
				/*
				 * Check the buffer if we are able to play
				 */
				assert currentState == State.BUFFERING_INIT || currentState == State.STALLING;
				if (currentState == State.STALLING) {
					/*
					 * Currently stalling
					 */
					if (model.isPlayable(chunkToPlay)
							&& model.getPlayableVideoBufferLength(chunkToPlay) >= minBufferLength) {
						switchState(State.PLAYING);
						Event.scheduleImmediately(this, null, EVENT_NEXT_CHUNK);
					}
				} else {
					assert currentState == State.BUFFERING_INIT;
					/*
					 * Is there a complete chunk available?
					 */
					Chunk chunk = model.getOldestPlayableChunk();
					if (chunk != null) {
						switchState(State.PLAYING);
						chunkToPlay = chunk.id;
						Event.scheduleImmediately(this, null, EVENT_NEXT_CHUNK);
					}
				}
			} else if (type == EVENT_NEXT_CHUNK) {
				assert currentState == State.PLAYING;
				if (model.isPlayable(chunkToPlay)) {
					// Play!
					long duration = model.markAsPlayed(chunkToPlay, durationCurrentStall);
					durationCurrentStall = 0;
					chunkToPlay++;
					Event.scheduleWithDelay(duration, this, null, EVENT_NEXT_CHUNK);
				} else {
					/*
					 * Skip? For now, we wait. On each received block,
					 * EVENT_CHECK_BUFFER will be triggered.
					 */
					switchState(State.STALLING);
				}
			}
		}

	}

	public static class Factory implements HostComponentFactory {

		public static long bufferLength = 0;

		@Override
		public HostComponent createComponent(Host host) {
			return new StreamConsumerApp(host);
		}

		/**
		 * The initial buffer length (time that we wait before starting to play
		 * the video)
		 * 
		 * @param bufferLength
		 */
		public void setBufferLength(long bufferLength) {
			Factory.bufferLength = bufferLength;
			StreamConsumerOperation.initialBufferWaitingTime = bufferLength;
		}

	}

}
