package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import de.tud.kom.p2psim.impl.simengine.Simulator;
import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.NoSinkAdvertising;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.SinkAdvertising;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.PerNodeRoleTimesAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api.CraterSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api.CraterSubComponent.SubComponentState;

/**
 * 
 * Analyzer for estimation of the times {@link CraterNodeComponent}s are in which {@link CraterNodeRole}. Beside that
 * estimation of the active times of the {@link SinkAdvertising} and the {@link NoSinkAdvertising} components.
 * 
 * Metrics:
 * 
 * Ratio being Sink over online time;
 * 
 * Ratio Active Leaf over online time
 * 
 * Ratio Active Sink over online time
 * 
 * @author Nils Richerzhagen
 * 
 *         Good one for Fairness estimates? Count how often nodes is what and estimate based on that the fairness. Also
 *         use how long in average a node has been sink and calculate fairness. Also use how long in total node has been
 *         in which role calculate fairness.
 *
 *
 */
public class PerNodeRoleTimesAnalyzerImpl implements PerNodeRoleTimesAnalyzer, EventHandler {

	private HashMap<INodeID, NodeRoleInfo> _currentNodeRoleInfos = new LinkedHashMap<INodeID, NodeRoleInfo>();
	private HashMap<INodeID, NodeRoleTimes> perNodeRoleTimes = new LinkedHashMap<INodeID, NodeRoleTimes>();

	private HashMap<INodeID, Long> _currentActiveLeafIntervals = new LinkedHashMap<INodeID, Long>();
	private HashMap<INodeID, Long> _currentActiveSinkIntervals = new LinkedHashMap<INodeID, Long>();
	private HashMap<INodeID, NodeActiveInfo> perNodeActiveTimes = new LinkedHashMap<INodeID, NodeActiveInfo>();

	private boolean started = false;

	private boolean ENABLE_DAO = false;

	private final int _EVENT_ENABLE_DAO_AT = 2001;

	private INodeID cloudID;

	private final AtomicBoolean _firstMethodCall = new AtomicBoolean(true);

	public PerNodeRoleTimesAnalyzerImpl() {
		DAO.addEntityClass(PerNodeRoleTimeMeasurement.class);
		// DAO.addEntityClass(OverallRoleTimeRatiosMeasurement.class);
		DAO.addEntityClass(PerNodeRoleFairnessMeasurement.class);
	}

	public void setEnableDao(boolean enableDao) {
		this.ENABLE_DAO = enableDao;
	}

	public void setTimeEnableDao(long timeEnableDao) {
		if (ENABLE_DAO) {
			Event.scheduleWithDelay(timeEnableDao, this, null, _EVENT_ENABLE_DAO_AT);
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		assert ENABLE_DAO;
		if (type == _EVENT_ENABLE_DAO_AT) {
			started = true;

			// As this can happen any time in the simulation we have to iterate
			// through the hosts and add the relevant
			// NodeRoleInfos.
			for (Host host : Oracle.getAllHosts()) {
				try {
					CraterNodeComponent comp = host.getComponent(CraterNodeComponent.class);
					long currentTime = Time.getCurrentTime();
					_currentNodeRoleInfos.put(host.getId(), new NodeRoleInfo(comp.getCraterNodeRole(), currentTime));

					boolean setted = false;
					if (comp.getSinkAdvertising().getComponentState() == SubComponentState.ACTIVE) {
						_currentActiveSinkIntervals.put(host.getId(), currentTime);
					}

					if (comp.getNoSinkAdvertising().getComponentState() == SubComponentState.ACTIVE) {
						assert !setted;
						_currentActiveLeafIntervals.put(host.getId(), currentTime);
					}

				} catch (ComponentNotAvailableException e) {
					if (_firstMethodCall.getAndSet(false)) {
						try {
							host.getComponent(CraterCentralComponent.class);
							// Central Entity
							cloudID = host.getId();
						} catch (ComponentNotAvailableException e1) {
							// Others don't care
						}
						// others don't care
					}
					// other runs don't care
				}
			}
		}
	}

	@Override
	public void start() {
		// done with event start enableDaoAt
	}

	@Override
	public void stop(Writer out) {
		if (ENABLE_DAO) {

			long currentTime = Time.getCurrentTime();

			/*
			 * Last elements
			 */
			for (INodeID actId : _currentNodeRoleInfos.keySet()) {
				NodeRoleInfo currentNodeRoleInfo = _currentNodeRoleInfos.get(actId);
				if (perNodeRoleTimes.containsKey(actId)) {
					NodeRoleTimes currentNodeRoleTimes = perNodeRoleTimes.get(actId);
					currentNodeRoleTimes.addInterval(currentNodeRoleInfo.getNodeRole(), currentNodeRoleInfo.getStartTimestamp(),
							currentTime);
					perNodeRoleTimes.put(actId, currentNodeRoleTimes);
				} else {
					perNodeRoleTimes.put(actId, new NodeRoleTimes(currentNodeRoleInfo.getNodeRole(),
							currentNodeRoleInfo.getStartTimestamp(), currentTime));
				}
			}

			/*
			 * Last elements
			 */
			for (INodeID actId : _currentActiveLeafIntervals.keySet()) {
				long startTime = _currentActiveLeafIntervals.get(actId);
				if (perNodeActiveTimes.containsKey(actId)) {
					NodeActiveInfo currentNodeActiveInfo = perNodeActiveTimes.get(actId);
					currentNodeActiveInfo.addInterval(false, startTime, currentTime);
					perNodeActiveTimes.put(actId, currentNodeActiveInfo);
				} else {
					perNodeActiveTimes.put(actId, new NodeActiveInfo(false, startTime, currentTime));
				}
			}

			/*
			 * Last elements
			 */
			for (INodeID actId : _currentActiveSinkIntervals.keySet()) {
				long startTime = _currentActiveSinkIntervals.get(actId);
				if (perNodeActiveTimes.containsKey(actId)) {
					NodeActiveInfo currentNodeActiveInfo = perNodeActiveTimes.get(actId);
					currentNodeActiveInfo.addInterval(true, startTime, currentTime);
					perNodeActiveTimes.put(actId, currentNodeActiveInfo);
				} else {
					perNodeActiveTimes.put(actId, new NodeActiveInfo(true, startTime, currentTime));
				}
			}

			/*
			 * Percentual Time Fairness Role time? Times being in role over leaf + sink time.
			 * 
			 * Percentual Time Fairness for Active Times? Active over time being in that role.
			 */

			int count_LeafRatio = 0;
			double sum_LeafRatio = 0;
			double sumSquares_LeafRatio = 0;

			int count_SinkRatio = 0;
			double sum_SinkRatio = 0;
			double sumSquares_SinkRatio = 0;

			int count_ActiveLeafRatio = 0;
			double sum_ActiveLeafRatio = 0;
			double sumSquares_ActiveLeafRatio = 0;

			int count_ActiveSinkRatio = 0;
			double sum_ActiveSinkRatio = 0;
			double sumSquares_ActiveSinkRatio = 0;

			for (INodeID actId : perNodeRoleTimes.keySet()) {

				NodeRoleTimes currentNodeRoleTimes = perNodeRoleTimes.get(actId);

				long sinkTime = currentNodeRoleTimes.getTimeAsSink();
				long leafTime = currentNodeRoleTimes.getTimeAsLeaf();
				// long offlineTime = currentNodeRoleTimes.getOfflineTime();

				NodeActiveInfo currentNodeActiveInfo = perNodeActiveTimes.remove(actId);
				long sinkActiveTime;
				long leafActiveTime;
				if (currentNodeActiveInfo != null) {
					sinkActiveTime = currentNodeActiveInfo.getActiveAsSink();
					leafActiveTime = currentNodeActiveInfo.getActiveAsLeaf();
				} else {
					sinkActiveTime = 0;
					leafActiveTime = 0;
				}

				assert sinkTime >= sinkActiveTime;
				assert leafTime >= leafActiveTime;

				double leafTimeRatio = (double) leafTime / (double) (leafTime + sinkTime);
				if (Double.isNaN(leafTimeRatio)) {
					leafTimeRatio = 0;
				}
				double sinkTimeRatio = (double) sinkTime / (double) (sinkTime + leafTime);
				if (Double.isNaN(sinkTimeRatio)) {
					sinkTimeRatio = 0;
				}
				double leafTimeActiveRatio = (double) leafActiveTime / (double) leafTime;
				if (Double.isNaN(leafTimeActiveRatio)) {
					leafTimeActiveRatio = 0;
				}
				double sinkTimeActiveRatio = (double) sinkActiveTime / (double) sinkTime;
				if (Double.isNaN(sinkTimeActiveRatio)) {
					sinkTimeActiveRatio = 0;
				}

				// System.out.println("Node " + actId.value() + " as Sink: " +
				// Time.getFormattedTime(sinkTime) + "
				// active Sink "
				// + Time.getFormattedTime(sinkActiveTime) + " as Leaf " +
				// Time.getFormattedTime(leafTime) + " active
				// Leaf "
				// + Time.getFormattedTime(leafActiveTime) + " offline: " +
				// Time.getFormattedTime(offlineTime) + "
				// overall "
				// + Time.getFormattedTime(sinkTime + leafTime + offlineTime));
				// System.out.println("Node " + actId.value() + " as Sink: " + sinkTimeRatio + " active Sink " +
				// sinkTimeActiveRatio
				// + " as Leaf " + leafTimeRatio + " active Leaf " + leafTimeActiveRatio);

				Monitor.log(PerNodeRoleTimesAnalyzer.class, Level.DEBUG,
						"Node " + actId.value() + " as Sink: " + sinkTimeRatio + " active Sink " + sinkTimeActiveRatio
								+ " as Leaf " + leafTimeRatio + " active Leaf " + leafTimeActiveRatio);

				/*
				 * Daten für CDF's. Alle zusammen und dann CDF drüber legen.
				 * 
				 * Knoten: Zeit als Sink, Zeit als Leaf, Zeit Aktiv als Sink, Zeit Aktiv als Leaf
				 * 
				 * Daten um z.B. aktiv als sink für sinkTime > 0 über SQL. (Wenn mal Sink, wie lange dann Aktiv?)
				 * 
				 * Daten um z.B. aktiv als leaf für leafTime > 0 über SQL. (Wenn mal Leaf, wie lange dann Aktiv?)
				 */
				PerNodeRoleTimeMeasurement stateTimeMeasurement = new PerNodeRoleTimeMeasurement(actId.value(), sinkTimeRatio,
						leafTimeRatio, sinkTimeActiveRatio, leafTimeActiveRatio);
				MeasurementDAO.storeCustomMeasurement(PerNodeRoleTimeMeasurement.PER_NODE_ROLE_TIME_MEASUREMENT_METRIC,
						actId.value(), Simulator.getCurrentTime(), stateTimeMeasurement);

				/*
				 * Node should be online at least as leaf or sink when used for Fairness calculations.
				 */
				if (leafTime > 0 || sinkTime > 0) {

					/*
					 * For Fairness calculations!
					 */
					if (leafTime >= 0) {
						count_LeafRatio++;
						sum_LeafRatio += (double) (leafTime) / ((double) (leafTime + sinkTime));
						sumSquares_LeafRatio += Math.pow((double) leafTime / (double) (leafTime + sinkTime), 2);
					}
					if (sinkTime >= 0) {
						count_SinkRatio++;
						sum_SinkRatio += (double) sinkTime / (double) (sinkTime + leafTime);
						sumSquares_SinkRatio += Math.pow((double) sinkTime / (double) (sinkTime + leafTime), 2);
					}

					/*
					 * Percentual Time Fairness for Active Times? Active over time being in that role.
					 */
					if (leafActiveTime >= 0) {
						count_ActiveLeafRatio++;
						if (leafTime == 0) {
							assert leafActiveTime == 0;
							sum_ActiveLeafRatio += 0;
							sumSquares_ActiveLeafRatio += 0;
						} else {
							sum_ActiveLeafRatio += (double) leafActiveTime / (double) (leafTime);
							sumSquares_ActiveLeafRatio += Math.pow((double) leafActiveTime / (double) (leafTime), 2);
						}
					}
					if (sinkActiveTime >= 0) {
						count_ActiveSinkRatio++;
						if (sinkTime == 0) {
							assert sinkActiveTime == 0;
							sum_ActiveSinkRatio += 0;
							sumSquares_ActiveSinkRatio += 0;
						} else {
							sum_ActiveSinkRatio += (double) sinkActiveTime / (double) (sinkTime);
							sumSquares_ActiveSinkRatio += Math.pow((double) sinkActiveTime / (double) (sinkTime), 2);
						}
					}
				}

			}

			assert perNodeActiveTimes
					.isEmpty() : "There should not be any entry left in this list as all nodes that can be active must be online at least once.";

			double jainFairness_leafRatio = Math.pow(sum_LeafRatio, 2) / (count_LeafRatio * sumSquares_LeafRatio);
			double jainFairness_sinkRatio = Math.pow(sum_SinkRatio, 2) / (count_SinkRatio * sumSquares_SinkRatio);
			double jainFairness_leafActiveRatio = Math.pow(sum_ActiveLeafRatio, 2)
					/ (count_ActiveLeafRatio * sumSquares_ActiveLeafRatio);
			double jainFairness_sinkActiveRatio = Math.pow(sum_ActiveSinkRatio, 2)
					/ (count_ActiveSinkRatio * sumSquares_ActiveSinkRatio);

			if (Double.isNaN(jainFairness_leafRatio)) {
				jainFairness_leafRatio = 0;
			}
			if (Double.isNaN(jainFairness_sinkRatio)) {
				jainFairness_sinkRatio = 0;
			}
			if (Double.isNaN(jainFairness_leafActiveRatio)) {
				jainFairness_leafActiveRatio = 0;
			}
			if (Double.isNaN(jainFairness_sinkActiveRatio)) {
				jainFairness_sinkActiveRatio = 0;
			}

			// System.out.println("Leaf Ratio: " + jainFairness_leafRatio + " leaf Active Ratio " +
			// jainFairness_leafActiveRatio
			// + " sink ratio " + jainFairness_sinkRatio + " sink active ratio " + jainFairness_sinkActiveRatio);

			PerNodeRoleFairnessMeasurement roleFairnessMeasurement = new PerNodeRoleFairnessMeasurement(jainFairness_sinkRatio,
					jainFairness_leafRatio, jainFairness_sinkActiveRatio, jainFairness_leafActiveRatio);
			MeasurementDAO.storeCustomMeasurement(PerNodeRoleFairnessMeasurement.PER_NODE_ROLE_FAIRNESS_MEASUREMENT_METRIC,
					cloudID.value(), Simulator.getCurrentTime(), roleFairnessMeasurement);

			/*
			 * Average Active Ratios für alle Knoten auch die, die nie Sink waren. (Aussage: Wie lange ist ein Knoten in
			 * diesem Netz activ als Sink.)
			 */
			// double avgActiveSinkRatio = sum_ActiveSinkRatio / count_ActiveSinkRatio;
			// double avgActiveLeafRatio = sum_ActiveLeafRatio / count_ActiveLeafRatio;
			// if (Double.isNaN(avgActiveSinkRatio)) {
			// avgActiveSinkRatio = 0;
			// }
			// if (Double.isNaN(avgActiveLeafRatio)) {
			// avgActiveLeafRatio = 0;
			// }

			// System.out.println("Avg active sink Ratio: " + avgActiveSinkRatio
			// + " avg active leaf ratio: " + avgActiveLeafRatio);
			// OverallRoleTimeRatiosMeasurement overallRoleTimeRatiosMeasurement = new OverallRoleTimeRatiosMeasurement(
			// avgActiveSinkRatio, avgActiveLeafRatio);
			// MeasurementDAO.storeCustomMeasurement(OverallRoleTimeRatiosMeasurement.OVERALL_ROLE_TIME_RATIOS_METRIC,
			// cloudID.value(), Simulator.getCurrentTime(), overallRoleTimeRatiosMeasurement);

		}

	}

	@Override
	public void onCraterNodeRoleChanged(CraterNodeComponent comp, CraterNodeRole craterNodeRole) {
		if (!started) {
			return;
		}

		INodeID hostId = comp.getHost().getId();
		long currentTime = Time.getCurrentTime();
		NodeRoleInfo nodeRoleInfo = _currentNodeRoleInfos.remove(hostId);
		if (perNodeRoleTimes.containsKey(hostId)) {
			NodeRoleTimes currentNodeRoleTimes = perNodeRoleTimes.get(hostId);
			currentNodeRoleTimes.addInterval(nodeRoleInfo.getNodeRole(), nodeRoleInfo.getStartTimestamp(), currentTime);
			perNodeRoleTimes.put(hostId, currentNodeRoleTimes);
		} else {
			perNodeRoleTimes.put(hostId,
					new NodeRoleTimes(nodeRoleInfo.getNodeRole(), nodeRoleInfo.getStartTimestamp(), currentTime));
		}
		_currentNodeRoleInfos.put(hostId, new NodeRoleInfo(craterNodeRole, currentTime));
	}

	@Override
	public void onSubComponentStateChange(CraterSubComponent subComp, SubComponentState componentState) {
		if (!started) {
			return;
		}

		INodeID hostId = subComp.getParentComponent().getHost().getId();
		// System.out.println(Time.getFormattedTime() + " " + hostId + " " +
		// subComp.getClass().getSimpleName() + " "
		// + componentState.toString());

		if (subComp instanceof NoSinkAdvertising) {
			switch (componentState) {
			case ACTIVE:
				/*
				 * Become active, thus searching for sinks. And start of activeInterval for leaf.
				 */
				_currentActiveLeafIntervals.put(hostId, Time.getCurrentTime());
				break;
			default:
				/*
				 * Become Inactive or Offline thus end of activeInterval for leaf --> remove it!
				 */
				if (_currentActiveLeafIntervals.containsKey(hostId)) {
					long startTime = _currentActiveLeafIntervals.remove(hostId);
					if (perNodeActiveTimes.containsKey(hostId)) {
						NodeActiveInfo currentNodeActiveInfo = perNodeActiveTimes.get(hostId);
						currentNodeActiveInfo.addInterval(false, startTime, Time.getCurrentTime());
						perNodeActiveTimes.put(hostId, currentNodeActiveInfo);
					} else {
						perNodeActiveTimes.put(hostId, new NodeActiveInfo(false, startTime, Time.getCurrentTime()));
					}
				}
				break;
			}
		} else if (subComp instanceof SinkAdvertising) {
			switch (componentState) {
			case ACTIVE:
				/*
				 * Become active, thus searching for sinks. And start of activeInterval for sink.
				 */
				_currentActiveSinkIntervals.put(hostId, Time.getCurrentTime());
				break;
			default:
				/*
				 * Become Inactive or Offline thus end of activeInterval for sink --> remove it!
				 */
				if (_currentActiveSinkIntervals.containsKey(hostId)) {
					long startTime = _currentActiveSinkIntervals.remove(hostId);

					if (perNodeActiveTimes.containsKey(hostId)) {
						NodeActiveInfo currentNodeActiveInfo = perNodeActiveTimes.get(hostId);
						currentNodeActiveInfo.addInterval(true, startTime, Time.getCurrentTime());
						perNodeActiveTimes.put(hostId, currentNodeActiveInfo);
					} else {
						perNodeActiveTimes.put(hostId, new NodeActiveInfo(true, startTime, Time.getCurrentTime()));
					}
				}
				break;
			}
		} else {
			throw new AssertionError("Class not known.");
		}
	}

	/**
	 * For storage of open time intervals for {@link CraterNodeRole}s. Includes the startTimestamp.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	private class NodeRoleInfo {
		private CraterNodeRole nodeRole;

		private long startTimestamp;

		public NodeRoleInfo(CraterNodeRole nodeRole, long startTimestamp) {
			this.nodeRole = nodeRole;
			this.startTimestamp = startTimestamp;
		}

		public CraterNodeRole getNodeRole() {
			return nodeRole;
		}

		public long getStartTimestamp() {
			return startTimestamp;

		}
	}

	/**
	 * Storage of active times per {@link CraterNodeComponent} in the roles of LEAF and SINK. Additional count to
	 * measure average times.
	 * 
	 * @author Nils Richerzhagen
	 */
	private class NodeActiveInfo {
		private long activeAsLeaf = 0;
		private long activeAsSink = 0;

		private int count_activeAsLeaf = 0;
		private int count_activeAsSink = 0;

		/**
		 * Start and end Timestamp to avoid mistakes.
		 * 
		 * @param asSink
		 * @param startTimestamp
		 * @param entTimestamp
		 */
		public NodeActiveInfo(boolean asSink, long startTimestamp, long endTimestamp) {
			if (asSink) {
				activeAsSink += endTimestamp - startTimestamp;
				count_activeAsSink++;
			} else {
				activeAsLeaf += endTimestamp - startTimestamp;
				count_activeAsLeaf++;
			}
		}

		public void addInterval(boolean asSink, long startTimestamp, long endTimestamp) {
			if (asSink) {
				activeAsSink += endTimestamp - startTimestamp;
				count_activeAsSink++;
			} else {
				activeAsLeaf += endTimestamp - startTimestamp;
				count_activeAsLeaf++;
			}
		}

		public long getActiveAsLeaf() {
			return activeAsLeaf;
		}

		public long getActiveAsSink() {
			return activeAsSink;
		}
	}

	/**
	 * 
	 * For storage of the overall times with additional counts.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	private class NodeRoleTimes {
		private long timeAsLeaf = 0;
		private int countTimeBeingLeaf = 0;

		private long timeAsSink = 0;
		private int countTimeBeingSink = 0;

		private long offlineTime = 0;
		private int countTimeBeingOffline = 0;

		public NodeRoleTimes(CraterNodeRole nodeRole, long startTimestamp, long endTimestamp) {
			switch (nodeRole) {
			case LEAF:
				timeAsLeaf += endTimestamp - startTimestamp;
				countTimeBeingLeaf++;
				break;
			case SINK:
				timeAsSink += endTimestamp - startTimestamp;
				countTimeBeingSink++;
				break;
			case OFFLINE:
				offlineTime += endTimestamp - startTimestamp;
				countTimeBeingOffline++;
				break;
			default:
				break;
			}
		}

		public void addInterval(CraterNodeRole nodeRole, long startTimestamp, long endTimestamp) {
			switch (nodeRole) {
			case LEAF:
				timeAsLeaf += endTimestamp - startTimestamp;
				countTimeBeingLeaf++;
				break;
			case SINK:
				timeAsSink += endTimestamp - startTimestamp;
				countTimeBeingSink++;
				break;
			case OFFLINE:
				offlineTime += endTimestamp - startTimestamp;
				countTimeBeingOffline++;
				break;
			default:
				break;
			}
		}

		public long getTimeAsLeaf() {
			return timeAsLeaf;
		}

		public long getTimeAsSink() {
			return timeAsSink;
		}

		public long getOfflineTime() {
			return offlineTime;
		}

	}
}
