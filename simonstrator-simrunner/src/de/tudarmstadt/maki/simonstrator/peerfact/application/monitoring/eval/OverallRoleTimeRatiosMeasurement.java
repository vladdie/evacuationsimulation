package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;

/**
 * Für alle Knoten.
 * 
 * @author Nils Richerzhagen
 *
 */
@Entity
@Table(name = "overall_role_ratios")
public class OverallRoleTimeRatiosMeasurement extends CustomMeasurement{

	public static final MetricDescription OVERALL_ROLE_TIME_RATIOS_METRIC = new MetricDescription(
			PerNodeRoleTimesAnalyzerImpl.class, "OverallRoleTimeRatios",
			"Provides the overall role time ratios for this run", "us");
	
	@SuppressWarnings("unused")
	private double ratioAverageActiveAsSink;
	
	@SuppressWarnings("unused")
	private double ratioAverageActiveAsNode;
	
	
	public OverallRoleTimeRatiosMeasurement(double ratioAverageActiveAsSink, double ratioAverageActiveAsNode){
		super();
		this.ratioAverageActiveAsSink = ratioAverageActiveAsSink;
		this.ratioAverageActiveAsNode = ratioAverageActiveAsNode;
		
	}
}
