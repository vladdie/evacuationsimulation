# Load Generator

This is a load generator that allows you to generate traffic in your overlay in a distributed manner. Based on conditions for senders and receivers, nodes can be selected that send and receive requests. After choosing nodes that fulfill the requirements for senders, the set of nodes is reduced by either a trace-based distribution or by a mathematical distribution. This is useful to simulate the behaviour of users as not always all users that are abled to use a service actually use it. After the distribution has been applied all nodes receive the request from the load generator. Once the node has received the request it is up to the overlay to distrube and route the request within the network.

## How to use the Load Generator?

**Step 1**

First of all create a new xml file that is dedicated to the configuration of the load generator in your config folder. Lets call it `load_generator.xml`. In this file all the [directives](#configuration-directives) need to be placed in order to describe a simulation scenario. Then include in your main configuration file (where `<Configuration xmlns:xi="http://www.w3.org/2001/XInclude">...</Configuration>` is located) `<xi:include href="load_generator.xml" parse="xml" />` right before the closing *</Configuration>* tag.

*Example*

```xml
<?xml version="1.0" encoding="UTF-8"?>
<Configuration xmlns:xi="http://www.w3.org/2001/XInclude">

  ...
  
  <xi:include href="load_generator.xml" parse="xml" />

</Configuration>
```

**Step 2**

Start your simulation like you always start your simulation. When all setup is finished the loaded scenario is printed to the standard output and requests are generated.

**Step 3**

There is no step three!

### Configuration directives

Here are all configuration directives that are supported by the load generator. A __*__ denotes that this parameter is required.

#### LoadGenerator

`<LoadGenerator class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.LoadGenerator" sampleRate="5m">`  

> __sampleRate*__: How often are events sampled from the load generator. This value is parsed with [Time.parseTime()](https://dev.kom.e-technik.tu-darmstadt.de/gitlab/maki/simonstrator-api/blob/master/src/de/tudarmstadt/maki/simonstrator/api/Time.java#L112).

This is main directive that initiates the whole load generator. 

#### NodeSelection

`<NodeSelection class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.nodeselection.StaticNodeSelection" />`  

This directive defines the node selection algorithm for the load generator. A node selection algorithm defines which nodes are selected when more nodes are available after filtering than requested by distribution.

Available node selections are: 

 * *FullyRandomNodeSelection* (Select the nodes everytime randomly)
 * *InitialRandomNodeSelection* (Select the nodes once randomly, then select those in preference of others)
 * *StaticNodeSelection* (Always select the same nodes)

#### Scenario

`<Scenario class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Scenario" name="Test">`  

> __name*__: Name of this Scenario. Just used for Output.

A container that holds a set of Requests.

#### Request

`<Request class="de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request">`  

A request contains a RequestType, a set of Sources and a Distribution. Multiple conditions for sources are concatinated via AND.

#### RequestType

`<RequestType class="de.tud.kom.p2psim.impl.loadgen.config.RequestType" attribute="PHY_LOCATION" type="PERIODIC" interval="5s" validity="20m">`   

> __attribute*__: The attribute that is requested from the source.  
> __type*__: One of the types above.  
> __interval__: How often should that data be send back? (only PERIODIC)  
> __validity__: How long should that data be send back? (only PERIODIC)  

A RequestType is a reprensentation of data that is requested from the sources. A set of targets who should reply to this request is given to this object. Multiple conditions for targets are concatinated via AND.

There are three different types of RequestType:

 * *PERIODIC*: Data is requested periodically from the source.
 * *ONE-SHOT*: Data is requested once from the source.
 * *EVENT*: Data is requested when an event occurs (not yet implemented).

#### NodeCondition (Target / Source)

`<Target class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Target" parameter="ENERGY_BATTERY_LEVEL" comparator=">" value="90" />`  
`<Source class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Source" parameter="ENERGY_BATTERY_LEVEL" comparator=">" value="40" />`  

> __parameter*__: The attribute that should be compared.  
> __comparator*__: A comparation operator (`<`, `<=`, `>`, `>=`, `=`, `==`, `!=`, `<>`).  
> __value__: The value that should be compared to.  

A condition that applies to a given node. 

#### Distribution

`<Distribution class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.distribution.UniformDistribution" percentage="0.4" minNodes="10" />`  
`<Distribution class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.distribution.TraceDistributionReader" file="/path/to/trace/file" />`  
`<Distribution class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.distribution.GaussianTimeDistribution" meanTime="0.4" stdev="3" minNumNodes="10" />`  

> __percentage__: The percentage for a uniform distribution.  
> __file__: The path to a trace file.  
> __meanTime__: The mean of a gaussian distribution.  
> __stdev__: The standard derivation of a gaussian distribution.  
> __minNumNodes / minNodes__: A minimal number of nodes that should be returned from this distribution.

A distribution that should be applied to a set of nodes after filtering. Available Distributions:

 * *UniformDistribution*
 * *TraceDistributionReader*: A reader for a trace file. [See format description](#tracefile-format).
 * *GaussianTimeDistribution*

##### TraceFile Format

Each line consists of three comma separated values. Comments start with a __#__. The first value describes the start time for the given percentage. The second value describes how long it takes to reach the given percentage. The last value is the percentage that should be reached at the end.

A trace file containing the following data produces the following trace.

```
# startTimeOfChange, lengthWhereChangeIsHappening, targetPercentage
1m, 1m, 0.5
4m, 2m, 0.7
```

![Trace Diagram](https://dev.kom.e-technik.tu-darmstadt.de/gitlab/maki/simonstrator-peerfact/raw/nr/monitoring-loadgen/src/de/tud/kom/p2psim/impl/loadgen/trace.png)

### Example load_generator.xml

```xml
<LoadGenerator class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.LoadGenerator" sampleRate="5m">
  <NodeSelection class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.nodeselection.StaticNodeSelection" />
  <Scenario class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Scenario" name="Test">
    <Request class="de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request">
      <RequestType class="de.tud.kom.p2psim.impl.loadgen.config.RequestType" attribute="PHY_LOCATION" type="PERIODIC" interval="5s" validity="20m">
        <Target class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Target" parameter="ENERGY_BATTERY_LEVEL" comparator=">" value="90" />
      </RequestType>
      <Source class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Source" parameter="ENERGY_BATTERY_LEVEL" comparator=">" value="40" />
      <Source class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Source" parameter="PHY_LOCATION" comparator="&lt;" value="10000" longitude="55" latitude="5" />
      <Distribution class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.distribution.UniformDistribution" percentage="0.4" minNodes="10" />
    </Request>
  </Scenario>
</LoadGenerator>
```

## Tips

Lets assume that you want to issue a request from nodes that have either more than 90 % or less than 30 % of Battery left. As the conditions for nodes are always concatinated with the AND-operation this Request has to be split into two separated requests.

```xml
<LoadGenerator ...>
  <NodeSelection ... />
  <Scenario ...>
    <!-- Request for BATTERY > 90% -->
    <Request ...>
      <RequestType ...>
        <Target ... />
      </RequestType>
      <Source class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Source" parameter="ENERGY_BATTERY_LEVEL" comparator=">" value="90" />
      <Distribution ... />
    </Request>

    <!-- Request for BATTERY < 30% -->
    <Request ...>
      <RequestType ...>
        <Target ... />
      </RequestType>
      <Source class="de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Source" parameter="ENERGY_BATTERY_LEVEL" comparator="<" value="30" />
      <Distribution ... />
    </Request>
  </Scenario>
</LoadGenerator>
```

## Caveats

Once a destination node has received through the `RequestInvokable`-Interface the overlay has to take care of routing the Request through the overlay.

Choose the `samplingRate` wisely. If the simulation time is long it is recommended that the sampling time is not too small as a lot of Events may be scheduled. In contrast to that when the simulation time is short do not choose a high sampling time as the Events may never get scheduled.

All conditions are concatinated with a logical AND. To use OR for concatination see [Tips](#tips).

Currently all data that can be requested and can be used for conditions of nodes are listed in [SiSTypes](https://dev.kom.e-technik.tu-darmstadt.de/gitlab/maki/simonstrator-api/blob/master/src/de/tudarmstadt/maki/simonstrator/api/component/sis/type/SiSTypes.java). When another data type is need, please add it there. The load generator will automatically take advantage of it.

