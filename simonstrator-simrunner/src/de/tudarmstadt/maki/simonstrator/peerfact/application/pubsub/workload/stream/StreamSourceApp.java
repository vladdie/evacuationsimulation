package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.handover.HandoverSensor;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.NotificationInfoImpl;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream.StreamModel.Block;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream.StreamModel.Chunk;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream.StreamModel.ChunkFactory;

/**
 * Stream source - a mobile client that can act as a stream source
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class StreamSourceApp extends StreamApp {

	private StreamOperation streamOp;

	private StreamModel streamModel;

	private ChunkFactory chunkFactory;

	private HandoverSensor handoverSensor;

	public StreamSourceApp(Host host) {
		super(host);
	}

	@Override
	public void initialize() {
		super.initialize();

		try {
			handoverSensor = getHost().getComponent(HandoverSensor.class);
		} catch (ComponentNotAvailableException e) {
			// no worries
		}

		if (Factory.useMdc) {
			// MDC
			assert Factory.bitrates.length == 1;
			chunkFactory = StreamModel.getMDCConstantBitrateFactory(Factory.chunksPerSecond, Factory.bitrates[0],
					Factory.mdcDescriptions, Factory.mdcMinDescriptions);
		} else if (Factory.bitrates.length == 1) {
			// Single-layer
			chunkFactory = StreamModel.getConstantBitrateFactory(Factory.chunksPerSecond, Factory.bitrates[0],
					Factory.blocksPerChunk);
		} else {
			// Multi-layer
			chunkFactory = StreamModel.getLayeredConstantBitrateFactory(Factory.chunksPerSecond,
					Factory.allLayersRequired, Factory.bitrates);
		}

		StreamDirectorApp.registerSource(this);
	}

	/**
	 * Start streaming from this source
	 */
	public void startStream(StreamModel streamModel) {
		assert streamOp == null;
		assert streamModel != null;
		this.streamModel = streamModel;
		this.streamModel.setChunkFactory(chunkFactory);
		streamOp = new StreamOperation();
		streamOp.start();
	}

	/**
	 * Stop streaming from this source
	 */
	public void stopStream() {
		assert streamOp != null;
		streamOp.stop();
		streamOp = null;
		streamModel = null;
	}

	/**
	 * True, if this source is actively streaming
	 * 
	 * @return
	 */
	public boolean isStreaming() {
		return streamOp != null;
	}

	/**
	 * True, if this source is currently connected to an access point.
	 * 
	 * @return
	 */
	public boolean isConnectedToAccessPoint() {
		return handoverSensor == null ? false : handoverSensor.isConnectedToAccessPoint();
	}

	public StreamModel getStreamModel() {
		return streamModel;
	}

	/**
	 * Publishes the current chunk. This might lead to multiple notifications,
	 * as a chunk might be further divided into multiple blocks.
	 * 
	 * @return the playout time of the chunk (delay until the next chunk is
	 *         generated)
	 */
	public long publishNextChunk() {
		Chunk nextChunk = streamModel.createChunk(getHost().getId().value());
		for (Block block : nextChunk.blocks) {
			// Create the notification
			List<Attribute<?>> attributes = new LinkedList<>();
			attributes.add(ATTR_CHUNK_ID.create(nextChunk.id));
			attributes.add(ATTR_BLOCK_ID.create(block.blockId));
			attributes.add(ATTR_BLOCK_REQUIRED.create(block.isRequired));
			attributes.add(ATTR_SRC_ID);
			Notification n = getPubSub().createNotification(ATTR_TOPIC, attributes, null);
			// Add fake payload
			n._getNotificationInfo(new NotificationInfoImpl(block.sizeInBytes, getHost().getId().value()));
			// Publish
			getPubSub().publish(n);
		}
		return nextChunk.chunkDuration;
	}


	/**
	 * Periodic operation with varying frequency depending on chunk playout
	 * times.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public class StreamOperation implements EventHandler {

		public static final int EVENT_STREAM_NEXT_CHUNK = 36;

		private boolean active = false;

		public void start() {
			Event.scheduleImmediately(this, null, EVENT_STREAM_NEXT_CHUNK);
			active = true;
		}

		public void stop() {
			active = false;
		}

		@Override
		public void eventOccurred(Object content, int type) {
			assert type == EVENT_STREAM_NEXT_CHUNK;
			if (!active) {
				return;
			}
			long delay = publishNextChunk();
			Event.scheduleWithDelay(delay, this, null, EVENT_STREAM_NEXT_CHUNK);
		}

	}

	/**
	 * The Factory
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class Factory implements HostComponentFactory {

		public static int chunksPerSecond = 5;

		public static long[] bitrates = null;

		public static int blocksPerChunk = 3;

		public static boolean useMdc = false;

		public static int mdcDescriptions = 5;

		public static int mdcMinDescriptions = 2;

		public static boolean allLayersRequired = true;

		@Override
		public HostComponent createComponent(Host host) {
			return new StreamSourceApp(host);
		}

		public static void setBitrate(long[] bitrate) {
			Factory.bitrates = bitrate;
		}

		public void setChunksPerSecond(int chunksPerSecond) {
			Factory.chunksPerSecond = chunksPerSecond;
		}

		public void setAllLayersRequired(boolean allLayersRequired) {
			Factory.allLayersRequired = allLayersRequired;
		}

		public void setBlocksPerChunk(int blocksPerChunk) {
			Factory.blocksPerChunk = blocksPerChunk;
		}

		public void setUseMdc(boolean useMdc) {
			Factory.useMdc = useMdc;
		}

		public void setMdcDescriptions(int mdcDescriptions) {
			Factory.mdcDescriptions = mdcDescriptions;
		}

		public void setMdcMinDescriptions(int mdcMinDescriptions) {
			Factory.mdcMinDescriptions = mdcMinDescriptions;
		}

	}

}
