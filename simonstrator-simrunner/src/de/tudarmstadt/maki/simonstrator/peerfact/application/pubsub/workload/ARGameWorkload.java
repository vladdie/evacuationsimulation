/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.tud.kom.p2psim.api.topology.Topology;
import de.tud.kom.p2psim.impl.topology.PositionVector;
import de.tudarmstadt.maki.simonstrator.api.Binder;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.LocationPubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.AttributeFilter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationActuator;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationListener;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubWorkloadApp;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.ARGameWorkload.ARNodeState;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.argame.AreaAttributeValue;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.argame.AreaIntersectsOperator;

/**
 * A workload for location-based games, includes some specific evaluation parts
 * as well.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, May 2, 2014
 */
public class ARGameWorkload extends PubSubWorkloadGenerator<ARNodeState> {

	public final static String TOPIC_POSITION_UPDATES_STRING = "position/update";

	private final static String ATTRIBUTE_NAME_POSITION = "position";
	private final static String ATTRIBUTE_NAME_POSITION_X = "pos-x";
	private final static String ATTRIBUTE_NAME_POSITION_Y = "pos-y";

	private final static int EVENT_PUBLISH_POSITION_UPDATE = 1;

	private final static int EVENT_SUBSCRIBE_TO_POSITION = 2;

	private Topic TOPIC_POSITION_UPDATES = null;

	protected PubSubWorkloadParameter radiusOfInterest;

	protected boolean useAreaAttribute = true;

	protected boolean useLocationAwarePubSub = false;

	protected boolean publishToRandomLocations = false;

	protected boolean publishToAttractionPoints = false;

	protected double publishToAttractionPointsFraction = 1.0;

	protected final Map<Long, PositionVector> realPositionsByHostId = new LinkedHashMap<>();

	private long locationUpdateInterval = -1;

	/**
	 * Node state within the AR-workload
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, May 2, 2014
	 */
	public class ARNodeState extends NodeState implements EventHandler,
			PubSubListener, LocationListener {

		// auto-updating instance
		private final PositionVector currentPosition;

		private final Attribute<AreaAttributeValue> areaAttributePrototype;

		private final Attribute<Double> xAttributePrototype;

		private final Attribute<Double> yAttributePrototype;

		private double lastSubX;

		private double lastSubY;

		private final LinkedHashSet<ARNodeState> nodesInAoi;
		
		protected Subscription lastPositionSubscription;

		public final long hostId;

		private boolean running;

		private double radiusOfInterest = -1;

		public ARNodeState(PubSubWorkloadApp app) {
			super(app);
			this.currentPosition = app.getHost().getTopologyComponent()
					.getRealPosition();
			this.hostId = app.getHost().getHostId();
			this.nodesInAoi = new LinkedHashSet<>();
			areaAttributePrototype = app.getPubsub().createAttribute(
					AreaAttributeValue.class, ATTRIBUTE_NAME_POSITION,
					new AreaAttributeValue(currentPosition, 0, hostId));
			xAttributePrototype = app.getPubsub().createAttribute(Double.class, ATTRIBUTE_NAME_POSITION_X, 0d);
			yAttributePrototype = app.getPubsub().createAttribute(Double.class, ATTRIBUTE_NAME_POSITION_Y, 0d);

			app.getHost().getTopologyComponent().requestLocationUpdates(null, this);
		}

		/**
		 * Global knowledge-view on all nodes within the AOI
		 * @return
		 */
		public LinkedHashSet<ARNodeState> getNodesInAoi() {
			return nodesInAoi;
		}

		public PositionVector getPosition() {
			return currentPosition;
		}

		public boolean isInAoi(PositionVector center) {
			return center.distanceTo(currentPosition) <= getAreaOfInterestRadius();
		}

		public Attribute<AreaAttributeValue> getCurrentAreaAttribute(
				double radius) {
			return areaAttributePrototype.create(new AreaAttributeValue(
					getPosition(), // ensure cloning constructor!
					radius, hostId));
		}

		/**
		 * Current position for notifications as x,y tuple
		 * 
		 * @return
		 */
		public List<Attribute<?>> getPositionAttributeTuple() {
			List<Attribute<?>> attrs = new LinkedList<>();
			attrs.add(xAttributePrototype.create(getPosition().getX()));
			attrs.add(yAttributePrototype.create(getPosition().getY()));
			return attrs;
		}

		public List<AttributeFilter<?>> getAreaOfInterestFilterTuple() {
			// TODO good-old x,y tuple - for subscriptions
			lastSubX = getPosition().getX();
			lastSubY = getPosition().getY();
			double roi = getAreaOfInterestRadius();
			List<AttributeFilter<?>> filters = new LinkedList<>();
			filters.add(app.getPubsub().createAttributeFilter(xAttributePrototype.create(lastSubX + roi),
					FilterOperator.LTE));
			filters.add(app.getPubsub().createAttributeFilter(xAttributePrototype.create(lastSubX - roi),
					FilterOperator.GTE));
			filters.add(app.getPubsub().createAttributeFilter(yAttributePrototype.create(lastSubY + roi),
					FilterOperator.LTE));
			filters.add(app.getPubsub().createAttributeFilter(yAttributePrototype.create(lastSubY - roi),
					FilterOperator.GTE));
			return filters;
		}

		public double getAreaOfInterestRadius() {
			return radiusOfInterest;
		}

		/**
		 * DOES NOT trigger automatic re-subscriptions.
		 * 
		 * @param radiusOfInterest
		 */
		public void setRadiusOfInterest(double radiusOfInterest) {
			this.radiusOfInterest = radiusOfInterest;
		}

		public void startWorkload() {
			assert !running;
			// Subscribe to own position, start publishing events
			Event.scheduleImmediately(ARNodeState.this, ARNodeState.this, EVENT_SUBSCRIBE_TO_POSITION);
			Event.scheduleImmediately(ARNodeState.this, ARNodeState.this, EVENT_PUBLISH_POSITION_UPDATE);
			running = true;
		}

		public void stopWorkload() {
			doUnsubscribe(lastPositionSubscription);
			running = false;
		}

		@Override
		public void onLocationChanged(Host host, Location location) {
			// Do we need to resubscribe?
			if (lastPositionSubscription != null && !useLocationAwarePubSub && !useAreaAttribute) {
				// yes.
				if (lastSubX != getPosition().getX() || lastSubY != getPosition().getY()) {
					/*
					 * TODO: we might want to add some tolerance. However, this
					 * will also impact coverage, as we would need to increase
					 * subscription size!
					 */
					// unsub old
					this.doUnsubscribe(lastPositionSubscription);
					lastPositionSubscription = getAreaOfInterestSubscription(this);
					this.doSubscribe(lastPositionSubscription, ARNodeState.this);
				}
			}
		}

		@Override
		public void eventOccurred(Object content, int type) {
			if (!running) {
				return;
			}
			ARNodeState state = (ARNodeState) content;

			switch (type) {
			case EVENT_PUBLISH_POSITION_UPDATE:
				state.doPublish(getPositionUpdateNotification(state));
				Event.scheduleWithDelay(
						(long) (1 * Time.SECOND / publicationsPerSecond
								.getCurrentValue()), this, state,
						EVENT_PUBLISH_POSITION_UPDATE);
				break;

			case EVENT_SUBSCRIBE_TO_POSITION:
				lastPositionSubscription = getAreaOfInterestSubscription(state);
				state.doSubscribe(lastPositionSubscription,
						ARNodeState.this);
				// updateNodesWithinAoi(this, nodesInAoi);
				break;

			default:
				throw new AssertionError("Unknown event type");
			}

		}

		@Override
		public void onNotificationArrived(Subscription matchedSubscription,
				Notification notification) {
			//
		}

		/**
		 * Is the workload active on the respective node?
		 * 
		 * @return
		 */
		public boolean isRunning() {
			return running;
		}

		@Override
		public String toString() {
			return "ARState " + hostId;
		}
	}

	@Override
	protected ARNodeState createNodeStateObject(PubSubWorkloadApp app) {
		ARNodeState nodeState = new ARNodeState(app);
		nodeState.setRadiusOfInterest(radiusOfInterest.getCurrentValue());
		realPositionsByHostId.put(nodeState.hostId, nodeState.currentPosition);
		return nodeState;
	}

	@Override
	protected void initialize() {
		if (useAreaAttribute) {
			/*
			 * Register the areaIntersects-operator on all pubsub-nodes!
			 */
			for (Host host : Oracle.getAllHosts()) {
				try {
					host.getComponent(PubSubComponent.class).setOperatorImplementation(FilterOperator.INTERSECTS,
							AreaAttributeValue.class, new AreaIntersectsOperator(0.0, true));
				} catch (ComponentNotAvailableException e) {
					continue;
				}
			}
		}
	}

	@Override
	protected void onPeerStatusChanged(ARNodeState state, PeerStatus newStatus) {
		if (newStatus == PeerStatus.PRESENT) {
			if (!state.isRunning()) {
				state.startWorkload();
			}
		} else {
			if (state.isRunning()) {
				state.stopWorkload();
			}
		}
	}

	private Random targetLocationsRnd = Randoms.getRandom("LocationTargetsWorkload");

	protected Location getRandomPosition() {
		PositionVector world = Binder.getComponentOrNull(Topology.class).getWorldDimensions();
		PositionVector rnd = new PositionVector(targetLocationsRnd.nextDouble() * world.getX(),
				targetLocationsRnd.nextDouble() * world.getY());
		return rnd;
	}

	private List<AttractionPoint> attractionPoints = null;

	protected Location getAttractionPointPosition() {
		if (attractionPoints == null) {
			attractionPoints = new LinkedList<>();
			/*
			 * We use global knowledge to retrieve the list of AttractionPoints
			 * from one of the clients.
			 */
			for (Host h : Oracle.getAllHosts()) {
				try {
					// check, if this is a client
					h.getComponent(BypassClientComponent.class);
					// get the location actuator
					LocationActuator actuator = h.getComponent(LocationActuator.class);
					attractionPoints.addAll(actuator.getAllAttractionPoints());
					// we only need to do this once.
					break;
				} catch (ComponentNotAvailableException e) {
					// don't care
				} catch (UnsupportedOperationException e) {
					// mobility models that do not have any APS
				}
			}
			if (attractionPoints.isEmpty()) {
				throw new AssertionError("No Attraction Points found.");
			}
		}
		return attractionPoints.get(targetLocationsRnd.nextInt(attractionPoints.size()));
	}

	/**
	 * A Notification for a position update
	 * 
	 * @param state
	 * @return
	 */
	protected Notification getPositionUpdateNotification(ARNodeState state) {
		if (TOPIC_POSITION_UPDATES == null) {
			TOPIC_POSITION_UPDATES = state.getApp().getPubsub()
					.createTopic(TOPIC_POSITION_UPDATES_STRING);
		}
		List<Attribute<?>> attrs = new LinkedList<Attribute<?>>();
		Notification n = null;
		if (useLocationAwarePubSub) {
			LocationPubSubComponent lPubSub = (LocationPubSubComponent) state.getApp().getPubsub();
			// Notifications do not have a RoI in this model.
			/*
			 * FIXME right now, the evaluation toolchain requires a X and Y
			 * attribute, even in case of a location-aware pub/sub
			 */
			attrs.addAll(state.getPositionAttributeTuple());
			if (publishToRandomLocations) {
				// just testing random LBS publications
				assert !publishToAttractionPoints;
				n = lPubSub.createNotification(TOPIC_POSITION_UPDATES, attrs, getRandomPosition(), 0, null);
			} else if (publishToAttractionPoints) {
				boolean local = rnd.nextDouble() > publishToAttractionPointsFraction;
				if (local) {
					n = lPubSub.createLocalNotification(TOPIC_POSITION_UPDATES, attrs, 0, null);
				} else {
					n = lPubSub.createNotification(TOPIC_POSITION_UPDATES, attrs, getAttractionPointPosition(), 0,
							null);
				}
			} else {
				n = lPubSub.createLocalNotification(TOPIC_POSITION_UPDATES, attrs, 0, null);
			}
		} else {
			if (useAreaAttribute) {
				// use custom area attribute
				attrs.add(state.getCurrentAreaAttribute(0));
			} else {
				// use x,y tuple
				attrs.addAll(state.getPositionAttributeTuple());
			}
			n = state.getApp().getPubsub().createNotification(TOPIC_POSITION_UPDATES, attrs, null);
		}
		// Set fake payload size
		n._getNotificationInfo(new NotificationInfoImpl((long) publicationSize
				.getCurrentValue(), state.hostId));
		return n;
	}

	/**
	 * A Subscription (dynamic) on the current position with the current AOI
	 */
	protected Subscription getAreaOfInterestSubscription(ARNodeState state) {
		if (TOPIC_POSITION_UPDATES == null) {
			TOPIC_POSITION_UPDATES = state.getApp().getPubsub()
					.createTopic(TOPIC_POSITION_UPDATES_STRING);
		}
		PubSubComponent ps = state.getApp().getPubsub();
		List<AttributeFilter<?>> attrFilters = new LinkedList<AttributeFilter<?>>();
		if (!useLocationAwarePubSub) {
			if (useAreaAttribute) {
				AttributeFilter<AreaAttributeValue> f = ps.createAttributeFilter(
						state.getCurrentAreaAttribute(state.getAreaOfInterestRadius()), FilterOperator.INTERSECTS);
				attrFilters.add(f);
			} else {
				attrFilters.addAll(state.getAreaOfInterestFilterTuple());
			}
		}
		Filter filter = ps.createFilter(attrFilters);
		Subscription sub;

		/*
		 * Use the location-based subscription if available
		 */
		if (useLocationAwarePubSub) {
			if (ps instanceof LocationPubSubComponent) {
				LocationSensor locSensor;
				try {
					locSensor = state.getApp().getHost().getComponent(LocationSensor.class);
				} catch (ComponentNotAvailableException e) {
					throw new AssertionError("Location Sensor is required");
				}
				LocationRequest req = locSensor.getLocationRequest();
				// Update interval:
				if (locationUpdateInterval == -1) {
					// FIXME: (JZ) Why the division by pubs per second?
					req.setInterval((long) (1 * Time.SECOND / publicationsPerSecond.getCurrentValue()));
				} else {
					req.setInterval(locationUpdateInterval);
				}
				req.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
				sub = ((LocationPubSubComponent) ps).createSubscription(TOPIC_POSITION_UPDATES, filter, req,
						state.getAreaOfInterestRadius());
			} else {
				throw new AssertionError("you want to use LocationAwarePubSub, which is not available.");
			}
		} else {
			sub = ps.createSubscription(TOPIC_POSITION_UPDATES, filter);
		}

		// Attach subscription info object for analyzing
		sub._getSubscriptionInfo(new SubscriptionInfoImpl(state.getApp().getHost().getId()));

		return sub;
	}

	/**
	 * The radius of the area of interest
	 */
	public void setRadiusOfInterest(PubSubWorkloadParameter radiusOfInterest) {
		this.radiusOfInterest = radiusOfInterest;
	}

	/**
	 * Are we relying on a location-aware pub/sub implementation?
	 * 
	 * @param locationAware
	 */
	public void setUseLocationAwarePubSub(boolean locationAware) {
		this.useLocationAwarePubSub = locationAware;
	}

	/**
	 * Enables/disables the use of our custom AreaAttribute. If set to false, x
	 * and y are used instead.
	 * 
	 * @param useAreaAttribute
	 */
	public void setUseAreaAttribute(boolean useAreaAttribute) {
		this.useAreaAttribute = useAreaAttribute;
	}

	/**
	 * Interval between location updates (default: on every publication). Only
	 * used if useLocationAwarePubSub is set to true as well.
	 * 
	 * @param locationUpdateInterval
	 */
	public void setLocationUpdateInterval(long locationUpdateInterval) {
		this.locationUpdateInterval = locationUpdateInterval;
	}

	/**
	 * Instead of publishing locally. publish to random locations.
	 * 
	 * @param publishToRandomLocations
	 */
	public void setPublishToRandomLocations(boolean publishToRandomLocations) {
		this.publishToRandomLocations = publishToRandomLocations;
	}

	/**
	 * Instead of publishing to our own location, publish to a random Attraction
	 * Point
	 * 
	 * @param publishToAttractionPoints
	 */
	public void setPublishToAttractionPoints(boolean publishToAttractionPoints) {
		this.publishToAttractionPoints = publishToAttractionPoints;
	}

	/**
	 * If publishToAttractionPoints is true, this controls the fraction of
	 * events that are published to attraction points. The remaining events are
	 * published to our own location instead.
	 * 
	 * @param publishToAttractionPointsFraction
	 */
	public void setPublishToAttractionPointsFraction(double publishToAttractionPointsFraction) {
		this.publishToAttractionPointsFraction = publishToAttractionPointsFraction;
	}


}
