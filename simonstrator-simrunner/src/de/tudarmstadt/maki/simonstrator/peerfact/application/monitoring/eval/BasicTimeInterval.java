package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import de.tudarmstadt.maki.simonstrator.api.Time;

/**
 * Basic implementation of {@link TimeInterval}.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 18.06.2014
 *
 */
public class BasicTimeInterval implements TimeInterval{
	private long startTimestamp;
	private long intervalLength;

	/**
	 * Default constructor for initializing a {@link BasicTimeInterval}. Start timestamp is taken in this constructor.
	 */
	public BasicTimeInterval() {
		this.startTimestamp = Time.getCurrentTime();
	}

	/**
	 * The finishing constructor, when a {@link BasicTimeInterval} should finish.
	 * @param beginningInterval
	 */
	public BasicTimeInterval(BasicTimeInterval beginningInterval) {
		this.startTimestamp = beginningInterval.getStartTimestamp();
		this.intervalLength = Time.getCurrentTime() - startTimestamp;
	}

	@Override
	public long getStartTimestamp() {
		return startTimestamp;
	}

	@Override
	public long getIntervalLength() {
		return intervalLength;
	}
	
}
