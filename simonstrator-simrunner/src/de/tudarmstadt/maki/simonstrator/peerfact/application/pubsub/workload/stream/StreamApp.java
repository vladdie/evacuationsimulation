package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.StreamAttributes;

/**
 * Base class for the stream apps.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public abstract class StreamApp implements HostComponent, IPeerStatusListener {

	private final Host host;

	private PubSubComponent pubSub;

	protected Attribute<Integer> ATTR_CHUNK_ID;

	protected Attribute<Long> ATTR_SRC_ID;

	protected Attribute<Short> ATTR_BLOCK_ID;

	protected Attribute<Boolean> ATTR_BLOCK_REQUIRED;

	protected Topic ATTR_TOPIC;

	public StreamApp(Host host) {
		this.host = host;
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		// TODO Auto-generated method stub

	}

	@Override
	public void initialize() {
		// bind pubsub
		try {
			pubSub = host.getComponent(PubSubComponent.class);
			pubSub.addPeerStatusListener(this);
			ATTR_TOPIC = pubSub.createTopic(StreamAttributes.TOPIC_ROOT);
			ATTR_CHUNK_ID = pubSub.createAttribute(Integer.class, StreamAttributes.CHUNK_ID, 0);
			ATTR_BLOCK_ID = pubSub.createAttribute(Short.class, StreamAttributes.BLOCK_ID, (short) 0);
			ATTR_BLOCK_REQUIRED = pubSub.createAttribute(Boolean.class, StreamAttributes.BLOCK_REQUIRED, true);
			ATTR_SRC_ID = pubSub.createAttribute(Long.class, StreamAttributes.SRC_ID, host.getId().value());
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("Requires Pub/Sub");
		}
	}

	public PubSubComponent getPubSub() {
		return pubSub;
	}

	@Override
	public void shutdown() {
		// not needed
	}

	@Override
	public Host getHost() {
		return host;
	}

}
