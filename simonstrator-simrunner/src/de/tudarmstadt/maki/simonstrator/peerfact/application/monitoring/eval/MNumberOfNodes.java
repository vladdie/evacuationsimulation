package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval.MNumberOfNodes.MNumberOfNodesValue;

/**
 * 
 * @author Nils Richerzhagen
 *
 */
public class MNumberOfNodes extends AbstractMetric<MNumberOfNodesValue> implements Metric<MNumberOfNodesValue> {

	public MNumberOfNodes() {
		super("Number of online Nodes", MetricUnit.NONE);
	}
	/**
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	protected class MNumberOfNodesValue
			implements de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue<Integer> {

		@Override
		public Integer getValue() {
			int numberOfNodes = 0;
			boolean valid = false;
			for (Host host : Oracle.getAllHosts()) {
				try {
					CraterNodeComponent comp = (CraterNodeComponent) host.getComponent(CraterNodeComponent.class);
					if (comp.isPresent()) {
						numberOfNodes++;
					}
					valid = true;
				} catch (ComponentNotAvailableException e) {
					// don't care
				}
			}
			if (valid) {
				return numberOfNodes;
			} else {
				return -1;
			}

		}

		@Override
		public boolean isValid() {
			return getValue() != -1;
		}

	}

	@Override
	public void initialize(List<Host> hosts) {
		setOverallMetric(new MNumberOfNodesValue());
	}

}
