package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen;

import java.util.ArrayList;

import de.tud.kom.p2psim.impl.common.DefaultHost;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;

/**
 * Node filtering methods
 * 
 * @author Marc Schiller
 *
 */
public class Filter {

	/**
	 * Fetch all NODES that are currently online
	 * 
	 * @return a list of node-IDs that are online
	 */
	public static ArrayList<INodeID> getActiveNodes() {
		ArrayList<INodeID> onlineNodes = new ArrayList<>();

		for (Host host : Oracle.getAllHosts()) {

			try {
				// FIXME: StackOverflow when requesting SiSTypes from Cloud
				if (((DefaultHost) host).getProperties().getGroupID()
						.equals("Nodes")
						&& host.getComponent(OverlayComponent.class)
								.isPresent()) {
					onlineNodes.add(host.getId());
				}
			} catch (ComponentNotAvailableException e) {
				// Ignore host if overlay component is not present or is not a
				// node
			}
		}

		return onlineNodes;
	}

	/**
	 * Fetch all NODES that are present in the network
	 * 
	 * @return a list of all nodeids
	 */
	public static ArrayList<INodeID> getAllNodeIDs() {
		ArrayList<INodeID> allNodeIDs = new ArrayList<>();
		for (Host h : Oracle.getAllHosts()) {
			// FIXME: StackOverflow when requesting SiSTypes from Cloud
			if (((DefaultHost) h).getProperties().getGroupID()
					.equals("Nodes")) {
				allNodeIDs.add(h.getId());
			}
		}
		return allNodeIDs;
	}
}
