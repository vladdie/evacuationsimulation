package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

/**
 * Interface for a time interval for analyzing purposes.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 18.06.2014
 *
 */
public interface TimeInterval {
	/**
	 * Returning the overall interval length.
	 * @return
	 */
	public long getIntervalLength();
	
	/**
	 * Returning the start timestamp of that interval.
	 * @return
	 */
	public long getStartTimestamp();
}
