package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.distribution;

import org.apache.commons.math.distribution.NormalDistributionImpl;

import de.tud.kom.p2psim.impl.scenario.DefaultConfigurator;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

/**
 * returns a gaussian-like curve for selection. Uses gaussian density function
 * with the time as input and the percentage as output.
 * 
 * @author Simon
 * @version 1.0, 16.06.2016
 */
public final class GaussianTimeDistribution implements AbstractDistribution {

	private NormalDistributionImpl normal;

	private int minNumNodes;

	@XMLConfigurableConstructor({ "meanTime", "stdev", "minNumNodes" })
	public GaussianTimeDistribution(String meanTime, String stdev,
			String minNumNodes) {
		double dMeanTime = DefaultConfigurator
				.parseNumber(meanTime.replaceAll("\\s+", ""), Double.class);
		double dStdev = DefaultConfigurator
				.parseNumber(stdev.replaceAll("\\s+", ""), Double.class);
		int iMinNumNodes = DefaultConfigurator
				.parseNumber(minNumNodes.replaceAll("\\s+", ""), Integer.class);

		if (dMeanTime < 0 || dStdev <= 0 || iMinNumNodes < 0) {
			System.err.println("Wrong inputs for gaussian");
			normal = new NormalDistributionImpl(0, 1);
			this.minNumNodes = 0;
		} else {
			normal = new NormalDistributionImpl(dMeanTime, dStdev);
			this.minNumNodes = iMinNumNodes;
		}
	}

	/**
	 * takes the density at a certain time point times numberOfFilteredNodes
	 */
	@Override
	public int getCount(long timePoint, int numberOfFilteredNodes) {
		int gaussOutput = (int) Math
				.round(normal.density(timePoint) * numberOfFilteredNodes);

		if (gaussOutput < minNumNodes)
			return minNumNodes;
		else
			return gaussOutput;
	}
}
