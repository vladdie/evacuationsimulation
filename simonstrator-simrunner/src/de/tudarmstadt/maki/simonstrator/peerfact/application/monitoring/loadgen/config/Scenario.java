package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config;

import java.util.HashSet;

import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

/**
 * A loader representing a scenario containing several requests
 * 
 * @author Marc Schiller
 * @version 1.0, May 30, 2016
 */
public class Scenario {

	// Storage
	private String name = "";

	private HashSet<RequestContainer> requests = new HashSet<>();

	/**
	 * Constructor
	 * 
	 * @param name
	 *            the name of the scenario
	 */
	@XMLConfigurableConstructor({ "name" })
	public Scenario(String name) {
		this.setName(name);
	}

	/**
	 * Set the name of the scenario
	 * 
	 * @param name
	 */
	private void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for the name
	 * 
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Add a request to the scenario
	 * 
	 * @param request
	 */
	public void setRequestContainer(RequestContainer request) {
		this.requests.add(request);
	}

	/**
	 * Get all requests in this scenario
	 * 
	 * @return a HashSet of Requests
	 */
	public HashSet<RequestContainer> getRequests() {
		return this.requests;
	}

	/**
	 * Pretty print
	 */
	public String toString() {

		String retString = "==============================\n";
		retString += "Scenarioname: " + this.name + "\n";
		retString += "Requests:\n";

		for (RequestContainer req : this.requests) {
			retString += req.toString();
			retString += "\t------------------------------\n";
		}

		retString += "==============================\n";

		return retString;
	}
}
