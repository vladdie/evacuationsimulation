package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessage;

/**
 * Delay of {@link CraterLocalMessage}s in the network. Only for
 * {@link DataMessage}s.
 * 
 * @author Nils Richerzhagen
 *
 */
public class MCraterLocalMessageDelay {


}
