package de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.stream.Collectors;

import javax.swing.JComponent;

import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;

/**
 * This component adds the possibility to select an rectangular area
 * in the visualisation. Corresponding {@link AreaSelectionListener}s will be notified
 * upon changes.
 *
 * @author Clemens Krug
 */
public class SelectionComponent extends JComponent implements VisualizationTopologyView.VisualizationInjector.MouseListener
{
    private Point mousePressed;
    private Point mouseDragged;
    private BufferedImage image;

    private LinkedList<AreaSelectionListener> listeners;

    /**
     * Areas which have been selected
     */
    private ArrayList<Rectangle> monitoredAreas;

    private boolean needRedraw = false;

    public SelectionComponent()
    {
        listeners = new LinkedList<>();
        monitoredAreas = new ArrayList<>();

        de.tudarmstadt.maki.simonstrator.api.Event.scheduleImmediately(new EventHandler()
        {
            @Override
            public void eventOccurred(Object content, int type)
            {
                setBounds(0, 0, VisualizationTopologyView.VisualizationInjector.getWorldX(),
                        VisualizationTopologyView.VisualizationInjector.getWorldY());
                VisualizationTopologyView.VisualizationInjector.injectComponent("DRASSelector", 999, SelectionComponent.this);
                VisualizationTopologyView.VisualizationInjector.addMouseListener(SelectionComponent.this);
                image = new BufferedImage(VisualizationTopologyView.VisualizationInjector.getWorldX(),
                        VisualizationTopologyView.VisualizationInjector.getWorldY(), BufferedImage.TYPE_INT_ARGB);
            }
        }, null, 0);

    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        if (mousePressed != null && mouseDragged != null)
        {
            g.drawRect(mousePressed.x, mousePressed.y, mouseDragged.x - mousePressed.x, mouseDragged.y - mousePressed.y);
        }

        if (needRedraw) g.drawImage(image, 0, 0, null);

    }

    private void redraw()
    {
        Graphics2D g2 = image.createGraphics();
        g2.setBackground(new Color(255, 255, 255, 0));
        g2.clearRect(0, 0, VisualizationTopologyView.VisualizationInjector.getWorldX(), VisualizationTopologyView.VisualizationInjector.getWorldY());

        g2.setColor(new Color(0, 0, 255, 100));
        monitoredAreas.stream().forEach(r -> g2.fillRect(r.x, r.y, r.width, r.height));

        g2.dispose();
        needRedraw = true;
    }

    public void addListener(AreaSelectionListener listener)
    {
        listeners.add(listener);
    }

    @Override
    public void mousePressed(int x, int y)
    {
        mousePressed = new Point(x, y);
    }

    @Override
    public void mouseReleased(int x, int y)
    {
        if (mousePressed != null && mouseDragged != null)
        {
            if (mouseDragged.x > mousePressed.x && mouseDragged.y > mousePressed.y)
            {
                Rectangle r = new Rectangle(mousePressed.x, mousePressed.y, mouseDragged.x - mousePressed.x, mouseDragged.y - mousePressed.y);
                monitoredAreas.add(r);
                listeners.forEach(l -> l.areaAdded(r));
                redraw();
                mousePressed = null;
                mouseDragged = null;
            }
        }

    }

    @Override
    public void mouseDragged(int x, int y)
    {
        mouseDragged = new Point(x, y);
    }

    @Override
    public void mouseClicked(int x, int y, int modifier)
    {
        int sizeBefore = monitoredAreas.size();
        LinkedList<Rectangle> toRemove = monitoredAreas.stream().filter(r -> r.contains(x, y)).collect(Collectors.toCollection(LinkedList<Rectangle>::new));
        monitoredAreas.removeAll(toRemove);
        toRemove.forEach(r -> listeners.forEach(l -> l.areaRemoved(r)));
        int sizeAfter = monitoredAreas.size();

        if (sizeAfter != sizeBefore) redraw();
    }

    /**
     * Called when an area has been removed via "close"-Button
     * @param id The area which has been removed, represanted as the hash-code of the rectangle.
     */
    public void areaOnClickRemoved(long id)
    {
        for(Rectangle r : monitoredAreas)
        {
            if(r.hashCode() == id)
            {
                monitoredAreas.remove(r);
                listeners.forEach(l -> l.areaRemoved(r));
                redraw();
                return;
            }
        }
    }
}
