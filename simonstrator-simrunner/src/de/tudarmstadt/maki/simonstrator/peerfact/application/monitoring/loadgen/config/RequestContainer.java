package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config;

import java.util.HashSet;

import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request;
import de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.distribution.AbstractDistribution;

/**
 * A class representing one request
 * 
 * @author Marc Schiller
 * @version 1.0, May 30, 2016
 */
public class RequestContainer {

	// Storage
	private Request request;

	private HashSet<Source> sources = new HashSet<>();

	private AbstractDistribution distribution;

	/**
	 * Constructor
	 */
	@XMLConfigurableConstructor({})
	public RequestContainer() {
	}

	/**
	 * Set the requestType for this request
	 * 
	 * @param request
	 */
	public void setRequest(Request request) {
		this.request = request;
	}

	/**
	 * Add a source to this request
	 * 
	 * @param source
	 */
	public void setSource(Source source) {
		this.sources.add(source);
	}

	/**
	 * Set the distribution for this request
	 * 
	 * @param distribution
	 */
	public void setDistribution(AbstractDistribution distribution) {
		this.distribution = distribution;
	}

	/**
	 * Get the Distribution of this Request
	 * 
	 * @return
	 */
	public AbstractDistribution getDistribution() {
		return distribution;
	}

	/**
	 * Get the conditions for sources (those nodes who request data from other
	 * nodes) of this request
	 * 
	 * @return
	 */
	public HashSet<Source> getSources() {
		return this.sources;
	}

	/**
	 * Get the request that should be triggered from the Sources
	 * 
	 * @return
	 */
	public Request getRequest() {
		return this.request;
	}

	/**
	 * Pretty print the Request
	 */
	public String toString() {
		String retString = "\tRequest: " + this.request.toString();
		retString += "\tDistribution: " + this.distribution.toString() + "\n";

		for (Source src : this.sources) {
			retString += "\tSource: " + src.toString() + "\n";
		}

		return retString;
	}

}
