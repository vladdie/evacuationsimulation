package de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.peerfact.application.dras.eval.DRASLocalAnalyzerImpl;

/**
 * Visualisation class that holds all {@link DRASLocalNodeView}s.
 */
public class DRASLocalMonitor extends JFrame
{
    /**
     * Used to track the order of the monitored nodes. Needed for proper
     * arrangement of the {@link DRASLocalNodeView}s in the visualisation.
     */
    private ArrayList<DRASLocalNodeView> nodeOrder = new ArrayList<>();

    private JPanel panel;
    private JScrollPane scrollPanel;
    private DRASLocalAnalyzerImpl analyzer;
    private HashMap<Long, DRASLocalNodeView> nodeViews = new HashMap<>();

    //Default panel size. Can be altered via XML
    private int panelSize = 400;

    public DRASLocalMonitor(DRASLocalAnalyzerImpl analyzer)
    {
        this.analyzer = analyzer;
        SwingUtilities.invokeLater(this::createGUI);
    }

    /**
     * Sets up GUI, but doesn't set it visible.
     */
    public void createGUI()
    {
        panel = new JPanel();
        panel.setLayout(null);

        scrollPanel = new JScrollPane(panel,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        scrollPanel.setPreferredSize(new Dimension(panelSize, panelSize));

        getContentPane().add(scrollPanel);

        pack();
    }

    /**
     * Update the {@link DRASLocalNodeView} of a specified monitored node
     * @param nodeID The node of which the {@link DRASLocalNodeView} should be updated.
     * @param noUMTSLocations updated positions without umts connection
     * @param netAvailableLocations updated position with umts connection
     */
	public void updateView(Long nodeID, List<Map.Entry<UniqueID, Location>> noUMTSLocations,
			List<Map.Entry<UniqueID, Location>> netAvailableLocations)
    {
        DRASLocalNodeView view = nodeViews.get(nodeID);
        if(view != null) view.update(noUMTSLocations, netAvailableLocations);
    }


    /**
     * Start monitoring another node. Adds a {@link DRASLocalNodeView} to this windows.
     * @param id id of the node which should be monitored
     */
    public void addNodeView(long id)
    {
        SwingUtilities.invokeLater(() -> {
            int viewCount = nodeViews.size();
            DRASLocalNodeView nodeView = new DRASLocalNodeView(id, this);
            nodeViews.put(id, nodeView);
            nodeOrder.add(nodeView);

            nodeView.setBounds(viewCount % 3 * panelSize, viewCount / 3 * panelSize, panelSize, panelSize);

            scrollPanel.setPreferredSize(new Dimension(viewCount < 3 ? (viewCount+1)*panelSize : panelSize*3,
                                                        viewCount / 3 * panelSize + panelSize));
            panel.add(nodeView);

            pack();
            panel.repaint();

            if(viewCount == 0) setVisible(true);
        });
    }

    /**
     * Stop monitoring this node. Removes the {@link DRASLocalNodeView} from this windows.
     * @param id id of the node which monitoring window should be removed
     */
    public void removeNodeView(long id)
    {
        SwingUtilities.invokeLater(() -> {
            DRASLocalNodeView nodeView = nodeViews.get(id);
            nodeView.onStop();
            nodeViews.remove(id);
            panel.remove(nodeView);

            //Algorithm for rearranging the DRASLocalNodeViews, so that no gaps appear.
            for(int i = nodeOrder.indexOf(nodeView)+1 ; i < nodeOrder.size(); i++)
            {
                DRASLocalNodeView cur = nodeOrder.get(i);
                int newX, newY;

                if(cur.getX() == 0)
                {
                    newX = 2*panelSize;
                    newY = cur.getY()-panelSize;
                }
                else
                {
                    newX = cur.getX()-panelSize;
                    newY = cur.getY();
                }

                cur.setLocation(newX, newY);

            }

            int viewCount = nodeViews.size();

            scrollPanel.setPreferredSize(new Dimension(viewCount < 3 ? (viewCount)*panelSize : 3*panelSize,
                    (viewCount-1) / 3 * panelSize + panelSize));
            panel.repaint();
            pack();

            if(nodeViews.size() < 1) setVisible(false);
        });
    }

    /**
     * Forwarding method to remove monitored areas/nodes via closing button
     * in the {@link DRASLocalNodeView}s
     * @param id id which should be removed
     */
    public void removeNodeOnClick(long id)
    {
        analyzer.onHostClick(id, false);
    }

    /**
     * Set panel size of this monitor and accordingly the panel
     * size of the {@link DRASLocalNodeView}s
     * @param size
     */
    public void setPanelSize(int size)
    {
        panelSize = size;
        panel.setPreferredSize(new Dimension(panelSize*3, panelSize*4));
        DRASLocalNodeView.setPanelSize(panelSize);
    }

    /**
     * Check if the specified id is monitored by itself
     * @param id The id to check on
     * @return <code>true</code> if the id is monitored by itself,
     * <code>false</code> if not or only monitored within a group.
     */
    public boolean isMonitored(long id)
    {
        return nodeViews.containsKey(id);
    }



}
