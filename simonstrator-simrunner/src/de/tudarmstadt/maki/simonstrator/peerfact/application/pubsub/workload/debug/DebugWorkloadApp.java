package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.debug;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.AttributeFilter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.NodeType;

/**
 * VERY simple workload app that is intended to be used with an actions.dat
 * file.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class DebugWorkloadApp implements HostComponent, PubSubListener {

	private final Host host;

	private BypassPubSubComponent pubsub;

	private Map<String, Subscription> subs = new LinkedHashMap<>();

	private Attribute<Integer> attrX;

	private Attribute<Integer> attrY;

	private int nodeTypeId = -1;

	private static int nTypeId = 0;

	private static NodeType prevNodeType = null;

	public DebugWorkloadApp(Host host) {
		this.host = host;
	}

	/*
	 * Methods to be called in the actions.dat
	 */

	/**
	 * Publish to the given topic with the provided attributes
	 * 
	 * @param topic
	 * @param x
	 * @param y
	 */
	public void publish(int nClient, String topic, int x, int y) {
		if (nodeTypeId != nClient) {
			// not this host
			return;
		}
		Topic t = pubsub.createTopic(topic);
		List<Attribute<?>> attributes = new LinkedList<>();
		attributes.add(attrX.create(x));
		attributes.add(attrY.create(y));
		Notification pub = pubsub.createNotification(t, attributes, null);
		System.out.println(Time.getFormattedTime() + " DebugWorkloadAPP: " + nClient + " publishing " + pub.toString());
		pubsub.publish(pub);
	}

	/**
	 * Send a subscription on the given host
	 * 
	 * @param subname
	 *            name used to identify the subscription lateron
	 * @param topic
	 *            topic of the subscription
	 * @param fromX
	 * @param toX
	 * @param fromY
	 * @param toY
	 */
	public void subscribe(int nClient, String subname, String topic, int fromX, int toX, int fromY, int toY) {
		if (nodeTypeId != nClient) {
			// not this host
			return;
		}
		Topic t = pubsub.createTopic(topic);
		List<AttributeFilter<?>> filters = new LinkedList<>();
		filters.add(pubsub.createAttributeFilter(attrX.create(fromX), FilterOperator.GTE));
		filters.add(pubsub.createAttributeFilter(attrX.create(toX), FilterOperator.LTE));
		filters.add(pubsub.createAttributeFilter(attrY.create(fromY), FilterOperator.GTE));
		filters.add(pubsub.createAttributeFilter(attrY.create(toY), FilterOperator.LTE));
		Subscription sub = pubsub.createSubscription(t, pubsub.createFilter(filters));
		subs.put(subname, sub);
		System.out.println(
				Time.getFormattedTime() + " DebugWorkloadAPP: " + nClient + " subscribing to " + sub.toString());
		pubsub.subscribe(sub, this);
	}

	/**
	 * Unsubscribe
	 * 
	 * @param subname
	 */
	public void unsubscribe(int nClient, String subname) {
		if (nodeTypeId != nClient) {
			// not this host
			return;
		}
		Subscription sub = subs.get(subname);
		if (sub == null) {
			System.err.println(Time.getFormattedTime() + " DebugWorkloadAPP: subscription " + subname
					+ " not available on " + nClient);
		} else {
			pubsub.unsubscribe(sub, null);
		}
	}

	/*
	 * Subscription-callback
	 */
	public void onNotificationArrived(Subscription matchedSubscription, Notification notification) {
		System.out.println(Time.getFormattedTime() + " DebugWorkloadAPP: host " + getHost().getId()
				+ " received notification " + notification + ", matching subscription: " + matchedSubscription);
	}

	/*
	 * HostComponent-Stuff
	 */

	@Override
	public void initialize() {
		//
		try {
			pubsub = getHost().getComponent(BypassPubSubComponent.class);
			if (pubsub.getNodeType() != prevNodeType) {
				prevNodeType = pubsub.getNodeType();
				nTypeId = 0;
			}
			nodeTypeId = ++nTypeId;
			attrX = pubsub.createAttribute(Integer.class, "x", 0);
			attrY = pubsub.createAttribute(Integer.class, "y", 0);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError();
		}
	}

	@Override
	public void shutdown() {
		//
	}

	@Override
	public Host getHost() {
		return host;
	}

	public static class Factory implements HostComponentFactory {

		@Override
		public HostComponent createComponent(Host host) {
			return new DebugWorkloadApp(host);
		}

	}

}
