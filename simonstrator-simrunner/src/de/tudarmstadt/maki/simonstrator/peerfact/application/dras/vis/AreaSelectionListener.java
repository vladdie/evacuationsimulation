package de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis;

import java.awt.*;

/**
 * This listener is notified when a selection has been made
 * or revoked in a {@link SelectionComponent}.
 *
 * @author Clemens Krug
 */
public interface AreaSelectionListener
{
    /**
     * Called when a new selection has been made
     * @param r selected area
     */
    void areaAdded(Rectangle r);

    /**
     * Called when a selection has been removed
     * @param r removed area
     */
    void areaRemoved(Rectangle r);

}
