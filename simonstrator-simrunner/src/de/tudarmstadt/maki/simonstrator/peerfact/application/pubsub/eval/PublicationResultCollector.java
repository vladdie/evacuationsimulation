package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.PubSubNotificationForwardingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.vis.PubSubAppVisualization;

/**
 * This objects collects all metrics related to a single publication. It is
 * persisted (written to the database) via an adapter that allows custom
 * handling (e.g., write each publication to the DB, write only averages, use
 * metrics, use custom table, etc.)
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class PublicationResultCollector {

	private final PubSubComponent initiator;

	public final INodeID initiatorHostID;

	public final Notification publication;

	private final List<PubSubGlobalKnowledgeNodeState> globalKnowledgeReceivers;

	private final List<PubSubGlobalKnowledgeNodeState> reachedReceivers;

	private final List<PubSubGlobalKnowledgeNodeState> unintendedReachedReceivers;

	private final int totalNumberOfInterestedReceivers;

	private int reachedRecipients = 0;

	// DAO wants to operate on double lateron
	private final Map<PubSubGlobalKnowledgeNodeState, Double> deliveryDelays;

	private final Map<PubSubGlobalKnowledgeNodeState, Double> numberOfDeliveries;

	public final long startedTimestamp = Time.getCurrentTime();

	private long firstDeliveryDelay = -1;

	private long lastDeliveryDelay = -1;

	private double deliveryRecall = -1.0;

	private double deliveryPrecision = -1.0;

	private long averageDeliveryDelay = -1;

	private double averageNumberOfDeliveriesPerSubscriber = -1;

	private int numberOfMatchOperations = -1;

	private int numberOfGatewaysUsed = 0;

	private boolean valid = false;

	private boolean finalized = false;

	private final List<ForwarderInfo> forwarders;

	public PublicationResultCollector(PubSubComponent initiator, Notification publication,
			List<PubSubGlobalKnowledgeNodeState> globalKnowledgeReceivers) {
		this.initiator = initiator;
		this.initiatorHostID = initiator.getHost().getId();
		this.publication = publication;
		this.globalKnowledgeReceivers = globalKnowledgeReceivers;
		this.totalNumberOfInterestedReceivers = globalKnowledgeReceivers.size();
		this.deliveryDelays = new LinkedHashMap<PubSubGlobalKnowledgeNodeState, Double>();
		this.numberOfDeliveries = new LinkedHashMap<PubSubGlobalKnowledgeNodeState, Double>();
		this.reachedReceivers = new LinkedList<PubSubGlobalKnowledgeNodeState>();
		this.unintendedReachedReceivers = new LinkedList<PubSubGlobalKnowledgeNodeState>();
		this.forwarders = new LinkedList<ForwarderInfo>();
	}

	/*
	 * TODO handle special case, where a publication is matched by none of the
	 * active subscriptions! Should be done after timeout (or directly in the
	 * constructor?)
	 */

	/**
	 * This method is only used by the visualization.
	 * 
	 * @return
	 */
	public List<ForwarderInfo> getForwarders() {
		return forwarders;
	}

	/**
	 * Number of gateways used to deliver this publication.
	 * 
	 * @param numberOfGateways
	 */
	public void setNumberOfGateways(int numberOfGateways) {
		this.numberOfGatewaysUsed = numberOfGateways;
	}

	public void notificationWasMatched(SubscriptionScheme.SchemeName scheme, int numberOfSubscriptions) {
		if (numberOfMatchOperations == -1) {
			numberOfMatchOperations = 0;
		}
		/*
		 * TODO here, we do not (yet) distinguish between schemes (e.g.,
		 * matching complexity).
		 */
		numberOfMatchOperations += numberOfSubscriptions;
	}

	public void notificationArrivedAt(PubSubGlobalKnowledgeNodeState comp) {
		if (finalized) {
			Monitor.log(PubSubGlobalKnowledge.class, Level.DEBUG,
					"Notification arrived after App-Level Finalization (after a timeout)!", comp);
			return;
		}
		assert comp != null;

		if (comp.hostId.equals(initiator.getHost().getId())) {
			/*
			 * Self-notification! The publisher itself is excluded from the
			 * Global Knowledge interested receivers, but we have to ensure this
			 * does not skew any other results. Just ignore this call.
			 */
			return;
		}

		if (reachedReceivers.contains(comp)) {
			/*
			 * TODO how to handle duplicate results?
			 */
			assert numberOfDeliveries.containsKey(comp);
			assert numberOfDeliveries.get(comp) >= 1;
			numberOfDeliveries.put(comp, numberOfDeliveries.get(comp) + 1);
		} else if (globalKnowledgeReceivers.contains(comp)) {
			// intended receive
			assert reachedRecipients < totalNumberOfInterestedReceivers;
			long delay = Time.getCurrentTime() - startedTimestamp;
			assert delay > 0;
			reachedReceivers.add(comp);
			deliveryDelays.put(comp, (double) delay);
			numberOfDeliveries.put(comp, 1d);
			if (firstDeliveryDelay == -1) {
				assert reachedRecipients == 0;
				firstDeliveryDelay = delay;
			}
			lastDeliveryDelay = delay;
			reachedRecipients++;
		} else {
			assert !comp.getApp().getHost().getId().equals(initiatorHostID);
			// Unintended receive
			if (!unintendedReachedReceivers.contains(comp)) {
				unintendedReachedReceivers.add(comp);
			}
		}
	}

	public void finalizeMeasurement() {
		if (finalized) {
			throw new AssertionError("Already finalized!");
		}
		finalized = true;

		if (totalNumberOfInterestedReceivers == 0) {
			// TODO no interested receivers... How to deal with that?
			valid = false;
			return;
		}

		/*
		 * Recall, Delivery ratio: # delivered / # interested (w/o duplicates).
		 * This metric is 1 in a perfect overlay, smaller otherwise.
		 */
		deliveryRecall = (double) reachedRecipients / (double) totalNumberOfInterestedReceivers;
		assert deliveryRecall >= 0 && deliveryRecall <= 1;

		/*
		 * Precision
		 */
		if (reachedRecipients + unintendedReachedReceivers.size() == 0) {
			// no message delivered at all.
			deliveryPrecision = -1;
		} else {
			deliveryPrecision = (double) reachedRecipients
					/ ((double) reachedRecipients + (double) unintendedReachedReceivers.size());
			assert deliveryPrecision >= 0 && deliveryPrecision <= 1;
		}

		/*
		 * Avg. delivery-delay
		 */
		long sum = 0;
		for (Double delay : deliveryDelays.values()) {
			sum += delay;
		}
		if (reachedRecipients > 0) {
			averageDeliveryDelay = sum / deliveryDelays.size();
			assert averageDeliveryDelay <= lastDeliveryDelay && averageDeliveryDelay >= firstDeliveryDelay;
		} else {
			averageDeliveryDelay = -1;
		}

		/*
		 * Avg. number of deliveries per node (duplication ratio). This metric
		 * is 1 in a perfect overlay and bigger otherwise.
		 */
		double totalDeliveries = 0;
		for (Double count : numberOfDeliveries.values()) {
			totalDeliveries += count;
		}
		if (reachedRecipients > 0) {
			averageNumberOfDeliveriesPerSubscriber = totalDeliveries / numberOfDeliveries.size();
		} else {
			averageNumberOfDeliveriesPerSubscriber = 0;
		}

		// System.out.println("PublicationCollector - recall: " +
		// deliveryRecall + " precision " + deliveryPrecision
		// + " avg. delay: " + Time.getFormattedTime(averageDeliveryDelay) +
		// " receivers total: "
		// + totalNumberOfInterestedReceivers + " reached:" +
		// reachedRecipients);

		valid = true;

	}

	public List<PubSubGlobalKnowledgeNodeState> getGlobalKnowledgeReceivers() {
		return globalKnowledgeReceivers;
	}

	public List<PubSubGlobalKnowledgeNodeState> getReachedReceivers() {
		return reachedReceivers;
	}

	public List<PubSubGlobalKnowledgeNodeState> getUnintendedReachedReceivers() {
		return unintendedReachedReceivers;
	}

	public Map<PubSubGlobalKnowledgeNodeState, Double> getDeliveryDelays() {
		return deliveryDelays;
	}

	public Map<PubSubGlobalKnowledgeNodeState, Double> getNumberOfDeliveries() {
		return numberOfDeliveries;
	}

	public int getTotalNumberOfInterestedReceivers() {
		return totalNumberOfInterestedReceivers;
	}

	public double getAverageNumberOfDeliveriesPerSubscriber() {
		return averageNumberOfDeliveriesPerSubscriber;
	}

	public List<Double> getDeliveryDelaysAsList() {
		return new LinkedList<Double>(deliveryDelays.values());
	}

	public List<Double> getNumberOfDeliveriesAsList() {
		return new LinkedList<Double>(numberOfDeliveries.values());
	}

	public double getDeliveryRecall() {
		return deliveryRecall;
	}

	/**
	 * Number of gateways involved in delivering this publication.
	 * 
	 * @return
	 */
	public int getNumberOfGatewaysUsed() {
		return numberOfGatewaysUsed;
	}

	/**
	 * Precision
	 * 
	 * @return -1 if invalid
	 */
	public double getDeliveryPrecision() {
		return deliveryPrecision;
	}

	/**
	 * Average delivery delay in TIME units.
	 * 
	 * @return -1 if invalid.
	 */
	public long getAverageDeliveryDelay() {
		return averageDeliveryDelay;
	}

	/**
	 * Number of match operations associated to this publication
	 * 
	 * @return -1 if invalid
	 */
	public double getMatchComplexity() {
		return numberOfMatchOperations;
	}

	public PubSubComponent getInitiator() {
		return initiator;
	}

	public boolean isValid() {
		return valid;
	}

	/**
	 * As defined in the {@link PubSubNotificationForwardingAnalyzer}, called
	 * whenever THIS notification is forwarded by a host.
	 * 
	 * @param localhost
	 * @param notification
	 * @param receivedFrom
	 * @param color
	 */
	public void onForwardNotification(Host localhost, Notification notification, INodeID receivedFrom, int color) {
		assert notification.equals(this.publication);
		if (PubSubAppVisualization.ENABLE_DEMO_MODE) {
			// only used for visualizations
			this.forwarders.add(new ForwarderInfo(localhost, receivedFrom, color, false));
		}
	}

	/**
	 * As defined in the {@link PubSubNotificationForwardingAnalyzer}, called
	 * whenever THIS notification is dropped on a host.
	 * 
	 * @param localhost
	 * @param notification
	 * @param receivedFrom
	 * @param color
	 */
	public void onDropNotification(Host localhost, Notification notification, INodeID receivedFrom, int color) {
		assert notification.equals(this.publication);
		if (PubSubAppVisualization.ENABLE_DEMO_MODE) {
			// only used for visualizations
			this.forwarders.add(new ForwarderInfo(localhost, receivedFrom, color, true));
		}
	}

	@Override
	public String toString() {
		return "PublicationCollector - recall: " + deliveryRecall + " precision: " + deliveryPrecision + " avg. delay: "
				+ Time.getFormattedTime(averageDeliveryDelay) + " receivers total: " + totalNumberOfInterestedReceivers
				+ " reached:" + reachedRecipients + " originator: " + initiatorHostID + " \n\t reached nodes: "
				+ reachedReceivers.toString() + " \n\t should have reached: " + globalKnowledgeReceivers.toString();
	}

	/**
	 * Helper class to store forwarding information.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class ForwarderInfo {

		public final Host to;

		public final INodeID from;

		public final int color;

		public final boolean drop;

		public ForwarderInfo(Host to, INodeID from, int color, boolean drop) {
			this.to = to;
			this.from = from;
			this.color = color;
			this.drop = drop;
		}

	}

}
