package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.transitions;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions.DefaultLocationBasedTransitionBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions.DefaultLocationBasedTransitionClient;

/**
 * Executes transitions between FilterSchemes in Bypass
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class FilterSchemeTransitionController extends AbstractTransitionController {

	@Override
	protected void registerCustomTransitions() {
		/*
		 * Register custom transitions, IFF state transfer is enabled
		 */
		if (!BypassSubscriptionSchemeConfiguration.disableStateTransfer) {
			for (BypassClientComponent client : getClients().values()) {
				client.getTransitionEngine().registerTransition(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME,
						new DefaultLocationBasedTransitionClient());
			}
			if (getCloud() != null) {
				getCloud().getTransitionEngine().registerTransition(
						BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
						new DefaultLocationBasedTransitionBroker());
			}
		}
	}

}
