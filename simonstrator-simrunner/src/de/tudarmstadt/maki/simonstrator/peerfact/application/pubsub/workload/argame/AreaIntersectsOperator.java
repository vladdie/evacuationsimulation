package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.argame;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.OperatorImplementation;

/**
 * Intersection for Areas - registered with the pub/sub component as operator
 * implementation. We create one instance per host, as we lateron might want to
 * add per-host analyzing.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, May 2, 2014
 */
public class AreaIntersectsOperator implements OperatorImplementation {

	private final double tolerance;

	private final boolean onServer;

	/**
	 * Specify some tolerance threshold on top of the sub-radius
	 * 
	 * @param tolerance
	 */
	public AreaIntersectsOperator(double tolerance, boolean onServer) {
		this.tolerance = tolerance;
		this.onServer = onServer;
	}

	@Override
	public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
		assert filter.getType().equals(AreaAttributeValue.class);
		assert candidate.getType().equals(AreaAttributeValue.class);

		AreaAttributeValue sub = (AreaAttributeValue) filter.getValue();
		AreaAttributeValue pub = (AreaAttributeValue) candidate.getValue();

		/*
		 * Context-based pub/sub workaround: update subscription on matching
		 * publication of same node (silent contract here: the notification will
		 * include the position of the node that the node also subscribed to ->
		 * AR gaming context!)
		 * 
		 * Sincerely, this is API-Gold ;)
		 */
		if (pub.getHostId() == sub.getHostId()) {
			// Update subscription with new position
			// sub.updatePosition(pub.getPosition());
			sub.updatePosition(pub.getX(), pub.getY());
			// no self-notifications!
			return false;
		}

		// return pub.getPosition().distanceTo(sub.getPosition()) <= pub
		// .getRadius() + sub.getRadius();

//		double dist = AreaIntersectsOperator.getDistance(pub.getX(),
//				pub.getY(), sub.getX(), sub.getY());
		double dist = Math.sqrt(Math.pow((pub.getX() - sub.getX()), 2)
				+ Math.pow((pub.getY() - sub.getY()), 2));

		return dist <= pub.getRadius() + sub.getRadius()
				+ tolerance;

	}

	public static double getDistance(double x1, double y1, double x2, double y2) {
		return Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
	}

}
