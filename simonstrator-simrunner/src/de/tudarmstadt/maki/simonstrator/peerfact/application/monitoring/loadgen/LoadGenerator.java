package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen;

import java.util.ArrayList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.RequestInvokable;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.Utils;
import de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.RequestContainer;
import de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config.Scenario;
import de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.nodeselection.AbstractNodeSelection;
import de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.nodeselection.AbstractNodeSelection.IllegalSelectionException;

public class LoadGenerator implements EventHandler {
	// Event Types
	private static final int INCOMING_REQUEST = 35185;

	private static final int SIMULATION_STARTED = 35186;

	// Storage
	private Scenario scenario;

	private long sampleRate;

	private AbstractNodeSelection nodeSelection;
	private Boolean debug;
	
	/**
	 * Constructor
	 * 
	 * @param sampleRate
	 *            the rate at which the load generator should emit events
	 */
	@XMLConfigurableConstructor({ "sampleRate" })
	public LoadGenerator(String sampleRate) {
		this.sampleRate = Time.parseTime(sampleRate);
		Event.scheduleImmediately(this, null, SIMULATION_STARTED);
		this.debug = false;
	}

	/**
	 * Constructor
	 * 
	 * @param sampleRate
	 *            the rate at which the load generator should emit events
	 * @param debug 
	 *            enable debugging for the load gen
	 */
	@XMLConfigurableConstructor({ "sampleRate", "debug" })
	public LoadGenerator(String sampleRate, String debug) {
		this.sampleRate = Time.parseTime(sampleRate);
		Event.scheduleImmediately(this, null, SIMULATION_STARTED);
		this.debug = Boolean.valueOf(debug);
	}

	/**
	 * Set the scenario for the load gen
	 * 
	 * @param scenario
	 *            An Object that contains Requests, Sources, Targets &
	 *            Distribution
	 */
	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
		// Schedule each request in scenario the scenario at sample time
		for (RequestContainer req : this.scenario.getRequests()) {
			Event.scheduleWithDelay(sampleRate, this, req, INCOMING_REQUEST);
		}
	}

	public void setDebug(String debug) {
		this.debug = Boolean.valueOf(debug);
	}

	/**
	 * Set the node selection algorithm for the scenario.
	 * 
	 * @param nodeSelection
	 */
	public void setNodeSelection(AbstractNodeSelection nodeSelection) {
		this.nodeSelection = nodeSelection;
		// For some node selection algorithms it is necessary that it knows all
		// nodes that are present in the network
		this.nodeSelection.setAllNodeIDs(Filter.getAllNodeIDs());
	}

	@Override
	public void eventOccurred(Object content, int type) {
		switch (type) {
		case SIMULATION_STARTED:
			// Simulation started
			if (this.debug) {
				System.out.println("Simulation started!");
			}
			break;
		case INCOMING_REQUEST:
			// Received a request
			if (this.debug) {
				System.out.println("Received REQUEST_TYPE");
			}
			if (content == null) {
				System.out
						.println("Received a Request without a valid object.");
				break;
			}

			handleRequest((RequestContainer) content);
			break;
		default:
			if (this.debug) {
				System.out.println("Unknown Event received: " + type);
			}
			break;
		}
	}

	/**
	 * Invokes a request at source nodes, according to distribution and node
	 * selection.
	 * 
	 * @param container
	 *            Request
	 */
	private void handleRequest(RequestContainer container) {

		// Schedule next request directly after next sampling time
		Event.scheduleWithDelay(sampleRate, this, container, INCOMING_REQUEST);

		if (this.debug) {
			System.out.println("Received Request");
			System.out.println(container.toString());
		}
		// Get all active nodes in the network
		ArrayList<INodeID> activeNodes = Filter.getActiveNodes();
		if (this.debug) {
			System.out.println("Active Nodes: " + activeNodes.size());
		}

		if (activeNodes.size() == 0) {
			if (this.debug) {
				System.out.println("No active nodes in Network");
			}
			return;
		}

		// Check each active node if it matches all conditions
		ArrayList<INodeID> filteredNodes = new ArrayList<>();
		for (INodeID nodeId : activeNodes) {
			if (Utils.matchNodeAgainstSetOfConditions(nodeId,
					container.getSources())) {
				filteredNodes.add(nodeId);
			}
		}
		if (this.debug) {
			System.out.println(
				"Nodes that match conditions: " + filteredNodes.size());
		}

		if (filteredNodes.size() == 0) {
			if (this.debug) {
				System.out.println("No nodes match all conditions");
			}
			return;
		}

		// Choose amount of nodes from distribution
		int nodeCount = container.getDistribution()
				.getCount(Time.getCurrentTime(), filteredNodes.size());

		if (this.debug) {
			System.out.println("Node count from Distribution: " + nodeCount);
		}
		if (nodeCount == 0) {
			if (this.debug) {
				System.out.println("No nodes selected from Distribution");
			}
			return;
		}

		// Node Selection and invoking
		try {
			List<INodeID> selectedNodes = this.nodeSelection
					.selectNodes(nodeCount, filteredNodes);

			for (INodeID nodeID : selectedNodes) {
				if (this.debug) {
					System.out.println(
							"Invoke Request on " + nodeID.toString() + ": " + container.getRequest().toString());
				}

				Host currentHost = Oracle.getHostByID(nodeID);
				//if (currentHost instanceof RequestInvokable)
				try{
					//((RequestInvokable) currentHost).invokeRequest(request.getRequestType());
					CraterNodeComponent cn = currentHost.getComponent(CraterNodeComponent.class);
					cn.invokeRequest(container.getRequest());
					
					// Christoph Storm: Changed RequestInvokable, so it can be
					// seamlessly used in Overlay, this necessitated some
					// changes in here, too
					
				//else
				} catch(ComponentNotAvailableException e){
					System.err.println(
							"Host with ID " + nodeID + " doesn't implement "
									+ RequestInvokable.class.getName());
				}
			}

		} catch (IllegalSelectionException e) {
			e.printStackTrace();
		}
	}
}
