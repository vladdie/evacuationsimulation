# Analyzer descriptions
Crater specific analyzer.

### still to be done

## CraterLinkAnalyzer
Analyzer to obtain the number of messages send on each node and the size of the respective messages.
Provides for the **number of messages** (needs to be specified) and the **size of the messages** (needs to be specified).

## AccuracyAndCompletenessAnalyzer
Analyzer to determine in an interval-based fashion the accuracy **(relative and total error)** and the number of nodes participating in the result **(completeness)**.
Uses a {@link DataObjectAggregationDupSensitiveImpl} received at the central entity for estimation of both metrics.
Furthermore, it is only measuring the node count (ignoring the content) for the completeness.

## CraterGlobalKnowledge
Currently used?

## CraterGridAvgNodesPerSinkAnalyzer
Used to periodically measure the positions of the nodes in the network. Differentiates SINKs and LEAFs to in the end compare density of sinks over time with density of leafs.

TODO Density estimates to in each step bewerten the topology and the current network situation.

## DataRoutingAnalyzer
Measures **latencies**, **delivery ratios**, **duplicate ratios**.

**Latencies**:
(i) inNetwork ContainerMessages (cdf?)
(ii) inUpload ContainerMessages
(iii) inNetwork/inUpload NormalData
(iv) inNetwork/inUpload DubSensitive


## HostEnergyAnalyzer
Analyzer for a node having the {@link SmartphoneCommunicationEnergyComponent} and {@link SmartphoneCellularCommunicationEnergyComponent} for WiFi and Cloud netlayers.
Distinguishes WiFi and Cloud energy consumption!

## PerNodeHasNoSinkTimesAnalyzer
Analyzer to measure the time {@link CraterNodeComponent} is being a LEAF without knowing a sink (empty sink table). Beside this this analyzer measures the average time the sink table of a LEAF needs to be refilled.
Metrics:
- Online Time per Node
- NoSinkTime per Node
- Average Refill Delay for sink table

## PerNodeRoleTimesAnalyzer
Analyzer for estimation of the times {@link CraterNodeComponent}s are in which {@link CraterNodeRole}. Beside that estimation of the active times of the {@link SinkAdvertising} and the {@link NoSinkAdvertising} components.
Metrics:
- Ratio being Leaf over online time
- Ratio being Sink over online time
- Ratio Active Leaf over online time
- Ratio Active Sink over online time



