package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;

@Entity
@Table(name = "request_response_measurements")
@SuppressWarnings("unused")
public class RequestResponseMeasurement extends CustomMeasurement {
	// FIXME make discrete request and response metrics, not both in here?

	public static final MetricDescription REQUEST_RESPONSE_METRIC = new MetricDescription(
			RequestResponseAnalyzerImpl.class, "RequestResponesMetrics",
			"Provides several measurements per Request", "");

	// ////////////////////////////
	// Request Metrics
	// ////////////////////////////

	/**
	 * The identifier / Hash value of the request
	 */

	private int request;

	/**
	 * Whether this was a periodic request (true) or not (false)
	 */
	private boolean periodic;

	/**
	 * When this request was generated. <br>
	 * [us]
	 */
	private long invocationTime;

	/**
	 * The request was valid until this time. After this moment, responses were
	 * discarded in the Network. <br>
	 * [us]
	 */
	private long validityTime;

	/**
	 * The Request was received by this many nodes *present* at the time of its
	 * creation. <br>
	 * #number
	 */
	private int initialNodesReachedWithRequest;

	/**
	 * Number of online nodes at request creation.
	 */
	private int numOfNodesAtCreation;

	/**
	 * The Request was received by this number of intended receivers (request
	 * -targets), which were online at request invokation time.
	 */
	private int numOfIntededReceiversReachedAtCreation;

	/**
	 * Number of online nodes at request creation that have one of the targeted
	 * receiver IDs. I.e., their NodeID is part of the
	 * {@link Request #getReceiver}.
	 */
	private int numOfIntendedReceiversAtCreation;

	/**
	 * The Request was received by this many nodes in total. This included nodes
	 * that were *offline* during the creation and went *present* sometime
	 * later, while the request was still valid. <br>
	 * #number
	 */
	private int allNodesReachedWithRequest;

	/**
	 * Number of all nodes online during the request validity. This includes
	 * duplicates, i.e., nodes that were online, went offline, and online again!
	 */
	private int numOfAllExpectedReceivers;

	/**
	 * The request was received by this number of its targeted receivers, i.e.,
	 * nodes that are part of {@link Request #getReceiver}.
	 */
	private int numOfIntendedReceiversReached;

	/**
	 * Number of online nodes during the request validity, that have one of the
	 * targeted receiver IDs
	 */
	private int numOfAllIntendedReceivers;

	/**
	 * The average time it took the request to be received at the reached,
	 * initially *present*, nodes. <br>
	 * [us]
	 */
	private long meanTimeToRequestReceivedInitialNodes;

	/**
	 * The average time it took the request to be received at all reached nodes,
	 * *present* sometime during its validity. <br>
	 * [us]
	 */
	private long meanTimeToRequestReceivedAllNodes;

	// //////////////////////////
	// Response Metrics
	// //////////////////////////

	/**
	 * How many responses were received at the requester. <br>
	 * #number
	 */
	private int receivedResponses;

	/**
	 * How many responses were received at the cloud component. This should be
	 * larger than the above, because requesters can be affected by churn! <br>
	 * #number
	 */
	private int receivedResponsesAtCloud;

	private int numOfSentResponses;

	/**
	 * Averaged Time it took a response to reach the requester. <br>
	 * <i>This is the per Node average travel time, averaged over all nodes!</i>
	 * Th averaging scheme should limit the effect of badly placed nodes
	 * somewhat. <br>
	 * This <i>should</i> effectively correspond to crater's staleness <br>
	 * [us]
	 */
	private long meanResponseTravelTimePerNode;

	/**
	 * The average time it took to receive the first response of each one of the
	 * initially *present* nodes. From request creation time on. <br>
	 * [us]
	 */
	private long meanTimeRequestToFirstResponseInitialNodes;

	/**
	 * From creation of the request on, the average time it took for to receive
	 * the first response of all responding nodes! [us]
	 */
	private long meanTimeRequestToFirstResponseAllNodes;


	public RequestResponseMeasurement(int request, boolean periodic,
			long invocationTime, long validityTime,
 int initialNodesReached,
			int numOfNodesAtCreation, int numOfIntededReceiversReachedAtCreation, int numOfIntendedReceiversAtCreation,
			int allNodesReached, int numOfAllExpectedReceivers, int numOfAllIntendedReceivers,
			int numOfIntendedReceiversReached,
			long meanTimeToRequestReceivedInitialNodes,
			long meanTimeToRequestReceivedAllNodes,
 int teceivedResponses,
			int receivedResponsesAtCloud, int numOfSentResponses,
			long meanResponseTravelTimePerNode,
			long meanTimeRequestToResponseInitialNodes,
			long meanTimeRequestToResponseAllNodes) {

		this.invocationTime = invocationTime;
		this.validityTime = validityTime;
		this.initialNodesReachedWithRequest = initialNodesReached;
		this.numOfNodesAtCreation = numOfNodesAtCreation;
		this.numOfIntededReceiversReachedAtCreation = numOfIntededReceiversReachedAtCreation;
		this.numOfIntendedReceiversAtCreation = numOfIntendedReceiversAtCreation;
		this.allNodesReachedWithRequest = allNodesReached;
		this.numOfAllExpectedReceivers = numOfAllExpectedReceivers;
		this.numOfIntendedReceiversReached = numOfIntendedReceiversReached;
		this.numOfAllIntendedReceivers = numOfAllIntendedReceivers;
		this.meanTimeToRequestReceivedInitialNodes = meanTimeToRequestReceivedInitialNodes;
		this.meanTimeToRequestReceivedAllNodes = meanTimeToRequestReceivedAllNodes;
		this.receivedResponses = teceivedResponses;
		this.receivedResponsesAtCloud = receivedResponsesAtCloud;
		this.numOfSentResponses = numOfSentResponses;
		this.meanResponseTravelTimePerNode = meanResponseTravelTimePerNode;
		this.meanTimeRequestToFirstResponseInitialNodes = meanTimeRequestToResponseInitialNodes;
		this.meanTimeRequestToFirstResponseAllNodes = meanTimeRequestToResponseAllNodes;
	}

}
