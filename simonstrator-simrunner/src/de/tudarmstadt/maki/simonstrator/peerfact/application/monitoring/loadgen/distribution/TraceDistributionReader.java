package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.distribution;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.TreeMap;

import de.tud.kom.p2psim.impl.churn.MaxPeerCountChurnGenerator;
import de.tud.kom.p2psim.impl.scenario.DefaultConfigurator;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

/**
 * Uses a given trace file to determine the current percentage. Works similar to
 * {@link MaxPeerCountChurnGenerator}. Only the last parameter in the file is
 * not the numberOfNodes, it is the percentage of nodes, which should be set at
 * the destination point.
 * 
 * @see MaxPeerCountChurnGenerator
 * @author Simon
 * @version 1.0, 16.06.2016
 */
public final class TraceDistributionReader implements AbstractDistribution {

	private static final String commentsDelimiter = "#";

	private static final String SEP = ",";

	/**
	 * {@link RequestInterval} from the csv file.
	 */
	private TreeMap<Long, Double> requestIntervals;

	@XMLConfigurableConstructor({ "file" })
	public TraceDistributionReader(String file) {
		requestIntervals = new TreeMap<>();

		parseTrace(file);
	}

	/**
	 * Reads the file given by the configuration and parses the trace
	 * distribution.
	 * 
	 * This parser works for the following csv file structure: startTime,
	 * intervalLength, percentageOfSendingClients
	 * 
	 * @param filename
	 */
	private void parseTrace(String filename) {
		BufferedReader csv = null;

		try {
			csv = new BufferedReader(new FileReader(filename));

			long previousEndTime = 0;

			// add default start point
			requestIntervals.put(0l, 0d);

			while (csv.ready()) {
				String line = csv.readLine();
				if (line.length() == 0 || line.startsWith(commentsDelimiter))
					continue;

				if (line.indexOf(SEP) > -1) {
					String[] parts = line.split(SEP);

					if (parts.length == 3) {
						long startTime = DefaultConfigurator.parseNumber(
								parts[0].replaceAll("\\s+", ""), Long.class);
						long burstLength = DefaultConfigurator.parseNumber(
								parts[1].replaceAll("\\s+", ""), Long.class);
						double percentageOfSendingClients = DefaultConfigurator
								.parseNumber(parts[2].replaceAll("\\s+", ""),
										Double.class);

						assert startTime >= previousEndTime : "Start time for next fluctuation must be greater than previous end time.";
						assert burstLength > 0 : "Burst must be bigger than 0";

						// if invalid percentage number, everybody is sending
						if (percentageOfSendingClients < 0
								|| percentageOfSendingClients > 1) {
							System.err.println(
									"Wrong inputs for percentageOfSendingClients");
							percentageOfSendingClients = 1;
						}

						previousEndTime = startTime + burstLength;

						// check if there's a difference in the entries with
						// slope = 0. If yes, another point at the new start
						// needs to be added to keep the graph continuous.
						Entry<Long, Double> floorEntry = requestIntervals
								.floorEntry(startTime);
						if (floorEntry != null
								&& floorEntry.getKey() < startTime)
							requestIntervals.put(startTime,
									floorEntry.getValue());

						if (requestIntervals.put(startTime + burstLength,
								percentageOfSendingClients) != null)
							throw new AssertionError("Same time added twice.");
					} else {
						throw new AssertionError("To many/few columns in CSV.");
					}
				}
			}
		} catch (Exception e) {
			System.err.println("Could not open " + filename);
			throw new RuntimeException("Could not open " + filename);
		} finally {
			if (csv != null) {
				try {
					csv.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * determines the lower and upper points nearby and makes a linear function
	 * between them. It is then called with the given time point.
	 * 
	 * @param timePoint
	 * @param numberOfFilteredNodes
	 * @return
	 */
	@Override
	public int getCount(long timePoint, int numberOfFilteredNodes) {
		Entry<Long, Double> lowerXEntry = requestIntervals
				.floorEntry(timePoint);
		Entry<Long, Double> higherXEntry = requestIntervals
				.ceilingEntry(timePoint);

		// outside of simulation time range number of nodes is always 0
		if (lowerXEntry == null || higherXEntry == null)
			return 0;

		// no change
		if (lowerXEntry.equals(higherXEntry))
			return (int) calculatePercentage(numberOfFilteredNodes,
					lowerXEntry.getValue());

		// determine slope from last point
		double slope = (higherXEntry.getValue() - lowerXEntry.getValue())
				/ (higherXEntry.getKey() - lowerXEntry.getKey());

		// difference in x direction
		long differenceX = timePoint - lowerXEntry.getKey();
		assert differenceX >= 0 : "Wrong distance -> wrong parsing";

		double newPercentage = slope * differenceX + lowerXEntry.getValue();

		return (int) calculatePercentage(numberOfFilteredNodes, newPercentage);
	}

	private static double calculatePercentage(int numberOfFilteredNodes,
			double percentage) {
		return Math.round(percentage * numberOfFilteredNodes);
	}
}
