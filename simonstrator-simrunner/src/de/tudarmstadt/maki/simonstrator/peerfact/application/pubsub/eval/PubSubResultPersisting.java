package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;

/**
 * Allows multiple persisting strategies to be used with the
 * {@link PubSubGlobalKnowledge} and the {@link PublicationResultCollector}.
 * 
 * This is also wrapped via {@link Monitor} (e.g., the Analyzer interface) to
 * support transparent processing by multiple persistence concepts in parallel
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface PubSubResultPersisting extends Analyzer {

	/**
	 * Corresponds to a single {@link Notification} being distributed by the
	 * system.
	 * 
	 * @param collector
	 */
	public void persistPublication(PublicationResultCollector collector);

	/**
	 * Optional method: may persist the subscription results as well. This is
	 * invoked periodically (using the SubscriptionResultInterval as specified
	 * in your implementation of this Analyzer) with a list of all current
	 * {@link SubscriptionResultCollector}s.
	 * 
	 * @param collectors
	 */
	public void persistSubscription(SubscriptionResultCollector collector);

}
