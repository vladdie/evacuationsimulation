package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import de.tud.kom.p2psim.api.analyzer.ConnectivityAnalyzer;
import de.tud.kom.p2psim.api.analyzer.EnergyAnalyzer;
import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.api.energy.ComponentType;
import de.tud.kom.p2psim.api.energy.EnergyCommunicationComponent;
import de.tud.kom.p2psim.api.energy.EnergyComponent;
import de.tud.kom.p2psim.api.network.SimNetInterface;
import de.tud.kom.p2psim.impl.energy.SmartphoneCellularCommunicationEnergyComponent;
import de.tud.kom.p2psim.impl.energy.SmartphoneCommunicationEnergyComponent;
import de.tud.kom.p2psim.impl.simengine.Simulator;
import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tud.kom.p2psim.impl.util.oracle.GlobalOracle;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;

/**
 * Analyzer for a node having the {@link SmartphoneCommunicationEnergyComponent}
 * and {@link SmartphoneCellularCommunicationEnergyComponent} for WiFi and Cloud
 * netlayers.
 * 
 * Distinguishes WiFi and Cloud energy consumption!
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 17.08.2014
 * 
 */
public class HostEnergyAnalyzer implements EnergyAnalyzer, ConnectivityAnalyzer, EventHandler {

	private boolean isRunning;

	private boolean ENABLE_DAO = false;

	private final int _EVENT_ENABLE_DAO_AT = 2001;

	public static NetInterfaceName netTypeCloudTransport = NetInterfaceName.MOBILE;

	public static NetInterfaceName netTypeLocalTransport = NetInterfaceName.WIFI;

	private LinkedHashMap<SimHost, EnergyInfo> energyMapWiFi;
	private LinkedHashMap<SimHost, EnergyInfo> energyMapCloud;

	private LinkedList<SimHost> offlineHosts;
	private LinkedList<SimHost> onlineHosts;

	public HostEnergyAnalyzer() {
		this.isRunning = false;
		this.energyMapWiFi = new LinkedHashMap<SimHost, HostEnergyAnalyzer.EnergyInfo>();
		this.energyMapCloud = new LinkedHashMap<SimHost, HostEnergyAnalyzer.EnergyInfo>();
		this.offlineHosts = new LinkedList<SimHost>();
		this.onlineHosts = new LinkedList<SimHost>();

		DAO.addEntityClass(HostEnergyInfoMeasurement.class);
	}

	public void setEnableDao(boolean enableDao) {
		this.ENABLE_DAO = enableDao;
	}

	public void setTimeEnableDao(long timeEnableDao) {
		if (ENABLE_DAO) {
			Event.scheduleWithDelay(timeEnableDao, this, null, _EVENT_ENABLE_DAO_AT);
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// _EVENT_ENABLE_DAO_AT
		assert ENABLE_DAO;
		if (type == _EVENT_ENABLE_DAO_AT) {
			createEnergyInfosForHosts();
			this.isRunning = true;
		}
	}

	@Override
	public void start() {
		// if (!ENABLE_DAO) {
		// return;
		// }

	}

	@Override
	public void stop(Writer out) {
		writeDownEnergyInfoForHosts();
		isRunning = false;
	}

	@Override
	public void consumeEnergy(SimHost host, double energy, EnergyComponent consumer) {
		if (isRunning) {

			try {
				host.getComponent(CraterCentralComponent.class);
				return;
			} catch (ComponentNotAvailableException e) {

			}

			/*
			 * When a host joins the network and changes its state from offline
			 * to online, this method will be triggered. As the corresponding
			 * EnergyInfo-Object for the host will be created with the
			 * onlineEvent-method, we have to avoid to call the following
			 * actions on the not yet existing object.
			 */
			if (consumer instanceof SmartphoneCommunicationEnergyComponent) {
				for (SimNetInterface net : host.getNetworkComponent().getSimNetworkInterfaces()) {
					if (net.getName() == netTypeLocalTransport && net.isOnline()) {
						energyMapWiFi.get(host).addConsumedEnergy(energy);
						return;
					}
				}
			} else if (consumer instanceof SmartphoneCellularCommunicationEnergyComponent) {
				for (SimNetInterface net : host.getNetworkComponent().getSimNetworkInterfaces()) {
					if (net.getName() == netTypeCloudTransport && net.isOnline()) {
						energyMapCloud.get(host).addConsumedEnergy(energy);
						return;
					}
				}
			} else if (energy > 0) {
				throw new RuntimeException("Something is wrong with energy monitoring, because the consumption of " + energy
						+ " µJ is not taken into consideration.");
			} else {
				// log.fatal("Energy analyzer for consumer " +
				// consumer.getClass().toString() + " not compatible...");
			}
		}
	}

	@Override
	public void wentOffline(SimHost host) {
		if (isRunning) {

			CraterNodeComponent nodeComp;
			try {
				nodeComp = host.getComponent(CraterNodeComponent.class);
			} catch (ComponentNotAvailableException e) {
				return;
			}

			assert nodeComp != null;

			if (!offlineHosts.contains(host)) {
				offlineHosts.add(host);
				return;
			}
			onlineHosts.remove(host);

			long currentTime = Simulator.getCurrentTime();
			LinkedList<EnergyInfo> infoList = new LinkedList<EnergyInfo>();
			// EnergyInfo wifiInfo = energyMapWiFi.remove(host);
			EnergyInfo wifiInfo = energyMapWiFi.get(host);
			// EnergyInfo ethernetInfo = energyMapEthernet.remove(host);
			EnergyInfo ethernetInfo = energyMapCloud.get(host);
			if (wifiInfo != null) {
				infoList.add(wifiInfo);
			}
			if (ethernetInfo != null) {
				infoList.add(ethernetInfo);
			}
			// wentOffline is called for both WiFi and Ethernet, but in first
			// call all is done, thus ignore second call.

			for (EnergyInfo info : infoList) {
				info.addSessionTime(currentTime);
			}
		}
	}

	@Override
	public void wentOnline(SimHost host) {
		if (isRunning) {

			// if the component is not a NodeComponent do nothing!
			CraterNodeComponent nodeComp;
			try {
				nodeComp = host.getComponent(CraterNodeComponent.class);
			} catch (ComponentNotAvailableException e) {
				return;
			}

			assert nodeComp != null;

			if (!onlineHosts.contains(host)) {
				onlineHosts.add(host);
				return;
			}
			offlineHosts.remove(host);

			for (SimNetInterface net : host.getNetworkComponent().getSimNetworkInterfaces()) {
				if (net.isOnline()) {
					if (net.getName() == netTypeLocalTransport) {
						if (energyMapWiFi.containsKey(host)) {
							EnergyInfo info = energyMapWiFi.get(host);
							info.setOnlineTime(Simulator.getCurrentTime());
						} else {
							EnergyInfo info = new EnergyInfo();
							info.setOnlineTime(Simulator.getCurrentTime());
							energyMapWiFi.put(host, info);
						}
					} else if (net.getName() == netTypeCloudTransport) {
						if (energyMapCloud.containsKey(host)) {
							EnergyInfo info = energyMapCloud.get(host);
							info.setOnlineTime(Simulator.getCurrentTime());
						} else {
							EnergyInfo info = new EnergyInfo();
							info.setOnlineTime(Simulator.getCurrentTime());
							energyMapCloud.put(host, info);
						}

					} else {
						throw new AssertionError();
					}
				}
			}
		}
	}

	private void createEnergyInfosForHosts() {
		long currentTime = Simulator.getCurrentTime();
		for (SimHost host : GlobalOracle.getHosts()) {

			try {
				host.getComponent(CraterNodeComponent.class);
			} catch (ComponentNotAvailableException e) {
				continue;
			}

			/*
			 * In the current implementation of Feeney, we have to switch the
			 * component on and off just to trigger the updates on the battery.
			 */
			List<EnergyCommunicationComponent> commComs = host.getEnergyModel().getComponents(ComponentType.COMMUNICATION,
					EnergyCommunicationComponent.class);
			for (EnergyCommunicationComponent com : commComs) {
				com.doFakeStateChange();
			}
			// Create an EnergyInfo-Object for each host and set the current
			// time as online time if the host is online

			for (SimNetInterface net : host.getNetworkComponent().getSimNetworkInterfaces()) {
				EnergyInfo info = null;

				if (net.isOnline()) {
					if (net.getName() == netTypeLocalTransport) {
						info = new EnergyInfo();
						info.setOnlineTime(currentTime);
						energyMapWiFi.put(host, info);
					} else if (net.getName() == netTypeCloudTransport) {
						info = new EnergyInfo();
						info.setOnlineTime(currentTime);
						energyMapCloud.put(host, info);
					} else {
						throw new AssertionError();
					}
				}
			}
		}
	}

	private void writeDownEnergyInfoForHosts() {
		Iterator<SimHost> iter = energyMapWiFi.keySet().iterator();
		SimHost host = null;
		EnergyInfo info = null;
		long currentTime = Simulator.getCurrentTime();
		while (iter.hasNext()) {
			host = iter.next();
			info = energyMapWiFi.get(host);
			/*
			 * In the current implementation of Feeney, we have to switch the
			 * component on and off just to trigger the updates on the battery.
			 */
			List<EnergyCommunicationComponent> commComs = host.getEnergyModel().getComponents(ComponentType.COMMUNICATION,
					EnergyCommunicationComponent.class);
			for (EnergyCommunicationComponent com : commComs) {
				com.doFakeStateChange();
			}
			for (SimNetInterface net : host.getNetworkComponent().getSimNetworkInterfaces()) {
				if (net.isOnline() && net.getName() == netTypeLocalTransport) {
					info.addSessionTime(currentTime);
				}
			}

			/*
			 * Write the results down to the database
			 */
			if (info.getSessionTime() > 0 && ENABLE_DAO) {
				HostEnergyInfoMeasurement measurement = new HostEnergyInfoMeasurement(host.getHostId(), info.getSessionTime(),
						info.getConsumedEnergy(),
						(info.getConsumedEnergy() / (info.getSessionTime() / (double) Simulator.SECOND_UNIT)),
						NetInterfaceName.WIFI);
				MeasurementDAO.storeCustomMeasurement(HostEnergyInfoMeasurement.HOST_ENERGY_INFO_METRIC, host.getId().value(),
						currentTime, measurement);
			} else {

			}
			// log.debug("EnergyConsumption WiFi: Host " + host.getHostId() +
			// " was "
			// + (info.getSessionTime() / (double) Simulator.SECOND_UNIT) +
			// " seconds online and consumed " + info.getConsumedEnergy()
			// / 1000 + " mJ! resulting in an average of "
			// + ((info.getConsumedEnergy() / (info.getSessionTime() / (double)
			// Simulator.SECOND_UNIT)) / 1000) + " mW");
			// log.debug("Host " + host.getHostId() + " was " +
			// (info.getSessionTime() / (double) Simulator.SECOND_UNIT)
			// + " seconds online and consumed " + info.getConsumedEnergy() + "
			// µJ! resulting in an average of "
			// + (info.getConsumedEnergy() / (info.getSessionTime() / (double)
			// Simulator.SECOND_UNIT)) + " µW");
		}

		iter = energyMapCloud.keySet().iterator();
		host = null;
		info = null;
		currentTime = Simulator.getCurrentTime();
		while (iter.hasNext()) {
			host = iter.next();
			info = energyMapCloud.get(host);
			/*
			 * In the current implementation of Feeney, we have to switch the
			 * component on and off just to trigger the updates on the battery.
			 */
			List<EnergyCommunicationComponent> commComs = host.getEnergyModel().getComponents(ComponentType.COMMUNICATION,
					EnergyCommunicationComponent.class);
			for (EnergyCommunicationComponent com : commComs) {
				com.doFakeStateChange();
			}
			for (SimNetInterface net : host.getNetworkComponent().getSimNetworkInterfaces()) {
				if (net.isOnline() && net.getName() == netTypeCloudTransport) {
						info.addSessionTime(currentTime);
				}
			}

			/*
			 * Write the results down to the database
			 */
			if (info.getSessionTime() > 0 && ENABLE_DAO) {
				HostEnergyInfoMeasurement measurement = new HostEnergyInfoMeasurement(host.getHostId(), info.getSessionTime(),
						info.getConsumedEnergy(),
						(info.getConsumedEnergy() / (info.getSessionTime() / (double) Simulator.SECOND_UNIT)),
						NetInterfaceName.ETHERNET);
				MeasurementDAO.storeCustomMeasurement(HostEnergyInfoMeasurement.HOST_ENERGY_INFO_METRIC, host.getHostId(),
						currentTime, measurement);

			} else {
				// log.fatal("This should never happen...");
			}
			// log.debug("EnergyConsumption Cellular: Host " + host.getHostId()
			// + " was "
			// + (info.getSessionTime() / (double) Simulator.SECOND_UNIT) +
			// " seconds online and consumed " + info.getConsumedEnergy()
			// / 1000 + " mJ! resulting in an average of "
			// + ((info.getConsumedEnergy() / (info.getSessionTime() / (double)
			// Simulator.SECOND_UNIT)) / 1000) + " mW");
			// log.debug("Host " + host.getHostId() + " was " +
			// (info.getSessionTime() / (double) Simulator.SECOND_UNIT)
			// + " seconds online and consumed " + info.getConsumedEnergy() + "
			// µJ! resulting in an average of "
			// + (info.getConsumedEnergy() / (info.getSessionTime() / (double)
			// Simulator.SECOND_UNIT)) + " µW");
		}
	}

	private class EnergyInfo {

		private long onlineTime;

		private long sessionTime;

		private double consumedEnergy;

		public EnergyInfo() {
			this.sessionTime = 0;
			this.consumedEnergy = 0;
		}

		public void setOnlineTime(long onlineTime) {
			this.onlineTime = onlineTime;
		}

		long getSessionTime() {
			return this.sessionTime;
		}

		void addSessionTime(long offlineTime) {
			this.sessionTime = this.sessionTime + (offlineTime - onlineTime);
		}

		double getConsumedEnergy() {
			return consumedEnergy;
		}

		void addConsumedEnergy(double consumedEnergy) {
			this.consumedEnergy += consumedEnergy;
		}

	}

	// --------------------------------------------------------------------
	// The remaining methods are not required for this type of analyzer
	// --------------------------------------------------------------------

	@Override
	public void batteryIsEmpty(SimHost host) {
		throw new AssertionError();
	}

	@Override
	public void highPowerMode(SimHost host, long time, double consumedEnergy, EnergyComponent component) {
		throw new AssertionError();
	}

	@Override
	public void lowPowerMode(SimHost host, long time, double consumedEnergy, EnergyComponent component) {
		throw new AssertionError();
	}

	@Override
	public void tailMode(SimHost host, long time, double consumedEnergy, EnergyComponent component) {
		throw new AssertionError();
	}

	@Override
	public void offMode(SimHost host, long time, double consumedEnergy, EnergyComponent component) {
		throw new AssertionError();
	}

}
