package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.vis;

import de.tud.kom.p2psim.api.application.Application;
import de.tud.kom.p2psim.api.common.SimHost;

public class CraterVisualizationApp implements Application {

	private final SimHost host;

	public CraterVisualizationApp(SimHost host) {
		this.host = host;
	}

	@Override
	public SimHost getHost() {
		return host;
	}

	@Override
	public void initialize() {

	}

	@Override
	public void shutdown() {
		// not used
	}

}
