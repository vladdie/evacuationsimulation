package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue;
import de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval.MAvgNodesPerSinkFairness.MAvgNodesPerSinkFairnessValue;

public class MAvgNodesPerSinkFairness extends AbstractMetric<MAvgNodesPerSinkFairnessValue>
		implements Metric<MAvgNodesPerSinkFairnessValue> {

	public MAvgNodesPerSinkFairness() {
		super("Crater: Nodes per Sink Fairness in a Grid Structure.", MetricUnit.NONE);
	}

	protected class MAvgNodesPerSinkFairnessValue implements MetricValue<Double> {

		@Override
		public Double getValue() {
			return CraterGridAvgNodesPerSinkAnalyzer.getInstance().getJainFairness();
		}

		@Override
		public boolean isValid() {
			return CraterGridAvgNodesPerSinkAnalyzer.getInstance().isValid();
		}
		
	}

	@Override
	public void initialize(List<Host> hosts) {
		setOverallMetric(new MAvgNodesPerSinkFairnessValue());
	}
}
