package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.NotificationInfo;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscription;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.ARGameWorkload;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.ARGameWorkload.ARNodeState;

/**
 * Info-object tied to a local subscription of a client, measuring recall and
 * precision on a per-client basis (this can also measure the frequency of
 * updates and staleness of information!). As this is a passive object, we need
 * to manually trigger evaluation of its current state.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class SubscriptionResultCollector {

	private final PubSubGlobalKnowledgeNodeState initiator;

	private final ARNodeState arNodeState;

	public final Subscription subscription;

	public final double radiusOfInterest;

	public final INodeID subscriberNodeId;

	private Map<INodeID, NotificationState> notificationStates = new LinkedHashMap<>();

	private long lastSnapshotTimestamp = -1;

	private double bytesReceived = 0;

	private int notificationsReceived = 0;

	private final long MAX_NOTIFICATION_AGE;

	public SubscriptionResultCollector(PubSubGlobalKnowledgeNodeState initiator, Subscription subscription,
			long maxNotificationAge) {
		this.initiator = initiator;
		this.subscription = subscription;
		if (subscription instanceof LocationBasedSubscription) {
			this.radiusOfInterest = ((LocationBasedSubscription) subscription).getRadiusOfInterest();
		} else {
			this.radiusOfInterest = -1;
		}
		this.subscriberNodeId = initiator.getApp().getHost().getId();
		this.MAX_NOTIFICATION_AGE = maxNotificationAge;
		if (initiator.getApp().getWorkload() instanceof ARGameWorkload) {
			ARGameWorkload wl = (ARGameWorkload) initiator.getApp().getWorkload();
			arNodeState = wl.getStateFor(subscriberNodeId);
		} else {
			arNodeState = null;
		}
	}

	public void notificationArrived(Notification notification) {
		NotificationInfo nInfo = notification._getNotificationInfo(null);
		INodeID originatorId = INodeID.get(nInfo.getOriginatorHostId());

		bytesReceived += notification.getTransmissionSize();
		notificationsReceived++;

		/*
		 * This method is only invoked iff a notification matches this
		 * subscription according to PubSubComponent.matches. Therefore, we
		 * simply use it to analyze the performance of location-based schemes
		 * which will report a match even if the notification did not really
		 * fall into the area of interest.
		 */

		NotificationState state = notificationStates.get(originatorId);
		if (state == null) {
			state = new NotificationState();
			notificationStates.put(originatorId, state);
		}

		if (isLocationBased() && notification instanceof LocationBasedNotification) {
			// TODO Location-based analyzing, e.g., accuracy of position?
			Location senderLocation = ((LocationBasedNotification) notification).getLocation();
			assert arNodeState != null;
			state.lastLocation = senderLocation;
		}
		state.lastUpdateTimestamp = Time.getCurrentTime();
	}

	/**
	 * Is this a location-based subscription?
	 * 
	 * @return
	 */
	public boolean isLocationBased() {
		return radiusOfInterest != -1;
	}

	public INodeID getSubscriberNodeId() {
		return subscriberNodeId;
	}

	/**
	 * Delete notifications that exceed MAX_NOTIFICATION_AGE
	 */
	private void clearOutdatedState() {
		long currentTime = Time.getCurrentTime();
		notificationStates.entrySet()
				.removeIf(e -> (e.getValue().lastUpdateTimestamp + MAX_NOTIFICATION_AGE < currentTime));
	}

	/**
	 * Create a snapshot used for persisting data related to this subscription.
	 * 
	 * @return
	 */
	public SubscriptionResultSnapshot createSnapshot() {
		clearOutdatedState();
		// old notifications have been deleted now
		Set<INodeID> truePositives = new LinkedHashSet<>();
		Set<INodeID> falsePositives = new LinkedHashSet<>(notificationStates.keySet());
		Set<INodeID> falseNegatives = new LinkedHashSet<>();
		falsePositives.remove(subscriberNodeId);
		if (arNodeState != null) {
			for (ARNodeState nodeState : arNodeState.getNodesInAoi()) {
				if (notificationStates.containsKey(nodeState.getId())) {
					/*
					 * True positive: node is in AOI and we know it, so remove
					 * from false positives.
					 */
					truePositives.add(nodeState.getId());
					falsePositives.remove(nodeState.getId());
				} else {
					/*
					 * False negative: node is in AOI but we do not know it.
					 */
					falseNegatives.add(nodeState.getId());
					assert !falsePositives.contains(nodeState.getId());
				}
			}
		} else {
			// we do not evaluate filter schemes in the AR scenario
		}

		long currentTime = Time.getCurrentTime();
		double trafficBytesPerSecond = 0;
		double notificationsPerSecond = 0;
		if (lastSnapshotTimestamp != -1) {
			long intervalLength = currentTime - lastSnapshotTimestamp;
			trafficBytesPerSecond = bytesReceived / ((double) (intervalLength / (double) Time.SECOND));
			notificationsPerSecond = notificationsReceived / ((double) intervalLength / (double) Time.SECOND);
		}

		bytesReceived = 0;
		notificationsReceived = 0;

		lastSnapshotTimestamp = currentTime;
		return new SubscriptionResultSnapshot(truePositives, falsePositives, falseNegatives, trafficBytesPerSecond,
				notificationsPerSecond);
	}

	/**
	 * Container for results that are to be persisted
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class SubscriptionResultSnapshot {

		private final Set<INodeID> truePositivesSet;

		private final Set<INodeID> falsePositivesSet;

		private final Set<INodeID> falseNegativesSet;

		private final int truePositives;

		private final int falsePositives;

		private final int falseNegatives;

		private final double trafficBytesPerSecond;

		private final double notificationsPerSecond;

		public SubscriptionResultSnapshot(Set<INodeID> truePositives, Set<INodeID> falsePositives,
				Set<INodeID> falseNegatives, double trafficBytesPerSecond, double notificationsPerSecond) {
			this.truePositives = truePositives.size();
			this.falsePositives = falsePositives.size();
			this.falseNegatives = falseNegatives.size();
			this.truePositivesSet = truePositives;
			this.falsePositivesSet = falsePositives;
			this.falseNegativesSet = falseNegatives;
			this.trafficBytesPerSecond = trafficBytesPerSecond;
			this.notificationsPerSecond = notificationsPerSecond;
		}

		/**
		 * Recall, Delivery ratio: # delivered / # interested (w/o duplicates).
		 * This metric is 1 in a perfect overlay, smaller otherwise.
		 * 
		 * @return
		 */
		public double getRecall() {
			if (truePositives + falseNegatives == 0) {
				return -1;
			}
			return (double) truePositives / (double) (truePositives + falseNegatives);
		}

		/**
		 * Ratio of irrelevant notifications out of all received notifications
		 * 
		 * @return
		 */
		public double getPrecision() {
			if (truePositives + falsePositives == 0) {
				return -1;
			}
			return (double) truePositives / (double) (truePositives + falsePositives);
		}

		/**
		 * Incoming notification traffic in bytes per second.
		 * 
		 * @return
		 */
		public double getTrafficBytesPerSecond() {
			return trafficBytesPerSecond;
		}

		/**
		 * Number of notifications received per second
		 * 
		 * @return
		 */
		public double getNotificationsPerSecond() {
			return notificationsPerSecond;
		}

		/**
		 * Notifications we received that actually matched the subscription
		 * 
		 * @return
		 */
		public int getTruePositives() {
			return truePositives;
		}

		/**
		 * Notifications we received that did NOT match the subscription
		 * 
		 * @return
		 */
		public int getFalsePositives() {
			return falsePositives;
		}

		/**
		 * Notifications we should have received.
		 * 
		 * @return
		 */
		public int getFalseNegatives() {
			return falseNegatives;
		}

		/**
		 * How many nodes were covered by this subscription?
		 * 
		 * @return
		 */
		public int getSubscriptionCoverage() {
			return truePositives + falseNegatives;
		}

		public Set<INodeID> getFalseNegativesSet() {
			return falseNegativesSet;
		}

		public Set<INodeID> getTruePositivesSet() {
			return truePositivesSet;
		}

		public Set<INodeID> getFalsePositivesSet() {
			return falsePositivesSet;
		}

		@Override
		public String toString() {
			return "SubscriptionResultSnapshot: recall: " + getRecall() + " precision: " + getPrecision()
					+ " coverage: " + getSubscriptionCoverage();
		}

	}

	private class NotificationState {

		public long lastUpdateTimestamp;

		public Location lastLocation;

	}

}
