package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.api.network.SimNetInterface;
import de.tud.kom.p2psim.impl.simengine.Simulator;
import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tud.kom.p2psim.impl.util.oracle.GlobalOracle;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.AccuracyAndCompletenessAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attributegenerator.AttributeGenerator;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attributegenerator.measurementprovider.IMeasurementProvider;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.MergeNotPossibleException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupSensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainer.MergePoint;

public class AccuracyAndCompletenessAnalyzerImpl implements AccuracyAndCompletenessAnalyzer, EventHandler {

	private DataObjectAggregationDupSensitiveImpl currentDupSensAggrDataObject;

	private boolean isRunning = false;

	private boolean _metricIsValid = false;

	private long interval;

	private boolean ENABLE_DAO = false;

	private boolean ENABLED_DAO_AT = false;

	private final int WRITE_DOWN_EVENT_ID = 72;

	private final int _EVENT_ENABLE_DAO_AT = 2001;

	private CraterCentralComponent cloud;

	private int writeDownCounter;

	private double onlineCompleteness;

	private long timeEnableDao;

	private long timeStopDao;

	private int writeDownCounterMax;

	private static AccuracyAndCompletenessAnalyzerImpl instance;

	public AccuracyAndCompletenessAnalyzerImpl() {
		instance = this;
		DAO.addEntityClass(AccuracyAndCompletenessMeasurement.class);
	}

	/**
	 * Returns the single instance of the {@link AccuracyAndCompletenessAnalyzer}
	 *
	 * @return
	 */
	public static AccuracyAndCompletenessAnalyzerImpl getInstance() {
		assert instance != null;
		return instance;
	}

	public void setTimeEnableDao(long timeEnableDao) {
		if (ENABLE_DAO) {
			this.timeEnableDao = timeEnableDao;
			Event.scheduleWithDelay(timeEnableDao, this, null, _EVENT_ENABLE_DAO_AT);
		}
	}

	public void setTimeStopDao(long timeStopDao) {
		this.timeStopDao = timeStopDao;
	}

	@Override
	public void start() {

		for (SimHost host : GlobalOracle.getHosts()) {
			try {
				CraterCentralComponent cloud = host.getComponent(CraterCentralComponent.class);
				this.cloud = cloud;
			} catch (ComponentNotAvailableException e) {
				// keine cloud
			}
			assert cloud != null : "Es wird keine Cloud Instanz in dieser Simulation genutzt.";
		}

		writeDownCounterMax = (int) ((timeStopDao - timeEnableDao) / interval);
		writeDownCounter = 0;
		isRunning = true;

		Event.scheduleWithDelay(interval, this, null, WRITE_DOWN_EVENT_ID);
	}

	@Override
	public void stop(Writer out) {
		if (ENABLE_DAO) {
			if (writeDownCounterMax > writeDownCounter) {
				calcAccuracyAndCompleteness();
				ENABLED_DAO_AT = false;
			}
		}
		isRunning = false;
		// calcAccuracyAndCompleteness();
		// isRunning = false;

		// if (writeDownCounter < (120 / ((double) interval / (double)
		// Time.MINUTE))) {
		// calcAccuracyAndCompleteness();
		// }
	}

	/**
	 * Called by {@link MCompleteness} Metric to get the current Completeness value.
	 * 
	 * @return
	 */
	public double getCompleteness() {
		return onlineCompleteness;
	}

	/**
	 * Called by {@link MCompleteness} Metric to check if current onlineCompletness is valid.
	 * 
	 * @return
	 */
	public boolean isValid() {
		if (_metricIsValid) {
			if (getCompleteness() >= 0 && getCompleteness() <= 100) {
				return _metricIsValid;
			} else {
				return !_metricIsValid;
			}
		}
		return _metricIsValid;
	}

	@Override
	public void onDuplicateSensitiveAggregationDataObjectReceived(
			LinkedList<DataObjectAggregationDupSensitiveImpl> toAddDupSensAggDataObjectList) {
		if (!isRunning)
			return;

		for (DataObjectAggregationDupSensitiveImpl toAddDupSensAggDataObject : toAddDupSensAggDataObjectList) {
			if (currentDupSensAggrDataObject == null) {
				currentDupSensAggrDataObject = toAddDupSensAggDataObject.clone();
				continue;
			}

			if (toAddDupSensAggDataObject.ofSameType(currentDupSensAggrDataObject)) {
				try {
					currentDupSensAggrDataObject.mergeValues(toAddDupSensAggDataObject, MergePoint.IN_ANALYZER);
				} catch (MergeNotPossibleException e) {
					e.printStackTrace();
				}
			} else
				throw new Error("Not possible");
		}

	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	public void setEnableDao(boolean enableDao) {
		this.ENABLE_DAO = enableDao;
	}


	private void calcAccuracyAndCompleteness() {
		if (currentDupSensAggrDataObject == null) {
			_metricIsValid = false;
			return;
		}
		_metricIsValid = true;
		LinkedList<INodeID> onlineHosts = new LinkedList<INodeID>();
		LinkedList<INodeID> offlineHosts = new LinkedList<INodeID>();
		LinkedList<INodeID> participatingOnlineHosts = new LinkedList<INodeID>();
		LinkedList<INodeID> participatingOfflineHosts = new LinkedList<INodeID>();

		LinkedHashMap<INodeID, Double> groundTruthValues = new LinkedHashMap<INodeID, Double>();

		for (SimHost host : GlobalOracle.getHosts()) {
			try {
				host.getComponent(CraterNodeComponent.class);
			} catch (ComponentNotAvailableException e1) {
				// no NodeComponent available -> Jump to next Host.
				continue;
			}

			boolean hostIsOnline = false;
			for (SimNetInterface net : host.getNetworkComponent().getSimNetworkInterfaces()) {
				if (net.isOnline()) {
					hostIsOnline = net.isOnline();
				} else {
					hostIsOnline = false;
				}
			}
			if (hostIsOnline) {

				try {
					AttributeGenerator attributeGenerator = host.getComponent(AttributeGenerator.class);
					LinkedList<IMeasurementProvider> measurementProvs = attributeGenerator.getProvs();
					for (IMeasurementProvider actMeasurementProvider : measurementProvs) {
						if (actMeasurementProvider.getIdentifier()
								.equals(host.getComponent(CraterNodeComponent.class).getMeasurementProviderToUse())) {
							groundTruthValues.put(host.getId(), actMeasurementProvider.getNextValue());
						}
					}

				} catch (ComponentNotAvailableException e) {
					e.printStackTrace();
				}
				onlineHosts.add(host.getId());
			} else {
				offlineHosts.add(host.getId());
			}
		}

		double ratioOnlineOfflineHosts = ((double) onlineHosts.size() / ((double) onlineHosts.size() + offlineHosts.size()))
				* 100;

		for (AggregationFunctionEntry actContainedEntry : currentDupSensAggrDataObject.getCurrentUsedEntries()) {
			if (onlineHosts.contains(actContainedEntry.getKey())
					&& !participatingOnlineHosts.contains(actContainedEntry.getKey())) {
				participatingOnlineHosts.add(actContainedEntry.getKey());
			} else if (offlineHosts.contains(actContainedEntry.getKey())
					&& !participatingOfflineHosts.contains(actContainedEntry.getKey()))
				participatingOfflineHosts.add(actContainedEntry.getKey());
			else if (participatingOnlineHosts.contains(actContainedEntry.getKey())
					|| participatingOfflineHosts.contains(actContainedEntry.getKey()))
				continue;
			else {
				// log.fatal("Node " + actContainedEntry.getKey() +
				// " is not contained in online neither offline list");
			}

		}

		onlineCompleteness = ((double) Math
				.round(((double) participatingOnlineHosts.size() / (double) onlineHosts.size()) * 10000)) / 100;

		double groundTruthValue = groundTruthValues.size();

		double relError = ((double) Math
				.round(((currentDupSensAggrDataObject.getAggregateValue() - groundTruthValue) / groundTruthValue) * 10000)) / 100;
		double totalError = currentDupSensAggrDataObject.getAggregateValue() - groundTruthValue;

		if (Double.isNaN(relError))
			relError = 0;
		if (Double.isNaN(totalError))
			totalError = 0;
		if (Double.isNaN(onlineCompleteness))
			onlineCompleteness = 0;
		if (Double.isNaN(ratioOnlineOfflineHosts))
			ratioOnlineOfflineHosts = 0;

		assert !Double.isNaN(relError) || !Double.isNaN(totalError) || !Double.isNaN(onlineCompleteness)
				|| !Double.isNaN(ratioOnlineOfflineHosts) : "Accuracy Assertion NaN";
		assert (relError < 0 && totalError < 0) || (relError >= 0 && totalError >= 0) : "Errors must have the same sign";

		if (ENABLED_DAO_AT && writeDownCounterMax > writeDownCounter) {
			writeDownCounter++;
			AccuracyAndCompletenessMeasurement currentMeasurement = new AccuracyAndCompletenessMeasurement(onlineCompleteness,
					relError, totalError, ratioOnlineOfflineHosts, onlineHosts.size());
			MeasurementDAO.storeCustomMeasurement(AccuracyAndCompletenessMeasurement.ACCURACY_AND_COMPLETENESS_METRIC,
					cloud.getHost().getId().value(), Simulator.getCurrentTime(), currentMeasurement);
		}

	}

	@Override
	public void eventOccurred(Object content, int type) {
		if (!isRunning)
			return;

		if (type == WRITE_DOWN_EVENT_ID) {
			calcAccuracyAndCompleteness();
			currentDupSensAggrDataObject = null;
			Event.scheduleWithDelay(interval, this, null, WRITE_DOWN_EVENT_ID);
		} else if (type == _EVENT_ENABLE_DAO_AT) {
			assert ENABLE_DAO;
			ENABLED_DAO_AT = true;
		}

	}

}
