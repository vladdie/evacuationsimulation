package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.transition.SelfTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.AbstractLocalTransport.NoAdHocTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.AbstractLocalTransport.WiFiAdHocTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.DirectUploadStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.GKRelayStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.GKRelayStrategy.RelayFunction;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream.StreamDirectorApp.Factory.Strategy;

/**
 * Application for the Stream Director (usually the cloud).
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class StreamDirectorApp extends StreamApp {

	private static List<StreamSourceApp> sources = new LinkedList<>();

	private static List<StreamConsumerApp> consumers = new LinkedList<>();

	private StreamModel streamModel = new StreamModel();

	public StreamDirectorApp(Host host) {
		super(host);
	}

	@Override
	public void initialize() {
		super.initialize();

		/*
		 * Basic Scenario: one source, one viewer, streaming for 1 minute
		 */
		Event.scheduleWithDelay(10 * Time.SECOND, new EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				/*
				 * Initial Transitions
				 */
				if (Factory.strategy == Strategy.DIRECT_UPLOAD) {
					// Default direct upload
					for (StreamSourceApp source : sources) {
						try {
							TransitionEngine tEngine = source.getHost().getComponent(TransitionEngine.class);
							tEngine.executeAtomicTransition(BypassPubSubComponent.PROXY_LOCAL_PHY,
									NoAdHocTransport.class);
							tEngine.executeAtomicTransition(BypassPubSubComponent.PROXY_UPLOAD_SCHEME,
									DirectUploadStrategy.class);
						} catch (ComponentNotAvailableException e) {
							e.printStackTrace();
						}
					}
				} else {
					// GK Strategy
					for (StreamSourceApp source : sources) {
						try {
							TransitionEngine tEngine = source.getHost().getComponent(TransitionEngine.class);
							tEngine.executeAtomicTransition(BypassPubSubComponent.PROXY_LOCAL_PHY,
									WiFiAdHocTransport.class);
							tEngine.executeAtomicTransition(BypassPubSubComponent.PROXY_UPLOAD_SCHEME,
									GKRelayStrategy.class);
							tEngine.executeSelfTransition(BypassPubSubComponent.PROXY_UPLOAD_SCHEME,
									GKRelayStrategy.class, new SelfTransition<GKRelayStrategy>() {
								@Override
								public void alterState(GKRelayStrategy mechanism) {
									if (Factory.strategy == Strategy.RELAY) {
										mechanism.setRelayFunction(
												new RoundRobbinRelayFunction(Integer.MAX_VALUE, false));
									} else if (Factory.strategy == Strategy.RELAY_AND_SELF) {
										mechanism.setRelayFunction(
												new RoundRobbinRelayFunction(Integer.MAX_VALUE, true));
									} else if (Factory.strategy == Strategy.ENHANCEMENT_LAYERS) {
										mechanism.setRelayFunction(new RelayEnhancementLayers());
									}
								}
							});
						} catch (ComponentNotAvailableException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}, null, 0);
		Event.scheduleWithDelay(Time.MINUTE, new EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				consumers.get(0).startViewing(streamModel);
				sources.get(0).startStream(streamModel);
			}
		}, null, 0);
		Event.scheduleWithDelay(Time.MINUTE + Factory.streamDuration, new EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				consumers.get(0).stopViewing();
				sources.get(0).stopStream();
			}
		}, null, 0);
	}

	/**
	 * Registers a source at the director (global knowledge)
	 * 
	 * @param source
	 */
	public static void registerSource(StreamSourceApp source) {
		StreamDirectorApp.sources.add(source);
	}

	/**
	 * Registers a sink (consumer) at the director
	 * 
	 * @param consumer
	 */
	public static void registerConsumer(StreamConsumerApp consumer) {
		StreamDirectorApp.consumers.add(consumer);
	}

	/**
	 * The Factory, used to configure this component.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class Factory implements HostComponentFactory {

		public static Strategy strategy = Strategy.DIRECT_UPLOAD;

		public static long streamDuration = 1 * Time.MINUTE;

		public static String experimentName = "none";

		public static enum Strategy {
			DIRECT_UPLOAD, RELAY, RELAY_AND_SELF, ENHANCEMENT_LAYERS
		}

		@Override
		public HostComponent createComponent(Host host) {
			return new StreamDirectorApp(host);
		}

		public static void setStrategy(String strategy) {
			Factory.strategy = Strategy.valueOf(strategy);
			assert Factory.strategy != null;
		}

		public void setStreamDuration(long streamDuration) {
			Factory.streamDuration = streamDuration;
		}

		public void setExperimentName(String name) {
			Factory.experimentName = name;
		}

	}

	/*
	 * Simple relay functions for inital paper and debugging
	 */

	/**
	 * Direct upload of base layer, relaying of enhancement layers if a relay is
	 * available.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public class RelayEnhancementLayers implements RelayFunction {

		@Override
		public OverlayContact getRelayForNotification(Notification n, List<OverlayContact> activeRelays,
				OverlayContact idForDirectUpload) {
			int blockId = n.getAttribute(ATTR_BLOCK_ID.getName(), ATTR_BLOCK_ID.getType()).getValue();
			/*
			 * Block ID == 0: Baselayer
			 */
			if (blockId == 0) {
				return idForDirectUpload;
			} else {
				if (blockId <= activeRelays.size()) {
					return activeRelays.get(blockId - 1);
				}
			}
			return null;
		}
	}

	/**
	 * Simple round robbin
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public class RoundRobbinRelayFunction implements RelayFunction {
		private int i = 0;
		private boolean enableDirectUpload;
		private int numRelays;

		public RoundRobbinRelayFunction(int numRelays, boolean enableDirectUpload) {
			this.enableDirectUpload = enableDirectUpload;
			this.numRelays = numRelays;
		}

		@Override
		public OverlayContact getRelayForNotification(Notification n, List<OverlayContact> activeRelays,
				OverlayContact idForDirectUpload) {
			int rounds = Math.min(numRelays, activeRelays.size());
			if (enableDirectUpload) {
				rounds++;
			}
			if (i >= rounds) {
				i = 0;
			}
			i++;
			if (enableDirectUpload && i == rounds) {
				// Direct Upload
				return idForDirectUpload;
			} else {
				return activeRelays.get(i - 1);
			}
		}
	}

}
