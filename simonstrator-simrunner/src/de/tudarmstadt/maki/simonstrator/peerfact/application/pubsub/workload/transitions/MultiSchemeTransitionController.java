package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.transitions;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Binder;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.GlobalComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.gossip.GossipDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway.NoGatewayBehavior;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway.StatelessGatewayBehavior;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.AbstractLocalTransport.NoAdHocTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.AbstractLocalTransport.WiFiAdHocTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.CloudPublicationHandler;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway.TransitionEnabledGatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudPublishMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction.AttractionSchemeClient;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.multi.MultiSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.multi.MultiSchemeBroker.MultiSchemeListener;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PubSubGlobalKnowledge;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlan;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.AtomicTransitionAction;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.StateChangeAction;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.TransitionAction;
import de.tudarmstadt.maki.simonstrator.service.transition.local.LocalTransitionEngine;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * This one does interact with the {@link MultiSchemeBroker} to trigger
 * sub-transitions of mechanisms for clients within a given attraction point
 * zone. It only interacts with the {@link BypassCloudComponent}.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class MultiSchemeTransitionController
		implements GlobalComponent, EventHandler, MultiSchemeListener, CloudPublicationHandler {

	private final int EVENT_INIT = 1;

	private BypassCloudComponent cloud;

	private TransitionCoordinator tCoord;

	private MultiSchemeBroker scheme;

	private LocalDistributionType localDirectDissemination = LocalDistributionType.GOSSIP;

	private LocalDistributionType localGatewayDissemination = LocalDistributionType.GOSSIP;

	private boolean useSelfHealing = false;

	private double adaptDensityOffset = 5.0;

	private double gatewayRatioOneHop = 0.2; // 20%

	private double gatewayRatioMultiHop = 0.1; // 10%

	private List<AttractionPointState> apStates = new LinkedList<>();

	/**
	 * Modes defined in the thesis
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public enum Mode {
		ADHOC, GATEWAY, ADAPT
	}

	private final Mode mode;

	private double localDisseminationGossipAlpha = 0.2;

	@XMLConfigurableConstructor({ "mode" })
	public MultiSchemeTransitionController(String mode) {
		this.mode = Mode.valueOf(mode);
		Binder.registerComponent(this);
		Event.scheduleImmediately(this, null, EVENT_INIT);
	}

	private boolean useGateways() {
		return mode != Mode.ADHOC;
	}

	@Override
	public void eventOccurred(Object content, int type) {
		if (type == EVENT_INIT) {
			for (Host host : Oracle.getAllHosts()) {
				try {
					cloud = host.getComponent(BypassCloudComponent.class);
					tCoord = host.getComponent(TransitionCoordinator.class);
				} catch (ComponentNotAvailableException e) {
					// next host
				}
			}
			if (cloud == null) {
				throw new AssertionError("Cloud component is required.");
			}

			/*
			 * Ensure and fetch MULTIBroker (we DO NOT support broker-side
			 * transitions between filter schemes in this scenario). Yes, we use
			 * the dirty _getMechanismInstance hack.
			 */
			scheme = ((LocalTransitionEngine) cloud.getTransitionEngine())._getMechanismInstance(
					BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER, MultiSchemeBroker.class);
			scheme.addMultiSchemeListener(this);

			/*
			 * Register as publication handler to enable interaction with the
			 * gateway selection.
			 */
			cloud.setPublicationHandler(this);
		}
	}

	@Override
	public void handlePublication(BypassCloudComponent cloud, CloudPublishMessage msg) {
		/*
		 * Use Gateway-Selection only within an ATTRACT-cluster!
		 */
		OverlayContacts attractClients = scheme.getAttractionSubscribers(msg.getNotification());
		AttractionPointState state = null;
		for (OverlayContact subscriber : attractClients) {
			state = getStateFor(subscriber);
			if (state != null) {
				break;
			}
		}
		if (!attractClients.isEmpty()) {
			if (useGateways()) {
				int numGws = getNumberOfGatewaysFor(state, attractClients.size());
				// With gateways
				if (numGws > 0) {
					/*
					 * FIX: for UNICAST delivery, we need to tell the
					 * GW-strategy to send contact data
					 */
					((TransitionEnabledGatewayLogic) cloud.getGatewayLogic()).setSendClientContacts(
							state.currentLocalGatewayDissemination == LocalDistributionType.UNICAST);

					Collection<CloudNotifyMessage> msgs = cloud.getGatewayLogic().packMessages(msg.getNotification(),
							attractClients, getNumberOfGatewaysFor(state, attractClients.size()));

					/*
					 * Store the number of gateways used (we consider each
					 * client that is contacted by the broker to be a gateway in
					 * this case, even if the individual client does not forward
					 * the message)
					 */
					PubSubGlobalKnowledge.getPublicationResultCollectorFor(msg.getNotification())
							.setNumberOfGateways(msgs.size());

					// System.out.println("Requested " + numGws + ", received "
					// + msgs.size() + " messages for "
					// + attractClients.size() + " subscribers.");

					for (CloudNotifyMessage notifyMessage : msgs) {
						cloud.sendViaMobileNetwork(notifyMessage);
					}
				} else {
					// Direct notification
					for (OverlayContact subscriber : attractClients) {
						CloudNotifyMessage notification = new CloudNotifyMessage(cloud.getLocalOverlayContact(),
								subscriber, msg.getNotification());
						cloud.sendViaMobileNetwork(notification);
					}
				}
			} else {
				// Direct notification
				for (OverlayContact subscriber : attractClients) {
					CloudNotifyMessage notification = new CloudNotifyMessage(cloud.getLocalOverlayContact(), subscriber,
							msg.getNotification());
					cloud.sendViaMobileNetwork(notification);
				}
			}
		}

		/*
		 * Other clients are notified via unicast
		 */
		OverlayContacts otherClients = scheme.getSecondSchemeSubscribers(msg.getNotification());
		if (!otherClients.isEmpty()) {
			for (OverlayContact subscriber : otherClients) {
				CloudNotifyMessage notification = new CloudNotifyMessage(cloud.getLocalOverlayContact(), subscriber,
						msg.getNotification());
				cloud.sendViaMobileNetwork(notification);
			}
		}

	}

	/**
	 * Simply return the expected number of gateways or -1
	 * 
	 * @param state
	 * @param subscribers
	 * @return
	 */
	private int getNumberOfGatewaysFor(AttractionPointState state, int subscribers) {
		/*
		 * Integrate the GW-Selection Ruleset -- depending on local
		 * dissemination (one-hop vs. multihop), number of nodes in the AP, and
		 * radius of the AP we chose more or less gateways. For now, these rules
		 * are static (derived from GW-evaluation), we do not add different
		 * rulesets here.
		 */

		assert mode == Mode.ADAPT || mode == Mode.GATEWAY;
		boolean isOneHop = state.currentLocalGatewayDissemination == LocalDistributionType.UNICAST
				|| state.currentLocalGatewayDissemination == LocalDistributionType.BROADCAST;

		/*
		 * If there is no local gateway dissemination, make everyone a gateway.
		 */
		if (state.currentLocalGatewayDissemination == LocalDistributionType.NONE) {
			// make everyone a gateway by returning -1
			return -1;
		}

		if (isOneHop) {
			// default: 20% GWs
			return (int) Math.ceil(((float) subscribers * gatewayRatioOneHop));
		} else {
			// default: 10% GWs
			return (int) Math.ceil(((float) subscribers * gatewayRatioMultiHop));
		}
	}

	/**
	 * Simply activate WiFi and the configured dissemination strategy, the
	 * gateway selection remains static.
	 * 
	 * @param client
	 * @param plan
	 * @param apState
	 */
	public void staticActivation(OverlayContact client, TransitionExecutionPlan plan, AttractionPointState apState) {
		if (useGateways()) {
			// Enable Gateway behavior + dissemination
			apState.currentLocalGatewayDissemination = localGatewayDissemination;
			plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
				localGatewayDissemination.clazz));
			if (localGatewayDissemination == LocalDistributionType.GOSSIP) {
				plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
						GossipDistribution.class, "Alpha", localDisseminationGossipAlpha));
			}
			// enable location updates in ATTRACT, as required by the GW-schemes
			plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME,
					AttractionSchemeClient.class, "ReportLocationUpdates", true));
		}
		apState.currentLocalDirectDissemination = localDirectDissemination;
		// Enable direct dissemination of local publications
		plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
				localDirectDissemination.clazz));
		if (localDirectDissemination == LocalDistributionType.GOSSIP) {
			plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
					GossipDistribution.class, "Alpha", localDisseminationGossipAlpha));
		}
		// Enable PHY
		plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_PHY, WiFiAdHocTransport.class));
	}

	/**
	 * Adaptive behavior based on the AttractionPoint size and its current
	 * number of subscribers.
	 * 
	 * We could adapt the local dissemination scheme (globally or within the AP
	 * only) and the gateway selection algorithm (however, only globally). For
	 * starters, just vary the utilized dissemination scheme based on the number
	 * of subscribers within the AP reach.
	 * 
	 * @param client
	 * @param plan
	 * @param apState
	 */
	public void adaptiveActivation(OverlayContact client, TransitionExecutionPlan plan, AttractionPointState apState) {

		/*
		 * Enable location updates in ATTRACT, as required by the GW-schemes.
		 * However, only update locations if the client moved at least 10m.
		 */
		plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME,
				AttractionSchemeClient.class, "ReportLocationUpdates", true));
		plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME,
				AttractionSchemeClient.class, "LocationUpdateThreshold", 10));

		/*
		 * Check, if we want to switch to another scheme. If so: with self
		 * healing, only add this to the plan and go on. Without self-healing,
		 * trigger a new total transition for all nodes within the AP.
		 * 
		 * For now, we simply use the same scheme for GW and direct
		 * dissemination.
		 */
		List<TransitionAction> actions = new LinkedList<>();

		/*
		 * Switch between PLAN-B for lower densities (better recall) and
		 * GOSSIP for higher densities (causes less traffic)
		 */
		double density = apState.getDensity();
		boolean change = false;
		if (density <= adaptDensityOffset) {
			// Transition back to PLANB
			actions.add(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
					LocalDistributionType.UNICAST.clazz));
			actions.add(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
					LocalDistributionType.PLANB.clazz));
			change = apState.currentLocalGatewayDissemination != LocalDistributionType.UNICAST;
			apState.currentLocalGatewayDissemination = LocalDistributionType.UNICAST;
			apState.currentLocalDirectDissemination = LocalDistributionType.PLANB;
		} else {
			// Transition to configured dissemination
			actions.add(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
					localGatewayDissemination.clazz));
			if (localGatewayDissemination == LocalDistributionType.GOSSIP) {
				actions.add(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
						LocalDistributionType.GOSSIP.clazz, "Alpha", localDisseminationGossipAlpha));
			}
			actions.add(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
					localDirectDissemination.clazz));
			if (localDirectDissemination == LocalDistributionType.GOSSIP) {
				actions.add(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
						LocalDistributionType.GOSSIP.clazz, "Alpha", localDisseminationGossipAlpha));
			}
			change = apState.currentLocalGatewayDissemination != localGatewayDissemination;
			apState.currentLocalGatewayDissemination = localGatewayDissemination;
			apState.currentLocalDirectDissemination = localDirectDissemination;
		}

		// use the provided plan to notify the node that just entered.
		for (TransitionAction action : actions) {
			plan.addAction(action);
		}
		if (change) {
			if (!useSelfHealing) {
				// TODO: trigger global transitions for all current subscribers.
				TransitionExecutionPlan planGlobal = tCoord.createExecutionPlan();
				// use the provided plan to notify the node that just entered.
				for (TransitionAction action : actions) {
					planGlobal.addAction(action);
				}
				tCoord.executeTransitionPlan(apState.getCurrentSubscribers(), planGlobal);
			} else {
				// Increment self-heal vector "clock".
				apState.numTransitions++;
			}
		}

		/*
		 * Activate self-heal for the newly joined node, if required.
		 */
		if (useSelfHealing && apState.currentLocalGatewayDissemination != LocalDistributionType.NONE) {
			plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
					AbstractLocalDissemination.class, "NumTransitions", apState.numTransitions));
			plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
					AbstractLocalDissemination.class, "ProxyName",
					BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION));
			plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
					AbstractLocalDissemination.class, "SelfHeal", true));
		}
		if (useSelfHealing && apState.currentLocalDirectDissemination != LocalDistributionType.NONE) {
			plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
					AbstractLocalDissemination.class, "NumTransitions", apState.numTransitions));
			plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
					AbstractLocalDissemination.class, "ProxyName", BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION));
			plan.addAction(new StateChangeAction(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
					AbstractLocalDissemination.class, "SelfHeal", true));
		}

		// Enable PHY
		plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_PHY, WiFiAdHocTransport.class));

	}

	/**
	 * Retrieve an {@link AttractionPointState} object for the AP, creates one
	 * if not yet done.
	 * 
	 * @param ap
	 * @return
	 */
	private AttractionPointState getOrCreateStateFor(AttractionPoint ap) {
		for (AttractionPointState apState : apStates) {
			if (apState.ap.equals(ap)) {
				return apState;
			}
		}
		AttractionPointState state = new AttractionPointState(ap);
		apStates.add(state);
		return state;
	}

	/**
	 * The state object the client is currently assigned to. Null otherwise.
	 * 
	 * @param client
	 * @return
	 */
	private AttractionPointState getStateFor(OverlayContact client) {
		for (AttractionPointState apState : apStates) {
			if (apState.getCurrentSubscribers().contains(client)) {
				return apState;
			}
		}
		return null;
	}

	@Override
	public void onSwitchClient(OverlayContact client, SchemeName newScheme, TransitionExecutionPlan plan,
			AttractionPoint ap) {
		if (newScheme == SchemeName.ATTRACTION) {
			AttractionPointState state = getOrCreateStateFor(ap);
			if (useGateways()) {
				plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY,
						StatelessGatewayBehavior.class));
			}
			if (mode == Mode.ADAPT) {
				adaptiveActivation(client, plan, state);
			} else {
				staticActivation(client, plan, state);
			}
		} else {
			// turn everything off
			if (useGateways()) {
				plan.addAction(
						new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY, NoGatewayBehavior.class));
				plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
						LocalDistributionType.NONE.clazz));
			}
			// + direct local dissemination off.
			plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
					LocalDistributionType.NONE.clazz));
			plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_LOCAL_PHY, NoAdHocTransport.class));
		}
	}

	/**
	 * Container for state associated to the ap
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private class AttractionPointState {

		public final AttractionPoint ap;

		public LocalDistributionType currentLocalDirectDissemination;

		public LocalDistributionType currentLocalGatewayDissemination;

		// Counter required for self-heal...
		public int numTransitions;

		public AttractionPointState(AttractionPoint ap) {
			this.ap = ap;
		}

		public Set<OverlayContact> getCurrentSubscribers() {
			return scheme.getAllAttractionSubscribers().get(ap);
		}

		public double getDensity() {
			double rad_100m2 = this.ap.getRadius() * this.ap.getRadius() / 10000;
			return this.getCurrentSubscribers().size() / rad_100m2;
		}

	}

	/**
	 * Alpha for gossip, if configured
	 * 
	 * @param type
	 */
	public void setGossipAlpha(double alpha) {
		localDisseminationGossipAlpha = alpha;
	}

	/**
	 * Offset for the density parameter (nodes per 100m2), below which
	 * GW-Selection will be disabled.
	 * 
	 * @param densityOffset
	 */
	public void setGatewayDensityOffset(double gatewayDensityOffset) {
		throw new UnsupportedOperationException("This parameter is no longer supported.");
	}

	/**
	 * Ratio of gateways out of the total number of subscribers used as input to
	 * the gateway selection in case of multi-hop dissemination
	 * 
	 * @param gatewayRatioMultiHop
	 */
	public void setGatewayRatioMultiHop(double gatewayRatioMultiHop) {
		this.gatewayRatioMultiHop = gatewayRatioMultiHop;
	}

	/**
	 * Ratio of gateways out of the total number of subscribers used as input to
	 * the gateway selection in case of one-hop dissemination
	 * (UNICAST/BROADCAST)
	 * 
	 * @param gatewayRatioOneHop
	 */
	public void setGatewayRatioOneHop(double gatewayRatioOneHop) {
		this.gatewayRatioOneHop = gatewayRatioOneHop;
	}

	/**
	 * Density for dissemination mechanism transitions in ADAPT
	 * 
	 * @param adaptDensityOffset
	 */
	public void setAdaptDensityOffset(double adaptDensityOffset) {
		this.adaptDensityOffset = adaptDensityOffset;
	}

	/**
	 * IF true, self-healing is used instead of a global transition
	 * 
	 * @param useSelfHealing
	 */
	public void setUseSelfHealing(boolean useSelfHealing) {
		this.useSelfHealing = useSelfHealing;
	}

	/**
	 * Dissemination to use for direct local publications
	 * 
	 * @param type
	 */
	public void setLocalDirectDissemination(String type) {
		localDirectDissemination = LocalDistributionType.valueOf(type);
	}

	/**
	 * Dissemination to use for the gateways.
	 * 
	 * @param type
	 */
	public void setLocalGatewayDissemination(String type) {
		localGatewayDissemination = LocalDistributionType.valueOf(type);
	}

}
