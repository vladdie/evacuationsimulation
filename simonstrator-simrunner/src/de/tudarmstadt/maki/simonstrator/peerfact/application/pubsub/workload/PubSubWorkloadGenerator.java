/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.tud.kom.p2psim.api.application.WorkloadGenerator;
import de.tud.kom.p2psim.api.application.WorkloadListener;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubWorkloadApp;

/**
 * A Workload-generator specifically for pub/sub overlays (i.e., getApplication
 * needs to return the pub/sub workload app).
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Oct 14, 2013
 */
public abstract class PubSubWorkloadGenerator<T extends NodeState> implements
		WorkloadGenerator {

	private final List<WorkloadListener> listeners = new LinkedList<WorkloadListener>();

	protected final List<T> states = new LinkedList<T>();

	protected final Map<INodeID, T> statesByHostId = new LinkedHashMap<INodeID, T>();

	protected boolean initialized = false;

	protected final Random rnd = Randoms
			.getRandom(PubSubWorkloadGenerator.class);

	protected PubSubWorkloadParameter publicationSize;

	protected PubSubWorkloadParameter publicationsPerSecond;

	@Override
	public void addWorkloadListener(WorkloadListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
			T state = createNodeStateObject((PubSubWorkloadApp) listener
					.getApplication());
			states.add(state);
			statesByHostId.put(listener.getApplication().getHost().getId(),
					state);
			new PeerStatusListener(
					(PubSubWorkloadApp) listener.getApplication());
		}
	}

	@Override
	public void removeWorkloadListener(WorkloadListener listener) {
		listeners.remove(listener);
	}

	public T getStateFor(INodeID hostId) {
		return statesByHostId.get(hostId);
	}

	/**
	 * 
	 * @param app
	 * @return
	 */
	protected abstract T createNodeStateObject(PubSubWorkloadApp app);

	/**
	 * Called once when the first node changes its peer status
	 */
	protected abstract void initialize();

	/**
	 * If the overlay's connectivity changed and if the IPeerStatus listener is
	 * informed correctly, you can act on this information as well - for
	 * example: pause the workload on Absent, or start the workload on Active.
	 * 
	 * @param state
	 * @param newStatus
	 */
	protected abstract void onPeerStatusChanged(T state,
			PeerStatus newStatus);

	/**
	 * Changes the default size of a publication (in byte)
	 * 
	 * @param publicationSize
	 */
	public void setPublicationSize(PubSubWorkloadParameter publicationSize) {
		this.publicationSize = publicationSize;
	}

	/**
	 * Average number of publications per second (globally)
	 * 
	 * @param publicationsPerSecond
	 */
	public void setPublicationsPerSecond(
			PubSubWorkloadParameter publicationsPerSecond) {
		this.publicationsPerSecond = publicationsPerSecond;
	}

	/**
	 * Trigger
	 * 
	 * @author bjoern
	 * @version 1.0, May 2, 2014
	 */
	private class PeerStatusListener implements IPeerStatusListener {

		private final INodeID hostId;

		public PeerStatusListener(PubSubWorkloadApp app) {
			app.getPubsub().addPeerStatusListener(this);
			this.hostId = app.getHost().getId();
		}

		@Override
		public void peerStatusChanged(OverlayComponent source,
				PeerStatus peerStatus) {
			if (!PubSubWorkloadGenerator.this.initialized) {
				PubSubWorkloadGenerator.this.initialized = true;
				PubSubWorkloadGenerator.this.initialize();
			}
			onPeerStatusChanged(statesByHostId.get(hostId), peerStatus);
		}

	}

	/**
	 * This workload generator publishes at fixed intervals. Subscriptions are
	 * sent only at the very beginning of the node's lifecycle.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, Oct 14, 2013
	 */
	public static class ConstantRateWorkloadGenerator extends
			PubSubWorkloadGenerator<NodeState> implements EventHandler {

		private final static int EVENT_SUBSCRIBE = 2;

		private final static int EVENT_PUBLISH = 3;

		private final static String TOPIC = "/pubsub/workload";

		private long MIN_DELAY_UNTIL_FIRST_SUBSCRIPTION = 1 * Time.MINUTE;

		private long MAX_DELAY_UNTIL_FIRST_SUBSCRIPTION = 3 * Time.MINUTE;

		private long MIN_DELAY_UNTIL_FIRST_PUBLICATION = 1 * Time.MINUTE;

		private long MAX_DELAY_UNTIL_FIRST_PUBLICATION = 2 * Time.MINUTE;

		private long DELAY_UNTIL_WORKLOAD_START = 5 * Time.MINUTE;

		private PubSubWorkloadParameter delayBetweenPublications;

		private int numSubscribers = 10;

		private int numPublishers = 5;

		private int numBoth = 0;

		private Topic t = null;

		@Override
		protected void initialize() {
			/*
			 * Assign Publishers, Subscribers, Both
			 */
			assert delayBetweenPublications != null;
			List<NodeState> statesToShuffle = new LinkedList<NodeState>(states);
			Collections.shuffle(statesToShuffle, rnd);
			int i = 0;
			for (NodeState nodeState : statesToShuffle) {
				if (i < numSubscribers) {
					nodeState.enableSubscriptions = true;
				} else if (i < numPublishers + numSubscribers) {
					nodeState.enablePublications = true;
				} else if (i < numBoth + numPublishers + numSubscribers) {
					nodeState.enablePublications = true;
					nodeState.enableSubscriptions = true;
				}
				i++;
			}
		}

		@Override
		public void eventOccurred(Object content, int type) {
			assert content instanceof NodeState;
			NodeState state = (NodeState) content;
			/*
			 * Subscribe to the test-topic
			 */
			if (type == EVENT_SUBSCRIBE) {
				if (t == null) {
					t = state.getApp().getPubsub().createTopic(TOPIC);
				}
				Subscription s = state.getApp().getPubsub()
						.createSubscription(t);
				s._getSubscriptionInfo(new SubscriptionInfoImpl(state.getApp().getHost().getId()));
				state.doSubscribe(s, new PubSubListener() {
					@Override
					public void onNotificationArrived(
							Subscription matchedSubscription,
							Notification notification) {
						// not interested.
					}
				});
			}

			/*
			 * Publish something on the test-topic and re-sechedule the publish
			 * event
			 */
			if (type == EVENT_PUBLISH) {
				if (t == null) {
					t = state.getApp().getPubsub().createTopic(TOPIC);
				}
				Notification n = state.getApp().getPubsub()
						.createNotification(t, null);
				n._getNotificationInfo(new NotificationInfoImpl(
						(long) publicationSize.getCurrentValue(), state
								.getApp().getHost().getHostId()));

				state.doPublish(n);
				long delay = (long) delayBetweenPublications.getCurrentValue();
				Event.scheduleWithDelay(delay, this, state, EVENT_PUBLISH);
			}
		}

		@Override
		protected void onPeerStatusChanged(NodeState state, PeerStatus newStatus) {
			if (newStatus == PeerStatus.PRESENT) {
				/*
				 * Schedule the first subscribe-action shortly after the node
				 * became present
				 */
				long delayUntilSubscription = DELAY_UNTIL_WORKLOAD_START;
				if (state.enableSubscriptions) {
					delayUntilSubscription = DELAY_UNTIL_WORKLOAD_START
							+ (long) (rnd.nextDouble() * (MAX_DELAY_UNTIL_FIRST_SUBSCRIPTION - MIN_DELAY_UNTIL_FIRST_SUBSCRIPTION));
					Event.scheduleWithDelay(delayUntilSubscription, this,
							state, EVENT_SUBSCRIBE);
				}
				/*
				 * Schedule the first publication either after the first
				 * subscription or, if subscriptions are disabled on this group,
				 * after the min delay.
				 */
				if (state.enablePublications) {
					long delayUntilPublication = delayUntilSubscription
							+ (long) (rnd.nextDouble() * (MAX_DELAY_UNTIL_FIRST_PUBLICATION - MIN_DELAY_UNTIL_FIRST_PUBLICATION));
					Event.scheduleWithDelay(delayUntilPublication, this, state,
							EVENT_PUBLISH);
				}
			}
		}

		@Override
		protected NodeState createNodeStateObject(PubSubWorkloadApp app) {
			return new NodeState(app);
		}

		/**
		 * The delay between publications of one node. Default: 5 Seconds
		 * 
		 * @param delayBetweenPublications
		 */
		public void setDelayBetweenPublications(
				PubSubWorkloadParameter delayBetweenPublications) {
			this.delayBetweenPublications = delayBetweenPublications;
		}

		/**
		 * Initial delay until the first action of the Workload takes place.
		 * Default: 5 Minutes
		 * 
		 * @param startWorkloadAfter
		 */
		public void setDelayUntilWorkloadStart(long delayUntilWorkloadStart) {
			DELAY_UNTIL_WORKLOAD_START = delayUntilWorkloadStart;
		}

		public void setNumPublishers(int numPublishers) {
			this.numPublishers = numPublishers;
		}

		public void setNumSubscribers(int numSubscribers) {
			this.numSubscribers = numSubscribers;
		}

		public void setNumBoth(int numBoth) {
			this.numBoth = numBoth;
		}

	}

	/**
	 * Dummy "No-Workload" generator
	 * 
	 * @author Bjoern Richerzhagen
	 */
	public static class NoWorkloadGenerator extends
			PubSubWorkloadGenerator<NodeState> {

		@Override
		protected void initialize() {
			// do nothing
		}

		@Override
		protected void onPeerStatusChanged(NodeState state, PeerStatus newStatus) {
			// don't care at all.
		}

		@Override
		protected NodeState createNodeStateObject(PubSubWorkloadApp app) {
			return new NodeState(app);
		}

	}

}
