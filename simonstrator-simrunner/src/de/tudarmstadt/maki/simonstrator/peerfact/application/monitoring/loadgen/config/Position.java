package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config;

import de.tud.kom.p2psim.impl.topology.PositionVector;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

public class Position extends PositionVector {

	@XMLConfigurableConstructor({ "longitude", "latitude"})
	public Position(double longitude, double latitude) {
		super(longitude, latitude);
	}

}
