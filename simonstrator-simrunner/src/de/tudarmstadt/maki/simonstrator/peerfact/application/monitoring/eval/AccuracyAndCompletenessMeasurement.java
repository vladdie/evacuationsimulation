package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;


@Entity
@Table(name = "accuracy_and_completeness_measurements")
public class AccuracyAndCompletenessMeasurement extends CustomMeasurement{

	public static final MetricDescription ACCURACY_AND_COMPLETENESS_METRIC = new MetricDescription(
			AccuracyAndCompletenessAnalyzerImpl.class, "AccuracyAndCompletenessErrors",
			"Provides the accuracy and completeness measures at the server for each interval", "");
	
	@SuppressWarnings("unused")
	private double completeness;
	
	@SuppressWarnings("unused")
	private double accuracyRelativeError;
	
	@SuppressWarnings("unused")
	private double accuracyTotalError;
	
	@SuppressWarnings("unused")
	private double ratioOnlineOfflineHosts;
	
	@SuppressWarnings("unused")
	private long numberOfOnlineHosts;
	
	public AccuracyAndCompletenessMeasurement(double completeness, double accuracyRelativeError, double accuracyTotalError, double ratioOnlineOfflineHosts, long numberOfOnlineHosts){
		super();
		this.completeness = completeness;
		this.accuracyRelativeError = accuracyRelativeError;
		this.accuracyTotalError = accuracyTotalError;
		this.ratioOnlineOfflineHosts = ratioOnlineOfflineHosts;
		this.numberOfOnlineHosts = numberOfOnlineHosts;
	}
}
