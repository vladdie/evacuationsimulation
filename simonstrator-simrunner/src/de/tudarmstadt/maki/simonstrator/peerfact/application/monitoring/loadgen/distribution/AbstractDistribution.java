package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.distribution;

/**
 * represents any sort of distribution.
 * 
 * @author Simon
 * @version 1.0, 16.06.2016
 */
public interface AbstractDistribution {

	/**
	 * @param timePoint
	 *            where the number of nodes are wanted from.
	 * 
	 * @param numberOfFilteredNodes
	 *            the number of nodes which is the maximum which can be chosen.
	 * 
	 * @return the current number of nodes which should send requests. If time
	 *         point is invalid, 0 is returned.
	 */
	public int getCount(long timePoint, int numberOfFilteredNodes);

}
