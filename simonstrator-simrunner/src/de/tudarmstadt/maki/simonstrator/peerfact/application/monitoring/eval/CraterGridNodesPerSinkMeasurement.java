package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;

@Entity
@Table(name = "crater_grid_nodes_per_sink")
public class CraterGridNodesPerSinkMeasurement extends CustomMeasurement {

	public static final MetricDescription CRATER_GRID_AVG_NODES_PER_SINK = new MetricDescription(CraterGridAvgNodesPerSinkAnalyzer.class,
			"CraterGridNodesPerSinkMeasurement",
			"Gives a periodic estimate for the snapshop how many nodes per sink are in avg per grid.", "");

	@SuppressWarnings("unused")
	private long nodeID;

	@SuppressWarnings("unused")
	private double avgNodesPerSink;

	@SuppressWarnings("unused")
	private double jainFairnessAvgNodesPerSink;

	public CraterGridNodesPerSinkMeasurement(long nodeID, double avgNodesPerSink, double jainFairnessAvgNodesPerSink) {
		super();
		this.nodeID = nodeID;
		this.avgNodesPerSink = avgNodesPerSink;
		this.jainFairnessAvgNodesPerSink = jainFairnessAvgNodesPerSink;
	}

}
