package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.vis;

import java.awt.Container;
import java.awt.Frame;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import javax.swing.JFrame;
import javax.xml.bind.annotation.XmlRootElement;

import de.tud.kom.p2psim.impl.analyzer.metric.MetricAnalyzer;
import de.tud.kom.p2psim.impl.topology.DefaultTopologyComponent;
import de.tud.kom.p2psim.impl.topology.movement.modularosm.ModularMovementModel;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView.VisualizationInjector;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.LifecycleComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.api.component.transition.SelfTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid.ExtGridSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste.SpaceTimeEnvelopeSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions.DefaultLocationBasedTransitionBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions.DefaultLocationBasedTransitionClient;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.PubSubWorkloadApp;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.PubSubResultPersistingLiveStats;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.SubscriptionResultCollector.SubscriptionResultSnapshot;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.ARGameWorkload;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.ARGameWorkload.ARNodeState;
import de.tudarmstadt.maki.simonstrator.peerfact.rest.ScenarioRestHandler;
import de.tudarmstadt.maki.simonstrator.peerfact.rest.ScenarioRestHandler.ClientConnection;
import de.tudarmstadt.maki.simonstrator.peerfact.rest.ScenarioRestHandler.RestClientEndpoint;
import de.tudarmstadt.maki.simonstrator.peerfact.rest.ScenarioRestHandler.RestClientListener;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlan;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.AtomicTransitionAction;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Sidebar for the pub/sub subscription scheme demo, used to control the
 * scenario and show some metrics.
 * 
 * @author Bjoern Richerzhagen
 *
 */
@SuppressWarnings("restriction")
public class PubSubAppVisSidebar extends JFXPanel implements RestClientEndpoint, RestClientListener {

	private static final long serialVersionUID = 1L;

	private static final PubSubAppVisSidebar instance = new PubSubAppVisSidebar();

	protected TransitionCoordinator tCoord;

	protected TransitionEngine tEngineBroker;

	protected List<TransitionEngine> clientEngines;

	protected BypassCloudComponent cloud;

	protected Set<OverlayContact> allClients;

	protected SchemeName activeScheme;

	protected LocalDistributionType activeDissemination;

	private List<Button> transitionButtons = new LinkedList<>();

	private List<Button> transitionButtonsDissemination = new LinkedList<>();

	private List<Button> selfTransitionButtonsGrid = new LinkedList<>();

	private List<Button> selfTransitionButtonsSTE = new LinkedList<>();

	private GridPane grid;

	private Text onlineNodesLabel;

	protected int[] activeGridSize = { 2, 2 };

	protected double alpha_ste = 1.0;

	private static final int CHURN_CTRL_ROW = 1;

	private static final int BTN_ROW = CHURN_CTRL_ROW + 2;

	private static final int PLOT_ROW = BTN_ROW + 1;

	protected final static int SIDEBAR_WIDTH = 400;

	protected DemoChurnLabel churn;

	private PubSubAppVisSidebar() {
		setBounds(0, 0, SIDEBAR_WIDTH, 200);
		setOpaque(true);
		setVisible(true);
		Event.scheduleWithDelay(Time.SECOND, new de.tudarmstadt.maki.simonstrator.api.EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				initialize();
			}
		}, null, 0);
	}

	public static PubSubAppVisSidebar getInstance() {
		return instance;
	}

	private void initialize() {

		churn = new DemoChurnLabel();

		ScenarioRestHandler.addRestClientEndpoint(this);
		ScenarioRestHandler.addRestClientListener(this);

		// Force main view to fullscreen
		Container frame = getParent();
		while (!(frame instanceof JFrame)) {
			frame = frame.getParent();
		}
		JFrame jFrame = (JFrame) frame;
		jFrame.setVisible(false);
		jFrame.dispose();
		jFrame.setExtendedState(Frame.MAXIMIZED_BOTH);
		jFrame.setUndecorated(true);
		jFrame.pack();
		jFrame.setVisible(true);

		allClients = new LinkedHashSet<>();
		clientEngines = new LinkedList<>();
		for (Host host : Oracle.getAllHosts()) {
			try {
				allClients.add(host.getComponent(BypassPubSubComponent.class).getLocalOverlayContact());
				TransitionEngine clientEngine = host.getComponent(TransitionEngine.class);
				clientEngines.add(clientEngine);
				clientEngine.registerTransition(
						BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME, new DefaultLocationBasedTransitionClient());

				// From here on only the cloud.
				cloud = host.getComponent(BypassCloudComponent.class);
				host.getComponent(TransitionEngine.class).registerTransition(
						BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
						new DefaultLocationBasedTransitionBroker());
				tCoord = host.getComponent(TransitionCoordinator.class);
				tEngineBroker = host.getComponent(TransitionEngine.class);
				clientEngines.remove(clientEngine);
			} catch (ComponentNotAvailableException e) {
				//
			}
		}

		// This method is invoked on the JavaFX thread
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				grid = new GridPane();
				Scene scene = new Scene(grid, SIDEBAR_WIDTH, 400);
				setScene(scene);

				activeScheme = SchemeName.STE;
				populateChurnCtrl();

				if (tCoord != null && tEngineBroker != null) {
					populateTransitionButtons();
					populateSelfTransitionButtonsGrid();
					populateSelfTransitionButtonsSTE();
					populateStats();
				}

				// local dissemination control
				populateTransitionButtonsDissemination();

				switchedScheme();
				setSize(SIDEBAR_WIDTH, 800);

				VisualizationInjector.invalidate();
			}
		});
	}

	@Override
	public String getEndpointId() {
		return "bypass";
	}

	@Override
	public Class<? extends LifecycleComponent> getEndpointComponentClass() {
		return BypassClientComponent.class;
	}

	/**
	 * Custom wrapper for metrics inside a JFX-Plot
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private static class MetricsLineChart extends LineChart<Number, Number>
			implements de.tudarmstadt.maki.simonstrator.api.EventHandler {

		private Map<Metric<?>, XYChart.Series<Number, Number>> data = new LinkedHashMap<>();

		protected NumberAxis xAx;

		protected NumberAxis yAx;

		protected final static int maxDataPts = 30;

		protected final static long INTERVAL = 5 * Time.SECOND;

		public MetricsLineChart() {
			super(new NumberAxis(0, maxDataPts, 30), new NumberAxis());
			Event.scheduleWithDelay(INTERVAL, MetricsLineChart.this, null, 1);
			yAx = (NumberAxis) getYAxis();
			xAx = (NumberAxis) getXAxis();
			xAx.setForceZeroInRange(false);
			xAx.setAutoRanging(false);
		}

		public void addMetric(Metric<?> m) {
			this.addMetric(m, m.getDescription());
		}

		public void addMetric(Metric<?> m, String label) {
			XYChart.Series<Number, Number> series = new XYChart.Series<>();
			series.setName(label);
			data.put(m, series);
			this.getData().add(series);
		}

		@Override
		public void eventOccurred(Object content, int type) {
			// Add new data items, reschedule
			for (Map.Entry<Metric<?>, XYChart.Series<Number, Number>> entry : data.entrySet()) {
				XYChart.Series<Number, Number> series = entry.getValue();
				final Number val = (Number) entry.getKey().getOverallMetric().getValue();
				if (entry.getKey().getOverallMetric().isValid()) {
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							if (series.getData().size() >= maxDataPts) {
								// move everything
								series.getData().remove(0);
								for (XYChart.Data<Number, Number> dataPt : series.getData()) {
									dataPt.setXValue(dataPt.getXValue().intValue() - 1);
								}
							}
							series.getData().add(new XYChart.Data<Number, Number>(series.getData().size(), val));
						}
					});
				}
			}
			Event.scheduleWithDelay(INTERVAL, MetricsLineChart.this, null, 0);

		}

	}

	private Text getCaption(String text) {
		Text txt = new Text(text);
		txt.setFont(Font.font("Verdana", 18));
		txt.setFill(Color.DARKGRAY);
		return txt;
	}

	private void populateChurnCtrl() {
		HBox vbLabel = new HBox();
		vbLabel.setSpacing(10);
		vbLabel.setPadding(new Insets(20, 20, 10, 20));
		onlineNodesLabel = getCaption("0");
		vbLabel.getChildren().addAll(getCaption("Churn Control:"), onlineNodesLabel, getCaption("Hosts online"));
		grid.add(vbLabel, 0, CHURN_CTRL_ROW, 4, 1);

		HBox vbButtons = new HBox();
		vbButtons.setSpacing(10);
		vbButtons.setPadding(new Insets(20, 20, 10, 20));

		vbButtons.getChildren().addAll(getChurnButton(-10, 5 * Time.SECOND), getChurnButton(-1, Time.SECOND),
				getChurnButton(+1, Time.SECOND), getChurnButton(+10, 5 * Time.SECOND));
		grid.add(vbButtons, 0, CHURN_CTRL_ROW + 1, 4, 1);
	}

	private void populateStats() {
		MetricsLineChart precisionAndRecall = new MetricsLineChart();
		precisionAndRecall.addMetric(MetricAnalyzer.getMetric("MDeliveryRecall"));
		precisionAndRecall.addMetric(MetricAnalyzer.getMetric("MDeliveryPrecision"));
		precisionAndRecall.yAx.setAutoRanging(false);
		precisionAndRecall.yAx.setLowerBound(0.0);
		precisionAndRecall.yAx.setUpperBound(1.0);
		precisionAndRecall.yAx.setTickUnit(0.2);
		precisionAndRecall.yAx.setMinorTickCount(4);
		grid.add(precisionAndRecall, 0, PLOT_ROW, 4, 1);

		MetricsLineChart complexity = new MetricsLineChart();
		complexity.addMetric(MetricAnalyzer.getMetric("MDeliveryMatchComplexity"), "Filter Complexity");
		grid.add(complexity, 0, PLOT_ROW + 1, 4, 1);

		MetricsLineChart cloudTraffic = new MetricsLineChart();
		cloudTraffic.addMetric(MetricAnalyzer.getMetric("Avg_Delta_Active_CountBypassSchemeMessageReceiveALL_Clouds"),
				"Ctrl. to Cloud");
		cloudTraffic.addMetric(MetricAnalyzer.getMetric("Avg_Delta_Active_CountBypassSchemeMessageSendALL_Clouds"),
				"Ctrl. from Cloud");
		grid.add(cloudTraffic, 0, PLOT_ROW + 2, 4, 1);
	}

	private void populateTransitionButtons() {
		VBox vbButtons = new VBox();
		vbButtons.setSpacing(10);
		vbButtons.setPadding(new Insets(20, 20, 10, 20));
		vbButtons.getChildren().add(getCaption("Filter Scheme"));
		vbButtons.getChildren().addAll(getTransitionButton(SchemeName.STE), getTransitionButton(SchemeName.LBS),
				getTransitionButton(SchemeName.GRID), getTransitionButton(SchemeName.EXT_GRID_RECT),
				getTransitionButton(SchemeName.ATTRACTION), getTransitionButton(SchemeName.MULTI));
		grid.add(vbButtons, 0, BTN_ROW);
	}

	private void populateTransitionButtonsDissemination() {
		VBox vbButtons = new VBox();
		vbButtons.setSpacing(10);
		vbButtons.setPadding(new Insets(20, 20, 10, 20));
		vbButtons.getChildren().add(getCaption("Dissemination"));
		for (LocalDistributionType type : LocalDistributionType.values()) {
			vbButtons.getChildren().add(getTransitionButtonDissemination(type));
		}
		grid.add(vbButtons, 3, BTN_ROW);
	}

	private void populateSelfTransitionButtonsGrid() {
		VBox vbButtons = new VBox();
		vbButtons.setSpacing(10);
		vbButtons.setPadding(new Insets(20, 20, 10, 20));
		vbButtons.getChildren().add(getCaption("Grid"));
		vbButtons.getChildren().addAll(getSelfTransitionButtonGrid(2, 2), getSelfTransitionButtonGrid(4, 4),
				getSelfTransitionButtonGrid(8, 8), getSelfTransitionButtonGrid(2, 8));
		grid.add(vbButtons, 1, BTN_ROW);
	}

	private void populateSelfTransitionButtonsSTE() {
		VBox vbButtons = new VBox();
		vbButtons.setSpacing(10);
		vbButtons.setPadding(new Insets(20, 20, 10, 20));
		vbButtons.getChildren().add(getCaption("Alpha"));
		vbButtons.getChildren().addAll(getSelfTransitionButtonSTE(0.5), getSelfTransitionButtonSTE(1.0),
				getSelfTransitionButtonSTE(2.0));
		grid.add(vbButtons, 2, BTN_ROW);
	}

	protected Set<OverlayContact> getContactsForTransition() {
		/*
		 * The following code wont work, as newly joining peers use the wrong
		 * scheme and the cloud itself might not be notified. For the sake of
		 * the demo, we simply rely on the oracle.
		 */
		// return cloud.getSubscriptionScheme().getSubscribers()
		return allClients;
	}

	protected void switchedScheme() {
		for (Button btn : selfTransitionButtonsGrid) {
			btn.setDisable(activeScheme != SchemeName.GRID && activeScheme != SchemeName.EXT_GRID_RECT);
		}
		for (Button btn : selfTransitionButtonsSTE) {
			btn.setDisable(activeScheme != SchemeName.STE);
		}
	}

	/**
	 * A button to manipulate the number of active nodes
	 * 
	 * @param targetClass
	 * @param label
	 * @return
	 */
	private Button getChurnButton(int difference, long duration) {
		Button btn = new Button(difference < 0 ? "- " + (-1 * difference) : "+ " + difference);
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				System.out.println("Churn: " + difference);
				ScenarioRestHandler.triggerChurn(difference, duration, PubSubAppVisSidebar.this);
			}
		});
		return btn;
	}

	/**
	 * Create and return a JButton that triggers a self-transition
	 * 
	 * @param targetClass
	 * @param label
	 * @return
	 */
	private Button getSelfTransitionButtonGrid(int gridx, int gridy) {
		Button btn = new Button(gridx + "x" + gridy);
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				System.out.println("Self-Transition to " + gridx + "x" + gridy);
				Event.scheduleWithDelay(Time.SECOND, new de.tudarmstadt.maki.simonstrator.api.EventHandler() {

					@Override
					public void eventOccurred(Object content, int type) {
						if (activeScheme == SchemeName.GRID) {
							cloud.getTransitionEngine().executeSelfTransition(
									BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER, GridSchemeBroker.class,
									new SelfTransition<GridSchemeBroker>() {
								@Override
								public void alterState(GridSchemeBroker mechanism) {
									mechanism.updateGrid(gridx, gridy);
								}
							});
						} else if (activeScheme == SchemeName.EXT_GRID_RECT) {
							cloud.getTransitionEngine().executeSelfTransition(
									BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER, ExtGridSchemeBroker.class,
									new SelfTransition<ExtGridSchemeBroker>() {
								@Override
								public void alterState(ExtGridSchemeBroker mechanism) {
									mechanism.updateGrid(gridx, gridy);
								}
							});
						}
					}
				}, 0, 0);
				for (Button jbtn : selfTransitionButtonsGrid) {
					jbtn.setDisable(false);
				}
				btn.setDisable(true);
				activeGridSize[0] = gridx;
				activeGridSize[1] = gridy;
			}
		});
		selfTransitionButtonsGrid.add(btn);
		return btn;
	}

	/**
	 * Create and return a JButton that triggers a self-transition for the
	 * STE-alpha
	 * 
	 * @param targetClass
	 * @param label
	 * @return
	 */
	private Button getSelfTransitionButtonSTE(double alpha) {
		Button btn = new Button("" + alpha);
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				System.out.println("Self-Transition to STE-" + alpha);
				Event.scheduleWithDelay(Time.SECOND, new de.tudarmstadt.maki.simonstrator.api.EventHandler() {

					@Override
					public void eventOccurred(Object content, int type) {
						if (activeScheme == SchemeName.STE) {
							tEngineBroker.alterLocalState(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
									SpaceTimeEnvelopeSchemeBroker.class, "Alpha", alpha);
						}
					}
				}, 0, 0);
				for (

				Button jbtn : selfTransitionButtonsSTE) {
					jbtn.setDisable(false);
				}
				btn.setDisable(true);
				alpha_ste = alpha;
			}
		});
		selfTransitionButtonsSTE.add(btn);
		return btn;
	}

	/**
	 * Create and return a JButton that triggers a given transition
	 * 
	 * @param targetClass
	 * @param label
	 * @return
	 */
	private Button getTransitionButton(SchemeName scheme) {
		Button btn = new Button(scheme.name());
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Transition to " + scheme.name());
				TransitionExecutionPlan plan = tCoord.createExecutionPlan();
				plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME,
						scheme.clientClass));
				Event.scheduleWithDelay(Time.SECOND, new de.tudarmstadt.maki.simonstrator.api.EventHandler() {
					@Override
					public void eventOccurred(Object content, int type) {
						TransitionExecutionPlan plan = (TransitionExecutionPlan) content;
						tCoord.executeTransitionPlan(getContactsForTransition(), plan);
						tEngineBroker.executeAtomicTransition(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
								scheme.brokerClass);
					}
				}, plan, 0);

				for (Button jbtn : transitionButtons) {
					jbtn.setDisable(false);
				}
				activeScheme = scheme;
				btn.setDisable(true);
				switchedScheme();
			}
		});
		transitionButtons.add(btn);
		if (scheme == activeScheme) {
			btn.setDisable(true);
		}
		btn.setMaxWidth(Double.MAX_VALUE);
		return btn;
	}

	/**
	 * Create and return a JButton that triggers a given transition
	 * 
	 * @param targetClass
	 * @param label
	 * @return
	 */
	private Button getTransitionButtonDissemination(LocalDistributionType type) {
		Button btn = new Button(type.name());
		btn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				System.out.println("Transition to " + type.name());
				Event.scheduleWithDelay(Time.SECOND, new de.tudarmstadt.maki.simonstrator.api.EventHandler() {
					@Override
					public void eventOccurred(Object content, int eventType) {
						for (TransitionEngine clientEngine : clientEngines) {
							clientEngine.executeAtomicTransition(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION,
									type.clazz);
						}
					}
				}, type, 0);

				for (Button jbtn : transitionButtonsDissemination) {
					jbtn.setDisable(false);
				}
				activeDissemination = type;
				btn.setDisable(true);
				switchedScheme();
			}
		});
		transitionButtonsDissemination.add(btn);
		if (type == activeDissemination) {
			btn.setDisable(true);
		}
		btn.setMaxWidth(Double.MAX_VALUE);
		return btn;
	}

	private Map<Integer, DemoClientController> assignedClients = new LinkedHashMap<>();

	/**
	 * Represents a remote-controlled client that is sent and manipulated via
	 * the REST access.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@XmlRootElement
	public static class DemoClientController {

		private transient Host host;

		private transient INodeID hostId;

		private transient BypassClientComponent bypass;

		private transient PubSubWorkloadApp app;

		private transient ARNodeState arState;

		private transient List<AttractionPoint> aps;

		private transient DefaultTopologyComponent tcomp;

		public int id;

		public float recall;

		public float precision;

		public float notificationsPerSecond;

		public int currentRoi;

		public int neighbors;

		public String currentScheme;

		public List<String> attractionPoints;

		public String currentAttractionPoint;

		public String colorHex;

		public DemoClientController() {
			// for serialization
		}

		public DemoClientController(int id, Host host, String colorHex) {
			this.id = id;
			this.host = host;
			this.hostId = host.getId();
			this.colorHex = colorHex;
			try {
				this.bypass = host.getComponent(BypassClientComponent.class);
				this.app = host.getComponent(PubSubWorkloadApp.class);
				this.arState = ((ARGameWorkload) app.getWorkload()).getStateFor(hostId);
			} catch (ComponentNotAvailableException e) {
				e.printStackTrace();
			}
			this.attractionPoints = new Vector<>();
			try {
				tcomp = host.getComponent(DefaultTopologyComponent.class);
				ModularMovementModel mModel = (ModularMovementModel) tcomp.getMovementModel();
				this.aps = new LinkedList<>(tcomp.getAllAttractionPoints());
				for (AttractionPoint ap : aps) {
					attractionPoints.add(ap.getName());
				}
			} catch (ComponentNotAvailableException e) {
				e.printStackTrace();
			}
		}

		public void update() {
			SubscriptionResultSnapshot snapshot = PubSubResultPersistingLiveStats.getSnapshot(host.getId());
			// For now: some random statistics for GUI testing
			if (snapshot == null) {
				return;
			}
			recall = roundTwoDecimals(snapshot.getRecall());
			precision = roundTwoDecimals(snapshot.getPrecision());
			notificationsPerSecond = roundTwoDecimals(snapshot.getNotificationsPerSecond());
			neighbors = snapshot.getSubscriptionCoverage();
			currentScheme = bypass.getSubscriptionScheme().getName().toString();
			currentAttractionPoint = tcomp.getCurrentTargetAttractionPoint().getName();
			currentRoi = (int) arState.getAreaOfInterestRadius();
		}

		private float roundTwoDecimals(double d) {
			/*
			 * http://stackoverflow.com/questions/8911356/whats-the-best-
			 * practice-to-round-a-float-to-2-decimals
			 */
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			return Float.valueOf(twoDForm.format(d));
		}

		public void setTargetWaypoint(String waypoint) {
			for (AttractionPoint ap : aps) {
				if (ap.getName().equals(waypoint)) {
					tcomp.setTargetAttractionPoint(ap);
				}
			}
		}

		public void updateRadiusOfInterest(int newValue) {
			ARGameWorkload wl = (ARGameWorkload) app.getWorkload();
			ARNodeState state = wl.getStateFor(hostId);
			// unsubscribe
			state.stopWorkload();
			// set RoI-parameter
			state.setRadiusOfInterest(newValue);
			// resubscribe
			Event.scheduleWithDelay(1 * Time.SECOND, new de.tudarmstadt.maki.simonstrator.api.EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					state.startWorkload();
				}
			}, null, 0);
		}
	}


	private static String[] clientColors = { "#779CAB", "#A2E8DD", "#816C61", "#8C1C13", "#F4D35E", "#EE2E31" };

	private static int clientColorIdx = 0;

	public DemoClientController createClientController() {

		System.out.println("Create Client Controller");

		ClientConnection connection = ScenarioRestHandler.bindRandomHost(this);
		if (connection == null) {
			System.err.println("No host.");
			return null;
		}
		Host host = connection.getHost();
		if (host == null) {
			System.err.println("No host.");
			return null;
		}

		String clientColorHex = clientColors[clientColorIdx];
		clientColorIdx++;
		clientColorIdx = clientColorIdx % clientColors.length;

		DemoClientController client = new DemoClientController((int) host.getId().value(), host, clientColorHex);
		assignedClients.put(client.id, client);

		// FIXME move this to onClientConnect
		PubSubAppVisualization.getInstance().getNodeVisualizationFor(client.id).setRestClient(clientColorHex);

		return client;
	}

	@Override
	public void onClientConnect(ClientConnection connection) {
		/*
		 * TODO later, color etc. can be stored in the connection object
		 */
	}

	@Override
	public void onClientDisconnect(ClientConnection connection) {
		// TODO Auto-generated method stub
		PubSubAppVisualization.getInstance().getNodeVisualizationFor(connection.getId()).unsetRestClient();
	}

	public DemoClientController getClientController(int id) {
		return assignedClients.get(id);
	}

	public void destroyClientController(DemoClientController client) {
		System.err.println("Destroy Client Controller " + client);
		assignedClients.remove(client.id);

		ScenarioRestHandler.freeHost(client.hostId);
	}

	/**
	 * Gather all clients at a specific attraction point
	 * 
	 * @param waypoint
	 */
	public void gatherClientsAt(String waypoint) {
		List<AttractionPoint> aps = null;
		for (BypassClientComponent client : churn.getActiveClients()) {
			DefaultTopologyComponent tcomp;
			try {
				tcomp = client.getHost().getComponent(DefaultTopologyComponent.class);
			} catch (ComponentNotAvailableException e) {
				continue;
			}
			if (aps == null) {
				aps = new LinkedList<>(tcomp.getAllAttractionPoints());
			}
			for (AttractionPoint ap : aps) {
				if (ap.getName().equals(waypoint)) {
					tcomp.setTargetAttractionPoint(ap);
				}
			}
		}
	}

	/**
	 * Churn is now controlled via the {@link ScenarioRestHandler}. This is just
	 * a simple label updater for our side panel.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private class DemoChurnLabel implements IPeerStatusListener {

		private int activeNodes = 0;

		private Set<BypassClientComponent> activeClients = new LinkedHashSet<>();

		public DemoChurnLabel() {
			for (Host h : Oracle.getAllHosts()) {
				try {
					BypassClientComponent comp = h.getComponent(BypassClientComponent.class);
					comp.addPeerStatusListener(this);
				} catch (ComponentNotAvailableException e) {
					continue;
				}
			}
		}

		public Set<BypassClientComponent> getActiveClients() {
			return activeClients;
		}

		@Override
		public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
			// React
			if (peerStatus == PeerStatus.PRESENT) {
				activeNodes++;
				activeClients.add((BypassClientComponent) source);
			} else if (peerStatus == PeerStatus.ABSENT) {
				activeNodes--;
				activeClients.remove((BypassClientComponent) source);
			}
			// Update churn label
			onlineNodesLabel.setText("" + activeNodes);
		}

	}

}
