package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.loadgen.config;

import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.SiSTypeNotFoundException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.UnknownComparatorType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.NodeCondition;

/**
 * A loader for target conditionals
 * 
 * @author Marc Schiller
 * @version 1.0, May 30, 2016
 */
public class Source extends NodeCondition {

	/**
	 * Constructor
	 * 
	 * @throws SiSTypeNotFoundException
	 *             if the wanted SiSType does not exist
	 * @throws UnknownComparatorType
	 *             if the comparator does not exist
	 */
	@XMLConfigurableConstructor({ "parameter", "comparator", "value" })
	public Source(String parameter, String comparator, String value)
			throws SiSTypeNotFoundException, UnknownComparatorType {
		super(parameter, comparator, value);
	}
}
