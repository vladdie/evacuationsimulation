package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import java.io.Writer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;
import de.tudarmstadt.maki.simonstrator.api.Time;

/**
 * This one uses a custom database to store per-publication metrics. Makes it a
 * lot easier to connect sever metrics or properties of the respective
 * publication.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class PubSubResultPersistingCustomMeasurement implements PubSubResultPersisting {

	public final static MetricDescription publicationsMetric = new MetricDescription(
			PubSubResultPersistingCustomMeasurement.class, "Publications",
			"Publication-related metrics in a separate table.", "none");

	public final static MetricDescription subscriptionsMetric = new MetricDescription(
			PubSubResultPersistingCustomMeasurement.class, "Subscriptions",
			"Subscription-related metrics in a separate table.", "none");

	private long startDaoAt = 0;

	private long stopDaoAt = Long.MAX_VALUE;

	private long SUBSCRIPTION_PERSIST_INTERVAL = 10 * Time.SECOND;

	public PubSubResultPersistingCustomMeasurement() {
		// register DAO-Entity
		DAO.addEntityClass(PubSubResultPersistingCustomMeasurement.PublicationMeasurement.class);
		PubSubGlobalKnowledge.OBSERVE_PUBLICATIONS = true;
	}

	@Override
	public void start() {
		// don't care
	}

	@Override
	public void stop(Writer out) {
		// don't care
	}

	@Override
	public void persistPublication(PublicationResultCollector collector) {
		if (collector.startedTimestamp < startDaoAt || collector.startedTimestamp > stopDaoAt) {
			// not interested
			return;
		}
		PublicationMeasurement pojo = new PublicationMeasurement(collector);
		MeasurementDAO.storeCustomMeasurement(publicationsMetric, collector.initiatorHostID.value(),
				collector.startedTimestamp, pojo);
	}

	@Override
	public void persistSubscription(SubscriptionResultCollector collector) {
		SubscriptionMeasurement pojo = new SubscriptionMeasurement(collector);
		MeasurementDAO.storeCustomMeasurement(subscriptionsMetric, collector.subscriberNodeId.value(),
					Time.getCurrentTime(), pojo);
	}

	/**
	 * Timestamp at which we start writing to the database (the creation time of
	 * the publication is used as indicator)
	 * 
	 * @param startDaoAt
	 */
	public void setStartDaoAt(long startDaoAt) {
		this.startDaoAt = startDaoAt;
	}

	/**
	 * Timestamp at which we start writing to the database (the creation time of
	 * the publication is used as indicator)
	 * 
	 * @param stopDaoAt
	 */
	public void setStopDaoAt(long stopDaoAt) {
		this.stopDaoAt = stopDaoAt;
	}

	/**
	 * A cool, custom POJO to store per-subscription data (usually, this is
	 * periodically sampled)
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@Entity
	@Table(name = "subscriptions")
	public static class SubscriptionMeasurement extends CustomMeasurement {

		/*
		 * For DB performance reasons, we only store up to 15 characters of the
		 * topic (longer topics are truncated)
		 */
		@Column(nullable = false, name = "[topic]", length = 15)
		final String topic;

		public SubscriptionMeasurement(SubscriptionResultCollector collector) {
			/*
			 * TODO Store all relevant fields
			 */
			this.topic = collector.subscription.getTopic().getValue().substring(0,
					Math.min(15, collector.subscription.getTopic().getValue().length()));
		}

	}

	/**
	 * A cool, custom POJO to store per-publication data
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@Entity
	@Table(name = "publications")
	public static class PublicationMeasurement extends CustomMeasurement {

		/*
		 * For DB performance reasons, we only store up to 15 characters of the
		 * topic (longer topics are truncated)
		 */
		@Column(nullable = false, name = "[topic]", length = 15)
		final String topic;

		@Column(nullable = false, name = "[recall]")
		final float recall;

		@Column(nullable = false, name = "[precision]")
		final float precision;

		@Column(nullable = false, name = "[subscribers]")
		final int subscribers;

		@Column(nullable = false, name = "[delayAvg]")
		final long delayAvg;

		public PublicationMeasurement(PublicationResultCollector collector) {
			/*
			 * Store all relevant fields
			 */
			this.topic = collector.publication.getTopic().getValue().substring(0,
					Math.min(15, collector.publication.getTopic().getValue().length()));
			this.recall = (float) collector.getDeliveryRecall();
			this.precision = (float) collector.getDeliveryPrecision();
			this.delayAvg = collector.getAverageDeliveryDelay();
			this.subscribers = collector.getTotalNumberOfInterestedReceivers();
		}

	}

}
