package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.io.Writer;
import java.util.concurrent.atomic.AtomicBoolean;

import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.api.topology.Topology;
import de.tud.kom.p2psim.impl.simengine.Simulator;
import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tud.kom.p2psim.impl.util.oracle.GlobalOracle;
import de.tudarmstadt.maki.simonstrator.api.Binder;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;

/**
 * Used to periodically measure the positions of the nodes in the network.
 * 
 * Differentiates SINKs and LEAFs to in the end compare density of sinks over time with density of leafs.
 * 
 * TODO Density estimates to in each step bewerten the topology and the current network situation.
 * 
 * 
 * @author Nils Richerzhagen
 *
 */
public class CraterGridAvgNodesPerSinkAnalyzer implements Analyzer, EventHandler {

	private final int _EVENT_ENABLE_DAO_AT = 2001;

	private final int _EVENT_CALCULATE_GRID = 2002;

	private boolean isRunning = false;

	private boolean ENABLE_DAO = false;

	private boolean ENABLED_DAO_AT = false;

	private long interval;

	private final int WORLD_X;

	private final int WORLD_Y;

	private GridComponentInfo[][] gridMatrix;

	private final double xGridSize;

	private final double yGridSize;

	private INodeID cloudID;

	private final AtomicBoolean _firstMethodCall = new AtomicBoolean(true);

	private static CraterGridAvgNodesPerSinkAnalyzer instance;

	private double jainFairness;

	private boolean _metricIsValid = false;

	@XMLConfigurableConstructor({ "minGridSize" })
	public CraterGridAvgNodesPerSinkAnalyzer(int minGridSize) {
		instance = this;

		WORLD_X = (int) Binder.getComponentOrNull(Topology.class).getWorldDimensions().getX();
		WORLD_Y = (int) Binder.getComponentOrNull(Topology.class).getWorldDimensions().getY();

		assert WORLD_X >= minGridSize;
		assert WORLD_Y >= minGridSize;
		gridMatrix = new GridComponentInfo[WORLD_X / minGridSize][WORLD_Y / minGridSize];

		initializeAndResetGridMatrix();

		xGridSize = WORLD_X / (int) (WORLD_X / minGridSize) + 1; // + 1 to overcome the last bit being cut off
		yGridSize = WORLD_Y / (int) (WORLD_Y / minGridSize) + 1;

		DAO.addEntityClass(CraterGridNodesPerSinkMeasurement.class);
	}

	/**
	 * Returns the single instance of the {@link CraterGridAvgNodesPerSinkAnalyzer}
	 *
	 * @return
	 */
	public static CraterGridAvgNodesPerSinkAnalyzer getInstance() {
		assert instance != null;
		return instance;
	}

	public void initializeAndResetGridMatrix() {
		for (int x = 0; x < gridMatrix[0].length; x++) {
			for (int y = 0; y < gridMatrix[1].length; y++) {
				gridMatrix[x][y] = new GridComponentInfo();
			}
		}
	}

	public void setEnableDao(boolean enableDao) {
		this.ENABLE_DAO = enableDao;
	}

	public void setTimeEnableDao(long timeEnableDao) {
		if (ENABLE_DAO) {
			Event.scheduleWithDelay(timeEnableDao, this, null, _EVENT_ENABLE_DAO_AT);
		}
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// assert ENABLE_DAO;
		if (type == _EVENT_ENABLE_DAO_AT) {
			// started = true;
			// Event.scheduleImmediately(this, null, _EVENT_CALCULATE_GRID);
			ENABLED_DAO_AT = true;
		} else if (type == _EVENT_CALCULATE_GRID && isRunning) {
			AvgNodesPerSinkResult result = estimateAvgNodesPerSink();
			if (ENABLED_DAO_AT) {
				writeDataIntoDB(result);
			}
			// writeDataIntoDB(estimateAvgNodesPerSink());
			Event.scheduleWithDelay(interval, this, null, _EVENT_CALCULATE_GRID);
			initializeAndResetGridMatrix();
		} else if (type == _EVENT_CALCULATE_GRID && !isRunning) {
			// don't care, was called in last time when sim was active.
		} else {
			throw new AssertionError("Unknown Event");
		}
	}

	@Override
	public void start() {
		// Not needed! Done by scheduled event.
		isRunning = true;
		Event.scheduleImmediately(this, null, _EVENT_CALCULATE_GRID);
	}

	@Override
	public void stop(Writer out) {
		AvgNodesPerSinkResult result = estimateAvgNodesPerSink();
		if (ENABLED_DAO_AT) {
			writeDataIntoDB(result);
		}
		isRunning = false;
	}

	/**
	 * Calculates based on the current snapshot the average value for the estimate of the grid.
	 * 
	 * Also calculates the fairness of a system based on Jain's Fairness Metric!
	 * 
	 * http://www.researchgate.net/profile/Raj_Jain3/publication/
	 * 220486705_A_Quantitative_Measure_Of_Fairness_And_Discrimination_For_Resource_Allocation_In_Shared_Computer_Systems
	 * /links/09e4150c0274cec7b2000000.pdf
	 * 
	 * Quadrat der Einzelsumme / (Anzahl der Messwerte * Summe der Einzelquadrate)
	 * 
	 * @return
	 */
	private AvgNodesPerSinkResult estimateAvgNodesPerSink() {
		assert isRunning;
		for (SimHost host : GlobalOracle.getHosts()) {
			try {
				CraterNodeComponent comp = (CraterNodeComponent) host.getComponent(CraterNodeComponent.class);

				if (comp.isPresent()) {

					double[] coordinates = host.getTopologyComponent().getRealPosition().asDoubleArray();
					int xcoordinate = (int) (Math.round(coordinates[0]));
					int ycoordinate = (int) (Math.round(coordinates[1]));

					assert xcoordinate <= WORLD_X;
					assert ycoordinate <= WORLD_Y;

					if (comp.isSink()) {
						gridMatrix[(int) (xcoordinate / xGridSize)][(int) (ycoordinate / yGridSize)].addSink();
					} else {
						gridMatrix[(int) (xcoordinate / xGridSize)][(int) (ycoordinate / yGridSize)].addLeaf();
					}
				}
			} catch (ComponentNotAvailableException e) {
				if (_firstMethodCall.getAndSet(false)) {
					try {
						host.getComponent(CraterCentralComponent.class);
						// Central Entity
						cloudID = host.getId();
					} catch (ComponentNotAvailableException e1) {
						// Others don't care
					}
					// others don't care
				}
				// other runs don't care
			}
		}

		int count = 0;
		double sum = 0;
		double squareSum = 0;
		for (GridComponentInfo[] gridComponentInfos : gridMatrix) {
			for (GridComponentInfo gridComponentInfo : gridComponentInfos) {
				double currentValue = gridComponentInfo.getLeafsOverSinks();
				/*
				 * Only ignore the cases where there are no sinks but leaves.
				 */
				if (currentValue >= 0) {
					count++;
					sum += currentValue;
					squareSum += Math.pow(currentValue, 2);
				}
			}
		}

		if (count == 0 && sum == 0) {
			_metricIsValid = false;
			return null;
		}
		assert count != 0;
		// Either no nodes in the network or only sinks. Both cases are 100% fair.
		// FIXME No as no one gets any resource!, thus not fair.
		if (count != 0 && sum == 0) {
			_metricIsValid = true;
			jainFairness = 0;
			return new AvgNodesPerSinkResult(0, 0);
		}
		assert sum != 0;
		double fairness_jain = Math.pow(sum, 2) / (count * squareSum);
		jainFairness = fairness_jain;
		_metricIsValid = true;
		return new AvgNodesPerSinkResult(sum / count, fairness_jain);
	}

	private void writeDataIntoDB(AvgNodesPerSinkResult avgNodesPerSinkResult) {
		assert ENABLED_DAO_AT;

		// System.out.println("Having in avg. " + avgNodesPerSinkResult.getAverage() + " nodes per sink, Fairness (Jain)
		// "
		// + avgNodesPerSinkResult.getJainFairness());
		Monitor.log(CraterGridAvgNodesPerSinkAnalyzer.class, Level.DEBUG, "Having in avg. " + avgNodesPerSinkResult.getAverage()
				+ " nodes per sink, Fairness (Jain) " + avgNodesPerSinkResult.getJainFairness());
		MeasurementDAO.storeCustomMeasurement(CraterGridNodesPerSinkMeasurement.CRATER_GRID_AVG_NODES_PER_SINK, cloudID.value(),
				Simulator.getCurrentTime(), new CraterGridNodesPerSinkMeasurement(cloudID.value(),
						avgNodesPerSinkResult.getAverage(), avgNodesPerSinkResult.getJainFairness()));

	}

	public double getJainFairness() {
		return jainFairness;
	}

	/**
	 * Called by {@link } Metric to check if current Jain Fairness is valid.
	 * 
	 * @return
	 */
	public boolean isValid() {
		if (_metricIsValid) {
			if (jainFairness >= 0 && jainFairness <= 1) {
				return _metricIsValid;
			} else {
				return !_metricIsValid;
			}
		}
		return _metricIsValid;
	}

	private class GridComponentInfo {
		private int numberOfSinks;

		private int numberOfLeafs;

		public GridComponentInfo() {
			numberOfSinks = 0;
			numberOfLeafs = 0;
		}

		public void addSink() {
			numberOfSinks++;
		}

		public void addLeaf() {
			numberOfLeafs++;
		}

		/**
		 * Returns the number of leaves per sink in this Grid.
		 * 
		 * 0 is returned when there are no leaves but sinks.
		 * 
		 * 0 is returned when there are no leaves and no sinks.
		 * 
		 * -1 is returned when there are no sinks but leaves. --> Invalid measurement
		 * 
		 * 
		 * @return
		 */
		public double getLeafsOverSinks() {
			if (numberOfLeafs == 0 && numberOfSinks == 0) {
				return 0;
			} else if (numberOfLeafs == 0) {
				return 0;
			} else if (numberOfSinks == 0) {
				return -1;
			}
			return numberOfLeafs / numberOfSinks;
		}
	}

	/**
	 * Result from compuation to return by method.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	private class AvgNodesPerSinkResult {

		double average;
		double jainFairness;

		public AvgNodesPerSinkResult(double average, double jainFairness) {
			this.average = average;
			this.jainFairness = jainFairness;
		}

		public double getAverage() {
			return average;
		}

		public double getJainFairness() {
			return jainFairness;
		}
	}

}
