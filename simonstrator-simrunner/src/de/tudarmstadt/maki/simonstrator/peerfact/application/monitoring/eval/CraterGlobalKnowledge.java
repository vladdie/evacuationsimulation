package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.io.Writer;

import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.Component;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.CraterGlobalKnowledgeAnalyzer;

/**
 * Global Knowledge Component for Crater.
 * 
 * @author Nils Richerzhagen
 *
 */
@Deprecated
public class CraterGlobalKnowledge implements Component, EventHandler, CraterGlobalKnowledgeAnalyzer {

	public static final int EVENT_PERIODIC_UPDATE = 15;

	public static final long INTERVAL_PERIODIC_UPDATE = 3 * Time.MINUTE;

	public static final CraterGlobalKnowledge instance = new CraterGlobalKnowledge();

	protected static final MetricDescription NUMBER_SINKS = new MetricDescription(CraterGlobalKnowledge.class, "NumberOfSinks",
			"Global Knowledge: Current number of online sinks in the network", "none");
	
	protected static final MetricDescription NUMBER_LEAFS = new MetricDescription(CraterGlobalKnowledge.class, "NumberOfLeafs",
			"Global Knowledge: Current number of online leafs in the network", "none");

	protected static final MetricDescription NUMBER_NODES = new MetricDescription(CraterGlobalKnowledge.class, "NumberOfNodes",
			"Global Knowledge: Current number of online nodes in the network", "none");




	@Override
	public void eventOccurred(Object content, int type) {
		switch (type) {
		case EVENT_PERIODIC_UPDATE:
			handlePeriodicUpdate();
			break;

		default:
			throw new AssertionError();
		}
	}
	
	public void handlePeriodicUpdate() {


	}
	
	/**
	 * Update the metrics that are used
	 */
	public void updateAverageMetrics() {

	}

	@Override
	public void start() {
		// not needed
	}

	@Override
	public void stop(Writer out) {
		// not needed
	}
}
