package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;

@Entity
@Table(name = "per_node_role_fairness_measurements")
public class PerNodeRoleFairnessMeasurement extends CustomMeasurement {

	public static final MetricDescription PER_NODE_ROLE_FAIRNESS_MEASUREMENT_METRIC = new MetricDescription(
			PerNodeRoleTimesAnalyzerImpl.class, "PerNodeRoleFairnessMeasurement",
			"Provides the Fairness of the nodes have been in their respective roles", "us");

	@SuppressWarnings("unused")
	private double sinkTimeRatioFairness;

	@SuppressWarnings("unused")
	private double leafTimeRatioFairness;

	@SuppressWarnings("unused")
	private double sinkActiveRatioFairness;

	@SuppressWarnings("unused")
	private double leafActiveRatioFairness;

	public PerNodeRoleFairnessMeasurement(double sinkTimeRatioFairness, double leafTimeRatioFairness,
			double sinkActiveRatioFairness, double leafActiveRatioFairness) {
		super();
		this.sinkTimeRatioFairness = sinkTimeRatioFairness;
		this.leafTimeRatioFairness = leafTimeRatioFairness;
		this.sinkActiveRatioFairness = sinkActiveRatioFairness;
		this.leafActiveRatioFairness = leafActiveRatioFairness;
	}
}
