package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import java.io.Writer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

import de.tud.kom.p2psim.api.analyzer.ConnectivityAnalyzer;
import de.tud.kom.p2psim.api.analyzer.LinklayerAnalyzer;
import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.api.linklayer.LinkLayerMessage;
import de.tud.kom.p2psim.api.network.SimNetInterface;
import de.tud.kom.p2psim.impl.simengine.Simulator;
import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tud.kom.p2psim.impl.util.oracle.GlobalOracle;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.NoSinkMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.SinkAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.configuration.CloudConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.CloudletUploadContainer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.SinkUploadContainer;
import de.tudarmstadt.maki.simonstrator.service.latency.PeriodicLatencySensorService.LatencyPingMsg;

/**
 * Analyzer to obtain the number of messages send on each node and the size of the respective messages.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 18.08.2014, refactored 05.12.2015
 * 
 */
public class CraterLinkAnalyzer implements LinklayerAnalyzer, ConnectivityAnalyzer, EventHandler {

	private boolean isRunning;

	private final int _EVENT_ENABLE_DAO_AT = 2001;

	private boolean ENABLE_DAO = false;

	private long dataID;

	private long interval;

	private HashMap<Long, Long> hostID2DataID;

	private HashMap<Long, LinkInfo> dataID2Data;

	private final int WRITE_DOWN_EVENT_ID = 71;

	// The following variables are to check the correct working
	private long checkHosts;

	private long checkData;

	private int sentmsgs;

	private int recmsgs;

	private long sentData;

	private long recData;

	public CraterLinkAnalyzer() {
		isRunning = false;
		this.checkHosts = 0;
		this.checkData = 0;
		sentmsgs = 0;
		recmsgs = 0;
		sentData = 0;
		recData = 0;

		hostID2DataID = new LinkedHashMap<Long, Long>();
		dataID2Data = new LinkedHashMap<Long, CraterLinkAnalyzer.LinkInfo>();

		DAO.addEntityClass(CraterLinkInfoMeasurement.class);
	}

	public void setEnableDao(boolean enableDao) {
		this.ENABLE_DAO = enableDao;
	}

	public void setTimeEnableDao(long timeEnableDao) {
		if (ENABLE_DAO) {
			Event.scheduleWithDelay(timeEnableDao, this, null, _EVENT_ENABLE_DAO_AT);
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		assert ENABLE_DAO;

		if (type == _EVENT_ENABLE_DAO_AT) {
			isRunning = true;
			startMeasurement();
			Event.scheduleWithDelay(interval, this, null, WRITE_DOWN_EVENT_ID);
		}
		// if (!isRunning)
		// return;

		else if (type == WRITE_DOWN_EVENT_ID) {
			writeDownDataFromMap();
			refillDataMapForNextInterval();
			Event.scheduleWithDelay(interval, this, null, WRITE_DOWN_EVENT_ID);
		}
	}

	@Override
	public void start() {

		// Not needed, done by start event

		// isRunning = true;
		//
		// startMeasurement();
		// Event.scheduleWithDelay(interval, this, null, WRITE_DOWN_EVENT_ID);

		// log.debug("CraterLinkAnalyzer started");
	}

	@Override
	public void stop(Writer out) {
		writeDownDataFromMap();

		// log.warn(sentmsgs + " link msgs were sent with a size of " + sentData
		// + " and " + recmsgs + " link msgs were rec with a size of "
		// + recData);

		isRunning = false;
	}

	@Override
	public void wentOffline(SimHost host) {
		if (!isRunning) {
			return;
		}

		// Host is already removed.
		if (!hostID2DataID.containsKey(host.getHostId()))
			return;

		// Remove the host from all maps and persist the data
		long currentTime = Simulator.getCurrentTime();
		Long dataID = hostID2DataID.remove(new Long(host.getHostId()));
		LinkInfo info = dataID2Data.remove(dataID);
		createBean(currentTime, host.getId().value(), info);
		checkHosts--;
		checkData--;

	}

	@Override
	public void wentOnline(SimHost host) {
		if (!isRunning) {
			return;
		}

		// Host is already contained.
		if (hostID2DataID.containsKey(host.getHostId()))
			return;

		long currentTime = Simulator.getCurrentTime();
		Long id = new Long(dataID);
		hostID2DataID.put(new Long(host.getHostId()), id);
		dataID2Data.put(id, new LinkInfo(dataID, currentTime));
		dataID++;
		checkHosts++;
		checkData++;

	}

	@Override
	public void linkMsgEvent(LinkLayerMessage msg, SimHost host, Reason reason) {
		if (!isRunning) {
			return;
		}

		// Long hostID = new Long(host.getHostId());
		Long dataID = hostID2DataID.get(host.getHostId());

		Message craterMessage = null;

		LinkInfo info = dataID2Data.get(dataID);
		switch (reason) {
		case SEND:
			sentData += msg.getSize();
			sentmsgs++;

			// if no payload is set, it might be a dummy ctrl-message
			if (msg.getPayload() == null) {
				break;
			}

			// Crater specific calculations
			craterMessage = msg.getPayload().getPayload().getPayload();
			if (craterMessage instanceof CraterCellMessage) {
				info.incrementSentCellularMsg();
				info.increaseUpCellularTraffic(craterMessage.getSize());
				if (craterMessage instanceof SinkUploadContainer) {
					info.incrementSentSinkUploadMsg();
					info.increaseUpSinkTraffic(craterMessage.getSize());
				} else if (craterMessage instanceof CloudletUploadContainer) {
					info.incrementSentCloudletUploadMsg();
					info.increaseUpCloudledTraffic(craterMessage.getSize());
				} else if (craterMessage instanceof CloudConfigurationMessage){
					// TODO Currently nothing
				} else {
					throw new Error("Unknown Upload Msg Type: " + craterMessage.getClass().toString());
				}
			} else if (craterMessage instanceof CraterLocalMessage) {
				info.incrementSentAdHocMsg();
				info.increaseUpAdHocTraffic(((CraterLocalMessage) craterMessage).getSize());
				if (craterMessage instanceof NoSinkMessage) {
					info.incrementSentNoSinkAdvMsg();
					info.increaseUpNoSinkAdvTraffic(((NoSinkMessage) craterMessage).getSize());
				} else if (craterMessage instanceof SinkAdvertisingMessage) {
					info.incrementSentSinkAdvMsg();
					info.increaseUpSinkAdvTraffic(((SinkAdvertisingMessage) craterMessage).getSize());
				} else if (craterMessage instanceof DataMessageContainerLocalImpl) {
					info.incrementSentDataMsg();
					info.increaseUpDataTraffic(((DataMessageContainerLocalImpl) craterMessage).getSize());
				} else {
					throw new RuntimeException("Unknown WiFi Ad Hoc message type" + craterMessage.getClass().toString());
				}
			} else if (craterMessage instanceof LatencyPingMsg) {
				// TODO
				// System.out.println("LatencyPingMsg send in analyzer.");
			} else {
				throw new RuntimeException("Unknown message type" + craterMessage.getClass().toString());
			}
			break;
		case RECEIVE:
			recData += msg.getSize();
			recmsgs++;

			// if no payload is set, it might be a dummy ctrl-message
			if (msg.getPayload() == null) {
				break;
			}

			// Crater specific calculations
			craterMessage = msg.getPayload().getPayload().getPayload();
			if (craterMessage instanceof CraterCellMessage) {
				info.incrementRecCellularMsg();
				info.increaseDownCellularTraffic(craterMessage.getSize());
			} else if (craterMessage instanceof CraterLocalMessage) {
				info.incrementRecAdHocMsg();
				info.increaseDownAdHocTraffic(((CraterLocalMessage) craterMessage).getSize());
				if (craterMessage instanceof NoSinkMessage) {
					info.incrementRecNoSinkAdvMsg();
					info.increaseDownNoSinkAdvTraffic(((NoSinkMessage) craterMessage).getSize());
				} else if (craterMessage instanceof SinkAdvertisingMessage) {
					info.incrementRecSinkAdvMsg();
					info.increaseDownSinkAdvTraffic(((SinkAdvertisingMessage) craterMessage).getSize());
				} else if (craterMessage instanceof DataMessageContainerLocalImpl) {
					info.incrementRecDataMsg();
					info.increaseDownDataTraffic(((DataMessageContainerLocalImpl) craterMessage).getSize());
				} else {
					throw new RuntimeException("Unknown WiFi Ad Hoc message type" + craterMessage.getClass().toString());
				}
			} else if (craterMessage instanceof LatencyPingMsg) {
				// TODO
				// System.out.println("LatencyPingMsg received in analyzer.");
			} else {
				throw new RuntimeException("Unknown message type" + craterMessage.getClass().toString());
			}
			break;
		default:
			return;
		}
		dataID2Data.put(dataID, info);
	}

	public void setInterval(long interval) {
		this.interval = interval;
	}

	/**
	 * At the beginning of the measurement-phase fill the tables with the information about the hosts, which are online.
	 */
	private void startMeasurement() {
		long currentTime = Simulator.getCurrentTime();
		for (SimHost host : GlobalOracle.getHosts()) {

			boolean checkedOnline = false;
			for (SimNetInterface net : host.getNetworkComponent().getSimNetworkInterfaces()) {
				if (net.isOnline() && !checkedOnline) {
					Long id = new Long(dataID);
					hostID2DataID.put(host.getHostId(), id);
					dataID2Data.put(id, new LinkInfo(dataID, currentTime));
					checkedOnline = true;
					dataID++;
					checkHosts++;
					checkData++;
				}
			}
		}
	}

	private void writeDownDataFromMap() {
		long currentTime = Simulator.getCurrentTime();
		Long hostID;
		Long dataID;
		LinkInfo info;
		Iterator<Long> dataIDIter = hostID2DataID.keySet().iterator();
		while (dataIDIter.hasNext()) {
			hostID = dataIDIter.next();
			dataID = hostID2DataID.get(hostID);
			info = dataID2Data.get(dataID);
			createBean(currentTime, hostID.longValue(), info);
		}

		checkHosts -= hostID2DataID.size();
		checkData -= dataID2Data.size();
		if (checkHosts != 0 || checkData != 0) {
			throw new RuntimeException("After writing down the data maps checkHosts still contains " + checkHosts
					+ " entries and checkData still " + checkData + " entries");
		}
		dataID2Data.clear();
	}

	private void refillDataMapForNextInterval() {
		long currentTime = Simulator.getCurrentTime();
		Long dataIDTemp;
		Long hostID;
		for (SimHost host : GlobalOracle.getHosts()) {
			// Check if the host-map already contains an entry, otherwise create
			// one and put it into the map
			boolean checkedOnline = false;
			for (SimNetInterface net : host.getNetworkComponent().getSimNetworkInterfaces()) {
				if (net.isOnline() && !checkedOnline) {
					hostID = new Long(host.getHostId());
					dataIDTemp = hostID2DataID.get(hostID);
					if (dataIDTemp == null) {
						dataIDTemp = new Long(this.dataID);
						hostID2DataID.put(hostID, dataIDTemp);
						this.dataID++;
					}
					// Finally, create an entry for the data map for all hosts.
					dataID2Data.put(dataIDTemp, new LinkInfo(dataIDTemp.longValue(), currentTime));
					checkHosts++;
					checkData++;
					checkedOnline = true;
				}
			}
		}
	}

	private void createBean(long time, long hostID, LinkInfo info) {
		if ((Simulator.getCurrentTime() - info.getStart()) > 0) {
			CraterLinkInfoMeasurement measurement = new CraterLinkInfoMeasurement(info.getDataID(),
					(Simulator.getCurrentTime() - info.getStart()), info.getUpAdHocTraffic(), info.getUpCellularTraffic(),
					info.getUpSinkTraffic(), info.getUpCloudletTraffic(), info.getUpNoSinkAdvTraffic(),
					info.getUpSinkAdvTraffic(), info.getUpDataTraffic(), info.getDownAdHocTraffic(),
					info.getDownCellularTraffic(), info.getDownNoSinkAdvTraffic(), info.getDownSinkAdvTraffic(),
					info.getDownDataTraffic(), info.getCountRec_AdHocMsg(), info.getCountRec_CellularMsg(),
					info.getCountRec_NoSinkAdvMsg(), info.getCountRec_SinkAdvMsg(), info.getCountRec_DataMsg(),
					info.getCountSent_AdHocMsg(), info.getCountSent_CellularMsg(), info.getCountSent_SinkUploadMsg(),
					info.getCountSent_CloudletUploadMsg(), info.getCountSent_NoSinkAdvMsg(), info.getCountSent_SinkAdvMsg(),
					info.getCountSent_DataMsg());
			MeasurementDAO.storeCustomMeasurement(CraterLinkInfoMeasurement.CRATER_HOST_LINK_INFO_METRIC, hostID, time,
					measurement);

			// System.out.println("Host " + hostID + " sent in network: " +
			// info.getCountSent_AdHocMsg() + " rec in network: "
			// + info.getCountRec_AdHocMsg() + " sent upload: " +
			// info.getCountSent_CellularMsg());
		}
	}

	private class LinkInfo {

		private long dataID;

		private long start;

		// the variables to measure the size of the sent messages
		private long upAdHocTraffic;

		private long upCellularTraffic;

		private long upSinkTraffic;

		private long upCloudletTraffic;

		private long upNoSinkAdvTraffic;

		private long upSinkAdvTraffic;

		private long upDataTraffic;

		// the variables to measure the size of the received messages
		private long downAdHocTraffic;

		private long downCellularTraffic;

		private long downNoSinkAdvTraffic;

		private long downSinkAdvTraffic;

		private long downDataTraffic;

		// the variables to count the received messages
		private int countRec_AdHocMsg;

		private int countRec_CellularMsg;

		private int countRec_NoSinkAdvMsg;

		private int countRec_SinkAdvMsg;

		private int countRec_DataMsg;

		// the variables to count the sent messages
		private int countSent_AdHocMsg;

		private int countSent_CellularMsg;

		private int countSent_SinkUploadMsg;

		private int countSent_CloudletUploadMsg;

		private int countSent_NoSinkAdvMsg;

		private int countSent_SinkAdvMsg;

		private int countSent_DataMsg;

		LinkInfo(long dataID, long start) {
			this.dataID = dataID;
			this.start = start;
		}

		// methods to increment the number of received messages
		public void incrementRecAdHocMsg() {
			countRec_AdHocMsg++;
		}

		public void incrementRecCellularMsg() {
			countRec_CellularMsg++;
		}

		public void incrementRecNoSinkAdvMsg() {
			countRec_NoSinkAdvMsg++;
		}

		public void incrementRecSinkAdvMsg() {
			countRec_SinkAdvMsg++;
		}

		public void incrementRecDataMsg() {
			countRec_DataMsg++;
		}

		// methods to increment the number of sent messages
		public void incrementSentAdHocMsg() {
			countSent_AdHocMsg++;
		}

		public void incrementSentCellularMsg() {
			countSent_CellularMsg++;
		}

		public void incrementSentSinkUploadMsg() {
			countSent_SinkUploadMsg++;
		}

		public void incrementSentCloudletUploadMsg() {
			countSent_CloudletUploadMsg++;
		}

		public void incrementSentNoSinkAdvMsg() {
			countSent_NoSinkAdvMsg++;
		}

		public void incrementSentSinkAdvMsg() {
			countSent_SinkAdvMsg++;
		}

		public void incrementSentDataMsg() {
			countSent_DataMsg++;
		}

		// methods to increment the size of sent messages
		public void increaseUpAdHocTraffic(long size) {
			upAdHocTraffic += size;
		}

		public void increaseUpCellularTraffic(long size) {
			upCellularTraffic += size;
		}

		public void increaseUpSinkTraffic(long size) {
			upSinkTraffic += size;
		}

		public void increaseUpCloudledTraffic(long size) {
			upCloudletTraffic += size;
		}

		public void increaseUpNoSinkAdvTraffic(long size) {
			upNoSinkAdvTraffic += size;
		}

		public void increaseUpSinkAdvTraffic(long size) {
			upSinkAdvTraffic += size;
		}

		public void increaseUpDataTraffic(long size) {
			upDataTraffic += size;
		}

		// methods to increment the size of sent messages
		public void increaseDownAdHocTraffic(long size) {
			downAdHocTraffic += size;
		}

		public void increaseDownCellularTraffic(long size) {
			downCellularTraffic += size;
		}

		public void increaseDownNoSinkAdvTraffic(long size) {
			downNoSinkAdvTraffic += size;
		}

		public void increaseDownSinkAdvTraffic(long size) {
			downSinkAdvTraffic += size;
		}

		public void increaseDownDataTraffic(long size) {
			downDataTraffic += size;
		}

		public long getDataID() {
			return dataID;
		}

		public long getStart() {
			return start;
		}

		public long getUpAdHocTraffic() {
			return upAdHocTraffic;
		}

		public long getUpCellularTraffic() {
			return upCellularTraffic;
		}

		public long getUpSinkTraffic() {
			return upSinkTraffic;
		}

		public long getUpCloudletTraffic() {
			return upCloudletTraffic;
		}

		public long getUpNoSinkAdvTraffic() {
			return upNoSinkAdvTraffic;
		}

		public long getUpSinkAdvTraffic() {
			return upSinkAdvTraffic;
		}

		public long getUpDataTraffic() {
			return upDataTraffic;
		}

		public long getDownAdHocTraffic() {
			return downAdHocTraffic;
		}

		public long getDownCellularTraffic() {
			return downCellularTraffic;
		}

		public long getDownNoSinkAdvTraffic() {
			return downNoSinkAdvTraffic;
		}

		public long getDownSinkAdvTraffic() {
			return downSinkAdvTraffic;
		}

		public long getDownDataTraffic() {
			return downDataTraffic;
		}

		public int getCountRec_AdHocMsg() {
			return countRec_AdHocMsg;
		}

		public int getCountRec_CellularMsg() {
			return countRec_CellularMsg;
		}

		public int getCountSent_SinkUploadMsg() {
			return countSent_SinkUploadMsg;
		}

		public int getCountSent_CloudletUploadMsg() {
			return countSent_CloudletUploadMsg;
		}

		public int getCountRec_NoSinkAdvMsg() {
			return countRec_NoSinkAdvMsg;
		}

		public int getCountRec_SinkAdvMsg() {
			return countRec_SinkAdvMsg;
		}

		public int getCountRec_DataMsg() {
			return countRec_DataMsg;
		}

		public int getCountSent_AdHocMsg() {
			return countSent_AdHocMsg;
		}

		public int getCountSent_CellularMsg() {
			return countSent_CellularMsg;
		}

		public int getCountSent_NoSinkAdvMsg() {
			return countSent_NoSinkAdvMsg;
		}

		public int getCountSent_SinkAdvMsg() {
			return countSent_SinkAdvMsg;
		}

		public int getCountSent_DataMsg() {
			return countSent_DataMsg;
		}

	}
}
