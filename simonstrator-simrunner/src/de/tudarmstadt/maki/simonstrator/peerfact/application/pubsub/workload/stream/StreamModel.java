package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import cern.colt.Arrays;
import de.tudarmstadt.maki.simonstrator.api.Rate;
import de.tudarmstadt.maki.simonstrator.api.Time;

/**
 * Encapsulates a Stream (as seen on the director). As part of a source change,
 * the {@link ChunkFactory} of this model can change.
 * 
 * FIXME: current limitation: this supports only one receiver.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class StreamModel {

	private final LinkedHashMap<Integer, Chunk> chunks = new LinkedHashMap<>();

	private int mostRecentChunk = -1;

	private ChunkFactory chunkFactory;

	/**
	 * Replace the chunk factory (e.g., after a source switch)
	 * 
	 * @param chunkFactory
	 */
	public void setChunkFactory(ChunkFactory chunkFactory) {
		this.chunkFactory = chunkFactory;
	}

	/**
	 * Called by the source to retrieve the next chunk that is to be played.
	 * 
	 * @param chunkNo
	 * @return
	 */
	public Chunk createChunk(long sourceId) {
		assert chunkFactory != null : "Chunk Factory must be set!";
		mostRecentChunk++;
		assert !chunks.containsKey(mostRecentChunk);
		Chunk chunk = chunkFactory.createChunk(mostRecentChunk);
		chunks.put(mostRecentChunk, chunk);
		chunk.markAsSent(sourceId);
		return chunk;
	}

	public int getMostRecentChunk() {
		return mostRecentChunk;
	}

	/**
	 * First chunk that is available for playback at the receiver (only one
	 * receiver is supported). Only considers chunks that have not yet been
	 * played (otherwise, pause and play might confuse the model), and only the
	 * first occurrence (counted from the newest chunk) of such an available
	 * chunk.
	 * 
	 * @return first playable chunk or null
	 */
	public Chunk getOldestPlayableChunk() {
		int i = mostRecentChunk;
		Chunk chunk = null;
		Chunk prevChunk = null;
		while (chunks.containsKey(i)) {
			chunk = chunks.get(i);
			if (chunk.isPlayable()) {
				prevChunk = chunk;
			} else {
				if (prevChunk != null) {
					break;
				}
			}
			if (chunk.hasBeenPlayed() || chunk.hasBeenSkipped()) {
				break;
			}
			i--;
		}
		return prevChunk;
	}

	/**
	 * Called by the receiver (currently, only one viewer is supported)
	 * 
	 * @param chunkId
	 * @param blockId
	 */
	public void onReceivedBlock(int chunkId, short blockId) {
		chunks.get(chunkId).markBlockAsReceived(blockId);
	}

	/**
	 * True, if the given chunk ID is playable at the receiver
	 * 
	 * @param chunkId
	 * @return
	 */
	public boolean isPlayable(int chunkId) {
		if (!chunks.containsKey(chunkId)) {
			return false;
		}
		return chunks.get(chunkId).isPlayable();
	}

	/**
	 * Length of currently playable video in our buffer,
	 * 
	 * @return
	 */
	public long getPlayableVideoBufferLength(int fromChunkId) {
		int currentChunkId = fromChunkId;
		long availableBuffer = 0;
		while (chunks.containsKey(currentChunkId)) {
			Chunk c = chunks.get(currentChunkId);
			if (!c.isPlayable()) {
				break;
			}
			availableBuffer += c.chunkDuration;
			currentChunkId++;
		}
		return availableBuffer;
	}

	/**
	 * Mark the chunk as being played (this is also allowed for non-playable
	 * chunks) in that case
	 * 
	 * @param chunk
	 * @param durationStallBefore
	 *            time we needed to stall before finally playing this chunk
	 * @return time until next chunk
	 */
	public long markAsPlayed(int chunkId, long durationStallBefore) {
		Chunk chunk = chunks.get(chunkId);
		chunk.markAsPlayed(durationStallBefore);
		return chunk.chunkDuration;
	}

	/**
	 * Statistics (mini-eval)
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public final class Statistics {

		public final long streamDuration;
		public final long stallDuration;
		public final long playDuration;

		public final long dataTotal;
		public final long startupDelay;
		public final int stallsNumber;
		public final double playbackPerformance;
		public final double playbackQualityAvg;
		public final long videoBitrate;
		public final long videoThroughput;

		public Statistics(long streamDuration, long stallDuration, long playDuration, long dataTotal, long startupDelay,
				int stallsNumber, double playbackPerformance, double playbackQualityAvg, long videoBitrate,
				long videoThroughput) {
			this.streamDuration = streamDuration;
			this.stallDuration = stallDuration;
			this.playDuration = playDuration;
			this.dataTotal = dataTotal;
			this.startupDelay = startupDelay;
			this.stallsNumber = stallsNumber;
			this.playbackPerformance = playbackPerformance;
			this.playbackQualityAvg = playbackQualityAvg;
			this.videoBitrate = videoBitrate;
			this.videoThroughput = videoThroughput;
		}

		@Override
		public String toString() {
			return String.format(
					"%n%n== Totals ==%nDuration:\t%s%nStalling:\t%s%nPlaying:\t%s%nTotal Data:\t%.2f kByte%nStartup Delay:\t%.2f ms%nno. Stalls:\t%d%nPlayback-Perf.:\t%.2f%nVideo Rate:\t%.2f kbit/s%nThroughput:\t%.2f kbit/s%nAvg. Quality:\t%.2f",
					Time.getFormattedTime(streamDuration), Time.getFormattedTime(stallDuration),
					Time.getFormattedTime(playDuration), (dataTotal / 1000d), (float) startupDelay / Time.MILLISECOND,
					stallsNumber, playbackPerformance, (float) videoBitrate / Rate.kbit_s,
					(float) videoThroughput / Rate.kbit_s, playbackQualityAvg);
		}

		public String toStringKeys() {
			return "Duration\tStalling\tPlaying\tTotalData\tStartupDelay\tnumStalls\tPlaybackPerf\tVideoRate\tThroughput\tAvgQuality\tOverallPerf";
		}

		public String toStringValues() {
			return String.format("%d\t%d\t%d\t%d\t%d\t%d\t%.2f\t%d\t%d\t%.2f\t%.2f", streamDuration / Time.MILLISECOND,
					stallDuration / Time.MILLISECOND, playDuration / Time.MILLISECOND, dataTotal / 1000,
					startupDelay / Time.MILLISECOND, stallsNumber, (float) playbackPerformance, videoBitrate,
					videoThroughput, (float) playbackQualityAvg, (float) (playbackPerformance * playbackQualityAvg));
		}

		public void appendToFile() {
			try {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Calendar cal = Calendar.getInstance();
				String path = "wowmom-eval-" + dateFormat.format(cal.getTime()) + ".txt";
				StringBuilder str = new StringBuilder();
				if (!Files.exists(Paths.get(path))) {
					// str.append("All times in [ms], all rates in [bit/s], all
					// sizes in kByte!");
					// str.append(String.format("%nConfiguration\t\t\t\t\t\tResults%n"));
					// configs
					str.append(String.format("ExpName\tStrategy\tChunks/s\tBuffer\tBitrates\t"));
					// results
					str.append(toStringKeys());
				}
				// Write configuration
				str.append(String.format("%n%s\t%s\t%d\t%d\t%s\t", StreamDirectorApp.Factory.experimentName,
						StreamDirectorApp.Factory.strategy, StreamSourceApp.Factory.chunksPerSecond,
						StreamConsumerApp.Factory.bufferLength / Time.MILLISECOND,
						Arrays.toString(StreamSourceApp.Factory.bitrates)));
				// Write results
				str.append(toStringValues());
				Files.write(Paths.get(path), str.toString().getBytes(), StandardOpenOption.APPEND,
						StandardOpenOption.CREATE, StandardOpenOption.WRITE);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Statistics up to now.
	 * 
	 * @return
	 */
	public Statistics getStatistics() {
		/*
		 * Print a string-representation of this stream-model. Rows = time
		 * (e.g., chunks)
		 */
		long timestampStarted = -1;
		long timestampStopped = Time.getCurrentTime();
		long startupDelay = -1;
		long totalPlaybackTime = 0;
		long totalSize = 0;
		double totalQuality = 0.0;
		int numberOfStalls = 0;
		int numberOfChunks = 0;
		for (Chunk chunk : chunks.values()) {
			if (!chunk.hasBeenPlayed() && !chunk.hasBeenSkipped()) {
				// Skip "unwatched" chunks
				continue;
			}
			if (timestampStarted == -1) {
				// First chunk: playback started.
				timestampStarted = chunk.timestampPlayed;
			}

			if (startupDelay == -1) {
				// Startup delay == Stall before first chunk
				startupDelay = chunk.durationStallBefore;
			} else {
				if (chunk.durationStallBefore > 0) {
					numberOfStalls++;
				}
			}
			if (chunk.timestampPlayed + chunk.chunkDuration > timestampStopped) {
				/*
				 * last chunk not yet completely played. Might also slighly skew
				 * other results, which is why we do not include the chunk in
				 * the overall calculations.
				 */
				// the transmission rate results...
				totalPlaybackTime += timestampStopped - chunk.timestampPlayed;
			} else {
				totalPlaybackTime += chunk.chunkDuration;
				totalSize += chunk.getReceivedChunkSize();
				totalQuality += chunk.getPlayedQuality();
				numberOfChunks++;
			}
		}

		long overallDuration = timestampStopped - timestampStarted;
		long totalStallingTime = overallDuration - totalPlaybackTime;

		double playbackPerformance = (float) (totalPlaybackTime / (double) (overallDuration));
		long averageVideoRate = (long) (totalSize * 8 / ((double) (totalPlaybackTime) / (double) Time.SECOND));
		long averageTransmissionRate = (long) (totalSize * 8 / ((double) overallDuration / (double) Time.SECOND));
		double averageQuality = (totalQuality / numberOfChunks);

		Statistics stats = new Statistics(overallDuration, totalStallingTime, totalPlaybackTime, totalSize,
				startupDelay, numberOfStalls, playbackPerformance, averageQuality, averageVideoRate,
				averageTransmissionRate);
		return stats;
	}

	public String printMetrics() {
		/*
		 * Print a string-representation of this stream-model. Rows = time
		 * (e.g., chunks)
		 */
		StringBuilder str = new StringBuilder();
		str.append(String.format("ID\tLength\tSize\tRecSize\tPlay\tSkip\tQual\tDelay\tRate\tStall%n"));

		for (Chunk chunk : chunks.values()) {
			if (!chunk.hasBeenPlayed() && !chunk.hasBeenSkipped()) {
				// Skip "unwatched" chunks
				continue;
			}
			float playbackDelay = (float) (chunk.getPlaybackDelay() / Time.MILLISECOND);
			float stallDuration = (float) (chunk.durationStallBefore / Time.MILLISECOND);
			str.append(String.format("%04d\t%d\t%d\t%d\t%b\t%b\t%.2f\t%.2f\t%.2f\t%.2f%n", chunk.id,
					chunk.chunkDuration / Time.MILLISECOND, chunk.getChunkSize(), chunk.getReceivedChunkSize(),
					chunk.hasBeenPlayed(), chunk.hasBeenSkipped(), chunk.getPlayedQuality(), playbackDelay,
					(float) chunk.getTransmissionRate() / Rate.kbit_s, stallDuration));
		}

		str.append(getStatistics().toString());

		str.append(String.format("%n%n== Configuration ==%nStrategy:\t%s%nChunks/s:\t%d",
				StreamDirectorApp.Factory.strategy, StreamSourceApp.Factory.chunksPerSecond));
		int layer = 0;
		for (long bitrate : StreamSourceApp.Factory.bitrates) {
			str.append(String.format("%nLayer %d:\t%.2f kbit/s", layer++, (float) bitrate / Rate.kbit_s));
		}

		// Overall statistics

		return str.toString();
	}

	@Override
	public String toString() {
		return printMetrics();
	}

	/**
	 * Creates chunks
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static interface ChunkFactory {
		public Chunk createChunk(int chunkNo);
	}

	/**
	 * A chunk, potentially consisting of multiple blocks.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class Chunk {

		public final int id;

		public final List<Block> blocks;

		private final int minBlocksRequiredForPlayback;

		public long sourceId;

		public long timestampPlayed = -1;

		public long timestampSent = -1;

		public long timestampPlayableSince = -1;

		public long durationStallBefore = -1;

		public double qualityPlayed = 0.0;

		private boolean skipped = false;

		private boolean played = false;

		private boolean playable = false;

		/**
		 * Playout duration of this chunk
		 */
		public final long chunkDuration;

		/**
		 * 
		 * @param id
		 * @param blocks
		 * @param chunkDuration
		 * @param minBlocksRequiredForPlayback
		 *            only relevant, if there are NO blocks in this chunk set to
		 *            required. This mimics MDC, where we do not actually care
		 *            for the blocks, but only for their count to determine a
		 *            quality.
		 */
		public Chunk(int id, List<Block> blocks, long chunkDuration, int minBlocksRequiredForPlayback) {
			this.id = id;
			this.blocks = blocks;
			this.chunkDuration = chunkDuration;
			this.minBlocksRequiredForPlayback = minBlocksRequiredForPlayback;
		}

		/**
		 * A "quality" of this segment between 0.0 and 1.0
		 * 
		 * @return
		 */
		public double getReceivedQuality() {
			if (!isPlayable()) {
				return 0.0;
			}
			// Simple: linear quality
			double quality = 0.0;
			double numBlocks = blocks.size();
			for (Block block : blocks) {
				if (block.isReceived) {
					quality += 1 / numBlocks;
				}
			}
			return quality;
		}

		/**
		 * Return the quality this chunk was played with.
		 * 
		 * @return
		 */
		public double getPlayedQuality() {
			return qualityPlayed;
		}

		/**
		 * The playback delay
		 * 
		 * @return delay or 0, if invalid
		 */
		public long getPlaybackDelay() {
			if (hasBeenPlayed()) {
				return timestampPlayed - timestampSent;
			} else {
				return 0;
			}
		}

		/**
		 * The effective transmission rate of this chunk, in {@link Rate}.
		 * 
		 * @return
		 */
		public long getTransmissionRate() {
			long overallDuration = 0;
			long overallSizeInBit = 0;
			for (Block block : blocks) {
				if (block.isReceived) {
					overallDuration += block.timestampReceived - timestampSent;
					overallSizeInBit += block.sizeInBytes * 8;
				}
			}
			return overallDuration == 0 ? 0 : overallSizeInBit * Time.SECOND / (overallDuration);
		}

		/**
		 * Size of this chunk in byte
		 * 
		 * @return
		 */
		public int getChunkSize() {
			int size = 0;
			for (Block block : blocks) {
				size += block.sizeInBytes;
			}
			return size;
		}

		/**
		 * Size of the chunk including only the received blocks (in bytes)
		 * 
		 * @return
		 */
		public int getReceivedChunkSize() {
			int size = 0;
			for (Block block : blocks) {
				if (block.isReceived) {
					size += block.sizeInBytes;
				}
			}
			return size;
		}

		/**
		 * True, if all required chunks of the segment are received
		 * 
		 * @return
		 */
		public boolean isPlayable() {
			if (playable) {
				return playable;
			}
			int receivedBlocks = 0;
			for (Block block : blocks) {
				if (block.isRequired && !block.isReceived) {
					return false;
				}
				if (block.isReceived) {
					receivedBlocks++;
				}
			}
			// MDC-style encoding
			playable = receivedBlocks >= minBlocksRequiredForPlayback;
			return playable;
		}

		/**
		 * True, if this chunk has been played by the viewer already.
		 * 
		 * @return
		 */
		public boolean hasBeenPlayed() {
			return played;
		}

		/**
		 * True, if this chunk was already skipped.
		 * 
		 * @return
		 */
		public boolean hasBeenSkipped() {
			return skipped;
		}

		/**
		 * Marks one block within this segment as being received.
		 * 
		 * @param blockId
		 */
		public void markBlockAsReceived(short blockId) {
			if (hasBeenPlayed() || hasBeenSkipped()) {
				// Ignore late arrivals, at least for now.
				return;
			}
			for (Block block : blocks) {
				if (block.blockId == blockId && !block.isReceived) {
					block.markAsReceived();
				}
			}
			if (timestampPlayableSince == -1 && isPlayable()) {
				this.timestampPlayableSince = Time.getCurrentTime();
			}
		}

		/**
		 * Mark this chunk as being played (or skipped) by the receiver.
		 * 
		 * @param durationStallBefore
		 *            time we stalled before actually playing (or skipping) this
		 *            chunk.
		 */
		public void markAsPlayed(long durationStallBefore) {
			this.timestampPlayed = Time.getCurrentTime();
			this.qualityPlayed = getReceivedQuality();
			if (isPlayable()) {
				this.skipped = false;
				this.played = true;
			} else {
				this.skipped = true;
				this.played = false;
			}
			this.durationStallBefore = durationStallBefore;
		}

		/**
		 * Marks the source of this chunk, as soon as it is sent.
		 * 
		 * @param sourceId
		 */
		public void markAsSent(long sourceId) {
			this.sourceId = sourceId;
			this.timestampSent = Time.getCurrentTime();
		}

		@Override
		public String toString() {
			return "Chunk " + id + " " + blocks.toString();
		}

	}

	/**
	 * A Block within a chunk
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class Block {

		/**
		 * ONLY unique within a chunk
		 */
		public final short blockId;

		public final long sizeInBytes;

		public final boolean isRequired;

		public boolean isReceived = false;

		public long timestampReceived = -1;

		public Block(short blockId, long sizeInBytes, boolean isRequired) {
			this.blockId = blockId;
			this.sizeInBytes = sizeInBytes;
			this.isRequired = isRequired;
		}

		public void markAsReceived() {
			this.isReceived = true;
			this.timestampReceived = Time.getCurrentTime();
		}

		@Override
		public String toString() {
			return "Blk" + blockId + " size: " + sizeInBytes + (isReceived ? " [REC] " : "")
					+ (isRequired ? " [REQ] " : "");
		}

	}

	/**
	 * Chunk factory with a constant bitrate stream
	 * 
	 * @param chunksPerSecond
	 * @param bitrate
	 * @param blocksPerChunk
	 * @return
	 */
	public static ChunkFactory getConstantBitrateFactory(final int chunksPerSecond, final long bitrate,
			final int blocksPerChunk) {
		return new ChunkFactory() {
			@Override
			public Chunk createChunk(int chunkNo) {
				long chunkPlayoutDuration = (long) Time.SECOND / chunksPerSecond;
				long chunkSize = bitrate / (8 * chunksPerSecond);
				// Create Blocks (equally sized)
				long blockSize = chunkSize / blocksPerChunk;
				List<Block> blocks = new LinkedList<>();
				for (int i = 0; i < blocksPerChunk; i++) {
					blocks.add(new Block((short) i, blockSize, true));
				}
				return new Chunk(chunkNo, blocks, chunkPlayoutDuration, 0);
			}
		};
	}

	/**
	 * Chunk factory with a constant bitrate stream divided into x quality
	 * layers. We assume that the first layer is always required, optionally all
	 * layers might be required. This is to model layered, SVC-like codecs.
	 * 
	 * @param chunksPerSecond
	 * @param allLayersRequired
	 *            if false, only the first layer is required for playback. If
	 *            true, all layers are required.
	 * @param layerBitrates
	 * @return
	 */
	public static ChunkFactory getLayeredConstantBitrateFactory(final int chunksPerSecond,
			final boolean allLayersRequired, final long... layerBitrates) {
		return new ChunkFactory() {
			@Override
			public Chunk createChunk(int chunkNo) {
				long chunkPlayoutDuration = (long) Time.SECOND / chunksPerSecond;
				List<Block> blocks = new LinkedList<>();
				for (int i = 0; i < layerBitrates.length; i++) {
					long blockSize = layerBitrates[i] / (8 * chunksPerSecond);
					blocks.add(new Block((short) i, blockSize, allLayersRequired ? true : i == 0));
				}
				return new Chunk(chunkNo, blocks, chunkPlayoutDuration, 0);
			}
		};
	}

	/**
	 * Chunk factory with a constant bitrate stream divided into x quality
	 * layers. This resembles MDC, meaning that no specific block is required.
	 * As soon as at least numDescriptionsRequired MDC blocks are available, the
	 * chunk can be played.
	 * 
	 * @param chunksPerSecond
	 * @param bitrate
	 *            total video bitrate, potentially including MDC overhead
	 * @param numDescriptions
	 *            how many descriptions (blocks) per chunk?
	 * @param numDescriptionsRequired
	 *            how many descriptions are required to decode a stream?
	 * @return
	 */
	public static ChunkFactory getMDCConstantBitrateFactory(final int chunksPerSecond, final long bitrate,
			final int numDescriptions, final int numDescriptionsRequired) {
		return new ChunkFactory() {
			@Override
			public Chunk createChunk(int chunkNo) {
				long chunkPlayoutDuration = (long) Time.SECOND / chunksPerSecond;
				long chunkSize = bitrate / (8 * chunksPerSecond);
				// Create Blocks (equally sized)
				long blockSize = chunkSize / numDescriptions;
				List<Block> blocks = new LinkedList<>();
				for (int i = 0; i < numDescriptions; i++) {
					blocks.add(new Block((short) i, blockSize, false));
				}
				return new Chunk(chunkNo, blocks, chunkPlayoutDuration, numDescriptionsRequired);
			}
		};
	}

}
