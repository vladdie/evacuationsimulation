package de.tudarmstadt.maki.simonstrator.peerfact.application.dras.eval;

import de.tud.kom.p2psim.impl.topology.movement.modularosm.mapvisualization.IMapVisualization;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView;
import de.tudarmstadt.maki.simonstrator.peerfact.application.dras.vis.DisasterRegionCentralView;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.disasterRegion.DRASCentralAnalyzer;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutPairedList;


/**
 * Implementation of the {@link DRASCentralAnalyzer}.
 *
 * @author Clemens Krug
 */
public class DRASCentralAnalyzerImpl implements DRASCentralAnalyzer, EventHandler
{
    DisasterRegionCentralView view;
    IMapVisualization mapVisualization = null;
    TimeoutPairedList<UniqueID, Location> data;
    private long updateInterval;
    private boolean newDataArrived = false;

    //TODO: Do the ID's follow any specific rule?
    private final int CENTRAL_UPDATE_EVENT_ID = 2403;

    @Override
    public void start()
    {
        view = new DisasterRegionCentralView(mapVisualization);
        Event.scheduleWithDelay(updateInterval, this, null, CENTRAL_UPDATE_EVENT_ID);
    }


    @Override
    public void onNoUMTSLocationReceived(TimeoutPairedList<UniqueID, Location> locations)
    {
        if(data == null)
        {
            data = locations;
        }
        else data.merge(locations);
        newDataArrived=true;
    }

    @Override
    public void eventOccurred(Object content, int type)
    {
        if((data != null && data.hasChanged()) || newDataArrived)
        {
            view.onDataUpdated(data.getUnmodifiableList());
            newDataArrived=false;
        }
        Event.scheduleWithDelay(updateInterval, this, null, CENTRAL_UPDATE_EVENT_ID);
    }

    /**
     * Used by XML configuration file
     * @param interval
     */
    public void setInterval(long interval)
    {
        updateInterval = interval;
    }

    /**
     * Used by XML configuration file
     * @param mapVisualization
     */
    public void setIMapVisualization(IMapVisualization mapVisualization) {
        this.mapVisualization = mapVisualization;
    }

}
