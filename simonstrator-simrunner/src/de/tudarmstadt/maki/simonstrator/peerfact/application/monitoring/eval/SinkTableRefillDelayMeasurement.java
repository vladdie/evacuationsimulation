package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.eval;

import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;


@Entity
@Table(name = "sink_table_refill_delay_measurements")
public class SinkTableRefillDelayMeasurement extends CustomMeasurement{

	public static final MetricDescription SINK_TABLE_REFILL_DELAY_METRIC = new MetricDescription(
			PerNodeHasNoSinkTimesAnalyzerImpl.class, "SinkTableRefillDelay",
			"Provides the average delay until sink table is refilled per node", "us");
	
	@SuppressWarnings("unused")
	private long nodeID;
	
	@SuppressWarnings("unused")
	private long averageSinkTableRefillDelay;
	
	
	public SinkTableRefillDelayMeasurement(long nodeID, long averageSinkTableRefillDelay) {
		super();
		this.nodeID = nodeID;
		this.averageSinkTableRefillDelay = averageSinkTableRefillDelay;
	}
}
