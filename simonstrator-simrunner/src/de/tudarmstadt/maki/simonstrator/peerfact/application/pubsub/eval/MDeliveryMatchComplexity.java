/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.eval.MDeliveryMatchComplexity.MDeliveryMatchComplexityValue;

/**
 * Complexity of match operations associated with this publication (e.g., match
 * function calls done by brokers)
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Nov 6, 2013
 */
public class MDeliveryMatchComplexity extends AbstractMetric<MDeliveryMatchComplexityValue>
		implements Metric<MDeliveryMatchComplexityValue> {

	public MDeliveryMatchComplexity() {
		super("Exp. smoothed Matching complexity as observed by brokers.",
				MetricUnit.NONE);
	}

	/**
	 * The value
	 * 
	 * @author Bjoern
	 * @version 1.0, Sep 22, 2013
	 */
	protected class MDeliveryMatchComplexityValue
			implements de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue<Double> {

		@Override
		public Double getValue() {
			return PubSubGlobalKnowledge.getMAverageMatchComplexity();
		}

		@Override
		public boolean isValid() {
			return PubSubGlobalKnowledge.getMAverageMatchComplexity() != -1;
		}

	}

	@Override
	public void initialize(List<Host> hosts) {
		setOverallMetric(new MDeliveryMatchComplexityValue());
	}

}
