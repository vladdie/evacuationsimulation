/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.workload;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

/**
 * This class allows the reconfiguration of an existing workload parameter
 * during the scenario without the need for an actions.dat scenario file.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Oct 27, 2013
 */
public abstract class PubSubWorkloadParameter {

	/**
	 * Returns the current value of the parameter
	 * 
	 * @return
	 */
	abstract public double getCurrentValue();

	/**
	 * A simple constant
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, Oct 27, 2013
	 */
	public static class Constant extends PubSubWorkloadParameter {

		private final double value;

		@XMLConfigurableConstructor({ "value" })
		public Constant(double value) {
			this.value = value;
		}

		@Override
		public double getCurrentValue() {
			return value;
		}

	}

	/**
	 * A parameter that increases/decreases linearly
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 29.10.2013
	 */
	public static class Linear extends PubSubWorkloadParameter {

		private final double startValue;

		private final double endValue;

		private final long transitionTime;

		private long startTime = 0;

		private final long startAt;

		private final boolean increasing;

		@XMLConfigurableConstructor({ "startValue", "endValue",
				"transitionTime", "startAt" })
		public Linear(double startValue, double endValue,
				long transitionTime, long startAt) {
			this.startValue = startValue;
			this.endValue = endValue;
			this.increasing = startValue <= endValue;
			this.transitionTime = transitionTime;
			this.startAt = startAt;
		}

		@Override
		public double getCurrentValue() {
			if (Time.getCurrentTime() < startAt) {
				return this.startValue;
			}
			if (startTime == 0 && Time.getCurrentTime() >= startAt) {
				this.startTime = Time.getCurrentTime();
			}
			long currentDelta = Math.min(Time.getCurrentTime() - startTime,
					transitionTime);
			if (increasing) {
				double currentValue = (endValue - startValue)
						* (currentDelta / ((double) (transitionTime)))
						+ startValue;
				return currentValue;
			} else {
				double currentValue = startValue - (startValue - endValue)
						* (currentDelta / ((double) (transitionTime)));
				return currentValue;
			}
		}

	}

}
