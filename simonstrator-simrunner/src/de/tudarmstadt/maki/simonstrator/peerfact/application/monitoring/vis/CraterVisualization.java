package de.tudarmstadt.maki.simonstrator.peerfact.application.monitoring.vis;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView.VisualizationInjector;
import de.tud.kom.p2psim.impl.topology.views.visualization.ui.VisHelper;
import de.tud.kom.p2psim.impl.topology.views.visualization.ui.VisualizationComponent;
import de.tud.kom.p2psim.impl.topology.views.visualization.world.NodeInfoComponentVis;
import de.tud.kom.p2psim.impl.util.oracle.GlobalOracle;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.NoKnownSinksException;

/**
 * Component for drawing the visualization layer for the Monitoring System
 * Crater.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, Jul 9, 2015
 *
 */
public class CraterVisualization extends JComponent implements VisualizationComponent {

	private static final long serialVersionUID = 1L;

	private static final CraterVisualization instance = new CraterVisualization();

	private final JMenu menu = new JMenu("Crater");

	protected boolean showBestSinks = false;

	private CraterVisualization() {
		setBounds(0, 0, VisualizationInjector.getWorldX(), VisualizationInjector.getWorldY());
		setOpaque(true);
		setVisible(true);

		menu.add(createCheckboxLinks());

		VisualizationInjector.injectComponent(new NodeInfoComponentVis(CraterNodeComponent.class));
	}

	public static CraterVisualization getInstance() {
		return instance;
	}

	/**
	 * This checkbox draws an overlay for adaptive monitoring components
	 * 
	 * @return
	 */
	private JCheckBoxMenuItem createCheckboxLinks() {
		final JCheckBoxMenuItem checkBox = new JCheckBoxMenuItem("BestSinks", showBestSinks);
		checkBox.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				showBestSinks = checkBox.isSelected();
				VisualizationInjector.invalidate();
			}
		});
		return checkBox;
	}

	@Override
	public JComponent getComponent() {
		return this;
	}

	@Override
	public JMenu getCustomMenu() {
		return menu;
	}

	@Override
	public String getDisplayName() {
		return "CRATER";
	}

	@Override
	public boolean isHidden() {
		return false;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		if (showBestSinks) {

			/*
			 * Drawing lines from nodes to their known best sinks.
			 */
			for (SimHost simHost : GlobalOracle.getHosts()) {
				try {
					SimHost currentSink = GlobalOracle.getHostForHostID(
							simHost.getComponent(CraterNodeComponent.class).getBestSinkTableEntry().getKey().value());

					g2.setColor(Color.LIGHT_GRAY);
					VisHelper.drawArrow(g2, simHost.getTopologyComponent().getLastLocation().getLatitude(),
							simHost.getTopologyComponent().getLastLocation().getLongitude(),
							currentSink.getTopologyComponent().getLastLocation().getLatitude(),
							currentSink.getTopologyComponent().getLastLocation().getLongitude(), 2);

				} catch (ComponentNotAvailableException e) {
					// don't care nothing to draw
				} catch (NoKnownSinksException e) {
					// don't care nothing to draw
				}
			}
		}
	}

}
