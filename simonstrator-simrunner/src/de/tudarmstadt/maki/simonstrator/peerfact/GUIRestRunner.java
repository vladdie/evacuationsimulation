package de.tudarmstadt.maki.simonstrator.peerfact;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.ContextResolver;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpContainerProvider;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.media.sse.SseFeature;
import org.glassfish.jersey.moxy.json.MoxyJsonConfig;
import org.glassfish.jersey.moxy.json.MoxyJsonFeature;
import org.glassfish.jersey.server.ResourceConfig;

import de.tudarmstadt.maki.simonstrator.peerfact.application.pubsub.vis.PubSubAppVisSidebar.DemoClientController;

public class GUIRestRunner {

	// Base URI the Grizzly HTTP server will listen on
	public static String BASE_URI;
	public static String protocol;
	public static Optional<String> host;
	public static String path;
	public static Optional<String> port;

	public static void main(String[] args) throws IOException {
		System.setProperty("logfile.name", "simguilog.log");
		System.setProperty("java.net.preferIPv4Stack", "true");

		/*
		 * Option to configure the host IP
		 */
		protocol = "http://";
		host = Optional.ofNullable(System.getenv("HOSTNAME"));
		port = Optional.ofNullable(System.getenv("PORT"));
		path = "simonstrator";
		if (args != null && args.length == 1) {
			// first argument: IP
			host = Optional.of(args[0]);
		}
		BASE_URI = protocol + host.orElse("0.0.0.0") + ":" + port.orElse("8080") + "/" + path + "/";

		final HttpServer server = startServer();
		System.out
				.println(String.format("Jersey app started with WADL available at " + "%sapplication.wadl", BASE_URI));
		new de.tud.kom.p2psim.impl.util.guirunner.GUIRunner();
		System.in.read();
		server.shutdown();
	}

	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this
	 * application.
	 * 
	 * @return Grizzly HTTP server.
	 */
	public static HttpServer startServer() {
		// create a resource config that scans for JAX-RS resources and
		// providers in de.tudarmstadt.maki.simonstrator package
		final ResourceConfig rc = new ResourceConfig().packages("de.tudarmstadt.maki.simonstrator")
				.register(createMoxyJsonResolver());
		rc.register(DemoClientController.class);
		rc.register(SseFeature.class);
		rc.register(MoxyJsonFeature.class);
		rc.register(GrizzlyHttpContainerProvider.class);
		rc.register(GUIRestRunner.CORSResponseFilter.class);

		// new ResourceConfig(ServerSentEventsResource.class, SseFeature.class);

		// create and start a new instance of grizzly http server
		// exposing the Jersey application at BASE_URI
		HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
		StaticHttpHandler staticHttpHandler = new StaticHttpHandler("assets/webroot/");
		server.getServerConfiguration().addHttpHandler(staticHttpHandler, "/");
		return server;
	}

	public static ContextResolver<MoxyJsonConfig> createMoxyJsonResolver() {
		final MoxyJsonConfig moxyJsonConfig = new MoxyJsonConfig();
		Map<String, String> namespacePrefixMapper = new HashMap<String, String>(1);
		namespacePrefixMapper.put("http://www.w3.org/2001/XMLSchema-instance", "xsi");
		moxyJsonConfig.setNamespacePrefixMapper(namespacePrefixMapper).setNamespaceSeparator(':');
		return moxyJsonConfig.resolver();
	}

	public static class CORSResponseFilter implements ContainerResponseFilter {

		@Override
		public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
				throws IOException {
			MultivaluedMap<String, Object> headers = responseContext.getHeaders();
			headers.add("Access-Control-Allow-Origin", "*");
			headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
			headers.add("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Codingpedia");
		}
	}
}
