package de.tudarmstadt.maki.simonstrator.peerfact.analyzer.common;

import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

/**
 * Based on suggestions by Nils and Marc - an analyzer, that simply writes the
 * current location (real and in a grid) of each host at a given interval into
 * the database.
 * 
 * Initially, this data is used to plot heatmaps of node densities on a map, but
 * we could also connect it to other time-sampled metrics to plot spatial
 * relations of metrics values such as recall and precision.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class HostLocationAnalyzer implements Analyzer, EventHandler {

	private final long samplingInterval;

	private final int EVENT_SAMPLE = 1;

	private boolean active = false;

	private Map<LocationSensor, OverlayComponent> locationSensors = null;

	private Class<? extends OverlayComponent> overlayClass = null;

	private final MetricDescription METRIC_LOCATION = new MetricDescription(HostLocationAnalyzer.class, "HostLocation",
			"Periodic sampling of a host's location.", "");

	@XMLConfigurableConstructor({ "samplingInterval" })
	public HostLocationAnalyzer(long samplingInterval) {
		this.samplingInterval = samplingInterval;
		DAO.addEntityClass(HostLocationAnalyzer.HostLocationMeasurement.class);
	}

	@Override
	public void start() {
		active = true;
		Event.scheduleWithDelay(samplingInterval, this, null, EVENT_SAMPLE);
	}

	@Override
	public void stop(Writer out) {
		active = false;
	}

	@Override
	public void eventOccurred(Object content, int type) {
		assert type == EVENT_SAMPLE;
		if (active) {
			if (locationSensors == null) {
				// initialize
				locationSensors = new LinkedHashMap<>();
				for (Host host : Oracle.getAllHosts()) {
					try {
						OverlayComponent overlay = null;
						if (overlayClass != null) {
							/*
							 * check for overlay class if configured, if not ->
							 * exception -> skip
							 */
							overlay = host.getComponent(overlayClass);
						}
						LocationSensor locSense = host.getComponent(LocationSensor.class);
						locationSensors.put(locSense, overlay); // null values
																// are OK
					} catch (ComponentNotAvailableException e) {
						// continue
					}
				}
			}

			long time = Time.getCurrentTime();
			for (LocationSensor locationSensor : locationSensors.keySet()) {
				if (overlayClass != null) {
					OverlayComponent oComp = locationSensors.get(locationSensor);
					assert oComp != null;
					if (!oComp.isPresent()) {
						// Exclude inactive hosts!
						continue;
					}
				}
				// if not continue due to inactive overlay, write
				HostLocationMeasurement measurement = new HostLocationMeasurement(locationSensor.getLastLocation());
				MeasurementDAO.storeCustomMeasurement(METRIC_LOCATION, locationSensor.getHost().getId().value(), time,
						measurement);
			}

			Event.scheduleWithDelay(samplingInterval, this, null, EVENT_SAMPLE);
		}
	}

	/**
	 * Set a class to determine the host's lifecycle, if wanted.
	 * 
	 * @param overlayClass
	 */
	@SuppressWarnings("unchecked")
	public void setOverlayClass(String overlayClass) {
		try {
			this.overlayClass = (Class<? extends OverlayComponent>) Class.forName(overlayClass);
		} catch (ClassNotFoundException e) {
			throw new AssertionError("Class " + overlayClass + " not found.");
		}
	}

	/**
	 * A custom POJO to store host locations in the database. Time (and host id)
	 * is taken care of by the DAO toolchain.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@Entity
	@Table(name = "hostlocations")
	public static class HostLocationMeasurement extends CustomMeasurement {

		@Column(nullable = false, name = "[locationX]")
		final int locationX;

		@Column(nullable = false, name = "[locationY]")
		final int locationY;

		public HostLocationMeasurement(Location location) {
			this.locationX = (int) location.getLongitude();
			this.locationY = (int) location.getLatitude();
		}

	}

}