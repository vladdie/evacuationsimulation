package de.tudarmstadt.maki.simonstrator.peerfact.analyzer.common;

import java.io.Writer;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.dao.metric.MeasurementDAO;
import de.tud.kom.p2psim.impl.util.db.metric.CustomMeasurement;
import de.tud.kom.p2psim.impl.util.db.metric.MetricDescription;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;

/**
 * Using the DAO to write Transition-related data
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class TransitionAnalyzerImpl implements TransitionAnalyzer, IPeerStatusListener {

	private Class<? extends OverlayComponent> overlayClass = null;

	private boolean writeLocations = false;

	private static final MetricDescription METRIC_TRANSITION = new MetricDescription(TransitionAnalyzer.class,
			"Transitions", "Keeping track of all transitions", "none");

	public enum TransitionMeasurementType {
		NODE_JOIN, NODE_LEAVE, EXECUTE, INITIATE
	}

	public TransitionAnalyzerImpl() {
		DAO.addEntityClass(TransitionAnalyzerImpl.TransitionMeasurement.class);
	}

	@Override
	public void start() {
		if (overlayClass != null) {
			for (Host host : Oracle.getAllHosts()) {
				try {
					host.getComponent(overlayClass).addPeerStatusListener(this);
				} catch (ComponentNotAvailableException e) {
					//
				}
			}
		}
	}

	@Override
	public void stop(Writer out) {
		//
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		if (peerStatus == PeerStatus.PRESENT) {
			TransitionMeasurement measurement = new TransitionMeasurement("", "", TransitionMeasurementType.NODE_JOIN,
					null);
			MeasurementDAO.storeCustomMeasurement(METRIC_TRANSITION, source.getHost().getId().value(),
					Time.getCurrentTime(), measurement);
		} else if (peerStatus == PeerStatus.ABSENT) {
			TransitionMeasurement measurement = new TransitionMeasurement("", "", TransitionMeasurementType.NODE_LEAVE,
					null);
			MeasurementDAO.storeCustomMeasurement(METRIC_TRANSITION, source.getHost().getId().value(),
					Time.getCurrentTime(), measurement);
		}
	}

	/**
	 * Set a class to determine the host's lifecycle, if wanted.
	 * 
	 * @param overlayClass
	 */
	@SuppressWarnings("unchecked")
	public void setOverlayClass(String overlayClass) {
		try {
			this.overlayClass = (Class<? extends OverlayComponent>) Class.forName(overlayClass);
		} catch (ClassNotFoundException e) {
			throw new AssertionError("Class " + overlayClass + " not found.");
		}
	}

	/**
	 * Enable writing locations on each transition event (for later spatial
	 * correlation)
	 * 
	 * @param writeLocations
	 */
	public void setWriteExecutionLocations(boolean writeLocations) {
		this.writeLocations = writeLocations;
	}

	@Override
	public void onExecuteAtomicTransition(Host localhost, String proxyName,
			Class<? extends TransitionEnabled> targetClass) {
		Location loc = null;
		if (writeLocations) {
			try {
				loc = localhost.getComponent(LocationSensor.class).getLastLocation();
			} catch (ComponentNotAvailableException e) {
				// ignore
			}
		}
		TransitionMeasurement measurement = new TransitionMeasurement(proxyName, targetClass.getSimpleName(),
				TransitionMeasurementType.EXECUTE, loc);
		MeasurementDAO.storeCustomMeasurement(METRIC_TRANSITION, localhost.getId().value(),
				Time.getCurrentTime(), measurement);
	}

	@Override
	public void onInitiateAtomicTransition(Host localhost, String proxyName,
			Class<? extends TransitionEnabled> targetClass, Collection<INodeID> targetHosts) {
		for (INodeID hostId : targetHosts) {
			TransitionMeasurement measurement = new TransitionMeasurement(proxyName, targetClass.getSimpleName(),
					TransitionMeasurementType.INITIATE, null);
			MeasurementDAO.storeCustomMeasurement(METRIC_TRANSITION, hostId.value(), Time.getCurrentTime(),
					measurement);
		}

	}

	/**
	 * A cool, custom POJO to store transitions. Time (and host id) is taken
	 * care of by the DAO toolchain.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@Entity
	@Table(name = "transitions")
	public static class TransitionMeasurement extends CustomMeasurement {

		@Column(nullable = false, name = "[proxyName]")
		final String proxyName;

		@Column(nullable = false, name = "[targetClass]")
		final String targetClass;

		/**
		 * As defined in {@link TransitionMeasurementType}
		 */
		@Column(nullable = false, name = "[type]")
		final int type;

		@Column(nullable = false, name = "[locationX]")
		final int locationX;

		@Column(nullable = false, name = "[locationY]")
		final int locationY;

		public TransitionMeasurement(String proxyName, String targetClass, TransitionMeasurementType type,
				Location location) {
			this.proxyName = proxyName;
			this.targetClass = targetClass;
			this.type = type.ordinal();
			if (location != null) {
				this.locationX = (int) location.getLongitude();
				this.locationY = (int) location.getLatitude();
			} else {
				this.locationX = 0;
				this.locationY = 0;
			}
		}

	}

}
