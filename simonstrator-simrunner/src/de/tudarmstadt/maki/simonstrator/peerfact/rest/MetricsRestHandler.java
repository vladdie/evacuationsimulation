package de.tudarmstadt.maki.simonstrator.peerfact.rest;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseFeature;

import de.tud.kom.p2psim.impl.analyzer.metric.MetricAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue;

/**
 * Access to metrics via REST
 * 
 * @author Bjoern Richerzhagen
 *
 */
@Path("metrics")
public class MetricsRestHandler {

	/**
	 * Provide a JSON-formatted list of all available metrics.
	 * 
	 * @return
	 */
	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllMetrics() {
		if (!MetricAnalyzer.isInitialized()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		MetricsXml allMetrics = new MetricsXml();
		for (Metric<?> metric : MetricAnalyzer.getMetrics()) {
			allMetrics.addMetric(metric);
		}
		return Response.ok(allMetrics).build();
	}

	/**
	 * Provide a JSON-formatted list of global (non-per-host) metrics
	 * 
	 * @return
	 */
	@GET
	@Path("/global")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getGlobalMetrics() {
		if (!MetricAnalyzer.isInitialized()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		MetricsXml globalMetrics = new MetricsXml();
		for (Metric<?> metric : MetricAnalyzer.getMetrics()) {
			if (metric.isOverallMetric()) {
				globalMetrics.addMetric(metric);
			}
		}
		return Response.ok(globalMetrics).build();
	}

	/**
	 * Provide a JSON-formatted list of per-host metrics of the host with the
	 * given ID
	 * 
	 * @return
	 */
	@GET
	@Path("/host/{hostid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHostMetrics(@PathParam("hostid") int hostid) {
		if (!MetricAnalyzer.isInitialized()) {
			return Response.status(Status.NOT_FOUND).build();
		}
		MetricsXml hostMetrics = new MetricsXml();
		for (Metric<?> metric : MetricAnalyzer.getMetrics()) {
			if (!metric.isOverallMetric() && metric.getPerHostMetric(INodeID.get(hostid)) != null) {
				hostMetrics.addMetric(metric);
			}
		}
		return Response.ok(hostMetrics).build();
	}

	/**
	 * Return a single metric info
	 * 
	 * @return
	 */
	@GET
	@Path("/info/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMetricInfo(@PathParam("name") String name) {
		if (!MetricAnalyzer.isInitialized()) {
			return Response.status(Status.NOT_FOUND).build();
		}

		Metric<?> metric = MetricAnalyzer.getMetric(name);
		if (metric == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(new MetricXml(metric)).build();
	}

	/**
	 * Return an SSE-Stream for a list of metrics. All non-global metrics are
	 * filtered for the given host id
	 * 
	 * @return
	 */
	@GET
	@Path("/stream/")
	@Produces(SseFeature.SERVER_SENT_EVENTS)
	public EventOutput getStream(@QueryParam("metric") List<String> metricNames,
			@DefaultValue("-1") @QueryParam("hostId") int hostId) {
		if (!MetricAnalyzer.isInitialized() || metricNames == null || metricNames.isEmpty()) {
			System.err.println("Analyzer not initialized, metricNames null or empty.");
			return null; // Response.status(Status.NOT_FOUND).build();
		}

		List<MetricValueXml> metrics = new LinkedList<>();

		for (String name : metricNames) {
			Metric<?> metric = MetricAnalyzer.getMetric(name);
			if (metric == null) {
				return null; // Response.status(Status.NOT_FOUND).build();
			}
			if (!metric.isOverallMetric()) {
				if (hostId == -1) {
					return null; // Response.status(Status.NOT_FOUND).build();
				}
				metrics.add(new MetricValueXml(metric.getName(), metric.getPerHostMetric(INodeID.get(hostId))));
			} else {
				metrics.add(new MetricValueXml(metric.getName(), metric.getOverallMetric()));
			}

		}

		final EventOutput eventOutput = new EventOutput();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (true) {
						for (MetricValueXml mv : metrics) {
							mv.updateValue();
						}
						final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
						eventBuilder.mediaType(MediaType.APPLICATION_JSON_TYPE);
						eventBuilder.data(new MetricValuesXml(metrics));
						final OutboundEvent event = eventBuilder.build();
						eventOutput.write(event);
						try {
							Thread.sleep(250);
						} catch (InterruptedException e) {
							//
						}
					}
				} catch (IOException | NullPointerException e) {
					// Fail silently!
					e.printStackTrace();
				} catch (WebApplicationException e) {
					// Fail silently
					e.printStackTrace();
				} finally {
					try {
						eventOutput.close();
					} catch (IOException ioClose) {
						throw new RuntimeException("Error when closing the event output.", ioClose);
					}
				}
			}
		}, "SSE.metrics").start();
		return eventOutput;
	}


	/**
	 * Container for all metrics.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@XmlRootElement
	public static class MetricsXml {

		@XmlElement
		private List<MetricXml> metrics;

		public MetricsXml() {
			// JAXB needs this
			this.metrics = new LinkedList<>();
		}

		public void addMetric(Metric<?> metric) {
			// todo
			metrics.add(new MetricXml(metric));
		}

	}

	/**
	 * A single metric.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@XmlRootElement
	public static class MetricXml {

		@XmlElement
		private String name;

		@XmlElement
		private String description;

		@XmlElement
		private boolean isPerHost;

		@XmlElement
		private String unit;

		public MetricXml() {
		} // JAXB needs this

		public MetricXml(Metric<?> metric) {
			this.name = metric.getName();
			this.description = metric.getDescription();
			this.isPerHost = !metric.isOverallMetric();
			this.unit = metric.getUnit().toString();
	    }
	}

	/**
	 * A metric value
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@XmlRootElement
	public static class MetricValueXml {

		@XmlElement
		private String name;

		@XmlElement
		private double value;

		@XmlElement
		private boolean valid;

		private transient MetricValue<?> mv;

		public MetricValueXml() {
		} // JAXB needs this

		public MetricValueXml(String name, MetricValue<?> mv) {
			this.name = name;
			this.valid = false;
			this.mv = mv;
		}

		public void updateValue() {
			this.value = ((Number) (mv.getValue())).doubleValue();
			this.valid = mv.isValid();
		}
	}

	/**
	 * Container for multiple metric values sent via SSE
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@XmlRootElement
	public static class MetricValuesXml {

		@XmlElement
		private List<MetricValueXml> values;

		public MetricValuesXml() {
		} // JAXB needs this

		public MetricValuesXml(List<MetricValueXml> values) {
			this.values = values;
		}
	}

}
