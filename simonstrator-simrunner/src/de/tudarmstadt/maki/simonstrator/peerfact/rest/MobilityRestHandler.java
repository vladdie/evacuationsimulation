package de.tudarmstadt.maki.simonstrator.peerfact.rest;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseFeature;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationActuator;

/**
 * Access to mobility parameters via REST. Access to these methods is restricted
 * and works only for hosts that have an active binding within the
 * {@link ScenarioRestHandler}.
 * 
 * - a list of all attraction points for a given host
 * 
 * - a stream source for the current movement target, updated whenever this
 * target changes.
 * 
 * @author Bjoern Richerzhagen
 *
 */
@Path("mobility")
public class MobilityRestHandler {

	/**
	 * Gathers all friends of the provided host at the specified attraction
	 * point. In later versions of this class, friends could actually be
	 * determined based on the application knowledge or workload. Currently, we
	 * simply gather all nodes.
	 * 
	 * @return
	 */
	@GET
	@Path("/gather/{hostId}/{target}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response gatherAt(@PathParam("hostId") int hostId, @PathParam("target") String target) {
		if (!ScenarioRestHandler.acceptConnectionTo(INodeID.get(hostId))) {
			return Response.status(Status.NOT_FOUND).build();
		}

		Host h = Oracle.getHostByID(INodeID.get(hostId));
		if (h == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		LocationActuator actuator = null;
		try {
			actuator = h.getComponent(LocationActuator.class);
			Set<AttractionPoint> aps = actuator.getAllAttractionPoints();
			for (AttractionPoint ap : aps) {
				if (ap.getName().equals(target)) {
					for (Host host : Oracle.getAllHosts()) {
						LocationActuator act = host.getComponent(LocationActuator.class);
						if (!ap.getName().equals(act.getCurrentTargetAttractionPoint().getName())) {
							act.setTargetAttractionPoint(ap);
						}
					}
					return Response.ok().build();
				}
			}
			return Response.status(Status.NOT_FOUND).build();
		} catch (ComponentNotAvailableException e1) {
			//
			System.err.println("LocationActuator for " + hostId + " not found.");
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	/**
	 * Set the target attraction point for a single host.
	 * 
	 * @return
	 */
	@GET
	@Path("/move/{hostId}/{target}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response moveTo(@PathParam("hostId") int hostId, @PathParam("target") String target) {
		if (!ScenarioRestHandler.acceptConnectionTo(INodeID.get(hostId))) {
			return Response.status(Status.NOT_FOUND).build();
		}

		Host h = Oracle.getHostByID(INodeID.get(hostId));
		if (h == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		LocationActuator actuator = null;
		try {
			actuator = h.getComponent(LocationActuator.class);
			Set<AttractionPoint> aps = actuator.getAllAttractionPoints();
			for (AttractionPoint ap : aps) {
				if (ap.getName().equals(target)) {
					if (!ap.getName().equals(actuator.getCurrentTargetAttractionPoint().getName())) {
						actuator.setTargetAttractionPoint(ap);
					}
					return Response.ok().build();
				}
			}
			return Response.status(Status.NOT_FOUND).build();
		} catch (ComponentNotAvailableException e1) {
			//
			System.err.println("LocationActuator for " + hostId + " not found.");
			return Response.status(Status.NOT_FOUND).build();
		}
	}

	/**
	 * Provide a JSON-formatted list of all attraction points
	 * 
	 * @return
	 */
	@GET
	@Path("/targets/{hostId}/")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAttractionPoints(@PathParam("hostId") int hostId) {
		if (!ScenarioRestHandler.acceptConnectionTo(INodeID.get(hostId))) {
			return Response.status(Status.NOT_FOUND).build();
		}

		Host h = Oracle.getHostByID(INodeID.get(hostId));
		if (h == null) {
			return Response.status(Status.NOT_FOUND).build();
		}

		LocationActuator actuator = null;
		try {
			actuator = h.getComponent(LocationActuator.class);
		} catch (ComponentNotAvailableException e1) {
			//
			System.err.println("LocationActuator for " + hostId + " not found.");
			return Response.status(Status.NOT_FOUND).build();
		}

		return Response.ok(new AttractionPointsXml(actuator)).build();
	}

	/**
	 * Create an SSE stream for the target waypoint of a node. To not interfere
	 * with the simulation, we do not register as listener, but instead perform
	 * periodic probing. Events are only sent, if the target waypoint changed.
	 * 
	 * @param hostId
	 *            host to observe
	 * @return
	 */
	@GET
	@Path("/target/{hostId}/")
	@Produces(SseFeature.SERVER_SENT_EVENTS)
	public EventOutput getTargetAttractionPoint(@PathParam("hostId") int hostId) {
		if (!ScenarioRestHandler.acceptConnectionTo(INodeID.get(hostId))) {
			return null;
		}
		Host h = Oracle.getHostByID(INodeID.get(hostId));
		if (h == null) {
			return null;
		}

		LocationActuator actuator = null;
		try {
			actuator = h.getComponent(LocationActuator.class);
		} catch (ComponentNotAvailableException e1) {
			//
			System.err.println("LocationActuator for " + hostId + " not found.");
			return null;
		}

		final AttractionPointXml xml = new AttractionPointXml(actuator);

		final EventOutput eventOutput = new EventOutput();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (true) {
						boolean sendUpdate = xml.update();
						if (sendUpdate) {
							final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
							eventBuilder.mediaType(MediaType.APPLICATION_JSON_TYPE);
							eventBuilder.data(xml);
							final OutboundEvent event = eventBuilder.build();
							eventOutput.write(event);
						}
						try {
							Thread.sleep(250);
						} catch (InterruptedException e) {
							//
						}
					}
				} catch (IOException | NullPointerException e) {
					// Fail silently!
					e.printStackTrace();
				} catch (WebApplicationException e) {
					// Fail silently
					e.printStackTrace();
				} finally {
					try {
						eventOutput.close();
					} catch (IOException ioClose) {
						throw new RuntimeException("Error when closing the event output.", ioClose);
					}
				}
			}
		}, "SSE.mobility." + hostId).start();
		return eventOutput;
	}

	/**
	 * Container for mobility stream, currently only containing the target
	 * waypoint.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@XmlRootElement
	public static class AttractionPointsXml {

		@XmlElement
		private List<AttractionPointXml> waypoints;

		public AttractionPointsXml() {
			// JAXB need this.
		}

		public AttractionPointsXml(LocationActuator locActuator) {
			waypoints = new LinkedList<>();
			Set<AttractionPoint> aps = locActuator.getAllAttractionPoints();
			for (AttractionPoint ap : aps) {
				waypoints.add(new AttractionPointXml(locActuator, ap));
			}
		}

	}

	/**
	 * Container for mobility stream, currently only containing the target
	 * waypoint.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	@XmlRootElement
	public static class AttractionPointXml {

		@XmlElement
		private String name;

		private transient LocationActuator locActuator;

		private transient AttractionPoint ap;

		public AttractionPointXml() {
			// JAXB need this.
		}

		/**
		 * Dynamic updates (for SSE stream)
		 * 
		 * @param locActuator
		 */
		public AttractionPointXml(LocationActuator locActuator) {
			this.locActuator = locActuator;
		}

		/**
		 * Static AP, for list of all aps.
		 * 
		 * @param ap
		 */
		public AttractionPointXml(LocationActuator locActuator, AttractionPoint ap) {
			this(locActuator);
			this.ap = ap;
			this.name = ap.getName();
		}

		/**
		 * True, if data changed and we need to send an event
		 * 
		 * @return
		 */
		public boolean update() {
			boolean update = false;
			if (locActuator.getCurrentTargetAttractionPoint() != null) {
				String newTarget = locActuator.getCurrentTargetAttractionPoint().getName();
				if (newTarget != null && !newTarget.equals(name)) {
					this.ap = locActuator.getCurrentTargetAttractionPoint();
					this.name = this.ap.getName();
					update = true;
				}
			}
			return update;
		}

	}

}
