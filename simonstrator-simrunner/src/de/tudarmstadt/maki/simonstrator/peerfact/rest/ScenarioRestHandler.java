package de.tudarmstadt.maki.simonstrator.peerfact.rest;

import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.glassfish.jersey.media.sse.EventOutput;
import org.glassfish.jersey.media.sse.OutboundEvent;
import org.glassfish.jersey.media.sse.SseFeature;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.LifecycleComponent;
import edu.emory.mathcs.backport.java.util.Collections;

/**
 * Access to hosts, churn, and some generic scenario parameters.
 * 
 * This is the class to extend if you intend to build your own application. To
 * enable devices to bind via REST to one of the simulation nodes, you need to
 * register a {@link RestClientEndpoint}. All API calls on the
 * {@link ScenarioRestHandler} then need to contain the respective endpointID
 * provided by your endpoint implementation.
 * 
 * @author Bjoern Richerzhagen
 *
 */
@Path("scenario")
public class ScenarioRestHandler {

	/**
	 * Enable or disable client connections with the REST interface. This is
	 * enabled as soon as at least one {@link RestClientListener} is registered.
	 */
	private static boolean acceptClientConnections = false;

	/**
	 * Listeners that are informed whenever a device connects to the REST api
	 * and is assigned to a simulation host. Can be used to alter visualizations
	 * accordingly or to trigger some application-specific actions.
	 */
	private static List<RestClientListener> restClientListeners = new LinkedList<>();

	/**
	 * Registered endpoints that wait for clients to connect or disconnect to
	 * the REST API to execute host-specific actions. As this is very much
	 * related to demonstration scenarios, you will most likely only have once
	 * active {@link RestClientEndpoint} at a time.
	 */
	private static Map<String, RestClientEndpoint> restClientEndpoints = new ConcurrentHashMap<>();

	/**
	 * Active connections (clients bound to a mobile device via REST). These
	 * clients are NOT affected by churn.
	 */
	private static Map<INodeID, ClientConnection> activeClientConnections = new ConcurrentHashMap<>();

	/**
	 * Setting at least one {@link RestClientEndpoint} allows web clients to
	 * connect to a single host in the simulation. The
	 * {@link RestClientEndpoint} determines the {@link HostComponent} (one of
	 * {@link LifecycleComponent}) that is filtered for.
	 * 
	 * @param endpoint
	 */
	public static void addRestClientEndpoint(RestClientEndpoint endpoint) {
		assert !restClientEndpoints.containsKey(endpoint.getEndpointId());
		restClientEndpoints.put(endpoint.getEndpointId(), endpoint);
		acceptClientConnections = true;
	}

	/**
	 * Remove an endpoint. If no endpoint is active afterwards, the REST api
	 * will no longer accept incoming bind requests.
	 * 
	 * @param endpoint
	 */
	public static void removeRestClientEndpoint(RestClientEndpoint endpoint) {
		restClientEndpoints.remove(endpoint.getEndpointId());
		acceptClientConnections = !restClientEndpoints.isEmpty();
	}

	/**
	 * Add a {@link RestClientListener}. This listener is informed as soon as a
	 * client connects or disconnects to the demo. Use this to highlight
	 * client-specific stuff in the visualization. This listener will only be
	 * invoked, if at least one {@link RestClientEndpoint} is registered.
	 * 
	 * @param listener
	 */
	public static void addRestClientListener(RestClientListener listener) {
		restClientListeners.add(listener);
	}

	/**
	 * Remove a client listener
	 * 
	 * @param listener
	 */
	public static void removeRestClientListener(RestClientListener listener) {
		restClientListeners.remove(listener);
	}

	/**
	 * Can be used by other API endpoints to check if client calls for the
	 * specific ID are allowed.
	 * 
	 * @param hostId
	 * @return
	 */
	public static boolean acceptConnectionTo(INodeID hostId) {
		if (!acceptClientConnections) {
			return false;
		}
		return activeClientConnections.containsKey(hostId);
	}

	/**
	 * Get a host the client is supposed to bind to.
	 * 
	 * @param hostId
	 *            may be -1 to disable the filter
	 * @param endpoint
	 *            the {@link RestClientEndpoint} taking care of this connection.
	 * @return Host or null
	 */
	private static Host getHostForClientConnection(int hostId, RestClientEndpoint endpoint) {
		assert acceptClientConnections;
		if (hostId != -1) {
			// return specific host
			if (activeClientConnections.containsKey(INodeID.get(hostId))) {
				// already in use
				System.err.println("The host " + hostId + " is already connected to a client.");
				return null;
			}
			Host h = Oracle.getHostByID(INodeID.get(hostId));
			return h;
		} else {
			List<Host> toShuffle = new LinkedList<>(Oracle.getAllHosts());
			Collections.shuffle(toShuffle);
			for (Host candidate : toShuffle) {
				try {
					// Filter for component
					LifecycleComponent comp = candidate.getComponent(endpoint.getEndpointComponentClass());
					if (comp.isActive() && !activeClientConnections.containsKey(candidate.getId())) {
						// only if ACTIVE and not yet controlled
						return candidate;
					}
				} catch (ComponentNotAvailableException e) {
					continue;
				}
			}
			return null;
		}
	}

	/*
	 * Internal calls related to churn - this takes care that we only trigger
	 * offline events for nodes that are not currently controlled by a client.
	 */

	/**
	 * Triggers churn (By interacting with the {@link LifecycleComponent} as
	 * defined in the {@link RestClientEndpoint}, not the Underlay!)
	 * 
	 * FIXME: we might need to add schedule an Event for this method.
	 * 
	 * @param delta
	 *            how many clients to affect? If <0, clients will leave.
	 * @param duration
	 *            period of simulation time over which the clients will join
	 * 
	 * @param endpoint
	 */
	public static void triggerChurn(int delta, long duration, RestClientEndpoint endpoint) {
		Class<? extends LifecycleComponent> clazz = endpoint.getEndpointComponentClass();
		if (delta > 0) {
			// active delta hosts at random
			List<Host> toShuffle = new LinkedList<>(Oracle.getAllHosts());
			Collections.shuffle(toShuffle);
			int done = 0;
			for (Host candidate : toShuffle) {
				if (done == delta) {
					// done.
					return;
				}
				try {
					// Filter for component
					LifecycleComponent comp = candidate.getComponent(clazz);
					if (!comp.isActive()) {
						done++;
						Event.scheduleWithDelay(done * duration / delta, new EventHandler() {
							@Override
							public void eventOccurred(Object content, int type) {
								if (!comp.isActive()) {
									comp.startComponent();
								}
							}
						}, null, 0);
					}
				} catch (ComponentNotAvailableException e) {
					continue;
				}
			}
		} else if (delta < 0) {
			Set<INodeID> controlledClients = new LinkedHashSet<>(activeClientConnections.keySet());
			// remove delta hosts (excluding API-controlled ones)
			List<Host> toShuffle = new LinkedList<>(Oracle.getAllHosts());
			Collections.shuffle(toShuffle);
			int done = 0;
			for (Host candidate : toShuffle) {
				if (done == delta) {
					// done.
					return;
				}
				try {
					// Filter for component
					LifecycleComponent comp = candidate.getComponent(clazz);
					if (comp.isActive() && !controlledClients.contains(candidate.getId())) {
						done--;
						Event.scheduleWithDelay(done * duration / delta, new EventHandler() {
							@Override
							public void eventOccurred(Object content, int type) {
								if (comp.isActive()) {
									comp.stopComponent();
								}
							}
						}, null, 0);
					}
				} catch (ComponentNotAvailableException e) {
					continue;
				}
			}
		}
	}

	/**
	 * Programmatic access to the bindHost method. You need to explicitly free
	 * the host once the connection is no longer maintained. This method ensures
	 * that churn is only applied to non-bound hosts.
	 * 
	 * This is only needed if you want to implement custom session handling
	 * within your application. Otherwise, simply use the bind REST-API within
	 * this class and then just use the assigned host ID to request a stream
	 * from your custom endpoint (i.e., host assignment should only be done in
	 * this class.)
	 * 
	 * @return
	 * @deprecated use a hostId-parameter in your custom SSE source instead.
	 */
	public static ClientConnection bindRandomHost(RestClientEndpoint endpoint) {
		Host assignedHost = getHostForClientConnection(-1, endpoint);
		if (assignedHost == null) {
			System.err.println("No suitable host found.");
			return null;
		}
		// register active connection
		ClientConnection clientConnectionXml = new ClientConnection(assignedHost);
		activeClientConnections.put(assignedHost.getId(), clientConnectionXml);
		// Notify listeners
		restClientListeners.forEach(listener -> listener.onClientConnect(clientConnectionXml));
		return clientConnectionXml;
	}

	/**
	 * Free a previously bound host.
	 * 
	 * @return
	 * @deprecated use a hostId-parameter in your custom SSE source instead.
	 */
	public static void freeHost(INodeID hostId) {
		ClientConnection clientConnectionXml = activeClientConnections.remove(hostId);
		if (clientConnectionXml != null) {
			restClientListeners.forEach(listener -> listener.onClientDisconnect(clientConnectionXml));
		}
	}

	/*
	 * API methods
	 */

	/**
	 * Used to bind the web client to a host. This is simply used to determine
	 * if a client is connected to the demo and to add or remove such
	 * connections.
	 * 
	 * @param endpointId
	 *            ID of the {@link RestClientEndpoint} expected to handle the
	 *            new connection.
	 * @param hostId
	 *            request access to the specific host with the given host id
	 *            (usually not used).
	 * @return
	 */
	@GET
	@Path("/connect/{endpointId}")
	@Produces(SseFeature.SERVER_SENT_EVENTS)
	public EventOutput bindHost(@PathParam("endpointId") String endpointId,
			@DefaultValue("-1") @QueryParam("hostId") int hostId) {

		if (!acceptClientConnections || !restClientEndpoints.containsKey(endpointId)) {
			System.err.println("No RestClientEndpoint " + endpointId + " available.");
			return null;
		}

		final ClientConnection clientConnectionXml = bindRandomHost(restClientEndpoints.get(endpointId));
		
		final EventOutput eventOutput = new EventOutput();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					while (acceptClientConnections) {
						final OutboundEvent.Builder eventBuilder = new OutboundEvent.Builder();
						eventBuilder.mediaType(MediaType.APPLICATION_JSON_TYPE);
						eventBuilder.data(clientConnectionXml);
						final OutboundEvent event = eventBuilder.build();
						eventOutput.write(event);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							//
						}
					}
				} catch (IOException | NullPointerException e) {
					// Fail silently!
					e.printStackTrace();
				} catch (WebApplicationException e) {
					// Fail silently
					e.printStackTrace();
				} finally {
					freeHost(clientConnectionXml.host.getId());
					try {
						eventOutput.close();
					} catch (IOException ioClose) {
						throw new RuntimeException("Error when closing the event output.", ioClose);
					}
				}
			}
		}, "SSE.scenario." + clientConnectionXml.id).start();
		return eventOutput;
	}
	
	/**
	 * An endpoint for clients trying to bind via the REST API (this is your
	 * application). The {@link ScenarioRestHandler} will take care of assigning
	 * a random host with an active component of the type specified in this
	 * endpoint to the client.
	 * 
	 * To ensure proper operation, you should not mess with client churn or
	 * activity other than through the {@link ScenarioRestHandler}.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static interface RestClientEndpoint {

		/**
		 * Interface of the HostComponent that should be controlled by this
		 * client listener. The simple name of this class is also used to filter
		 * incoming bind requests.
		 * 
		 * @return
		 */
		public Class<? extends LifecycleComponent> getEndpointComponentClass();

		/**
		 * A simple string representation used when invoking API endpoints. You
		 * should stick to the simple name of the component class.
		 * 
		 * @return
		 */
		public String getEndpointId();

	}

	/**
	 * Register your custom visualization as a {@link RestClientListener} to be
	 * informed as soon as a client associates to the demo (and highlight the
	 * corresponding node accordingly, or anything related).
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static interface RestClientListener {

		/**
		 * Called whenever a client associates via the REST endpoint.
		 * 
		 * @param host
		 */
		public void onClientConnect(ClientConnection connection);

		/**
		 * Called when the connection to the client is closed.
		 * 
		 * @param host
		 */
		public void onClientDisconnect(ClientConnection connection);

	}

	@XmlRootElement
	public static class ClientConnection {

		@XmlElement
		private int id;

		/*
		 * TODO color?
		 */

		@XmlElement
		private String colorHex;

		private transient Host host;

		// Required for JSON
		public ClientConnection() {
			// TODO Auto-generated constructor stub
		}

		public ClientConnection(Host host) {
			this.host = host;
			this.id = (int) host.getId().value();
			this.colorHex = "#990000"; // FIXME
		}

		public Host getHost() {
			return host;
		}

		public int getId() {
			return id;
		}

	}

}
