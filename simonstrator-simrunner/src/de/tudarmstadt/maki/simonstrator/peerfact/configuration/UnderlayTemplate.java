package de.tudarmstadt.maki.simonstrator.peerfact.configuration;

import de.tud.kom.p2psim.impl.linklayer.LinkLayerFactory;
import de.tud.kom.p2psim.impl.linklayer.mac.configs.Ieee80211AdHocConfig;
import de.tud.kom.p2psim.impl.linklayer.mac.configs.SimpleMac;
import de.tud.kom.p2psim.impl.network.routed.RoutedNetLayerFactory;
import de.tud.kom.p2psim.impl.network.routed.config.Routing;
import de.tud.kom.p2psim.impl.transport.modular.ModularTransLayerFactory;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Rate;

/**
 * Base class for all underlay templates, supporting some general purpose
 * configuration options.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class UnderlayTemplate implements HostTemplate {

	private final LinkLayerFactory linkFactory = new LinkLayerFactory();

	private final RoutedNetLayerFactory netFactory = new RoutedNetLayerFactory();

	private final ModularTransLayerFactory transFactory = new ModularTransLayerFactory();

	/**
	 * Default Constructor starting all hosts offline.
	 */
	public UnderlayTemplate() {
		netFactory.setStartHostsOffline(true);
		netFactory.setEnableFragmenting(true);
	}

	/*
	 * XML-Setters
	 */

	/**
	 * Use wifi
	 * 
	 * @param model
	 *            80211 or default
	 */
	public void setWifi(String model) {
		if (model.equals("80211")) {
			configureWiFi80211();
		} else if (model.equals("default")) {
			configureWiFi();
		} else {
			throw new AssertionError("WiFi has to be one of [80211,default]");
		}
	}

	/**
	 * Should hosts start offline?
	 * 
	 * @param startOffline
	 */
	public void setStartOffline(boolean startOffline) {
		netFactory.setStartHostsOffline(startOffline);
	}

	/**
	 * Enable NET Fragmentation
	 * 
	 * @param startOffline
	 */
	public void setEnableFragmentation(boolean enableFragmentation) {
		netFactory.setEnableFragmenting(enableFragmentation);
	}

	public void setEthernet(boolean useEthernet) {
		if (useEthernet) {
			configureEthernet();
		}
	}

	public void setCell(boolean useCell) {
		if (useCell) {
			configureCell();
		}
	}

	public void setBluetooth(boolean useBluetooth) {
		if (useBluetooth) {
			configureBT();
		}
	}

	/**
	 * Creates a default Ethernet-Stack
	 */
	protected void configureEthernet() {
		SimpleMac mac = new SimpleMac();
		mac.setPhy("ETHERNET");
		mac.setDownBandwidth(100 * Rate.Mbit_s);
		mac.setUpBandwidth(100 * Rate.Mbit_s);
		mac.setTrafficQueueSize(5000);
		mac.setTrafficQueueTimeout(0);
		mac.setMaxRetransmissions(0);
		linkFactory.setMac(mac);
		Routing routing = new Routing("IPv4", "ONE_HOP");
		routing.setPhy("ETHERNET");
		netFactory.setRouting(routing);
	}

	/**
	 * Creates a default WiFi-Stack
	 */
	protected void configureWiFi() {
		SimpleMac mac = new SimpleMac();
		mac.setPhy("WIFI");
		mac.setDownBandwidth(22 * Rate.Mbit_s);
		mac.setUpBandwidth(22 * Rate.Mbit_s);
		mac.setTrafficQueueSize(5000);
		mac.setTrafficQueueTimeout(0);
		mac.setMaxRetransmissions(3);
		linkFactory.setMac(mac);
		Routing routing = new Routing("IPv4", "ONE_HOP");
		routing.setPhy("WIFI");
		netFactory.setRouting(routing);
	}

	/**
	 * Creates a default WiFi-Stack
	 */
	protected void configureWiFi80211() {
		Ieee80211AdHocConfig mac = new Ieee80211AdHocConfig();
		mac.setPhy("WIFI");
		linkFactory.setMac(mac);
		Routing routing = new Routing("IPv4", "ONE_HOP");
		routing.setPhy("WIFI");
		netFactory.setRouting(routing);
	}

	/**
	 * Creates a stack for cellular communication
	 */
	protected void configureCell() {
		SimpleMac mac = new SimpleMac();
		mac.setPhy("UMTS");
		mac.setDownBandwidth(2 * Rate.Mbit_s);
		mac.setUpBandwidth(2 * Rate.Mbit_s);
		mac.setTrafficQueueSize(10000);
		mac.setTrafficQueueTimeout(0);
		linkFactory.setMac(mac);
		Routing routing = new Routing("IPv4", "GLOBAL_KNOWLEDGE");
		routing.setPhy("UMTS");
		netFactory.setRouting(routing);
	}

	/**
	 * Creates a Bluetooth stack
	 */
	protected void configureBT() {
		SimpleMac mac = new SimpleMac();
		mac.setPhy("BLUETOOTH");
		mac.setDownBandwidth(3 * Rate.Mbit_s);
		mac.setUpBandwidth(3 * Rate.Mbit_s);
		mac.setTrafficQueueSize(5000);
		mac.setMaxRetransmissions(3);
		linkFactory.setMac(mac);
		Routing routing = new Routing("IPv4", "ONE_HOP");
		routing.setPhy("BLUETOOTH");
		netFactory.setRouting(routing);
	}

	@Override
	public void configure(Host host) {
		host.registerComponent(linkFactory.createComponent(host));
		host.registerComponent(netFactory.createComponent(host));
		host.registerComponent(transFactory.createComponent(host));
	}

}
