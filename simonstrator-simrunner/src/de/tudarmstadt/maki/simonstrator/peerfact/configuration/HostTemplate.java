package de.tudarmstadt.maki.simonstrator.peerfact.configuration;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;

/**
 * Template interface for a host configuration, specifying a number of
 * components used by that host. Each host may have multiple templates assigned.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface HostTemplate {

	/**
	 * This method has to add the respective {@link HostComponent}s to the host.
	 * The components are not yet initialized at this stage.
	 * 
	 * @param host
	 *            the host to configure
	 */
	public void configure(Host host);

}
