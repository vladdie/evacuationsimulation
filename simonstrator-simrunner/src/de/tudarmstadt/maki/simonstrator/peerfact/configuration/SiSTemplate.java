package de.tudarmstadt.maki.simonstrator.peerfact.configuration;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSProviderErrorModel;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.oracle.GlobalKnowledgeResolver;
import de.tudarmstadt.maki.simonstrator.service.sis.minimal.MinimalSiSComponent;

/**
 * Creating the SiS and a corresponding monitoring solution
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class SiSTemplate implements HostTemplate {

	MinimalSiSComponent.Factory sisFactory = new MinimalSiSComponent.Factory();

	GlobalKnowledgeResolver.Factory monitoringFactory = new GlobalKnowledgeResolver.Factory();

	@Override
	public void configure(Host host) {
		host.registerComponent(sisFactory.createComponent(host));
		host.registerComponent(monitoringFactory.createComponent(host));
	}

	public void setErrorModel(SiSProviderErrorModel<?> errorModel) {
		sisFactory.addErrorModel(errorModel);
	}

}
