package de.tudarmstadt.maki.simonstrator.peerfact.configuration;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.besteffort.BestEffortTransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.globalknowledge.GlobalKnowledgeTransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.local.LocalTransitionEngine;

/**
 * Configures a host with all basic components needed to make use of the
 * transition engine. Currently, this includes:
 * 
 * {@link LocalTransitionEngine}: the local implementation of a transition
 * engine, responsible for creating and controlling mechanism proxies and atomic
 * transitions.
 * 
 * {@link BestEffortTransitionCoordinator}: a simple implementation of a
 * {@link TransitionCoordinator} that can be used to trigger remote transitions.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class TransitionEngineTemplate implements HostTemplate {

	private LocalTransitionEngine.Factory tEngineLocal = new LocalTransitionEngine.Factory();

	private HostComponentFactory coordinatorFactory = new GlobalKnowledgeTransitionCoordinator.Factory();

	private long globalKnowledgeDisseminationDelay = 0;

	private long globalKnowledgeDisseminationVariance = 0;

	private boolean configured = false;

	public TransitionEngineTemplate() {
		//
	}

	@Override
	public void configure(Host host) {
		if (!configured) {
			if (coordinatorFactory instanceof GlobalKnowledgeTransitionCoordinator.Factory) {
				((GlobalKnowledgeTransitionCoordinator.Factory) coordinatorFactory)
						.setDisseminationDelay(globalKnowledgeDisseminationDelay);
				((GlobalKnowledgeTransitionCoordinator.Factory) coordinatorFactory)
						.setDisseminationDelayVariance(globalKnowledgeDisseminationVariance);
			}
		}
		host.registerComponent(tEngineLocal.createComponent(host));
		host.registerComponent(coordinatorFactory.createComponent(host));
	}

	/**
	 * If true, dissemination is done via the global knowledge engine. Shortcut,
	 * to get rid of the need to pass the factory itself to this template.
	 * 
	 * @param useGlobalKnowledge
	 */
	public void setUseGlobalKnowledge(boolean useGlobalKnowledge) {
		if (useGlobalKnowledge) {
			this.coordinatorFactory = new GlobalKnowledgeTransitionCoordinator.Factory();
		} else {
			this.coordinatorFactory = new BestEffortTransitionCoordinator.Factory();
		}
		this.configured = false;
	}

	public void setGlobalKnowledgeDisseminationDelay(long globalKnowledgeDisseminationDelay) {
		this.globalKnowledgeDisseminationDelay = globalKnowledgeDisseminationDelay;
		this.configured = false;
	}

	public void setGlobalKnowledgeDisseminationVariance(long globalKnowledgeDisseminationVariance) {
		this.globalKnowledgeDisseminationVariance = globalKnowledgeDisseminationVariance;
		this.configured = false;
	}

	/**
	 * Can be used to overwrite the default Factory for the
	 * {@link TransitionCoordinator}.
	 * 
	 * @param coordinatorFactory
	 */
	public void setCoordinatorFactory(HostComponentFactory coordinatorFactory) {
		this.coordinatorFactory = coordinatorFactory;
		this.configured = true;
	}

}
