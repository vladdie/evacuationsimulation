package de.tudarmstadt.maki.simonstrator.peerfact.configuration;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.dom4j.Element;

import de.tud.kom.p2psim.api.common.SimHost;
import de.tud.kom.p2psim.api.scenario.Configurator;
import de.tud.kom.p2psim.api.topology.Topology;
import de.tud.kom.p2psim.api.topology.TopologyComponent;
import de.tud.kom.p2psim.impl.common.DefaultHost;
import de.tud.kom.p2psim.impl.common.DefaultHostProperties;
import de.tud.kom.p2psim.impl.common.FakeHost;
import de.tud.kom.p2psim.impl.scenario.DefaultConfigurator;
import de.tud.kom.p2psim.impl.scenario.DefaultHostBuilder;
import de.tud.kom.p2psim.impl.util.oracle.GlobalOracle;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;

/**
 * An extended builder supporting host templates.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class TemplatedHostBuilder extends DefaultHostBuilder {

	public static final String TEMPLATE_TAG = "Template";

	@Override
	public void parse(Element elem, Configurator configurator) {
		DefaultConfigurator config = (DefaultConfigurator) configurator;

		// Go through all top-level (group, host) tags
		for (Iterator<?> iterator = elem.elementIterator(); iterator.hasNext();) {
			Element groupElem = (Element) iterator.next();
			if (groupElem.getName().equals(HOST_TAG)) {
				/*
				 * To unify configuration, hosts are now groups of size 1
				 * (<host> is no longer supported)
				 */
				throw new IllegalArgumentException("<host> is deprecated. Use <group size=\"1\"> instead!");
			}

			String groupID = groupElem.attributeValue(GROUP_ID_TAG);
			int groupSize = Integer.parseInt(config.parseValue(groupElem.attributeValue(GROUP_SIZE_TAG)));
			if (groupSize == 0) {
				// Skip this group
				continue;
			}

			/*
			 * Create all layer-factories - per convention, each factory was
			 * instantiated exactly ONCE
			 */
			List<HostComponentFactory> factories = new LinkedList<>();
			List<HostTemplate> templates = new LinkedList<>();
			Element hostPropertiesElement = null;
			for (Iterator<?> layers = groupElem.elementIterator(); layers.hasNext();) {
				Element layerElem = (Element) layers.next();
				switch (layerElem.getName()) {
				case Configurator.HOST_PROPERTIES_TAG:
					hostPropertiesElement = layerElem;
					continue;

				case TEMPLATE_TAG:
					HostTemplate template = (HostTemplate) config.configureComponent(layerElem);
					if (template == null) {
						Monitor.log(TemplatedHostBuilder.class, Level.WARN, "Host group " + groupElem.getName()
								+ ": A template of name '" + layerElem.getName() + "' is not configured. Ignoring.");
					} else {
						templates.add(template);
					}
					break;

				default:
					// LayerFactory
					HostComponentFactory layer = (HostComponentFactory) config.configureComponent(layerElem);
					if (layer == null) {
						Monitor.log(TemplatedHostBuilder.class, Level.WARN, "Host group " + groupElem.getName()
								+ ": An element of name '" + layerElem.getName() + "' is not configured. Ignoring.");
					} else {
						factories.add(layer);
					}
					break;
				}
			}

			List<SimHost> group = new LinkedList<>();
			// Create-hosts loop
			for (int i = 0; i < groupSize; i++) {
				DefaultHost host = createNewDefaultHost();
				/*
				 * Skip this round if no new host was created. Might happen with
				 * RealNetworkingLayer.
				 */
				if (!(host instanceof FakeHost)) {
					// initialize properties
					DefaultHostProperties hostProperties = new DefaultHostProperties();
					host.setProperties(hostProperties);
					// minimal information for host properties is the group id
					hostProperties.setGroupID(groupID);
					// host properties
					if (hostPropertiesElement != null) {
						config.configureAttributes(hostProperties, hostPropertiesElement);
					}
					/*
					 * Use templates in the specified order
					 */
					for (HostTemplate template : templates) {
						template.configure(host);
					}
					for (HostComponentFactory cF : factories) {
						HostComponent comp = cF.createComponent(host);
						host.registerComponent(comp);
					}
				}
				Monitor.log(TemplatedHostBuilder.class, Level.DEBUG, "Created a group with " + group.size() + " hosts");
				group.add(host);
			}
			hosts.addAll(group);
			groups.put(groupID, group);
		}

		Monitor.log(TemplatedHostBuilder.class, Level.INFO, "CREATED " + hosts.size() + " hosts");

		// Populate Global Oracle
		GlobalOracle.populate(hosts);

		// initialize all hosts
		for (SimHost host : hosts) {
			host.initialize();
		}

		// initializeTopology (should be called after the initialize of all
		// hosts.
		/*
		 * FIXME (BR): is this really necessary?
		 */
		if (hosts.size() > 0) {
			TopologyComponent comp = hosts.iterator().next().getTopologyComponent();
			if (comp != null) {
				Topology topo = comp.getTopology();
				if (topo != null) {
					topo.initializeSocial();
				}
			}
		}
	}

}
