# Contributing to the API

Maintainer: @br

## New Features (non-breaking)

Including new features in the API is simple: just start a new branch of the master-branch and include your feature (interfaces).
Please note, that we only accept interfaces accompanied with a working (minimal) implementation in the overlays or simrunner project.
Before adding interfaces to the API, the feature should have reached a somewhat final stage (usually indicated by working sample code).
Once you uploaded your feature branch, submit a new merge request via GitLab and document the respective feature in the merge request.
Obviously, code has to be commented.

## Updates (breaking)

Changing existing features (interfaces), especially if it affects a significant amount of existing projects, has to be discussed carefully.
Propose your change in a new branch derived of the current master branch and submit a new merge request.
Within the merge request, provide reasons as to why the change is required and how it affects existing projects.
Once the overall theme of the change is accepted, you have to create branches of the master branch in all affected projects (e.g., overlays or simrunner) and implement the introduced changes there (i.e., fix the branches to compile with the new API)
Again, all code has to be commented.
