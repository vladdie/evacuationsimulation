/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.api.component.sensor.location;

import de.tudarmstadt.maki.simonstrator.api.Host;

/**
 * @see http://developer.android.com/reference/com/google/android/gms/location/
 *      LocationListener.html
 * @author Bjoern Richerzhagen
 * 
 */
public interface LocationListener {

	/**
	 * Notified, if the location changed. Depending on the
	 * {@link LocationRequest}, this is triggered periodically (at least in
	 * simulations). It is also triggered if the node did <strong>not</strong>
	 * move since the last update, to ease implementation for periodic checks in
	 * simulation scenarios.
	 * 
	 * If you only want to do sth. as a consequence of an actual position
	 * change, you need to store a copy of the old location and use the
	 * distanceTo() method to determine, if the node moved since the last
	 * update.
	 * 
	 * @param location
	 */
	public void onLocationChanged(Host host, Location location);

}
