/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.api.component.sensor.location;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;

/**
 * Again, based on Android.Location. Does not support direction and speed at the
 * moment.
 * 
 * @see http://developer.android.com/reference/android/location/Location.html
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public interface Location extends Transmitable, Cloneable {

	/**
	 * Sets the contents of this location object to the specified location
	 * 
	 * @param l
	 */
	public void set(Location l);

	/**
	 * Latitude in degrees
	 * 
	 * Consider using only the "relative" methods distanceTo and bearingTo
	 * instead of absolute coordinates, whenever it is possible. This avoids
	 * conflicts in simulations on planar scenarios.
	 * 
	 * In most simulation setups, instead of geographic coordinates, we simply
	 * rely on x and y (therefore, please use distanceTo and bearingTo for
	 * calculations)! In such circumstances, the Latitude corresponds to
	 * <strong>y</strong>.
	 * 
	 * @return latitude (or y)
	 */
	public double getLatitude();

	/**
	 * Longitude in degrees
	 * 
	 * Consider using only the "relative" methods distanceTo and bearingTo
	 * instead of absolute coordinates, whenever it is possible. This avoids
	 * conflicts in simulations on planar scenarios.
	 * 
	 * In most simulation setups, instead of geographic coordinates, we simply
	 * rely on x and y (therefore, please use distanceTo and bearingTo for
	 * calculations)! In such circumstances, the Latitude corresponds to
	 * <strong>x</strong>.
	 * 
	 * @return longitude (or x)
	 */
	public double getLongitude();

	/**
	 * Allows manipulation of this location object. Can throw an
	 * {@link UnsupportedOperationException}, if not permitted.
	 * 
	 * @param latitude
	 *            (or y)
	 */
	default public void setLatitude(double latitude) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Allows manipulation of this location object. Can throw an
	 * {@link UnsupportedOperationException}, if not permitted.
	 * 
	 * @param latitude
	 *            (or x)
	 */
	default public void setLongitude(double longitude) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Returns the elapsed time since this location was retrieved (measured)
	 * 
	 * @return
	 */
	public long getAgeOfLocation();

	/**
	 * Accuracy of this location estimate in meters (0 meaning perfect
	 * accuracy).
	 * 
	 * @return accuracy in meters (undefined, if hasAccuracy is false!)
	 */
	default public double getAccuracy() {
		return 0;
	}

	/**
	 * True, if the location object has an accuracy associated to it.
	 * 
	 * @return
	 */
	default public boolean hasAccuracy() {
		return false;
	}

	/**
	 * Sets the accuracy of this location estimate in meters (basically the
	 * upper error margin for this estimation). Throws an
	 * {@link UnsupportedOperationException} if this method is not supported by
	 * the implementation.
	 * 
	 * @param accuracy
	 */
	default public void setAccuracy(double accuracy) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/**
	 * 
	 * Distance to the destination in meters
	 * 
	 * @param dest
	 * @return distance to the destination in meters
	 */
	public double distanceTo(Location dest);

	/**
	 * 
	 * Angle to the destination in degrees
	 * 
	 * @param dest
	 * @return bearing angle between this and dest, counting clockwise from true
	 *         north.
	 */
	public float bearingTo(Location dest);

	/**
	 * Copy of the current location (use this to retrieve a valid
	 * {@link Location} object). Usually, the object can then be manipulated via
	 * the setters.
	 * 
	 * @return
	 */
	public Location clone();

}
