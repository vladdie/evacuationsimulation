/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.api.component.transition;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;

/**
 * This analyzer monitors the coordination and execution of transitions.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface TransitionAnalyzer extends Analyzer {

	/**
	 * Invoked right before the transition is executed.
	 * 
	 * @param localhost
	 *            the host executing the transition
	 * @param proxyName
	 * @param targetClass
	 */
	public void onExecuteAtomicTransition(Host localhost, String proxyName,
			Class<? extends TransitionEnabled> targetClass);

	/**
	 * Invoked on the transition coordinator, if a new {@link AtomicTransition}
	 * is to be executed at the target hosts. Can be used to measure the time it
	 * takes until a transition is executed at the client (if the decision
	 * reaches the client at all...)
	 * 
	 * @param localhost
	 * @param proxyName
	 * @param targetClass
	 * @param targetHosts
	 */
	public void onInitiateAtomicTransition(Host localhost, String proxyName,
			Class<? extends TransitionEnabled> targetClass, Collection<INodeID> targetHosts);

}
