/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.api.component.sensor.location;

/**
 * Interface for an attraction point (basically a named location)
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface AttractionPoint extends Location {

	/**
	 * Name of the attraction point
	 * 
	 * @return
	 */
	public String getName();

	/**
	 * A weight (should be normalized between 0 and 1), can be used for example
	 * in the social movement model. This refers to the "importance" of an
	 * {@link AttractionPoint}.
	 * 
	 * @return weight between 0 and 1
	 */
	public double getWeight();

	/**
	 * A radius (optional), basically the radius of influence of the given
	 * attraction point.
	 * 
	 * @return radius in meters
	 */
	public double getRadius();

	/**
	 * A weight (should be normalized between 0 and 1), can be used for example
	 * in the social movement model. This refers to the "importance" of an
	 * {@link AttractionPoint}.
	 * 
	 * @param weight
	 */
	public void setWeight(double weight);

	/**
	 * A radius (optional), basically the radius of influence of the given
	 * attraction point.
	 * 
	 * @param radius
	 */
	public void setRadius(double radius);

	/**
	 * Clones the current attraction point and manipulates the new AP's name.
	 * The location of the new AP can then be manipulated through the
	 * {@link Location} API.
	 * 
	 * @param newName
	 * @return
	 */
	public AttractionPoint clone(String newName);

}
