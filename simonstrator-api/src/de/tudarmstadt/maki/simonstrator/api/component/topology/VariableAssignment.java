package de.tudarmstadt.maki.simonstrator.api.component.topology;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.DirectedEdge;
import de.tudarmstadt.maki.simonstrator.api.common.graph.EdgeID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.IEdge;
import de.tudarmstadt.maki.simonstrator.api.common.graph.IElement;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INode;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.Node;

/**
 * This class represents a binding of pattern variables to graph elements
 */
public final class VariableAssignment {

	private final BiMap<INodeID, INode> nodeBinding;
	private final Map<EdgeID, IEdge> linkBinding;

	public VariableAssignment() {
		this.nodeBinding = HashBiMap.create();
		this.linkBinding = new HashMap<>();
	}

	public VariableAssignment(final VariableAssignment other) {
		this();
		this.nodeBinding.putAll(other.nodeBinding);
		this.linkBinding.putAll(other.linkBinding);
	}

	public INodeID getNodeVariableBinding(final INodeID variable) {
		INode binding = this.nodeBinding.get(variable);
		return binding != null ? binding.getId() : null;
	}

	public INode getBindingNodeForNodeVariable(final INodeID variable) {
		return this.nodeBinding.get(variable);
	}

	public INodeID getInverseVariableBinding(final INodeID variable) {
		return this.nodeBinding.inverse().get(variable);
	}

	public INodeID getNodeVariableBinding(final String key) {
		return nodeBinding.get(INodeID.get(key)).getId();
	}

	public Set<Entry<INodeID, INode>> getNodeBindingEntrySet() {
		return nodeBinding.entrySet();
	}

	public Set<Entry<EdgeID, IEdge>> getLinkBindingEntrySet() {
		return linkBinding.entrySet();
	}

	public void bindVariable(UniqueID variable, IElement candidate) {
		if (variable instanceof INodeID) {
			this.bindNodeVariable((INodeID) variable, (INodeID) candidate);
		} else if (variable instanceof EdgeID) {
			this.bindLinkVariable((EdgeID) variable, (EdgeID) candidate);
		} else {
			throw new IllegalArgumentException("Cannot handle variable type: " + variable);
		}
	}

	/**
	 * @deprecated Use {@link #bindNodeVariable(INodeID, INode)} instead
	 */
	@Deprecated
	public void bindNodeVariable(final INodeID nodeVariable, final INodeID value) {
		nodeBinding.put(nodeVariable, new Node(value));
	}

	public void bindNodeVariable(final INodeID nodeVariable, final INode value) {
		nodeBinding.put(nodeVariable, value);
	}

	public void unbindNodeVariable(final INodeID nodeVariable) {
		nodeBinding.remove(nodeVariable);
	}

	public boolean isBound(final INodeID variable) {
		return nodeBinding.containsKey(variable);
	}

	public boolean isUnbound(final INodeID variable) {
		return !this.isBound(variable);
	}

	/**
	 * Returns whether there exists some variable that is bound by the given
	 * nodeId.
	 * 
	 * @deprecated Use {@link #isBindingForSomeVariable(INode)}
	 */
	@Deprecated
	public boolean isBindingForSomeVariable(final INodeID bindingValue) {
		return nodeBinding.inverse().containsKey(new Node(bindingValue));
	}

	public boolean isBindingForSomeVariable(final INode node) {
		return nodeBinding.inverse().containsKey(node);
	}

	/**
	 * @deprecated Use {@link #getBindingLink(EdgeID)}
	 */
	@Deprecated
	public EdgeID getLinkVariableBinding(final EdgeID edgeVariable) {
		for (final EdgeID linkVariable : this.linkBinding.keySet()) {
			if (linkVariable.equals(edgeVariable))
				return this.linkBinding.get(linkVariable).getId();
		}
		return null;
	}

	public EdgeID getBindingLink(final EdgeID linkVariable) {
		return this.linkBinding.get(linkVariable).getId();
	}

	@Deprecated
	public EdgeID getLinkVariableBinding(final String linkVariableId) {
		return this.linkBinding.get(EdgeID.get(linkVariableId)).getId();
	}

	public UniqueID getVariableBinding(UniqueID variable) {
		if (variable instanceof EdgeID)
			return getLinkVariableBinding((EdgeID) variable);
		else if (variable instanceof INodeID)
			return getNodeVariableBinding((INodeID) variable);
		return null;
	}

	public void bindLinkVariable(final EdgeID linkVariable, final EdgeID edgeId) {
		this.linkBinding.put(linkVariable, new DirectedEdge(null, null, edgeId));
	}

	public void removeAllLinkBindings() {
		this.linkBinding.clear();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((linkBinding == null) ? 0 : linkBinding.hashCode());
		result = prime * result + ((nodeBinding == null) ? 0 : nodeBinding.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final VariableAssignment other = (VariableAssignment) obj;
		if (linkBinding == null) {
			if (other.linkBinding != null) {
				return false;
			}
		} else if (!linkBinding.equals(other.linkBinding)) {
			return false;
		}
		if (nodeBinding == null) {
			if (other.nodeBinding != null) {
				return false;
			}
		} else if (!nodeBinding.equals(other.nodeBinding)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "VA [node vars:" + this.nodeBinding + ", link vars: " + this.linkBinding.toString() + "]";
	}

}
