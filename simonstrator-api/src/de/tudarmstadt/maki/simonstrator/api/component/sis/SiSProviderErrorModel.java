/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.api.component.sis;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;

/**
 * Intercepts {@link SiSInformationProvider} with a custom error model.
 * 
 * @author Nils Richerzhagen
 *
 */
public interface SiSProviderErrorModel<T> {

	/**
	 * The Type this model operates on.
	 * 
	 * @return
	 */
	public SiSType<T> getSiSType();

	/**
	 * 
	 * @param localHost
	 * @param type
	 * @param originalDataSource
	 * @return
	 */
	public SiSDataCallback<T> createErrorModelCallback(Host localHost, SiSType<T> type,
			SiSDataCallback<T> originalDataSource);

}
