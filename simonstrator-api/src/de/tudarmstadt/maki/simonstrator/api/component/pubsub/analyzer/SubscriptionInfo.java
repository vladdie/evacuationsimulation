/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer;

/**
 * An object that can be attached to a subscription by an application to enable
 * easier analyzing in simulation mode. This object is NOT transmitted to
 * brokers.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public interface SubscriptionInfo {

	/**
	 * True, if this subscription is active on the client
	 * 
	 * @return
	 */
	public boolean isActive();

	/**
	 * host-ID of the originator of this subscription
	 * 
	 * @return
	 */
	public long getOriginatorHostId();

	/**
	 * Time this subscription was created (!) by the overlay. If it is not
	 * immediately published by the app, this can not be used to calculate
	 * delays.
	 * 
	 * @return
	 */
	public long getTimestampOfCreation();

}
