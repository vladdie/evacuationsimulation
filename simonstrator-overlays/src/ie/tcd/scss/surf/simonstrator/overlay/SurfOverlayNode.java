/**
 * Author: Christian Cabrera
 * Generic class for an Overlay Node in SURF
 */

package ie.tcd.scss.surf.simonstrator.overlay;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.TCPMessageBased;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;

public abstract class SurfOverlayNode extends AbstractOverlayNode
		implements HostComponent, TransMessageListener {

	protected SurfOverlayContact sdContact;
	protected UDP udpWifi;
	protected UDP udpEthernet;
	// protected TCPMessageBased tcpWifi;
	protected TCPMessageBased tcpEthernet;
	protected NetInterfaceName netTypeWifi = NetInterfaceName.WIFI;
	protected NetInterfaceName netTypeEthernet = NetInterfaceName.ETHERNET;
	// TODO: How should the Port number be choosen?
	private final int PORT_UDP = 2403;

	private final int PORT_TCP = 2404;

	public SurfOverlayNode(Host host) {
		super(host);
	}

	@Override
	public void initialize() {
		SurfOverlayNodeId sdId = new SurfOverlayNodeId(
				getHost().getId().valueAsString().length(), getHost().getId().value());
		sdContact = new SurfOverlayContact(getHost().getId(), sdId);


		NetInterface wifi = getHost().getNetworkComponent().getByName(netTypeWifi);
		wifi.getLocalInetAddress();
		assert wifi != null : "NetInterface 'wifi' not set up properly!";

		NetInterface ethernet = getHost().getNetworkComponent().getByName(netTypeEthernet);
		ethernet.getLocalInetAddress();
		assert ethernet != null : "NetInterface 'ethernet' not set upproperly!";

		try {

			setUdpWifi(getHost().getTransportComponent().getProtocol(UDP.class, wifi.getLocalInetAddress(), PORT_UDP));
			/*
			 * setTcpWifi(getHost().getTransportComponent().getProtocol(
			 * TCPMessageBased.class, wifi.getLocalInetAddress(), PORT_TCP));
			 */

			setUdpEthernet(
					getHost().getTransportComponent().getProtocol(UDP.class, ethernet.getLocalInetAddress(), PORT_UDP));
			setTcpEthernet(getHost().getTransportComponent().getProtocol(TCPMessageBased.class,
					ethernet.getLocalInetAddress(), PORT_TCP));
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError("Need wifi net for central analysing.");
		}
		getUdpWifi().setTransportMessageListener(this);
		// getTcpWifi().setTransportMessageListener(this);
		getUdpEthernet().setTransportMessageListener(this);
		getTcpEthernet().setTransportMessageListener(this);

		sdContact.addTransInfo(netTypeWifi, getUdpWifi().getTransInfo());
		// sdContact.addTransInfo(netTypeWifi, getTcpWifi().getTransInfo());

		sdContact.addTransInfo(netTypeEthernet, getUdpEthernet().getTransInfo());
		sdContact.addTransInfo(netTypeEthernet, getTcpEthernet().getTransInfo());
	}

	@Override
	public OverlayContact getLocalOverlayContact() {
		return this.sdContact;
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// TODO Auto-generated method stub
		

	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		// TODO Auto-generated method stub

	}

	public UDP getUdpWifi() {
		return udpWifi;
	}

	public void setUdpWifi(UDP udpWifi) {
		this.udpWifi = udpWifi;
	}

	public UDP getUdpEthernet() {
		return udpEthernet;
	}

	public void setUdpEthernet(UDP udpEthernet) {
		this.udpEthernet = udpEthernet;
	}

	/*
	 * public TCPMessageBased getTcpWifi() { return tcpWifi; }
	 * 
	 * public void setTcpWifi(TCPMessageBased tcpWifi) { this.tcpWifi = tcpWifi;
	 * }
	 */

	public TCPMessageBased getTcpEthernet() {
		return tcpEthernet;
	}

	public void setTcpEthernet(TCPMessageBased tcpEthernet) {
		this.tcpEthernet = tcpEthernet;
	}

}
