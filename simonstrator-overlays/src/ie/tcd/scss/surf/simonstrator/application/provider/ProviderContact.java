/**
 * Author: Christian Cabrera
 * This class simulates a provider contact in service discovery process. 
 * A provider contact is the communication interface between providers and overlay nodes.
 * This entity helps to identify and distinct providers.
 *  
 */

package ie.tcd.scss.surf.simonstrator.application.provider;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import ie.tcd.scss.surf.simonstrator.overlay.SurfOverlayContact;

public class ProviderContact implements OverlayContact, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private INodeID nodeId;

	private ProviderId pNodeID;

	private Map<NetInterfaceName, NetID> netIds = new LinkedHashMap<>();

	private Map<NetInterfaceName, Integer> ports = new LinkedHashMap<>();

	private transient int _cachedSize = -1;

	private boolean isAlive;

	@SuppressWarnings("unused")
	private ProviderContact() {
		// for Kryo
	}

	/**
	 * Using the {@link INodeID} of the host
	 * 
	 * @param nodeId
	 */
	public ProviderContact(INodeID nodeId, ProviderId pNodeID) {
		this.nodeId = nodeId;
		this.pNodeID = pNodeID;
	}

	/**
	 * Convenience constructor contacts with only one netInterface
	 * 
	 * @param nodeID
	 * @param transInfo
	 */
	public ProviderContact(INodeID nodeId, ProviderId pNodeID, NetInterfaceName netName, NetID netId, int port) {
		this.nodeId = nodeId;
		this.pNodeID = pNodeID;
		this.netIds.put(netName, netId);
		this.ports.put(netName, port);
	}

	/**
	 * Convenience constructor contacts with only one netInterface
	 * 
	 * @param nodeID
	 * @param transInfo
	 */
	public ProviderContact(INodeID nodeId, NetInterfaceName netName, NetID netId, int port) {
		this.nodeId = nodeId;
		this.netIds.put(netName, netId);
		this.ports.put(netName, port);
	}

	/**
	 * @deprecated explicitly specify the NetInterfaceName Instead!
	 * @param nodeID
	 * @param transInfo
	 */
	@Deprecated
	public ProviderContact(INodeID nodeID, ProviderId pNodeID, TransInfo transInfo) {
		this(nodeID, pNodeID, NetInterfaceName.ETHERNET, transInfo.getNetId(), transInfo.getPort());
	}

	/**
	 * Add a new contact information to this contact. Only one info per NetName
	 * is stored - existing entries are replaced by new ones if the netName is
	 * already in use.
	 * 
	 * @param netName
	 * @param transInfo
	 * @return
	 */
	public ProviderContact addInformation(NetInterfaceName netName, NetID netId, int port) {
		this.netIds.put(netName, netId);
		this.ports.put(netName, port);
		_cachedSize = -1;
		return this;
	}

	/**
	 * Legacy support for {@link TransInfo}
	 * 
	 * @param netName
	 * @param transInfo
	 * @return
	 */
	public ProviderContact addTransInfo(NetInterfaceName netName, TransInfo transInfo) {
		this.netIds.put(netName, transInfo.getNetId());
		this.ports.put(netName, transInfo.getPort());
		_cachedSize = -1;
		return this;
	}

	/**
	 * Removes the TransInfo for the given NetName
	 * 
	 * @param netName
	 * @return
	 */
	public ProviderContact removeTransInfo(NetInterfaceName netName) {
		this.netIds.remove(netName);
		this.ports.remove(netName);
		_cachedSize = -1;
		return this;
	}

	@Override
	public int getTransmissionSize() {
		if (_cachedSize == -1) {
			_cachedSize += nodeId.getTransmissionSize();
			for (NetID netId : netIds.values()) {
				_cachedSize += netId.getTransmissionSize();
				_cachedSize += 2; // port
			}
		}
		return _cachedSize;
	}

	@Override
	public INodeID getNodeID() {
		return nodeId;
	}

	@Override
	public NetID getNetID(NetInterfaceName netInterface) {
		if (!netIds.containsKey(netInterface)) {
			return null;
		}
		return netIds.get(netInterface);
	}

	@Override
	public int getPort(NetInterfaceName netInterface) {
		if (!ports.containsKey(netInterface)) {
			return -1;
		}
		return ports.get(netInterface);
	}

	@Override
	public Collection<NetInterfaceName> getInterfaces() {
		return Collections.unmodifiableCollection(netIds.keySet());
	}

	@Override
	public String toString() {
		return "Contact " + nodeId + ": " + netIds.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
		return result;
	}

	public UniqueID getUniqueID() {
		// TODO Auto-generated method stub
		return pNodeID;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public ProviderContact clone() {
		ProviderContact contact = new ProviderContact(this.nodeId, this.pNodeID);
		Iterator<?> itIds = netIds.entrySet().iterator();
		while (itIds.hasNext()) {
			@SuppressWarnings("unchecked")
			Map.Entry<NetInterfaceName, NetID> pair = (Map.Entry<NetInterfaceName, NetID>) itIds.next();
			contact.netIds.put(pair.getKey(), pair.getValue());
		}

		Iterator<?> itPorts = ports.entrySet().iterator();
		while (itPorts.hasNext()) {
			@SuppressWarnings("unchecked")
			Map.Entry<NetInterfaceName, Integer> pair = (Map.Entry<NetInterfaceName, Integer>) itPorts.next();
			contact.ports.put(pair.getKey(), pair.getValue());
		}
		return contact;
	}

	public int getDistance(SurfOverlayContact contact) {
		return this.pNodeID.getDistance(contact.getUniqueID());

	}

	public int compareTo(ProviderContact contact) {
		return this.pNodeID.compareTo(contact.pNodeID);

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProviderContact other = (ProviderContact) obj;
		if (nodeId == null) {
			if (other.nodeId != null)
				return false;
		} else if (!nodeId.equals(other.nodeId))
			return false;
		return true;
	}

}
