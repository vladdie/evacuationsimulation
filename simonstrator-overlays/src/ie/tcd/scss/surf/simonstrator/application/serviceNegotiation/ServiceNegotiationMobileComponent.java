
package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation;

import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationListener;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.operations.InitializateGatewayOperation;

public class ServiceNegotiationMobileComponent extends ServiceNegotiationComponent
		implements LocationListener, EventHandler {

	private Location pastLocation;
	private LocationRequest locationRequest;
	private long locationUpdateInterval;
	private long locationTimeout;
	private int radio;

	public ServiceNegotiationMobileComponent(Host host) {
		super(host);
	}


	@Override
	public void initialize() {
		super.initialize();
		try {
			pastLocation = locationSensor.getLastLocation();
			locationRequest = getLocationSensor().getLocationRequest();
			locationRequest.setInterval(getLocationUpdateInterval());
			getLocationSensor().requestLocationUpdates(locationRequest, this);

			// PeriodicNeighbourCheck pnec = new PeriodicNeighbourCheck(this);
			// pnec.startWithDelay(this.getCheckNeighbourInterval());
			// PeriodicClientCheck pcc = new PeriodicClientCheck(this);
			// pcc.startWithDelay(this.getCheckClientInterval());
		} catch (Exception e) {
			throw new AssertionError("LocationSensor not available!");
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// TODO Auto-generated method stub
	}

	/**
	 * Updates routing tables according to the gateway movement. It implements
	 * different strategies for each approach
	 */
	@Override
	public void onLocationChanged(Host host, Location location) {

	}

	public long getLocationUpdateInterval() {
		return locationUpdateInterval;
	}

	public void setLocationUpdateInterval(long locationUpdateInterval) {
		this.locationUpdateInterval = locationUpdateInterval;
	}

	public long getLocationTimeout() {
		return locationTimeout;
	}

	public void setLocationTimeout(long locationTimeout) {
		this.locationTimeout = locationTimeout;
	}

	public int getRadio() {
		return radio;
	}

	public void setRadio(int radio) {
		this.radio = radio;
	}

	/**
	 * Initializes the gateway for experiments. It sets the approach according
	 * to the parameters in the actions file
	 */
	public void initializateGateway() {
		InitializateGatewayOperation igop = new InitializateGatewayOperation(this, getUdpEthernet());
		igop.start();
	}

}

