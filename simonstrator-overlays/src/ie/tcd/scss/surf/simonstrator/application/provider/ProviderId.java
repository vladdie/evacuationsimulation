/**
 * Author: Christian Cabrera
 * This class creates provider IDs
 */

package ie.tcd.scss.surf.simonstrator.application.provider;

import java.math.BigInteger;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;

public class ProviderId implements UniqueID {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * BigInt-Representation of this ID
	 */
	private BigInteger bigInt; // Represents IDs with big number as long as
								// computer RAM.

	/**
	 * Performance boost on comparisons for small IDSpaces?
	 */
	private transient boolean hasLongRepresentation;

	private transient long longRepresentation;

	private int numberOfBits;

	@SuppressWarnings("unused")
	private ProviderId() {
		// for Kryo
	}

	/**
	 * Convenience constructor for shorter IDs (below 64 bit)
	 * 
	 * @param idSpace
	 * @param bigDecimal
	 */
	public ProviderId(int numberOfBits, long value) {
		this(numberOfBits, BigInteger.valueOf(value));
	}

	/**
	 * Constructor, <b>only to be used by an IDSpace</b>! To achieve consistency
	 * in ID creation you should always use the IDSpace to create IDs!
	 * 
	 * @param idSpace
	 * @param bigDecimal
	 */
	public ProviderId(int numberOfBits, BigInteger value) {
		this.bigInt = value;
		if (numberOfBits < 64) {
			this.hasLongRepresentation = true;
			this.longRepresentation = value.longValue();
		} else {
			this.hasLongRepresentation = false;
			this.longRepresentation = Long.MIN_VALUE;
		}
		this.numberOfBits = numberOfBits;
	}

	/*
	 * ID TransmissionSize
	 */
	@Override
	public int getTransmissionSize() {
		return numberOfBits;
	}
	
	/*
	 * It returns an integer depending on two IDs comparison 
	 *  -1 this < uid
	 *   0 this == uid
	 *   1 this > uid 
	*/
	@Override
	public int compareTo(UniqueID uid) {
		if (uid instanceof ProviderId) {
			ProviderId soid = (ProviderId) uid;
			if (hasLongRepresentation && soid.hasLongRepresentation) {
				return Long.compare(longRepresentation, soid.longRepresentation);
			}
			return this.bigInt.compareTo(soid.bigInt);
		}
		if (hasLongRepresentation) {
			return Long.compare(longRepresentation, uid.value());
		} else {
			return this.bigInt.compareTo(BigInteger.valueOf(uid.value()));
		}
	}

	/*
	 * It returns the difference between two IDs (this and uid)
	 */
	public int getDistance(UniqueID uid) {
		if (uid instanceof ProviderId) {
			ProviderId soid = (ProviderId) uid;
			if (hasLongRepresentation && soid.hasLongRepresentation) {
				return (int) (longRepresentation - soid.longRepresentation);
			}
			return this.bigInt.subtract(soid.bigInt).abs().intValue();
		}
		if (hasLongRepresentation) {
			return (int) (longRepresentation - uid.value());

		} else {
			return this.bigInt.subtract(BigInteger.valueOf(uid.value())).abs().intValue();
		}
	}

	public BigInteger getBigInteger() {
		return bigInt;
	}

	@Override
	public long value() {
		return longRepresentation;
	}

	@Override
	public String valueAsString() {
		if (hasLongRepresentation) {
			return String.valueOf(longRepresentation);
		}
		return bigInt.toString();
	}

	/*
	 * It returns a boolean depending on two IDs comparison true this == uid
	 * false this != uid
	 */
	@Override
	public boolean equals(Object obj) {
		/*
		 * Two IDs are only equal, if they have the same numerical value.
		 */
		if (obj instanceof ProviderId) {
			ProviderId soid = (ProviderId) obj;
			if (this.hasLongRepresentation && soid.hasLongRepresentation) {
				return this.longRepresentation == soid.longRepresentation;
			}
			return bigInt.equals(soid.getBigInteger());
		}
		return false;
	}

	/*
	 * It returns an integer which represent an ID
	 */
	@Override
	public int hashCode() {
		if (hasLongRepresentation) {
			return Long.valueOf(longRepresentation).hashCode();
		}
		return bigInt.hashCode();
	}

	@Override
	public String toString() {
		/*
		 * As some analyzers rely on a numeric value for parsing, we just return
		 * the value and no additional information in this implementation.
		 */
		return this.bigInt.toString();
	}

}
