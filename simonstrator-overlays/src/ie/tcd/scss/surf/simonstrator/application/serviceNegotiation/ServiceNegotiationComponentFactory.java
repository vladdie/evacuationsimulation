/**
 * Author: Christian Cabrera
 * This class creates service discovery components according to the xml file values
 */

package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationComponent.ServiceNegotiationComponentType;


public class ServiceNegotiationComponentFactory implements HostComponentFactory {

	ServiceNegotiationComponentType compType;

	ServiceNegotiationParameterConfiguration config;

	@Override
	public HostComponent createComponent(Host host) {

		if (compType == null) {
			throw new AssertionError("There must be a component type specified!");
		}

		ServiceNegotiationComponent comp;

		switch (compType) {
		case MOBILE:
			comp = new ServiceNegotiationMobileComponent(host);
			comp.setGatewayType("MOBILE");
			break;
		case STATIC:
			comp = new ServiceNegotiationStaticComponent(host);
			comp.setGatewayType("STATIC");
			break;
		default:
			throw new AssertionError("Unknown component Type.");
		}
		config.configure(comp, compType);
		return comp;
	}

	/**
	 * Used by XML configuration
	 * 
	 * @param compType
	 *            Type of this component
	 */
	public void setCompType(String compType) {
		this.compType = ServiceNegotiationComponentType.valueOf(compType.toUpperCase());
	}

	/**
	 * Used by XML configuration
	 * 
	 * @param config
	 *            Configuration-Class for this factory
	 */
	public void setConfiguration(ServiceNegotiationParameterConfiguration config) {
		this.config = config;
	}

}
