package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import ie.tcd.scss.surf.simonstrator.application.consumer.Consumer;
import ie.tcd.scss.surf.simonstrator.entities.Gateway;
import ie.tcd.scss.surf.simonstrator.entities.PathFindingRequest;

public class ServiceRequestMessage extends AbstractOverlayMessage {

	private static final long serialVersionUID = 1L;
	private PathFindingRequest request;
	private Consumer source;

	public ServiceRequestMessage(Consumer sender, Gateway receiver, PathFindingRequest request) {
		super(sender.getConsumerContact(), receiver.getContact(), -1);
		this.setRequest(request);
		this.setSource(sender);
	}

	@Override
	public Message getPayload() {
		// TODO Auto-generated method stub
		return null;
	}

	public PathFindingRequest getRequest() {
		return request;
	}

	public void setRequest(PathFindingRequest request) {
		this.request = request;
	}

	public Consumer getSource() {
		return source;
	}

	public void setSource(Consumer source) {
		this.source = source;
	}



}
