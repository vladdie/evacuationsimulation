/**
 * Author: Christian Cabrera
 * This class simulate a service provider which registers services in a gateway. 
 * The provider can be mobile or static, and can performs service registration.
 * 
 */

package ie.tcd.scss.surf.simonstrator.application.provider;

import java.util.ArrayList;

import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.TCPMessageBased;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import ie.tcd.scss.surf.simonstrator.entities.Gateway;


public abstract class Provider implements EventHandler, HostComponent, TransMessageListener {

	private LocationSensor locationSensor;
	private ProviderContact pContact;
	private UDP udp;
	private TCPMessageBased tcp;
	private NetInterfaceName netTypeEthernet = NetInterfaceName.WIFI;
	// private NetInterfaceName netTypeEthernet = NetInterfaceName.ETHERNET;
	private ArrayList<Gateway> gateways;
	private Host host;

	private String providerType;

	private static int i = 0;

	// TODO: How should the Port number be choosen?
	private final int PORT_UDP = 2403;

	private final int PORT_TCP = 2404;

	public Provider(Host host) {
		this.setHost(host);
	}


	@Override
	public void initialize() {
		gateways = new ArrayList<>();
		ProviderId pId = new ProviderId(getHost().getId().valueAsString().length(),
				getHost().getId().value());
		setpContact(new ProviderContact(getHost().getId(), pId));

		NetInterface wifi = getHost().getNetworkComponent().getByName(netTypeEthernet);
		assert wifi != null : "NetInterface 'wifi' not set up properly!";

		try {
			setUdp(getHost().getTransportComponent().getProtocol(UDP.class, wifi.getLocalInetAddress(), PORT_UDP));
			setTcp(getHost().getTransportComponent().getProtocol(TCPMessageBased.class, wifi.getLocalInetAddress(),
					PORT_TCP));
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError("Need wifi net for central analysing.");
		}
		try {
			setLocationSensor(getHost().getComponent(LocationSensor.class));
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("LocationSensor not available!");
		}

		udp.setTransportMessageListener(this);
		tcp.setTransportMessageListener(this);
		getpContact().addTransInfo(netTypeEthernet, getUdp().getTransInfo());
		getpContact().addTransInfo(netTypeEthernet, getTcp().getTransInfo());

		// initNegotiationParameters();
	}





	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {


	}

	public enum ProviderType {
		/**
		 * Consumer types
		 */
		MOBILE, STATIC
	}



	public ArrayList<Gateway> getGateways() {
		return gateways;
	}

	public void setGateways(ArrayList<Gateway> gateways) {
		this.gateways = gateways;
	}

	public void addGateway(Gateway gateway) {
		this.gateways.add(gateway);
	}

	public Host getHost() {
		return host;
	}

	public void setHost(Host host) {
		this.host = host;
	}

	public String getProviderType() {
		return providerType;
	}

	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}

	public ProviderContact getpContact() {
		return pContact;
	}

	public void setpContact(ProviderContact pContact) {
		this.pContact = pContact;
	}

	public UDP getUdp() {
		return udp;
	}

	public void setUdp(UDP udp) {
		this.udp = udp;
	}

	public TCPMessageBased getTcp() {
		return tcp;
	}

	public void setTcp(TCPMessageBased tcp) {
		this.tcp = tcp;
	}



	public LocationSensor getLocationSensor() {
		return locationSensor;
	}

	public void setLocationSensor(LocationSensor locationSensor) {
		this.locationSensor = locationSensor;
	}


	/**
	 * Prints gateways list
	 */
	public void printGateways() {
		System.out.println("+++++++++Mobile Provider " + this.getpContact().getNodeID().value() + "+++++++++");
		for (int i = 0; i < this.getGateways().size(); i++) {
			System.out.println("Gateway " + this.getGateways().get(i).getTransInfo().getNetId());
			System.out.println("Gateway Location" + this.getGateways().get(i).getLocation());
			System.out.println("Provider Location" + this.getLocationSensor().getLastLocation());
			System.out.println("Distance between provider and gateway: "
					+ this.getLocationSensor().getLastLocation().distanceTo(this.getGateways().get(i).getLocation()));
		}
		System.out.println("++++++++++++++++++++++++++++++++++++ ");
	}




}
