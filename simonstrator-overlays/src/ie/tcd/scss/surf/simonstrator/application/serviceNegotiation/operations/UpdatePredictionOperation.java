package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.operations;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationComponent;

public class UpdatePredictionOperation extends PeriodicOperation<ServiceNegotiationComponent, Object> {

	ServiceNegotiationComponent component;
	private static long resultCheckInterval = 1 * Time.MINUTE;

	public UpdatePredictionOperation(ServiceNegotiationComponent component) {
		super(component, null, resultCheckInterval);
		this.component = component;
	}

	@Override
	protected void executeOnce() {


	}

	@Override
	public Object getResult() {
		// TODO Auto-generated method stub
		return null;
	}


}
