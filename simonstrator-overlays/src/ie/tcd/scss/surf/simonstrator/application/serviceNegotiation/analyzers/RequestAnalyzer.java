package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.analyzers;

public interface RequestAnalyzer {
	/**
	 * Starts the analyzer. Can be used to execute code on startup, but only
	 * after the {@link Service Discovery Component}s have been initialised.
	 */
	void start();

}
