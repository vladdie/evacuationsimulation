package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages;

import java.util.Stack;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import ie.tcd.scss.surf.simonstrator.application.consumer.Consumer;
import ie.tcd.scss.surf.simonstrator.application.consumer.ConsumerContact;
import ie.tcd.scss.surf.simonstrator.entities.Gateway;

public class PingMessage extends AbstractOverlayMessage {

	private static final long serialVersionUID = 1L;

	private String contentForConsumer = "";
	private messageType type;
	private Stack<Gateway> routeTable;
	private Gateway dstExit;
	private Gateway source;
	private int messageIndex;

	private static int msgCounter = 0;

	// copied msg. from relay to broadcast to other gateways
	public PingMessage(PingMessage msg, Gateway relay, messageType type) {
		super(msg.getSender(), null, -1);
		this.setSource(msg.getSource());
		this.setRoute(msg.getRouteTable());
		this.addRoute(relay);
		this.setType(type);
		this.messageIndex = msgCounter++;
	}

	// originating msg to find exit. broadcast from current node
	public PingMessage(Gateway sender, messageType type) {
		super(sender.getContact(), null, -1);
		this.setType(type);
		this.setSource(sender);
		this.routeTable = new Stack<Gateway>();
		this.messageIndex = msgCounter++;
	}

	// response from exit to originating node
	public PingMessage(Gateway exit, PingMessage msg,
			messageType type) {
		super(exit.getContact(), msg.getSource().getContact(), -1);
		this.setType(type);
		this.setRoute(msg.getRouteTable());
		this.setDstExit(exit);
		this.setSource(msg.getSource());
		this.messageIndex = msgCounter++;
	}

	// broadcast request from consumer
	public PingMessage(Consumer consumer, String msg, messageType type) {
		super(consumer.getConsumerContact(), null, -1);
		this.setType(type);
		this.setContentForConsumer(msg);
	}

	/**
	 * response from gateway to consumer
	 * 
	 * @return
	 */
	public PingMessage(Gateway gateway, ConsumerContact consumerContact, String msg, messageType type) {
		super(gateway.getContact(), consumerContact, -1);
		this.setType(type);
		this.setSource(gateway);
		this.setContentForConsumer(msg);
	}


	public int getMessageIndex() {
		return messageIndex;
	}

	public messageType getType() {
		return type;
	}

	public void setType(messageType type) {
		this.type = type;
	}

	public enum messageType {
		PING, PONG;
	}

	@Override
	public Message getPayload() {
		// TODO Auto-generated method stub
		return null;
	}

	public Stack<Gateway> getRouteTable() {
		return routeTable;
	}

	public void addRoute(Gateway relay) {
		this.routeTable.push(relay);
	}

	private void setRoute(Stack<Gateway> table) {
		this.routeTable = (Stack<Gateway>) table.clone();

	}

	public Gateway getDstExit() {
		return dstExit;
	}

	private void setDstExit(Gateway dstExit) {
		this.dstExit = dstExit;
	}

	public Gateway getSource() {
		return source;
	}

	public void setSource(Gateway source) {
		this.source = source;
	}

	public String getContentForConsumer() {
		return contentForConsumer;
	}

	public void setContentForConsumer(String strContent) {
		this.contentForConsumer = strContent;
	}
}
