package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import ie.tcd.scss.surf.simonstrator.entities.RequestResponse;

public class RequestResponseMessage extends AbstractOverlayMessage {

	private static final long serialVersionUID = 1L;
	private RequestResponse response;
	// private ConsumerContact requestConsumer;
	private responseMessageType type;
	// private ServiceRequestMessage requestMsg;

	public RequestResponseMessage(RequestResponse response) {
		// this.setRequestConsumer(consumer);
		this.setResponse(response);
		this.setType(responseMessageType.SUCCESS);
	}

	public RequestResponseMessage() {
		// this.setRequestMsg(requestMsg);
		// this.requestConsumer = requestMsg.getSource().getConsumerContact();
		this.setType(responseMessageType.FAIL);
	}

	public enum responseMessageType {
		SUCCESS, FAIL;
	}

	@Override
	public Message getPayload() {
		// TODO Auto-generated method stub
		return null;
	}

	public responseMessageType getType() {
		return type;
	}

	private void setType(responseMessageType type) {
		this.type = type;
	}


	public RequestResponse getResponse() {
		return response;
	}

	private void setResponse(RequestResponse response) {
		this.response = response;
	}



}
