package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import ie.tcd.scss.surf.simonstrator.overlay.SurfOverlayContact;

public class PublishExitMessage extends AbstractOverlayMessage {
	private static final long serialVersionUID = 1L;

	private String message;

	public PublishExitMessage(SurfOverlayContact sender, String message) {
		super(sender, null, -1);
		this.setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public Message getPayload() {
		// TODO Auto-generated method stub
		return null;
	}
}
