/**
 * Author: Christian Cabrera
 * This class simulates a consumer contact in service discovery process. 
 * A consumer contact is the communication interface between consumers and overlay nodes.
 * This entity helps to identify and distinct consumers.
 *  
 */

package ie.tcd.scss.surf.simonstrator.application.consumer;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;

public class ConsumerContact implements OverlayContact, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private INodeID nodeId;

	private ConsumerId consumerId;

	private Map<NetInterfaceName, NetID> netIds = new LinkedHashMap<>();

	private Map<NetInterfaceName, Integer> ports = new LinkedHashMap<>();

	private transient int _cachedSize = -1;

	private boolean isAlive;

	@SuppressWarnings("unused")
	private ConsumerContact() {
		// for Kryo
	}

	/**
	 * Using the {@link INodeID} of the host
	 * 
	 * @param nodeId
	 */
	public ConsumerContact(INodeID nodeId, ConsumerId consumerId) {
		this.nodeId = nodeId;
		this.consumerId = consumerId;
	}

	/**
	 * Convenience constructor contacts with only one netInterface
	 * 
	 * @param nodeID
	 * @param transInfo
	 */
	public ConsumerContact(INodeID nodeId, ConsumerId consumerId, NetInterfaceName netName, NetID netId, int port) {
		this.nodeId = nodeId;
		this.consumerId = consumerId;
		this.netIds.put(netName, netId);
		this.ports.put(netName, port);
	}

	/**
	 * Convenience constructor contacts with only one netInterface
	 * 
	 * @param nodeID
	 * @param transInfo
	 */
	public ConsumerContact(INodeID nodeId, NetInterfaceName netName, NetID netId, int port) {
		this.nodeId = nodeId;
		this.netIds.put(netName, netId);
		this.ports.put(netName, port);
	}

	/**
	 * @deprecated explicitly specify the NetInterfaceName Instead!
	 * @param nodeID
	 * @param transInfo
	 */
	@Deprecated
	public ConsumerContact(INodeID nodeID, ConsumerId consumerId, TransInfo transInfo) {
		this(nodeID, consumerId, NetInterfaceName.ETHERNET, transInfo.getNetId(), transInfo.getPort());
	}

	/**
	 * Add a new contact information to this contact. Only one info per NetName
	 * is stored - existing entries are replaced by new ones if the netName is
	 * already in use.
	 * 
	 * @param netName
	 * @param transInfo
	 * @return
	 */
	public ConsumerContact addInformation(NetInterfaceName netName, NetID netId, int port) {
		this.netIds.put(netName, netId);
		this.ports.put(netName, port);
		_cachedSize = -1;
		return this;
	}

	/**
	 * Legacy support for {@link TransInfo}
	 * 
	 * @param netName
	 * @param transInfo
	 * @return
	 */
	public ConsumerContact addTransInfo(NetInterfaceName netName, TransInfo transInfo) {
		this.netIds.put(netName, transInfo.getNetId());
		this.ports.put(netName, transInfo.getPort());
		_cachedSize = -1;
		return this;
	}

	/**
	 * Removes the TransInfo for the given NetName
	 * 
	 * @param netName
	 * @return
	 */
	public ConsumerContact removeTransInfo(NetInterfaceName netName) {
		this.netIds.remove(netName);
		this.ports.remove(netName);
		_cachedSize = -1;
		return this;
	}

	@Override
	public int getTransmissionSize() {
		if (_cachedSize == -1) {
			_cachedSize += nodeId.getTransmissionSize();
			for (NetID netId : netIds.values()) {
				_cachedSize += netId.getTransmissionSize();
				_cachedSize += 2; // port
			}
		}
		return _cachedSize;
	}

	@Override
	public INodeID getNodeID() {
		return nodeId;
	}

	@Override
	public NetID getNetID(NetInterfaceName netInterface) {
		if (!netIds.containsKey(netInterface)) {
			return null;
		}
		return netIds.get(netInterface);
	}

	@Override
	public int getPort(NetInterfaceName netInterface) {
		if (!ports.containsKey(netInterface)) {
			return -1;
		}
		return ports.get(netInterface);
	}

	@Override
	public Collection<NetInterfaceName> getInterfaces() {
		return Collections.unmodifiableCollection(netIds.keySet());
	}

	@Override
	public String toString() {
		return "Consumer Contact " + nodeId + ": " + netIds.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeId == null) ? 0 : nodeId.hashCode());
		return result;
	}


	public UniqueID getUniqueID() {
		// TODO Auto-generated method stub
		return nodeId;
	}

	public boolean isAlive() {
		return isAlive;
	}

	public void setAlive(boolean isAlive) {
		this.isAlive = isAlive;
	}

	public ConsumerContact clone() {
		ConsumerContact contact = new ConsumerContact(this.nodeId, this.consumerId);
		Iterator<?> itIds = netIds.entrySet().iterator();
		while (itIds.hasNext()) {
			@SuppressWarnings("unchecked")
			Map.Entry<NetInterfaceName, NetID> pair = (Map.Entry<NetInterfaceName, NetID>) itIds.next();
			contact.netIds.put(pair.getKey(), pair.getValue());
		}

		Iterator<?> itPorts = ports.entrySet().iterator();
		while (itPorts.hasNext()) {
			@SuppressWarnings("unchecked")
			Map.Entry<NetInterfaceName, Integer> pair = (Map.Entry<NetInterfaceName, Integer>) itPorts.next();
			contact.ports.put(pair.getKey(), pair.getValue());
		}
		return contact;
	}

	public int compareTo(ConsumerContact contact) {
		return this.nodeId.compareTo(contact.nodeId);

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsumerContact other = (ConsumerContact) obj;
		if (nodeId == null) {
			if (other.nodeId != null)
				return false;
		} else if (!nodeId.equals(other.nodeId))
			return false;
		return true;
	}
}
