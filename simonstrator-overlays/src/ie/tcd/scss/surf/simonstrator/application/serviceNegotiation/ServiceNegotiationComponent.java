package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation;

import java.util.ArrayList;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import ie.tcd.scss.surf.simonstrator.application.consumer.ConsumerContact;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.analyzers.RequestAnalyzer;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages.PingMessage;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages.RequestResponseMessage;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages.ServiceRequestMessage;
import ie.tcd.scss.surf.simonstrator.entities.EnvironmentParameters;
import ie.tcd.scss.surf.simonstrator.entities.EvacuationTime;
import ie.tcd.scss.surf.simonstrator.entities.Gateway;
import ie.tcd.scss.surf.simonstrator.entities.Path;
import ie.tcd.scss.surf.simonstrator.entities.PathFindingRequest;
import ie.tcd.scss.surf.simonstrator.entities.RequestResponse;
import ie.tcd.scss.surf.simonstrator.entities.Route;
import ie.tcd.scss.surf.simonstrator.entities.TimeAllocationGraph;
import ie.tcd.scss.surf.simonstrator.entities.TimeSlot;
import ie.tcd.scss.surf.simonstrator.overlay.SurfOverlayNode;



public abstract class ServiceNegotiationComponent extends SurfOverlayNode implements HostComponent, TransMessageListener {

	private long lifeTimeOut;
	private Host host;
	RequestAnalyzer requestAnalyzer;
	boolean analyzer;

	public static ArrayList<Gateway> gateways = new ArrayList<>();
	protected LocationSensor locationSensor;
	private String gatewayType;
	//private PredictionModel predictionModel;
	private static int beginIndex = 0;
	public static ArrayList<Gateway> exits = new ArrayList<Gateway>();
	protected Gateway gateway;
	protected ArrayList<Path> paths = new ArrayList<Path>();
	private final int MAX_GATEWAY_NUMBER = 5;

	private static long initialTimeStamp;
	private static boolean noRequest = true;

	private static ArrayList<TimeSlot> timeSlots = new ArrayList<TimeSlot>();

	private static ArrayList<RequestResponse> responses = new ArrayList<RequestResponse>();

	public ServiceNegotiationComponent(Host host) {
		super(host);
		this.host = host;
	}
	

	@Override
	public void initialize() {
		super.initialize();
		try {
			setLocationSensor(getHost().getComponent(LocationSensor.class));
			gateway = new Gateway(sdContact, locationSensor.getLastLocation(), beginIndex);
			gateways.add(gateway);
			beginIndex++;
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("LocationSensor not available!");
		}

		if (analyzer)
			requestAnalyzer.start();

		if (beginIndex == MAX_GATEWAY_NUMBER) {
			EnvironmentParameters.getInstance();
		}
	}

	public enum ServiceNegotiationComponentType {
		MOBILE, STATIC
	}

	public String getGatewayType() {
		return gatewayType;
	}

	public void setGatewayType(String t) {
		this.gatewayType = t;
	}

	@Override
	public void shutdown() {
		// Nothing to do
	}

	@Override
	public Host getHost() {
		return this.host;
	}


	public OverlayContact getLocalOverlayContact() {
		return sdContact;
	}

	public int getGatewayIndex() {
		return gateway.getGatewayIndex();
	}

	public Gateway getGateway() {
		return gateway;
	}

	private void processPingMessage(PingMessage im, TransInfo sender) {
		if (im.getType() == PingMessage.messageType.PING
				&& !im.getContentForConsumer().contains("Hello from consumer")) {
			if (im.getSource().equals(gateway)) {
				System.out.println(
						"Gateway_" + sdContact.getNodeID() + " ignore received ping. Reason: sender of the message");
				return;
			}
			if (im.getRouteTable().contains(gateway)) {
				System.out.println(
						"Gateway_" + sdContact.getNodeID()
								+ " ignore received ping from gateway_" + im.getRouteTable().peek().getIdentifier()
								+ ". Reason: processed the message before.");
				return;
			}

			int lastIndex;
			if (im.getRouteTable().isEmpty()) {
				lastIndex = im.getSource().getGatewayIndex();
			} else {
				lastIndex = im.getRouteTable().peek().getGatewayIndex();
			}

			if (EnvironmentParameters.Q[lastIndex][getGatewayIndex()] > 500) {
				if (im.getRouteTable().isEmpty()) {
					System.out.println("Gateway_" + sdContact.getNodeID() + " ignore received ping from gateway_"
							+ im.getSender().getNodeID() + ".  Reason: " + im.getSender().getNodeID()
							+ " is not accessible. Q=" + EnvironmentParameters.Q[lastIndex][getGatewayIndex()]
							+ ", message index: " + im.getMessageIndex());
				} else {
					System.out.println("Gateway_" + sdContact.getNodeID() + " ignore received ping from gateway_"
							+ im.getRouteTable().peek().getIdentifier() + ".  Reason: "
							+ im.getRouteTable().peek().getIdentifier()
							+ " is not accessible. Q=" + EnvironmentParameters.Q[lastIndex][getGatewayIndex()]
							+ ", message index: " + im.getMessageIndex());
				}

				return;
			} else {
				// check if I'm exit
				if (gateway.isExit()) {
					PingMessage pong = new PingMessage(gateway, im, PingMessage.messageType.PONG);
					udpEthernet.send(pong, pong.getReceiver().getNetID(netTypeEthernet),
							pong.getReceiver().getPort(netTypeEthernet));
					if (im.getRouteTable().isEmpty()) {
						System.out.println("Gateway_" + sdContact.getNodeID() + " process received ping from gateway_"
								+ im.getSender().getNodeID()
								+ ": Response with pong, I'm exit. Received message index: " + im.getMessageIndex()
								+ ", produced message index: " + pong.getMessageIndex());
					} else {
						System.out.println("Gateway_" + sdContact.getNodeID() + " process received ping from gateway_"
								+ im.getRouteTable().peek().getIdentifier()
								+ ": Response with pong, I'm exit. Received message index: " + im.getMessageIndex()
								+ ", produced message index: " + pong.getMessageIndex());
					}
				} else {
					// not exit, add as relay
					PingMessage ping = new PingMessage(im, gateway, PingMessage.messageType.PING);
					udpEthernet.send(ping, udpEthernet.getNetInterface().getBroadcastAddress(),
							udpEthernet.getLocalPort());
					if (im.getRouteTable().isEmpty()) {
						System.out.println("Gateway_" + sdContact.getNodeID() + " process received ping from gateway_"
								+ im.getSender().getNodeID() + ": Broadcast, I'm not exit. Received message index: "
								+ im.getMessageIndex() + ", produced message index: " + ping.getMessageIndex());

					} else {
						System.out.println("Gateway_" + sdContact.getNodeID() + " process received ping from gateway_"
								+ im.getRouteTable().peek().getIdentifier()
								+ ": Broadcast, I'm not exit. Received message index: " + im.getMessageIndex()
								+ ", produced message index: " + ping.getMessageIndex());
					}

				}
			}
		}
		if (im.getType() == PingMessage.messageType.PING
				&& im.getContentForConsumer().contains("Hello from consumer")) {
			PingMessage pong = new PingMessage(gateway, (ConsumerContact) im.getSender(), "Hello from gateway",
					PingMessage.messageType.PONG);
			udpWifi.send(pong, pong.getReceiver().getNetID(netTypeWifi), pong.getReceiver().getPort(netTypeWifi));
		}
		if (im.getType() == PingMessage.messageType.PONG) {
			System.out.println("gateway_" + getLocalOverlayContact().getNodeID() + " receives pong at time: "
					+ Time.getCurrentTime() / Time.MINUTE + ". Received Message index: " + im.getMessageIndex());
			// get route table
			Path path = new Path(this.getGateway(), im.getDstExit(), im.getRouteTable());
			// check if path already existed?
			paths.add(path);

		}
	}

	public String getIdentifierByIndex(int index) {
		for (int i = 0; i < gateways.size(); i++) {
			if (gateways.get(i).getGatewayIndex() == index) {
				return gateways.get(i).getIdentifier().toString();
			}
		}
		return null;
	}

	
	/**
	 * Receives messages from consumers, providers and other gateways.
	 * 
	 */
	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		if (msg.getClass() == PingMessage.class) {
			PingMessage im = (PingMessage) msg;
			this.processPingMessage(im, sender);
		}
		if (msg.getClass() == ServiceRequestMessage.class) {
			ServiceRequestMessage im = (ServiceRequestMessage) msg;
			this.processServiceRequestMessage(im);
		}

	}


	private void processServiceRequestMessage(ServiceRequestMessage im) {
		System.out.println("Gateway_" + sdContact.getNodeID() + " received request from consumer_"
				+ im.getSource().getConsumerContact().getNodeID() + " at time: " + Time.getCurrentTime() / Time.SECOND
				+ " s.");

		PathFindingRequest request = im.getRequest();
		if (noRequest) {
			noRequest = false;
			initialTimeStamp = Time.getCurrentTime() / Time.SECOND;
			System.out.println("\n *************************************************************************");
			System.out.println("initial time stamp in seconds: " + initialTimeStamp);
			System.out.println("initial time stamp in mins: " + initialTimeStamp * Time.SECOND / Time.MINUTE);
			System.out.println("*************************************************************************\n");
		}


		// get current related time stamp
		// long relatedBeginTime = Time.getCurrentTime() / Time.SECOND -
		// initialTimeStamp;
		long relatedBeginTime = 0;
		System.out.println("\nbegin time stamp of request for consumer_"
				+ im.getSource().getConsumerContact().getNodeID()
				+ " is: " + relatedBeginTime);
		request.setSubmitTimeStamp(relatedBeginTime);
		/*
		 * relatedBeginTime = 0; request.setSubmitTimeStamp(0);
		 */

		// check if I'm the exit
		if (gateway.isExit()) {
			double distance = gateway.getLocation().distanceTo(request.getCurrentLocation());
			int time = (int)(distance / im.getSource().getMovingSpeed());
			EvacuationTime et = new EvacuationTime(time, 0, null);
			RequestResponse response = new RequestResponse(et, null);
			responses.add(response);
			System.out.println("Gateway_" + sdContact.getNodeID() + " is the exit. Send response to consumer_"
					+ im.getSource().getConsumerContact().getNodeID() + " at time: "
					+ Time.getCurrentTime() / Time.SECOND + " s.");
			return;
		}

		if (paths.isEmpty()) {
			System.out.println("Gateway_" + sdContact.getNodeID() + " can not find path for consumer_"
					+ im.getSource().getConsumerContact().getNodeID() + " at time: "
					+ Time.getCurrentTime() / Time.SECOND + " s.");
			RequestResponseMessage msg = new RequestResponseMessage();

			udpWifi.send(msg, im.getSource().getConsumerContact().getNetID(netTypeWifi),
					im.getSource().getConsumerContact().getPort(netTypeWifi));
			return;
		}

		// not exit,iterate path to calculate time on each path
		ArrayList<EvacuationTime> minEvacuationTime = new ArrayList<EvacuationTime>();
		ArrayList<EvacuationTime> pathTotalTime = new ArrayList<EvacuationTime>();
		int minValue = 0;
		for (int i = 0; i < paths.size(); i++) {
			System.out.println(" path " + (i + 1) + ":  to exit " + paths.get(i).getDstExit().getContact().getNodeID()
					+ ", route is: ");
			pathTotalTime.clear();
			long routeBeginTime = relatedBeginTime;
			for (int j = 0; j < paths.get(i).getRoutes().size(); j++) {
				System.out.println("	" + paths.get(i).getRoutes().get(j).getStartNode().getContact().getNodeID()
						+ " --> " + paths.get(i).getRoutes().get(j).getEndNode().getContact().getNodeID() + ": ");
				Route route = paths.get(i).getRoutes().get(j);

				double dist = EnvironmentParameters.D[route.getStartNode().getGatewayIndex()][route.getEndNode()
						.getGatewayIndex()];
				double qValue = EnvironmentParameters.Q[route.getStartNode().getGatewayIndex()][route.getEndNode()
						.getGatewayIndex()];
				int t1 = (int) (dist / (qValue * im.getSource().getMovingSpeed()));
				long routeEndTime = routeBeginTime + (long) (t1);
				// check from t0(initial time stamp) to t1, how many people in
				// the route
				TimeSlot t = new TimeSlot(routeBeginTime, routeEndTime);
				int maxPeople1 = 0;
				// route from node 1 to node 2 is the same route from node 2 to
				// node 1
				for (int k = 0; k < route.getGraphInstance().getValues().size(); k++) {
					TimeSlot existedSlot = route.getGraphInstance().getValues().get(k).getX_timeInUnit();
					if (t.hasCommonPart(existedSlot)) {
						// numberOfPeople +=
						// route.getGraphInstance().getValues().get(k).getY_numberOfPeople();
						int currentPeople = route.getGraphInstance().getValues().get(k).getY_numberOfPeople();
						if (currentPeople > maxPeople1) {
							maxPeople1 = currentPeople;
						}
					}
				}
				Route route2;
				int maxPeople2 = 0;
				if ((route2 = EnvironmentParameters.getRouteInOppositeDirection(route)) != null) {
					for (int k = 0; k < route2.getGraphInstance().getValues().size(); k++) {
						if (t.hasCommonPart(route2.getGraphInstance().getValues().get(k).getX_timeInUnit())) {
							// numberOfPeople +=
							// route2.getGraphInstance().getValues().get(k).getY_numberOfPeople();
							int currentPeople = route2.getGraphInstance().getValues().get(k).getY_numberOfPeople();
							if (currentPeople > maxPeople2) {
								maxPeople2 = currentPeople;
							}
						}
					}
				}

				int t2 = (maxPeople1 + maxPeople2) / EnvironmentParameters.NB_j;
				// int totalTime = t1 + t2;
				EvacuationTime evat = new EvacuationTime(t1, t2, route);
				pathTotalTime.add(evat);
				routeBeginTime += (t1 + t2);
				// pathTotalTime += totalTime;// path time
				// pathTotalTime += evat.getEstimatedTime();// path time
				System.out
						.println("	 estimated time in seconds: " + (t1 + t2) + " = (computed)"
								+ t1 + " + (congestion)" + t2 + "; ");
			}

			if (minEvacuationTime.isEmpty()) {
				minEvacuationTime = (ArrayList) pathTotalTime.clone();
				for (int m = 0; m < minEvacuationTime.size(); m++) {
					minValue += minEvacuationTime.get(m).getEstimatedTime();
				}
				// System.out.println("update to min value " + minValue);
			} else {
				int currentTotalTime = 0;
				for (int m = 0; m < pathTotalTime.size(); m++) {
					currentTotalTime += pathTotalTime.get(m).getEstimatedTime();
				}
				if (currentTotalTime < minValue) {
					minEvacuationTime = (ArrayList) pathTotalTime.clone();
					minValue = currentTotalTime;
					// System.out.println("update to min value " + minValue);
				}
			}


		}
		System.out.println("\n *********************");
		System.out.println("the best path is: ");
		long t_begin = request.getSubmitTimeStamp();
		for (int m = 0; m < minEvacuationTime.size(); m++) {
			System.out.println("node " + minEvacuationTime.get(m).getRoute().getStartNode().getIdentifier()
					+ " --> node " + minEvacuationTime.get(m).getRoute().getEndNode().getIdentifier()
					+ ", estimated time in seconds: " + minEvacuationTime.get(m).getEstimatedTime() + " = (computed)"
					+ minEvacuationTime.get(m).getComputedTime() + " + (congestion)"
					+ minEvacuationTime.get(m).getCongestionTime());
			// add the time stamp to the time graph
			TimeAllocationGraph graph = minEvacuationTime.get(m).getRoute().getGraphInstance();
			graph.addSlot(new TimeSlot(t_begin, t_begin + minEvacuationTime.get(m).getEstimatedTime()));
			t_begin = t_begin + minEvacuationTime.get(m).getEstimatedTime();
		}
		// System.out.println("estimated min time: " + minEvacuationTime);

	}


	public long getLifeTimeOut() {

		return lifeTimeOut;
	}

	public void setLifeTimeOut(long lifeTimeOut) {
		this.lifeTimeOut = lifeTimeOut;
	}

	public void setRequestAnalyzer(RequestAnalyzer requestAnalyzer) {
		this.requestAnalyzer = requestAnalyzer;
	}

	public void setAnalyzer(boolean active) {
		this.analyzer = active;
	}

	public ArrayList<Gateway> getGateways() {
		return gateways;
	}

	public void setGateways(ArrayList<Gateway> gateways) {
		ServiceNegotiationComponent.gateways = gateways;
	}

	public void addGateway(Gateway gateway) {
		ServiceNegotiationComponent.gateways.add(gateway);
	}


	public LocationSensor getLocationSensor() {
		return locationSensor;
	}

	public void setLocationSensor(LocationSensor locationSensor) {
		this.locationSensor = locationSensor;
	}


}
