/**
 * Author: Christian Cabrera
 * This class simulates a mobile provider
 */

package ie.tcd.scss.surf.simonstrator.application.provider;

import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationListener;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;

public class MobileProvider extends Provider implements LocationListener, EventHandler {

	private LocationRequest locationRequest;
	private long locationUpdateInterval;
	private long locationTimeout;
	private Location pastLocation;

	public MobileProvider(Host host) {
		// TODO Auto-generated constructor stub
		super(host);
		this.setProviderType("MOBILE");
	}

	@Override
	public void initialize() {
		super.initialize();
		try {
			this.setLocationSensor(getHost().getComponent(LocationSensor.class));
			pastLocation = this.getLocationSensor().getLastLocation();
			locationRequest = this.getLocationSensor().getLocationRequest();
			locationRequest.setInterval(getLocationUpdateInterval());
			this.getLocationSensor().requestLocationUpdates(locationRequest, this);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("LocationSensor not available!");
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// TODO Auto-generated method stub

	}

	/**
	 * Updates the list of gateways according to provider location changes
	 */
	@Override
	public void onLocationChanged(Host host, Location location) {

	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	public LocationRequest getLocationRequest() {
		return locationRequest;
	}

	public void setLocationRequest(LocationRequest locationRequest) {
		this.locationRequest = locationRequest;
	}

	public long getLocationUpdateInterval() {
		return locationUpdateInterval;
	}

	public void setLocationUpdateInterval(long locationUpdateInterval) {
		this.locationUpdateInterval = locationUpdateInterval;
	}

	public long getLocationTimeout() {
		return locationTimeout;
	}

	public void setLocationTimeout(long locationTimeout) {
		this.locationTimeout = locationTimeout;
	}
}
