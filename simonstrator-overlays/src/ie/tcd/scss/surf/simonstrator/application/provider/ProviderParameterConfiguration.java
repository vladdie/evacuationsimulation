/**
 * Author: Christian Cabrera
 * This class configures the parameters for a provider
 */

package ie.tcd.scss.surf.simonstrator.application.provider;

import ie.tcd.scss.surf.simonstrator.application.provider.Provider.ProviderType;


public class ProviderParameterConfiguration {
	private long LOCATION_UPDATE_INTERVAL;
	private long LOCATION_TIMEOUT;
	private long LIFE_TIMEOUT;

	/**
	 * Called by the {@link ProviderFactory} to configure the respective
	 * {@link ProviderComponent}s.
	 * 
	 * @param comp
	 *            The component to be configured
	 * @param type
	 *            The type of the component
	 */
	public void configure(Provider comp, ProviderType type) {
		switch (type) {
		case MOBILE:
			((MobileProvider) comp).setLocationUpdateInterval(LOCATION_UPDATE_INTERVAL);
			((MobileProvider) comp).setLocationTimeout(LOCATION_TIMEOUT);
			break;
		case STATIC:
			((StaticProvider) comp).setLifeTimeOut(LIFE_TIMEOUT);
			break;
		}
	}

	public void setLocationUpdateInterval(long interval) {
		LOCATION_UPDATE_INTERVAL = interval;
	}

	public void setLocationTimeout(long timeout) {
		LOCATION_TIMEOUT = timeout;
	}

	public void setLifeTimeout(long timeout) {
		LIFE_TIMEOUT = timeout;
	}

}
