/**
 * Author: Christian Cabrera
 * This class creates consumer components according to the xml file values
 */

package ie.tcd.scss.surf.simonstrator.application.consumer;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import ie.tcd.scss.surf.simonstrator.application.consumer.Consumer.ServiceConsumerType;

public class ConsumerFactory implements HostComponentFactory {

	// Component Type to be created.
	ServiceConsumerType compType;
	ConsumerParameterConfiguration config;

	@Override
	public HostComponent createComponent(Host host) {
		// TODO Auto-generated method stub
		if (compType == null) {
			throw new AssertionError("There must be a component type specified!");
		}

		Consumer comp;

		switch (compType) {
		case MOBILE:
			comp = new Consumer(host);
			break;
		default:
			throw new AssertionError("Unknown component Type.");
		}
		config.configure(comp, compType);
		return comp;
	}

	/**
	 * Used by XML configuration
	 * 
	 * @param compType
	 *            Type of this component
	 */
	public void setCompType(String compType) {
		this.compType = ServiceConsumerType.valueOf(compType.toUpperCase());
	}

	/**
	 * Used by XML configuration
	 * 
	 * @param config
	 *            Configuration-Class for this factory
	 */
	public void setConfiguration(ConsumerParameterConfiguration config) {
		this.config = config;
	}
}
