package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.operations;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.api.operation.AbstractOperation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationComponent;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationMobileComponent;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationStaticComponent;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages.PingMessage;
import ie.tcd.scss.surf.simonstrator.overlay.SurfOverlayContact;


public class InitializateGatewayOperation
		extends AbstractOperation<ServiceNegotiationComponent, OperationCallback<Object>> {

	private OperationCallback<Object> callback;

	private final UDP udp;
	private final SurfOverlayContact localContact;
	private final NetID broadcastAddr;
	private ServiceNegotiationComponent component;
	// private int type;
	// private String gatewayType;

	public InitializateGatewayOperation(ServiceNegotiationComponent component, UDP udp,
			OperationCallback<Object> callback) {
		super(component);
		this.callback = callback;
		this.udp = udp;
		this.localContact = new SurfOverlayContact(component.getHost().getId(), udp.getNetInterface().getName(),
				udp.getTransInfo().getNetId(), udp.getTransInfo().getPort());
		this.broadcastAddr = udp.getNetInterface().getBroadcastAddress();
		this.setGateway(component);
		if (component.getClass() == ServiceNegotiationMobileComponent.class){
			ServiceNegotiationMobileComponent mobileComp = (ServiceNegotiationMobileComponent) component;
			component.setGatewayType("MOBILE");
		}
		if (component.getClass() == ServiceNegotiationStaticComponent.class){
			ServiceNegotiationStaticComponent staticComp = (ServiceNegotiationStaticComponent) component;
			component.setGatewayType("STATIC");
		}
	}

	public InitializateGatewayOperation(ServiceNegotiationComponent component, UDP udp) {
		super(component);
		this.udp = udp;
		this.localContact = new SurfOverlayContact(component.getHost().getId(), udp.getNetInterface().getName(),
				udp.getTransInfo().getNetId(), udp.getTransInfo().getPort());
		this.broadcastAddr = udp.getNetInterface().getBroadcastAddress();
		this.setGateway(component);
		if (component.getClass() == ServiceNegotiationMobileComponent.class) {
			component.setGatewayType("MOBILE");
		}
		if (component.getClass() == ServiceNegotiationStaticComponent.class) {
			component.setGatewayType("STATIC");
		}
	}

	public InitializateGatewayOperation(ServiceNegotiationComponent component, UDP udpEthernet, UDP udpWifi) {
		super(component);
		this.udp = udpEthernet;
		this.localContact = new SurfOverlayContact(component.getHost().getId(), udpEthernet.getNetInterface().getName(),
				udpEthernet.getTransInfo().getNetId(), udpEthernet.getTransInfo().getPort(),
				udpWifi.getNetInterface().getName(), udpWifi.getTransInfo().getNetId(),
				udpWifi.getTransInfo().getPort());
		this.broadcastAddr = udp.getNetInterface().getBroadcastAddress();
		this.setGateway(component);
		if (component.getClass() == ServiceNegotiationMobileComponent.class) {
			component.setGatewayType("MOBILE");
		}
		if (component.getClass() == ServiceNegotiationStaticComponent.class) {
			component.setGatewayType("STATIC");
		}
	}

	@Override
	protected void execute() {
		// TODO Auto-generated method stub
		PingMessage initializationMessage = new PingMessage(component.getGateway(), PingMessage.messageType.PING);
		udp.send(initializationMessage, udp.getNetInterface().getBroadcastAddress(), udp.getLocalPort());

		System.out.println("gateway_" + component.getLocalOverlayContact().getNodeID() + " send ping at time: "
				+ Time.getCurrentTime() / Time.MINUTE + ", message index=" + initializationMessage.getMessageIndex());

	}

	@Override
	public OperationCallback<Object> getResult() {
		// TODO Auto-generated method stub
		System.out.println("Operation Callback: " + this.callback.toString());
		return callback;
	}

	public SurfOverlayContact getLocalContact() {
		return localContact;
	}

	public NetID getBroadcastAddr() {
		return broadcastAddr;
	}

	public void start() {
		// TODO Auto-generated method stub
		this.execute();
	}

	public ServiceNegotiationComponent getGateway() {
		return component;
	}

	public void setGateway(ServiceNegotiationComponent gateway) {
		this.component = gateway;
	}
}
