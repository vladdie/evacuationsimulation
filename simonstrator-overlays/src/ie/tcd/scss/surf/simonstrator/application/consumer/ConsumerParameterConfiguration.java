/**
 * Author: Christian Cabrera
 * This class configures the parameters for a consumer
 */

package ie.tcd.scss.surf.simonstrator.application.consumer;

import ie.tcd.scss.surf.simonstrator.application.consumer.Consumer.ServiceConsumerType;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationComponent;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationComponentFactory;


public class ConsumerParameterConfiguration {
	private long LOCATION_UPDATE_INTERVAL;
	private long LOCATION_TIMEOUT;
	private long LIFE_TIMEOUT;

	/**
	 * Called by the {@link ServiceNegotiationComponentFactory} to
	 * configure the respective {@link ServiceNegotiationComponent}s.
	 * 
	 * @param comp
	 *            The component to be configured
	 * @param type
	 *            The type of the component
	 */
	public void configure(Consumer comp, ServiceConsumerType type) {
		switch (type) {
		case MOBILE:
			((Consumer) comp).setLocationUpdateInterval(LOCATION_UPDATE_INTERVAL);
			((Consumer) comp).setLocationTimeout(LOCATION_TIMEOUT);
			break;
		}
	}

	public void setLocationUpdateInterval(long interval) {
		LOCATION_UPDATE_INTERVAL = interval;
	}

	public void setLocationTimeout(long timeout) {
		LOCATION_TIMEOUT = timeout;
	}

	public long getLifeTimeout() {
		return LIFE_TIMEOUT;
	}

	public void setLifeTimeout(long lIFE_TIMEOUT) {
		LIFE_TIMEOUT = lIFE_TIMEOUT;
	}

}
