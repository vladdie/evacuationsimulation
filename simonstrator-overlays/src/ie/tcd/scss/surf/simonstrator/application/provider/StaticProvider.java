/**
 * Author: Christian Cabrera
 * This class simulates a static provider
 */
package ie.tcd.scss.surf.simonstrator.application.provider;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;

public class StaticProvider extends Provider {

	private long checkGatewaysInterval = 30 * Time.MINUTE;

	private long LIFE_TIMEOUT;

	public StaticProvider(Host host) {
		// TODO Auto-generated constructor stub
		super(host);
		this.setProviderType("STATIC");
	}

	public long getLifeTimeOut() {
		return LIFE_TIMEOUT;
	}

	public void setLifeTimeOut(long lIFE_TIMEOUT) {
		LIFE_TIMEOUT = lIFE_TIMEOUT;
	}

	/**
	 * Initializes the component and start a periodic gateway check
	 */
	@Override
	public void initialize() {
		super.initialize();
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// TODO Auto-generated method stub

	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	public long getCheckGatewaysInterval() {
		return checkGatewaysInterval;
	}

	public void setCheckGatewaysInterval(long checkGatewaysInterval) {
		this.checkGatewaysInterval = checkGatewaysInterval;
	}


}
