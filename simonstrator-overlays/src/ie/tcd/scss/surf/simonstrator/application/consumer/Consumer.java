/**
 * Author: Christian Cabrera
 * This class simulates a service consumer in service discovery process. 
 * Consumers have location, and mobility, can react to events, and can send 
 * discovery requests to overlay nodes according to their location.
 */

package ie.tcd.scss.surf.simonstrator.application.consumer;


import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationListener;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.TCPMessageBased;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages.PingMessage;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages.RequestResponseMessage;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages.RequestResponseMessage.responseMessageType;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.operations.PathFindingRequestOperation;
import ie.tcd.scss.surf.simonstrator.entities.Gateway;

public class Consumer implements LocationListener, EventHandler, HostComponent, TransMessageListener {

	private LocationSensor locationSensor;
	private LocationRequest locationRequest;

	private long locationUpdateInterval;
	private long locationTimeout;

	private final Host host;
	private ConsumerContact consumerContact;

	private UDP udp;
	private TCPMessageBased tcp;

	private NetInterfaceName netTypeWifi = NetInterfaceName.WIFI;

	private final int PORT_UDP = 2403;

	private final int PORT_TCP = 2404;

	private Gateway closeastGateway;
	private double speed;
	private PathFindingRequestOperation pfro;

	public Consumer(Host host) {
		this.host = host;
	}



	@Override
	public void initialize() {
		ConsumerId cId = new ConsumerId(getHost().getId().valueAsString().length(), getHost().getId().value());
		setConsumerContact(new ConsumerContact(getHost().getId(), cId));

		NetInterface wifi = getHost().getNetworkComponent().getByName(netTypeWifi);
		assert wifi != null : "NetInterface 'wifi' not set up properly!";

		try {
			setUdp(getHost().getTransportComponent().getProtocol(UDP.class, wifi.getLocalInetAddress(), PORT_UDP));
			setTcp(getHost().getTransportComponent().getProtocol(TCPMessageBased.class, wifi.getLocalInetAddress(),
					PORT_TCP));
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError("Need wifi net for analysing.");
		}
		udp.setTransportMessageListener(Consumer.this);
		tcp.setTransportMessageListener(Consumer.this);
		getConsumerContact().addTransInfo(netTypeWifi, getUdp().getTransInfo());
		getConsumerContact().addTransInfo(netTypeWifi, getTcp().getTransInfo());

		try {
			setLocationSensor(getHost().getComponent(LocationSensor.class));
			setLocationRequest(getLocationSensor().getLocationRequest());
			getLocationRequest().setInterval(getLocationUpdateInterval());
			getLocationSensor().requestLocationUpdates(getLocationRequest(), this);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("LocationSensor not available!");
		}

		speed = 1.5 + Math.random() * (2.5 - 1.5);
	}

	public enum ServiceConsumerType {
		MOBILE
	}

	public Gateway getCloseastGateway() {
		return closeastGateway;
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// TODO Auto-generated method stub

	}

	/**
	 * Updates the list of gateways according to consumer location changes
	 */
	@Override
	public void onLocationChanged(Host host, Location location) {

	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	@Override
	public Host getHost() {
		return this.host;
	}

	public long getLocationUpdateInterval() {
		return locationUpdateInterval;
	}

	public void setLocationUpdateInterval(long locationUpdateInterval) {
		this.locationUpdateInterval = locationUpdateInterval;
	}

	public long getLocationTimeout() {
		return locationTimeout;
	}

	public void setLocationTimeout(long locationTimeout) {
		this.locationTimeout = locationTimeout;
	}

	public LocationSensor getLocationSensor() {
		return locationSensor;
	}

	public void setLocationSensor(LocationSensor locationSensor) {
		this.locationSensor = locationSensor;
	}

	public LocationRequest getLocationRequest() {
		return locationRequest;
	}

	public void setLocationRequest(LocationRequest locationRequest) {
		this.locationRequest = locationRequest;
	}

	public ConsumerContact getConsumerContact() {
		return consumerContact;
	}

	public void setConsumerContact(ConsumerContact consumerContact) {
		this.consumerContact = consumerContact;
	}

	public TCPMessageBased getTcp() {
		return tcp;
	}

	public void setTcp(TCPMessageBased tcp) {
		this.tcp = tcp;
	}

	public UDP getUdp() {
		return udp;
	}

	public void setUdp(UDP udp) {
		this.udp = udp;
	}

	public Location getLocation() {
		return locationSensor.getLastLocation();
	}

	public void submitRequest() {
		// System.out.println("Consumer_" +
		// this.getConsumerContact().getNodeID() + " submitting requests....");
		this.getRequestOperationInstance().start();
	}

	private void processPingMessage(PingMessage im) {
		if (im.getType() == PingMessage.messageType.PONG && im.getContentForConsumer().contains("Hello from gateway")) {
			if (closeastGateway == null) {
				closeastGateway = im.getSource();
			}

			double newGatewayDist = im.getSource().getLocation().distanceTo(this.getLocation());
			double olgGatewayDist = closeastGateway.getLocation().distanceTo(this.getLocation());

			if (newGatewayDist < olgGatewayDist) {
				closeastGateway = im.getSource();
			}
		}


	}

	/**
	 * unit: m/s
	 * 
	 * @return
	 */
	public double getMovingSpeed() {
		return speed;
	}

	private PathFindingRequestOperation getRequestOperationInstance() {
		if (pfro == null) {
			pfro = new PathFindingRequestOperation(this, udp);
		}
		return pfro;
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {

		if (msg.getClass() == PingMessage.class) {
			PingMessage im = (PingMessage) msg;
			this.processPingMessage(im);
		}

		if (msg.getClass() == RequestResponseMessage.class) {
			RequestResponseMessage im = (RequestResponseMessage) msg;
			if (im.getType().equals(responseMessageType.FAIL)) {

				Event.scheduleWithDelay(1 * Time.SECOND, new EventHandler() {

					@Override
					public void eventOccurred(Object content, int type) {
						System.out.println("Consumer_" + consumerContact.getNodeID() + " re-submitting requests....");
						getRequestOperationInstance().start();

					}

				}, this, 0);
			}

		}
	}






}
