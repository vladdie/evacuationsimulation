package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.operations;

import java.util.ArrayList;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.api.operation.AbstractOperation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import ie.tcd.scss.surf.simonstrator.application.consumer.Consumer;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages.PingMessage;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.messages.ServiceRequestMessage;
import ie.tcd.scss.surf.simonstrator.entities.Gateway;
import ie.tcd.scss.surf.simonstrator.entities.PathFindingRequest;

public class PathFindingRequestOperation extends AbstractOperation<Consumer, OperationCallback<Object>> {

	private OperationCallback<Object> callback;

	private final UDP udp;
	private final Consumer component;
	private final NetID broadcastAddr;
	private NetInterfaceName netTypeWifi = NetInterfaceName.WIFI;
	// private NetInterfaceName netTypeEthernet = NetInterfaceName.ETHERNET;

	// private int type;
	// private String gatewayType;

	public PathFindingRequestOperation(Consumer component, UDP udp, OperationCallback<Object> callback) {
		super(component);
		this.callback = callback;
		this.udp = udp;

		this.broadcastAddr = udp.getNetInterface().getBroadcastAddress();
		this.component = component;

	}

	public PathFindingRequestOperation(Consumer component, UDP udp) {
		super(component);
		this.udp = udp;
		this.broadcastAddr = udp.getNetInterface().getBroadcastAddress();
		this.component = component;
	}

	public PathFindingRequestOperation(Consumer component, UDP udp, ArrayList<Gateway> unfeasibleGateways) {
		super(component);
		this.udp = udp;
		this.broadcastAddr = udp.getNetInterface().getBroadcastAddress();
		this.component = component;
	}

	@Override
	protected void execute() {
		// TODO Auto-generated method stub
		if (component.getCloseastGateway() != null) {
			PathFindingRequest request = new PathFindingRequest(component.getLocation());
			ServiceRequestMessage msg = new ServiceRequestMessage(component, component.getCloseastGateway(), request);
			udp.send(msg, msg.getReceiver().getNetID(netTypeWifi), msg.getReceiver().getPort(netTypeWifi));
		} else {
			System.out.println("Error! No gateway found!");
			// start();
		}

	}


	public void start() {
		// ping to find the most closet gateway
		PingMessage ping = new PingMessage(component, "Hello from consumer", PingMessage.messageType.PING);
		udp.send(ping, udp.getNetInterface().getBroadcastAddress(), udp.getLocalPort());
		// schedule for 100ms
		Event.scheduleWithDelay(10 * Time.MILLISECOND, new EventHandler() {

			@Override
			public void eventOccurred(Object content, int type) {
				if (component.getCloseastGateway() != null) {
					System.out.println("Consumer_" + component.getConsumerContact().getNodeID()
							+ " ready to submit request to closeast gateway: "
							+ component.getCloseastGateway().getContact().getNodeID() + " at time: "
							+ Time.getCurrentTime() / Time.SECOND + ". Distance is: "
							+ component.getCloseastGateway().getLocation().distanceTo(component.getLocation()));
					execute();
				} else {
					start();
				}

			}

		}, this, 0);
	}

	@Override
	public OperationCallback<Object> getResult() {
		// TODO Auto-generated method stub
		return null;
	}

}
