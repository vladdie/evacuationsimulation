/**
 * Author: Christian Cabrera
 * This class creates provider components according to the xml file values
 */

package ie.tcd.scss.surf.simonstrator.application.provider;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import ie.tcd.scss.surf.simonstrator.application.provider.Provider.ProviderType;

public class ProviderFactory implements HostComponentFactory {

	// Component Type to be created.
	ProviderType compType;
	ProviderParameterConfiguration config;

	@Override
	public HostComponent createComponent(Host host) {
		// TODO Auto-generated method stub
		if (compType == null) {
			throw new AssertionError("There must be a component type specified!");
		}

		Provider comp;

		switch (compType) {
		case MOBILE:
			comp = new MobileProvider(host);
			break;
		case STATIC:
			comp = new StaticProvider(host);
			break;
		default:
			throw new AssertionError("Unknown component Type.");
		}
		config.configure(comp, compType);
		return comp;
	}

	/**
	 * Used by XML configuration
	 * 
	 * @param compType
	 *            Type of this component
	 */
	public void setCompType(String compType) {
		this.compType = ProviderType.valueOf(compType.toUpperCase());
	}

	/**
	 * Used by XML configuration
	 * 
	 * @param config
	 *            Configuration-Class for this factory
	 */
	public void setConfiguration(ProviderParameterConfiguration config) {
		this.config = config;
	}
}
