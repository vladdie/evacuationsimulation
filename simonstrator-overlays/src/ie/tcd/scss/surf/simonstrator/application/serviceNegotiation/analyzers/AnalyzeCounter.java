package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.analyzers;

import java.util.ArrayList;
import java.util.HashMap;

public class AnalyzeCounter {

	public static ArrayList<String> templateAdvertisementFail = new ArrayList<String>();//

	public static ArrayList<Long> responseTimes = new ArrayList<Long>();//
	public static ArrayList<Integer> negotiationRounds = new ArrayList<Integer>();//
	// public static long registeredServices = 0;//
	public static ArrayList<String> registeredServices = new ArrayList<String>();
	public static long negotiationRequestMessageCounter = 0;//
	public static long negotiationMessageCounter = 0;//
	public static long clientInquiryMessageCounter = 0;//
	public static long templateAdvertiseMessageCounter = 0;//
	public static long configurationMessageCounter = 0;//

	public static long singleGateway = 0;

	public static ArrayList<Integer> registeredModels = new ArrayList<Integer>();

	public static ArrayList<String> gatewayList = new ArrayList<String>();
	public static HashMap<String, Integer> tempMap = new HashMap<String, Integer>();
	/*
	 * public static HashMap<Integer, Double> SPCompromise = new
	 * HashMap<Integer, Double>(); public static HashMap<Integer, Double>
	 * SCCompromise = new HashMap<Integer, Double>();
	 */

	public static HashMap<String, String> registerResultMap = new HashMap<String, String>();
	public static ArrayList<String> unRegisteredServices = new ArrayList<String>();
	
	public static long computeAverageRT() {
		if (AnalyzeCounter.responseTimes.isEmpty()) {
			return 0;
		}

		long avg = 0;
		for(int i=0; i<AnalyzeCounter.responseTimes.size();i++){
			avg += AnalyzeCounter.responseTimes.get(i);
		}
		avg = avg/AnalyzeCounter.responseTimes.size();
		return avg;
	}

	public static void incTotalServices(String templateID) {
		if (registeredServices.contains(templateID)) {
			return;
		} else {
			registeredServices.add(templateID);
		}
	}


	public static int computeAverageRounds() {
		if (AnalyzeCounter.negotiationRounds.isEmpty()) {
			return 0;
		}
		int avg = 0;
		for (int i = 0; i < AnalyzeCounter.negotiationRounds.size(); i++) {
			avg += AnalyzeCounter.negotiationRounds.get(i);
		}
		avg = avg / AnalyzeCounter.negotiationRounds.size();
		return avg;
	}


	public static void addGatewayCounter(String gwAddr) {

		boolean add = true;
		for (String iter : gatewayList) {
			if (iter.equals(gwAddr)) {
				add = false;
				break;
			}
		}
		if (add) {
			gatewayList.add(gwAddr);
			System.out.println("gateway_" + gwAddr + " add to the stack, stack size=" + gatewayList.size());
		}
	}

}
