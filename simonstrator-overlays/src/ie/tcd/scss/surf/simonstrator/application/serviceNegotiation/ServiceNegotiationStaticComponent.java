package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation;


import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.operations.InitializateGatewayOperation;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.operations.InitializeRouteOperation;
import ie.tcd.scss.surf.simonstrator.entities.EnvironmentParameters;


public class ServiceNegotiationStaticComponent extends ServiceNegotiationComponent {


	private Location location;
	private static boolean once = true;

	public ServiceNegotiationStaticComponent(Host host) {
		super(host);
	}


	/**
	 * Initializes the component and start a periodic neighbour check
	 */
	@Override
	public void initialize() {
		super.initialize();
		location = this.locationSensor.getLastLocation();

	}

	public void initializeParameters() {
		InitializeRouteOperation iro = new InitializeRouteOperation(this, getUdpEthernet());
		iro.start();
	}

	public void initializePlanningLayer() {
		if (!gateway.isExit()) {
			// InitializateGatewayOperation igop = new
			// InitializateGatewayOperation(this, getUdpEthernet());
			InitializateGatewayOperation igop = new InitializateGatewayOperation(this, getUdpEthernet(), getUdpWifi());
			igop.start();

		}

	}
	
	public void printValues() {
		if (gateway.isExit()) {
			System.out.println("gateway_" + sdContact.getNodeID() + " is exit.");
		}
		if (once) {
			for (int i = 0; i < gateways.size(); i++) {
				System.out.println(
						"gateway_" + gateways.get(i).getIdentifier() + " index:" + gateways.get(i).getGatewayIndex());
			}
			// System.out.println("Value of Q: ");
			for (int i = 0; i < EnvironmentParameters.Q.length; i++) {
				for (int j = i + 1; j < EnvironmentParameters.Q[i].length; j++) {
					// System.out.print(Q[i][j] + " ");
					if (EnvironmentParameters.Q[i][j] > 500) {
						System.out.println(i + " and " + j + " unaccessable!");
						System.out.println("gateway_" + getIdentifierByIndex(i) + " and gateway_"
								+ getIdentifierByIndex(j) + " unaccessable!");
					}
				}
				// System.out.println("");
			}
			// System.out.println("Value of D: ");
			/*
			 * for (int i = 0; i < D.length; i++) { for (int j = 0; j <
			 * D[i].length; j++) { System.out.print(D[i][j] + " "); }
			 * System.out.println(""); }
			 */
			once = false;
		}

	}

	public void printPath() {
		// System.out.println("latitutde of gateway_" + sdContact.getNodeID() +
		// ": " + location.getLatitude());
		if (gateway.isExit()) {
			System.out.println("\n Gateway_" + sdContact.getNodeID() + " is the exit!");
		} else {
			System.out.println("\n Gateway_" + sdContact.getNodeID() + ": ");
			for (int i = 0; i < paths.size(); i++) {
				System.out.print(" path " + (i + 1) + ": ");
				System.out.print("exit=" + paths.get(i).getDstExit().getContact().getNodeID() + ", route is: ");
				for (int j = 0; j < paths.get(i).getRoutes().size(); j++) {
					System.out.print(paths.get(i).getRoutes().get(j).getStartNode().getContact().getNodeID() + " --> "
							+ paths.get(i).getRoutes().get(j).getEndNode().getContact().getNodeID() + ", ");
				}
				System.out.println("");
			}
		}

	}







}
