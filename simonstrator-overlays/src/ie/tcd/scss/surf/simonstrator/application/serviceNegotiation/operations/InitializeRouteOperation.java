package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.operations;

import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.api.operation.AbstractOperation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationComponent;
import ie.tcd.scss.surf.simonstrator.overlay.SurfOverlayContact;

public class InitializeRouteOperation
		extends AbstractOperation<ServiceNegotiationComponent, OperationCallback<Object>> {
	private OperationCallback<Object> callback;

	private final UDP udp;
	private final SurfOverlayContact localContact;
	private final NetID broadcastAddr;
	private ServiceNegotiationComponent gateway;
	private static boolean initializedQD = false;
	// private int type;
	// private String gatewayType;

	public InitializeRouteOperation(ServiceNegotiationComponent component, UDP udp,
			OperationCallback<Object> callback) {
		super(component);
		this.callback = callback;
		this.udp = udp;
		this.localContact = new SurfOverlayContact(component.getHost().getId(), udp.getNetInterface().getName(),
				udp.getTransInfo().getNetId(), udp.getTransInfo().getPort());
		this.broadcastAddr = udp.getNetInterface().getBroadcastAddress();
		this.setGateway(component);

	}

	public InitializeRouteOperation(ServiceNegotiationComponent component, UDP udp) {
		super(component);
		this.udp = udp;
		this.localContact = new SurfOverlayContact(component.getHost().getId(), udp.getNetInterface().getName(),
				udp.getTransInfo().getNetId(), udp.getTransInfo().getPort());
		this.broadcastAddr = udp.getNetInterface().getBroadcastAddress();
		this.setGateway(component);

	}

	@Override
	protected void execute() {
		// TODO Auto-generated method stub

	}

	@Override
	public OperationCallback<Object> getResult() {
		// TODO Auto-generated method stub
		System.out.println("Operation Callback: " + this.callback.toString());
		return callback;
	}

	public SurfOverlayContact getLocalContact() {
		return localContact;
	}

	public NetID getBroadcastAddr() {
		return broadcastAddr;
	}

	public void start() {
		// TODO Auto-generated method stub
		this.execute();
	}

	public ServiceNegotiationComponent getGateway() {
		return gateway;
	}

	public void setGateway(ServiceNegotiationComponent gateway) {
		this.gateway = gateway;
	}
}
