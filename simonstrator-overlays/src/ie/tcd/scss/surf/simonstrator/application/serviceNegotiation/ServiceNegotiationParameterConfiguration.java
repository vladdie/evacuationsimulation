/**
 * Author: Christian Cabrera
 * This class configures the parameters for a gateway
 */

package ie.tcd.scss.surf.simonstrator.application.serviceNegotiation;

import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationComponent.ServiceNegotiationComponentType;
import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.analyzers.RequestAnalyzer;


public class ServiceNegotiationParameterConfiguration {
	private long LOCATION_UPDATE_INTERVAL;
	private long LOCATION_TIMEOUT;
	private long LIFE_TIMEOUT;
	private boolean ANALYZER;
	private RequestAnalyzer REQUEST_ANALYZER;

	/**
	 * Called by the {@link ServiceNegotiationComponentFactory} to
	 * configure the respective {@link ServiceNegotiationComponent}s.
	 * 
	 * @param comp
	 *            The component to be configured
	 * @param type
	 *            The type of the component
	 */
	public void configure(ServiceNegotiationComponent comp, ServiceNegotiationComponentType type) {
		switch (type) {
		case MOBILE:
			((ServiceNegotiationMobileComponent) comp).setLocationUpdateInterval(LOCATION_UPDATE_INTERVAL);
			((ServiceNegotiationMobileComponent) comp).setLocationTimeout(LOCATION_TIMEOUT);
			((ServiceNegotiationMobileComponent) comp).setLifeTimeOut(LIFE_TIMEOUT);
			((ServiceNegotiationMobileComponent) comp).setAnalyzer(ANALYZER);
			((ServiceNegotiationMobileComponent) comp).setRequestAnalyzer(REQUEST_ANALYZER);

			break;
		case STATIC:
			((ServiceNegotiationStaticComponent) comp).setLifeTimeOut(LIFE_TIMEOUT);
			((ServiceNegotiationStaticComponent) comp).setAnalyzer(ANALYZER);
			((ServiceNegotiationStaticComponent) comp).setRequestAnalyzer(REQUEST_ANALYZER);
			break;
		}

	}

	public void setLocationUpdateInterval(long interval) {
		LOCATION_UPDATE_INTERVAL = interval;
	}

	public void setLocationTimeout(long timeout) {
		LOCATION_TIMEOUT = timeout;
	}

	public void setLifeTimeout(long timeout) {
		LIFE_TIMEOUT = timeout;
	}

	public boolean isRequestAnalizer() {
		return ANALYZER;
	}

	public void setAnalyzer(boolean active) {
		ANALYZER = active;
	}

	public RequestAnalyzer getRequestAnalyzer() {
		return REQUEST_ANALYZER;
	}

	public void setRequestAnalyzer(RequestAnalyzer requestAnalyzer) {
		REQUEST_ANALYZER = requestAnalyzer;
	}

}
