/**
 * path is consist of a sequence of routes
 * 
 * route connects waypoints in pairs
 */
package ie.tcd.scss.surf.simonstrator.entities;

public class Route {

	private Gateway startNode;
	private Gateway endNode;

	private TimeAllocationGraph graph;

	public Route(Gateway startNode, Gateway endNode) {
		super();
		this.startNode = startNode;
		this.endNode = endNode;
	}

	public Gateway getStartNode() {
		return startNode;
	}

	public Gateway getEndNode() {
		return endNode;
	}

	public TimeAllocationGraph getGraphInstance() {
		if (graph == null) {
			graph = new TimeAllocationGraph();
		}
		return graph;
	}

}
