package ie.tcd.scss.surf.simonstrator.entities;

public class EvacuationTime {

	private int computedTime;
	private int congestionTime;
	private int estimatedTime;
	private Route route;

	public EvacuationTime(int computedTime, int predictedTime, Route route) {
		super();
		this.computedTime = computedTime;
		this.congestionTime = predictedTime;
		this.route = route;
		this.setEstimatedTime(computedTime + predictedTime);
	}

	public int getComputedTime() {
		return computedTime;
	}

	public void setComputedTime(int computedTime) {
		this.computedTime = computedTime;
	}

	public int getCongestionTime() {
		return congestionTime;
	}

	public void setCongestionTime(int predictedTime) {
		this.congestionTime = predictedTime;
	}

	public int getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(int estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

}
