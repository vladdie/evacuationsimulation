package ie.tcd.scss.surf.simonstrator.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Objects;

/**
 * the graph is per route
 * 
 * @author Fan
 *
 */

public class TimeAllocationGraph {

	private ArrayList<Coordinate> values;
	private long max_X;
	private long min_X;

	public TimeAllocationGraph() {
		values = new ArrayList<Coordinate>();
		max_X = 0;
		min_X = 0;
	}

	public void addSlot(TimeSlot timeSlot) {

		// System.out.println(" processing slot: " + timeSlot.getBeginTime() + "
		// ~ " + timeSlot.getFinishTime());
		// no value yet
		if (values.isEmpty()) {
			Coordinate c1 = new Coordinate(timeSlot, 1);
			values.add(c1);
			min_X = timeSlot.getBeginTime();
			max_X = timeSlot.getFinishTime();
		} // graph has values
		else {

			// current slot located in the end
			if (timeSlot.getBeginTime() >= max_X) {
				// add directly
				Coordinate c1 = new Coordinate(timeSlot, 1);
				values.add(c1);
				max_X = timeSlot.getFinishTime();
			} // current slot located in the start
			else if (timeSlot.getFinishTime() <= min_X) {
				// add directly
				Coordinate c1 = new Coordinate(timeSlot, 1);
				values.add(c1);
				min_X = timeSlot.getBeginTime();
			} else {
				// current slot located in the middle
				processMiddlePart(timeSlot);
				// processContain(timeSlot);
				if (timeSlot.getBeginTime() < min_X) {
					min_X = timeSlot.getBeginTime();
				}
				if (timeSlot.getFinishTime() > max_X) {
					max_X = timeSlot.getFinishTime();
				}

			}


		}

		/*
		 * for (int i = 0; i < values.size(); i++) { System.out.println(
		 * "  current values are: (" +
		 * values.get(i).getX_timeInUnit().getBeginTime() + ", " +
		 * values.get(i).getX_timeInUnit().getFinishTime() + "), " +
		 * values.get(i).getY_numberOfPeople()); } System.out.println("max=" +
		 * max_X + ", min=" + min_X);
		 */
	}

	private void processMiddlePart(TimeSlot timeSlot) {
		// (origin slot, new slot)
		HashMap<Coordinate, TimeSlot> intersec_L = new HashMap<Coordinate, TimeSlot>();
		HashMap<Coordinate, TimeSlot> intersec_H = new HashMap<Coordinate, TimeSlot>();
		HashMap<Coordinate, TimeSlot> contained_By = new HashMap<Coordinate, TimeSlot>();
		HashMap<Coordinate, TimeSlot> contains = new HashMap<Coordinate, TimeSlot>();
		ArrayList<Coordinate> modifiedBlocks = new ArrayList<Coordinate>();
		ArrayList<Coordinate> bakBlocks = new ArrayList<Coordinate>();

		for (int i = 0; i < values.size(); i++) {

			TimeSlot t_L, t_H, t_cb, t_c;
			TimeSlot existedSlot = values.get(i).getX_timeInUnit();
			// at the middle of current slot and next slot
			if (timeSlot.getBeginTime() >= existedSlot.getFinishTime()
					&& timeSlot.getFinishTime() <= values.get(i + 1).getX_timeInUnit().getBeginTime()
					&& i < values.size() - 1) {
				Coordinate em = new Coordinate(timeSlot, 1);
				bakBlocks.add(em);
			}
			// same time slot
			if (timeSlot.isSame(existedSlot)) {
				values.get(i).setY_numberOfPeople(values.get(i).getY_numberOfPeople() + 1);
			} else if ((t_L = timeSlot.intersect_L(existedSlot)) != null) {
				// get the intersected part
				intersec_L.put(values.get(i), t_L);
				values.set(i, null);

			} else if ((t_H = timeSlot.intersect_H(existedSlot)) != null) {
				intersec_H.put(values.get(i), t_H);
				values.set(i, null);
			} else if ((t_cb = timeSlot.containedBy(existedSlot)) != null) {
				// get the contained part
				contained_By.put(values.get(i), t_cb);
				values.set(i, null);
			} else {
				t_c = timeSlot.contains(existedSlot);
				if (t_c != null) {
					contains.put(values.get(i), t_c);
					values.set(i, null);

				}
			}
		}



		values.removeIf(Objects::isNull);
		for (Entry<Coordinate, TimeSlot> entry : intersec_L.entrySet()) {
			// System.out.println("Key : " + entry.getKey() + " Value : " +
			// entry.getValue());
			Coordinate slotCord = entry.getKey();
			int y = slotCord.getY_numberOfPeople() + 1;
			Coordinate c1 = new Coordinate(entry.getValue(), y);
			// values.add(c1);
			modifiedBlocks.add(c1);
			TimeSlot t2 = new TimeSlot(slotCord.getX_timeInUnit().getFinishTime(), timeSlot.getFinishTime());
			Coordinate c2 = new Coordinate(t2, 1);
			// values.add(c2);
			bakBlocks.add(c2);
			TimeSlot t3 = new TimeSlot(slotCord.getX_timeInUnit().getBeginTime(), timeSlot.getBeginTime());
			Coordinate c3 = new Coordinate(t3, slotCord.getY_numberOfPeople());
			// values.add(c3);
			bakBlocks.add(c3);

		}

		for (Entry<Coordinate, TimeSlot> entry : intersec_H.entrySet()) {
			Coordinate slotCord = entry.getKey();
			int y = slotCord.getY_numberOfPeople() + 1;
			Coordinate c1 = new Coordinate(entry.getValue(), y);
			// values.add(c1);
			modifiedBlocks.add(c1);
			TimeSlot t2 = new TimeSlot(timeSlot.getBeginTime(), slotCord.getX_timeInUnit().getBeginTime());
			Coordinate c2 = new Coordinate(t2, 1);
			// values.add(c2);
			bakBlocks.add(c2);
			TimeSlot t3 = new TimeSlot(timeSlot.getFinishTime(), slotCord.getX_timeInUnit().getFinishTime());
			Coordinate c3 = new Coordinate(t3, slotCord.getY_numberOfPeople());
			// values.add(c3);
			bakBlocks.add(c3);

		}

		for (Entry<Coordinate, TimeSlot> entry : contained_By.entrySet()) {
			Coordinate slotCord = entry.getKey();
			int y = slotCord.getY_numberOfPeople() + 1;
			Coordinate c1 = new Coordinate(entry.getValue(), y);
			// values.add(c1);
			modifiedBlocks.add(c1);
			if (slotCord.getX_timeInUnit().getBeginTime() != timeSlot.getBeginTime()) {
				TimeSlot t2 = new TimeSlot(slotCord.getX_timeInUnit().getBeginTime(), timeSlot.getBeginTime());
				Coordinate c2 = new Coordinate(t2, slotCord.getY_numberOfPeople());
				// values.add(c2);
				bakBlocks.add(c2);
			}
			if (timeSlot.getFinishTime() != slotCord.getX_timeInUnit().getFinishTime()) {
				TimeSlot t3 = new TimeSlot(timeSlot.getFinishTime(), slotCord.getX_timeInUnit().getFinishTime());
				Coordinate c3 = new Coordinate(t3, slotCord.getY_numberOfPeople());
				// values.add(c3);
				bakBlocks.add(c3);
			}

		}

		for (Entry<Coordinate, TimeSlot> entry : contains.entrySet()) {
			Coordinate slotCord = entry.getKey();
			int y = slotCord.getY_numberOfPeople() + 1;
			Coordinate c1 = new Coordinate(entry.getValue(), y);
			modifiedBlocks.add(c1);
			// values.add(c1);
			if (timeSlot.getBeginTime() != slotCord.getX_timeInUnit().getBeginTime()) {
				TimeSlot t2 = new TimeSlot(timeSlot.getBeginTime(), slotCord.getX_timeInUnit().getBeginTime());
				Coordinate c2 = new Coordinate(t2, 1);
				// values.add(c2);
				bakBlocks.add(c2);
			}
			if (slotCord.getX_timeInUnit().getFinishTime() != timeSlot.getFinishTime()) {
				TimeSlot t3 = new TimeSlot(slotCord.getX_timeInUnit().getFinishTime(), timeSlot.getFinishTime());
				Coordinate c3 = new Coordinate(t3, 1);
				// values.add(c3);
				bakBlocks.add(c3);
			}
		}

		Collections.sort(modifiedBlocks);
		Collections.sort(bakBlocks);

		for (int i = 0; i < bakBlocks.size(); i++) {
			for (int j = 0; j < modifiedBlocks.size(); j++) {
				/*
				 * System.out.println("\ni=" + i + ": begin=" +
				 * bakBlocks.get(i).getX_timeInUnit().getBeginTime() + ", end="
				 * + bakBlocks.get(i).getX_timeInUnit().getFinishTime());
				 * System.out.println("j=" + j + ": begin=" +
				 * modifiedBlocks.get(j).getX_timeInUnit().getBeginTime() +
				 * ", end=" +
				 * modifiedBlocks.get(j).getX_timeInUnit().getFinishTime());
				 */

				TimeSlot temp = bakBlocks.get(i).getX_timeInUnit();
				if (!temp.hasCommonPart(modifiedBlocks.get(j).getX_timeInUnit())) {
					// System.out.println(" no interception");
					continue;
				}
				// is same?
				if (temp.isSame(modifiedBlocks.get(j).getX_timeInUnit())) {
					bakBlocks.remove(i);
					// System.out.println(" same slot, removed");
					i--;
					break;
				}
				// is contained by?
				if (temp.containedBy(modifiedBlocks.get(j).getX_timeInUnit()) != null) {
					bakBlocks.remove(i);
					// System.out.println(" contained slot, removed");
					i--;
					break;
				}
				// is intercepted?
				ArrayList<TimeSlot> remains = temp.getDisjointedPart(modifiedBlocks.get(j).getX_timeInUnit());
				if (!remains.isEmpty()) {
					int y = bakBlocks.get(i).getY_numberOfPeople();
					if (remains.size() == 1) {// replaced by one
						bakBlocks.set(i, new Coordinate(remains.get(0), y));
						/*
						 * System.out.println("    replaced by: begin=" +
						 * bakBlocks.get(i).getX_timeInUnit().getBeginTime() +
						 * " ,end=" +
						 * bakBlocks.get(i).getX_timeInUnit().getFinishTime());
						 */
						continue;
					} else {// replaced by multiple
						for (int n = 0; n < remains.size(); n++) {
							bakBlocks.add(new Coordinate(remains.get(n), y));
							/*
							 * System.out.println("    add to end: begin=" +
							 * remains.get(n).getBeginTime() + " ,end=" +
							 * remains.get(n).getFinishTime());
							 */
						}
						bakBlocks.remove(i);
						// System.out.println(" removed origin");
						i--;
						continue;
					}

				}
			}
		}

		// remove duplicated
		for (int i = 0; i < bakBlocks.size(); i++) {
			for (int j = i + 1; j < bakBlocks.size(); j++) {
				if (bakBlocks.get(i).getX_timeInUnit().isSame(bakBlocks.get(j).getX_timeInUnit())) {
					bakBlocks.remove(j);
					j--;
				}
			}
		}

		// add to values
		for (int i = 0; i < bakBlocks.size(); i++) {
			values.add(bakBlocks.get(i));
		}
		for (int j = 0; j < modifiedBlocks.size(); j++) {
			values.add(modifiedBlocks.get(j));
		}
		Collections.sort(values);
		mergeCoordinates(values);


	}

	public void mergeCoordinates(ArrayList<Coordinate> t) {
		for (int i = 0; i < t.size(); i++) {
			for (int j = i; j < t.size(); j++) {
				Coordinate merged = t.get(i).merges(t.get(j));
				if (merged != null) {
					t.set(i, merged);
					t.remove(j);
					j--;
				}
			}
		}
	}


	public ArrayList<Coordinate> getValues() {
		return values;
	}

}
