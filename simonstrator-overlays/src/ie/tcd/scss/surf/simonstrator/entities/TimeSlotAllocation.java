package ie.tcd.scss.surf.simonstrator.entities;

public class TimeSlotAllocation {

	private TimeSlot allocatedSlot;
	private Route route;

	public TimeSlotAllocation(TimeSlot allocatedSlot, Route route) {
		super();
		setAllocatedSlot(allocatedSlot);
		setRoute(route);
	}

	public TimeSlot getAllocatedSlot() {
		return allocatedSlot;
	}

	private void setAllocatedSlot(TimeSlot allocatedSlot) {
		this.allocatedSlot = allocatedSlot;
	}

	public Route getRoute() {
		return route;
	}

	private void setRoute(Route route) {
		this.route = route;
	}

}
