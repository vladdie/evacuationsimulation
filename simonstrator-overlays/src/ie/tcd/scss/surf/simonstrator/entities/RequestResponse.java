package ie.tcd.scss.surf.simonstrator.entities;

public class RequestResponse {

	private EvacuationTime estimatedTime;
	private Path path;

	public RequestResponse(EvacuationTime estimatedTime, Path path) {
		super();
		this.estimatedTime = estimatedTime;
		this.path = path;
	}

	public EvacuationTime getEstimatedTime() {
		return estimatedTime;
	}

	public void setEstimatedTime(EvacuationTime estimatedTime) {
		this.estimatedTime = estimatedTime;
	}

	public Path getPath() {
		return path;
	}

	public void setPath(Path path) {
		this.path = path;
	}



}
