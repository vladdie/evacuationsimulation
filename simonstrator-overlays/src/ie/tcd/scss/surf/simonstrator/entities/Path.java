package ie.tcd.scss.surf.simonstrator.entities;

import java.util.ArrayList;
import java.util.Stack;

public class Path {
	private Gateway dstExit;
	// private ArrayList<Gateway> waypoints;
	private ArrayList<Route> routes;

	public Path(Gateway currentGateway, Gateway dstExit, Stack<Gateway> routeTable) {
		super();
		this.dstExit = dstExit;
		/*
		 * this.waypoints = new ArrayList<Gateway>(); for (int i = 0; i <
		 * routeTable.size(); i++) { this.waypoints.add(routeTable.get(i)); }
		 */
		this.dstExit = dstExit;
		this.routes = new ArrayList<Route>();
		if (routeTable.isEmpty()) {
			// Route route = new Route(currentGateway, dstExit);
			Route route = EnvironmentParameters.getRouteInstance(currentGateway, dstExit);
			this.routes.add(route);
		} else {
			// Route route = new Route(currentGateway, routeTable.get(0));
			Route route = EnvironmentParameters.getRouteInstance(currentGateway, routeTable.get(0));
			this.routes.add(route);
			if (routeTable.size() > 1) {
				for (int i = 0; i < routeTable.size() - 1; i++) {
					// route = new Route(routeTable.get(i), routeTable.get(i +
					// 1));
					route = EnvironmentParameters.getRouteInstance(routeTable.get(i), routeTable.get(i + 1));
					this.routes.add(route);
				}
			}
			// route = new Route(routeTable.get(routeTable.size() - 1),
			// dstExit);
			route = EnvironmentParameters.getRouteInstance(routeTable.get(routeTable.size() - 1), dstExit);
			this.routes.add(route);
		}

	}

	/*
	 * public ArrayList<Gateway> getWayPoints() { return waypoints; }
	 */

	public Gateway getDstExit() {
		return dstExit;
	}

	public void routesInPath(Gateway dstExit, Stack<Gateway> routeTable) {
		this.dstExit = dstExit;
		this.routes = new ArrayList<Route>();
		for (int i = 0; i < routeTable.size(); i++) {
			Route route = new Route(routeTable.get(i), routeTable.get(i + 1));
			this.routes.add(route);
		}
	}

	public ArrayList<Route> getRoutes() {
		return routes;
	}


}
