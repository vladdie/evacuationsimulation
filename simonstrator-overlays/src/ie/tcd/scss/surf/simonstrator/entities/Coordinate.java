package ie.tcd.scss.surf.simonstrator.entities;

import java.util.ArrayList;
import java.util.Collections;

public class Coordinate implements Comparable<Coordinate> {
	
	private TimeSlot X_timeInUnit;
	private int Y_numberOfPeople;
	
	
	public Coordinate(TimeSlot slot, int numberOfPeople) {
		this.setX_timeInUnit(slot);
		this.setY_numberOfPeople(numberOfPeople);
	}


	public TimeSlot getX_timeInUnit() {
		return X_timeInUnit;
	}

	public void setX_timeInUnit(TimeSlot x_timeInUnit) {
		X_timeInUnit = x_timeInUnit;
	}

	public int getY_numberOfPeople() {
		return Y_numberOfPeople;
	}

	public void setY_numberOfPeople(int y_numberOfPeople) {
		Y_numberOfPeople = y_numberOfPeople;
	}


	/**
	 * merge two coordinates into 1 eg: intput: time slot=(5,8), y=1, time
	 * slot=(8,11), y=1, output: time slot =(5,11), y=1
	 * 
	 * @param c1
	 * @param c2
	 */
	public Coordinate merges(Coordinate c) {
		if (this.getY_numberOfPeople() == c.Y_numberOfPeople) {
			if (c.getX_timeInUnit().getFinishTime() == this.getX_timeInUnit().getBeginTime()) {
				TimeSlot t = new TimeSlot(c.getX_timeInUnit().getBeginTime(), this.getX_timeInUnit().getFinishTime());
				Coordinate nc = new Coordinate(t, this.getY_numberOfPeople());
				return nc;
			}
			if (this.getX_timeInUnit().getFinishTime() == c.getX_timeInUnit().getBeginTime()) {
				TimeSlot t = new TimeSlot(this.getX_timeInUnit().getBeginTime(), c.getX_timeInUnit().getFinishTime());
				Coordinate nc = new Coordinate(t, this.getY_numberOfPeople());
				return nc;
			}
		}
		return null;
	}

	@Override
	public int compareTo(Coordinate comparedCord) {
		// TODO Auto-generated method stub
		int result = (int) (this.getX_timeInUnit().getBeginTime() - comparedCord.getX_timeInUnit().getBeginTime());
		return result;
	}

	public static void main(String[] args) {
		ArrayList<Coordinate> t = new ArrayList<Coordinate>();
		for (int i = 0; i < 5; i++) {
			long begin;
			if (i % 2 == 0) {
				begin = (long) (i + 1000);
			} else {
				begin = (long) (i + 10);
			}
			TimeSlot ts = new TimeSlot(begin, begin + 5);
			Coordinate c = new Coordinate(ts, 1);
			t.add(c);
		}
		for (int i = 0; i < t.size(); i++) {
			System.out.println("begin=" + t.get(i).getX_timeInUnit().getBeginTime() + ", end="
					+ t.get(i).getX_timeInUnit().getFinishTime());
		}
		Collections.sort(t);
		System.out.println("\n ");
		for (int i = 0; i < t.size(); i++) {
			System.out.println("begin=" + t.get(i).getX_timeInUnit().getBeginTime() + ", end="
					+ t.get(i).getX_timeInUnit().getFinishTime());
		}
	}

}
