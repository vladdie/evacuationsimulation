package ie.tcd.scss.surf.simonstrator.entities;

import java.util.ArrayList;

import ie.tcd.scss.surf.simonstrator.application.serviceNegotiation.ServiceNegotiationComponent;

public class EnvironmentParameters {
	public static double[][] Q;
	public static double[][] D;
	private static EnvironmentParameters environmentParameters;
	private int nodeNumber;
	private static final int EXIT_NUMBER = 1;
	public static ArrayList<Route> aviailableRoutes = new ArrayList<Route>();

	public static final int NB_j = 1;

	public EnvironmentParameters() {
		nodeNumber = ServiceNegotiationComponent.gateways.size();
		Q = new double[nodeNumber][nodeNumber];
		D = new double[nodeNumber][nodeNumber];
		initializeParameters();
	}

	public static EnvironmentParameters getInstance() {
		if (environmentParameters == null) {
			environmentParameters = new EnvironmentParameters();
		}
		return environmentParameters;
	}

	public static Route getRouteInstance(Gateway node1, Gateway node2) {
		for (int i = 0; i < aviailableRoutes.size(); i++) {
			if (aviailableRoutes.get(i).getStartNode().getIdentifier().toString()
					.equals(node1.getIdentifier().toString())
					&& aviailableRoutes.get(i).getEndNode().getIdentifier().toString()
							.equals(node2.getIdentifier().toString())) {
				return aviailableRoutes.get(i);
			}
		}
		Route route = new Route(node1, node2);
		aviailableRoutes.add(route);
		return route;
	}

	public static Route getRouteInOppositeDirection(Route route) {
		for (int i = 0; i < aviailableRoutes.size(); i++) {
			if (aviailableRoutes.get(i).getStartNode().getIdentifier().toString()
					.equals(route.getEndNode().getIdentifier().toString())
					&& aviailableRoutes.get(i).getEndNode().getIdentifier().toString()
							.equals(route.getStartNode().getIdentifier().toString())) {
				return aviailableRoutes.get(i);
			}
		}
		return null;
	}

	private void initializeParameters() {
		// initialize D based on distance between g_i and g_j
		for (int i = 0; i < nodeNumber; i++) {
			for (int j = i + 1; j < nodeNumber; j++) {
				double distance = ServiceNegotiationComponent.gateways.get(j).getLocation()
						.distanceTo(ServiceNegotiationComponent.gateways.get(i).getLocation());
				D[i][j] = distance;
				D[j][i] = distance;
			}
		}

		// assign exits
		for (int i = 0; i < EXIT_NUMBER; i++) {
			int r;
			do {
				r = (int) (nodeNumber * Math.random());
			} while (ServiceNegotiationComponent.gateways.get(r).isExit());
			// System.out.println("exit index: " + r);
			ServiceNegotiationComponent.gateways.get(r).setExit(true);
		}

		// simulate terrain by random value to initialize Q
		for (int i = 0; i < nodeNumber; i++) {
			for (int j = i + 1; j < nodeNumber; j++) {
				if (ServiceNegotiationComponent.gateways.get(i).isExit()
						&& ServiceNegotiationComponent.gateways.get(j).isExit()) {
					Q[i][j] = 1000;
					Q[j][i] = 1000;
					continue;
				}
				// possibility=0.3: unaccessible; 0.35: uphill, 0.35: downhill
				double r = Math.random();
				if (r < 0.5) {
					Q[i][j] = 1000;
					Q[j][i] = 1000;
				} else if (r < 0.75) {
					double uphill = 2 + 3 * Math.random();
					Q[i][j] = 10 + uphill;
					Q[j][i] = 10 - uphill;
				} else {
					double downhill = 2 + 3 * Math.random();
					Q[i][j] = 10 - downhill;
					Q[j][i] = 10 + downhill;
				}

			}
		}

	}


}
