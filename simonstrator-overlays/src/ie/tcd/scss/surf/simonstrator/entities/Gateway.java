package ie.tcd.scss.surf.simonstrator.entities;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import ie.tcd.scss.surf.simonstrator.overlay.SurfOverlayContact;

public class Gateway {

	private SurfOverlayContact contact;
	private TransInfo transInfo;
	private Location location;
	private String type;
	private int gatewayIndex;

	protected boolean exit = false;

	public Gateway() {
		contact = null;
		transInfo = null;
		location = null;
		type = "";
	}

	public Gateway(SurfOverlayContact contact, TransInfo transInfo, Location location, String type) {
		this.setContact(contact);
		this.setLocation(location);
		this.setTransInfo(transInfo);
		this.setType(type);
	}

	public Gateway(SurfOverlayContact contact, Location location, int index) {
		this.setContact(contact);
		this.setLocation(location);
		this.setGatewayIndex(index);
	}


	public TransInfo getTransInfo() {
		return transInfo;
	}

	public void setTransInfo(TransInfo transInfo) {
		this.transInfo = transInfo;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public SurfOverlayContact getContact() {
		return contact;
	}

	public void setContact(SurfOverlayContact contact) {
		this.contact = contact;
	}

	public INodeID getIdentifier() {
		return contact.getNodeID();
	}

	public int getGatewayIndex() {
		return gatewayIndex;
	}

	private void setGatewayIndex(int gatewayIndex) {
		this.gatewayIndex = gatewayIndex;
	}

	public boolean isExit() {
		return exit;
	}

	public void setExit(boolean exit) {
		this.exit = exit;
	}

}
