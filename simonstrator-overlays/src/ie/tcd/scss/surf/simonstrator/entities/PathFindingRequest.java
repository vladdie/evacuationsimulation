package ie.tcd.scss.surf.simonstrator.entities;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

public class PathFindingRequest {

	private Location currentLocation;
	private long submitTimeStamp;// the related time gateway received the
									// request
	// private HashMap<Route, TimeSlot> timeSlot;// time slot collection for the
	// request

	public PathFindingRequest(Location currentLocation) {
		super();
		setCurrentLocation(currentLocation);
		// setSubmitTimeStamp(submitTimeStamp);
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	private void setCurrentLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}

	public long getSubmitTimeStamp() {
		return submitTimeStamp;
	}

	public void setSubmitTimeStamp(long submitTimeStamp) {
		this.submitTimeStamp = submitTimeStamp;
	}


}
