/**
 * Time stamp unit: second
 * 
 * Time is measured by related time to initial time stamp
 */
package ie.tcd.scss.surf.simonstrator.entities;

import java.util.ArrayList;

public class TimeSlot {

	private long beginTimeStamp;
	private long finishTimeStamp;

	public TimeSlot(long begin, long end) {
		if (begin < end) {
			this.beginTimeStamp = begin;
			this.finishTimeStamp = end;
		} else {
			this.beginTimeStamp = end;
			this.finishTimeStamp = begin;
		}
		if (begin == end) {
			System.out.println("ERROR! illegal time slot!!!!");
		}
	}

	public long getBeginTime() {
		return beginTimeStamp;
	}

	public long getFinishTime() {
		return finishTimeStamp;
	}

	/**
	 * check if the slot's lower part interacts with another
	 * 
	 * @param slot
	 * @return the interacted part
	 */
	public TimeSlot intersect_L(TimeSlot slot) {
		// boolean intersected = false;
		if (this.getBeginTime() < slot.getFinishTime() && this.getBeginTime() > slot.getBeginTime()
				&& this.getFinishTime() > slot.getFinishTime()) {
			TimeSlot t = new TimeSlot(this.getBeginTime(), slot.getFinishTime());
			return t;
		}
		return null;
	}

	/**
	 * check if the slot's higher part interacts with another
	 * 
	 * @param slot
	 * @return the interacted part
	 */
	public TimeSlot intersect_H(TimeSlot slot) {
		if (this.getFinishTime() < slot.getFinishTime() && this.getFinishTime() > slot.getBeginTime()
				&& this.getBeginTime() < slot.getBeginTime()) {
			TimeSlot t = new TimeSlot(slot.getBeginTime(), this.getFinishTime());
			return t;
		}
		return null;
	}

	/**
	 * check if the slot is contained by another
	 * 
	 * @param slot
	 * @return contained part
	 */
	public TimeSlot containedBy(TimeSlot slot) {
		if ((this.getBeginTime() >= slot.getBeginTime() && this.getFinishTime() < slot.getFinishTime())
				|| (this.getBeginTime() > slot.getBeginTime() && this.getFinishTime() <= slot.getFinishTime())) {
			return this;
		}

		return null;
	}

	/**
	 * check if the slot contains another
	 * 
	 * @param slot
	 * @return contained part
	 */
	public TimeSlot contains(TimeSlot slot) {

		if ((this.getBeginTime() <= slot.getBeginTime() && this.getFinishTime() > slot.getFinishTime())
				|| (this.getBeginTime() < slot.getBeginTime() && this.getFinishTime() >= slot.getFinishTime())) {
			return slot;
		}
		return null;
	}

	public boolean isSame(TimeSlot slot) {

		if (this.getBeginTime() == slot.getBeginTime() && this.getFinishTime() == slot.getFinishTime()) {
			return true;
		}
		return false;
	}

	public boolean hasCommonPart(TimeSlot slot) {
		if (this.getBeginTime() >= slot.getFinishTime() || this.getFinishTime() <= slot.getBeginTime()) {
			return false;
		}

		return true;
	}

	/**
	 * get the disjoint part of two time slots
	 * 
	 * @param slot
	 * @return
	 */
	public ArrayList<TimeSlot> getDisjointedPart(TimeSlot slot) {
		ArrayList<TimeSlot> disjoints = new ArrayList<TimeSlot>();
		// lower part intercept
		if (this.getBeginTime() < slot.getFinishTime() && this.getBeginTime() > slot.getBeginTime()
				&& this.getFinishTime() > slot.getFinishTime()) {
			TimeSlot disjoint = new TimeSlot(slot.getFinishTime(), this.getFinishTime());
			disjoints.add(disjoint);
			// return disjoints;
		}
		// higher part intercept
		if (this.getFinishTime() < slot.getFinishTime() && this.getFinishTime() > slot.getBeginTime()
				&& this.getBeginTime() < slot.getBeginTime()) {
			TimeSlot disjoint = new TimeSlot(this.getBeginTime(), slot.getBeginTime());
			disjoints.add(disjoint);
			// return disjoints;
		}
		// contains another slot
		if ((this.getBeginTime() <= slot.getBeginTime() && this.getFinishTime() > slot.getFinishTime())
				|| (this.getBeginTime() < slot.getBeginTime() && this.getFinishTime() >= slot.getFinishTime())) {
			if (this.getBeginTime() != slot.getBeginTime()) {
				TimeSlot disjoint = new TimeSlot(this.getBeginTime(), slot.getBeginTime());
				disjoints.add(disjoint);
			}
			if (this.getFinishTime() != slot.getFinishTime()) {
				TimeSlot disjoint = new TimeSlot(slot.getFinishTime(), this.getFinishTime());
				disjoints.add(disjoint);
			}
			// return disjoints;
		}
		// be contained by another slot-not considered

		return disjoints;
	}

}
