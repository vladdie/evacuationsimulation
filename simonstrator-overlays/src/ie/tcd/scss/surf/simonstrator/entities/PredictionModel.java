package ie.tcd.scss.surf.simonstrator.entities;

public class PredictionModel {

	// vector from gateway i to other gateways, g_i to g_i is recorded as 0
	private double[] D_vector;
	private double[] Q_vector;


	public PredictionModel(double[] distanceVec, double[] QVec) {
		super();
		this.D_vector = distanceVec;
		this.Q_vector = QVec;
	}

	public double computePredictedTime(int dstIndex, double vk, int pk, int n, double bj) {
		// get d_max from vector
		double D_max = D_vector[dstIndex];
		double Q_j = Q_vector[dstIndex];
		return D_max / (Q_j * vk) + pk / (n * bj);
	}


}
