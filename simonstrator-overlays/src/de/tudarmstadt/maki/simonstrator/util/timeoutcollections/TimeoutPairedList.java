package de.tudarmstadt.maki.simonstrator.util.timeoutcollections;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import de.tudarmstadt.maki.simonstrator.api.Time;

/**
 * Inspired by {@link TimeoutMap}.
 * <p>
 * A list of Pairs where elements 'run out' after a certain amount of time and get
 * thrown out of the collection. A Pair (added via addNow(k,v)) is removed from this list
 * after a defined timeout since it is added. Essentially like a {@link TimeoutMap} although there can be multiple
 * entries with the same key.
 * <br><br>
 * You can add listeners to this set that announce if elements were removed from
 * this set during the cleanup process.
 *
 * @param <K> Type of the keys
 * @param <V> Type of the values
 * @author Clemens Krug
 */
public class TimeoutPairedList<K, V> implements Cloneable, Iterable<Map.Entry<K, V>>
{
	private List<AbstractMap.SimpleEntry<K, V>> elements = new LinkedList<>();

    private Queue<TimeoutObject> timeQ = new LinkedBlockingQueue<>();

    private LinkedList<TimeoutPairedListListener> listeners = new LinkedList<>();

    long timeout;

    public TimeoutPairedList(long timeout)
    {
        this.timeout = timeout;
    }

    public void addNow(K key, V value)
    {
        cleanup();
        long time = getCurrentTime();

        if (containsPair(key, value))
			timeQ.remove(new TimeoutObject(new AbstractMap.SimpleEntry<>(key, value), 0));
        else
			elements.add(new AbstractMap.SimpleEntry<>(key, value));

		timeQ.add(new TimeoutObject(new AbstractMap.SimpleEntry<K, V>(key, value), time + timeout));
        assertSet();
    }

	public void addNow(AbstractMap.SimpleEntry<K, V> pair)
    {
        addNow(pair.getKey(), pair.getValue());
    }


    public void merge(TimeoutPairedList<K, V> mergeList)
    {
        if(mergeList==null) return;

        TimeoutPairedList<K,V> mergeClone = mergeList.clone();
		List<AbstractMap.SimpleEntry<K, V>> newElem = new LinkedList<>();

        if(mergeClone.size() < 1)
        {
            return;
        }

        if(size() < 1)
        {
            timeQ = mergeClone.timeQ;
            elements = mergeClone.elements;
            return;
        }



        Queue<TimeoutObject> newQueue = new LinkedBlockingQueue<>();

        Iterator<TimeoutObject> l1 = timeQ.iterator();
        Iterator<TimeoutObject> l2 = mergeClone.timeQ.iterator();


        TimeoutObject to1 = l1.next();
        TimeoutObject to2 = l2.next();

        while(to1 != null || to2 != null)
        {
           if(to1 != null && (to2 == null || to1.timeout < to2.timeout))
           {
				AbstractMap.SimpleEntry<K, V> pToAdd = new AbstractMap.SimpleEntry<>(to1.object.getKey(),
						to1.object.getValue());
               TimeoutObject tooToAdd = new TimeoutObject(pToAdd, to1.timeout);
               if(!newQueue.contains(tooToAdd))
               {
                   newQueue.add(tooToAdd);
                   newElem.add(pToAdd);
               }
               to1 = l1.hasNext() ? l1.next() : null;
           }
           else
           {
				AbstractMap.SimpleEntry<K, V> pToAdd = new AbstractMap.SimpleEntry<>(to2.object.getKey(),
						to2.object.getValue());
               TimeoutObject tooToAdd = new TimeoutObject(pToAdd, to2.timeout);
               if(!newQueue.contains(tooToAdd))
               {
                   newQueue.add(tooToAdd);
                   newElem.add(pToAdd);
               }
               to2 = l2.hasNext() ? l2.next() : null;
           }
        }

        elements = newElem;
        timeQ = newQueue;

        cleanup();
        assertSet();
    }

    /**
     * Returns whether this list contains the specified pair.
     *
     * @param key   the key of the pair
     * @param value the value of the pair
     * @return <code>true</code> if the list contains the pair
     */
    public boolean containsPair(K key, V value)
    {
        cleanup();
        assertSet();
		return elements.contains(new AbstractMap.SimpleEntry<>(key, value));
    }

    /**
     * Returns an unmodifiable List view of this TimeoutPairedList.
     *
     * @return an unmodifiable List view of this TimeoutPairedList.
     */
	public List<Map.Entry<K, V>> getUnmodifiableList()
    {
        cleanup();
        return Collections.unmodifiableList(elements);
    }


    /**
     * Returns all Pairs containing the given key.
     *
     * @param key the key to check for
     * @return all Pairs containing the given key
     */
	public LinkedList<Map.Entry<K, V>> getAll(K key)
    {
        cleanup();
		return elements.stream().filter(p -> p.getKey().equals(key))
				.collect(Collectors.toCollection(LinkedList<Map.Entry<K, V>>::new));
    }

    /**
     * Returns the size of this TimeoutPairedList.
     *
     * @return the size of this TimeoutPairedList
     */
    public int size()
    {
        cleanup();
        return elements.size();
    }

    private void assertSet()
    {
        if (elements.size() != timeQ.size())
            throw new AssertionError("Inequal sizes of queue and set: "
                    + timeQ.size() + ", " + elements.size());
    }

    /**
     * Cleans up this list. Calling this method does not change anything in the semantics
     * of the TimeoutPairedList, but should be done when you want to purge old entries from memory.
     * Automatically done through other methods, if needed.
     */
    public void cleanup()
    {
        long time = getCurrentTime();
        while (!timeQ.isEmpty() && timeQ.element().timeout <= time)
        {
            TimeoutObject obj = timeQ.remove();
            boolean removed = elements.remove(obj.object);
            if (removed) onElementTimeout();
        }
        assertSet();
    }

    protected long getCurrentTime()
    {
        return Time.getCurrentTime();
    }

    /**
     * Adds an {@link TimeoutPairedListListener} to this TimeoutMap.
     *
     * @param l the listener to be added
     */
    public void addListener(TimeoutPairedListListener l)
    {
        listeners.add(l);
    }

    protected void onElementTimeout()
    {
        for (TimeoutPairedListListener l : listeners)
            l.onElementTimeout();
    }

    public void clear()
    {
        elements = new LinkedList<>();
        timeQ = new LinkedBlockingQueue<>();
    }

    public boolean isEmpty()
    {
        //Cleanup done by calling size()
        return size() <= 0;
    }

    public long getTimeout() { return timeout; }

    /**
     * Can be used to determine if the map has changed (entries have timed out), if no other methods
     * were used on the map.
     * @return <code>true</code> if the map has changed.
     */
    public boolean hasChanged()
    {
        return elements.size() != size();
    }

    @Override
    public TimeoutPairedList<K, V> clone()
    {
        try
        {
            super.clone();

            TimeoutPairedList<K,V> clonedList = new TimeoutPairedList<>(timeout);
            Queue<TimeoutObject> clonedQueue = new LinkedBlockingQueue<>();

            Iterator<TimeoutObject> timeQIter = timeQ.iterator();
            while(timeQIter.hasNext())
            {
                clonedQueue.add(timeQIter.next().clone());
            }
            clonedList.timeQ = clonedQueue;
            clonedList.elements = new LinkedList<>(elements);

            return clonedList;
        } catch (CloneNotSupportedException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterator iterator()
    {
        return elements.iterator();
    }

    protected class TimeoutObject implements Cloneable
    {

		public TimeoutObject(AbstractMap.SimpleEntry<K, V> object, long timeout)
        {
            this.object = object;
            this.timeout = timeout;
        }

		public AbstractMap.SimpleEntry<K, V> object;

        public long timeout;

        @Override
        public boolean equals(Object o)
        {
            if(!(o instanceof TimeoutPairedList.TimeoutObject)) return false;
            return ((TimeoutObject) o).object.equals(this.object);
        }

        public int hashCode()
        {
            return this.object.hashCode();
        }

        @Override
        protected TimeoutObject clone() throws CloneNotSupportedException
        {
            super.clone();

			AbstractMap.SimpleEntry<K, V> pair = new AbstractMap.SimpleEntry<>(object.getKey(), object.getValue());
            return new TimeoutObject(pair, timeout);
        }
    }


}


