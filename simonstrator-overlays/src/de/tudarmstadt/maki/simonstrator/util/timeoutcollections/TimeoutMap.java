/*
 * Copyright (c) 2005-2011 KOM - Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


package de.tudarmstadt.maki.simonstrator.util.timeoutcollections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import de.tudarmstadt.maki.simonstrator.api.Time;

/**
 * A map of objects where elements 'run out' after a certain amount of time and get
 * thrown out of the collection. Entry (k,v) (added via putNow(k,v)) is removed from this map
 * after a defined timeout since it is added.
 * <br><br>
 * You can add listeners to this set that announce elements that are removed from
 * this set during the cleanup process.
 * 
 * @author Leo Nobach (original)
 * @author Extended funktionality by Nils Richerzhagen, Clemens Krug
 *
 * @param <K> Type of the keys
 * @param <V> Type of the values
 */
public class TimeoutMap<K, V> implements Cloneable{

	Map<K, V> elements = new HashMap<K, V>();

	Queue<TimeoutObject> timeQ = new LinkedBlockingQueue<TimeoutObject>();

	List<ITimeoutMapListener<K, V>> listeners = new ArrayList<ITimeoutMapListener<K, V>>();


	long timeout;

	/**
	 * Creates a new TimeoutMap with the given timeout in simulation time units, 
	 * i.e. the time until entry (k,v) (added via putNow(k,v)) is removed from this map.
	 * @param timeout , the time until entry (k,v) (added via putNow(k,v)) is removed from this map.
	 */
	public TimeoutMap(long timeout) {
		this.timeout = timeout;
	}

	/**
	 * Puts an entry into this timeout map. The entry will
	 * time out in currentTime + timeout simulation time units.
	 * @param key
	 * @param value
	 */
	public void putNow(K key, V value) {
		cleanup();
		int sizeBefore = elements.size();
		long time = getCurrentTime();
		if (elements.containsKey(key))
			timeQ.remove(new TimeoutObject(key, 0));
		elements.put(key, value);
		timeQ.add(new TimeoutObject(key, time + timeout));
		assertSet();
		if (sizeBefore == 0 && elements.size() >= 1) {
			gotFilled();
		}
	}

	/**
	 * Returns whether this set contains the specified key.
	 * Behaves like java.util.Map
	 * @param key
	 * @return
	 */
	public boolean containsKey(K key) {
		cleanup();
		assertSet();
		return elements.containsKey(key);
	}
	
	/**
	 * Returns an unmodifiable Map view of this TimeoutMap.
	 * @return
	 */
	public Map<K, V> getUnmodifiableMap() {
		cleanup();
		return Collections.unmodifiableMap(elements);
	}

	/**
	 * Removes the entry with the given key from this map, although
	 * it is not timed out.
	 * Behaves like java.util Map
	 * @param key
	 * @return
	 */
	public boolean remove(K key) {
		// System.out.println("Q+SET-BEFORE: " + timeQ + ", " + elements +
		// " REMOVING: " + element);
		cleanup();
		int sizeBefore = elements.size();
		boolean result = false;
		if (elements.containsKey(key)) {
			result = true;
			timeQ.remove(new TimeoutObject(key, 0));
			// System.out.println("SET CONTAINS ELEMENT");
		}
		elements.remove(key);
		assertSet();
		if (sizeBefore >= 1 && elements.size() == 0)
			gotEmpty();
		return result;
	}

	/**
	 * Returns the value associated with the given key.
	 * Behaves like java.util.Map
	 * @param key
	 * @return
	 */
	public V get(K key) {
		cleanup();
		return elements.get(key);
	}

	/**
	 * Returns the size of this TimeoutMap. Behaves like
	 * java.util.Map
	 * @return
	 */
	public int size() {
		cleanup();
		return elements.size();
	}

	private void assertSet() {
		if (elements.size() != timeQ.size())
			throw new AssertionError("Inequal sizes of queue and set: "
					+ timeQ.size() + ", " + elements.size());
	}

	/**
	 * Cleans up this map. Calling this method does not change anything in the semantics
	 * of the TimeoutMap, but should be done when you want to purge old entries from memory.
	 * Automatically done through other methods, if needed.
	 */
	public void cleanup() {
		int sizeBefore = elements.size();
		long time = getCurrentTime();
		while (!timeQ.isEmpty() && timeQ.element().timeout <= time) {
			TimeoutObject obj = timeQ.remove();
			V value = elements.remove(obj.object);
			elementTimeouted(obj.object, value, obj.timeout);
		}
		assertSet();
		if (sizeBefore >= 1 && elements.size() == 0)
			gotEmpty();
	}

	protected long getCurrentTime() {
		return Time.getCurrentTime();
	}

	/**
	 * Adds an ITimeoutMapListener to this TimeoutMap.
	 * @param l
	 */
	public void addListener(ITimeoutMapListener<K, V> l) {
		listeners.add(l);
	}

	public void clear()
	{
		boolean wasFull = elements.size() > 0;
		elements = new HashMap<K, V>();
		timeQ = new LinkedBlockingQueue<TimeoutObject>();
		if(wasFull) gotEmpty();
	}

	protected void elementTimeouted(K key, V value, long timeoutTime) {
		for (ITimeoutMapListener<K, V> l : listeners)
			l.elementTimeouted(this, key, value, timeoutTime);
	}

	/**
	 * Informs listener when map size changed from empty to size >= 1
	 */
	protected void gotFilled() {
		for (ITimeoutMapListener l : listeners)
			l.gotFilled();
	}

	/**
	 * Informs listener when map size changed from >0 to empty
	 */
	protected void gotEmpty(){
		for (ITimeoutMapListener l : listeners)
			l.gotEmpty();
	}

	public boolean isEmpty()
	{
		cleanup();
		return size() <= 0;
	}

	/**
	 * Can be used to determine if the map has changed (entries have timed out), if no other methods
	 * were used on the map.
	 * @return <code>true</code> if the map has changed.
	 */
	public boolean hasChanged()
	{
		return elements.size() != size();
	}

	@Override
	public TimeoutMap<K, V> clone()
	{
		try
		{
			return (TimeoutMap<K, V>) super.clone();
		} catch (CloneNotSupportedException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	protected class TimeoutObject {

		public TimeoutObject(K object, long timeout) {
			this.object = object;
			this.timeout = timeout;
		}

		public K object;

		public long timeout;

		public boolean equals(Object o) {
			return ((TimeoutObject) o).object.equals(this.object);
		}

		public int hashCode() {
			return this.object.hashCode();
		}
	}

}
