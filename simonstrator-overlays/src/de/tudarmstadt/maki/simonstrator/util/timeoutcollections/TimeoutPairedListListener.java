package de.tudarmstadt.maki.simonstrator.util.timeoutcollections;

/**
 * Created by Clemens on 16.06.2016.
 */
public interface TimeoutPairedListListener
{
    void onElementTimeout();

}
