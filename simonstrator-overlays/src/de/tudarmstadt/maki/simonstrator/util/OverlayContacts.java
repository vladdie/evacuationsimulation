package de.tudarmstadt.maki.simonstrator.util;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;

/**
 * Utility class for some functions related to the maintenance of overlay
 * contacts. Contains additional static helper methods.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class OverlayContacts extends AbstractSet<OverlayContact> {

	private final Map<INodeID, OverlayContact> idToContact;

	private final List<INodeID> ids;

	public OverlayContacts() {
		idToContact = new LinkedHashMap<>();
		ids = new LinkedList<>();
	}

	public OverlayContacts(Collection<OverlayContact> contacts) {
		this();
		for (OverlayContact contact : contacts) {
			if (!idToContact.containsKey(contact.getNodeID())) {
				idToContact.put(contact.getNodeID(), contact);
				ids.add(contact.getNodeID());
			}
		}
	}

	public List<OverlayContact> getListFor(Collection<INodeID> ids) {
		List<OverlayContact> contacts = new LinkedList<>();
		for (INodeID id : ids) {
			contacts.add(idToContact.get(id));
		}
		return contacts;
	}

	public OverlayContact get(INodeID nodeId) {
		return idToContact.get(nodeId);
	}

	public Set<INodeID> getIdSet() {
		return Collections.unmodifiableSet(idToContact.keySet());
	}

	public List<INodeID> getIdList() {
		return Collections.unmodifiableList(ids);
	}

	public OverlayContact removeContact(INodeID id) {
		ids.remove(id);
		return idToContact.remove(id);
	}

	@Override
	public boolean add(OverlayContact e) {
		if (!idToContact.containsKey(e.getNodeID())) {
			idToContact.put(e.getNodeID(), e);
			ids.add(e.getNodeID());
			return true;
		}
		return false;
	}


	@Override
	public Iterator<OverlayContact> iterator() {
		return new Iterator<OverlayContact>() {

			private int idx = 0;

			@Override
			public boolean hasNext() {
				return idx < idToContact.size() && ids.get(idx) != null;
			}

			@Override
			public OverlayContact next() {
				return idToContact.get(ids.get(idx++));
			}

			@Override
			public void remove() {
				removeContact(ids.get(idx - 1));
			}
		};
	}

	@Override
	public int size() {
		return idToContact.size();
	}

	@Override
	public boolean isEmpty() {
		if (size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	/*
	 * Static helper methods below
	 */

	/**
	 * Returns the overlay contact associated with the given {@link INodeID}.
	 * 
	 * @param nodeId
	 * @param outOf
	 * @return
	 */
	public static OverlayContact get(INodeID nodeId,
			Collection<OverlayContact> outOf) {
		for (OverlayContact contact : outOf) {
			if (contact.getNodeID().equals(nodeId)) {
				return contact;
			}
		}
		return null;
	}

	/**
	 * A list of {@link INodeID}s of the given contacts
	 * 
	 * @param contacts
	 * @return
	 */
	public static List<INodeID> getNodeIDs(Collection<OverlayContact> contacts) {
		LinkedList<INodeID> result = new LinkedList<>();
		for (OverlayContact overlayContact : contacts) {
			result.add(overlayContact.getNodeID());
		}
		return result;
	}

}
