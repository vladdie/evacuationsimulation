/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.service.virtualtopology.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;

/**
 * Datastructure that is being distributed to the nodes and enables the
 * respective local runtime to limit the receiving of messages to neighbors in
 * the virtual topology.
 * 
 * It consists of a simple matrix of booleans represented by a BitSet.
 * 
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0 Jul 13, 2013
 * @since Jul 13, 2013
 * 
 */
public class VirtualTopology implements Transmitable {

	private static final long serialVersionUID = 1L;

	private final BitSet matrix;

	private final byte maxLocationId;

	public VirtualTopology(BitSet matrix, byte maxLocationId) {
		this.matrix = matrix;
		this.maxLocationId = maxLocationId;
	}

	@Override
	public int getTransmissionSize() {
		return (int) Math.ceil(matrix.length() / 8) + 1;
	}

	/**
	 * True, if a message can be sent from a node at location with id
	 * <code>from</code> to a node at location with id <code>to</code>.
	 * 
	 * @param from
	 * @param to
	 * @return
	 */
	public boolean areConnected(byte from, byte to) {
		if (from < 0 || to < 0) {
			Monitor.log(VirtualTopology.class, Level.WARN,
					"One id < 0, disconnected");
			return false;
		}
		int idxBitSet = to * (maxLocationId + 1) + from;
		// Monitor.log(VirtualTopology.class,
		// "Checking entry " + idxBitSet + " from " + from + " to " + to
		// + ", result: "
		// + (matrix.get(idxBitSet) ? "true" : "false"),
		// Level.INFO);
		return matrix.get(idxBitSet);
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("\t  ");
		for (int col = 0; col <= maxLocationId; col++) {
			str.append(col);
			str.append(' ');
		}
		str.append('\n');
		for (int row = 0; row <= maxLocationId; row++) {
			str.append('\t');
			str.append(row);
			str.append(' ');
			for (int col = 0; col <= maxLocationId; col++) {
				if (matrix.get(row * (maxLocationId + 1) + col)) {
					str.append('X');
				} else {
					str.append('-');
				}
				str.append(' ');
			}
			str.append('\n');
		}
		return str.toString();
		// return matrix.toString();
	}

	/**
	 * De-Serialize a {@link VirtualTopology}
	 * 
	 * @param in
	 * @return
	 */
	public static VirtualTopology fromBytes(InputStream in) {
		try {
			ObjectInputStream oIn = new ObjectInputStream(in);
			byte maxLocationId = oIn.readByte();
			BitSet matrix = (BitSet) oIn.readObject();
			return new VirtualTopology(matrix, maxLocationId);
		} catch (IOException e) {
			throw new AssertionError("De-Serialization failed!");
		} catch (ClassNotFoundException e) {
			throw new AssertionError("Unexpected Object!");
		}
	}

	/**
	 * Serialize a {@link VirtualTopology}.
	 * 
	 * @param out
	 * @param virtualTopology
	 */
	public static void toBytes(OutputStream out, VirtualTopology virtualTopology) {
		try {
			ObjectOutputStream oOut = new ObjectOutputStream(out);
			// here, we rely on the BitSets serialization.
			oOut.writeByte(virtualTopology.maxLocationId);
			oOut.writeObject(virtualTopology.matrix);
		} catch (IOException e) {
			throw new AssertionError("Serialization failed!");
		}
	}

	/**
	 * Parses a virtual topology given by a csv-file.
	 * 
	 * Format of the file: columns denote the source, rows the recipient
	 * location ID. If a connection is possible, this is denoted by a "1" in the
	 * respective entry. Otherwise, the entry is "0".
	 * 
	 * A sanity-check is performed - if sender = receiver, the entry has to be
	 * "1". The matrix has to be symmetric, i.e., its top right half must be
	 * identical to its bottom left half. This implies, that communication links
	 * in the virtual topology are symmetric.
	 * 
	 * @param data
	 */
	public static VirtualTopology parseFromFile(String filename) {
		/*
		 * TODO
		 */

		return null;
	}

	/**
	 * Mainly for testing purposes: a default topology supporting up to 5
	 * locations that are all connected as a chain.
	 * 
	 * @return
	 */
	public static VirtualTopology getDefaultTopology() {
		BitSet matrix = new BitSet();
		int maxLocationID = 3;
		matrix.set(0);
		matrix.set(1);
		matrix.set(4);
		matrix.set(5);
		matrix.set(6);
		matrix.set(9);
		matrix.set(10);
		matrix.set(11);
		matrix.set(14);
		matrix.set(15);
		return new VirtualTopology(matrix, (byte) maxLocationID);
	}

}
