/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.service.virtualtopology.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;

/**
 * This message is piggybacked to each sent message to add the current position
 * of our node for later filtering on the receiver side. It is also used to
 * inform the controller of a new position (i.e., movement) of a node. The
 * controller might react by broadcasting a {@link TopologyUpdateMsg}.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0 Jul 13, 2013
 * @since Jul 13, 2013
 * 
 */
public class CurrentPositionMsg implements Message {

	private static final long serialVersionUID = 1L;

	private byte senderPositionId;

	@SuppressWarnings("unused")
	private CurrentPositionMsg() {
		// For Kryo
	}

	public CurrentPositionMsg(byte senderPositionId) {
		this.senderPositionId = senderPositionId;
	}

	/**
	 * @return the senderPositionId
	 */
	public byte getSenderPositionId() {
		return senderPositionId;
	}

	@Override
	public long getSize() {
		return 1; // position-int;
	}

	@Override
	public Message getPayload() {
		return null;
	}

}
