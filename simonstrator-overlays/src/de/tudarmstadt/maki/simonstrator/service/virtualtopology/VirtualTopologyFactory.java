/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.service.virtualtopology;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.service.virtualtopology.data.VirtualTopology;

/**
 * Factory-class for the virtual topology-service
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0 Jul 13, 2013
 * @since Jul 13, 2013
 * 
 */
public class VirtualTopologyFactory implements HostComponentFactory {

	private VirtualTopology initialTopology;

	public static int PORT;

	@Override
	public HostComponent createComponent(Host host) {
		if (initialTopology == null) {
			initialTopology = VirtualTopology.getDefaultTopology();
		}
		return new VirtualTopologyNode(host, initialTopology, PORT);
	}

	/**
	 * A topology-file that is to be used as initial topology
	 * 
	 * @param filename
	 */
	public void setTopologyFile(String filename) {
		initialTopology = VirtualTopology.parseFromFile(filename);
	}

}
