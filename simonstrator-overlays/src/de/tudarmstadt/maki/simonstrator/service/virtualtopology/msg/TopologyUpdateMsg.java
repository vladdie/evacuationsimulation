/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.service.virtualtopology.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.service.virtualtopology.data.VirtualTopology;

/**
 * This message tells the receivers, from which senders they are able to receive
 * messages according to the current state of the virtual topology. At the same
 * time, this set of NetIDs also defines the recipients of a broadcast in the
 * virtual topology.
 * 
 * This message always originates from the central controller and is usually
 * broadcasted, as it contains information required on all nodes.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0 Jul 13, 2013
 * @since Jul 13, 2013
 * 
 */
public class TopologyUpdateMsg implements Message {

	private static final long serialVersionUID = 1L;

	private final VirtualTopology virtualTopology;

	public TopologyUpdateMsg(VirtualTopology virtualTopology) {
		this.virtualTopology = virtualTopology;
	}

	public VirtualTopology getVirtualTopology() {
		return virtualTopology;
	}

	@Override
	public long getSize() {
		return virtualTopology.getTransmissionSize();
	}

	@Override
	public Message getPayload() {
		return null;
	}

}
