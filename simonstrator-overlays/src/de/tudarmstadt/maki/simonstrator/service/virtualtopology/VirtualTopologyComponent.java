/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.service.virtualtopology;

import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.service.virtualtopology.data.VirtualTopology;

/**
 * This interface provides the public methods for steering the VirtualTopology.
 * It allows to define a node's current position within the topology and also to
 * define exceptions where messages should not be blocked.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0 Aug 20, 2013
 * @since Aug 20, 2013
 * 
 */
public interface VirtualTopologyComponent extends HostComponent {

	/**
	 * Update the node's current position by setting a new ID. This can be
	 * triggered for example by scanning a NFC-Tag or through an application's
	 * preferences.
	 * 
	 * @param positionID
	 */
	public void updateCurrentPositionId(byte positionID);

	/**
	 * Returns the current positionID of the component.
	 * 
	 * @return
	 */
	public byte getCurrentPositionId();

	/**
	 * Tells the virtual topology to always deliver messages on the specified
	 * port, regardless of the current virtual topology. Useful for example for
	 * maintenance or monitoring messages that do not affect the
	 * simulated/tested scenario.
	 * 
	 * @param port
	 */
	public void addPortToIgnore(int port);

	/**
	 * Revoke an ignore-rule for a given port. Messages delivered via this port
	 * will be subject to filtering again.
	 * 
	 * @param port
	 */
	public void removePortToIgnore(int port);

	/**
	 * Set a new virtual topology. The component will periodically distribute it
	 * to other nodes (via 1-hop broadcast), if the flag is set to true. Ensure,
	 * that this is only called on ONE component, as otherwise you might run
	 * into consistency-related problems.
	 * 
	 * @param topology
	 */
	public void updateAndAnnounceVirtualTopology(VirtualTopology topology);

	/**
	 * Returns the current topology
	 * 
	 * @return
	 */
	public VirtualTopology getCurrentVirtualTopology();

}
