/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.service.virtualtopology;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ServiceNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransportProtocol;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.api.component.transport.service.FirewallService;
import de.tudarmstadt.maki.simonstrator.api.component.transport.service.PiggybackMessageService;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;
import de.tudarmstadt.maki.simonstrator.service.virtualtopology.data.VirtualTopology;
import de.tudarmstadt.maki.simonstrator.service.virtualtopology.msg.CurrentPositionMsg;
import de.tudarmstadt.maki.simonstrator.service.virtualtopology.msg.TopologyUpdateMsg;

/**
 * A node within the virtual Topology (i.e., a smartphone). The position of the
 * node is defined by a one-byte ID that can (for example) be obtained from
 * scanning a NFC-Tag.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0 Jul 13, 2013
 * @since Jul 13, 2013
 * 
 */
public class VirtualTopologyNode extends AbstractOverlayNode implements
		VirtualTopologyComponent, PiggybackMessageService, FirewallService,
		TransMessageListener {

	private final static byte PIGGYBACK_SERVICE_ID = 6;

	private final static byte MSG_TOPOLOGY_UPDATE = 2;

	private final static byte MSG_CURRENT_POSITION = 3;

	protected final static byte UNKNOWN_POSITION = -1;

	protected final int VIRTUAL_TOPOLOGY_PORT;

	private final Set<Integer> whitelistedPorts = new LinkedHashSet<Integer>();

	private UDP udp;

	private byte currentPositionId = UNKNOWN_POSITION;

	public static final long PERIODIC_BROADCAST_INTERVAL = 20 * Time.SECOND;

	private final PeriodicTopologyBroadcast topoBroadcastOp = new PeriodicTopologyBroadcast(
			this, PERIODIC_BROADCAST_INTERVAL);

	private VirtualTopology currentTopology;

	private final HashMap<NetID, Byte> lastKnownPositions = new LinkedHashMap<NetID, Byte>();

	/**
	 * @param host
	 * @param peerId
	 */
	public VirtualTopologyNode(Host host, VirtualTopology initialTopology,
			int port) {
		super(host);
		this.currentTopology = initialTopology;
		this.VIRTUAL_TOPOLOGY_PORT = port;
	}

	@Override
	public void initialize() {
		super.initialize();
		try {
			NetInterface net = getHost().getNetworkComponent().getByName(
					NetInterfaceName.WIFI);
			if (net == null) {
				net = getHost().getNetworkComponent().getByName(
						NetInterfaceName.ETHERNET);
			}
			if (net == null) {
				throw new AssertionError("No Net...");
			}
			udp = getAndBindUDP(net.getLocalInetAddress(),
					VIRTUAL_TOPOLOGY_PORT);
			udp.setTransportMessageListener(this);

			// Register as a service
			getHost().getTransportComponent().registerService(
					PiggybackMessageService.class, this);
			getHost().getTransportComponent().registerService(
					FirewallService.class, this);
		} catch (ServiceNotAvailableException e) {
			throw new AssertionError(
					"The full functionality of the VirtualTopology is not available on this platform. Ensure, that you are using the latest version of the platform's runtime!");
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError(
					"This service requires a WiFi-AdHoc-Netinterface to work properly.");
		}
	}

	/**
	 * @return the udp
	 */
	protected UDP getUdp() {
		return udp;
	}

	/**
	 * Current ID of the Position of this node
	 * 
	 * @return
	 */
	@Override
	public byte getCurrentPositionId() {
		return currentPositionId;
	}

	/**
	 * The current topology object
	 * 
	 * @return
	 */
	@Override
	public VirtualTopology getCurrentVirtualTopology() {
		return currentTopology;
	}

	@Override
	public void updateAndAnnounceVirtualTopology(VirtualTopology topology) {
		if (topoBroadcastOp.isStopped()) {
			/*
			 * Initialize Operation ONCE, it takes care of always picking the
			 * most recent topology to broadcast.
			 */
			this.currentTopology = topology;
			this.topoBroadcastOp.start();
		}
	}

	/**
	 * This is called by the android application that scans the NFC-Tags.
	 * 
	 * @param newId
	 */
	@Override
	public void updateCurrentPositionId(byte newId) {
		this.currentPositionId = newId;
		Monitor.log(VirtualTopology.class, Level.INFO,
				"Updated current position to %d.", currentPositionId);
		getUdp().send(new CurrentPositionMsg(currentPositionId),
				getUdp().getNetInterface().getBroadcastAddress(),
				VIRTUAL_TOPOLOGY_PORT);
	}

	@Override
	public void addPortToIgnore(int port) {
		whitelistedPorts.add(port);
	}

	@Override
	public void removePortToIgnore(int port) {
		whitelistedPorts.remove(port);
	}

	@Override
	public OverlayContact getLocalOverlayContact() {
		throw new AssertionError("Not supported by this component.");
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		setPeerStatus(PeerStatus.PRESENT);
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		setPeerStatus(PeerStatus.ABSENT);
	}

	@Override
	public Class<?>[] getSerializableTypes() {
		return new Class<?>[] { CurrentPositionMsg.class,
				TopologyUpdateMsg.class, VirtualTopology.class };
	}

	@Override
	public void serialize(OutputStream out, Message msg) {
		try {
			if (msg instanceof CurrentPositionMsg) {
				out.write(MSG_CURRENT_POSITION);
				out.write(((CurrentPositionMsg) msg).getSenderPositionId());
			} else if (msg instanceof TopologyUpdateMsg) {
				out.write(MSG_TOPOLOGY_UPDATE);
				VirtualTopology.toBytes(out,
						((TopologyUpdateMsg) msg).getVirtualTopology());
			}
		} catch (IOException e) {
			throw new AssertionError("Serialization failed...");
		}
	}

	@Override
	public Message create(InputStream in) {
		try {
			byte type = (byte) in.read();
			switch (type) {
			case MSG_CURRENT_POSITION:
				byte currentPositionID = (byte) in.read();
				return new CurrentPositionMsg(currentPositionID);

			case MSG_TOPOLOGY_UPDATE:
				VirtualTopology topo = VirtualTopology.fromBytes(in);
				return new TopologyUpdateMsg(topo);

			default:
				throw new AssertionError("Unknown message type...");
			}
		} catch (IOException e) {
			throw new AssertionError("De-Serialization failed...");
		}
	}

	@Override
	public Message piggybackOnSendMessage(NetID to, int receiverPort,
			TransportProtocol protocol) {
		// Monitor.log(VirtualTopologyNode.class,
		// "Piggybacking current position: " + currentPositionId,
		// Level.INFO);
		return new CurrentPositionMsg(currentPositionId);
	}

	@Override
	public void onReceivedPiggybackedMessage(Message msg, TransInfo sender) {
		if (msg instanceof CurrentPositionMsg) {
			lastKnownPositions.put(sender.getNetId(),
					((CurrentPositionMsg) msg).getSenderPositionId());
			// Monitor.log(VirtualTopologyNode.class,
			// "Received piggybacked information of current position: "
			// + currentPositionId, Level.INFO);
		} else {
			throw new AssertionError(
					"Only expecting current position messages...");
		}
	}

	@Override
	public byte getPiggybackServiceID() {
		return PIGGYBACK_SERVICE_ID;
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		if (msg instanceof TopologyUpdateMsg) {
			handleTopologyUpdateMsg((TopologyUpdateMsg) msg, sender);
		} else if (msg instanceof CurrentPositionMsg) {
			handleCurrentPositionMsg((CurrentPositionMsg) msg, sender);
		}
	}

	protected void handleTopologyUpdateMsg(TopologyUpdateMsg msg,
			TransInfo sender) {
		currentTopology = msg.getVirtualTopology();
	}

	protected void handleCurrentPositionMsg(CurrentPositionMsg msg,
			TransInfo sender) {
		lastKnownPositions.put(sender.getNetId(), msg.getSenderPositionId());
	}

	@Override
	public boolean allowIncomingConnection(TransInfo from, int onPort) {
		if (onPort == VIRTUAL_TOPOLOGY_PORT
				|| whitelistedPorts.contains(onPort)) {
			// exclude Virtual Topology Protocol
			return true;
		}
		if (lastKnownPositions.containsKey(from.getNetId())) {
			byte lastKnownPositionSender = lastKnownPositions.get(from
					.getNetId());
			return currentTopology.areConnected(lastKnownPositionSender,
					currentPositionId);
		}
		return true;
	}

	@Override
	public boolean allowOutgoingConnection(NetID to, int toPort, int onPort) {
		if (onPort == VIRTUAL_TOPOLOGY_PORT
				|| whitelistedPorts.contains(onPort)) {
			// exclude Virtual Topology Protocol
			return true;
		}
		if (lastKnownPositions.containsKey(to)) {
			byte lastKnownPositionReceiver = lastKnownPositions.get(to);
			return currentTopology.areConnected(currentPositionId,
					lastKnownPositionReceiver);
		}
		// to allow broadcasts and unknown protocols to pass
		return true;
	}

	/**
	 * Periodically broadcasts the current topology
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0 Jul 14, 2013
	 * @since Jul 14, 2013
	 * 
	 */
	private class PeriodicTopologyBroadcast extends
			PeriodicOperation<VirtualTopologyNode, Object> {

		/**
		 * @param component
		 * @param callback
		 * @param interval
		 */
		protected PeriodicTopologyBroadcast(VirtualTopologyNode component,
				long interval) {
			super(component, Operations.getEmptyCallback(), interval);
		}

		@Override
		protected void executeOnce() {
			getUdp().send(new TopologyUpdateMsg(currentTopology),
					getUdp().getNetInterface().getBroadcastAddress(),
					VIRTUAL_TOPOLOGY_PORT);
			this.operationFinished(true);
		}

		@Override
		public Object getResult() {
			return null;
		}

	}

}
