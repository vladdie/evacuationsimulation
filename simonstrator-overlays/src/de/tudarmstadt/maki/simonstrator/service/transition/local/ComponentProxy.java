package de.tudarmstadt.maki.simonstrator.service.transition.local;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.transition.AtomicTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionListener;

/**
 * A proxy for a component. This proxy intercepts and forwards all method
 * invocations to a concrete component instance. Transitions can exchange the
 * used components at runtime. The proxy ensures that these transitions are
 * transparent for the application (developer).
 * 
 * Important: Do not store any references to the actual components as these
 * might get outdated during execution.
 * 
 * @author Alexander Froemmgen, Bjoern Richerzhagen
 * 
 */
public class ComponentProxy<R extends TransitionEnabled> implements
		InvocationHandler, Comparable<ComponentProxy<?>> {

	/*
	 * The name of the component proxy which can be used to filter and define
	 * transitions.
	 */
	private final String name;

	private final String interfaceName;

	private final List<TransitionListener> transitionListeners;

	private final List<AtomicTransition> customTransitions = new LinkedList<AtomicTransition>();

	/**
	 * The id of the component proxy. This id is not globally unique. However,
	 * the tupel (nodeId, proxyId) is unique.
	 */
	private final String id;

	/**
	 * The currently used and active component.
	 */
	private R activeComponent;

	/**
	 * Initially, the mechanism masked by this proxy is not started.
	 */
	private boolean isStarted = false;

	/**
	 * True, if the proxy has been destroyed and can no longer be used.
	 */
	private boolean isDestroyed = false;

	/**
	 * Specifies if the proxy is currently affected by a transition
	 */
	private boolean inTransition = false;

	private final static Random rnd = Randoms.getRandom(ComponentProxy.class);

	/*
	 * The reference to proxy as seen from the application.
	 */
	@SuppressWarnings("unused")
	private R proxyFassade;

	/**
	 * Creates a new component proxy, only to be used by the
	 * {@link TransitionEngine}. Proxy creation is done via the
	 * {@link TransitionEngine}.
	 * 
	 * @param name
	 * @param proxyInterface
	 * @param defaultInstance
	 */
	public ComponentProxy(String name, Class<R> proxyInterface,
			R defaultInstance) {
		this.name = name;
		this.activeComponent = defaultInstance;
		this.interfaceName = proxyInterface.getName();
		this.transitionListeners = new LinkedList<>();
		// Generate an ID that is "unique" on the given node.
		this.id = name + String.valueOf(rnd.nextInt());
	}
	
	/**
	 * Adds a {@link TransitionListener}, if not already present
	 * 
	 * @param listener
	 */
	public void addListener(TransitionListener listener) {
		if (!this.transitionListeners.contains(listener)) {
			this.transitionListeners.add(listener);
		}
	}

	/**
	 * Removes a {@link TransitionListener}, if present
	 * 
	 * @param listener
	 */
	public void removeListener(TransitionListener listener) {
		this.transitionListeners.remove(listener);
	}

	/**
	 * Register a custom transition
	 * 
	 * @param transition
	 */
	public void addCustomTransition(AtomicTransition<?, ?> transition) {
		this.customTransitions.add(transition);
	}
	
	/**
	 * IFF a custom {@link AtomicTransition} has been registered for this proxy,
	 * and if the respective target type and our current source type is
	 * supported by that custom transition, it is returned.
	 * 
	 * @param targetType
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 */
	public <T extends TransitionEnabled> AtomicTransition<?, ?> getCustomTransition(
			Class<T> targetType) throws InstantiationException, IllegalAccessException {
		if (customTransitions.isEmpty()) {
			return null;
		}

		AtomicTransition<?, ?> srcMatch = null;
		AtomicTransition<?, ?> dstMatch = null;
		AtomicTransition<?, ?> assignMatch = null;
		
		Class<?> activeType = activeComponent.getClass();

		for (AtomicTransition<?, ?> transition : customTransitions) {
			/*
			 * TODO: check for a 100% match -> src and target class match. After
			 * that, check with loosed constraints -> src must be assignable,
			 * target should match. After that, check for src match and target
			 * assignable. Finally, use both assignable. Do we need to cache the
			 * result of this computation?
			 * 
			 * We use the first full match (succeed fast).
			 * 
			 * FIXME check order of isAssignable-calls!
			 */
			if (transition.getSourceType().equals(activeType)) {
				if (transition.getTargetType().equals(targetType)) {
					// full match
					return transition.getClass().newInstance();
				}
				if (transition.getTargetType().isAssignableFrom(targetType)) {
					srcMatch = transition;
				}
			} else if (transition.getTargetType().equals(targetType)) {
				if (transition.getSourceType().isAssignableFrom(activeType)) {
					dstMatch = transition;
				}
			} else if (dstMatch == null && srcMatch == null
					&& transition.getSourceType().isAssignableFrom(activeType)
					&& transition.getTargetType().isAssignableFrom(targetType)) {
				// Only check if no better match found yet
				assignMatch = transition;
			}
		}
		return (dstMatch != null ? dstMatch.getClass().newInstance()
				: (srcMatch != null ? srcMatch.getClass().newInstance()
						: (assignMatch != null ? assignMatch.getClass().newInstance() : null)));
	}

	/**
	 * Sets the currently active component. This is managed by a transition.
	 * 
	 * @param component
	 */
	@SuppressWarnings("unchecked")
	public void setActiveComponent(TransitionEnabled component) {
		// Please Note: The assignment of a reference in Java is atomic!
		// In case this method will be extended, make sure it stays "atomic"
		this.activeComponent = (R) component;
		Monitor.log(ComponentProxy.class, Level.DEBUG,
				"%s - Switching active component to %s",
				Time.getFormattedTime(), component.getClass()
						.getSimpleName());

		/*
		 * To stay "atomic", we trigger the listener using an event!
		 */
		for (TransitionListener listener : transitionListeners) {
			Event.scheduleImmediately(new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					listener.executedTransition();
				}
			}, null, 0);
		}
	}

	@Override
	public Object invoke(Object proxy, Method m, Object[] args)
			throws Throwable {
		assert activeComponent != null;
		assert isStarted;
		assert !isDestroyed;
		return m.invoke(activeComponent, args);
	}

	public String getId() {
		return id;
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public String getName() {
		return name;
	}

	@Override
	public int compareTo(ComponentProxy<?> o) {
		return id.compareTo(o.id);
	}

	public TransitionEnabled getActiveComponent() {
		return activeComponent;
	}

	/**
	 * Tests if the proxy is currently in a transition and starts a transition
	 * if not. This method is synchronized to ensure atomicity.
	 * 
	 * @return
	 */
	public boolean testAndStartTransition() {
		if (inTransition)
			return false;
		for (TransitionListener listener : transitionListeners) {
			listener.startingTransition();
		}
		inTransition = true;
		return true;
	}

	/**
	 * Finishes the transition and allows the proxy to be part of another
	 * transition
	 */
	public void finishTransition() {
		assert inTransition;
		inTransition = false;
	}
	
	/**
	 * Called, after the proxy has been initialized with the first mechanism
	 * (and can now be used).
	 */
	public void initializationFinished() {
		this.isStarted = true;
	}

	/**
	 * Clear internal state.
	 */
	public void shutdownFinished() {
		this.activeComponent = null;
		this.customTransitions.clear();
		this.transitionListeners.clear();
		this.proxyFassade = null;
		this.isDestroyed = true;
	}

}
