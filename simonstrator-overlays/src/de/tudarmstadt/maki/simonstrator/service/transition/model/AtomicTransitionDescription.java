package de.tudarmstadt.maki.simonstrator.service.transition.model;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import de.tudarmstadt.maki.simonstrator.api.component.transition.AtomicTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;

/**
 * Description of an AtomicTransition. This description contains all modeled
 * details about the available transitions and is created from the transition
 * description file.
 * 
 * @author Bjoern Richerzhagen, Alexander Froemmgen
 * 
 * @param <F>
 *            Source component type for the transition
 * @param <T>
 *            Target component type for the transition
 */
public class AtomicTransitionDescription<F extends TransitionEnabled, T extends TransitionEnabled>
		implements TransitionDescription {

	private static final long serialVersionUID = 1L;

	/*
	 * The name of the atomic transition
	 */
	private final String name;

	/*
	 * The source component type
	 */
	public final Class<F> sourceComponentType;

	/*
	 * The target component type
	 */
	public final Class<T> targetComponentType;

	/*
	 * The transition strategy which is called at certain lifecycle events of
	 * the transition
	 */
	public Class<? extends AtomicTransition<F, T>> transitionStrategy;

	/*
	 * The variables of the components which should be transfered during the
	 * transition
	 */
	// TODO at which point in time? which state?
	private final Set<String> transferVariables = new TreeSet<String>();

	// TODO use!
	private final Set<TransitionMethodDescription> transitionMethodDescriptions = new TreeSet<TransitionMethodDescription>();

	/**
	 * Creates a new AtomicTransitionDescription which contains all information
	 * about the transition.
	 * 
	 * @param name
	 *            The name of the transition.
	 * @param sourceClass
	 *            The class of the source component
	 * @param targetClass
	 *            The class of the target component
	 * @param transitionClass
	 *            The class of the transition strategy
	 * @param transitionType
	 *            The type of the transition
	 */
	@SuppressWarnings("unchecked")
	public AtomicTransitionDescription(String name, Class<? extends TransitionEnabled> sourceClass,
			Class<? extends TransitionEnabled> targetClass, Class<? extends AtomicTransition> transitionClass) {
		this.name = name;
		this.sourceComponentType = (Class<F>) sourceClass;
		this.targetComponentType = (Class<T>) targetClass;
		assert !sourceComponentType.equals(targetComponentType) : "Self-transition: do we allow those?";
		this.transitionStrategy = (Class<? extends AtomicTransition<F, T>>) transitionClass;
	}

	/**
	 * Adds a transfer variable
	 * 
	 * @param name
	 */
	public void addTransferVariable(String name) {
		transferVariables.add(name);
	}

	/**
	 * Adds a transition method description
	 * 
	 * @param transitionMethodDescription
	 */
	public void addMethod(
			TransitionMethodDescription transitionMethodDescription) {
		transitionMethodDescriptions.add(transitionMethodDescription);
	}

	@Override
	public String getName() {
		return name;
	}

	public Set<String> getTransferVariables() {
		return Collections.unmodifiableSet(transferVariables);
	}

	@Override
	public int getTransmissionSize() {
		/*
		 * TODO For simulations: how big is this object in bytes, when being
		 * transmitted? For now, we just use a simple fixed value.
		 */
		return 100;
	}

	@Override
	public String toString() {
		return name + ": " + sourceComponentType.getSimpleName() + " -> "
				+ targetComponentType.getSimpleName();
	}
}
