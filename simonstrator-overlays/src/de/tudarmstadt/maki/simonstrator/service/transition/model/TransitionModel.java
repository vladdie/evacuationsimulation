package de.tudarmstadt.maki.simonstrator.service.transition.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.service.transition.language.TransitionLanguageLexer;
import de.tudarmstadt.maki.simonstrator.service.transition.language.TransitionLanguageListener;
import de.tudarmstadt.maki.simonstrator.service.transition.language.TransitionLanguageParser;
import de.tudarmstadt.maki.simonstrator.service.transition.language.TransitionLanguageRealListener;

/**
 * The TransitionModel manages the the representations of all transitions. This
 * model is created from the transition description file.
 * 
 * @author Alexander Froemmgen
 * 
 */
public class TransitionModel {
	private final Map<String, InterfaceDescription> interfaces = new TreeMap<String, InterfaceDescription>();
	private final Map<String, ComponentDescription> components = new TreeMap<String, ComponentDescription>();

	/**
	 * Mapping: transition-name to TransitionDescription
	 */
	private final Map<String, AtomicTransitionDescription<TransitionEnabled, TransitionEnabled>> atomicTransitions = new TreeMap<String, AtomicTransitionDescription<TransitionEnabled, TransitionEnabled>>();
	
	/**
	 * Creates a new instance of the TransitionModel. Therefore, the transition
	 * description file is parsed.
	 * 
	 * @param filename
	 *            The filename of the transition description file.
	 * @throws IOException
	 */
	public TransitionModel(String filename) throws IOException {
		File source = new File(filename + ".maki");
		ANTLRInputStream in = new ANTLRInputStream(new FileInputStream(source));
		TransitionLanguageLexer lexer = new TransitionLanguageLexer(
				(CharStream) in);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		TransitionLanguageParser parser = new TransitionLanguageParser(tokens);

		TransitionLanguageParser.DefineContext context = parser.define();

		ParseTreeWalker walker = new ParseTreeWalker();
		TransitionLanguageListener listener = new TransitionLanguageRealListener(
				this);
		walker.walk(listener, context);
	}

	/**
	 * Adds a ComponenDescription
	 * 
	 * @param componentDescription
	 */
	public void add(ComponentDescription componentDescription) {
		components.put(componentDescription.name, componentDescription);
	}

	/**
	 * Adds an InterfaceDescription
	 * 
	 * @param interfaceDescription
	 */
	public void add(InterfaceDescription interfaceDescription) {
		interfaces.put(interfaceDescription.getName(), interfaceDescription);
	}

	/**
	 * Gets an InterfaceDescription with the specified name. Returns
	 * <code>null</code> if no such InterfaceDescription exists.
	 * 
	 * @param name
	 * @return
	 */
	public InterfaceDescription getInterfaceDescription(String name) {
		return interfaces.get(name);
	}

	/**
	 * Gets a MethodDescription with the specified name for the specified
	 * component. Returns </code>null</code> if no such MethodDescription
	 * exists.
	 * 
	 * @param componentName
	 * @param methodName
	 * @return
	 */
	public MethodDescription getMethodDescription(String componentName,
			String methodName) {
		ComponentDescription componentDescription = components
				.get(componentName);
		if (componentDescription == null)
			return null;

		return componentDescription.getMethod(methodName);
	}

	/**
	 * Dumps the whole transition model to the console. Used for debugging
	 * purposes mostly.
	 */
	public void dumpTransitionModel() {
		System.out.println("Transition Model Dump");

		System.out.println("Interfaces");
		for (InterfaceDescription interfaceDescription : interfaces.values()) {
			System.out.println("\t- " + interfaceDescription.getName());
			for (MethodDescription method : interfaceDescription
					.getMethodDescriptions()) {
				System.out.println("\t\t- " + method + "()");
			}
		}
		System.out.println("Components");
		for (ComponentDescription componentDescription : components.values()) {
			System.out.print("\t- " + componentDescription.name);
			if (componentDescription.implementsInterfaces.size() > 0) {
				System.out.print(" implements");
			}
			for (InterfaceDescription interfaceDescription : componentDescription.implementsInterfaces) {
				System.out.print(" " + interfaceDescription.getName());
			}
			System.out.println();
		}
		System.out.println("Atomic Transitions");
		for (AtomicTransitionDescription<?, ?> atomicTransitionDescription : atomicTransitions
				.values()) {
			System.out.println("\t- " + atomicTransitionDescription.getName());
			System.out
					.println("\t  "
							+ atomicTransitionDescription.sourceComponentType
									.getName()
							+ " -> "
							+ atomicTransitionDescription.targetComponentType
									.getName());
			System.out.println("\t    State (" + atomicTransitionDescription.getTransferVariables() + ")");
		}
	}

	/**
	 * Adds a AtomicTransitionDescription to the TransitionModel.
	 * 
	 * @param atomicTransitionDescription
	 */
	public void add(
			AtomicTransitionDescription<TransitionEnabled, TransitionEnabled> atomicTransitionDescription) {
		atomicTransitions.put(atomicTransitionDescription.getName(),
				atomicTransitionDescription);
	}

	public AtomicTransitionDescription<TransitionEnabled, TransitionEnabled> getTransitionDescription(
			String name) {
		return atomicTransitions.get(name);
	}

}
