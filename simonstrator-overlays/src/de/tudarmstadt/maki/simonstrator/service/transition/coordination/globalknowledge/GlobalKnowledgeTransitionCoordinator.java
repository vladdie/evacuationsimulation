package de.tudarmstadt.maki.simonstrator.service.transition.coordination.globalknowledge;

import java.util.Collection;
import java.util.List;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlan;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlanImpl;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.AtomicTransitionAction;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.TransitionAction;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * 
 * Coordinator relying on global knowledge to trigger transitions (no message
 * transmission). Can be configured to add some additional delay before
 * triggering local transitions.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class GlobalKnowledgeTransitionCoordinator implements TransitionCoordinator, EventHandler {

	private Host host;

	private TransitionEngine tEngine = null;

	private long disseminationDelay = 0;

	private long disseminationDelayVariance = 0;

	private Random rnd;

	public GlobalKnowledgeTransitionCoordinator(Host host) {
		this.host = host;
		this.rnd = Randoms.getRandom(GlobalKnowledgeTransitionCoordinator.class);
	}

	@Override
	public void initialize() {
		// don't care
	}

	@Override
	public void shutdown() {
		// don't care
	}

	@Override
	public Host getHost() {
		return host;
	}

	@Override
	public void executeTransitionPlan(Collection<OverlayContact> affectedNodes, TransitionExecutionPlan executionPlan) {
		/*
		 * Uses global knowledge to iterate over all affected nodes.
		 */

		TransitionAnalyzer analyzer = Monitor.getOrNull(TransitionAnalyzer.class);
		if (analyzer != null) {
			for (TransitionAction action : executionPlan.getActions()) {
				// Retrieve list of clients
				OverlayContacts contacts = new OverlayContacts(affectedNodes);
				// Check for AtomicTransitions
				if (action instanceof AtomicTransitionAction) {
					String proxyName = ((AtomicTransitionAction) action).getProxyName();
					Class<? extends TransitionEnabled> targetClass = ((AtomicTransitionAction) action).getTarget();
					analyzer.onInitiateAtomicTransition(getHost(), proxyName, targetClass, contacts.getIdList());
				}
			}
		}

		for (OverlayContact contact : affectedNodes) {
			Host h = Oracle.getHostByID(contact.getNodeID());
			try {
				GlobalKnowledgeTransitionCoordinator coordinator = h
						.getComponent(GlobalKnowledgeTransitionCoordinator.class);
				coordinator.executeTransitionPlan(executionPlan);
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError("No TransitionCoordinator configured at the target node.");
			}
		}
		
	}

	/**
	 * Execute the plan on the current host.
	 * 
	 * TODO we might add some configurable delay/offset
	 * 
	 * @param executionPlan
	 */
	public void executeTransitionPlan(TransitionExecutionPlan executionPlan) {
		if (tEngine == null) {
			try {
				tEngine = getHost().getComponent(TransitionEngine.class);
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError(
						"Unable to execute TransitionExecutionPlan, no TransitionEngine available at host "
								+ getHost().getId());
			}
		}
		/*
		 * Schedule an event for the local execution of the plan.
		 */
		Event.scheduleWithDelay(getExecutionDelay(), this, executionPlan, 0);
	}

	@Override
	public void eventOccurred(Object content, int type) {
		assert content instanceof TransitionExecutionPlan;
		/*
		 * Simple in-order execution of actions.
		 */
		TransitionExecutionPlan executionPlan = (TransitionExecutionPlan) content;
		List<TransitionAction> actions = executionPlan.getActions();
		for (TransitionAction action : actions) {
			action.executeAction(tEngine);
		}
	}

	/**
	 * Delay before the execution of the current transition
	 * 
	 * @return
	 */
	protected long getExecutionDelay() {
		if (disseminationDelay == 0) {
			return 0;
		} else {
			assert disseminationDelayVariance < disseminationDelay;
			return disseminationDelay
					+ (rnd.nextBoolean() ? -1 : 1) * (long) (rnd.nextDouble() * disseminationDelayVariance);
		}
	}

	/**
	 * Adds a delay before triggering the transition locally
	 * 
	 * @param disseminationDelay
	 */
	public void setDisseminationDelay(long disseminationDelay) {
		this.disseminationDelay = disseminationDelay;
	}

	/**
	 * Adds some variance to the delay before a transition is triggered.
	 * 
	 * @param variance
	 */
	public void setDisseminationDelayVariance(long variance) {
		this.disseminationDelayVariance = variance;
	}

	@Override
	public TransitionExecutionPlan createExecutionPlan() {
		return new TransitionExecutionPlanImpl();
	}

	/**
	 * Factory class for {@link GlobalKnowledgeTransitionCoordinator}s
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class Factory implements HostComponentFactory {

		private long disseminationDelay = 0;

		private long disseminationDelayVariance = 0;

		@Override
		public HostComponent createComponent(Host host) {
			GlobalKnowledgeTransitionCoordinator coord = new GlobalKnowledgeTransitionCoordinator(host);
			coord.setDisseminationDelay(disseminationDelay);
			coord.setDisseminationDelayVariance(disseminationDelayVariance);
			return coord;
		}

		public void setDisseminationDelay(long disseminationDelay) {
			this.disseminationDelay = disseminationDelay;
		}

		public void setDisseminationDelayVariance(long disseminationDelayVariance) {
			this.disseminationDelayVariance = disseminationDelayVariance;
		}


	}

}
