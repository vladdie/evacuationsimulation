package de.tudarmstadt.maki.simonstrator.service.transition.coordination;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;

/**
 * This component takes care of orchestrating remote transitions within a
 * network of nodes. This component has to be initialized on all nodes that are
 * to be controlled by or that control other nodes. It does *not* imply a
 * client/server dependency between components.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface TransitionCoordinator extends HostComponent {

	/**
	 * Execute a remote transitions according to the attached execution plan.
	 * The plan is sent to all remote components and has to be processed there.
	 * Depending on the implementation of the {@link TransitionCoordinator},
	 * this might involve additional coordination with the entity that triggered
	 * the remote transitions (e.g., some transaction-like steps).
	 * 
	 * For simplicity, we assume that a plan is the same for all nodes that are
	 * addressed in this method (e.g., within a plan we do not distinguish
	 * further between nodes).
	 * 
	 * @param affectedNodes
	 *            a list of affected nodes. If our current node is affected
	 *            (included in the list), the plan is also executed locally.
	 * @param executionPlan
	 */
	public void executeTransitionPlan(Collection<OverlayContact> affectedNodes,
			TransitionExecutionPlan executionPlan);

	/**
	 * Returns an execution-plan object to be used to setup the relevant
	 * transitions and their order. This object is not supposed to hold any
	 * state information about the execution of the plan itself, as it is not
	 * cloned before being sent to nodes.
	 * 
	 * @return
	 */
	public TransitionExecutionPlan createExecutionPlan();

}
