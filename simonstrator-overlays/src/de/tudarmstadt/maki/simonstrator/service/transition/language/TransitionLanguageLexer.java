package de.tudarmstadt.maki.simonstrator.service.transition.language;

// Generated from TransitionLanguage.g4 by ANTLR 4.4
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TransitionLanguageLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__21=1, T__20=2, T__19=3, T__18=4, T__17=5, T__16=6, T__15=7, T__14=8, 
		T__13=9, T__12=10, T__11=11, T__10=12, T__9=13, T__8=14, T__7=15, T__6=16, 
		T__5=17, T__4=18, T__3=19, T__2=20, T__1=21, T__0=22, NAME=23, WHITESPACE=24;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'", "'\\u0014'", "'\\u0015'", "'\\u0016'", "'\\u0017'", "'\\u0018'"
	};
	public static final String[] ruleNames = {
		"T__21", "T__20", "T__19", "T__18", "T__17", "T__16", "T__15", "T__14", 
		"T__13", "T__12", "T__11", "T__10", "T__9", "T__8", "T__7", "T__6", "T__5", 
		"T__4", "T__3", "T__2", "T__1", "T__0", "NAME", "WHITESPACE"
	};


	public TransitionLanguageLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "TransitionLanguage.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\32\u00f7\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3"+
		"\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t"+
		"\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f"+
		"\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3\20\3"+
		"\20\3\21\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3\23\3\23\3\23\3\24\3\24\3"+
		"\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3"+
		"\24\3\24\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3"+
		"\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3"+
		"\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\6\30\u00ed"+
		"\n\30\r\30\16\30\u00ee\3\31\6\31\u00f2\n\31\r\31\16\31\u00f3\3\31\3\31"+
		"\2\2\32\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17"+
		"\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31\61\32\3\2\4\6\2/\60C\\a"+
		"ac|\5\2\13\f\16\17\"\"\u00f8\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3"+
		"\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2"+
		"\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37"+
		"\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3"+
		"\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\3\63\3\2\2\2\58\3\2\2\2\7B\3"+
		"\2\2\2\tL\3\2\2\2\13S\3\2\2\2\rW\3\2\2\2\17`\3\2\2\2\21e\3\2\2\2\23g\3"+
		"\2\2\2\25n\3\2\2\2\27u\3\2\2\2\31\u0089\3\2\2\2\33\u009d\3\2\2\2\35\u00a1"+
		"\3\2\2\2\37\u00a6\3\2\2\2!\u00a8\3\2\2\2#\u00ad\3\2\2\2%\u00b0\3\2\2\2"+
		"\'\u00b3\3\2\2\2)\u00c5\3\2\2\2+\u00d9\3\2\2\2-\u00e4\3\2\2\2/\u00ec\3"+
		"\2\2\2\61\u00f1\3\2\2\2\63\64\7h\2\2\64\65\7t\2\2\65\66\7q\2\2\66\67\7"+
		"o\2\2\67\4\3\2\2\289\7e\2\29:\7q\2\2:;\7o\2\2;<\7r\2\2<=\7q\2\2=>\7p\2"+
		"\2>?\7g\2\2?@\7p\2\2@A\7v\2\2A\6\3\2\2\2BC\7k\2\2CD\7p\2\2DE\7v\2\2EF"+
		"\7g\2\2FG\7t\2\2GH\7h\2\2HI\7c\2\2IJ\7e\2\2JK\7g\2\2K\b\3\2\2\2LM\7v\2"+
		"\2MN\7c\2\2NO\7t\2\2OP\7i\2\2PQ\7g\2\2QR\7v\2\2R\n\3\2\2\2ST\7w\2\2TU"+
		"\7u\2\2UV\7g\2\2V\f\3\2\2\2WX\7v\2\2XY\7t\2\2YZ\7c\2\2Z[\7p\2\2[\\\7u"+
		"\2\2\\]\7h\2\2]^\7g\2\2^_\7t\2\2_\16\3\2\2\2`a\7d\2\2ab\7q\2\2bc\7v\2"+
		"\2cd\7j\2\2d\20\3\2\2\2ef\7.\2\2f\22\3\2\2\2gh\7f\2\2hi\7g\2\2ij\7h\2"+
		"\2jk\7k\2\2kl\7p\2\2lm\7g\2\2m\24\3\2\2\2no\7u\2\2op\7q\2\2pq\7w\2\2q"+
		"r\7t\2\2rs\7e\2\2st\7g\2\2t\26\3\2\2\2uv\7d\2\2vw\7n\2\2wx\7q\2\2xy\7"+
		"e\2\2yz\7m\2\2z{\7\"\2\2{|\7e\2\2|}\7q\2\2}~\7o\2\2~\177\7o\2\2\177\u0080"+
		"\7w\2\2\u0080\u0081\7p\2\2\u0081\u0082\7k\2\2\u0082\u0083\7e\2\2\u0083"+
		"\u0084\7c\2\2\u0084\u0085\7v\2\2\u0085\u0086\7k\2\2\u0086\u0087\7q\2\2"+
		"\u0087\u0088\7p\2\2\u0088\30\3\2\2\2\u0089\u008a\7r\2\2\u008a\u008b\7"+
		"c\2\2\u008b\u008c\7t\2\2\u008c\u008d\7c\2\2\u008d\u008e\7n\2\2\u008e\u008f"+
		"\7n\2\2\u008f\u0090\7g\2\2\u0090\u0091\7n\2\2\u0091\u0092\7\"\2\2\u0092"+
		"\u0093\7v\2\2\u0093\u0094\7t\2\2\u0094\u0095\7c\2\2\u0095\u0096\7p\2\2"+
		"\u0096\u0097\7u\2\2\u0097\u0098\7k\2\2\u0098\u0099\7v\2\2\u0099\u009a"+
		"\7k\2\2\u009a\u009b\7q\2\2\u009b\u009c\7p\2\2\u009c\32\3\2\2\2\u009d\u009e"+
		"\7h\2\2\u009e\u009f\7q\2\2\u009f\u00a0\7t\2\2\u00a0\34\3\2\2\2\u00a1\u00a2"+
		"\7h\2\2\u00a2\u00a3\7n\2\2\u00a3\u00a4\7k\2\2\u00a4\u00a5\7r\2\2\u00a5"+
		"\36\3\2\2\2\u00a6\u00a7\7=\2\2\u00a7 \3\2\2\2\u00a8\u00a9\7y\2\2\u00a9"+
		"\u00aa\7k\2\2\u00aa\u00ab\7v\2\2\u00ab\u00ac\7j\2\2\u00ac\"\3\2\2\2\u00ad"+
		"\u00ae\7c\2\2\u00ae\u00af\7u\2\2\u00af$\3\2\2\2\u00b0\u00b1\7v\2\2\u00b1"+
		"\u00b2\7q\2\2\u00b2&\3\2\2\2\u00b3\u00b4\7c\2\2\u00b4\u00b5\7v\2\2\u00b5"+
		"\u00b6\7q\2\2\u00b6\u00b7\7o\2\2\u00b7\u00b8\7k\2\2\u00b8\u00b9\7e\2\2"+
		"\u00b9\u00ba\7\"\2\2\u00ba\u00bb\7v\2\2\u00bb\u00bc\7t\2\2\u00bc\u00bd"+
		"\7c\2\2\u00bd\u00be\7p\2\2\u00be\u00bf\7u\2\2\u00bf\u00c0\7k\2\2\u00c0"+
		"\u00c1\7v\2\2\u00c1\u00c2\7k\2\2\u00c2\u00c3\7q\2\2\u00c3\u00c4\7p\2\2"+
		"\u00c4(\3\2\2\2\u00c5\u00c6\7y\2\2\u00c6\u00c7\7k\2\2\u00c7\u00c8\7v\2"+
		"\2\u00c8\u00c9\7j\2\2\u00c9\u00ca\7\"\2\2\u00ca\u00cb\7v\2\2\u00cb\u00cc"+
		"\7t\2\2\u00cc\u00cd\7c\2\2\u00cd\u00ce\7p\2\2\u00ce\u00cf\7u\2\2\u00cf"+
		"\u00d0\7h\2\2\u00d0\u00d1\7q\2\2\u00d1\u00d2\7t\2\2\u00d2\u00d3\7o\2\2"+
		"\u00d3\u00d4\7c\2\2\u00d4\u00d5\7v\2\2\u00d5\u00d6\7k\2\2\u00d6\u00d7"+
		"\7q\2\2\u00d7\u00d8\7p\2\2\u00d8*\3\2\2\2\u00d9\u00da\7k\2\2\u00da\u00db"+
		"\7o\2\2\u00db\u00dc\7r\2\2\u00dc\u00dd\7n\2\2\u00dd\u00de\7g\2\2\u00de"+
		"\u00df\7o\2\2\u00df\u00e0\7g\2\2\u00e0\u00e1\7p\2\2\u00e1\u00e2\7v\2\2"+
		"\u00e2\u00e3\7u\2\2\u00e3,\3\2\2\2\u00e4\u00e5\7t\2\2\u00e5\u00e6\7w\2"+
		"\2\u00e6\u00e7\7p\2\2\u00e7\u00e8\7t\2\2\u00e8\u00e9\7w\2\2\u00e9\u00ea"+
		"\7p\2\2\u00ea.\3\2\2\2\u00eb\u00ed\t\2\2\2\u00ec\u00eb\3\2\2\2\u00ed\u00ee"+
		"\3\2\2\2\u00ee\u00ec\3\2\2\2\u00ee\u00ef\3\2\2\2\u00ef\60\3\2\2\2\u00f0"+
		"\u00f2\t\3\2\2\u00f1\u00f0\3\2\2\2\u00f2\u00f3\3\2\2\2\u00f3\u00f1\3\2"+
		"\2\2\u00f3\u00f4\3\2\2\2\u00f4\u00f5\3\2\2\2\u00f5\u00f6\b\31\2\2\u00f6"+
		"\62\3\2\2\2\5\2\u00ee\u00f3\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}