package de.tudarmstadt.maki.simonstrator.service.transition.coordination.action;

import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;

/**
 * An action corresponding to an atomic transition.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class AtomicTransitionAction implements TransitionAction {

	private static final long serialVersionUID = 1L;

	private Class<? extends TransitionEnabled> target;

	private String proxyName;

	public <T extends TransitionEnabled> AtomicTransitionAction(String proxyName, Class<T> target) {
		this.target = target;
		this.proxyName = proxyName;
	}

	@Override
	public void executeAction(TransitionEngine localEngine) {
		localEngine.executeAtomicTransition(proxyName, target);
	}

	public String getProxyName() {
		return proxyName;
	}

	public Class<? extends TransitionEnabled> getTarget() {
		return target;
	}

	@Override
	public int getTransmissionSize() {
		return 16; // Assuming 8 byte IDs for proxies and classes.
	}

}
