package de.tudarmstadt.maki.simonstrator.service.transition.coordination.besteffort;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlan;

/**
 * This message carries an execution plan
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class ExecutionPlanMsg extends AbstractOverlayMessage {

	private static final long serialVersionUID = 1L;

	private TransitionExecutionPlan executionPlan;

	/**
	 * 
	 * @param sender
	 * @param receiver
	 * @param executionPlan
	 */
	public ExecutionPlanMsg(OverlayContact sender, OverlayContact receiver, TransitionExecutionPlan executionPlan) {
		super(sender, receiver);
		this.executionPlan = executionPlan;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	public TransitionExecutionPlan getExecutionPlan() {
		return executionPlan;
	}

	@Override
	public long getSize() {
		return super.getSize() + executionPlan.getTransmissionSize();
	}

}
