package de.tudarmstadt.maki.simonstrator.service.transition.local;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.component.transition.AtomicTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;

/**
 * An implementation of an atomic transition internally used by the
 * {@link TransitionEngine} - maintains the state of components and triggers the
 * lifecycle-methods. This is a simplified version of the previous
 * flip-transition with a simplified lifecycle - init() is now the
 * constructor-step, start and stop are the remaining lifecycle methods. Cleanup
 * is no longer provided (it has to be part of stop() in your implementations).
 * 
 * State transfer got a major overhaul, now supporting annotations:
 * {@link TransferState}, for both: types and constructors.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class AtomicTransitionExecution<T extends TransitionEnabled> implements TransitionEnabled.Callback {

	@SuppressWarnings("rawtypes")
	private AtomicTransition customTransition;

	private ComponentProxy<?> proxy;

	private TransitionEnabled sourceComponent;

	private T targetComponent;

	private Class<T> targetType;

	private Class<?> sourceType;

	private ComponentState sourceState = ComponentState.RUNNING;

	private ComponentState targetState = ComponentState.UNDEFINED;

	private TransitionState transitionState = TransitionState.UNDEFINED;

	public enum ComponentState {
		UNDEFINED, STARTING, RUNNING, STOPPING, FINISHED;
	}

	public enum TransitionState {
		UNDEFINED, INIT, START_TARGET, STOP_SOURCE, ABORT, FINISH;
	}

	/**
	 * Create a transition execution object for the given proxy and the
	 * targetType
	 * 
	 * @param proxy
	 * @param targetType
	 */
	public AtomicTransitionExecution(ComponentProxy<?> proxy,
			Class<T> targetType) {
		this.proxy = proxy;
		try {
			this.customTransition = proxy.getCustomTransition(targetType);
		} catch (InstantiationException | IllegalAccessException e) {
			// no custom transition found
		}
		this.targetType = targetType;
		this.sourceType = proxy.getActiveComponent().getClass();
	}

	/**
	 * Initializes the target component and transfers state information.
	 * 
	 * @return
	 * 
	 * @return an instantiated target component
	 */
	@SuppressWarnings("unchecked")
	private void initializeTarget() {
		/*
		 * Inform the proxy that he is affected by a transition. This ensures
		 * that each proxy is only affected by one transtion at a time.
		 */
		if (!proxy.testAndStartTransition()) {
			return;
		}

		changeTransitionState(TransitionState.INIT);
		sourceComponent = proxy.getActiveComponent();

		/*
		 * Find all annotated state of the source component. Has to be
		 * accessible via corresponding get-Methods
		 */
		Map<String, Field> states = new LinkedHashMap<>();
		Set<Field> fields = findFields(sourceType, TransferState.class);
		for (Field field : fields) {
			states.put(field.getAnnotation(TransferState.class).value()[0], field);
		}

		/*
		 * Go through constructors: if annotated constructor is present, match
		 * with found annotations and use. If not, use default constructor and
		 * target state transfer.
		 */
		Constructor<?>[] constructors = targetType.getConstructors();
		Constructor<?> annotatedConstructor = null;
		for (int i = 0; i < constructors.length; i++) {
			Constructor<?> constructor = constructors[i];
			if (constructor.isAnnotationPresent(TransferState.class)) {
				if (annotatedConstructor != null) {
					throw new AssertionError("The @TransferState annotation is only allowed on ONE constuctor!");
				}
				annotatedConstructor = constructor;
			}
		}

		/*
		 * Check, if some or all of the target fields are to be set via the
		 * constructor - and build the targetType
		 */
		targetComponent = null;
		if (annotatedConstructor != null) {
			TransferState toTransfer = annotatedConstructor
					.getAnnotation(TransferState.class);
			Object[] arguments = new Object[toTransfer.value().length];
			for (int i = 0; i < arguments.length; i++) {
				// If used in constructor, do not attempt to transfer it lateron
				states.remove(toTransfer.value()[i]);
				try {
					Method m = sourceType.getMethod("get" + toTransfer.value()[i]);
					// Class<?> type =
					// annotatedConstructor.getParameterTypes()[i];
					Object value = m.invoke(sourceComponent);
					arguments[i] = value; // type.cast(value);
				} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException | ClassCastException e) {
					throw new AssertionError("State transfer failed for " + toTransfer.value()[i], e);
				}
			}
			// Finally - call the constructor
			try {
				targetComponent = targetType.cast(annotatedConstructor.newInstance(arguments));
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				throw new AssertionError("Unable to instantiate component.", e);
			}
		} else {
			try {
				Constructor<?> noArgConstructor = targetType.getConstructor();
				targetComponent = targetType.cast(noArgConstructor
						.newInstance());
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException e) {
				throw new AssertionError(
						"No annotated constructor and no default constructor (public) found.");
			}
		}
		
		assert targetComponent != null : "target has not been constructed!";
		
		/*
		 * Find all fields at the target component that are annotated with
		 * TransferState. It is assumed that the fields are accessible at the
		 * source component through public getter-methods.
		 */
		Set<Field> targetFields = findFields(targetType, TransferState.class);
		for (Field field : targetFields) {
			if (states.containsKey(field.getAnnotation(TransferState.class).value()[0])) {
				/*
				 * Transfer state based on the target component
				 */
				try {
					String methodName = field.getAnnotation(TransferState.class).value()[0];
					Method srcMethod = sourceType.getMethod("get" + methodName);
					Object value = srcMethod.invoke(sourceComponent);
					Method targetMethod = targetType.getMethod("set" + methodName, field.getType());
					targetMethod.invoke(targetComponent, value);
					Monitor.log(AtomicTransitionExecution.class, Level.INFO, "Transferred state: %s, value: %s",
							methodName, value);
				} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					throw new AssertionError(
							"State transfer of " + field.getAnnotation(TransferState.class).value()[0] + " failed.", e);
				}
			}
		}

		/*
		 * Do custom state transfer
		 */
		if (customTransition != null) {
			customTransition.transferState(sourceComponent, targetComponent);
		}
	}

	/**
	 * Starts the current transition - including object instantiation and state
	 * transfer.
	 */
	public void startTransition() {
		initializeTarget();
		if (targetComponent == null) {
			// TODO What todo now? who to inform?
			Monitor.log(AtomicTransitionExecution.class, Level.WARN,
					"Proxy %s is already in transition, new transition is ignored.", proxy);
			changeTransitionState(TransitionState.ABORT);
			return;
		}
		/*
		 * Start the transition
		 */
		changeTransitionState(TransitionState.START_TARGET);
	}

	/**
	 * State machine for the transition.
	 * 
	 * @param targetTransitionState
	 */
	@SuppressWarnings("unchecked")
	protected void changeTransitionState(TransitionState targetTransitionState) {
		switch (targetTransitionState) {
		case INIT:
			assert transitionState == TransitionState.UNDEFINED;
			transitionState = TransitionState.INIT;
			break;
			
		case START_TARGET:
			assert sourceState == ComponentState.RUNNING;
			targetState = ComponentState.STARTING;
			transitionState = TransitionState.START_TARGET;
			targetComponent.startMechanism(this);
			break;

		case STOP_SOURCE:
			assert targetState == ComponentState.RUNNING;
			assert sourceState == ComponentState.RUNNING;
			transitionState = TransitionState.STOP_SOURCE;
			/*
			 * Switch to new active component!
			 */
			proxy.setActiveComponent(targetComponent);
			sourceState = ComponentState.STOPPING;
			sourceComponent.stopMechanism(this);
			break;

		case FINISH:
			transitionState = TransitionState.FINISH;
			assert targetState == ComponentState.RUNNING;
			assert sourceState == ComponentState.FINISHED || sourceState == ComponentState.UNDEFINED;
			if (customTransition != null) {
				customTransition.transitionFinished(sourceComponent, targetComponent, true);
			}
			proxy.finishTransition();
			targetComponent = null;
			sourceComponent = null;
			proxy = null;
			customTransition = null;
			break;

		case ABORT:
			transitionState = TransitionState.ABORT;
			if (customTransition != null) {
				customTransition.transitionFinished(sourceComponent, targetComponent, false);
			}
			proxy.finishTransition();
			targetComponent = null;
			sourceComponent = null;
			proxy = null;
			customTransition = null;
			break;

		default:
			throw new AssertionError("Undefined state.");
		}
	}

	@Override
	public void finished(boolean successful) {
		/*
		 * Callback of TransitionEnabled
		 */
		switch (transitionState) {
		case START_TARGET:
			assert targetState == ComponentState.STARTING;
			assert sourceState == ComponentState.RUNNING;
			if (!successful) {
				targetState = ComponentState.UNDEFINED;
				changeTransitionState(TransitionState.ABORT);
			} else {
				targetState = ComponentState.RUNNING;
				changeTransitionState(TransitionState.STOP_SOURCE);
			}
			break;

		case STOP_SOURCE:
			assert targetState == ComponentState.RUNNING;
			assert sourceState == ComponentState.STOPPING;
			if (!successful) {
				// We still consider the transition to be finished!
				sourceState = ComponentState.UNDEFINED;
				changeTransitionState(TransitionState.FINISH);
			} else {
				sourceState = ComponentState.FINISHED;
				changeTransitionState(TransitionState.FINISH);
			}
			break;

		default:
			throw new AssertionError("Invalid callback!");
		}

	}

	/**
	 * State of this transition
	 * 
	 * @return
	 */
	public TransitionState getTransitionState() {
		return transitionState;
	}

	/**
	 * @return null safe set
	 */
	private Set<Field> findFields(Class<?> classs,
			Class<? extends Annotation> ann) {
		Set<Field> set = new LinkedHashSet<>();
		Class<?> c = classs;
		while (c != null) {
			for (Field field : c.getDeclaredFields()) {
				if (field.isAnnotationPresent(ann)) {
					set.add(field);
				}
			}
			c = c.getSuperclass();
		}
		return set;
	}

}
