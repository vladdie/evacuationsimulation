package de.tudarmstadt.maki.simonstrator.service.transition.model;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;


public class InterfaceDescription implements Comparable<InterfaceDescription> {
	private final String name;
	private final Map<String, MethodDescription> methods = new TreeMap<String, MethodDescription>();
	
	public InterfaceDescription(String name) {
		this.name = name;
	}
	
	public void addMethod(String name) {
		methods.put(name, new MethodDescription(name));
	}

	@Override
	public int compareTo(InterfaceDescription arg) {
		return this.name.compareTo(arg.name);
	}

	public MethodDescription getMethod(String methodName) {
		return methods.get(methodName);
	}
	
	public String getName() {
		return name;
	}
	
	public Collection<MethodDescription> getMethodDescriptions() {
		return Collections.unmodifiableCollection(methods.values());
	}
}
