package de.tudarmstadt.maki.simonstrator.service.transition.model;

public class MethodDescription implements Comparable<MethodDescription> {
	private final String name;
	
	public MethodDescription(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	@Override
	public int compareTo(MethodDescription o) {
		return name.compareTo(o.name);
	}
}
