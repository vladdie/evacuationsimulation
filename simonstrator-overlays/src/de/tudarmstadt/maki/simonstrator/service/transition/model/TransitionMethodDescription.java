package de.tudarmstadt.maki.simonstrator.service.transition.model;

import de.tudarmstadt.maki.simonstrator.service.transition.model.MethodDescription;

public class TransitionMethodDescription implements Comparable<TransitionMethodDescription> {

	private final MethodDescription methodDescription;
	
	public TransitionMethodDescription(MethodDescription methodDescription) {
		this.methodDescription = methodDescription;
	}

	@Override
	public int compareTo(TransitionMethodDescription o) {
		return methodDescription.compareTo(o.methodDescription);
	}
}
