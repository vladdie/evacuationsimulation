package de.tudarmstadt.maki.simonstrator.service.transition.coordination;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.TransitionAction;

/**
 * Bundler for a set of {@link TransitionAction}s that are executed by nodes
 * receiving a {@link TransitionExecutionPlan} via their local
 * {@link TransitionCoordinator}
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface TransitionExecutionPlan extends Transmitable {

	/**
	 * Add an action
	 * 
	 * @param action
	 */
	public void addAction(TransitionAction action);

	/**
	 * Ordered list of actions.
	 * 
	 * @return
	 */
	public List<TransitionAction> getActions();

}
