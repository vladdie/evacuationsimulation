package de.tudarmstadt.maki.simonstrator.service.transition.language;

// Generated from TransitionLanguage.g4 by ANTLR 4.4
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link TransitionLanguageParser}.
 */
public interface TransitionLanguageListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link TransitionLanguageParser#methods}.
	 * @param ctx the parse tree
	 */
	void enterMethods(@NotNull TransitionLanguageParser.MethodsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TransitionLanguageParser#methods}.
	 * @param ctx the parse tree
	 */
	void exitMethods(@NotNull TransitionLanguageParser.MethodsContext ctx);
	/**
	 * Enter a parse tree produced by {@link TransitionLanguageParser#parallelTransition}.
	 * @param ctx the parse tree
	 */
	void enterParallelTransition(@NotNull TransitionLanguageParser.ParallelTransitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TransitionLanguageParser#parallelTransition}.
	 * @param ctx the parse tree
	 */
	void exitParallelTransition(@NotNull TransitionLanguageParser.ParallelTransitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TransitionLanguageParser#inter}.
	 * @param ctx the parse tree
	 */
	void enterInter(@NotNull TransitionLanguageParser.InterContext ctx);
	/**
	 * Exit a parse tree produced by {@link TransitionLanguageParser#inter}.
	 * @param ctx the parse tree
	 */
	void exitInter(@NotNull TransitionLanguageParser.InterContext ctx);
	/**
	 * Enter a parse tree produced by {@link TransitionLanguageParser#define}.
	 * @param ctx the parse tree
	 */
	void enterDefine(@NotNull TransitionLanguageParser.DefineContext ctx);
	/**
	 * Exit a parse tree produced by {@link TransitionLanguageParser#define}.
	 * @param ctx the parse tree
	 */
	void exitDefine(@NotNull TransitionLanguageParser.DefineContext ctx);
	/**
	 * Enter a parse tree produced by {@link TransitionLanguageParser#transfer}.
	 * @param ctx the parse tree
	 */
	void enterTransfer(@NotNull TransitionLanguageParser.TransferContext ctx);
	/**
	 * Exit a parse tree produced by {@link TransitionLanguageParser#transfer}.
	 * @param ctx the parse tree
	 */
	void exitTransfer(@NotNull TransitionLanguageParser.TransferContext ctx);
	/**
	 * Enter a parse tree produced by {@link TransitionLanguageParser#component}.
	 * @param ctx the parse tree
	 */
	void enterComponent(@NotNull TransitionLanguageParser.ComponentContext ctx);
	/**
	 * Exit a parse tree produced by {@link TransitionLanguageParser#component}.
	 * @param ctx the parse tree
	 */
	void exitComponent(@NotNull TransitionLanguageParser.ComponentContext ctx);
	/**
	 * Enter a parse tree produced by {@link TransitionLanguageParser#atomicTransition}.
	 * @param ctx the parse tree
	 */
	void enterAtomicTransition(@NotNull TransitionLanguageParser.AtomicTransitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link TransitionLanguageParser#atomicTransition}.
	 * @param ctx the parse tree
	 */
	void exitAtomicTransition(@NotNull TransitionLanguageParser.AtomicTransitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link TransitionLanguageParser#method}.
	 * @param ctx the parse tree
	 */
	void enterMethod(@NotNull TransitionLanguageParser.MethodContext ctx);
	/**
	 * Exit a parse tree produced by {@link TransitionLanguageParser#method}.
	 * @param ctx the parse tree
	 */
	void exitMethod(@NotNull TransitionLanguageParser.MethodContext ctx);
	/**
	 * Enter a parse tree produced by {@link TransitionLanguageParser#childs}.
	 * @param ctx the parse tree
	 */
	void enterChilds(@NotNull TransitionLanguageParser.ChildsContext ctx);
	/**
	 * Exit a parse tree produced by {@link TransitionLanguageParser#childs}.
	 * @param ctx the parse tree
	 */
	void exitChilds(@NotNull TransitionLanguageParser.ChildsContext ctx);
}