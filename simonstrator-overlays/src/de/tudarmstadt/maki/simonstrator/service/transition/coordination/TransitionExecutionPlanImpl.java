package de.tudarmstadt.maki.simonstrator.service.transition.coordination;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.TransitionAction;

/**
 * Basic implementation of a {@link TransitionExecutionPlan}
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class TransitionExecutionPlanImpl implements TransitionExecutionPlan {

	private static final long serialVersionUID = 1L;

	private List<TransitionAction> actions = new LinkedList<>();

	private transient int _size = -1;

	@Override
	public int getTransmissionSize() {
		if (_size == -1) {
			_size = 0;
			for (TransitionAction transitionAction : actions) {
				_size += transitionAction.getTransmissionSize();
			}
		}
		return _size;
	}

	@Override
	public void addAction(TransitionAction action) {
		actions.add(action);
	}

	@Override
	public List<TransitionAction> getActions() {
		return Collections.unmodifiableList(actions);
	}

}
