package de.tudarmstadt.maki.simonstrator.service.transition.language;

import org.antlr.v4.runtime.misc.NotNull;

import de.tudarmstadt.maki.simonstrator.api.component.transition.AtomicTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.service.transition.model.AtomicTransitionDescription;
import de.tudarmstadt.maki.simonstrator.service.transition.model.ComponentDescription;
import de.tudarmstadt.maki.simonstrator.service.transition.model.InterfaceDescription;
import de.tudarmstadt.maki.simonstrator.service.transition.model.MethodDescription;
import de.tudarmstadt.maki.simonstrator.service.transition.model.TransitionMethodDescription;
import de.tudarmstadt.maki.simonstrator.service.transition.model.TransitionModel;

/**
 * This class transfers the intermediate representation of the transition
 * language ({@link TransitionLanguageParser.DefineContext}) to a
 * {@link TransitionModel}, which contains the transition model and therefore
 * all knowledge about the available/modeled transitions.
 * 
 * @author Alexander Froemmgen
 * 
 */
public class TransitionLanguageRealListener extends
		TransitionLanguageBaseListener {
	private final TransitionModel transitionModel;

	private InterfaceDescription currentInterfaceDescription;
	private AtomicTransitionDescription<TransitionEnabled, TransitionEnabled> currentAtomicTransitionDescription;

	public TransitionLanguageRealListener(TransitionModel transitionModel) {
		this.transitionModel = transitionModel;
	}

	@Override
	public void enterInter(@NotNull TransitionLanguageParser.InterContext ctx) {
		String interfaceName = ctx.NAME().getText();
		currentInterfaceDescription = new InterfaceDescription(interfaceName);

		super.enterInter(ctx);
	}

	@Override
	public void exitInter(@NotNull TransitionLanguageParser.InterContext ctx) {
		transitionModel.add(currentInterfaceDescription);
		currentInterfaceDescription = null;

		super.exitInter(ctx);
	}

	@Override
	public void enterMethods(
			@NotNull TransitionLanguageParser.MethodsContext ctx) {
		currentInterfaceDescription.addMethod(ctx.NAME().getText());

		super.enterMethods(ctx);
	}

	@Override
	public void enterComponent(
			@NotNull TransitionLanguageParser.ComponentContext ctx) {
		ComponentDescription componentDescription = new ComponentDescription(
				ctx.NAME(0).getText());
		transitionModel.add(componentDescription);

		if (ctx.NAME().size() > 1) {
			InterfaceDescription interfaceDescription = transitionModel
					.getInterfaceDescription(ctx.NAME(1).getText());
			
			if(null == interfaceDescription) {
				throw new InvalidTransitionDescriptionException("The interface " + ctx.NAME(1).getText() + " is not definned but used by component " + componentDescription.name);
			}
			componentDescription.addImplementsInterface(interfaceDescription);
		}

		super.enterComponent(ctx);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void enterAtomicTransition(
			@NotNull TransitionLanguageParser.AtomicTransitionContext ctx) {
		String nameStr = ctx.NAME(0).getText();
		String fromStr = ctx.NAME(1).getText();
		String toStr = ctx.NAME(2).getText();
		String typeStr = ctx.children.get(7).getText();
		if (!typeStr.equals("runrun") && !typeStr.equals("flip")) { // TODO no hard index ?
			throw new InvalidTransitionDescriptionException("The transition type " + typeStr + " is not valid for transition " + nameStr);
		} 

		Class<? extends TransitionEnabled> fromClass;
		Class<? extends TransitionEnabled> toClass;
		Class<? extends AtomicTransition> transitionClass;

		try {
			fromClass = (Class<? extends TransitionEnabled>) Class
					.forName(fromStr);
			toClass = (Class<? extends TransitionEnabled>) Class.forName(toStr);
			transitionClass = (Class<? extends AtomicTransition>) Class
					.forName(nameStr);
		} catch (ClassCastException e) {
			throw new InvalidTransitionDescriptionException(
					"A class for transition " + nameStr
							+ " does not implement the necessary interfaces. "
							+ e.getMessage());
		} catch (ClassNotFoundException e) {
			throw new InvalidTransitionDescriptionException(
					"A class for transition " + nameStr
							+ " could not be found. " + e.getMessage());
		}

		currentAtomicTransitionDescription = new AtomicTransitionDescription<TransitionEnabled, TransitionEnabled>(
				nameStr, fromClass, toClass, transitionClass);
		transitionModel.add(currentAtomicTransitionDescription);

		super.enterAtomicTransition(ctx);

	}

	@Override
	public void enterTransfer(
			@NotNull TransitionLanguageParser.TransferContext ctx) {
		currentAtomicTransitionDescription.addTransferVariable(ctx.NAME(0)
				.getText());

		super.enterTransfer(ctx);
	}

	@Override
	public void exitAtomicTransition(
			@NotNull TransitionLanguageParser.AtomicTransitionContext ctx) {
		currentAtomicTransitionDescription = null;

		super.exitAtomicTransition(ctx);
	}

	@Override
	public void enterMethod(@NotNull TransitionLanguageParser.MethodContext ctx) {
		// TODO Ensure that the MethodDescriptions are available for source and
		// target component.
		MethodDescription methodDescription = transitionModel
				.getMethodDescription(
						currentAtomicTransitionDescription.sourceComponentType
								.getName(), ctx.NAME().getText());
		TransitionMethodDescription transitionMethodDescription = new TransitionMethodDescription(
				methodDescription);
		currentAtomicTransitionDescription
				.addMethod(transitionMethodDescription);

		super.enterMethod(ctx);
	}
}
