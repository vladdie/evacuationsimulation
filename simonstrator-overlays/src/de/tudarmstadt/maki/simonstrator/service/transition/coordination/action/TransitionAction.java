package de.tudarmstadt.maki.simonstrator.service.transition.coordination.action;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlan;

/**
 * Template for an action that can be added to an
 * {@link TransitionExecutionPlan}. This allows overlays to actually specify
 * their own actions and associated execution code. Via the
 * {@link TransitionEngine} getHost-method, such code could even operate on the
 * overlay itself.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface TransitionAction extends Transmitable {

	/**
	 * This method has to perform relevant actions using the engine (and, via
	 * the host-reference, using additional state information. It is very
	 * important that NO state is saved in-between calls of this method (i.e.,
	 * in the Action object itself), as the object is not cloned but instead
	 * used on each receiving node.
	 * 
	 * @param localEngine
	 */
	public void executeAction(TransitionEngine localEngine);

}
