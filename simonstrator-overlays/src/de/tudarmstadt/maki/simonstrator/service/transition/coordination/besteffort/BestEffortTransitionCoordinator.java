package de.tudarmstadt.maki.simonstrator.service.transition.coordination.besteffort;

import java.util.Collection;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.api.IOverlayMessageAnalyzer;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlan;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlanImpl;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.AtomicTransitionAction;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.TransitionAction;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Implementation of a {@link TransitionCoordinator} using unicast messages on a
 * dedicated port to distribute {@link TransitionExecutionPlan}s.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class BestEffortTransitionCoordinator extends AbstractOverlayNode
		implements TransitionCoordinator, TransMessageListener {

	private int port;

	private BasicOverlayContact ownContact;

	private TransitionEngine tEngine = null;

	public BestEffortTransitionCoordinator(Host host, int port) {
		super(host);
		this.port = port;
	}

	@Override
	public void initialize() {
		/*
		 * Add all local network interfaces to our own overlay contact.
		 */
		ownContact = new BasicOverlayContact(getHost().getId());
		for (NetInterface netInterface : getHost().getNetworkComponent().getNetworkInterfaces()) {
			ownContact.addInformation(netInterface.getName(), netInterface.getLocalInetAddress(), port);
			try {
				UDP udp = getAndBindUDP(netInterface.getLocalInetAddress(), port, null);
				udp.setTransportMessageListener(this);
			} catch (ProtocolNotAvailableException e) {
				throw new AssertionError("This should not happen...");
			}
		}
	}

	@Override
	public void executeTransitionPlan(Collection<OverlayContact> affectedNodes,
			TransitionExecutionPlan executionPlan) {
		/*
		 * Send the execution plan via unicast-messages to all affected nodes.
		 * 
		 * FIXME for A LOT of nodes in parallel, this might run into problems
		 * w.r.t. the queue sizes of the underlay. In such cases, we might need
		 * to introduce some additional delay in-between individual messages.
		 */

		TransitionAnalyzer analyzer = Monitor.getOrNull(TransitionAnalyzer.class);
		if (analyzer != null) {
			for (TransitionAction action : executionPlan.getActions()) {
				// Retrieve list of clients
				OverlayContacts contacts = new OverlayContacts(affectedNodes);
				// Check for AtomicTransitions
				if (action instanceof AtomicTransitionAction) {
					String proxyName = ((AtomicTransitionAction) action).getProxyName();
					Class<? extends TransitionEnabled> targetClass = ((AtomicTransitionAction) action).getTarget();
					analyzer.onInitiateAtomicTransition(getHost(), proxyName, targetClass, contacts.getIdList());
				}
			}
		}

		for (OverlayContact overlayContact : affectedNodes) {
			if (overlayContact.equals(ownContact)) {
				// Local execution
				executePlan(executionPlan);
			} else {
				try {
					sendExecutionPlan(overlayContact, executionPlan);
				} catch (ProtocolNotAvailableException e) {
					throw new AssertionError("Unreachable contact " + overlayContact);
				}
			}
		}

	}

	/**
	 * Search for a {@link NetInterfaceName} that is available on our local
	 * contact as well as the remote node and try sending via this interface (in
	 * this simple implementation, we use UDP without any ACKs or retries).
	 * 
	 * @param to
	 * @throws ProtocolNotAvailableException
	 */
	protected void sendExecutionPlan(OverlayContact to, TransitionExecutionPlan executionPlan)
			throws ProtocolNotAvailableException {
		for (NetInterfaceName netInterface : to.getInterfaces()) {
			if (ownContact.getInterfaces().contains(netInterface)) {
				UDP udp = getAndBindUDP(ownContact.getNetID(netInterface), port, null);
				ExecutionPlanMsg msg = new ExecutionPlanMsg(ownContact, to, executionPlan);
				udp.send(msg, to.getNetID(netInterface), port);
				if (Monitor.hasAnalyzer(IOverlayMessageAnalyzer.class)) {
					Monitor.getOrNull(IOverlayMessageAnalyzer.class).onSentOverlayMessage(msg, getHost(), netInterface);
				}
				return;
			}
		}
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		assert msg instanceof ExecutionPlanMsg;
		ExecutionPlanMsg eMsg = (ExecutionPlanMsg) msg;
		executePlan(eMsg.getExecutionPlan());
		if (Monitor.hasAnalyzer(IOverlayMessageAnalyzer.class)) {
			Monitor.getOrNull(IOverlayMessageAnalyzer.class).onReceivedOverlayMessage(eMsg, getHost());
		}
	}

	/**
	 * Execute a plan locally
	 * 
	 * @param executionPlan
	 */
	protected void executePlan(TransitionExecutionPlan executionPlan) {
		if (tEngine == null) {
			try {
				tEngine = getHost().getComponent(TransitionEngine.class);
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError(
						"Unable to execute TransitionExecutionPlan, no TransitionEngine available at host "
								+ getHost().getId());
			}
		}

		/*
		 * Simple in-order execution of actions.
		 */
		List<TransitionAction> actions = executionPlan.getActions();
		for (TransitionAction action : actions) {
			action.executeAction(tEngine);
		}
	}

	@Override
	public TransitionExecutionPlan createExecutionPlan() {
		return new TransitionExecutionPlanImpl();
	}

	@Override
	public BasicOverlayContact getLocalOverlayContact() {
		return ownContact;
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// don't care
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		// don't care
	}

	public static class Factory implements HostComponentFactory {

		private int port = 7517;

		@Override
		public HostComponent createComponent(Host host) {
			return new BestEffortTransitionCoordinator(host, port);
		}

	}

}
