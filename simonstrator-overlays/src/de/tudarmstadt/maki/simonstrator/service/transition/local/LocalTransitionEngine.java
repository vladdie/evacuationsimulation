package de.tudarmstadt.maki.simonstrator.service.transition.local;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Proxy;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.transition.AtomicTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transition.MechanismState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.SelfTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionListener;

/**
 * Local maintainer of proxies and executor of transitions. Now completely
 * decoupled from any global engine, model or description to ease development
 * and programmatic interaction with the local engine.
 * 
 * This engine only knows atomicTransitions - everything else needs to be
 * orchestrated from the outside to keep this one as simple as possible.
 * 
 * @author Alexander Froemmgen, Bjoern Richerzhagen
 * 
 */
public class LocalTransitionEngine implements TransitionEngine {

	/**
	 * Maps proxy name to proxy
	 */
	private final Map<String, ComponentProxy<?>> proxies = new LinkedHashMap<>();

	private final Map<String, TransitionEnabled> proxyFacades = new LinkedHashMap<>();

	private Host host;

	public LocalTransitionEngine(Host host) {
		this.host = host;
	}

	@Override
	public void initialize() {
		//
	}

	@Override
	public boolean proxyExists(String proxyName) {
		return proxies.containsKey(proxyName);
	}

	@Override
	public <T extends TransitionEnabled> T getProxy(String proxyName, Class<T> proxyInterface) {
		if (!proxyExists(proxyName)) {
			return null;
		}
		return proxyInterface.cast(proxyFacades.get(proxyName));
	}

	/**
	 * This is used for some visualization tricks - it enables full access to
	 * the currently active mechanism within a proxy. This should NEVER be used
	 * within production code of a service or overlay.
	 * 
	 * @param proxyName
	 * @param proxyInterface
	 * @return
	 */
	public <T extends TransitionEnabled> T _getMechanismInstance(String proxyName, Class<T> proxyInterface) {
		return proxyInterface.cast(proxies.get(proxyName).getActiveComponent());
	}

	@Override
	public <T extends TransitionEnabled> T createMechanismProxy(Class<T> proxyInterface, final T defaultInstance,
			String proxyName) {

		if (proxyExists(proxyName)) {
			return null;
		}

		ComponentProxy<T> componentProxy = new ComponentProxy<T>(proxyName, proxyInterface, defaultInstance);
		proxies.put(proxyName, componentProxy);

		Class<?>[] proxyInterfaces = new Class[] { proxyInterface };
		@SuppressWarnings("unchecked")
		T proxy = (T) Proxy.newProxyInstance(proxyInterface.getClassLoader(), proxyInterfaces, componentProxy);
		proxyFacades.put(proxyName, proxy);

		/*
		 * FIXME BR not sure, whether this is the right place to do it - but I
		 * guess the initial component should also run via the usual lifecycle
		 * (i.e., init and active should be triggered).
		 */
		Event.scheduleImmediately(new EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				new InitialCallbackCtrl(defaultInstance, componentProxy);
			}
		}, null, 0);

		return proxy;
	}

	@Override
	public void destroyMechanismProxy(String proxyName) {
		if (!proxyExists(proxyName)) {
			return;
		}
		ComponentProxy<?> proxy = proxies.get(proxyName);
		Event.scheduleImmediately(new EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				new ShutdownCallbackCtrl(_getMechanismInstance(proxyName, TransitionEnabled.class), proxy);
			}
		}, null, 0);
	}

	@Override
	public void addTransitionListener(String proxyName, TransitionListener listener) {
		ComponentProxy<?> proxy = proxies.get(proxyName);
		assert proxy != null;
		proxy.addListener(listener);
	}

	@Override
	public void removeTransitionListener(String proxyName, TransitionListener listener) {
		ComponentProxy<?> proxy = proxies.get(proxyName);
		assert proxy != null;
		proxy.removeListener(listener);
	}


	public <T extends TransitionEnabled> void alterLocalState(String proxyName, Class<T> targetClass, String fieldName,
			Object value) {
		this.executeSelfTransition(proxyName, targetClass, new SelfTransition<T>() {
			@Override
			public void alterState(T mechanism) {
				for (Field field : targetClass.getDeclaredFields()) {
					if (field.isAnnotationPresent(MechanismState.class)
							&& field.getAnnotation(MechanismState.class).value().equals(fieldName)) {
						/*
						 * We do NOT alter the field directly but instead search
						 * for a setter with sufficient visibility.
						 */
						try {
							mechanism.getClass().getMethod("set" + fieldName, field.getType()).invoke(mechanism,
									value);
							return;
						} catch (NoSuchMethodException | SecurityException | IllegalAccessException
								| IllegalArgumentException | InvocationTargetException e) {
							throw new AssertionError(e);
						}
					}
				}
				throw new AssertionError(
						"No matching field " + fieldName + " with @MechanismState annotation found in " + targetClass);
			}
		});
	}

	@Override
	public <T extends TransitionEnabled> void executeSelfTransition(String proxyName, Class<T> targetClass,
			SelfTransition<T> selfTransition) throws UnsupportedOperationException {
		ComponentProxy<?> proxy = proxies.get(proxyName);
		if (proxy == null) {
			throw new AssertionError("Unknown Proxy: " + proxyName);
		}
		if (!targetClass.isAssignableFrom(proxy.getActiveComponent().getClass())) {
			throw new UnsupportedOperationException(
					"Incompatible types: " + proxy.getActiveComponent().getClass().getSimpleName() + " and "
							+ targetClass.getSimpleName());
		} else {
			selfTransition.alterState(targetClass.cast(proxy.getActiveComponent()));
		}
	}

	@Override
	public <T extends TransitionEnabled> void executeAtomicTransition(String proxyName, Class<T> targetClass) {
		ComponentProxy<?> proxy = proxies.get(proxyName);
		if (proxy == null) {
			throw new AssertionError("Unknown Proxy: " + proxyName);
		}
		TransitionAnalyzer analyzer = Monitor.getOrNull(TransitionAnalyzer.class);
		if (analyzer != null) {
			analyzer.onExecuteAtomicTransition(getHost(), proxyName, targetClass);
		}
		if (targetClass.equals(proxy.getActiveComponent().getClass())) {
			// Self-Transition. Discard silently.
			return;
		}

		new AtomicTransitionExecution<T>(proxy, targetClass).startTransition();
	}

	@Override
	public <F extends TransitionEnabled, T extends TransitionEnabled> void registerTransition(String proxyName,
			AtomicTransition<F, T> transitionStrategy) {
		ComponentProxy<?> proxy = proxies.get(proxyName);
		if (proxy == null) {
			throw new AssertionError("Unknown Proxy: " + proxyName);
		}
		proxy.addCustomTransition(transitionStrategy);
	}

	@Override
	public void shutdown() {
		// do nothing
	}

	@Override
	public Host getHost() {
		return host;
	}

	/**
	 * Callback used to instantiate the default component of a proxy correctly.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private class InitialCallbackCtrl implements TransitionEnabled.Callback {

		private final ComponentProxy<?> proxy;

		public InitialCallbackCtrl(TransitionEnabled instance, ComponentProxy<?> proxy) {
			this.proxy = proxy;
			instance.startMechanism(InitialCallbackCtrl.this);
		}

		@Override
		public void finished(boolean successful) {
			if (!successful) {
				throw new AssertionError("Default Component is not supposed to fail...");
			}
			// OK.
			proxy.initializationFinished();
		}

	}

	/**
	 * Callback used to instantiate the default component of a proxy correctly.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private class ShutdownCallbackCtrl implements TransitionEnabled.Callback {

		private final ComponentProxy<?> proxy;

		public ShutdownCallbackCtrl(TransitionEnabled instance, ComponentProxy<?> proxy) {
			this.proxy = proxy;
			instance.stopMechanism(ShutdownCallbackCtrl.this);
		}

		@Override
		public void finished(boolean successful) {
			if (!successful) {
				throw new AssertionError("Shutdown is not supposed to fail...");
			}
			// unlink references
			proxies.remove(proxy.getName());
			proxyFacades.remove(proxy.getName());
			// OK.
			proxy.shutdownFinished();
		}

	}

	/**
	 * 
	 * @author Bjoern Richerzhagen
	 * @version Oct 23, 2014
	 */
	public static class Factory implements HostComponentFactory {

		@Override
		public HostComponent createComponent(Host host) {
			return new LocalTransitionEngine(host);
		}
	}
}