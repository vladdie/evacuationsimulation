package de.tudarmstadt.maki.simonstrator.service.transition.model;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;

/**
 * TODO comment
 * 
 * @author Alex Froemmgen
 * @version Aug 14, 2014
 */
public interface TransitionDescription extends Transmitable {

	public String getName();

}
