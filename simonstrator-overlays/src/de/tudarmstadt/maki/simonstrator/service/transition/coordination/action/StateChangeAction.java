package de.tudarmstadt.maki.simonstrator.service.transition.coordination.action;

import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;

/**
 * This action triggers a local state change on the target mechanism.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class StateChangeAction implements TransitionAction {

	private static final long serialVersionUID = 1L;

	private Class<? extends TransitionEnabled> target;

	private String proxyName;

	private String fieldName;

	private Object value;

	public <T extends TransitionEnabled> StateChangeAction(String proxyName, Class<T> target, String fieldName,
			Object value) {
		this.target = target;
		this.proxyName = proxyName;
		this.fieldName = fieldName;
		this.value = value;
	}

	@Override
	public void executeAction(TransitionEngine localEngine) {
		localEngine.alterLocalState(proxyName, target, fieldName, value);
	}

	@Override
	public int getTransmissionSize() {
		/*
		 * 16 bit for proxy name and target class (assuming globally known IDs).
		 * As we do not know the object's size, we just assume a fixed size of 8
		 * byte (most states that are changed remotely will be numerical).
		 */
		return 16 + fieldName.length() + 8;
	}

}
