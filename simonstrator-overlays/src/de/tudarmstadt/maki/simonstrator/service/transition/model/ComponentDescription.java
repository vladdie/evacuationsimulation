package de.tudarmstadt.maki.simonstrator.service.transition.model;
import java.util.Set;
import java.util.TreeSet;



public class ComponentDescription {
	public final String name;
	public final Set<InterfaceDescription> implementsInterfaces = new TreeSet<InterfaceDescription>();
	
	public ComponentDescription(String name) {
		this.name = name;
	}
	
	public void addImplementsInterface(InterfaceDescription interfaceDescription) {
		implementsInterfaces.add(interfaceDescription);
	}

	public MethodDescription getMethod(String methodName) {
		// components do not have methods... check all implemented interfaces
		for(InterfaceDescription interfaceDescription:implementsInterfaces) {
			MethodDescription md = interfaceDescription.getMethod(methodName);
			if(null != md)
				return md;
		}
		return null;
	}
}
