package de.tudarmstadt.maki.simonstrator.service.transition.language;

/**
 * The InvalidTransitionDescriptionException is thrown in case an invalid
 * transition description is found.
 * 
 * @author Alexander
 *
 */
public class InvalidTransitionDescriptionException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidTransitionDescriptionException(String arg) {
		super(arg);
	}
}
