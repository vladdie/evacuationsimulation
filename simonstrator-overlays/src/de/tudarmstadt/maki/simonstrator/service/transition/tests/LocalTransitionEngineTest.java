package de.tudarmstadt.maki.simonstrator.service.transition.tests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import de.tudarmstadt.maki.simonstrator.api.component.transition.AtomicTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transition.MechanismState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.service.transition.local.LocalTransitionEngine;

/**
 * A bunch of test cases for the local {@link TransitionEngine}
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class LocalTransitionEngineTest {

	public final static String PROXY_NAME = "MyProxy";

	private LocalTransitionEngine tEngine = null;

	@Before
	public void setUp() throws Exception {
		tEngine = new LocalTransitionEngine(null);
	}

	@After
	public void tearDown() throws Exception {
		tEngine = null;
	}

	@Test
	public void proxyFunctionality() {
		MyInterface proxy = tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		Assert.assertEquals(ComponentGetterState.METHOD_RETURN, proxy.foo());
	}

	@Test
	public void executeTransition() {
		tEngine = new LocalTransitionEngine(null);
		MyInterface proxy = tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		Assert.assertEquals(ComponentGetterState.METHOD_RETURN, proxy.foo());
		Assert.assertEquals(ComponentGetterState.STATE, proxy.getState());
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentConstructorState.class);
		// As long as the callback is not triggered, source needs to answer
		Assert.assertEquals("foo() has to be served by the source component, as start callback is not answered yet.",
				ComponentGetterState.METHOD_RETURN, proxy.foo());
		ComponentConstructorState.cb.finished(true);
		// foo() has to be answered by target
		Assert.assertEquals("foo() has to be served by the target component now.",
				ComponentConstructorState.METHOD_RETURN, proxy.foo());
		// State should have been migrated!
		Assert.assertEquals("State of source component has to be migrated to constructor call!",
				ComponentGetterState.STATE, proxy.getState());
	}

	@Test
	public void abortTransition() {
		tEngine = new LocalTransitionEngine(null);
		MyInterface proxy = tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentConstructorState.class);
		// As long as the callback is not triggered, source needs to answer
		Assert.assertEquals("foo() has to be served by the source component, as start callback is not answered yet.",
				ComponentGetterState.METHOD_RETURN, proxy.foo());
		ComponentConstructorState.cb.finished(false);
		// foo() has to be answered by source, as target startup failed
		Assert.assertEquals("foo() has to be served by the source component, as startup of target failed.",
				ComponentGetterState.METHOD_RETURN, proxy.foo());
		// State should still be the same
		Assert.assertEquals("State of source component has to remain intact", ComponentGetterState.STATE,
				proxy.getState());
	}

	@Test
	public void executeTransitionWithStateViaSetter() {
		tEngine = new LocalTransitionEngine(null);
		MyInterface proxy = tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		Assert.assertEquals(ComponentGetterState.STATE, proxy.getState());
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentSetterState.class);
		// State should have been migrated!
		Assert.assertEquals("State of source component has to be migrated to constructor call!",
				ComponentGetterState.STATE, proxy.getState());
	}

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Test
	public void executeTransitionWithoutAccessToState() {
		tEngine = new LocalTransitionEngine(null);
		MyInterface proxy = tEngine.createMechanismProxy(MyInterface.class, new ComponentSetterState(), PROXY_NAME);
		Assert.assertEquals(ComponentSetterState.STATE, proxy.getState());
		/*
		 * Should throw a runtime exception (here: AssertionError) to indicate a
		 * failure in state transfer annotations. We DO NOT want to fail
		 * silently as this is most likely not intentional!
		 */
		exception.expect(AssertionError.class);
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentGetterState.class);
	}

	@Test
	public void executeSelfTransition() {
		tEngine = new LocalTransitionEngine(null);
		tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		int origInstance = ComponentGetterState.instances;
		// Self-Transition
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentGetterState.class);
		Assert.assertEquals("Self-transition: do NOT create new instances!", origInstance,
				ComponentGetterState.instances);
	}

	@Test
	public void executeCustomTransition() {
		tEngine = new LocalTransitionEngine(null);
		tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		CustomTransition transition = new CustomTransition();
		tEngine.registerTransition(PROXY_NAME, transition);
		Assert.assertFalse("Has to be unused up to now.", transition.accessedTransferState);
		Assert.assertFalse("Has to be unused up to now.", transition.accessedTransitionFinished);
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentSetterState.class);
		Assert.assertTrue("Has to be used now.", transition.accessedTransferState);
		Assert.assertTrue("Has to be used now.", transition.accessedTransitionFinished);
	}

	@Test
	public void executeCustomTransitionGenericSource() {
		tEngine = new LocalTransitionEngine(null);
		tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		CustomTransitionGenericSource transition = new CustomTransitionGenericSource();
		tEngine.registerTransition(PROXY_NAME, transition);
		Assert.assertFalse("Has to be unused up to now.", transition.accessedTransferState);
		Assert.assertFalse("Has to be unused up to now.", transition.accessedTransitionFinished);
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentSetterState.class);
		Assert.assertTrue("Has to be used now.", transition.accessedTransferState);
		Assert.assertTrue("Has to be used now.", transition.accessedTransitionFinished);
	}

	@Test
	public void executeCustomTransitionGenericTarget() {
		tEngine = new LocalTransitionEngine(null);
		tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		CustomTransitionGenericTarget transition = new CustomTransitionGenericTarget();
		tEngine.registerTransition(PROXY_NAME, transition);
		Assert.assertFalse("Has to be unused up to now.", transition.accessedTransferState);
		Assert.assertFalse("Has to be unused up to now.", transition.accessedTransitionFinished);
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentSetterState.class);
		Assert.assertTrue("Has to be used now.", transition.accessedTransferState);
		Assert.assertTrue("Has to be used now.", transition.accessedTransitionFinished);
	}

	@Test
	public void executeCustomTransitionSelectMostSpecificOne() {
		/*
		 * If more than one custom transition match, we need to use the most
		 * specific one.
		 */
		tEngine = new LocalTransitionEngine(null);
		tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		CustomTransition specificTransition = new CustomTransition();
		CustomTransitionGenericSource genericTransition = new CustomTransitionGenericSource();
		tEngine.registerTransition(PROXY_NAME, specificTransition);
		tEngine.registerTransition(PROXY_NAME, genericTransition);
		Assert.assertFalse("Has to be unused up to now.", specificTransition.accessedTransferState);
		Assert.assertFalse("Has to be unused up to now.", specificTransition.accessedTransitionFinished);
		Assert.assertFalse("Has to be unused up to now.", genericTransition.accessedTransferState);
		Assert.assertFalse("Has to be unused up to now.", genericTransition.accessedTransitionFinished);
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentSetterState.class);
		Assert.assertTrue("Has to be used now.", specificTransition.accessedTransferState);
		Assert.assertTrue("Has to be used now.", specificTransition.accessedTransitionFinished);
		Assert.assertFalse("Has to be unused up to now.", genericTransition.accessedTransferState);
		Assert.assertFalse("Has to be unused up to now.", genericTransition.accessedTransitionFinished);
	}

	@Test
	public void executeCustomTransitionSelectMostSpecificOneReverseRegistrationOrder() {
		/*
		 * Same as above, with reverse registration order to prevent lucky
		 * punches.
		 */
		tEngine = new LocalTransitionEngine(null);
		tEngine.createMechanismProxy(MyInterface.class, new ComponentGetterState(), PROXY_NAME);
		CustomTransition specificTransition = new CustomTransition();
		CustomTransitionGenericSource genericTransition = new CustomTransitionGenericSource();
		tEngine.registerTransition(PROXY_NAME, genericTransition);
		tEngine.registerTransition(PROXY_NAME, specificTransition);
		Assert.assertFalse("Has to be unused up to now.", specificTransition.accessedTransferState);
		Assert.assertFalse("Has to be unused up to now.", specificTransition.accessedTransitionFinished);
		Assert.assertFalse("Has to be unused up to now.", genericTransition.accessedTransferState);
		Assert.assertFalse("Has to be unused up to now.", genericTransition.accessedTransitionFinished);
		tEngine.executeAtomicTransition(PROXY_NAME, ComponentSetterState.class);
		Assert.assertTrue("Has to be used now.", specificTransition.accessedTransferState);
		Assert.assertTrue("Has to be used now.", specificTransition.accessedTransitionFinished);
		Assert.assertFalse("Has to be unused up to now.", genericTransition.accessedTransferState);
		Assert.assertFalse("Has to be unused up to now.", genericTransition.accessedTransitionFinished);
	}

	@Test
	public void executeLocalStateChange() {
		tEngine = new LocalTransitionEngine(null);
		MyInterface proxy = tEngine.createMechanismProxy(MyInterface.class, new ComponentLocalState(), PROXY_NAME);
		Assert.assertEquals(ComponentLocalState.CUSTOM_STATE, proxy.getCustomState(), 0.0);
		tEngine.alterLocalState(PROXY_NAME, ComponentLocalState.class, "CustomState", 0.2);
		// State should have been changed!
		Assert.assertEquals("Local state should have changed.",
				0.2, proxy.getCustomState(), 0.0);
	}

	@Test
	public void executeLocalStateChangeWithWrongType() {
		tEngine = new LocalTransitionEngine(null);
		MyInterface proxy = tEngine.createMechanismProxy(MyInterface.class, new ComponentLocalState(), PROXY_NAME);
		Assert.assertEquals(ComponentLocalState.CUSTOM_STATE, proxy.getCustomState(), 0.0);
		// Should lead to an exception
		exception.expect(AssertionError.class);
		tEngine.alterLocalState(PROXY_NAME, ComponentLocalState.class, "CustomState", "WrongState");
	}

	@Test
	public void executeLocalStateChangeWithWrongMethodName() {
		tEngine = new LocalTransitionEngine(null);
		MyInterface proxy = tEngine.createMechanismProxy(MyInterface.class, new ComponentLocalState(), PROXY_NAME);
		Assert.assertEquals(ComponentLocalState.CUSTOM_STATE, proxy.getCustomState(), 0.0);
		// Should lead to an exception
		exception.expect(AssertionError.class);
		tEngine.alterLocalState(PROXY_NAME, ComponentLocalState.class, "CustomStateWrong", 0.2);
	}

	/**
	 * Testing custom transitions (with a generic source)
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class CustomTransitionGenericSource implements AtomicTransition<MyInterface, ComponentSetterState> {

		public boolean accessedTransferState = false;

		public boolean accessedTransitionFinished = false;

		@Override
		public Class<MyInterface> getSourceType() {
			return MyInterface.class;
		}

		@Override
		public Class<ComponentSetterState> getTargetType() {
			return ComponentSetterState.class;
		}

		@Override
		public void transferState(MyInterface sourceComponent, ComponentSetterState targetComponent) {
			this.accessedTransferState = true;
		}

		@Override
		public void transitionFinished(MyInterface sourceComponent, ComponentSetterState targetComponent,
				boolean successful) {
			this.accessedTransitionFinished = true;
		}

	}

	/**
	 * Testing custom transitions (with a generic target)
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class CustomTransitionGenericTarget implements AtomicTransition<ComponentGetterState, MyInterface> {

		public boolean accessedTransferState = false;

		public boolean accessedTransitionFinished = false;

		@Override
		public Class<ComponentGetterState> getSourceType() {
			return ComponentGetterState.class;
		}

		@Override
		public Class<MyInterface> getTargetType() {
			return MyInterface.class;
		}

		@Override
		public void transferState(ComponentGetterState sourceComponent, MyInterface targetComponent) {
			this.accessedTransferState = true;
		}

		@Override
		public void transitionFinished(ComponentGetterState sourceComponent, MyInterface targetComponent,
				boolean successful) {
			this.accessedTransitionFinished = true;
		}

	}

	/**
	 * Testing custom transitions
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class CustomTransition implements AtomicTransition<ComponentGetterState, ComponentSetterState> {

		public boolean accessedTransferState = false;

		public boolean accessedTransitionFinished = false;

		@Override
		public Class<ComponentGetterState> getSourceType() {
			return ComponentGetterState.class;
		}

		@Override
		public Class<ComponentSetterState> getTargetType() {
			return ComponentSetterState.class;
		}

		@Override
		public void transferState(ComponentGetterState sourceComponent, ComponentSetterState targetComponent) {
			this.accessedTransferState = true;
		}

		@Override
		public void transitionFinished(ComponentGetterState sourceComponent, ComponentSetterState targetComponent,
				boolean successful) {
			this.accessedTransitionFinished = true;
		}

	}

	/**
	 * Component with a default constructor and a setter for the required state.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class ComponentSetterState implements MyInterface {

		public static final String STATE = "C";

		public static final String METHOD_RETURN = "foo() of C";

		@TransferState({ "MyVari" })
		private String myVari = STATE;

		@Override
		public void startMechanism(Callback cb) {
			cb.finished(true);
		}

		@Override
		public void stopMechanism(Callback cb) {
			cb.finished(true);
		}

		@Override
		public String foo() {
			return METHOD_RETURN;
		}

		@Override
		public String getState() {
			return myVari;
		}

		public String getMyVari() {
			return myVari;
		}

		public void setMyVari(String value) {
			myVari = value;
		}

		@Override
		public double getCustomState() {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Component with a constructor that requires state and a getter for the
	 * state object. Furthermore, the callback for start and stop is made
	 * available for external triggering.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class ComponentConstructorState implements MyInterface {

		public static final String STATE = "B";

		public static final String METHOD_RETURN = "foo() of B";

		public static Callback cb;

		@TransferState({ "MyVari" })
		private String myVari = STATE;

		@TransferState({ "MyVari" })
		public ComponentConstructorState(String myVari) {
			this.myVari = myVari;
		}

		@Override
		public void startMechanism(Callback cb) {
			ComponentConstructorState.cb = cb;
		}

		@Override
		public void stopMechanism(Callback cb) {
			ComponentConstructorState.cb = cb;
		}

		@Override
		public String foo() {
			return METHOD_RETURN;
		}

		@Override
		public String getState() {
			return myVari;
		}

		public String getMyVari() {
			return myVari;
		}

		@Override
		public double getCustomState() {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Component with default constructor, a getter for the state, but NO setter
	 * (cannot be the target of a state transfer...). Has an instance counter
	 * for self-transition test
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class ComponentGetterState implements MyInterface {

		public static final String STATE = "A";

		public static final String METHOD_RETURN = "foo() of A";

		public static int instances = 0;

		public int instanceNo = instances++;

		@TransferState({ "MyVari" })
		private String myVari = STATE;

		@Override
		public void startMechanism(Callback cb) {
			cb.finished(true);
		}

		@Override
		public void stopMechanism(Callback cb) {
			cb.finished(true);
		}

		@Override
		public String foo() {
			return METHOD_RETURN;
		}

		@Override
		public String getState() {
			return myVari;
		}

		public String getMyVari() {
			return myVari;
		}

		@Override
		public double getCustomState() {
			throw new UnsupportedOperationException();
		}
	}

	/**
	 * Component with default constructor, getter and setter for state and a
	 * custom state variable.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class ComponentLocalState implements MyInterface {

		public static final String STATE = "D";

		public static final double CUSTOM_STATE = 0.4;

		public static final String METHOD_RETURN = "foo() of D";

		public static int instances = 0;

		public int instanceNo = instances++;

		@MechanismState("CustomState")
		private double customState = CUSTOM_STATE;

		@TransferState({ "MyVari" })
		private String myVari = STATE;

		@Override
		public void startMechanism(Callback cb) {
			cb.finished(true);
		}

		@Override
		public void stopMechanism(Callback cb) {
			cb.finished(true);
		}

		@Override
		public String foo() {
			return METHOD_RETURN;
		}

		@Override
		public String getState() {
			return myVari;
		}

		public String getMyVari() {
			return myVari;
		}

		@Override
		public double getCustomState() {
			return customState;
		}

		public void setCustomState(double customState) {
			this.customState = customState;
		}

		public void setMyVari(String myVari) {
			this.myVari = myVari;
		}
	}

	/**
	 * Interface used by the proxy
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static interface MyInterface extends TransitionEnabled {
		/**
		 * Defined return value for each implementation
		 * 
		 * @return
		 */
		public String foo();

		/**
		 * Returns the stored State variable
		 * 
		 * @return
		 */
		public String getState();

		/**
		 * Per-instance custom state
		 * 
		 * @return
		 */
		public double getCustomState();
	}
}
