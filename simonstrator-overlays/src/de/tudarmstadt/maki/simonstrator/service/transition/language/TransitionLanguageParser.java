package de.tudarmstadt.maki.simonstrator.service.transition.language;

// Generated from TransitionLanguage.g4 by ANTLR 4.4
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class TransitionLanguageParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.4", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__21=1, T__20=2, T__19=3, T__18=4, T__17=5, T__16=6, T__15=7, T__14=8, 
		T__13=9, T__12=10, T__11=11, T__10=12, T__9=13, T__8=14, T__7=15, T__6=16, 
		T__5=17, T__4=18, T__3=19, T__2=20, T__1=21, T__0=22, NAME=23, WHITESPACE=24;
	public static final String[] tokenNames = {
		"<INVALID>", "'from'", "'component'", "'interface'", "'target'", "'use'", 
		"'transfer'", "'both'", "','", "'define'", "'source'", "'block communication'", 
		"'parallel transition'", "'for'", "'flip'", "';'", "'with'", "'as'", "'to'", 
		"'atomic transition'", "'with transformation'", "'implements'", "'runrun'", 
		"NAME", "WHITESPACE"
	};
	public static final int
		RULE_define = 0, RULE_inter = 1, RULE_component = 2, RULE_atomicTransition = 3, 
		RULE_parallelTransition = 4, RULE_childs = 5, RULE_methods = 6, RULE_transfer = 7, 
		RULE_method = 8;
	public static final String[] ruleNames = {
		"define", "inter", "component", "atomicTransition", "parallelTransition", 
		"childs", "methods", "transfer", "method"
	};

	@Override
	public String getGrammarFileName() { return "TransitionLanguage.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public TransitionLanguageParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class DefineContext extends ParserRuleContext {
		public List<AtomicTransitionContext> atomicTransition() {
			return getRuleContexts(AtomicTransitionContext.class);
		}
		public InterContext inter(int i) {
			return getRuleContext(InterContext.class,i);
		}
		public ComponentContext component(int i) {
			return getRuleContext(ComponentContext.class,i);
		}
		public List<ComponentContext> component() {
			return getRuleContexts(ComponentContext.class);
		}
		public List<InterContext> inter() {
			return getRuleContexts(InterContext.class);
		}
		public AtomicTransitionContext atomicTransition(int i) {
			return getRuleContext(AtomicTransitionContext.class,i);
		}
		public DefineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_define; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).enterDefine(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).exitDefine(this);
		}
	}

	public final DefineContext define() throws RecognitionException {
		DefineContext _localctx = new DefineContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_define);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(27);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__13) {
				{
				{
				setState(21);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(18); inter();
					}
					break;
				case 2:
					{
					setState(19); component();
					}
					break;
				case 3:
					{
					setState(20); atomicTransition();
					}
					break;
				}
				setState(23); match(T__7);
				}
				}
				setState(29);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class InterContext extends ParserRuleContext {
		public MethodsContext methods() {
			return getRuleContext(MethodsContext.class,0);
		}
		public TerminalNode NAME() { return getToken(TransitionLanguageParser.NAME, 0); }
		public InterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_inter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).enterInter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).exitInter(this);
		}
	}

	public final InterContext inter() throws RecognitionException {
		InterContext _localctx = new InterContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_inter);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(30); match(T__13);
			setState(31); match(T__19);
			setState(32); match(NAME);
			setState(35);
			_la = _input.LA(1);
			if (_la==T__6) {
				{
				setState(33); match(T__6);
				setState(34); methods();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComponentContext extends ParserRuleContext {
		public TerminalNode NAME(int i) {
			return getToken(TransitionLanguageParser.NAME, i);
		}
		public List<TerminalNode> NAME() { return getTokens(TransitionLanguageParser.NAME); }
		public ComponentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_component; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).enterComponent(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).exitComponent(this);
		}
	}

	public final ComponentContext component() throws RecognitionException {
		ComponentContext _localctx = new ComponentContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_component);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(37); match(T__13);
			setState(38); match(T__20);
			setState(39); match(NAME);
			setState(42);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(40); match(T__1);
				setState(41); match(NAME);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AtomicTransitionContext extends ParserRuleContext {
		public MethodContext method() {
			return getRuleContext(MethodContext.class,0);
		}
		public TerminalNode NAME(int i) {
			return getToken(TransitionLanguageParser.NAME, i);
		}
		public List<TerminalNode> NAME() { return getTokens(TransitionLanguageParser.NAME); }
		public TransferContext transfer() {
			return getRuleContext(TransferContext.class,0);
		}
		public AtomicTransitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_atomicTransition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).enterAtomicTransition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).exitAtomicTransition(this);
		}
	}

	public final AtomicTransitionContext atomicTransition() throws RecognitionException {
		AtomicTransitionContext _localctx = new AtomicTransitionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_atomicTransition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(44); match(T__13);
			setState(45); match(T__3);
			setState(46); match(NAME);
			setState(47); match(T__21);
			setState(48); match(NAME);
			setState(49); match(T__4);
			setState(50); match(NAME);
			setState(51);
			_la = _input.LA(1);
			if ( !(_la==T__8 || _la==T__0) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(54);
			_la = _input.LA(1);
			if (_la==T__16) {
				{
				setState(52); match(T__16);
				setState(53); transfer();
				}
			}

			setState(58);
			_la = _input.LA(1);
			if (_la==T__17) {
				{
				setState(56); match(T__17);
				setState(57); method();
				}
			}

			setState(61);
			_la = _input.LA(1);
			if (_la==T__11) {
				{
				setState(60); match(T__11);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParallelTransitionContext extends ParserRuleContext {
		public ChildsContext childs() {
			return getRuleContext(ChildsContext.class,0);
		}
		public TerminalNode NAME() { return getToken(TransitionLanguageParser.NAME, 0); }
		public ParallelTransitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parallelTransition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).enterParallelTransition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).exitParallelTransition(this);
		}
	}

	public final ParallelTransitionContext parallelTransition() throws RecognitionException {
		ParallelTransitionContext _localctx = new ParallelTransitionContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_parallelTransition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(63); match(T__13);
			setState(64); match(T__10);
			setState(65); match(NAME);
			setState(66); match(T__5);
			setState(67); childs();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ChildsContext extends ParserRuleContext {
		public ChildsContext childs() {
			return getRuleContext(ChildsContext.class,0);
		}
		public TerminalNode NAME() { return getToken(TransitionLanguageParser.NAME, 0); }
		public ChildsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_childs; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).enterChilds(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).exitChilds(this);
		}
	}

	public final ChildsContext childs() throws RecognitionException {
		ChildsContext _localctx = new ChildsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_childs);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(69); match(NAME);
			setState(72);
			_la = _input.LA(1);
			if (_la==T__14) {
				{
				setState(70); match(T__14);
				setState(71); childs();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodsContext extends ParserRuleContext {
		public MethodsContext methods() {
			return getRuleContext(MethodsContext.class,0);
		}
		public TerminalNode NAME() { return getToken(TransitionLanguageParser.NAME, 0); }
		public MethodsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_methods; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).enterMethods(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).exitMethods(this);
		}
	}

	public final MethodsContext methods() throws RecognitionException {
		MethodsContext _localctx = new MethodsContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_methods);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74); match(NAME);
			setState(77);
			_la = _input.LA(1);
			if (_la==T__14) {
				{
				setState(75); match(T__14);
				setState(76); methods();
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TransferContext extends ParserRuleContext {
		public TransferContext transfer(int i) {
			return getRuleContext(TransferContext.class,i);
		}
		public TerminalNode NAME(int i) {
			return getToken(TransitionLanguageParser.NAME, i);
		}
		public List<TerminalNode> NAME() { return getTokens(TransitionLanguageParser.NAME); }
		public List<TransferContext> transfer() {
			return getRuleContexts(TransferContext.class);
		}
		public TransferContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_transfer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).enterTransfer(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).exitTransfer(this);
		}
	}

	public final TransferContext transfer() throws RecognitionException {
		TransferContext _localctx = new TransferContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_transfer);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(79); match(NAME);
			setState(82);
			_la = _input.LA(1);
			if (_la==T__2) {
				{
				setState(80); match(T__2);
				setState(81); match(NAME);
				}
			}

			setState(88);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(84); match(T__14);
					setState(85); transfer();
					}
					} 
				}
				setState(90);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MethodContext extends ParserRuleContext {
		public List<MethodContext> method() {
			return getRuleContexts(MethodContext.class);
		}
		public TerminalNode NAME() { return getToken(TransitionLanguageParser.NAME, 0); }
		public MethodContext method(int i) {
			return getRuleContext(MethodContext.class,i);
		}
		public MethodContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_method; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).enterMethod(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof TransitionLanguageListener ) ((TransitionLanguageListener)listener).exitMethod(this);
		}
	}

	public final MethodContext method() throws RecognitionException {
		MethodContext _localctx = new MethodContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_method);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(91);
			_la = _input.LA(1);
			if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__18) | (1L << T__15) | (1L << T__12))) != 0)) ) {
			_errHandler.recoverInline(this);
			}
			consume();
			setState(92); match(T__9);
			setState(93); match(NAME);
			setState(98);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(94); match(T__14);
					setState(95); method();
					}
					} 
				}
				setState(100);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,11,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\32h\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\3\2\3\2\3\2"+
		"\5\2\30\n\2\3\2\3\2\7\2\34\n\2\f\2\16\2\37\13\2\3\3\3\3\3\3\3\3\3\3\5"+
		"\3&\n\3\3\4\3\4\3\4\3\4\3\4\5\4-\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\5\5\59\n\5\3\5\3\5\5\5=\n\5\3\5\5\5@\n\5\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\7\3\7\3\7\5\7K\n\7\3\b\3\b\3\b\5\bP\n\b\3\t\3\t\3\t\5\tU\n\t\3\t\3"+
		"\t\7\tY\n\t\f\t\16\t\\\13\t\3\n\3\n\3\n\3\n\3\n\7\nc\n\n\f\n\16\nf\13"+
		"\n\3\n\2\2\13\2\4\6\b\n\f\16\20\22\2\4\4\2\20\20\30\30\5\2\6\6\t\t\f\f"+
		"k\2\35\3\2\2\2\4 \3\2\2\2\6\'\3\2\2\2\b.\3\2\2\2\nA\3\2\2\2\fG\3\2\2\2"+
		"\16L\3\2\2\2\20Q\3\2\2\2\22]\3\2\2\2\24\30\5\4\3\2\25\30\5\6\4\2\26\30"+
		"\5\b\5\2\27\24\3\2\2\2\27\25\3\2\2\2\27\26\3\2\2\2\30\31\3\2\2\2\31\32"+
		"\7\21\2\2\32\34\3\2\2\2\33\27\3\2\2\2\34\37\3\2\2\2\35\33\3\2\2\2\35\36"+
		"\3\2\2\2\36\3\3\2\2\2\37\35\3\2\2\2 !\7\13\2\2!\"\7\5\2\2\"%\7\31\2\2"+
		"#$\7\22\2\2$&\5\16\b\2%#\3\2\2\2%&\3\2\2\2&\5\3\2\2\2\'(\7\13\2\2()\7"+
		"\4\2\2),\7\31\2\2*+\7\27\2\2+-\7\31\2\2,*\3\2\2\2,-\3\2\2\2-\7\3\2\2\2"+
		"./\7\13\2\2/\60\7\25\2\2\60\61\7\31\2\2\61\62\7\3\2\2\62\63\7\31\2\2\63"+
		"\64\7\24\2\2\64\65\7\31\2\2\658\t\2\2\2\66\67\7\b\2\2\679\5\20\t\28\66"+
		"\3\2\2\289\3\2\2\29<\3\2\2\2:;\7\7\2\2;=\5\22\n\2<:\3\2\2\2<=\3\2\2\2"+
		"=?\3\2\2\2>@\7\r\2\2?>\3\2\2\2?@\3\2\2\2@\t\3\2\2\2AB\7\13\2\2BC\7\16"+
		"\2\2CD\7\31\2\2DE\7\23\2\2EF\5\f\7\2F\13\3\2\2\2GJ\7\31\2\2HI\7\n\2\2"+
		"IK\5\f\7\2JH\3\2\2\2JK\3\2\2\2K\r\3\2\2\2LO\7\31\2\2MN\7\n\2\2NP\5\16"+
		"\b\2OM\3\2\2\2OP\3\2\2\2P\17\3\2\2\2QT\7\31\2\2RS\7\26\2\2SU\7\31\2\2"+
		"TR\3\2\2\2TU\3\2\2\2UZ\3\2\2\2VW\7\n\2\2WY\5\20\t\2XV\3\2\2\2Y\\\3\2\2"+
		"\2ZX\3\2\2\2Z[\3\2\2\2[\21\3\2\2\2\\Z\3\2\2\2]^\t\3\2\2^_\7\17\2\2_d\7"+
		"\31\2\2`a\7\n\2\2ac\5\22\n\2b`\3\2\2\2cf\3\2\2\2db\3\2\2\2de\3\2\2\2e"+
		"\23\3\2\2\2fd\3\2\2\2\16\27\35%,8<?JOTZd";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}