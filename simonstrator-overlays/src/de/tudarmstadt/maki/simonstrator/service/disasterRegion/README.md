# DRAS - How does it work?

### Central analyzing:
The central analyzing is fairly simple. When a mobile client notices that is has no UMTS connectivity it starts
logging its position until the cellular net is available again. Upon reaching this point the collected data is simply
uploaded to the central DRAS entity (CentralDRASComponent).

### Local analyzing:

Local analyzing is a bit more complicated with the possibility to create a lot of overhead. Exact numbers need to be
investigated.

So, how does it work?
Since the nodes start to communicate when using this analyzing methods there are 4 possible cases that need to be considered:

1. A node has no UMTS connectivity
2. A node has UMTS connectivity
3. A message from (1) has been received
4. A message from (2) has been received.

#### 1)
When a node enters an area without UMTS connectivity (NO_NET) it starts logging its positions. Simultaneously an event is
scheduled with a random hesitation factor to send a broadcast containing the logged positions.

If another message (3) has not been received during the hesitation time, the broadcast is transmitted.
However, if there has been another message(3), the received locations are collected and merged with the logged positions. The event is scheduled again, again with a random hesitation time and the whole process starts again. Note: During this time, the locations of the receiving node are still being logged. Logging only stops when entering an area with connection again.

If a message (4) has been received during hesitation time, the process is basically the same. The received positions of (2)
are stored and an event is scheduled to broadcast these positions as well (separate broadcast, though). If there is
another message(4) during hesitation time, the positions are again merged and the broadcast is scheduled again.


#### 2)
As opposed to above, a node(2) with connectivity does nothing by default.
- Reason for this is, that only nodes that are close to a disaster region should participate in the analyzing to decrease overhead for the whole network.
A node(2) only starts its actions, when a message(3) has been received.
- In that case, the node(2) starts logging is positions and schedules and an event to broadcast these positions.

Again, if the node(2) receives other messages(4), the positions are stored to be broadcasted later together with the own positions. However, if the hesitation time is over and there has been another message(4), the event is not simply rescheduled. With difference to (1), logging stops once the hesitation time has passed, to prevent logging positions with a high probability of being useless for analyzing.

Logging starts again, when another message(3) has been received.

 Certainly, if a node(2) receives a message(4), this message(4) will not be forwarded. That is to prevent messages
 spreading over the network and advancing in regions where the connectivity is perfectly fine, therefore increasing
 general network overhead.

 Altogether, the prevented mechanisms should ensure that messages concerning DRAS are only send in, and in close
  neighborhood to disaster regions without affecting general network overhead and stability too much.