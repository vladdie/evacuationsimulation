package de.tudarmstadt.maki.simonstrator.service.disasterRegion;

import de.tudarmstadt.maki.simonstrator.service.disasterRegion.DRASComponent.DRASComponentType;

/**
 * Configuration class used to set up the {@link DRASComponent}s.
 *
 * @author Clemens Krug
 */
public class DRASParameterConfiguration
{
    private long LOCATION_UPDATE_INTERVAL;
    private long LOCATION_TIMEOUT;
    private boolean CENTRAL_ANALYSING;
    private boolean LOCAL_ANALYSING;
    private long MIN_HESITATION_TIME;
    private long MAX_HESITATION_TIME;

    private DRASCentralAnalyzer CENTRAL_ANALYZER;
    private DRASLocalAnalyzer LOCAL_ANALYZER;


    /**
     * Called by the {@link DisasterRegionAnalyzingServiceFactory} to configure the respective {@link DRASComponent}s.
     * @param comp The component to be configured
     * @param type The type of the component
     */
    public void configure(DRASComponent comp, DRASComponentType type)
    {
        switch (type)
        {
            case MOBILE:
                ((DRASMobileComponent) comp).setLocationUpdateInterval(LOCATION_UPDATE_INTERVAL);
                ((DRASMobileComponent) comp).setLocationTimeout(LOCATION_TIMEOUT);
                ((DRASMobileComponent) comp).enableCentralAnalyzing(CENTRAL_ANALYSING);
                ((DRASMobileComponent) comp).enableLocalAnalyzing(LOCAL_ANALYSING);
                ((DRASMobileComponent) comp).setAnalyzer(LOCAL_ANALYZER);
                ((DRASMobileComponent) comp).setMinHesitationTime(MIN_HESITATION_TIME);
                ((DRASMobileComponent) comp).setMaxHesitationTime(MAX_HESITATION_TIME);
                break;
            case CENTRAL:
                ((DRASCentralComponent) comp).setAnalyzer(CENTRAL_ANALYZER);
                break;
        }

    }

    public void setLocationUpdateInterval(long interval)
    {
        LOCATION_UPDATE_INTERVAL = interval;
    }

    public void setLocationTimeout(long timeout)
    {
        LOCATION_TIMEOUT = timeout;
    }

    public void setCentralAnalysing(boolean central) { CENTRAL_ANALYSING = central;}

    public void setLocalAnalysing(boolean local) { LOCAL_ANALYSING = local;}

    public void setCentralAnalyzer(DRASCentralAnalyzer analyzer)
    {
        CENTRAL_ANALYZER = analyzer;
    }

    public void setLocalAnalyzer(DRASLocalAnalyzer analyzer)
    {
        LOCAL_ANALYZER = analyzer;
    }

    public void setMinHesitationTime(long time) { MIN_HESITATION_TIME = time; }

    public void setMaxHesitationTime(long time) { MAX_HESITATION_TIME = time; }
}
