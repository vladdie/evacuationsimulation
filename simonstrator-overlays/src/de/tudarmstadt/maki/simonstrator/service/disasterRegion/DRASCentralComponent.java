package de.tudarmstadt.maki.simonstrator.service.disasterRegion;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;

/**
 * DRAS central entity. Should be placed on central, stationary structures, e.g. servers/clouds.
 * When using central analyzing, all data is uploaded to this entity, which forwards it to the respective
 * {@link DRASCentralAnalyzer}.
 *
 * @author Clemens Krug
 */
public class DRASCentralComponent extends DRASComponent
{
    DRASCentralAnalyzer analyzer;

    public DRASCentralComponent(Host host)
    {
        super(host);
    }

    @Override
    public void initialize()
    {
        super.initialize();

        if(analyzer!= null) analyzer.start();
    }

    @Override
    public void messageArrived(Message msg, TransInfo sender, int commID)
    {
        assert msg instanceof NoUMTSLocationUploadContainer : "Wrong message passed to DRASCentralComponent";

        if(analyzer != null) analyzer.onNoUMTSLocationReceived(((NoUMTSLocationUploadContainer) msg).getNoUMTSLocations());
    }

    public void setAnalyzer(DRASCentralAnalyzer analyzer)
    {
        this.analyzer = analyzer;
    }

}
