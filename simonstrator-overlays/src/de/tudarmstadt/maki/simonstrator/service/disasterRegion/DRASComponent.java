package de.tudarmstadt.maki.simonstrator.service.disasterRegion;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;

/**
 * Base-class for all components belonging to DRAS. Provides some basic functionality used by all components.
 *
 * @author Clemens Krug
 */
public abstract class DRASComponent implements HostComponent, TransMessageListener
{
    private final Host host;
    protected BasicOverlayContact ownContact;
    private UDP udp;
    NetInterfaceName netTypeCell = NetInterfaceName.MOBILE;

    //TODO: How should the Port number be choosen?
    private final int uploadPort = 2403;

    public DRASComponent(Host host)
    {
        this.host = host;
    }


    @Override
    public void initialize()
    {
        ownContact = new BasicOverlayContact(getHost().getId());

        NetInterface cellular = getHost().getNetworkComponent().getByName(netTypeCell);
        assert cellular != null : "NetInterface 'cellular' not set up properly!";

        try {
            udp = getHost().getTransportComponent().getProtocol(UDP.class, cellular.getLocalInetAddress(), uploadPort);
        } catch (ProtocolNotAvailableException e) {
            throw new AssertionError("Need cellular net for central analysing.");
        }
        udp.setTransportMessageListener(this);
        ownContact.addTransInfo(netTypeCell, udp.getTransInfo());
    }

    /**
     * Send a {@link OverlayMessage} via the cellular net.
     * @param m The message to be sent
     */
    public void sendViaCellular(OverlayMessage m) {
        udp.send(m, m.getReceiver().getNetID(netTypeCell), m.getReceiver().getPort(netTypeCell));
    }


    /**
     * The component types DRAS consists of
     */
    public enum DRASComponentType
    {
        /**
         * As implemented by {@link DRASMobileComponent}
         */
        MOBILE,

        /**
         * As implemented by {@link DRASCentralComponent}
         */
        CENTRAL
    }

    @Override
    public void shutdown()
    {
        //Nothing to do
    }

    @Override
    public Host getHost()
    {
        return host;
    }

    /**
     * Retrieves the {@link OverlayContact} of this node.
     * @return The {@link OverlayContact} of this node.
     */
    public OverlayContact getLocalOverlayContact()
    {
        return ownContact;
    }
}
