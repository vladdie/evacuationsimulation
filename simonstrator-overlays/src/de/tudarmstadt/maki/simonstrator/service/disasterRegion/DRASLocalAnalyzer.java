package de.tudarmstadt.maki.simonstrator.service.disasterRegion;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutPairedList;

/** Analyzer to collect and process data for determination of
 *  disaster regions (cells without connection) when using local analysing.
 *
 *  @author Clemens Krug
 */
public interface DRASLocalAnalyzer
{
    /**
     * Starts the analyzer. Can be used to execute code on startup, but
     * only after the {@link DRASComponent}s have been initialised.
     */
    void start();

    /**
     * Called when the noUMTS-dataset changed on the corresponding node.
     * @param locations The updated noUMTSLocations
     * @param nodeID The node which made the call
     */
    void onNoUTMSLocationsChanged(TimeoutPairedList<UniqueID, Location> locations, UniqueID nodeID);

    /**
     * Called when the netAvailable-dataset changed on the corresponding node.
     * @param locations The updated netAvailableLocations
     * @param nodeID The node which made the call
     */
    void onNetAvailableLocationsChanged(TimeoutPairedList<UniqueID, Location> locations, UniqueID nodeID);
}
