package de.tudarmstadt.maki.simonstrator.service.disasterRegion;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutPairedList;

/**
 * Upload container for the noUMTSLocation TimeoutMap used for disaster region determination.
 *
 * @author Clemens Krug
 */
public class NoUMTSLocationUploadContainer extends AbstractOverlayMessage
{
    private TimeoutPairedList<UniqueID, Location> noUMTSLocations;

    public NoUMTSLocationUploadContainer(OverlayContact sender, OverlayContact receiver,
                                         TimeoutPairedList<UniqueID, Location> locations)
    {
        super(sender, receiver);
        this.noUMTSLocations = locations;
    }


    public TimeoutPairedList<UniqueID, Location> getNoUMTSLocations()
    {
        return noUMTSLocations;
    }

    @Override
    public Message getPayload()
    {
        return null;
    }
}
