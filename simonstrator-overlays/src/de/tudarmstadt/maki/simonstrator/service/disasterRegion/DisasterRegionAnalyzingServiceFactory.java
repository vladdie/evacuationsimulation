package de.tudarmstadt.maki.simonstrator.service.disasterRegion;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.service.disasterRegion.DRASComponent.DRASComponentType;

/**
 * Factory for creation  of the {@link DRASComponent} {@link HostComponent}s.
 *
 * @author Clemens Krug
 */
public class DisasterRegionAnalyzingServiceFactory implements HostComponentFactory
{
    // Component Type to be created.
    DRASComponentType compType;

    DRASParameterConfiguration config;


    @Override
    public HostComponent createComponent(Host host)
    {
        if (compType == null) {
            throw new AssertionError("There must be a component type specified!");
        }

        DRASComponent comp;

        switch(compType)
        {
            case MOBILE: comp = new DRASMobileComponent(host);
                break;
            case CENTRAL: comp = new DRASCentralComponent(host);
                break;
            default:
                throw new AssertionError("Unknown component Type.");
        }

        config.configure(comp, compType);
        return comp;
    }

    /**
     * Used by XML configuration
     * @param compType Type of this component
     */
    public void setCompType(String compType)
    {
        this.compType = DRASComponentType.valueOf(compType.toUpperCase());
    }

    /**
     * Used by XML configuration
     * @param config Configuration-Class for this factory
     */
    public void setConfiguration(DRASParameterConfiguration config)
    {
        this.config = config;
    }


}
