package de.tudarmstadt.maki.simonstrator.service.disasterRegion;

import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationListener;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.service.disasterRegion.DRASLocalBroadcastContainer.DRASBroadcastType;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutPairedList;

/**
 * A {@link DRASComponent} used on mobile entities, e.g smartphones.
 *
 * @author Clemens Krug
 */
public class DRASMobileComponent extends DRASComponent implements LocationListener, EventHandler {

	private LocationSensor locationSensor;
	private LocationRequest locationRequest;
	private OverlayContact centralComponent;
	private UDP localTransport;
	private NetInterfaceName netTypeLocal = NetInterfaceName.WIFI;
	private Random random;
	private DRASLocalAnalyzer analyzer;

	private boolean noUMTSHesitationScheduled = false;
	private boolean netAvailableHesitationScheduled = false;
	private boolean noUMTSBroadcastArrived = false;
	private boolean netAvailableBroadcastArrived = false;
	private boolean logNetAvailablePositions = false;

	// TODO: How to choose?
	private final int NO_UMTS_HESITATION_EVENT_ID = 2500;
	private final int NET_AVAILABLE_HESITATION_EVENT_ID = 2501;
	private final int localPort = 2404;

	private long locationUpdateInterval;
	private long locationTimeout;
	private long minHesitationTime = 10 * Time.SECOND;
	private long maxHesitationTime = 60 * Time.SECOND;

	private long totalTimeWaitedToSendNoNet = 0;
	private int attemptsToSendNoNet = 0;
	private long totalTimeWaitedToSendNet = 0;
	private int attemptsToSendNet = 0;

	private TimeoutPairedList<UniqueID, Location> noUMTSLocations;
	private TimeoutPairedList<UniqueID, Location> netAvailableLocations;

	private boolean centralAnalyzing;
	private boolean localAnalyzing;

	// Debugging counters.
	private long scheduleNetCounter = 0;
	private long scheduleNoNetCounter = 0;
	private long eventNetRecCounter = 0;
	private long eventNoNetRecCounter = 0;

	public DRASMobileComponent(Host host) {
		super(host);
		random = Randoms.getRandom(this);
	}

	@Override
	public void initialize() {
		super.initialize();

		NetInterface wifi = getHost().getNetworkComponent().getByName(netTypeLocal);
		assert wifi != null : "NetInterface 'wifi' not set up properly!";

		try {
			localTransport = getHost().getTransportComponent().getProtocol(UDP.class, wifi.getLocalInetAddress(), localPort);
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError("Need wifi for decentralized analysing.");
		}
		localTransport.setTransportMessageListener(this);
		ownContact.addTransInfo(netTypeLocal, localTransport.getTransInfo());

		try {
			locationSensor = getHost().getComponent(LocationSensor.class);
			locationRequest = locationSensor.getLocationRequest();
			locationRequest.setInterval(locationUpdateInterval);
			locationSensor.requestLocationUpdates(locationRequest, this);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("LocationSensor not available!");
		}

		noUMTSLocations = new TimeoutPairedList<>(locationTimeout);
		netAvailableLocations = new TimeoutPairedList<>(locationTimeout);

		if (localAnalyzing) {
			analyzer.start();
		}
	}

	@Override
	public void onLocationChanged(Host host, Location location) {
		if (umtsNotAvailable()) {
			// NO_NET NODE
			// logging
			noUMTSLocations.addNow(host.getId(), location);

			if (localAnalyzing)
			{
				analyzer.onNoUTMSLocationsChanged(noUMTSLocations, host.getId());
				if (!noUMTSHesitationScheduled) {
					scheduleHesitation(NO_UMTS_HESITATION_EVENT_ID);
				}
			}
		} else if (umtsAvailable()) {
			// NET NODE
			if (logNetAvailablePositions)
			{
				netAvailableLocations.addNow(host.getId(), location);
				if (localAnalyzing) {
					analyzer.onNetAvailableLocationsChanged(netAvailableLocations, host.getId());
				}
			}

			if (centralAnalyzing && !noUMTSLocations.isEmpty()) {
				NoUMTSLocationUploadContainer uploadContainer = new NoUMTSLocationUploadContainer(getLocalOverlayContact(),
						getCentralComponent(), noUMTSLocations);
				sendViaCellular(uploadContainer);
			}
		}
	}

	/**
	 * Retrieves the {@link DRASCentralComponent} configured in the network.
	 *
	 * @return the central entity
	 */
	public OverlayContact getCentralComponent() {
		if (centralComponent == null) {
			if (Oracle.isSimulation()) {
				for (Host host : Oracle.getAllHosts()) {
					try {
						centralComponent = host.getComponent(DRASCentralComponent.class).getLocalOverlayContact();
						break;
					} catch (ComponentNotAvailableException e) {
						// don't care
					}
				}
				if (centralComponent == null) {
					throw new AssertionError(
							"Central component not found. Deactivate central analysing or add a central component");
				}
			} else {
				throw new AssertionError("No Simulation.");
			}
		}
		return centralComponent;
	}

	/**
	 * Schedule random hesitation time (within the specified bounds
	 * for this component) for the corresponding event
	 * @param id The event to be scheduled.
     */
	private void scheduleHesitation(int id)
	{
		double hesitationFactor = random.nextDouble();
		long hesitationTime;

		switch (id)
		{
		case NO_UMTS_HESITATION_EVENT_ID:
			assert !noUMTSHesitationScheduled : "Should not schedule hesitation Time, if already scheduled. Check before calling!";
			double waitedFactor = totalTimeWaitedToSendNoNet == 0 ? 1 : 1 - (totalTimeWaitedToSendNoNet / (attemptsToSendNoNet * maxHesitationTime));
			hesitationTime = minHesitationTime + (long) (hesitationFactor * waitedFactor * (maxHesitationTime - minHesitationTime));
			noUMTSHesitationScheduled = true;
			totalTimeWaitedToSendNoNet +=hesitationTime;
			attemptsToSendNoNet++;
			break;
		case NET_AVAILABLE_HESITATION_EVENT_ID:
			assert !netAvailableHesitationScheduled : "Should not schedule hesitation Time, if already scheduled. Check before calling!";
			waitedFactor = totalTimeWaitedToSendNet == 0 ? 1 : 1 - (totalTimeWaitedToSendNet / (attemptsToSendNet * maxHesitationTime));
			hesitationTime = minHesitationTime + (long) (hesitationFactor * waitedFactor * (maxHesitationTime - minHesitationTime));
			netAvailableHesitationScheduled = true;
			totalTimeWaitedToSendNet += hesitationTime;
			attemptsToSendNet++;
			break;

			default: throw new Error("Unknown event-ID!");
		}

		Event.scheduleWithDelay(hesitationTime, this, this, id);

		// Debugging output
		switch (id) {
		case NO_UMTS_HESITATION_EVENT_ID:
			Monitor.log(DRASMobileComponent.class, Monitor.Level.DEBUG,
					this.getHost().getId() + " Scheduled NO NET Event " + ++scheduleNoNetCounter);
			break;
		case NET_AVAILABLE_HESITATION_EVENT_ID:
			Monitor.log(DRASMobileComponent.class, Monitor.Level.DEBUG,
					this.getHost().getId() + " Scheduled NET Event " + ++scheduleNetCounter);
		}
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		assert msg instanceof DRASLocalBroadcastContainer;
		DRASLocalBroadcastContainer inMsg = (DRASLocalBroadcastContainer) msg;

		if (umtsNotAvailable()) {
			// NO_NET NODE
			switch (inMsg.getDRASType()) {
			case NO_UMTS:
				noUMTSBroadcastArrived = true;
				noUMTSLocations.merge(inMsg.getLocations());
				analyzer.onNoUTMSLocationsChanged(noUMTSLocations, getHost().getId());
				break;
			case NET_AVAILABLE:
				netAvailableBroadcastArrived = true;
				netAvailableLocations.merge(inMsg.getLocations());
				analyzer.onNetAvailableLocationsChanged(netAvailableLocations, getHost().getId());

				if (!netAvailableHesitationScheduled) {
					scheduleHesitation(NET_AVAILABLE_HESITATION_EVENT_ID);
				}
				break;
			}
		} else if (umtsAvailable()) {
			// NET NODE
			switch (inMsg.getDRASType()) {
			case NO_UMTS:
				// NO_NET msg --> start logging & start hesTime to broadcast own positions.
				logNetAvailablePositions = true;

				if (!netAvailableHesitationScheduled) {
					scheduleHesitation(NET_AVAILABLE_HESITATION_EVENT_ID);
				}
				break;
			case NET_AVAILABLE:
				// This node has NET and rec. NET msg. Add NET positions to own positions storage!
				netAvailableLocations.merge(inMsg.getLocations());

				break;
			}
		} else {
			throw new Error("That should not happen!");
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		// debugging output
		switch (type) {
		case NO_UMTS_HESITATION_EVENT_ID:
			Monitor.log(DRASMobileComponent.class, Monitor.Level.DEBUG,
					this.getHost().getId() + " Rec NO NET Event: " + ++eventNoNetRecCounter);
			break;
		case NET_AVAILABLE_HESITATION_EVENT_ID:
			Monitor.log(DRASMobileComponent.class, Monitor.Level.DEBUG,
					this.getHost().getId() + " Rec NET Event: " + ++eventNetRecCounter);
		}
		// ------


		if (content == this) {
			if (umtsAvailable()) {
				// NET NODE
				switch (type) {
				case NO_UMTS_HESITATION_EVENT_ID:
					// reset scheduling variable - Currently should no "noUMTS..." Event been scheduled, as the only one
					// is received here.
					noUMTSHesitationScheduled = false;
					totalTimeWaitedToSendNoNet = 0;
					attemptsToSendNoNet = 0;
					// do nothing
					break;
				case NET_AVAILABLE_HESITATION_EVENT_ID:
					// reset NET scheduling && logging
					netAvailableHesitationScheduled = false;
					logNetAvailablePositions = false;

					// send own Broadcast ONCE!
					DRASLocalBroadcastContainer broadcastContainer = new DRASLocalBroadcastContainer(getLocalOverlayContact(),
							netAvailableLocations, DRASBroadcastType.NET_AVAILABLE);
					sendLocalBroadcast(broadcastContainer);
					Monitor.log(DRASMobileComponent.class, Monitor.Level.DEBUG, this.getHost().getId() + " as NET send NET");
					totalTimeWaitedToSendNet = 0;
					attemptsToSendNet = 0;
					break;
				}
			} else if (umtsNotAvailable()) {
				// NO_NET NODE
				switch (type) {
				case NO_UMTS_HESITATION_EVENT_ID:
					// reset scheduling variable - Currently should no "noUMTS..." Event been scheduled, as the only one
					// is received here.
					noUMTSHesitationScheduled = false;

					if (!noUMTSBroadcastArrived) {
						// received no NO_NET msg
						totalTimeWaitedToSendNoNet = 0;
						attemptsToSendNoNet = 0;

						DRASLocalBroadcastContainer broadcastContainer = new DRASLocalBroadcastContainer(getLocalOverlayContact(),
								noUMTSLocations, DRASBroadcastType.NO_UMTS);
						sendLocalBroadcast(broadcastContainer);
						Monitor.log(DRASMobileComponent.class, Monitor.Level.DEBUG,
								this.getHost().getId() + " as NO_NET send NO NET");
					} else {
						// received any other no NO_NET Broadcast -- try to send in next hes interval.
						noUMTSBroadcastArrived = false;
						if (!noUMTSHesitationScheduled) {
							scheduleHesitation(NO_UMTS_HESITATION_EVENT_ID);
						}
						Monitor.log(DRASMobileComponent.class, Monitor.Level.DEBUG,
								this.getHost().getId() + " HES TIME BLOCK as NO_NET for NO_NET");
					}
					break;

				case NET_AVAILABLE_HESITATION_EVENT_ID:
					// reset NET scheduling && logging
					netAvailableHesitationScheduled = false;
					logNetAvailablePositions = false;

					// received no NET msg during hesitation time - broadcast own NET msg.
					if (!netAvailableBroadcastArrived) {
						// received NO NET msg... -- send own Broadcast.
						totalTimeWaitedToSendNet = 0;
						attemptsToSendNet = 0;

						DRASLocalBroadcastContainer broadcastContainer = new DRASLocalBroadcastContainer(getLocalOverlayContact(),
								netAvailableLocations, DRASBroadcastType.NET_AVAILABLE);
						sendLocalBroadcast(broadcastContainer);
						Monitor.log(DRASMobileComponent.class, Monitor.Level.DEBUG,
								this.getHost().getId() + " as NO_NET send NET");
					} else {
						netAvailableBroadcastArrived = false;
						// received a NET msg as NO_NET NODE during hes time -- schedule msg again.
						if (!netAvailableHesitationScheduled) {
							scheduleHesitation(NET_AVAILABLE_HESITATION_EVENT_ID);
						}
					}
					break;
				}
			} else {
				throw new Error("That should not happen!");
			}
		}
	}

	/**
	 * Send a {@link OverlayMessage} via wifi.
	 *
	 * @param m
	 *            The message to be sent
	 */
	public void sendLocalBroadcast(OverlayMessage m) {
		localTransport.send(m, localTransport.getNetInterface().getBroadcastAddress(), localTransport.getLocalPort());
	}

	/**
	 * Used to determine of UMTS is not available when it should be, i.e.
	 * the node has interest in establishing a connection.
	 * @return <code>true</code> if UMTS is not available although the node wants to use it
     */
	public boolean umtsNotAvailable() {
		NetInterface netInterface = getHost().getNetworkComponent().getByName(NetInterfaceName.MOBILE);
		return netInterface.getMaxBandwidth().getUpBW() == 0 && netInterface.isUp();
	}

	/**
	 * Used to determine of UMTS is  available when it should be, i.e.
	 * the node has interest in establishing a connection.
	 * @return <code>true</code> if UMTS is available.
	 */
	public boolean umtsAvailable() {
		NetInterface netInterface = getHost().getNetworkComponent().getByName(NetInterfaceName.MOBILE);
		return netInterface.getMaxBandwidth().getUpBW() != 0 && netInterface.isUp();
	}

	public void setLocationUpdateInterval(long interval) {
		locationUpdateInterval = interval;
	}

	public void setLocationTimeout(long timeout) {
		locationTimeout = timeout;
	}

	public void enableCentralAnalyzing(boolean active) {
		centralAnalyzing = active;
	}

	public void enableLocalAnalyzing(boolean active) {
		localAnalyzing = active;
	}

	public void setAnalyzer(DRASLocalAnalyzer analyzer) {
		this.analyzer = analyzer;
	}

	public void setMinHesitationTime(long time) {
		minHesitationTime = time;
	}

	public void setMaxHesitationTime(long time) {
		maxHesitationTime = time;
	}

}
