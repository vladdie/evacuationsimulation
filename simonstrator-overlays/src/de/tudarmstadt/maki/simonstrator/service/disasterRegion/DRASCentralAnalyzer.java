package de.tudarmstadt.maki.simonstrator.service.disasterRegion;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutPairedList;

/**
 * Analyzer to collect and process data for determination of
 * disaster regions (cells without connection) when using central analysing.
 *
 * @author Clemens Krug
 */
public interface DRASCentralAnalyzer
{
    /**
     * Starts the analyzer. Can be used if there needs to be some code
     * executed on startup, but only after the {@link DRASComponent}s have been initialised.
     */
    void start();


    /**
     * Used to pass information to the analyzer.
     *
     * @param locations the locations without connection.
     */
    void onNoUMTSLocationReceived(TimeoutPairedList<UniqueID, Location> locations);

}
