package de.tudarmstadt.maki.simonstrator.service.disasterRegion;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.simple.broadcast.BroadcastNode;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutPairedList;

/**
 * Container used to broadcast data belonging to DRAS.
 *
 * @author Clemens Krug
 */
public class DRASLocalBroadcastContainer extends BroadcastNode.BroadcastMessage
{
	private static final long serialVersionUID = 1L;

	private DRASBroadcastType type;
	private TimeoutPairedList<UniqueID, Location> locations;

	/**
	 * Instantiates a new {@link DRASLocalBroadcastContainer}
	 * @param sender sender of the message
	 * @param locations data which will be sent
	 * @param type type of the data
     */
	public DRASLocalBroadcastContainer(OverlayContact sender, TimeoutPairedList<UniqueID, Location> locations,
			DRASBroadcastType type) {
		super(sender, null, (byte) 0);
		this.locations = locations;
		this.type = type;
	}

	public TimeoutPairedList<UniqueID, Location> getLocations() {
		return locations;
	}

	public DRASBroadcastType getDRASType() {
		return type;
	}

	/**
	 * Used to determine which data the container holds.
	 */
	public enum DRASBroadcastType {
		/**
		 * Locations without umts connectivity
		 */
		NO_UMTS,

		/**
		 * Locations with available umts net
		 */
		NET_AVAILABLE
	}

}
