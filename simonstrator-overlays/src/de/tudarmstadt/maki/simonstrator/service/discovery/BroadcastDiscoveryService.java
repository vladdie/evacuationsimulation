package de.tudarmstadt.maki.simonstrator.service.discovery;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.service.DiscoveryService;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;

/**
 * TODO
 * @author Julian Wulfheide
 */
public class BroadcastDiscoveryService implements HostComponent, DiscoveryService {

    private final Host host;

    private final Map<DiscoveryRequest, DiscoveryOperation> ops = new LinkedHashMap<>();

    public BroadcastDiscoveryService(Host host) {
        this.host = host;
    }

    @Override
    public void initialize() {
        //
    }

    @Override
    public DiscoveryRequest createRequest(String serviceName, NetInterfaceName network, int port,
                                          boolean offerService) {
        return new DiscoveryRequestImpl(serviceName, network, port, offerService);
    }

    @Override
    public void startDiscovery(DiscoveryRequest request, DiscoveryListener listener) throws ProtocolNotAvailableException {
        if (ops.containsKey(request)) {
            throw new AssertionError("Already started the request.");
        }
        DiscoveryRequestImpl req = (DiscoveryRequestImpl) request;
        NetInterface net = getHost().getNetworkComponent().getByName(req.getNetwork());
        UDP udp = getHost().getTransportComponent().getProtocol(UDP.class, net.getLocalInetAddress(),
                req.getServicePort());
        DiscoveryOperation op = new DiscoveryOperation(req, udp, listener);
        ops.put(request, op);
        op.start();
    }

    @Override
    public void stopDiscovery(DiscoveryRequest request) {
        DiscoveryOperation op = ops.remove(request);
        if (op == null) {
            throw new AssertionError("Already stopped the request.");
        }
        op.stop();
    }

    @Override
    public void shutdown() {
        for (DiscoveryOperation op : ops.values()) {
            op.stop();
        }
        ops.clear();
    }

    @Override
    public Host getHost() {
        return host;
    }

    /**
     * Operation used for the service discovery.
     * @author Bjoern Richerzhagen
     */
    private class DiscoveryOperation extends PeriodicOperation<BroadcastDiscoveryService, Void>
            implements TransMessageListener {

        private final UDP udp;

        private final DiscoveryListener listener;

        private Map<OverlayContact, Long> endpoints = new LinkedHashMap<>();

        private final OverlayContact localContact;

        private final String serviceName;

        private final NetID broadcastAddr;

        private final boolean isOffering;

        private long timeout;

        private int numEndpoints;

        protected DiscoveryOperation(DiscoveryRequestImpl request, UDP udp, DiscoveryListener listener) {
            super(BroadcastDiscoveryService.this, null, request.getDiscoveryInterval());
            this.udp = udp;
            this.listener = listener;
            this.serviceName = request.getServiceName();
            udp.setTransportMessageListener(DiscoveryOperation.this);
            this.localContact = new BasicOverlayContact(getHost().getId(), udp.getNetInterface().getName(),
					udp.getTransInfo().getNetId(), udp.getTransInfo().getPort());
            this.broadcastAddr = udp.getNetInterface().getBroadcastAddress();
            this.isOffering = request.isOfferingService();
            this.timeout = 2 * request.getDiscoveryInterval();
        }

        @Override
        public void stop() {
            udp.removeTransportMessageListener();
            super.stop();
        }

        @Override
        protected void executeOnce() {
            sendOffer();
            checkTimeouts();
        }

        private void sendOffer() {
            if (isOffering) {
                // Send offer.
                OfferServiceMsg oMsg = new OfferServiceMsg(localContact, serviceName);
                udp.send(oMsg, broadcastAddr, udp.getLocalPort());
            }
        }

        private void checkTimeouts() {
            long removeIfAfter = Time.getCurrentTime() - timeout;

            endpoints = endpoints.entrySet().stream()
                    .filter(entry -> entry.getValue() > removeIfAfter) // remove if last OfferMsg was received too long ago
                    .collect(Collectors.toMap(p -> p.getKey(), p -> p.getValue()));

            int sizeAfter = endpoints.size();

            if (numEndpoints == 0 && sizeAfter > 0) {
                listener.onServiceFound(serviceName, endpoints.keySet());
            } else if (numEndpoints > 0 && sizeAfter == 0) {
                listener.onServiceLost(serviceName);
            } else if (numEndpoints != sizeAfter) {
                listener.onServiceUpdate(serviceName, endpoints.keySet());
            }

            numEndpoints = endpoints.size();
        }

        @Override
        public Void getResult() {
            return null;
        }

        @Override
        public void messageArrived(Message msg, TransInfo sender, int commID) {
            assert msg instanceof OfferServiceMsg;
            OfferServiceMsg oMsg = (OfferServiceMsg) msg;
            endpoints.put(oMsg.getSender(), Time.getCurrentTime());
            assert oMsg.getServiceName().equals(serviceName);
        }

    }

    /**
     * Offer Service Message
     * @author Bjoern Richerzhagen
     */
    private static class OfferServiceMsg extends AbstractOverlayMessage {

        private static final long serialVersionUID = 1L;

        private final String serviceName;

        public OfferServiceMsg(OverlayContact localContact, String serviceName) {
            super(localContact, null);
            this.serviceName = serviceName;
        }

        public String getServiceName() {
            return serviceName;
        }

        @Override
        public Message getPayload() {
            return null;
        }

    }


    public static class Factory implements HostComponentFactory {

        @Override
        public HostComponent createComponent(Host host) {
            return new BroadcastDiscoveryService(host);
        }

    }

}
