package de.tudarmstadt.maki.simonstrator.service.discovery;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.service.DiscoveryService.DiscoveryRequest;

/**
 * Implementation of a {@link DiscoveryRequest}
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class DiscoveryRequestImpl implements DiscoveryRequest {

	private final String name;

	private final NetInterfaceName network;

	private final int port;

	private final boolean isOffering;

	private boolean finalized = false;

	private long discoveryInterval = Time.SECOND;

	public DiscoveryRequestImpl(String name, NetInterfaceName network, int port, boolean isOffering) {
		this.name = name;
		this.network = network;
		this.port = port;
		this.isOffering = isOffering;
	}

	public int getServicePort() {
		return port;
	}

	public NetInterfaceName getNetwork() {
		return network;
	}

	public String getServiceName() {
		return name;
	}

	public boolean isOfferingService() {
		return isOffering;
	}

	public long getDiscoveryInterval() {
		return discoveryInterval;
	}

	@Override
	public void setDiscoveryInterval(long discoveryInterval) {
		if (!finalized) {
			this.discoveryInterval = discoveryInterval;
		}
	}

	public void finalize() {
		this.finalized = true;
	}

}
