package de.tudarmstadt.maki.simonstrator.service.location;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;

/**
 * Location Request class, only realizing the update interval
 *
 * @author Martin Hellwig, Bjoern Richerzhagen
 */
public class DummyLocationRequest implements LocationRequest {

	public long interval;

	public DummyLocationRequest() {
		//
	}

	public long getInterval() {
		return interval;
	}

	@Override
	public void setInterval(long interval) {
		this.interval = interval;
	}

	@Override
	public void setPriority(int priority) {
		// not used
	}

}