package de.tudarmstadt.maki.simonstrator.service.location;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

public class DummyLocation implements Location {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private double longitude;
	private double latitude;

	@SuppressWarnings("unused")
	private DummyLocation() {
		// for Kryo
	}

	public DummyLocation(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	public int getTransmissionSize() {
		return 8 + 8;
	}

	@Override
	public String toString() {
		return latitude + ";" + longitude;
	}

	@Override
	public void set(Location l) {
		this.latitude = l.getLatitude();
		this.longitude = l.getLongitude();
	}

	public void set(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	@Override
	public double getLatitude() {
		return latitude;
	}

	@Override
	public double getLongitude() {
		return longitude;
	}

	@Override
	public long getAgeOfLocation() {
		return 0;
	}

	@Override
	public DummyLocation clone() {
		return new DummyLocation(latitude, longitude);
	}

	private static double earthRadius = 6371000; // meters

	@Override
	public double distanceTo(Location dest) {
		double dLat = Math.toRadians(dest.getLatitude()-latitude);
		double dLng = Math.toRadians(dest.getLongitude()-longitude);
		double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
				Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(dest.getLatitude())) *
						Math.sin(dLng/2) * Math.sin(dLng/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		return (earthRadius * c);
	}

	@Override
	public float bearingTo(Location dest) {
		double lat1R = Math.toRadians(getLatitude());
		double lat2R = Math.toRadians(dest.getLatitude());
		double dLngR = Math.toRadians(dest.getLongitude() - getLongitude());
		double a = Math.sin(dLngR) * Math.cos(lat2R);
		double b = Math.cos(lat1R) * Math.sin(lat2R) - Math.sin(lat1R) * Math.cos(lat2R)
				* Math.cos(dLngR);
		return (float) normalizeBearing(Math.toDegrees(Math.atan2(a, b)));
	}

	private static double normalizeBearing(double bearing) {
		if (Double.isNaN(bearing) || Double.isInfinite(bearing))
			return Double.NaN;
		double bearingResult = bearing % 360;
		if (bearingResult < 0)
			bearingResult += 360;
		return bearingResult;
	}
}