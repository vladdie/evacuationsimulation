package de.tudarmstadt.maki.simonstrator.service.location;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.location.movement.MovementModel;

public class DummyLocationSensorFactory implements HostComponentFactory {

	private MovementModel movementModel;

	@Override
	public DummyLocationSensor createComponent(Host host) {
		return new DummyLocationSensor(host, movementModel);
	}

	/**
	 * Sets the movement-model, which the location sensor uses to change its
	 * position. The movement model determines the update frequency of the location reported by the
	 * sensor.
	 * 
	 * @param movementModel
	 */
	public void setMovementModel(MovementModel movementModel) {
		this.movementModel = movementModel;
	}

}
