package de.tudarmstadt.maki.simonstrator.service.location;

import java.util.LinkedHashMap;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationListener;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.service.location.movement.MovementListener;
import de.tudarmstadt.maki.simonstrator.service.location.movement.MovementModel;

/**
 * A Location Sensor that reports fake location data based on a movement model
 *
 * @author Martin Hellwig, Bjoern Richerzhagen
 */
public class DummyLocationSensor implements LocationSensor, MovementListener
{

	private Location currentLocation;

	private PeriodicLocationSender sendLocationOp;

	private final MovementModel movementModel;

	private final Host host;

	private final LinkedHashMap<LocationListener, PeriodicLocationSender> ongoingRequests;


	/**
	 * New Location Sensor instance
	 *
	 * @param host host instance
	 * @param movementModel model that alters the node's position
	 */
	public DummyLocationSensor(Host host, MovementModel movementModel) {
		this.host = host;
		this.currentLocation = movementModel.getCurrentLocation();
		this.movementModel = movementModel;
		this.ongoingRequests = new LinkedHashMap<LocationListener, PeriodicLocationSender>();
	}

	@Override
	public void initialize() {
		movementModel.requestMovementUpdates(this);
	}

	@Override
	public void shutdown() {
		movementModel.removeMovementUpdates();
		for (PeriodicLocationSender op : ongoingRequests.values()) {
			op.stop();
		}
		ongoingRequests.clear();
	}

	@Override
	public Host getHost() {
		return host;
	}

	@Override
	public Location getLastLocation() {
		return movementModel.getCurrentLocation();
	}

	@Override
	public void requestLocationUpdates(LocationRequest request,
			LocationListener listener) {
		assert (request instanceof DummyLocationRequest);
		PeriodicLocationSender op = new PeriodicLocationSender(this,
				((DummyLocationRequest) request).getInterval(), listener);
		op.start();
		ongoingRequests.put(listener, op);
	}

	@Override
	public void removeLocationUpdates(LocationListener listener) {
		PeriodicLocationSender op = ongoingRequests.remove(listener);
		if (op != null) {
			op.stop();
		}
	}

	@Override
	public LocationRequest getLocationRequest() {
		return new DummyLocationRequest();
	}

	@Override
	public void onMovementChanged(Location location) {
		currentLocation = location;
	}

	/**
	 * This class is used to periodically send the location to the requestor
	 */
	private class PeriodicLocationSender extends
		PeriodicOperation<DummyLocationSensor, Object> {

		private final LocationListener listener;

		/**
		 * @param component
		 * @param interval
		 * @param listener
		 */
		protected PeriodicLocationSender(DummyLocationSensor component,
				long interval, LocationListener listener) {
			super(component, Operations.getEmptyCallback(), interval);
			this.listener = listener;
		}

		@Override
		protected void executeOnce() {
			listener.onLocationChanged(getHost(), currentLocation);
			this.operationFinished(true);
		}

		@Override
		public Object getResult() {
			return null;
		}
	}
}