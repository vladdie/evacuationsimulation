package de.tudarmstadt.maki.simonstrator.service.location.movement;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

public interface MovementListener {

	/**
	 * Notified, if the node has moved
	 * 
	 * @param location
	 */
	public void onMovementChanged(Location location);
}
