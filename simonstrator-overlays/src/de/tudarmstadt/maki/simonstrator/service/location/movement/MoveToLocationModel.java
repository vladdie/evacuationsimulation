package de.tudarmstadt.maki.simonstrator.service.location.movement;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

/**
 *
 * Moves towards a location and stays there.
 *
 * @author Bjoern Richerzhagen
 */
public class MoveToLocationModel extends AbstractMovementModel {

    private Location currentTarget;

    private double currentBearing;

    private double stepSize;

    private boolean paused = false;

    @Override
    protected Location calculateNewPosition(Location currentPosition) {
        if (paused) {
            return currentPosition;
        }
        double distance = currentPosition.distanceTo(currentTarget);
        if (distance >= stepSize) {
            travelTowards(currentPosition, currentTarget, currentBearing, stepSize);
        } else {
            paused = true;
        }
        return currentPosition;
    }

    /**
     * Size of one "step" in meter
     * @param stepSize
     */
    public void setStepSize(double stepSize) {
        this.stepSize = stepSize;
    }

    /**
     * Updates the target
     * @param targetLocation
     */
    public void setNewTarget(Location targetLocation) {
        this.currentTarget = targetLocation;
        this.currentBearing = getCurrentLocation().bearingTo(targetLocation);
        this.paused = false;
    }
}
