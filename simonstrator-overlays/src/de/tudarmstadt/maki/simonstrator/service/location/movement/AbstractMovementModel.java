package de.tudarmstadt.maki.simonstrator.service.location.movement;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.location.DummyLocation;

/**
 * Defines some configurables that are valid for all models
 *
 * @author Bjoern Richerzhagen
 */
public abstract class AbstractMovementModel implements MovementModel {

    public long timeBetweenUpdates = -1;

    private Location currentPosition;

    private double initialLongitude = Double.NaN;

    private double initialLatitude = Double.NaN;

    protected MovementListener listener;

    private boolean updating = false;

    /**
     * Calculate the initial position
     *
     * @return initial location
     */
    private Location getInitialPosition() {
        if (currentPosition == null) {
            if (initialLongitude != Double.NaN && initialLatitude != Double.NaN) {
                currentPosition = new DummyLocation(initialLatitude, initialLongitude);
            } else {
                throw new AssertionError("Invalid instantiation: no initial position given!");
            }
        }
        return currentPosition;
    }

    @Override
    public void requestMovementUpdates(MovementListener l) {
        this.listener = l;
        // We use this method to start updates once the first position has been requested.
        if (!updating && timeBetweenUpdates != -1) {
            Event.scheduleWithDelay(timeBetweenUpdates, new PeriodicEventHandler(), null, 0);
        }
    }

    @Override
    public void removeMovementUpdates() {
        this.listener = null;
        this.updating = false;
    }

    public void setInitialPosition(Location location) {
        this.currentPosition = location;
    }

    public void setInitialLong(double longitude) {
        this.initialLongitude = longitude;
    }

    public void setInitialLat(double latitude) {
        this.initialLatitude = latitude;
    }

    public void setTimeBetweenUpdates(long timeBetweenUpdates) {
        this.timeBetweenUpdates = timeBetweenUpdates;
    }

    /**
     *
     * @param toUpdate this object will be updated
     * @param target
     * @param bearing
     * @param distance in meters
     */
    protected void travelTowards(Location toUpdate, Location target, double bearing, double distance) {
        double bR = Math.toRadians(bearing);
        double lat1R = Math.toRadians(toUpdate.getLatitude());
        double lon1R = Math.toRadians(toUpdate.getLongitude());
        double dR = distance / 6371009; // Radius (Earth) in meters

        double a = Math.sin(dR) * Math.cos(lat1R);
        double lat2 = Math.asin(Math.sin(lat1R) * Math.cos(dR) + a * Math.cos(bR));
        double lon2 = lon1R
                + Math.atan2(Math.sin(bR) * a, Math.cos(dR) - Math.sin(lat1R) * Math.sin(lat2));
        toUpdate.set(new DummyLocation(Math.toDegrees(lat2), Math.toDegrees(lon2)));
    }

    @Override
    public Location getCurrentLocation() {
        return (currentPosition == null ? getInitialPosition() : currentPosition);
    }

    /**
     * Called, whenever timeBetweenUpdates elapsed. If the model is not configured with a
     * timeBetweenUpdates, this method is never called.
     *
     * @return the new location
     */
    protected abstract Location calculateNewPosition(Location currentPosition);

    private class PeriodicEventHandler implements EventHandler {
        @Override
        public void eventOccurred(Object content, int type) {
            if (updating) {
                currentPosition = calculateNewPosition(currentPosition);
                Event.scheduleWithDelay(timeBetweenUpdates, PeriodicEventHandler.this, null, 0);
            }
        }
    }
}
