package de.tudarmstadt.maki.simonstrator.service.location.movement;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

public interface MovementModel {

	/**
	 * Gets the current location of the node. Usually, this is used to determine the initial
	 * position, as updates are then announced via the movement listener.
	 *
	 * @return location
	 */
	public Location getCurrentLocation();

	/**
	 * Start receiving movement updates on the provided listener
	 * 
	 * @param l
	 */
	public void requestMovementUpdates(MovementListener l);

	/**
	 * Get rid of movement updates for the corresponding listener
	 */
	public void removeMovementUpdates();
}
