package de.tudarmstadt.maki.simonstrator.service.latency;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.latency.LatencyRequest;

/**
 * Implementation of a {@link LatencyRequest} in its minimal implementation.
 * 
 * @author Nils Richerzhagen
 *
 */
public class LatencyRequestImpl implements LatencyRequest {

	private final long interval;

	public LatencyRequestImpl(long interval) {
		this.interval = interval;
	}

	@Override
	public long getInterval() {
		return interval;
	}
}
