# LatencySensor
## and its current implementation `PeriodicLatencySensorService`

_based on the initial Version 22.04.2016 by Nils Richerzhagen

# In PROGRESS not finished!

The `PeriodicLatencySensorService` is used to measure the latency between two entities using a given network type e.g. `CELL, MOBILE, ...`.
Calling the Service on a Host A one can determine the `LatencyRequest` Parameters (e.g. the interval length between successive measurements).
After the `LatencyRequest` is obtained from the respective `LatencySensor` implementation, the `LatencySensor` can be called with a Host B to which the latency should be measured, a listener who wants the latency updates (irrespective to the SiS) and the `LatencyRequest`.   

## OPTIONAL - Second Reply Concept
The second reply concept is used to measure the cellular latency for both entities with three messages.
If the `withSecondReply` parameter is set from the configuration for the `PeriodicLatencySensorService` it sends out a third message back to the "to be measured" entity. In doing so we are able to measure the latencies for both entities with three messages.
The latency is then available via the `lastMeasuredLatencies` Storage Map for SiS Interaction. Enabling this concept is sensible when the service is started at node A and measures the latency to node B. If node B also is requested for the latency between A and B normally SiS Requests and the corresponding "Auflösestrategie" of the Monitoring would at least result in 1-2 additional messages (assuming only one-way piggybacking).


# Usage of the latency service

### Setup in configuration files

``` java
<**LatencyMeasurementService** class="de.tudarmstadt.maki.simonstrator.service.latency.PeriodicLatencySensorFactory"
		network="MOBILE" withSecondReply="false" />
```
and on the respective `HostBuilder` the tag **<LatencyMeasurementService />** must be used.

### Setup for components that are interested in updates of the latencies
Those components must implement the `LatencyListener` interface and should register themselves when they call the method `requestLatencyUpdates`.

``` java
...
private LatencySensor latencyMeasurementService;
private LatencyRequest latencyRequest;
...
try {
	latencyMeasurementService = getHost().getComponent(LatencySensor.class);
	latencyRequest = latencyMeasurementService.getLatencyRequest(20 * Time.SECOND);
	latencyMeasurementService.requestLatencyUpdates(handler, listener, latencyRequest);
} catch (ComponentNotAvailableException e) {
	throw new AssertionError("LatencySensor not available.");
}
```

### SiS interaction
If you want to retrieve the current value of the observed latency from the SiS you can call it as visible in the following.
There a observation of the latency from the current entity to the respective handler (`nodeID`) can be requested. If the infromation is not available or the there is no `SiSComponent` instantiated on this host either a `ComponentNotAvailableException` or a `InformationNotAvailableException` is thrown.   

``` java
...
SiSComponent sis;
try {
	sis = getHost().getComponent(SiSComponent.class);
	SiSRequest request = new SiSRequest();
	double currentMeasuredLatency = sis.get().localObservationOf(handler nodeID, SiSTypes.LATENCY_CELL, request);
} catch (ComponentNotAvailableException | InformationNotAvailableException e) {
	e.printStackTrace();
}
``` 