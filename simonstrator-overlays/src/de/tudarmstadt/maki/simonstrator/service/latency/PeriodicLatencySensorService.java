package de.tudarmstadt.maki.simonstrator.service.latency;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.latency.LatencyRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.latency.LatencySensor;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSDataCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInfoProperties;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider.SiSProviderHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageCallback;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;

/**
 * 
 * *Added on 15.04. NR*
 * 
 * Second reply concept. If withSecondReply parameter is set the {@link PeriodicLatencySensorService} sends out a third
 * message back to the "to be measured" node. In doing so we are able to measure the latencies for both entities with
 * three messages. The latency is then available via the lastMeasuredLatencies Storage Map for SiS Interaction. Enabling
 * this concept is sensible when the service is started at node A and measures the latency to node B. If node B also is
 * requested for the latency between A and B normally SiS Requests and the corresponding "Auflösestrategie" of the
 * Monitoring would at least result in 1-2 additional messages (assuming only one-way piggybacking).
 * 
 * TODO Wie hier umgehen mit verschiedenen Requests die sich nicht/teils überschneiden, z.B. verschiedene
 * NetInterfaceNames? Must be handled in method "requestLatencyUpdates"
 * 
 * @author Nils Richerzhagen
 *
 */
public class PeriodicLatencySensorService implements LatencySensor, TransMessageListener, HostComponent {

	private final Host host;

	private final NetInterfaceName network;

	private UDP udp;

	private boolean withSecondReply;

	private Map<LatencyListener, LatencyMeasurementOperation> periodicOperations = new LinkedHashMap<LatencyListener, LatencyMeasurementOperation>();

	/*
	 * Storage Map for SiS interaction
	 */
	private Map<INodeID, Double> lastMeasuredLatencies = new LinkedHashMap<INodeID, Double>();

	/**
	 * Used from within the {@link PeriodicLatencySensorFactory}.
	 * 
	 * @param host
	 *            - the {@link Host} on which the {@link PeriodicLatencySensorService} is instantiated.
	 * @param network
	 *            - the {@link NetInterfaceName} in which the {@link PeriodicLatencySensorService} should operate.
	 */
	public PeriodicLatencySensorService(Host host, NetInterfaceName network, boolean withSecondReply) {
		this.host = host;
		this.network = network;
		this.withSecondReply = withSecondReply;
	}

	@Override
	public void initialize() {
		NetInterface net = getHost().getNetworkComponent().getByName(network);
		try {
			udp = getHost().getTransportComponent().getProtocol(UDP.class, net.getLocalInetAddress(),
					LatencySensor._LATENCY_SERVICE_PORT);
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError("Need protocol for discovery.");
		}
		udp.setTransportMessageListener(PeriodicLatencySensorService.this);

		SiSComponent sis;
		try {
			sis = getHost().getComponent(SiSComponent.class);

			sis.provide().nodeState(SiSTypes.LATENCY_CELL, new SiSDataCallback<Double>() {

				@Override
				public Double getValue(INodeID nodeID, SiSProviderHandle providerHandle) throws InformationNotAvailableException {
					Monitor.log(PeriodicLatencySensorService.class, Level.DEBUG, "Incoming SiS getValue request.");
					/*
					 * Cellular Latency can only be delived when this service operates in the mobile network.
					 */
					if (PeriodicLatencySensorService.this.network != NetInterfaceName.MOBILE) {
						throw new InformationNotAvailableException();
					}
					/*
					 * If not available resolver must care the problem including the potential possibility to start the
					 * LatencyMeasurementService. However, this is not started here!!
					 */
					if (lastMeasuredLatencies.containsKey(nodeID)) {
						Monitor.log(PeriodicLatencySensorService.class, Level.DEBUG, "Answering SiS getValue request.");
						return lastMeasuredLatencies.get(nodeID);
					} else {
						throw new InformationNotAvailableException();
					}
				}

				@Override
				public Set<INodeID> getObservedNodes() {
					/*
					 * Return the Set of Probes that are currently measured.
					 */
					return lastMeasuredLatencies.keySet();
				}

				@Override
				public SiSInfoProperties getInfoProperties() {
					// return new SiSInfoProperties().setScope(SiSScope.NODE_LOCAL);
					return new SiSInfoProperties();
				}
			});
		} catch (ComponentNotAvailableException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void shutdown() {
		for (LatencyMeasurementOperation currentOperation : periodicOperations.values()) {
			currentOperation.stop();
		}
		periodicOperations.clear();
	}

	@Override
	public Host getHost() {
		return this.host;
	}

	@Override
	public void requestLatencyUpdates(ProbeHandler handler, LatencyListener listener, LatencyRequest request) {
		/*
		 * Currently requests (of any content) received of a listener that is already registered overwrite the old
		 * listener etc.
		 */
		if (periodicOperations.containsKey(listener)) {
			LatencyMeasurementOperation op = periodicOperations.remove(listener);
			if (op == null) {
				throw new AssertionError("Periodic operation already stopped or never activated!");
			}
			op.stop();
		}

		LatencyMeasurementOperation periodicOperation = new LatencyMeasurementOperation(request, udp, listener, handler);
		periodicOperations.put(listener, periodicOperation);
		periodicOperation.start();
	}

	@Override
	public void removeLatencyUpdates(LatencyListener listener) {
		assert periodicOperations.containsKey(
				listener) : "Listener should be contained in current active listeners, otherwise wrong or dublicated method involcation.";

		LatencyMeasurementOperation op = periodicOperations.remove(listener);
		if (op == null) {
			throw new AssertionError("Periodic operation already stopped or never activated!");
		}
		op.stop();
	}

	@Override
	public LatencyRequest getLatencyRequest(long interval) {
		return new LatencyRequestImpl(interval);
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		assert msg instanceof LatencyPingMsg : "Message should be of type LatencyPingMsg.";

		LatencyPingMsg latencyPingMsg = (LatencyPingMsg) msg;
		/*
		 * If withSecondReply Parameter.
		 */
		if (withSecondReply) {
			/*
			 * When replyTimestamp has been set (so third message) just update the latency here and do not send again.
			 */
			if (latencyPingMsg.isValidReplyLatency()) {
				long replyLatency = latencyPingMsg.getReplyLatency();
				assert replyLatency >= 0 : "Latency must be greater or equal to 0!";
				// Update current storage here for SiS Requests. The receiver is the node to which this reply latency
				// has been measured (remember three steps)
				lastMeasuredLatencies.put(latencyPingMsg.getSender().getNodeID(), (double) replyLatency);

				Monitor.log(PeriodicLatencySensorService.class, Level.DEBUG, "Rec 2nd Reply Lat: " + host.getId() + " to "
						+ latencyPingMsg.getSender().getNodeID() + " "
								+ replyLatency);
				return;
			}

			/*
			 * Add replyTimestamp for send of reply.
			 */
			latencyPingMsg.addReplyTimestamp();
			Monitor.log(PeriodicLatencySensorService.class, Level.DEBUG,
					"Add reply Timestamp on 1st Reply from " + host.getId() + " to " + latencyPingMsg.getSender().getNodeID());
		}

		udp.sendReply(msg, sender.getNetId(), sender.getPort(), commID);
		Monitor.log(PeriodicLatencySensorService.class, Level.DEBUG,
				"LatencyService received PingMsg at Host " + getHost().getId());
	}

	/**
	 * {@link PeriodicOperation} for each incoming {@link LatencyRequest} + {@link LatencyListener} providing the sensor
	 * readings. Sending the ping messages in the specified interval of the {@link LatencyRequest}, updating the SiS
	 * Map, and the {@link LatencyListener}.
	 * 
	 * @author Nils Richerzhagen
	 */
	private class LatencyMeasurementOperation extends PeriodicOperation<PeriodicLatencySensorService, Void>
			implements TransMessageCallback {

		private final UDP udp;

		private final LatencyListener listener;

		private final ProbeHandler probeHandler;

		private final OverlayContact localContact;

		protected LatencyMeasurementOperation(LatencyRequest request, UDP udp, LatencyListener listener,
				ProbeHandler probeHandler) {
			super(PeriodicLatencySensorService.this, null, request.getInterval());
			this.udp = udp;
			this.listener = listener;
			this.probeHandler = probeHandler;
			this.localContact = new BasicOverlayContact(getHost().getId(), udp.getNetInterface().getName(),
					udp.getTransInfo().getNetId(), udp.getTransInfo().getPort());
		}

		@Override
		public void stop() {
			udp.removeTransportMessageListener();
			super.stop();
		}

		@Override
		protected void executeOnce() {
			sendPing();
		}

		/**
		 * Send the {@link LatencyPingMsg}s to the respective {@link ProbeHandler}s in the network using the given
		 * {@link NetInterfaceName} from the configuration.
		 */
		private void sendPing() {
			for (OverlayContact currentProbeHandler : probeHandler.getContacts()) {
				LatencyPingMsg latencyPingMsg = new LatencyPingMsg(localContact, currentProbeHandler);
				// FIXME Maybe use other Timeout length later? Currently timeouts are ignored anyway, but they might be
				// interesting later.
				udp.sendAndWait(latencyPingMsg, currentProbeHandler.getNetID(udp.getNetInterface().getName()), udp.getLocalPort(),
						LatencyMeasurementOperation.this, 1000 * Time.HOUR);
			}
		}

		@Override
		public Void getResult() {
			return null;
		}

		@Override
		public void receive(Message reply, TransInfo source, int commId) {
			assert reply instanceof LatencyPingMsg;

			LatencyPingMsg latencyPingMsg = (LatencyPingMsg) reply;

			for (OverlayContact currentProbeHandler : probeHandler.getContacts()) {
				/*
				 * Message from one of the requested probes?
				 */
				if (currentProbeHandler.getNetID(udp.getNetInterface().getName()).equals(source.getNetId())) {
					long currentLatency = latencyPingMsg.getCurrentLatency();
					assert currentLatency >= 0 : "Latency must be greater or equal to 0!";
					// Update current storage here for SiS Requests.
					Monitor.log(PeriodicLatencySensorService.class, Level.DEBUG,
							"Rec 1st reply: " + PeriodicLatencySensorService.this.host.getId() + " to "
							+ currentProbeHandler.getNodeID() + " " + currentLatency);
					lastMeasuredLatencies.put(currentProbeHandler.getNodeID(), (double) currentLatency);
					// Update listener
					listener.updatedLatency(currentProbeHandler, currentLatency);
				}
			}

			/*
			 * If with secondReply.
			 */
			if (withSecondReply) {
				Monitor.log(PeriodicLatencySensorService.class, Level.DEBUG,
						"Send 2nd Reply from " + host.getId() + " to " + latencyPingMsg.getReceiver().getNodeID());
				udp.send(latencyPingMsg, source.getNetId(), udp.getLocalPort());
			}
		}

		@Override
		public void messageTimeoutOccured(int commId) {
			// TODO Timeouts are currently ignored. Right? How to handle Timeouts?
			// throw new AssertionError("What to do with timeouts?");
		}
	}

	/**
	 * The message that is used for request-reply scheme of the ping latency measurement of the {@link LatencySensor}.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	public class LatencyPingMsg extends AbstractOverlayMessage {

		private static final long serialVersionUID = 1L;

		private final long sendTimestamp;

		private long replyTimestamp;

		private boolean validReplyLatency = false;

		public LatencyPingMsg(OverlayContact localContact, OverlayContact receiver) {
			super(localContact, receiver);
			this.sendTimestamp = Time.getCurrentTime();
		}

		@Override
		public Message getPayload() {
			return null;
		}

		/**
		 * Returns the latency (one-way) in Milliseconds the message has taken. That's why the time is divided by 2.
		 * 
		 * @return latency - in Milliseconds.
		 */
		public long getCurrentLatency() {
			return ((Time.getCurrentTime() - sendTimestamp) / 2) / Time.MILLISECOND;
		}

		/**
		 * When the message is received for reply, one might add a reply timestamp.
		 */
		public void addReplyTimestamp() {
			this.validReplyLatency = true;
			this.replyTimestamp = Time.getCurrentTime();
		}

		/**
		 * Returns if the replyLatency timestamp has ever been set, so that a latency calculation is valid.
		 * 
		 * @return
		 */
		public boolean isValidReplyLatency() {
			return validReplyLatency;
		}

		/**
		 * Returns the reply latency (one-way) in Milliseconds the message has taken. That's why the time is divided by
		 * 2.
		 * 
		 * @return latency - in Milliseconds.
		 */
		public long getReplyLatency() {
			if (isValidReplyLatency()) {
				return (long) ((Time.getCurrentTime() - replyTimestamp) / 2) / Time.MILLISECOND;
			}
			else {
				throw new AssertionError("The value of the reply Timestamp has never been set.");
			}
		}
	}
}
