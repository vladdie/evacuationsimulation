package de.tudarmstadt.maki.simonstrator.service.latency;

import javax.naming.ConfigurationException;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.latency.LatencySensor;

/**
 * Factory for the {@link PeriodicLatencySensorService} {@link HostComponent}.
 * 
 * Use this one to instantiate the {@link LatencySensor} on {@link Host}s.
 * 
 * @author Nils Richerzhagen
 *
 */
public class PeriodicLatencySensorFactory implements HostComponentFactory {

	/**
	 * The network the {@link LatencySensor} should work with must be specified in the configuration.
	 */
	private NetInterfaceName network;
	
	private boolean withSecondReply;

	public void setNetwork(String network) throws ConfigurationException {
		network = network.toUpperCase();
		try {
			this.network = NetInterfaceName.valueOf(network);
		} catch (IllegalArgumentException e) {
			throw new ConfigurationException("The NetInterfaceName " + network + " is unknown.");
		}
		if (this.network == null) {
			throw new ConfigurationException("The NetInterfaceName " + network + " is unknown.");
		}
	}

	public void setWithSecondReply(boolean withSecondReply) {
		this.withSecondReply = withSecondReply;
	}

	@Override
	public HostComponent createComponent(Host host) {
		if (network == null) {
			throw new AssertionError();
		}
		return new PeriodicLatencySensorService(host, network, withSecondReply);
	}
}
