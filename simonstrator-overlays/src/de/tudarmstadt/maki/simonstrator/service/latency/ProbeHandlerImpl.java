package de.tudarmstadt.maki.simonstrator.service.latency;

import java.util.LinkedHashSet;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.latency.LatencySensor.ProbeHandler;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * A encapsulating Object for {@link OverlayContact} as {@link OverlayContacts} is not visible in the API.
 * 
 * TODO maybe change {@link OverlayContacts} place to API visibility?
 * 
 * @author Nils Richerzhagen
 *
 */
public class ProbeHandlerImpl implements ProbeHandler {

	private Set<OverlayContact> probes;

	public ProbeHandlerImpl(OverlayContact probe) {
		this.probes = new LinkedHashSet<OverlayContact>();
		this.probes.add(probe);
	}

	public ProbeHandlerImpl(Set<OverlayContact> probes) {
		this.probes = probes;
	}

	@Override
	public Set<OverlayContact> getContacts() {
		return probes;
	}
}
