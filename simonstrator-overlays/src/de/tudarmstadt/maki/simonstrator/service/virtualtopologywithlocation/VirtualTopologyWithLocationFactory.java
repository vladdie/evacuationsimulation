package de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;

/**
* Factory-class for the virtual topology-service
* 
* @author Martin Hellwig
* @version 17.04.2015
* 
*/
public class VirtualTopologyWithLocationFactory implements HostComponentFactory {

	private long locationRequestInterval = 10 * Time.SECOND;
	private long neighborActivityTimeout = 20 * Time.SECOND;
	private long periodicTopologyBroadcastInterval = 30 * Time.SECOND;
	private float communicationRange = 50;
	private boolean useRandomPort = false;
	private int startPort; // Won't be used if the above boolean-value is false
	private int destinationPort; // Won't be used if the above boolean-value is
									// false

	public VirtualTopologyWithLocationFactory() {
		super();
	}

	@Override
	public VirtualTopologyWithLocationNode createComponent(Host host) {
		VirtualTopologyWithLocationNode vtNode = new VirtualTopologyWithLocationNode(
				host);
		vtNode.setLocationRequestInterval(locationRequestInterval);
		vtNode.setNeighborActivityTimeout(neighborActivityTimeout);
		vtNode.setPeriodicBroadcastInterval(periodicTopologyBroadcastInterval);
		vtNode.setCommunicationRange(communicationRange);
		vtNode.setUseRandomPort(useRandomPort);
		vtNode.setStartPort(startPort);
		vtNode.setDestinationPort(destinationPort);
		return vtNode;
	}

	/**
	 * Sets the interval of this node how often it should ask for its new
	 * location
	 * 
	 * @param interval
	 */
	public void setLocationRequestInterval(long interval) {
		this.locationRequestInterval = interval;
	}

	/**
	 * Sets the time of this after which it deletes other nodes, if it hasn't
	 * more actual information location
	 * 
	 * @param timeout
	 */
	public void setNeighborActivityTimeout(long timeout) {
		this.neighborActivityTimeout = timeout;
	}

	/**
	 * Sets the interval of this node how often it broadcasts its topology
	 * location
	 * 
	 * @param interval
	 */
	public void setPeriodicTopologyBroadcastInterval(long interval) {
		this.periodicTopologyBroadcastInterval = interval;
	}

	/**
	 * Sets the range of this node how far it can communicate
	 * 
	 * @param range
	 */
	public void setCommunicationRange(double range) {
		this.communicationRange = (float) range;
	}

	/**
	 * Set this true, if this node should use a random port (specified in the
	 * next two parameters) instead of the fixed one This is useful if u want to
	 * run multiple sessions on one host
	 * 
	 * @param setRandomPort
	 */
	public void setUseRandomPort(boolean useRandomPort) {
		this.useRandomPort = useRandomPort;
	}

	/**
	 * If "useRandomPort" is true, the node will start to use a random port
	 * beginning with this number
	 * 
	 * @param startPort
	 */
	public void setStartPort(int startPort) {
		this.startPort = startPort;
	}

	/**
	 * If "useRandomPort" is true, the node will start to use a random port
	 * ending with this number
	 * 
	 * @param endPort
	 */
	public void setDestinationPort(int destinationPort) {
		this.destinationPort = destinationPort;
	}
}