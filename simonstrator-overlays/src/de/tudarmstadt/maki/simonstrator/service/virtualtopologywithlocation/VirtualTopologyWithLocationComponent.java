/*
 * Copyright (c) 2005-2015 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation;

import java.util.HashMap;

import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.data.NodeMetaData;

/**
 * This interface provides the public methods for steering the VirtualTopology together with location-information.
 * It allows to define/change a node's current position by simply set its longitude and latitude
 * 
 * @author Martin Hellwig
 * @version 1.0 15.04.2015
 * 
 */
public interface VirtualTopologyWithLocationComponent extends HostComponent {

	/**
	 * Update the node's current location by setting a new (gps)-location. This can be
	 * triggered for example by manual set locations or via the gps-sensor of android-phones
	 * 
	 * @param location
	 */
	public void updateCurrentLocation(Location location);
	
	/**
	 * Returns the actual meta data of this component
	 * This includes the location and its actual communication range.
	 * Each time this method is called the seqNo will be incremented 
	 * @return
	 */
	public NodeMetaData getNodeMetaData();

	/**
	 * Tells the virtual topology to always deliver messages on the specified
	 * port, regardless of the current virtual topology. Useful for example for
	 * maintenance or monitoring messages that do not affect the
	 * simulated/tested scenario.
	 * 
	 * @param port
	 */
	public void addPortToIgnore(int port);

	/**
	 * Revoke an ignore-rule for a given port. Messages delivered via this port
	 * will be subject to filtering again.
	 * 
	 * @param port
	 */
	public void removePortToIgnore(int port);

	/**
	 * Set a new virtual topology. The component will periodically distribute it
	 * to other nodes (via 1-hop broadcast), if the flag is set to true. Ensure,
	 * that this is only called on ONE component, as otherwise you might run
	 * into consistency-related problems.
	 * 
	 * @param topology
	 */
	public void updateAndAnnounceTopology(HashMap<NetID, NodeMetaData> nodes);

	/**
	 * Returns the current topology
	 * 
	 * @return
	 */
	public HashMap<NetID, NodeMetaData> getCurrentTopologyWithLocation();

}

