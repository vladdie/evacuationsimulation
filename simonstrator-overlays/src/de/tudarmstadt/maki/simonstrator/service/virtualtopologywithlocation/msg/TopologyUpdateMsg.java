package de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.msg;

import java.util.HashMap;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.data.NodeMetaData;

/**
 * This message tells the receivers, from which senders they are able to receive
 * messages according to the current state of the virtual topology. At the same
 * time, this set of NetIDs also defines the recipients of a broadcast in the
 * virtual topology.
 * 
 * @author Martin Hellwig
 * @version 20.04.2015
 * 
 */
public class TopologyUpdateMsg implements Message {
	
	private static final long serialVersionUID = 1L;

	private HashMap<NetID, NodeMetaData> nodes;

	@SuppressWarnings("unused")
	private TopologyUpdateMsg() {
		// for Kryo
	}

	public TopologyUpdateMsg(HashMap<NetID, NodeMetaData> nodes) {
		this.nodes = nodes;
	}

	public HashMap<NetID, NodeMetaData> getTopology() {
		return nodes;
	}

	@Override
	public long getSize() {
		if(nodes.size() > 0 ) {
			// return NodeMetaData.getTransmissionSize() * nodes.size();
			return nodes.size() * 30;
		}
		else return 0;
	}

	@Override
	public Message getPayload() {
		return null;
	}

}
