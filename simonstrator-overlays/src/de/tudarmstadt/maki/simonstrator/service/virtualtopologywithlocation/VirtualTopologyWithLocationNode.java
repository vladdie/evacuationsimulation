package de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.Serializer;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationListener;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ServiceNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransportProtocol;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.api.component.transport.service.FirewallService;
import de.tudarmstadt.maki.simonstrator.api.component.transport.service.PiggybackMessageService;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;
import de.tudarmstadt.maki.simonstrator.service.location.DummyLocation;
import de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.data.NodeMetaData;
import de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.msg.CurrentPositionMsg;
import de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.msg.TopologyUpdateMsg;

/**
 * A node within the virtual Topology (i.e., a smartphone). The position of the
 * node is defined by a location-object hold in the meta-data
 * 
 * @author Martin Hellwig
 * @version 22.04.2015
 * 
 */
public class VirtualTopologyWithLocationNode extends AbstractOverlayNode
		implements VirtualTopologyWithLocationComponent,
		PiggybackMessageService, FirewallService, TransMessageListener,
		LocationListener {

	private final static byte PIGGYBACK_SERVICE_ID = 6;

	private final static byte MSG_TOPOLOGY_UPDATE = 2;

	private final static byte MSG_CURRENT_POSITION = 3;

	protected final static int DEFAULT_VIRTUAL_TOPOLOGY_PORT = 5328;

	protected static int virtualTopologyPort;

	private static long LOCATION_REQUEST_INTERVAL = 10 * Time.SECOND; // Default
																			// value

	private static float COMMNICATION_RANGE = 10.0f; // Default value

	private static long NEIGHBOR_ACTIVITY_TIMEOUT = 30 * Time.SECOND; // Default
																			// value

	private final Set<Integer> whitelistedPorts = new LinkedHashSet<Integer>();

	private UDP udp;

	private NodeMetaData metaData;

	public static long PERIODIC_BROADCAST_INTERVAL = 20 * Time.SECOND; // Default
																				// value

	private PeriodicTopologyBroadcast topoBroadcastOp;

	//This will always be updated, if a neighbor node sends its meta-data
	private HashMap<NetID, NodeMetaData> lastKnownPositions = new LinkedHashMap<NetID, NodeMetaData>();

	// If this is set to true, this node will randomly choose a port to send and
	// will check on various ingoing ports
	private boolean useRandomPort;
	private int startPort;
	private int destinationPort;

	/**
	 * @param host
	 */
	public VirtualTopologyWithLocationNode(Host host) {
		super(host);
	}

	public void setLocationRequestInterval(long requestInterval) {
		LOCATION_REQUEST_INTERVAL = requestInterval;
	}

	public void setNeighborActivityTimeout(long activityTimeout) {
		NEIGHBOR_ACTIVITY_TIMEOUT = activityTimeout;
	}

	public void setPeriodicBroadcastInterval(long broadcastInterval) {
		PERIODIC_BROADCAST_INTERVAL = broadcastInterval;
	}

	public void setCommunicationRange(float commRange) {
		COMMNICATION_RANGE = commRange;
	}

	public void setUseRandomPort(boolean useRandomPort) {
		this.useRandomPort = useRandomPort;
	}

	public void setStartPort(int startPort) {
		this.startPort = startPort;
	}

	public void setDestinationPort(int destinationPort) {
		this.destinationPort = destinationPort;
	}

	@Override
	public void initialize() {
		super.initialize();
		try {
			NetInterface net = getHost().getNetworkComponent().getByName(
					NetInterfaceName.WIFI);
			if (net == null) {
				net = getHost().getNetworkComponent().getByName(
						NetInterfaceName.ETHERNET);
			}
			if (net == null) {
				throw new AssertionError("No Net...");
			}

			// set the desired port
			if (useRandomPort) {
				virtualTopologyPort = startPort
						+ (int) (Math.random() * (destinationPort - startPort));
			} else {
				virtualTopologyPort = DEFAULT_VIRTUAL_TOPOLOGY_PORT;
			}

			udp = getAndBindUDP(net.getLocalInetAddress(), virtualTopologyPort,
					new Serializer() {

						@Override
						public Class<?>[] getSerializableTypes() {
							return VirtualTopologyWithLocationNode.this.getSerializableTypes();
						}

						@Override
						public void serialize(OutputStream out, Message msg) {
							// no longer used
						}

						@Override
						public Message create(InputStream in) {
							return null;
						}
					});
			udp.setTransportMessageListener(this);

			// Register as a service
			getHost().getTransportComponent().registerService(
					PiggybackMessageService.class, this);
			getHost().getTransportComponent().registerService(
					FirewallService.class, this);
		} catch (ServiceNotAvailableException e) {
			throw new AssertionError(
					"The full functionality of the VirtualTopology is not available on this platform. Ensure, that you are using the latest version of the platform's runtime!");
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError(
					"This service requires a WiFi-AdHoc-Netinterface to work properly.");
		}

		try {
			LocationSensor locSensor = getHost().getComponent(
					LocationSensor.class);
			LocationRequest locReq = locSensor.getLocationRequest();
			locReq.setInterval(LOCATION_REQUEST_INTERVAL);
			locSensor.requestLocationUpdates(locReq, this);

			metaData = new NodeMetaData(udp.getNetInterface()
					.getLocalInetAddress(), locSensor.getLastLocation(),
					COMMNICATION_RANGE, System.currentTimeMillis());

			topoBroadcastOp = new PeriodicTopologyBroadcast(this,
					PERIODIC_BROADCAST_INTERVAL);
			updateAndAnnounceTopology(getCurrentTopologyWithLocation());

		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("Location Component required.");
		}
		assert metaData != null;
	}

	/**
	 * @return the udp
	 */
	protected UDP getUdp() {
		return udp;
	}

	@Override
	public void onLocationChanged(Host host, Location location) {
		updateCurrentLocation(location);
	}

	@Override
	public void addPortToIgnore(int port) {
		whitelistedPorts.add(port);
	}

	@Override
	public void removePortToIgnore(int port) {
		whitelistedPorts.remove(port);
	}

	@Override
	public OverlayContact getLocalOverlayContact() {
		throw new AssertionError("Not supported by this component.");
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		setPeerStatus(PeerStatus.PRESENT);
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		setPeerStatus(PeerStatus.ABSENT);
	}

	@Override
	public Class<?>[] getSerializableTypes() {
		return new Class<?>[]{NodeMetaData.class, CurrentPositionMsg.class, TopologyUpdateMsg.class, DummyLocation.class};
	}

	@Override
	public void serialize(OutputStream out, Message msg) {
		try {
			if (msg instanceof CurrentPositionMsg) {
				out.write(MSG_CURRENT_POSITION);
				PrintStream printStream = new PrintStream(out);
				printStream.print(((CurrentPositionMsg) msg).getSenderMetaData().toString());
				printStream.close();
			} else if (msg instanceof TopologyUpdateMsg) {
				HashMap<NetID, NodeMetaData> nodesToSend = getCurrentTopologyWithLocation();
				
				out.write(MSG_TOPOLOGY_UPDATE);
				PrintStream printStream = new PrintStream(out);
				for (NodeMetaData metaDataToSend : nodesToSend.values()) {
					printStream.println(metaDataToSend.toString());
				}
				printStream.close();
			}
		} catch (IOException e) {
			throw new AssertionError("Serialization failed...");
		}
	}

	/**
	 * This Deserialization doesn't work, because the Location and NetID-objects do not offer deserialization via Strings
	 */
	@Override
	public Message create(InputStream in) {
		try {
			byte type = (byte) in.read();
			
			InputStreamReader is = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(is);
			String readMetaData = br.readLine();
			switch (type) {
			case MSG_CURRENT_POSITION:
				is = new InputStreamReader(in);
				br = new BufferedReader(is);
				readMetaData = br.readLine();
				return new CurrentPositionMsg(new NodeMetaData(readMetaData));

			case MSG_TOPOLOGY_UPDATE:
				HashMap<NetID, NodeMetaData> nodesToSend = new HashMap<NetID, NodeMetaData>();
				
				is = new InputStreamReader(in);
				br = new BufferedReader(is);
				readMetaData = br.readLine();

				while(readMetaData != null) {
					NodeMetaData node = new NodeMetaData(readMetaData);
					nodesToSend.put(node.getNetID(), node);
					readMetaData =br.readLine();
				}
				return new TopologyUpdateMsg(nodesToSend);

			default:
				throw new AssertionError("Unknown message type...");
			}
		} catch (IOException e) {
			throw new AssertionError("De-Serialization failed...");
		}
	}

	@Override
	public Message piggybackOnSendMessage(NetID to, int receiverPort,
			TransportProtocol protocol) {
		metaData.incSeqNo();
		metaData.setLastInformationTimestamp(Time.getCurrentTime());
		return new CurrentPositionMsg(metaData);
	}

	@Override
	/**
	 * This method is called if any message arrives at this client. Either via a broadcast or via piggybacked data on top of normal data
	 * @param msg
	 * @param sender
	 */
	public void onReceivedPiggybackedMessage(Message msg, TransInfo sender) {
		if (msg instanceof CurrentPositionMsg) {
			if (sender.getNetId() != metaData.getNetID()) {
				lastKnownPositions.put(sender.getNetId(),
						((CurrentPositionMsg) msg).getSenderMetaData());
			}
		} else {
			throw new AssertionError(
					"Only expecting current position messages...");
		}
	}

	@Override
	public byte getPiggybackServiceID() {
		return PIGGYBACK_SERVICE_ID;
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		if (msg instanceof TopologyUpdateMsg) {
			handleTopologyUpdateMsg((TopologyUpdateMsg) msg, sender);
		} else if (msg instanceof CurrentPositionMsg) {
			handleCurrentPositionMsg((CurrentPositionMsg) msg, sender);
		}
	}

	protected void handleTopologyUpdateMsg(TopologyUpdateMsg msg,
			TransInfo sender) {
		checkAllNeighborsAgainstTimeout();

		HashMap<NetID, NodeMetaData> newNodes = msg.getTopology();

		//First watch if there are updates for the already saved nodes
		for (NetID netIDOldNode : lastKnownPositions.keySet()) {
			for (NetID netIDNewNode : newNodes.keySet()) {
				if (netIDOldNode.equals(netIDNewNode)) {
					if (newNodes.get(netIDNewNode).getSeqNo() > lastKnownPositions
							.get(netIDOldNode).getSeqNo()) {
						lastKnownPositions.put(netIDNewNode,
								newNodes.get(netIDNewNode));
					}
				}
			}
		}
		
		//Now add nodes, which are yet not saved in the actual list
		for (NetID netIDNewNode : newNodes.keySet()) {
			boolean foundInOld = false;
			for (NetID netIDOldNode : lastKnownPositions.keySet()) {
				if (netIDNewNode.equals(netIDOldNode)) {
					foundInOld = true;
				}
			}
			//If no occurrence of this node is found and the new node is not this node itself, add it!
			if (!foundInOld && !netIDNewNode.equals(metaData.getNetID()))
				lastKnownPositions
						.put(netIDNewNode, newNodes.get(netIDNewNode));
		}
	}

	/**
	 * This method is only called if a client broadcasts his data
	 * 
	 * @param msg
	 * @param sender
	 */
	protected void handleCurrentPositionMsg(CurrentPositionMsg msg,
			TransInfo sender) {
		if (sender.getNetId() != metaData.getNetID()) {
			lastKnownPositions.put(sender.getNetId(),
					((CurrentPositionMsg) msg).getSenderMetaData());
		}
	}

	@Override
	public boolean allowIncomingConnection(TransInfo from, int onPort) {
		checkAllNeighborsAgainstTimeout();

		if (onPort == virtualTopologyPort
				|| (useRandomPort && onPort <= destinationPort && onPort >= startPort)
				|| whitelistedPorts.contains(onPort)) {
			// allow CurrentPosition Messages (via Broadcast) which would be
			// declined, because the sender is too far away
			return true;
		}

		if (lastKnownPositions.containsKey(from.getNetId())) {
			NodeMetaData lastKnownMetaDataSender = lastKnownPositions.get(from
					.getNetId());
			double distance = metaData.getLocation().distanceTo(
					lastKnownMetaDataSender.getLocation());
			return distance < metaData.getCommunicationRange()
					&& distance < lastKnownMetaDataSender
							.getCommunicationRange();
		}

		// If its a normal piggybacked message and the sender is not in
		// lastKnownPositions-list, allow this transaction
		return true;
	}

	@Override
	public boolean allowOutgoingConnection(NetID to, int toPort, int onPort) {
		checkAllNeighborsAgainstTimeout();

		if (onPort == virtualTopologyPort
				|| (useRandomPort && onPort <= destinationPort && onPort >= startPort)
				|| whitelistedPorts.contains(onPort)) {
			// allow CurrentPosition Messages (via Broadcast) which would be
			// declined, because the receiver is too far away
			return true;
		}
		if (lastKnownPositions.containsKey(to)) {
			NodeMetaData lastKnownMetaDataReceiver = lastKnownPositions.get(to);
			double distance = metaData.getLocation().distanceTo(
					lastKnownMetaDataReceiver.getLocation());
			return distance < metaData.getCommunicationRange()
					&& distance < lastKnownMetaDataReceiver
							.getCommunicationRange();
		}
		// to allow broadcasts and unknown protocols to pass
		return true;
	}

	/**
	 * Periodically broadcasts the current topology
	 * 
	 * @author Martin Hellwig
	 * @version 22.04.2015
	 * 
	 */
	private class PeriodicTopologyBroadcast extends
			PeriodicOperation<VirtualTopologyWithLocationNode, Object> {

		/**
		 * @param component
		 * @param callback
		 * @param interval
		 */
		protected PeriodicTopologyBroadcast(VirtualTopologyWithLocationNode component,
				long interval) {
			super(component, Operations.getEmptyCallback(), interval);
		}

		@Override
		protected void executeOnce() {
			getUdp().send(new TopologyUpdateMsg(getCurrentTopologyWithLocation()),
					getUdp().getNetInterface().getBroadcastAddress(),
					virtualTopologyPort);
			this.operationFinished(true);
		}

		@Override
		public Object getResult() {
			return null;
		}

	}

	@Override
	public void updateCurrentLocation(Location location) {
		if (this.metaData != null) {
			this.metaData.setLocation(location);
			this.metaData.setLastInformationTimestamp(Time.getCurrentTime());

			// Monitor.log(NodeMetaData.class, Level.INFO,
			// "Updated current position to %s.", location.toString());
			getUdp().send(new CurrentPositionMsg(metaData),
					getUdp().getNetInterface().getBroadcastAddress(),
					virtualTopologyPort);
		}
	}

	@Override
	public NodeMetaData getNodeMetaData() {
		metaData.setLastInformationTimestamp(Time.getCurrentTime());
		return metaData;
	}

	@Override
	public void updateAndAnnounceTopology(HashMap<NetID, NodeMetaData> nodes) {
		if (topoBroadcastOp.isStopped()) {
			/*
			 * Initialize Operation ONCE, it takes care of always picking the
			 * most recent topology to broadcast.
			 */
			HashMap<NetID, NodeMetaData> newNodeList = new HashMap<NetID, NodeMetaData>();
			for (NetID newNodeNetID : nodes.keySet()) {
				if (!newNodeNetID.equals(metaData.getNetID()))
					newNodeList.put(newNodeNetID, nodes.get(newNodeNetID));
			}

			this.lastKnownPositions = newNodeList;
			// Start 1 second delayed due to an ongoing setup process
			this.topoBroadcastOp.startWithDelay(1 * Time.SECOND);
		}
	}

	/**
	 * This method adds itself to the topology when sending
	 * 
	 * @return
	 */
	@Override
	public HashMap<NetID, NodeMetaData> getCurrentTopologyWithLocation() {
		checkAllNeighborsAgainstTimeout();

		HashMap<NetID, NodeMetaData> nodesToSend = new HashMap<NetID, NodeMetaData>();
		for (NetID node : lastKnownPositions.keySet()) {
			nodesToSend.put(node, lastKnownPositions.get(node));
		}
		metaData.incSeqNo();
		metaData.setLastInformationTimestamp(Time.getCurrentTime());
		nodesToSend.put(metaData.getNetID(), metaData);
		return nodesToSend;
	}

	/**
	 * Checks for all stored nodes if their values are not too old. If they are
	 * too old (specified in timeout) they get removed from actual node-list
	 */
	private void checkAllNeighborsAgainstTimeout() {
		List<NetID> toRemove = new ArrayList<NetID>();

		for (NetID nodeNetID : lastKnownPositions.keySet()) {
			if ((Time.getCurrentTime() - NEIGHBOR_ACTIVITY_TIMEOUT) > lastKnownPositions
					.get(nodeNetID).getLastInformationTimestamp()) {
				toRemove.add(nodeNetID);
			}
		}

		for (NetID netID : toRemove) {
			lastKnownPositions.remove(netID);
		}
	}
}
