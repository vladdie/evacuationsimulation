package de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.data;

import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

/**
 * This class defines meta information of each node
 * Additionally each node updates its seqNo each time someone asks for the current meta-data or if 
 * the node broadcasts its actual virtual topology
 * 
 * @author Martin Hellwig
 * @version 1.0 15.04.2015
 * 
 */
public class NodeMetaData {

	private Location location;
	private double communicationRange;
	private long seqNo;
	private NetID netID;
	private long lastInformationTimestamp;
	
	@SuppressWarnings("unused")
	private NodeMetaData() {
		// for Kryo
	}

	public NodeMetaData(NetID netID, Location location,
			double communicationRange, long lastInformationTimestamp) {
		super();
		this.netID = netID;
		this.location = location;
		this.communicationRange = communicationRange;
		this.seqNo = 0;
		this.lastInformationTimestamp = lastInformationTimestamp;
	}
	
	/**
	 * This doesn't work; Deserialization of Location and NetID-objects are not possible
	 * @param serialized
	 */
	public NodeMetaData(String serialized) {
		super();
		String[] values = serialized.split(";");
		//this.netID = new Ne;
		//this.location = ;
		this.communicationRange = Double.valueOf(values[1]);
		this.seqNo = Long.valueOf(values[2]);
		this.lastInformationTimestamp = Long.valueOf(values[3]);
	}
	
	@Override
	public String toString() {
		return netID.toString() + ";" + communicationRange + ";" + seqNo + ";"
				+ lastInformationTimestamp + ";" + location.toString();
	}
	
	/**
	 * Returns the size of this object in byte
	 * @return
	 */
	public int getTransmissionSize() {
		return location.getTransmissionSize() + 8 + 8 + 8
				+ netID.getTransmissionSize();
	}
	
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	public double getCommunicationRange() {
		return communicationRange;
	}
	public void setCommunicationRange(double communicationRange) {
		this.communicationRange = communicationRange;
	}

	public long getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(long seqNo) {
		this.seqNo = seqNo;
	}

	public void incSeqNo() {
		this.seqNo++;
	}

	public NetID getNetID() {
		return netID;
	}

	public long getLastInformationTimestamp() {
		return lastInformationTimestamp;
	}

	public void setLastInformationTimestamp(long lastInformationTimestamp) {
		this.lastInformationTimestamp = lastInformationTimestamp;
	}
}
