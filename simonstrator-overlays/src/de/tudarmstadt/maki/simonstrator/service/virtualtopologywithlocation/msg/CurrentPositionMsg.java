package de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.data.NodeMetaData;

/**
 * This message is piggybacked to each sent message to add the current position
 * of our node for later filtering on the receiver side. 
 * 
 * @author Martin Hellwig
 * @version 20.04.2015
 * 
 */
public class CurrentPositionMsg implements Message {

	private static final long serialVersionUID = 1L;

	private NodeMetaData metaData;

	@SuppressWarnings("unused")
	private CurrentPositionMsg() {
		// For Kryo
	}

	public CurrentPositionMsg(NodeMetaData metaData) {
		this.metaData = metaData;
	}

	/**
	 * @return the meta data
	 */
	public NodeMetaData getSenderMetaData() {
		return metaData;
	}

	@Override
	public long getSize() {
		return metaData.getTransmissionSize();
	}

	@Override
	public Message getPayload() {
		return null;
	}
}
