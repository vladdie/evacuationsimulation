package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

public class WeightExchangeMessage extends AbstractDGSMessage implements
		MessageWithHopCount, MessageWithTTL, MessageWithWeight {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4312057826029284419L;

	private double weight;
	private int ttl;
	private int hops;
	private INodeID weightHolder;

	public WeightExchangeMessage(int round, UniqueID msgID, double weight,
			int ttl, INodeID weightHolder) {
		super(round, msgID);
		this.weight = weight;
		this.ttl = ttl;
		this.weightHolder = weightHolder;

		hops = 1; // initialize with 1!
	}

	public WeightExchangeMessage(WeightExchangeMessage msg) {
		super(msg);
		this.weight = msg.getWeight();
		this.ttl = msg.getTTL();
		this.weightHolder = msg.getWeightHolder();
		this.hops = msg.getHopcount();
	}

	@Override
	public Message getPayload() {
		return this;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	@Override
	public void setTTL(int ttl) {
		this.ttl = ttl;
	}

	@Override
	public int getTTL() {
		return ttl;
	}

	@Override
	public int getHopcount() {
		return hops;
	}

	@Override
	public void increaseHopcount() {
		hops++;
	}

	public INodeID getWeightHolder() {
		return weightHolder;
	}

	@Override
	protected long size() {
		// double weight (8) + int ttl(4) + int hops(4) + INodeID
		return 16 + weightHolder.getTransmissionSize();
	}

}
