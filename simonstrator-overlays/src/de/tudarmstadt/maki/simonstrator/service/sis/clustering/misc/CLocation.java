package de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc;

import org.apache.commons.math3.ml.clustering.Clusterable;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

@SuppressWarnings("serial")
public class CLocation implements Location, Clusterable {

	private double xPos;
	private double yPos;
	private INodeID nodeID;

	public CLocation(double x, double y) {
		xPos = x;
		yPos = y;
	}

	public CLocation(Location l) {
		xPos = l.getLongitude();
		yPos = l.getLatitude();
	}

	public CLocation(INodeID node, Location l) {
		nodeID = node;
		xPos = l.getLongitude();
		yPos = l.getLatitude();
	}

	public static Location getBaseStation() {
		return new CLocation(TempClusteringConfig.xBS, TempClusteringConfig.yBS);
	}

	public INodeID getNodeID() {
		return nodeID;
	}

	@Override
	public int getTransmissionSize() {
		return 0;
	}

	@Override
	public void set(Location l) {
		xPos = l.getLongitude();
		yPos = l.getLatitude();
	}

	@Override
	public double getLatitude() {
		return this.yPos;
	}

	@Override
	public double getLongitude() {
		return this.xPos;
	}

	@Override
	public long getAgeOfLocation() {
		return 0;
	}

	@Override
	public CLocation clone() {
		return new CLocation(xPos, yPos);
	}

	@Override
	public double distanceTo(Location dest) {
		double distance = Math.sqrt(Math.pow(dest.getLongitude() - xPos, 2) + Math.pow(dest.getLatitude() - yPos, 2));
		return distance;
	}

	@Override
	public float bearingTo(Location dest) {
		return 0;
	}

	@Override
	public double[] getPoint() {
		double[] point = { xPos, yPos };
		return point;
	}

}