package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

public interface MessageWithClusterID {

	public INodeID getClusterID();

}
