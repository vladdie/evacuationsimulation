package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponent;

//TODO use different Message Interface (clusterspecific, generic)
public interface LocalDissemination extends DGSSubComponent {

	/**
	 * Should be called when this node received an Message of a type to be
	 * disseminated. The LocalDissemination then decides if, when and how to
	 * disseminate it further, after reducing TTL and checking for duplicates.
	 * <p>
	 * This is to be called by the scheme, as the scheme decides how e.g.,
	 * weightMsgs are handled.
	 * 
	 * @param msg
	 */
	// FIXME
	public void onMessageReceived(Message msg);

	/**
	 * Disseminates the given message according to the dissemination type.
	 * 
	 * @param msg
	 */
	public void disseminate(Message msg);

	/**
	 * Disseminates the given message according to this type, and talking into
	 * account the given weight <b>normalized between 0 and 1</b>, where 1 means
	 * always to send.
	 * 
	 * @param msg
	 * @param weight
	 */
	public void disseminate(Message msg, double weight);

}
