# Gateway Selection
This package includes an approach to combine the central `clustering` `GatewaySelectionStrategy` and the `decenetralGatewaySelection` and expose unified client and server stubs.
While the `GatewaySelectionStrategy` as well as the `IDecentralGatewaySelectionClient` can be utilized as stand alone, (or with their respective clients and servers out of this package), it is encouraged to use the GatewaySelectionController stubs for servers and clients. These automatically create the other components when needed, and do the component transitions between them.

## Components
Components are divided into `Server` and `Client` stubs. Clients are supposed to run on mobile or managed nodes, while the server components are to be instantiated at a central entity.
Communication only takes part between the same prefaced server and Client components (meaning controller, centralized and decentralized). The only violation of that rule, is that controller entities may use access to their controlled instance's api when bootstrapping nodes into the network.
* Clients inform their node of its status (if they know it)
* Servers are used to trigger a GatewaySelection (with given input, for the centralized version), and alter the components in used. (I.e., `StrategyImplementations`, or `DGSSubComponents`)

### CentralizedGatewaySelection
The `CentralizedGatewaySelection` makes up the proxy stub for the `GatewaySelectionStrategy`. The `Server` Component pretty much only forwards inputs to the currently running Strategy. It is also responsible for transitions between these strategies. It may also message and inform the `Client` Components of their node's status, if enabled.

### DecentralizedGatewaySelection
The `DecentralizedGatewaySelectionServer` may trigger a decentralized selection (if enabled), but its main function is to inform the Client stubs of the to be used `DGSSubComponents`.
If the DGS Clients are set to inform the server of its status, the Server will return the last known GatewaySelection, upon calling the `getGateways()` methods.
For more information about the `DecentralGatewaySelection`, please refer to the ReadMe file in that package. 

### Controllers
The Controllers maintain a reference to the proxy interface for the centralized or decentralized components, and perform the transitions between those. (More correctly; they trigger the `TransitionEngine` to do just that.)
Upon Creation, the controller also instantiates a default instance of the component.

The Server does not trigger transitions between centralized and decentralized or the utilized components by itself, instead it requires some form of `GatewaySelectionMechanismLogic` to tell it when and what to exchange.
Transition or component exchanges are expected as String-list by the `changeConfiguration` method. Transitions between Centralized or decentralized mechanisms, should use the appropriate String from the Controller's interface, while Transitions between `StrategyImplementations` should use the enum type identifier out of the same named file. The same is true for substitutions of `DGSSubComponents`.
In order to do this transparently of an application, the Server also exposes the same methods as the centralized server would, and relay invokes to the current proxy.

The Client is not much more than a Proxy for the used Selection -mechanism. But it can request Bootstrapping information from the Server, and relay that information to the SelectionClients.

## Config options
There are currently no setters for these. instead their fields should be set in the respective classes, before starting a simulation. Part of the reason is that these must already be configured, when an instance if created by the TransitionEngine on behalf of the Controller.
*Parameter, type, default, description *

### Parameters in `CentralizedGatewaySelectionServer`:
* informNodesLevel: Enum, ALL. Controls which Nodes' Clients should be informed about their `NodeRole` by this Server. Possible levels are: <br>
	 * <b>ALL</b> informs all nodes of their changed gateway affiliation and NodeRole<br>
	 * <b>GATEWAYS</b> informs only current and previous Gateways that their NodeRole changed. No information about the new gateway is transmitted to leafs.<br>
	 * <b>NONE</b> No information is send to the clients. Use this if the application takes care of informing the clients itself.

* behaviour: GatewayLogicBehavior, SUGGESTION. Used to determine how a `GatewaySelectionStrategy` should use the given parameter *maxNumberOfGateways*.

#### Parameters in `DecentralizedGatewaySelectionServer`:
* selectGatewaysAfterStart: boolean, true. If true, each transition to DGS starts a gateway selection
* selectGatewaysAfterComponentChange: boolean, true. If true, each component exchange starts a gateway selection
* startSelectionUponGetGateways: boolean, false. If true, each call to `getGateways()` results in ordering a new GatewaySelection.
* afterTransitionWait: long, 500ms. How long the DGS Controller waits before sending out a config message, after a transition from centralized version. Reason is that all Clients must have enough time to do the transition to the decentralized Client, and associated initializations.