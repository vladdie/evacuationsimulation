package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.SEC;

public class SEC_Random extends SEC {

	public SEC_Random() {
		super("SEC_Random");
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		double k = maxNumberOfGateways;
		double optimalProbability = k / forNodes.size();
		for (INodeID v : forNodes) {
			setProbability(v, optimalProbability);
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		return Collections.emptyMap(); // No SiS interaction is required
	}

}
