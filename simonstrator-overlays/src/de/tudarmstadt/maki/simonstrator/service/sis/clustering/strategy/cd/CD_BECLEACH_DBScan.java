package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.DBScanClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.TempClusteringConfig;

public class CD_BECLEACH_DBScan extends CD_BECLEACH {

	public CD_BECLEACH_DBScan() {
		super(false);
	}

	@Override
	protected List<List<INodeID>> calculateCluster(List<INodeID> nodes) {
		Map<INodeID, Location> curLocations = positionsHolder.getLast();

		// clusterer = new GridDensityClusterer();
		clusterer = new DBScanClusterer(TempClusteringConfig.minPTS);
		// clusterer = new KppMeansClusterer(maxNumberOfGateways);

		List<List<INodeID>> cluster = new ArrayList<List<INodeID>>();
		cluster.addAll(clusterer.getClusters(curLocations));

		return cluster;
	}

}
