package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;

public class CentralGatewaySelectionMessage implements Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3689842645538885017L;

	private INodeID gateway; 
	private NodeRole nodeRole;
	
	public CentralGatewaySelectionMessage(NodeRole nodeRole, INodeID gateway) {
		this.nodeRole = nodeRole;
		this.gateway = gateway;
	}
	
	/**
	 * @return the new Gateway for this node. May return null
	 */
	public INodeID getGateway(){
		return gateway;
	}
	
	public NodeRole getNodeRole() {
		return nodeRole;
	}

	@Override
	public long getSize() {
		return gateway.getTransmissionSize() + 1;
	}

	@Override
	public Message getPayload() {
		return null;
	}

}
