package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponent;

/**
 * Implementing classes are responsible for assigning their node to a cluster,
 * based on intercepted Messages.
 * 
 * @author Christoph Storm
 */
// TODO use different Message Interface (clusterspecific, generic)
public interface DecentralClusterer extends DGSSubComponent {
	// FIXME include callback for scheme!

	/**
	 * The INodeID of the Clusterhead to who's cluster this node currently
	 * belongs to.
	 * 
	 * @return
	 */
	public INodeID getClusterID();

	/**
	 * This method should be called by the DGS main class whenever a DGS message
	 * arrived.
	 * 
	 * @param msg
	 */
	public void onMessageArrived(Message msg);

	/**
	 * Can be called by the scheme to finalize clustering computations
	 */
	// TODO add callback
	public void finalizeClustering();

	/**
	 * To be called by the scheme if we areCH
	 * 
	 * @param clusterID
	 */
	public void setClusterID(INodeID clusterID);
}
