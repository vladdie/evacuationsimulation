package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.BasicGraph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.IEdge;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.IClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.KppMeansClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.TempClusteringConfig;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.CS;

public class CS_WCA extends CS {

	private IClusterer clusterer;

	// Parameters - Weight Factors
	private double a_1 = 0.7;
	private double a_2 = 0.2;
	private double a_3 = 0.05;
	private double a_4 = 0.05;

	public CS_WCA() {
		super("CS_WCA", true);
	}

	@Override
	protected List<List<INodeID>> calculateCluster(List<INodeID> nodes) {
		Map<INodeID, Location> curLocations = positionsHolder.getLast();

		// clusterer = new GridDensityClusterer();
		// clusterer = new DBScanClusterer(TempClusteringConfig.minPTS);
		clusterer = new KppMeansClusterer(maxNumberOfGateways);

		List<List<INodeID>> cluster = new ArrayList<List<INodeID>>();
		cluster.addAll(clusterer.getClusters(curLocations));

		return cluster;
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		// Get required information
		int l = TempClusteringConfig.optimalNumberOfLeafsPerGateway;

		Map<INodeID, Double> tempWeights = new HashMap<INodeID, Double>(forNodes.size());
		double smallestWeight = Double.MAX_VALUE;

		// Calculate weights
		for (INodeID v : forNodes) {

			double degree = neighborhoodGraph.getOutdegree(v);
			double deltaDegree = Math.abs(degree - l);

			double sumOfTheDistances = computeSumOfTheDistances(v);

			double v_avg = calcAvgSpeed(v);

			long timesAsGateway = super.timesAsGateway.get(v);

			double w_v = a_1 * deltaDegree + a_2 * sumOfTheDistances + a_3 * v_avg + a_4 * timesAsGateway;
			w_v = w_v * -1.0;
			if (w_v < smallestWeight) {
				smallestWeight = w_v;
			}
			tempWeights.put(v, w_v);
		}

		for (INodeID v : forNodes) {
			double w_v = tempWeights.get(v);
			w_v = w_v + Math.abs(smallestWeight);
			// WCA assigns the mobile station which has the smallest value
			setProbability(v, w_v);
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw informations
		typeLocation = SiSTypes.PHY_LOCATION;
		types.put(typeLocation, SiSRequest.NONE);

		// Higher Data
		positionsHolder = new LimitedQueue<>(2); // Positions
		neighborhoodGraph = new BasicGraph();// Neighborhood

		return types;
	}

	private double computeSumOfTheDistances(INodeID node) {

		double sumOfTheDistances = 0.0;
		Map<INodeID, Location> curLocations = positionsHolder.getLast();
		Location vLoc = curLocations.get(node);
		Set<IEdge> edges = neighborhoodGraph.getOutgoingEdges(node);
		for (IEdge edge : edges) {
			double distance;
			if (edge.fromId() != node) {
				distance = vLoc.distanceTo(curLocations.get(edge.fromId()));
			} else {
				distance = vLoc.distanceTo(curLocations.get(edge.toId()));
			}
			sumOfTheDistances += distance;
		}
		return sumOfTheDistances;
	}

}
