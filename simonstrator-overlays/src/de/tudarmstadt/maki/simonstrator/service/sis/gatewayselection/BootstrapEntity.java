package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;

public class BootstrapEntity {

	private static BootstrapEntity instance;

	private TransInfo serverContact;

	private LinkedList<NetInterface> connections = new LinkedList<NetInterface>();

	private BootstrapEntity() {
		instance = this;
	}

	private static BootstrapEntity getInstance() {
		if (instance == null) {
			return new BootstrapEntity();
		}
		return instance;
	}

	public static TransInfo getServerContact() {
		return getInstance().serverContact;
	}

	public static void setServerContact(TransInfo serverContact) {
		getInstance().serverContact = serverContact;
	}

	///////////////////////////////////////
	// Helper functions that mimic global Server knowledge...
	// ... maybe a *bit* too global

	public static List<NetInterface> getAllNetConnections() {
		return getInstance().connections;
	}

	public static void register(NetInterface netInterface) {
		getInstance().connections.add(netInterface);
	}

	public static void deregister(NetInterface connection) {
		getInstance().connections.remove(connection);
	}

}
