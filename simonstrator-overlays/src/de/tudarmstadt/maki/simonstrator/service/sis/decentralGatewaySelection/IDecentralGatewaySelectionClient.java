package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer.DecentralClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination.LocalDissemination;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme.DGSScheme;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.Trigger;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.Trigger.TriggerListener;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.WeightComputation;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient;

/**
 * Main Interface for the decentral gateway selection. All other components can
 * be reached via this container. Implementations should set themselves as
 * {@link TriggerListener} at the used trigger. <br>
 * To start the CH algorithm from outside, call {@link #getTrigger()} and
 * {@link Trigger #onTriggeredFromOutside()}.
 * 
 * @author Christoph Storm
 * @version 2016.06.29
 */
public interface IDecentralGatewaySelectionClient extends GatewaySelectionClient, Trigger.TriggerListener {

	public static final int DGSPort = 180;

	// FIXME maybe move this to be a standalone interface?
	public static enum DGSComponent {
		TRIGGER(Trigger.class), SCHEME(DGSScheme.class), WEIGHTCOMPUTATION(WeightComputation.class), DISSEMNINATION(
				LocalDissemination.class), CLUSTERER(DecentralClusterer.class);
		DGSComponent(Class<? extends DGSSubComponent> componentInterface) {
			this.componentInterface = componentInterface;
		}

		private Class<? extends DGSSubComponent> componentInterface;

		public Class<? extends DGSSubComponent> getComponentInterface() {
			return componentInterface;
		}
	};

	public Host getHost();

	public Trigger getTrigger();

	public DGSScheme getScheme();

	public WeightComputation getWeightComputation();

	public LocalDissemination getLocalDissemination();

	public DecentralClusterer getClusterer();

	/**
	 * To be called by the DGSScheme
	 * 
	 * @param role
	 */
	public void DGSSchemeFinished(NodeRole role);

	/**
	 * Whether the Gateway Selection is <b>currently actively computing a
	 * gateway</b>,
	 * 
	 * @return
	 */
	public boolean isActive();

	/**
	 * Returns true, if the DGS was set to {@link #start()}, and the node
	 * {@link de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface #isUp()}
	 * .
	 * 
	 * @return
	 */
	public boolean isOnline();

	// /**
	// * Starts the DGS and its components. Use {@link #shutdown()} to deactivate
	// * the dgs again, when switching to centralized CH selection.
	// */
	// public void start();

	/**
	 * Implementations also give access to the Transport layer and act as
	 * MessageListener
	 * 
	 * @return
	 */
	public UDP getTransport();

	/**
	 * Can be called to exchange subcomponents. <br>
	 * <b>Attention:</b> Exchanging Components <b>WILL</b> disrupt the
	 * GatewaySelection Process. When using this, get the if Selection currently
	 * {@link #isActive()} and retrigger it once via
	 * {@link Trigger #onTriggeredFromOutside()}, accessible from
	 * {@link #getTrigger()} after Components have been exchanged.
	 * 
	 * @param comp
	 * @param newInstance
	 */
	public <T extends DGSSubComponent> void ComponentTransition(DGSComponent comp, Class<T> implementingClass);

	/**
	 * Setter for the Components. To be used by the Factory. Expects ready
	 * configured instances.
	 * 
	 * @param comp
	 * @param defaultInstance
	 */
	public <T extends DGSSubComponent> void setDefaultComponent(DGSComponent comp, T defaultInstance);

	/**
	 * The current or last active GS round
	 * 
	 * @return
	 */
	public int getRound();

	public SiSComponent getSis();

}
