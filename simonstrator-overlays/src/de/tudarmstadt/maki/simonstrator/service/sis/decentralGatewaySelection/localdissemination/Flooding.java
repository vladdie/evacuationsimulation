package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;

public class Flooding extends AbstractDissemination {

	private NetID broadcast;

	@TransferState({ "Parent" })
	public Flooding(IDecentralGatewaySelectionClient parent) {
		super(parent);
	}

	@Override
	public void startMechanism(Callback cb) {
		super.startMechanism();
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		super.stopMechanism();
		broadcast = null;

		cb.finished(true);
	}

	@Override
	public void disseminate(Message msg) {
		sendMessage(msg, broadcast);
	}

	@Override
	public void disseminate(Message msg, double weight) {
		// pure flooding doesn't use no weights, no
		disseminate(msg);
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.FLOODING;
	}

	@Override
	void init() {
		NetInterface wifi = getParent().getHost().getNetworkComponent().getByName(NetInterfaceName.WIFI);
		broadcast = wifi.getBroadcastAddress();
	}

}
