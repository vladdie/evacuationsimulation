package de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc;

/**
 * Just a temporary class to get settings about the scenario which is currently
 * used.
 * 
 * Should only be used as long as the settings are not available from the real
 * configuration file.
 * 
 * @author Michael Walter
 *
 *
 * @deprecated (BR): this should become part of the initial cluster strategy
 *             configuration (as a parameter) - would allow us to execute self
 *             transitions to alter the parameter accordingly. Some ideas: base
 *             stations could be for example the current cloudlet position (for
 *             brokers), sent to a client on handover... Transmission range
 *             could be altered to reflect the density of nodes.
 */
@Deprecated
public class TempClusteringConfig {

	// General Settings
	public static double xMap = 1500;
	public static double yMap = 1500;

	// Basestation Settings
	public static double xBS = xMap / 2;
	public static double yBS = yMap / 2;
	
	// Mobile Station Settings
	public static double maxTransmissionRange = 88;
	public static int optimalNumberOfLeafsPerGateway = 8;
	public static int optimalClusterSize = optimalNumberOfLeafsPerGateway + 1;

	// Other Settings for DBScan
	public static double cushionFactor = 0.5;
	public static double maxRange = cushionFactor * maxTransmissionRange;
	public static int numberOfOldPositions = 2;
	/*
	 * FIXME (BR): This affects performance in Bypass, as a cluster needs to
	 * contain at least 6 nodes in its default configuration. For now, we also
	 * consider clusters with only 2 nodes.
	 */
	public static int minPTS = 2;
}
