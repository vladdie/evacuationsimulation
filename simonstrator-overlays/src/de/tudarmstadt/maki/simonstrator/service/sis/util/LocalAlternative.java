package de.tudarmstadt.maki.simonstrator.service.sis.util;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInfoProperties;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;

/**
 * If for a single {@link SiSType} there exist multiple {@link LocalSource}
 * objects, these are bundled here.
 * 
 * @author Bjoern Richerzhagen
 *
 * @param <T>
 */
public class LocalAlternative<T> {

	public final List<LocalSource<T>> sources = new LinkedList<>();

	/**
	 * True, if based on the properties, this source is suitable for the given
	 * other properties description (usually, a request).
	 * 
	 * @param other
	 * @return
	 */
	public boolean isSuitableSource(SiSInfoProperties source, SiSRequest request) {
		return request.satisfiableBy(source);
	}

	public T getBestValue(INodeID nodeId, SiSRequest request)
			throws InformationNotAvailableException {
		assert !sources.isEmpty();
		for (LocalSource<T> src : sources) {
			if (src.toRemove) {
				continue;
			}
			if (request != null) {
				if (src.dataCallback.getInfoProperties() == null) {
					continue;
				}
				// Match request with information source
				if (!isSuitableSource(src.dataCallback.getInfoProperties(),
						request)) {
					continue;
				}
			}
			try {
				return src.dataCallback.getValue(nodeId, src);
			} catch (InformationNotAvailableException e) {
				continue;
			}
		}
		throw new InformationNotAvailableException();
	}

	public Set<INodeID> getObservedNodes(SiSRequest request) {
		Set<INodeID> nodes = new LinkedHashSet<>();
		for (LocalSource<T> src : sources) {
			if (src.toRemove) {
				continue;
			}
			nodes.addAll(src.dataCallback.getObservedNodes());
		}
		return nodes;
	}

	protected void removeInformationSource(LocalSource<T> src) {
		sources.remove(src);
	}

	public boolean hasNoSources() {
		return sources.isEmpty();
	}
}
