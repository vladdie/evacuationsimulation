package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.MessageWithClusterID;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.Trigger.TriggerListener;

public class FirstHeardClusterer implements DecentralClusterer, TriggerListener {

	@TransferState({ "ClusterID" })
	private INodeID clusterID;
	
	boolean isClustering = false;

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	@TransferState({ "Parent"})
	public FirstHeardClusterer(IDecentralGatewaySelectionClient parent) {
		if (parent != null) {
			this.parent = parent;
			init();
		}
	}

	@Override
	public INodeID getClusterID() {
		return clusterID;
	}

	@Override
	public void onMessageArrived(Message msg) {
		if (isClustering && msg instanceof MessageWithClusterID) {
			INodeID id = ((MessageWithClusterID) msg).getClusterID();
			if (id != null) {
				clusterID = id;
				isClustering = !isClustering; // no longer Clustering
			}
		}
	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void startMechanism(Callback cb) {
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		parent.getTrigger().removeTriggerListener(this);
		parent = null;

		clusterID = null;

		cb.finished(true);
	}
	
	@Override
	public void setClusterID(INodeID clusterID){
		this.clusterID = clusterID;
	}

	@Override
	public void finalizeClustering() {
		// Nothing to do here
	}

	@Override
	public void onTriggered() {
		this.isClustering = true;
		clusterID = null;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
		init();
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.FIRSTHEARDCLUSTERER;
	}

	private void init() {
		parent.getTrigger().addTriggerListener(this);
	}

}
