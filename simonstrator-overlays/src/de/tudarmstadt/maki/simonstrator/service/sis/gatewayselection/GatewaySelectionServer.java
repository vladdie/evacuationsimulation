package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection;

import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.GatewaySelectionStrategy;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.StrategyImplementations;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;

/**
 * Select a number of gateways out of a list of nodes based on additional information available via the
 * {@link SiSComponent}.
 * 
 * @author Bjoern Richerzhagen including changes by Nils Richerzhagen
 *
 */
public interface GatewaySelectionServer extends TransitionEnabled {

	/**
	 * Returns the current selection of Gateways and associated nodes. Does not
	 * trigger a gateway selection.
	 * 
	 * @return
	 */
	public Map<INodeID, List<INodeID>> getCurrentGateways();

	/**
	 * Behavior is different for Centralized and Decentralized Setting. <br>
	 * In Centralized mode this is guaranteed to start a new GatewaySelection,
	 * using the passed parameters. It returns the list of nodes associated to
	 * each Gateway, and may inform nodes of their changed {@link NodeRole} <br>
	 * In Decentralized mode, this may or may not start a new GatewaySelection,
	 * and returns the last centrally known Gateways. <br>
	 * 
	 * Behavior is up to the individual implementations and settings.
	 * 
	 * @param outOf
	 *            select the gateways out of this list of INodeIDs. The list
	 *            must not be changed within this method!
	 * @param dataGraph
	 *            an <strong>optional</strong> graph object carrying all
	 *            annotated nodes. Can be used to save state as a node
	 *            annotation.
	 * @param maxNumberOfGateways
	 *            how many gateways should be picked at max
	 * @return a map with gateway nodes as keys and associated client nodes as
	 *         values. If the strategy does not associate clients to gateways,
	 *         the values of the returned map are empty lists.
	 */
	public Map<INodeID, List<INodeID>> getGateways(List<INodeID> outOf, Graph dataGraph, int maxNumberOfGateways);

	/**
	 * Sets a Component to be used by this GatewaySelection. This can be a
	 * {@link GatewaySelectionStrategy} as found in
	 * {@link StrategyImplementations} or one of the
	 * {@link IDecentralGatewaySelectionClient #DGSComponent} as found in
	 * {@link DGSSubComponents}.
	 * 
	 * @param simpleName
	 *            the simple classname of the component.
	 */
	public void setComponent(String simpleName);

	/**
	 * Generate a config message for the given node. This is called for
	 * Bootstrapping puroses. <i>May return null</i>
	 * 
	 * @param node
	 * @return
	 */
	public Message getConfigMessageFor(INodeID node);

	public Host getHost();
}
