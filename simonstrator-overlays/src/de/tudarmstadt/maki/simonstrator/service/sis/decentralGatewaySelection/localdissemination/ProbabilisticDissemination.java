package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination;

import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.MessageWithHopCount;

/**
 * Dissemination class that works with a given probability. It can be set to
 * always send Messages on the first two hops.
 * 
 * @author Christoph Storm
 * @version 2016.06.23
 */
public class ProbabilisticDissemination extends AbstractDissemination {
	// TODO maybe a layered probability decreasing, until a given radius is
	// reached?

	private static double probability = 0.75d;
	private static boolean alwaysSendOnFirstHop = true;

	private Random rand;
	private NetID broadcast;

	@TransferState({ "Parent" })
	public ProbabilisticDissemination(IDecentralGatewaySelectionClient parent) {
		super(parent);
		rand = Randoms.getRandom(getParent());
	}

	@Override
	public void startMechanism(Callback cb) {
		super.startMechanism();
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		super.stopMechanism();
		broadcast = null;

		cb.finished(true);
	}


	@Override
	public void disseminate(Message msg) {
		if (alwaysSendOnFirstHop && msg instanceof MessageWithHopCount) {
			// Set to 2 because we may just increased it directly before in the
			// abstract class
			if (((MessageWithHopCount) msg).getHopcount() < 2) {
				sendMessage(msg, broadcast);
				return;
			}
		}
		disseminate(msg, probability);
	}

	@Override
	public void disseminate(Message msg, double weight) {
		double val = rand.nextDouble();
		if (val < weight) {
			sendMessage(msg, broadcast);
		}
	}

	public static void setProbability(double prob) {
		probability = prob;
	}

	public static void setAlwaysSendOnFirstHop(boolean send) {
		alwaysSendOnFirstHop = send;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.PROBABILISTICDISSEMINATION;
	}

	@Override
	void init() {
		NetInterface wifi = getParent().getHost().getNetworkComponent().getByName(NetInterfaceName.WIFI);
		broadcast = wifi.getBroadcastAddress();
	}

}
