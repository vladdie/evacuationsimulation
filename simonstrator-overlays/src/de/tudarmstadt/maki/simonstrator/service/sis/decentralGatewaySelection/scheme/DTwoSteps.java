package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSComponentFactory;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.DeclareCHMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.WeightExchangeMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;

/**
 * This is a deterministic scheme, that compares the given weight to ones it
 * receives from other nodes (First Phase). After a timeout, the node with the
 * highest heard weight is choosen as CH (Second Phase).
 * 
 * @author Christoph Storm
 * @version 2016.06.27
 */
public class DTwoSteps implements DGSScheme {

	private static int radius = -1;
	private static long phase1Timeout = 30 * Time.SECOND;
	private static long phase2Timeout = 20 * Time.SECOND;

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	// @TransferState({ "IsCH" })
	private boolean isCH = false;

	private double bestWeight;
	private int bestHops;

	@TransferState({ "Parent" })
	public DTwoSteps(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public void startMechanism(Callback cb) {
		// nothing to do here?!
		cb.finished(true);

	}

	@Override
	public void stopMechanism(Callback cb) {
		parent = null;
		isCH = false;
		bestWeight = 0d;
		bestHops = 0;
		cb.finished(true);
	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public void startScheme(double weight) {
		isCH = false;
		bestWeight = weight;
		bestHops = 0;
		// Could be started by outside message, in which case we should first
		// look at that message!
		Event.scheduleImmediately(new EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				firstPhase((double) content);
			}
		}, weight, 01);
	}

	private void firstPhase(double weight) {
		if (weight >= bestWeight && weight > 0d) {
			isCH = true;
			WeightExchangeMessage wem = new WeightExchangeMessage(
					parent.getRound(), DGSComponentFactory.getNewMessageID(),
					weight, radius, parent.getHost().getId());

			// we are originator, so always disseminate
			parent.getLocalDissemination().disseminate(wem, 1d);
		}
		Event.scheduleWithDelay(phase1Timeout, new EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				secondPhase();

			}
		}, null, 24);
	}

	private void secondPhase() {
		if (parent != null) {
			if (isCH && bestWeight > 0d) {
				// we are elected CH!
				onBecameClusterHead();
				parent.DGSSchemeFinished(NodeRole.CLUSTERHEAD);
			}
			// else wait for ch message...
			else {
				Event.scheduleWithDelay(phase2Timeout, new EventHandler() {
					@Override
					public void eventOccurred(Object content, int type) {
						if (parent != null && parent.isActive()) {
							// This really should not happen.
							parent.DGSSchemeFinished(NodeRole.NONE);
						}
					}
				}, null, 24);
			}
		}
	}

	@Override
	public void onMessageArrived(Message msg) {
		// CH message
		if (msg instanceof DeclareCHMessage && !isCH) {
			// Node is not CH, so take the first we hear?
			// not isCH means that we knew a node with the better weight in
			// range
			parent.getLocalDissemination().onMessageReceived(msg);
			if (parent.getClusterer() != null) {
				parent.getClusterer().finalizeClustering();
			}
			parent.DGSSchemeFinished(NodeRole.CLUSTERMEMBER);
		}
		// Weight message
		else if (msg instanceof WeightExchangeMessage) {
			WeightExchangeMessage wm = (WeightExchangeMessage) msg;
			boolean fwd = false;
			// better than what we got?
			if (wm.getWeight() > bestWeight) {
				bestWeight = wm.getWeight();
				fwd = true;
			} else if (wm.getWeight() == bestWeight
					&& wm.getHopcount() < bestHops) {
				fwd = true;
			}
			if (fwd) {
				isCH = false;
				bestHops = wm.getHopcount();
				parent.getLocalDissemination().disseminate(wm);
			}
		}
	}

	@Override
	public void onBecameClusterHead() {
		isCH = false;
		bestWeight = 0d;
		bestHops = 0;
		DeclareCHMessage msg = new DeclareCHMessage(parent.getRound(),
				DGSComponentFactory.getNewMessageID(), parent.getHost().getId(),
				radius);
		// Ensure that this message is disseminated
		parent.getLocalDissemination().disseminate(msg, 1d);

		if (parent.getClusterer() != null) {
			parent.getClusterer().setClusterID(parent.getHost().getId());
		}

	}

	/**
	 * Sets the TTL for CH-messages. Values smaller one should be treated as
	 * infinity
	 * 
	 * @param hops
	 */
	public static void setRadius(int hops) {
		if (hops > 0)
			radius = hops;
		else
			radius = -1;
	}

	public static void setPhase1TimeoutOut(long timeout) {
		assert timeout > 0 : "Timout must be larger zero!";
		DTwoSteps.phase1Timeout = timeout;
	}

	public static void setPhase2TimeoutOut(long timeout) {
		assert timeout > 0 : "Timout must be larger zero!";
		DTwoSteps.phase2Timeout = timeout;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.DTWOSTEPS;
	}

	/*
	 * public boolean getIsCH() { return isCH; }
	 * 
	 * public void setIsCH(boolean isCH) { this.isCH = isCH; }
	 */

}
