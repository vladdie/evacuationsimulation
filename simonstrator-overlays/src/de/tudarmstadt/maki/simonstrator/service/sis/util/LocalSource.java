package de.tudarmstadt.maki.simonstrator.service.sis.util;

import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSDataCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInfoProperties;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider.SiSProviderHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;

/**
 * Container for a single local source described by its
 * {@link SiSInfoProperties}, the {@link SiSType} and a {@link SiSDataCallback}.
 * This object also acts as the {@link SiSProviderHandle}.
 * 
 * @author Bjoern Richerzhagen
 *
 * @param <T>
 */
public class LocalSource<T> implements SiSProviderHandle {

	public boolean toRemove = false;

	public final SiSType<T> type;

	public final SiSDataCallback<T> dataCallback;

	public LocalSource(SiSType<T> type, SiSDataCallback<T> dataCallback) {
		this.type = type;
		this.dataCallback = dataCallback;
	}
}
