package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.StrategyImplementations;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;

public class CSVreaderLogic implements GatewaySelectionMechanismLogic {
	// This is the remnant of the OverlordCSVreader which was a
	// copy of {@link CSVController} which is based of
	// {@linkMaxPeerCountChurnGenerator}

	private final String comentMarker = "#";
	private final String separator = ",";

	private LinkedList<Infoholder> switchList = new LinkedList<Infoholder>();

	private GatewaySelectionServerController controller;

	@XMLConfigurableConstructor({ "file" })
	public CSVreaderLogic(String file) {
		parseFile(file);
	}

	@Override
	public void init() {
		for (Infoholder change : switchList) {
			// FIXME I'm not entirely sure that this is correct...
			Event.scheduleWithDelay(change.getTime(), new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					controller.changeConfiguration(((Infoholder) content).getComponents());
				}
			}, change, 012);
		}
	}

	// Copy of the 'parseTrace' method in MaxPeerCountChurnGenerator
	private void parseFile(String file) {
		/*
		 * Expects each line of the file consist of:
		 * 
		 * TransitionTime, "centralized" or "decentralized" or none,
		 * sub-component identifier as specified in StrategyImplementations or
		 * DGSSubComponents (only those that are different from last time!)
		 */

		BufferedReader csv = null;
		// file = file.getAbsoluteFile();

		try {
			csv = new BufferedReader(new FileReader(file));

			while (csv.ready()) {
				String line = csv.readLine();
				if (line.length() == 0 || line.startsWith(comentMarker))
					continue;

				if (line.indexOf(separator) > -1) {
					String[] parts = line.split(separator);

					// Reading the time
					String sTime = parts[0].replaceAll("\\s+", "");
					long factor = 1;
					// Blatant Copy from DefaultConfigurator
					if (sTime.matches("\\d+(ms|s|m|h)")) {
						if (sTime.matches("\\d+(ms)")) {
							sTime = sTime.substring(0, sTime.length() - 2);
							factor = Time.MILLISECOND;
						} else {
							factor = 1;
							char unit = sTime.charAt(sTime.length() - 1);
							sTime = sTime.substring(0, sTime.length() - 1);
							switch (unit) {
							case 'm':
								factor = Time.MINUTE;
								break;
							case 'h':
								factor = Time.HOUR;
								break;
							case 's':
								factor = Time.SECOND;
								break;
							default:
								throw new IllegalStateException(
										"time unit " + unit + " is not allowed is not allowed in csv-file" + file);
							}
						}
					}
					long time = Long.parseLong(sTime);
					time *= factor;
					assert time >= 0 : "Cannot change GatewaySelection before Simulation starts!";

					// Reading the Components
					List<String> comps = new LinkedList<String>();
					for (int i = 1; i < parts.length; i++) {
						String identifier = parts[i].replaceAll("\\s+", "");
						comps.add(identifier);
					}

					testListedComponents(comps);

					Infoholder info = new Infoholder(time, comps);
					switchList.add(info);
				}
			}
		} catch (IOException e) {
			// e.printStackTrace();
			throw new RuntimeException("CSVreaderLogic could not read " + file);
		} finally {
			if (csv != null) {
				try {
					csv.close();
				} catch (IOException e) {
					// eh, ok?!
				}
			}
		}
	}

	/**
	 * Test here for validity, so we throw an Error immediately, and not
	 * sometime during the simulation
	 * 
	 * @param components
	 * @return
	 */
	private static void testListedComponents(List<String> components) {

		String[] types = { "unspecified", "centralized", "decentralized" };
		int centralized = 1;
		int decentralized = 2;

		LinkedList<String> comps = new LinkedList<String>(components);

		int type = 0;

		if (comps.get(0).toLowerCase().equals(GatewaySelectionServerController.centralized)) {
			type = centralized;
			comps.remove(0);
		} else if (comps.get(0).toLowerCase().equals(GatewaySelectionServerController.decentralized)) {
			type = decentralized;
			comps.remove(0);
		}

		for (String comp : comps) {

			comp = comp.toUpperCase();
			boolean validString = false;

			if (type != decentralized) {
				// unspecified or centralized
				try {
					validString = StrategyImplementations.valueOf(comp) != null;
				} catch (IllegalArgumentException e) {
					// nothing to do here...
				}
			}

			if (type != centralized) {
				// unspecified or decentralized
				try {
					validString = DGSSubComponents.valueOf(comp) != null;
				} catch (IllegalArgumentException e) {
					// nothing to do here...
				}
			}

			if (!validString) {
				String errorMsg = comp + " is not a known Component for " + types[type] + " configuration!";
				// System.err.println(errorMsg);
				throw new RuntimeException(errorMsg);
			}

		}
		// if it got here, everything is (likely) correct...
	}

	public class Infoholder {
		private long time;
		private List<String> components;

		public Infoholder(long time, List<String> components) {
			this.time = time;
			this.components = components;
		}

		public long getTime() {
			return time;
		}

		public List<String> getComponents() {
			return components;
		}
	}

	@Override
	public void setGSServerController(GatewaySelectionServerController controller) {
		this.controller = controller;
	}
}
