package de.tudarmstadt.maki.simonstrator.service.sis.util;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSDataCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider.SiSProviderHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSLocalData;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;

/**
 * A container of a number of {@link LocalSource} for a single observation
 * target (e.g., our own local node or any remote node).
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class LocalSources implements SiSLocalData {

	private List<LocalSource<?>> sources = new LinkedList<>();

	private Map<SiSType<?>, LocalAlternative<?>> sourcesByType = new LinkedHashMap<>();

	public <T> SiSProviderHandle addSource(SiSType<T> type,
			SiSDataCallback<T> dataCallback) {
		LocalSource<T> source = new LocalSource<>(type, dataCallback);
		sources.add(source);

		@SuppressWarnings("unchecked")
		LocalAlternative<T> alternatives = (LocalAlternative<T>) sourcesByType
				.get(type);
		if (alternatives == null) {
			alternatives = new LocalAlternative<T>();
			sourcesByType.put(type, alternatives);
		}
		alternatives.sources.add(source);
		return source;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void removeSource(SiSProviderHandle handle) {
		if (!sources.contains(handle)) {
			return;
		}
		LocalSource src = (LocalSource) handle;
		sources.remove(src);
		LocalAlternative<?> alt = sourcesByType.get(src.type);
		alt.removeInformationSource(src);

		// remove the alternative, if there are no more sources attached to it.
		if (alt.hasNoSources()) {
			sourcesByType.remove(src.type);
		}
	}

	public Set<SiSType<?>> getLocallyAvailableTypes() {
		return sourcesByType.keySet();
	}

	@Override
	public <T> T getValue(INodeID nodeId, SiSType<T> type, SiSRequest request)
			throws InformationNotAvailableException {
		@SuppressWarnings("unchecked")
		LocalAlternative<T> alt = (LocalAlternative<T>) sourcesByType.get(type);
		if (alt == null) {
			throw new InformationNotAvailableException();
		}
		return alt.getBestValue(nodeId, request);
	}

	public <T> Map<INodeID, T> getAllObservations(SiSType<T> type,
			SiSRequest request) {
		Map<INodeID, T> values = new LinkedHashMap<>();
		for (INodeID nodeID : getObservedNodes(type, request)) {
			try {
				T value = getValue(nodeID, type, request);
				values.put(nodeID, value);
			} catch (InformationNotAvailableException e) {
				//
			}
		}
		return values;
	}

	@Override
	public <T> Set<INodeID> getObservedNodes(SiSType<T> type, SiSRequest request) {
		@SuppressWarnings("unchecked")
		LocalAlternative<T> alt = (LocalAlternative<T>) sourcesByType.get(type);
		assert alt != null;
		return alt.getObservedNodes(request);
	}

	/*
	 * TODO add convenience methods for source management
	 */

}
