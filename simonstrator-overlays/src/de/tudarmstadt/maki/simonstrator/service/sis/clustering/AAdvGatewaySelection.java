package de.tudarmstadt.maki.simonstrator.service.sis.clustering;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.common.graph.BasicGraph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transition.MechanismState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.TempClusteringConfig;

/**
 * 
 * Nils Richerzhagen 29.05.16: Added setter for GatewayLogicBahavior. In doing
 * so one is able to set the LogicBahvior from a Configuration. See
 * CraterGatewayConfiguration for more details.
 * 
 * @author Michael Walter
 *
 */
public abstract class AAdvGatewaySelection extends AGatewaySelectionStrategy {

	// Parameter
	protected SiSType<Location> typeLocation;
	protected SiSType<Double> typeEnergylevel;
	protected SiSType<Double> typeEnergycapacity;
	protected SiSType<Double> typeMessageSize;
	protected SiSType<Double> typeLatency;

	// Component
	protected long round = -1;
	protected Map<INodeID, Long> lastTimeGateway;
	protected Map<INodeID, Long> timesAsGateway;
	protected boolean lastRoundSelectionExecuted = true;
	protected LimitedQueue<Map<INodeID, Location>> positionsHolder;
	protected Graph neighborhoodGraph;
	protected Map<INodeID, Double> normalizedEnergielevels;
	protected Map<INodeID, Double> normalizedLatencies;

	// Class
	private Map<SiSType<?>, SiSRequest> types = null;
	protected String name;

	/**
	 * Used to determine how a {@link GatewaySelectionStrategy} should use the given parameter {@link Integer} maxNumberOfGateways.<br>
	 * 
	 * CUTTING - Given maxNumberOfGateways is used as upper bound! The algorithms must not return more gateways than that bound.<br>
	 * 
	 * SUGGESTION - Given maxNumberOfGateways is used as suggestion! The algorithms <strong>can</strong> use the given number as suggestion (e.g. LEACH for the
	 * internal expectation value (Erwartungswert)). However in SUGGESTION case the algorithms can also return "the optimal number" of gateways they think is
	 * best for the current network.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	public enum GatewayLogicBehavior {
		CUTTING,

		SUGGESTION
	}

	protected int maxNumberOfGateways;

	@TransferState({ "Behaviour" })
	@MechanismState("Behaviour")
	private GatewayLogicBehavior behaviour;

	protected boolean methodNeedsNumberOfGateways;

	public AAdvGatewaySelection(String name, boolean needsNumberOfGateway) {
		this.name = name;
		Monitor.log(AAdvGatewaySelection.class, Level.DEBUG, "Starte %s as Gateway-Selection.", name);
		this.methodNeedsNumberOfGateways = needsNumberOfGateway;
		lastTimeGateway = new HashMap<INodeID, Long>();
		timesAsGateway = new HashMap<INodeID, Long>();
	}

	public void setBehaviour(GatewayLogicBehavior behaviour) {
		this.behaviour = behaviour;
		Monitor.log(AAdvGatewaySelection.class, Level.DEBUG, "Setze das Gateway-Verhalten auf %s. Gewollt?",
				behaviour.toString());
	}

	public GatewayLogicBehavior getBehaviour() {
		return behaviour;
	}

	protected abstract Map<INodeID, List<INodeID>> determineGateways(List<INodeID> outOf);

	@Override
	public Map<INodeID, List<INodeID>> getGateways(List<INodeID> outOf, Graph dataGraph, int maxNumberOfGateways) {

		this.maxNumberOfGateways = maxNumberOfGateways;

		// Round based metrics
		round++;
		for (INodeID node : outOf) {
			// Init of unknown nodes
			if (!lastTimeGateway.containsKey(node)) {
				lastTimeGateway.put(node, null);
			}
			if (!timesAsGateway.containsKey(node)) {
				timesAsGateway.put(node, new Long(0));
			}
		}

		// If necessary, load and save latest positions
		if (positionsHolder != null) {
			if (!lastRoundSelectionExecuted) {
				saveLocations(new LinkedList<INodeID>());
			}
			saveLocations(outOf);
		}

		Map<INodeID, List<INodeID>> result;
		// If less stations online, then all mobile station are gateways, otherwise determine gateways.
		if (outOf.size() <= maxNumberOfGateways) {
			result = new HashMap<INodeID, List<INodeID>>(outOf.size());
			for (INodeID node : outOf) {
				result.put(node, new LinkedList<INodeID>());
			}
			lastRoundSelectionExecuted = false;
			// System.out.println(
			// "Round " + round + ": Gateway limitation not reached, all mobile
			// stations assigned to gateways.");
		} else {
			// If necessary, calculate neighborhood
			if (neighborhoodGraph != null) {
				calcNeighborhood(outOf);
			}
			// If necessary, load and normalize energy levels
			if (normalizedEnergielevels != null) {
				calcNormalizedEnergyLevels(outOf);
			}
			// If necessary, load and normalize latencies
			if (normalizedLatencies != null) {
				calcNormalizedLatencies(outOf);
			}

			result = determineGateways(outOf);
			if (behaviour == GatewayLogicBehavior.CUTTING) {
				INodeID smallestCluster = null;
				int smallestClusterSize, curClusterSize;
				if (result.size() > maxNumberOfGateways) {
					Monitor.log(AAdvGatewaySelection.class, Level.DEBUG,
							"Gateway CUTTING active. Returning only largest %s clusters instead of all %s results.",
							maxNumberOfGateways, result.size());
				}
				while (result.size() > maxNumberOfGateways) {
					smallestClusterSize = Integer.MAX_VALUE;
					for (Entry<INodeID, List<INodeID>> e : result.entrySet()) {
						curClusterSize = e.getValue().size();
						if (smallestClusterSize > curClusterSize) {
							smallestClusterSize = curClusterSize;
							smallestCluster = e.getKey();
							if (smallestClusterSize == 0) {
								break;
							}
						}
					}
					result.remove(smallestCluster);
				}
			}
			for (INodeID gw : result.keySet()) {
				lastTimeGateway.put(gw, round);
				timesAsGateway.put(gw, timesAsGateway.get(gw) + 1);
			}
			lastRoundSelectionExecuted = true;
		}

		// System.out.println("Round " + round + ": Assigned " + result.size() +
		// " Gateways out of " + outOf.size()
		// + " nodes. p = " + ((double) result.size() / (double) outOf.size()));

		return result;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Map<SiSType<?>, SiSRequest> getRequiredInformation() {
		if (types == null) {
			types = new LinkedHashMap<>();

			types.putAll(getRequiredInformationBySuperior());
			types.putAll(getRequiredInformationByStrategy());

			if (normalizedEnergielevels != null) {
				typeEnergylevel = SiSTypes.getType("ENERGY_BATTERY_LEVEL", Double.class);
				types.put(typeEnergylevel, SiSRequest.NONE);
				typeEnergycapacity = SiSTypes.getType("ENERGY_BATTERY_CAPACITY", Double.class);
				types.put(typeEnergycapacity, SiSRequest.NONE);
			}
		}
		return types;
	}

	protected abstract Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy();

	protected abstract Map<SiSType<?>, SiSRequest> getRequiredInformationBySuperior();

	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////
	// Parameters //
	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////

	/**
	 * Gets for each given node the locations and saves them into the position holder.<br>
	 * Requires <b> Raw data: Location</b>
	 * 
	 * @param outOf
	 *            the nodes
	 */
	private void saveLocations(List<INodeID> outOf) {
		Map<INodeID, Location> newPositions = new HashMap<INodeID, Location>(outOf.size());
		for (INodeID node : outOf) {
			try {
				newPositions.put(node, getValue(node, typeLocation));
			} catch (InformationNotAvailableException e) {
				// mobile station will be ignored
				// newPositions.put(node, null);
			}
		}
		positionsHolder.add(newPositions);
	}

	/**
	 * Calculates the average speed of a node since the last GW selection. Time isn't considered. <br>
	 * Requires <b>positionsHolder</b> and at least 2 positions.
	 * 
	 * @param node
	 *            the node
	 * @return 0 if less than 2 positions available, otherwise speed
	 */
	protected double calcAvgSpeed(INodeID node) {
		// Get last r locations
		LinkedList<Location> locationsOfANode = new LinkedList<Location>();
		for (Map<INodeID, Location> locationsOfRound : positionsHolder) {
			Location curLocation = locationsOfRound.get(node);
			if (curLocation == null) {
				break;
			}
			locationsOfANode.add(curLocation);
		}

		// Calculate speed
		if (locationsOfANode.size() < 2) {
			// Speed undefined
			return 0;
		} else {
			double speed = 0;
			Location aPosition = locationsOfANode.getFirst();
			for (int i = 1; i < locationsOfANode.size(); i++) {
				Location bPosition = locationsOfANode.get(i);
				speed += aPosition.distanceTo(bPosition);
				aPosition = bPosition;
			}
			speed = speed / (locationsOfANode.size() - 1);
			return speed;
		}
	}

	/**
	 * Determines the highest and lowest energy level to normalize the level. <br>
	 * Requires <b> Raw data: Location</b>
	 * 
	 * @param nodes
	 *            the nodes
	 */
	protected void calcNormalizedEnergyLevels(List<INodeID> nodes) {
		double energylevelMax = 0;
		double energylevelMin = Double.MAX_VALUE;
		for (INodeID node : nodes) {
			try {
				double energiePercentage = getValue(node, typeEnergylevel);
				double energieCapacity = getValue(node, typeEnergycapacity);
				double energielevel = energiePercentage / 100 * energieCapacity;
				normalizedEnergielevels.put(node, energielevel);
				if (energielevel > energylevelMax) {
					energylevelMax = energielevel;
				}
				if (energielevel < energylevelMin) {
					energylevelMin = energielevel;
				}
			} catch (InformationNotAvailableException e) {
				normalizedEnergielevels.put(node, new Double(0));
			}
		}

		for (INodeID node : normalizedEnergielevels.keySet()) {
			double energy = normalizedEnergielevels.get(node);
			if (energy != 0) {
				energy = (energy - energylevelMin) / (energylevelMax - energylevelMin);
				normalizedEnergielevels.put(node, energy);
			}
		}
	}

	/**
	 * Determines the highest and lowest latency to normalize the level. <br>
	 * Requires <b> Raw data: Latency</b>
	 * 
	 * @param nodes
	 *            the nodes
	 * @author Jonas Huelsmann
	 */
	protected void calcNormalizedLatencies(List<INodeID> nodes) {
		double latencyMax = 0;
		double latencyMin = Double.MAX_VALUE;
		for (INodeID node : nodes) {
			try {
				double latency = getValue(node, typeLatency);
				normalizedLatencies.put(node, latency);
				if (latency > latencyMax) {
					latencyMax = latency;
				}
				if (latency < latencyMin) {
					latencyMin = latency;
				}
			} catch (InformationNotAvailableException e) {
				normalizedLatencies.put(node, Double.MAX_VALUE);
			}
		}
		// normalize
		for (INodeID node : normalizedLatencies.keySet()) {
			double latency = normalizedLatencies.get(node);
			if (latency != Double.MIN_VALUE) {
				latency = (latency - latencyMin) / (latencyMax - latencyMin);
				normalizedLatencies.put(node, latency);
			}
		}
	}

	/**
	 * 
	 * @param nodes
	 */
	protected void calcNeighborhood(List<INodeID> nodes) {
		// Create a new empty graph
		neighborhoodGraph = new BasicGraph();
		neighborhoodGraph.createAndAddNodes(nodes);

		// Get Locations
		Map<INodeID, Location> curLocations = positionsHolder.getLast();

		// Calculate Edges
		double distance;
		for (int i = 0; i < nodes.size(); i++) {
			INodeID nodeA = nodes.get(i);
			Location locA = curLocations.get(nodeA);
			if (locA == null) {
				continue;
			}
			for (int j = i + 1; j < nodes.size(); j++) {
				INodeID nodeB = nodes.get(j);
				Location locB = curLocations.get(nodeB);
				if (locB == null) {
					continue;
				}
				distance = locA.distanceTo(locB);
				if (distance <= TempClusteringConfig.maxRange) {
					neighborhoodGraph.addEdge(neighborhoodGraph.createAndAddEdge(nodeA, nodeB));
					neighborhoodGraph.addEdge(neighborhoodGraph.createAndAddEdge(nodeB, nodeA));
				}
			}
		}
	}

}
