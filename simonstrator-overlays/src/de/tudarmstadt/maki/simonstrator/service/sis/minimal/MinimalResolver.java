package de.tudarmstadt.maki.simonstrator.service.sis.minimal;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.monitoring.MonitoringComponent;
import de.tudarmstadt.maki.simonstrator.api.component.monitoring.MonitoringComponent.MonitoringHandle;
import de.tudarmstadt.maki.simonstrator.api.component.monitoring.MonitoringComponent.MonitoringResultCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSDataCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer.AggregationFunction;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer.SiSConsumerHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider.SiSProviderHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationResolver;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSProviderErrorModel;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSResultCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.DerivationNotPossibleException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypeDerivation;
import de.tudarmstadt.maki.simonstrator.service.sis.util.LocalSources;

/**
 * Simple implementation of a resolver for the SiS
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class MinimalResolver implements SiSInformationResolver {

	private LocalSources observations = new LocalSources();

	private Map<SiSType<?>, SiSProviderErrorModel> errorModels = new LinkedHashMap<>();

	private final SiSComponent sis;

	public MinimalResolver(SiSComponent sis) {
		this.sis = sis;
	}

	@Override
	public <T> T resolveLocally(INodeID observedNode, SiSType<T> type,
			SiSRequest request) throws InformationNotAvailableException {

		if (observedNode == null) {
			observedNode = sis.getHost().getId();
		}

		/*
		 * Local lookup
		 */
		try {
			return observations.getValue(observedNode, type,
					request);
		} catch (InformationNotAvailableException e) {
			// Local direct call failed
		}

		/*
		 * Try to derive the information
		 */
		SiSTypeDerivation<T> derivation = type.canDerive(observations
				.getLocallyAvailableTypes());
		if (derivation != null) {
			try {
				return derivation.derive(observations,
						observations.getLocallyAvailableTypes());
			} catch (DerivationNotPossibleException e) {
				// Derivation not possible
			}
		}

		throw new InformationNotAvailableException();
	}

	@Override
	public <T> Map<INodeID, T> getAllLocalObservations(SiSType<T> type,
			SiSRequest request) {
		return observations.getAllObservations(type, request);
	}

	@Override
	public <T> SiSConsumerHandle aggregatedObservation(final AggregationFunction aggFunction, final SiSType<T> type,
			final SiSRequest request, final SiSResultCallback<T> callback) {
		/*
		 * TODO Step 1: check local storage (cache) for an answer
		 */

		/*
		 * Step 2: tell monitoring to collect the result
		 */

		// Just do the same as "rawObservations", just not as 'gracious' >.<
		try {
			// grab the monitoring component
			MonitoringComponent monitoring = sis.getHost().getComponent(MonitoringComponent.class);

			// start monitoring, generate the handle...
			// Monitoring should call onResult() as soon as it has all Info, or
			// the Request times out.
			MonitoringHandle monHandle = monitoring.collectAggregatedObservation(aggFunction, type, request,
					new MonitoringResultCallbackImpl<T>(callback));
			// generate the cosumerHandle, just as rawObservations
			final SiSConsumerHandle consumerHandle = new SiSConsumerHandleImpl(monHandle);

			if (callback != null) {
				// Store the generated handle for later
				callbackMapper.put(callback, consumerHandle);
			}
			return consumerHandle;

		} catch (ComponentNotAvailableException e) {
			throw new AssertionError();
		}
	}

	// Callback for a specific Request
	private class MonitoringResultCallbackImpl<T> implements MonitoringResultCallback<T> {
		private final SiSResultCallback<T> callback;

		public MonitoringResultCallbackImpl(final SiSResultCallback<T> callback) {
			this.callback = callback;
		}

		@Override
		public void onResult(T result) {
			gotResult(result, callback);
		}
	}

	@SuppressWarnings("rawtypes")
	private Map<SiSResultCallback, SiSConsumerHandle> callbackMapper = new HashMap<SiSResultCallback, SiSConsumerHandle>();

	protected <T> void gotResult(T result, SiSResultCallback<T> callback) {
		if (callback != null)
			// Callback with the same Consumerhandle as it was generated for
			// this specific request.
			callback.onResult(result, callbackMapper.remove(callback));
	}

	@Override
	public <T> SiSConsumerHandle rawObservations(final SiSType<T> type,
			final SiSRequest request,
			final SiSResultCallback<Map<INodeID, T>> callback) {
		/*
		 * TODO Check local state, if accurate information is available. Check,
		 * if we need to ask monitoring again.
		 */

		/*
		 * Else: configure Monitoring to collect the required information
		 * 
		 * FIXME timeout handling
		 */
		try {
			MonitoringComponent monitoring = sis.getHost().getComponent(
					MonitoringComponent.class);
			/*
			 * TODO save and maintain monitoring handles
			 */
			MonitoringHandle monHandle = monitoring.collectRawObservations(
					type, request);
			final SiSConsumerHandle consumerHandle = new SiSConsumerHandleImpl(
					monHandle);
			/*
			 * Trigger result callback (TODO after timeout or earlier?)
			 */
			if (callback != null) {
				Event.scheduleWithDelay(request.getTimeout(),
						new EventHandler() {
							@Override
							public void eventOccurred(Object content,
									int eventType) {
								callback.onResult(
										getAllLocalObservations(type, request),
										consumerHandle);
							}
						}, null, 0);
			}
			return consumerHandle;
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError();
		}
	}

	/**
	 * Adds error models
	 * 
	 * @param errorModel
	 */
	public void setErrorModels(Map<SiSType<?>, SiSProviderErrorModel> errorModels) {
		this.errorModels = errorModels;
	}

	@Override
	public <T> SiSProviderHandle addObservationProvider(SiSType<T> type,
			SiSDataCallback<T> dataCallback) {

		if (errorModels.containsKey(type)) {
			// wrap data callback into error model
			return observations.addSource(type,
					errorModels.get(type).createErrorModelCallback(sis.getHost(), type, dataCallback));
		} else {
			return observations.addSource(type, dataCallback);
		}
	}

	/**
	 * Remove the local source
	 * 
	 * @param handle
	 */
	@Override
	public void removeLocalProvider(SiSProviderHandle handle) {
		observations.removeSource(handle);
	}

	@Override
	public void removeConsumer(SiSConsumerHandle handle) {
		assert handle instanceof SiSConsumerHandleImpl;
		/*
		 * TODO implement "real" monitoring handling.
		 */
		SiSConsumerHandleImpl sisHandle = (SiSConsumerHandleImpl) handle;
		try {
			MonitoringComponent monitoring = sis.getHost().getComponent(
					MonitoringComponent.class);
			if (sisHandle.monitoringHandle != null) {
				// FIXME this should probably try to only stop Collecting if no
				// other consumer tries to access that information
				monitoring.stopCollecting(sisHandle.monitoringHandle);
			}
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError();
		}
	}

	/*
	 * TODO: add monitoring gateway
	 */

	private class SiSConsumerHandleImpl implements SiSConsumerHandle {

		public final MonitoringHandle monitoringHandle;

		public SiSConsumerHandleImpl(MonitoringHandle monitoringHandle) {
			this.monitoringHandle = monitoringHandle;
		}

		//
	}

}
