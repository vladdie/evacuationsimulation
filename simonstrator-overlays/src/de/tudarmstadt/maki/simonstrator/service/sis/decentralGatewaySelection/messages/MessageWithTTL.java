package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages;

public interface MessageWithTTL {

	public void setTTL(int ttl);

	public int getTTL();
}
