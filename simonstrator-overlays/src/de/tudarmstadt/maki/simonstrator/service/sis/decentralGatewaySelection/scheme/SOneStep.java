package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme;

import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSComponentFactory;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.DeclareCHMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;

/**
 * Stochastic scheme that takes the given weight and compares it to a random
 * value. If the weight is greater than the random number, the scheme terminates
 * as CH.
 * 
 * @author Christoph Storm
 *
 */
public class SOneStep implements DGSScheme {

	private static int radius = -1;
	private static long timeout = 30 * Time.SECOND;

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	// @TransferState({ "IsCH" })
	private boolean isCH = false;

	private Random random;

	@TransferState({ "Parent" })
	public SOneStep(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
		random = Randoms.getRandom(parent);
	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public void startScheme(double weight) {
		// higher weight means higher probability...
		double rand = random.nextDouble();
		if (rand < weight) {
			// we are CH
			onBecameClusterHead();
			parent.DGSSchemeFinished(NodeRole.CLUSTERHEAD);
		} else {
			// We are not CH
			isCH = false;

			// wait for timeout or CH Message
			Event.scheduleWithDelay(timeout, new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					if (parent != null && parent.isActive()) {
						parent.DGSSchemeFinished(NodeRole.NONE);
					}
				}
			}, null, 24);
		}
	}

	@Override
	public void onMessageArrived(Message msg) {
		if (msg instanceof DeclareCHMessage && !isCH && parent.isActive()) {
			// Node is not CH, so take the first we hear?
			parent.getLocalDissemination().onMessageReceived(msg);
			if (parent.getClusterer() != null) {
				parent.getClusterer().finalizeClustering();
			}
			parent.DGSSchemeFinished(NodeRole.CLUSTERMEMBER);
		}
	}

	@Override
	public void startMechanism(Callback cb) {
		// nothing to do here?!
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		parent = null;
		isCH = false;
		cb.finished(true);
	}
	
	/**
	 * Sets the TTL for CH-messages. Values smaller one should be treated as
	 * infinity
	 * 
	 * @param hops
	 */
	public static void setRadius(int hops){
		if (hops > 0)
			SOneStep.radius = hops;
		else
			SOneStep.radius = -1;
	}

	public static void setTimeOut(long timeout){
		assert timeout > 0 : "Timout must be larger zero!";
		SOneStep.timeout = timeout;
	}

	@Override
	public void onBecameClusterHead() {
		// TODO move to Abstract Superclass!
		isCH = true;
		DeclareCHMessage msg = new DeclareCHMessage(parent.getRound(),
				DGSComponentFactory.getNewMessageID(), parent.getHost().getId(),
				radius);
		// Ensure that this message is disseminated
		parent.getLocalDissemination().disseminate(msg, 1d);

		if (parent.getClusterer() != null) {
			parent.getClusterer().setClusterID(parent.getHost().getId());
		}

	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.SONESTEP;
	}

	/*
	 * public boolean getIsCH() { return isCH; }
	 * 
	 * public void setIsCH(boolean isCH) { this.isCH = isCH; }
	 */

}
