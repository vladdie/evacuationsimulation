package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.SkC;

public class SkC_HID extends SkC {

	public SkC_HID() {
		super("SkC_HID");
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		for (INodeID v : forNodes) {
			setProbability(v, v.value());
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		return Collections.emptyMap(); // No SiS interaction is required
	}

}
