package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionListener;

/**
 * These are pretty much the same methods as for the clients, but this is not a
 * transition-enabled class!
 *
 */
public interface GatewaySelectionClientController {

	/**
	 * Is this client currently a selected Gateway?
	 * 
	 * @return
	 */
	public boolean isGateway();

	/**
	 * Which node {@link INodeID} is this' nodes gateway?
	 * 
	 * @return
	 */
	public INodeID getGateway();

	public NodeRole getNodeRole();

	/**
	 * Adds a listener that is called whenever a Gateway selection process
	 * ended.
	 * 
	 * @param listener
	 */
	public void addGatewaySelectionListener(GatewaySelectionListener listener);

	public void removeGatewaySelectionListener(GatewaySelectionListener listener);
}
