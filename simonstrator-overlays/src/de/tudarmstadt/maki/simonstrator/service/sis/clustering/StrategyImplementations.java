package de.tudarmstadt.maki.simonstrator.service.sis.clustering;

import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_BECLEACH_DBScan;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_BECLEACH_Grid;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_BECLEACH_kMeans;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_HID_DBScan;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_HID_Grid;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_HID_kMeans;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_LAT_DBScan;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_LAT_Grid;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_LAT_kMeans;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_Random_DBScan;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_Random_Grid;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_Random_kMeans;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_WCA_DBScan;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_WCA_Grid;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_WCA_kMeans;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs.CS_BECLEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs.CS_HID;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs.CS_Random;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs.CS_WCA;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_FWCABP;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_HID;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_LAT;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_Random;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_WCA;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_ALEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_DEEC;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_HID;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_LAT;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_LEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_Random;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec.SEC_ALEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec.SEC_DEEC;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec.SEC_LEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec.SEC_Random;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_ALEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_DEEC;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_HID;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_LEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_Random;

/**
 * Should <i>eventually</i> include all non-abstract implementations of
 * {@link GatewaySelectionStrategy} in upper-case only letters.
 * <p>
 * <b>Update when needed!</b>
 */
public enum StrategyImplementations {
	// CD
	CD_BECLEACH_DBSCAN(CD_BECLEACH_DBScan.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_BECLEACH_DBScan"),
	CD_BECLEACH_GRID(CD_BECLEACH_Grid.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_BECLEACH_Grid"),
	CD_BECLEACH_KMEANS(CD_BECLEACH_kMeans.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_BECLEACH_kMeans"),
	CD_HID_DBSCAN(CD_HID_DBScan.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_HID_DBScan"),
	CD_HID_GRID(CD_HID_Grid.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_HID_Grid"),
	CD_HID_KMEANS(CD_HID_kMeans.class,"de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_HID_kMeans"),
	CD_LAT_DBSCAN(CD_LAT_DBScan.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_LAT_DBScan"),
	CD_LAT_GRID(CD_LAT_Grid.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_LAT_Grid"),
	CD_LAT_KMEANS(CD_LAT_kMeans.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_LAT_kMeans"),
	CD_RANDOM_DBSCAN(CD_Random_DBScan.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_Random_DBScan"),
	CD_RANDOM_GRID(CD_Random_Grid.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_Random_Grid"),
	CD_RANDOM_KMEANS(CD_Random_kMeans.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_Random_kMeans"),
	CD_WCA_DBSCAN(CD_WCA_DBScan.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_WCA_DBScan"),
	CD_WCA_GRID(CD_WCA_Grid.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_WCA_Grid"),
	CD_WCA_KMEANS(CD_WCA_kMeans.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_WCA_kMeans"),
	// CS
	CS_BECLEACH(CS_BECLEACH.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs.CS_BECLEACH"),
	CS_HID(CS_HID.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs.CS_HID"),
	CS_RANDOM(CS_Random.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs.CS_Random"),
	CS_WCA(CS_WCA.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs.CS_WCA"),
	// D1C
	D1C_FWCABP(D1C_FWCABP.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_FWCABP"),
	D1C_HID(D1C_HID.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_HID"),
	D1C_LAT(D1C_LAT.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_LAT"),
	D1C_RANDOM(D1C_Random.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_Random"),
	D1C_WCA(D1C_WCA.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_WCA"),
	// DKC
	DKC_ALEACH(DkC_ALEACH.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_ALEACH"),
	DKC_DEEC(DkC_DEEC.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_DEEC"),
	DKC_HID(DkC_HID.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_HID"),
	DKC_LAT(DkC_LAT.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_LAT"),
	DKC_LEACH(DkC_LEACH.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_LEACH"),
	DKC_RANDOM(DkC_Random.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_Random"),
	// SEC
	SEC_ALEACH(SEC_ALEACH.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec.SEC_ALEACH"),
	SEC_DEEC(SEC_DEEC.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec.SEC_DEEC"),
	SEC_LEACH(SEC_LEACH.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec.SEC_LEACH"),
	SEC_RANDOM(SEC_Random.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec.SEC_Random"),
	// SKC
	SKC_ALEACH(SkC_ALEACH.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_ALEACH"),
	SKC_DEEC(SkC_DEEC.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_DEEC"),
	SKC_HID(SkC_HID.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_HID"),
	SKC_LEACH(SkC_LEACH.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_LEACH"),
	SKC_RANDOM(SkC_Random.class, "de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc.SkC_Random");

	private final Class<? extends GatewaySelectionStrategy> implementingClass;
	private final String path;
	
	private StrategyImplementations(Class<? extends GatewaySelectionStrategy> implementingClass, String path) {
		this.implementingClass = implementingClass;
		this.path = path;
	}
	
	public Class<? extends GatewaySelectionStrategy> getImplementingClass() {
		return implementingClass;
	}
	
	public String getPath(){
		return path;
	}
}
