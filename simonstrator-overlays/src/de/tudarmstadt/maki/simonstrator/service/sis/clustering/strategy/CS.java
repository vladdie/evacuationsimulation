package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.AStochasticGatewaySelection;

public abstract class CS extends AStochasticGatewaySelection {

	public CS(String name, boolean needsNumberOfGateway) {
		super(name, needsNumberOfGateway);
	}

	@Override
	protected Map<INodeID, List<INodeID>> determineGateways(List<INodeID> outOf) {

		// Copy list
		LinkedList<INodeID> nodes = new LinkedList<>(outOf);
		Set<INodeID> unknownNodes = new HashSet<>(outOf);

		// Calculate clusters
		List<List<INodeID>> clusters = calculateCluster(nodes);

		Map<INodeID, List<INodeID>> tree = new LinkedHashMap<>();

		// Calculate probabilities and determine gateway for each cluster
		for (List<INodeID> cluster : clusters) {
			// Remove known nodes
			unknownNodes.removeAll(cluster);

			// Draw Gateway
			INodeID newGW;
			if (cluster.size() > 1) {
				calculateProbability(cluster);
				newGW = drawGateway(cluster);
				cluster.remove(newGW);
			} else {
				newGW = cluster.remove(0);
			}
			tree.put(newGW, cluster);
		}

		// Add unknown nodes
		for (INodeID node : unknownNodes) {
			tree.put(node, new LinkedList<INodeID>());
		}

		return tree;
	}

	protected abstract List<List<INodeID>> calculateCluster(List<INodeID> nodes);

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationBySuperior() {
		return new LinkedHashMap<>();
	}

}
