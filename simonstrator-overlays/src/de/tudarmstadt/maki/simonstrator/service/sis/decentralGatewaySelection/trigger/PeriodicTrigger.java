package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;

/**
 * A periodic trigger that may be retriggered from outside.
 * 
 * @author Nils Richerzhagen
 *
 */
public class PeriodicTrigger implements Trigger, EventHandler {

	private static long interval = 10 * Time.MINUTE;

	/**
	 * Adds 20% fluctuation on the start timestamp using the current interval.
	 */
	private static boolean useStartupJitter = true;
	
	@TransferState({ "TriggerListeners" })
	private LinkedList<TriggerListener> listeners = new LinkedList<TriggerListener>();

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	@TransferState({ "Parent" })
	public PeriodicTrigger(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	/**
	 * List for all Events scheduled.
	 */
	private LinkedList<TriggerEvent> triggerEvents = new LinkedList<TriggerEvent>();

	@Override
	public void onTriggeredFromOutside() {
		triggerEvents.clear();
		gotTriggered(); // Call the listeners anyway
	}

	protected void gotTriggered() {
		if (!parent.isActive()) {
			// System.out.println(Time.getFormattedTime() + "Triggered");
			for (TriggerListener listener : listeners) {
				listener.onTriggered();
			}
		}
		scheduleTriggerEvent(interval);
	}

	@Override
	public void addTriggerListener(TriggerListener listener) {
		listeners.add(listener);
	}

	@Override
	public void startMechanism(Callback cb) {

		long delay = interval - (Time.getCurrentTime() % interval);
		// long delay = interval; // no more synchronized timing...

		if (useStartupJitter) {
			// using up to 20% derivation from the actual scheduled time
			long dt = (long) ((-0.2d + (0.2d + 0.2d) * Randoms.getRandom(this).nextDouble()) * delay);
			delay += dt;
		}
		scheduleTriggerEvent(delay);

		cb.finished(true);
	}

	private void scheduleTriggerEvent(long delay) {
		TriggerEvent event = new TriggerEvent();
		Event.scheduleWithDelay(delay, this, event, 0);
		triggerEvents.add(event);
	}

	@Override
	public void stopMechanism(Callback cb) {
		parent = null;
		triggerEvents.clear();

		cb.finished(true);
	}

	public LinkedList<TriggerListener> getTriggerListeners() {
		return listeners;
	}

	public void setTriggerListeners(LinkedList<TriggerListener> listeners) {
		this.listeners = listeners;
	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	public static void setInterval(long time) {
		interval = time;
	}

	@Override
	public void removeTriggerListener(TriggerListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void eventOccurred(Object content, int type) {
		assert content instanceof TriggerEvent;

		TriggerEvent trigEvent = (TriggerEvent) content;
		// Trigger and remove if event is still valid.
		if (triggerEvents.remove(trigEvent)) {
			gotTriggered();
		}
	}

	// FIXME Does this need any extra info?
	private class TriggerEvent {

		public TriggerEvent() {

		}
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.TIMERBASEDTRIGGER;
	}

}
