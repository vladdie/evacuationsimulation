package de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.TempClusteringConfig;

/**
 * 
 * @author Michael Walter
 *
 */
public class GridDensityClusterer implements IClusterer {

	private double maxRange;
	private double maxDensity;

	public GridDensityClusterer() {
		this.maxRange = TempClusteringConfig.maxTransmissionRange;
		this.maxDensity = TempClusteringConfig.optimalClusterSize;
	}

	public List<List<INodeID>> getClusters(Map<INodeID, Location> nodes) {

		// Create grid
		ArrayList<List<INodeID>> grid = new ArrayList<List<INodeID>>();

		// Discover area dimensions
		double xMin = Double.MAX_VALUE;
		double yMin = Double.MAX_VALUE;
		double xMax = 0;
		double yMax = 0;
		for (Location l : nodes.values()) {
			double curX = l.getLongitude();
			double curY = l.getLatitude();
			xMin = Math.min(curX, xMin);
			yMin = Math.min(curY, yMin);
			xMax = Math.max(curX, xMax);
			yMax = Math.max(curY, yMax);
		}
		double xRange = xMax - xMin;
		double yRange = yMax - yMin;

		// Determine xStep and yStep
		double maxGridStep = Math.sqrt(Math.pow(maxRange, 2) / 2);
		double numberOfXRectangle = Math.ceil(xRange / maxGridStep);
		double numberOfYRectangle = Math.ceil(yRange / maxGridStep);
		double xStep = xRange / numberOfXRectangle;
		double yStep = yRange / numberOfYRectangle;

		// Assign Nodes to rectangles
		Map<Integer, Rectangle> rectangles = new HashMap<Integer, Rectangle>();
		for (Entry<INodeID, Location> entry : nodes.entrySet()) {
			Location curLocation = entry.getValue();
			int xRectangle = getRectangle(xMin, xMax, xStep, curLocation.getLongitude());
			int yRectangle = getRectangle(yMin, yMax, yStep, curLocation.getLatitude());
			int numberOfRectangle = xRectangle * 1000 + yRectangle;
			Rectangle curRectangle = rectangles.get(numberOfRectangle);
			if (curRectangle == null) {
				rectangles.put(numberOfRectangle, new Rectangle(entry));
			} else {
				curRectangle.addNode(entry);
			}
		}

		// Determine Squares
		for (Entry<Integer, Rectangle> e : rectangles.entrySet()) {
			Rectangle rectangle = e.getValue();
			Map<INodeID, Location> nodesOfRectangle = rectangle.getNodesWithLocations();
			if (nodesOfRectangle.size() > maxDensity) {
				// System.out.println("Zelle " + e.getKey() + ": " +
				// nodesOfRectangle.size());
				for (List<INodeID> cluster : divideSquare(rectangle.getNodesWithLocations())) {
					if (!cluster.isEmpty()) {
						grid.add(cluster);
					}
				}
			} else {
				grid.add(new LinkedList<INodeID>(nodesOfRectangle.keySet()));
			}
		}

		// Check for empty clusters and remove them.
		for (List<INodeID> cluster : grid) {
			if (cluster.isEmpty()) {
				grid.remove(cluster);
			}
		}

		return grid;

	}

	private List<List<INodeID>> divideSquare(Map<INodeID, Location> nodes) {
		List<List<INodeID>> result = new ArrayList<List<INodeID>>(2);
		if (nodes.size() <= maxDensity) {
			result.add(new ArrayList<INodeID>(nodes.keySet()));
		} else {
			// Find x and y Range
			double xMin = Double.MAX_VALUE;
			double yMin = Double.MAX_VALUE;
			double xMax = 0;
			double yMax = 0;
			for (Location l : nodes.values()) {
				double curX = l.getLongitude();
				double curY = l.getLatitude();
				xMin = Math.min(curX, xMin);
				yMin = Math.min(curY, yMin);
				xMax = Math.max(curX, xMax);
				yMax = Math.max(curY, yMax);
			}
			double xRange = xMax - xMin;
			double yRange = yMax - yMin;
			// System.out.println("N-Zelle: " + xMin + "/" + xMax + "\t" + yMin
			// + "/" + yMax);

			Map<INodeID, Location> area1 = new HashMap<INodeID, Location>();
			Map<INodeID, Location> area2 = new HashMap<INodeID, Location>();
			if (xRange > yRange) { // Teile durch x
				double newXRange = xRange / 2;
				// System.out.println(
				// "x-Zelle 1: " + xMin + "/" + (xMin + newXRange) + "\t" +
				// (yMin) + "/" + (yMin + yRange)); // Area 2
				// System.out.println("x-Zelle 2: " + (xMin + newXRange) + "/" +
				// (xMin + xRange) + "\t" + (yMin) + "/"
				// + (yMin + yRange)); // Area 1
				for (Entry<INodeID, Location> e : nodes.entrySet()) {
					double xNode = e.getValue().getLongitude();
					if (xNode > xMin + newXRange) {
						area2.put(e.getKey(), e.getValue());
					} else {
						area1.put(e.getKey(), e.getValue());
					}
				}
			} else {
				double newYRange = yRange / 2;
				// System.out.println(
				// "y-Zelle 1: " + xMin + "/" + (xMin + xRange) + "\t" + (yMin)
				// + "/" + (yMin + newYRange)); // Area 2
				// System.out
				// .println("y-Zelle 2: " + xMin + "/" + xMin + "\t" + (yMin +
				// newYRange) + "/" + (yMin + yRange)); // Area 1

				for (Entry<INodeID, Location> e : nodes.entrySet()) {
					double yNode = e.getValue().getLatitude();
					if (yNode > yMin + newYRange) {
						area2.put(e.getKey(), e.getValue());
					} else {
						area1.put(e.getKey(), e.getValue());
					}
				}
			}
			result.addAll(divideSquare(area1));
			result.addAll(divideSquare(area2));
		}
		return result;
	}

	private int getRectangle(double min, double max, double step, double pos) {
		double relPos = pos - min;
		double numberOfRectangle = relPos / step;
		// Check borders [0...step][step+e...2step]
		if (pos == max) {
			numberOfRectangle--;
		}
		return (int) numberOfRectangle;

	}

	private class Rectangle {

		private Map<INodeID, Location> nodes;

		public Rectangle(Entry<INodeID, Location> node) {
			nodes = new HashMap<INodeID, Location>();
			addNode(node);
		}

		public Map<INodeID, Location> getNodesWithLocations() {
			return nodes;
		}

		public void addNode(Entry<INodeID, Location> node) {
			nodes.put(node.getKey(), node.getValue());
		}

		public String toString() {
			String result = "[";
			for (Entry<INodeID, Location> node : nodes.entrySet()) {
				result += node + ", ";
			}
			result = result.substring(0, result.length() - 2) + "]";
			return result;
		}
	}

}
