package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.IEdge;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.util.SiSRequestTripple;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;

/**
 * This class tries to compute a normalized weight, while staying somewhat true
 * to the original WCA paper <br>
 * ("WCA: A Weighted Clustering Algorithm forMobile Ad Hoc Networks" by
 * ChatterJee at al.)
 * 
 * @author Christoph Storm
 * @version 2016.06.22
 */
public class Scaled_WCA implements WeightComputation {

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	@TransferState({ "BaseProbability" })
	// This doesn't seem right...
	private Double idealClusterSize = 10d;

	private Location lastLocation = null;
	private double lastSpeed = 0;

	private long totalTime = 0;
	private long timeAsCH = 0;
	private long lastActive = Long.MIN_VALUE;

	@TransferState({ "Parent" })
	public Scaled_WCA(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public void startMechanism(Callback cb) {
		// passive class, nothing to do here...
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		parent = null;
		cb.finished(true);
	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public List<SiSRequestTripple> getRequiredInformationTypes() {
		LinkedList<SiSRequestTripple> reqList = new LinkedList<SiSRequestTripple>();
		// Really only local info?!
		return reqList;
	}

	@Override
	public double calculateWeight(List<Object> args, List<Object> kwargs) {
		// Trying to get WCA to return a normalized [0, 1] value

		// FIXME This is way to convoluted...

		// initialize values for the final computation
		int numOfNeighbours = 0;
		double scaledSumOfDistances = 0;
		double scaledDegreeDiff = 0;
		Location loc = null;
		double percentageAsCH = 0;
		double speed = 0;
		// Is no information better than information about a bad state?

		// Try to get SiS info
		try {
			Graph neighbourhood = parent.getSis().get().localState(SiSTypes.NEIGHBORS_WIFI, new SiSRequest());


			INodeID parentID = parent.getHost().getId();

			if (!neighbourhood.containsNode(parentID)) {

				boolean isUp = parent.getHost().getNetworkComponent().getByName(NetInterfaceName.WIFI).isUp();

				Monitor.log(Scaled_WCA.class, Level.ERROR, "%s: Node %s is not included in wifi Graph!",
						Time.getFormattedTime(), parentID);
				return 0d;
			}

			neighbourhood = neighbourhood.getLocalView(parentID, 1, true);

			// Incoming or outgoing, what should be used depends on what CHs
			// responsibilities are...
			Set<IEdge> connections = neighbourhood.getIncomingEdges(parentID);
			numOfNeighbours = connections.size();

			scaledSumOfDistances = sumAndScaleDistances(connections);

			// This only returns the location that was set at the very
			// beginning...
			loc = neighbourhood.getNode(parentID).getProperty(SiSTypes.PHY_LOCATION);
			// loc = parent.getSis().get().localState(SiSTypes.PHY_LOCATION, new
			// SiSRequest());
		} catch (InformationNotAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		scaledDegreeDiff = computeScaledDegreeDifference(numOfNeighbours);

		// compute the time stuff
		long timeSinceLastCall = 0;
		if (lastActive >= 0) {
			timeSinceLastCall = Time.getCurrentTime() - lastActive;
			totalTime += timeSinceLastCall;

			if (parent.getNodeRole().equals(NodeRole.CLUSTERHEAD)) {
				timeAsCH += timeSinceLastCall;
			}

			percentageAsCH = timeAsCH / totalTime;
		}
		lastActive = Time.getCurrentTime();

		// compute speed
		if (lastLocation != null && loc != null) {
			double dist = loc.distanceTo(lastLocation);
			speed = (dist / timeSinceLastCall) * Time.SECOND;

			// Original WCA uses the true average over all speeds
			// We use a moving average instead
			speed = (speed + lastSpeed) / 2;
		}
		lastLocation = loc;
		lastSpeed = speed;
		// Normalize speed. How? Normalize to 20m/s?
		double maxNormalSpeed = 20d;
		if (speed > maxNormalSpeed) {
			speed = 1;
		} else {
			speed = speed / maxNormalSpeed;
		}

		// Weights from original WCA paper:
		double combinedWeight, w1 = 0.7, w2 = 0.2, w3 = 0.05, w4 = 0.05;
		
		combinedWeight = w1 * scaledDegreeDiff + w2 * scaledSumOfDistances + w3 * speed + w4 * percentageAsCH;

		// WCA is smallest weight wins! -> Must do a 1-answer at the end!
		combinedWeight = 1 - combinedWeight;

		assert combinedWeight >= 0;
		return combinedWeight;
	}

	private double sumAndScaleDistances(Set<IEdge> connections) {

		double scaledSumOfDistances = 0;

		int numOfNeighbours = connections.size();
		int numOfSummedConnections = numOfNeighbours;

		for (IEdge connection : connections) {
			// this would require a monitoring step in a real system...
			if (connection.getProperty(SiSTypes.PHY_DISTANCE) == null) {
				numOfSummedConnections--;
				continue;
			}
			double distance = connection.getProperty(SiSTypes.PHY_DISTANCE);
			if (distance != distance) {
				// is NaN
				numOfSummedConnections--;
				continue;
			}
			scaledSumOfDistances += distance;
		}
		if (numOfSummedConnections == 0) {
			Monitor.log(Scaled_WCA.class, Level.WARN, "Could not access required information!",
					SiSTypes.PHY_DISTANCE);
			// return a .5 instead of a really good or bad value
			return 0.5;

		} else {
			// assumed radio distance = 300m
			scaledSumOfDistances = scaledSumOfDistances / (numOfSummedConnections * 300);
			scaledSumOfDistances = Math.min(scaledSumOfDistances, 1);
		}
		return scaledSumOfDistances;
	}

	private double computeScaledDegreeDifference(int degree) {

		// for comparison with clustersize, we are a contact of us...
		degree++;

		/*
		 * Compute the degree difference
		 * 
		 * Should result in higher differences having higher values, but bound
		 * by one
		 */
		double degreeDiff = Math.abs(degree - idealClusterSize);
		// value-scaling is no longer linear, but this should not affect
		// the algorithm much...

		if (degreeDiff > 0) {
			// try a better scaling factor...
			double scaledNodeDegree = degreeDiff / (degreeDiff + 2);
			return scaledNodeDegree;
		}
		return degreeDiff;
	}

	/**
	 * For Transferstate...
	 * 
	 * @param p
	 */
	@Override
	public void setBaseProbability(Double p) {
		assert p > 0 : "Probability must be larger than zero!";
		assert p <= 1 : "Probability must be smaller or equall to one!";
		idealClusterSize = (1 / p);
		assert idealClusterSize > 0;
	}

	public Double getBaseProbability() {
		return (1d / idealClusterSize);
	}

	public void setIdealClusterSize(int clusterSize) {
		this.idealClusterSize = (double) clusterSize;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.SCALED_WCA;
	}

}
