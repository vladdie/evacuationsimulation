package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;

/**
 * Trigger without any internal logic. It will <b>never</b> trigger from alone.
 * Purpose is to be used as 'Event-based' trigger, so Overlays that use the DGS
 * can trigger CH-selection.
 * 
 * @author Christoph Storm
 *
 */
public class NoLogicTrigger implements Trigger {

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	@TransferState({ "TriggerListeners" })
	private LinkedList<TriggerListener> listeners = new LinkedList<TriggerListener>();

	@TransferState({ "Parent" })
	public NoLogicTrigger(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public void startMechanism(Callback cb) {
		// passive class, nothing to do here
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		parent = null;
		cb.finished(true);
	}

	@Override
	public void onTriggeredFromOutside() {
		for (TriggerListener listener : listeners) {
			listener.onTriggered();
		}
	}

	@Override
	public void addTriggerListener(TriggerListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeTriggerListener(TriggerListener listener) {
		listeners.remove(listener);
	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	public LinkedList<TriggerListener> getTriggerListeners() {
		return listeners;
	}

	public void setTriggerListeners(LinkedList<TriggerListener> listeners) {
		this.listeners = listeners;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.NOLOGICTRIGGER;
	}

}
