package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.SEC;

public class SEC_LEACH extends SEC {

	public SEC_LEACH() {
		super("SEC_LEACH");
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		// Get Parameters
		double k = maxNumberOfGateways;
		double N = forNodes.size();
		
		// Find a k for full rounds
		double kNew = N;
		double div = 1;
		while (kNew > k) {
			kNew = N / ++div;
		}
		k = kNew;
		
		double lastXRounds = N / k; // 1/p with p = k/N

		// Calculate Probability
		double t_v = k / (N - k * (round % (N / k)));
		for (INodeID node_v : forNodes) {
			Long lastRoundAsGateway = lastTimeGateway.get(node_v);
			if (lastRoundAsGateway == null || lastRoundAsGateway < round - lastXRounds) {
				setProbability(node_v, t_v);
			} else {
				setProbability(node_v, 0.0);
			}
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		return Collections.emptyMap(); // No SiS interaction is required
	}

}
