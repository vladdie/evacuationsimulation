package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger;

import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponent;

/**
 * An interface for Triggers.
 * 
 * @author Christoph Storm
 */
public interface Trigger extends DGSSubComponent {

	/**
	 * Should be called when an outside entity triggered <i>things</i>. Purpose
	 * is to notify this trigger, so it can reset timers or <i>stuff</i>
	 * accordingly.
	 */
	public void onTriggeredFromOutside();

	public void addTriggerListener(TriggerListener listener);

	public void removeTriggerListener(TriggerListener listener);

	public interface TriggerListener {
		public void onTriggered();
	}

}
