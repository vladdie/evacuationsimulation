package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.util.SiSRequestTripple;

/**
 * Implementation of the ALEACH weighting Algorithm (
 * "ALEACH: Advanced LEACH Routing Protocol for Wireless Microsensor Networks"
 * by Ali et al.) <br>
 * Now based of a LEACH parent class
 * 
 * @author Christoph Storm
 * @version 2016.06.22
 */
public class ALEACH extends LEACH {

	@TransferState({ "Parent" })
	public ALEACH(IDecentralGatewaySelectionClient parent) {
		super(parent);
		setClusterHeadExclusion(ClusterHeadExclusion.Last);
	}

	@Override
	public void startMechanism(Callback cb) {
		// Passive class, nothing to do here
		super.startMechanism(cb);
	}

	@Override
	public void stopMechanism(Callback cb) {
		// Passive class, nothing to do here
		super.stopMechanism(cb);
	}

	@Override
	public List<SiSRequestTripple> getRequiredInformationTypes() {
		// No info needed for this?
		return super.getRequiredInformationTypes();
	}

	@Override
	public double calculateWeight(List<Object> args, List<Object> kwargs) {
		// Getting needed information from our Node:

		double threshold = 0;
		if (shouldComputeThreshold(getBaseProbability())) {

			threshold = this.getLeachThreshold(getBaseProbability(),
					parent.getRound());

			SiSRequest request = new SiSRequest();
			// TODO real request, stating infoAge?
			double battery = 100;
			try {
				battery = parent.getSis().get().localState(SiSTypes.ENERGY_BATTERY_LEVEL, request);
			} catch (InformationNotAvailableException e) {
				// should not happen...
				e.printStackTrace();
			}
			battery = battery / 100;

			threshold = (threshold + battery) / 2;
		}

		return threshold;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.ALEACH;
	}

}
