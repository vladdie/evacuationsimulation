package de.tudarmstadt.maki.simonstrator.service.sis.clustering;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * 
 * @author Michael Walter
 *
 */
public abstract class ADeterministicGatewaySelection extends AAdvGatewaySelection {

	private final Map<INodeID, Double> weights;
	protected final WeightComperator comperator;

	public ADeterministicGatewaySelection(String name, boolean needsNumberOfGateway) {
		super(name, needsNumberOfGateway);
		this.weights = new LinkedHashMap<>();
		this.comperator = new WeightComperator();
	}

	/**
	 * Returns the currently stored weight of the given node
	 * 
	 * @param node
	 * @return
	 */
	protected double getWeight(INodeID node) {
		return weights.get(node);
	}

	/**
	 * Update the weight of a given node
	 * 
	 * @param nodeId
	 * @param weight
	 */
	protected void setWeight(INodeID nodeId, double weight) {
		weights.put(nodeId, weight);
	}

	protected abstract void calculateWeights(List<INodeID> forNodes);

	/**
	 * Just a comparator to sort the nodes by their weight.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private class WeightComperator implements Comparator<INodeID> {

		@Override
		public int compare(INodeID o1, INodeID o2) {
			return Double.compare(getWeight(o1), getWeight(o2));
		}

	}

}
