package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.GridDensityClusterer;

/**
 * Implementation of {@link CD_LAT} using the Grid method for clustering
 * 
 * @author Jonas Huelsmann
 *
 */
public class CD_LAT_Grid extends CD_LAT {

	public CD_LAT_Grid() {
		super(false);
	}

	/**
	 * Method used to build the clusters
	 * 
	 * @param nodes
	 *            A list of all nodes
	 * @return A list containing lists with INodeIDs representing the clusters
	 */
	@Override
	protected List<List<INodeID>> calculateCluster(List<INodeID> nodes) {
		Map<INodeID, Location> curLocations = positionsHolder.getLast();

		clusterer = new GridDensityClusterer();
		// clusterer = new DBScanClusterer(TempClusteringConfig.minPTS);
		// clusterer = new KppMeansClusterer(maxNumberOfGateways);

		List<List<INodeID>> cluster = new ArrayList<List<INodeID>>();
		cluster.addAll(clusterer.getClusters(curLocations));

		return cluster;
	}

}
