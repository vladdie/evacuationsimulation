package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.DkC;

public class DkC_HID extends DkC {

	public DkC_HID() {
		super("DkC_HID");
	}

	@Override
	protected void calculateWeights(List<INodeID> forNodes) {
		for (INodeID v : forNodes) {
			setWeight(v, v.value());
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		return Collections.emptyMap(); // No SiS interaction is required
	}

}
