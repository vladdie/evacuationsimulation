package de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc;

import java.util.LinkedList;

@SuppressWarnings("serial")
/**
 * Helper class to the a stack with a limited size. The oldest entry will be deleted.
 * 
 * @author Michael Walter
 *
 * @param <E>
 */
public class LimitedQueue<E> extends LinkedList<E> {

	private int maxSize;

	/**
	 * Constructor
	 * 
	 * @param limit
	 *            - max size of this collection
	 */
	public LimitedQueue(int limit) {
		this.maxSize = limit;
	}

	@Override
	public boolean add(E o) {
		boolean added = super.add(o);
		while (added && size() > maxSize) {
			super.removeFirst();
		}
		return added;
	}

}
