# What it is, and how to use it
The decentralized gateway selection (DGS) is a node based algorithm for ClusterHead (CH) selection and clustering.
It tries to determine the next CH and disseminate that knowledge in the network.
Interested parties can implement the `GatewaySelectionListener`-Interface and use

```java
DecentralGatewaySelection.addGatewaySelectionListener(listener);
```

Registered Listeners will be informed of the new Node- *Role* and the `INodeID` of the CH, each time the algorithm finished.
A new CH selection can only be started if the DGS `isOnline()`. This means that it was set to `startMrvhsnidm()`, and that the Node's `NetInterface` `isUp()`. Not to be confused with `isActive()`, which signals that the DGS is currently active at selecting a new CH.
To start a new Selection round manually from the outside, call

``` java
DecentralGatewaySelection.getTrigger().onTriggeredFromOutside();
```

To exchange SubComponents of the DGS via the `TransitionEngine`, call 

``` java
DecentralGatewaySelection.ComponentTransition(DGSComponent comp, Class<T> nextInstance);
```

with the appropriate `DGSComponent` enum and Class.

## Backround on Decentral Clusterhead selection
Most algorithms fall into one of two Categories. Either they probabilistically let Nodes declare themselves CH, and disseminate that Knowledge so other nodes in range join that CH and form a Cluster. The other method is for nodes to compute a utility or *weight*, and disseminate that in a first step. After that, the node with the best weight is selected to act as a CH for the cluster.
Both of these methods can be performed by this DGS. Because of its modularity, the computation of the weight or probability to become CH can easily switched out. As can the Dissemination or any other part besides the main DGS Component.


# DGS Components
The DGS consists of a number of mostly independent sub-Components. Configuration of all these is done via the `DGSComponentFactory`, which holds working default values for all instances. An Enumeration of all `DGSSubComponents` is used to map names to classes, and should be updated with new Components as they arise.
All following config call notations are the XML-pendant, meaning that they are missing the leading *set*, prepended by the XML-parser.

## The DGS Main Component
Currently there exists only the `defaultDGS`. This class is acts as a container for all other subComponents, and has responding getters for each of them.
The node's last *Role* and *clusterID*, as well as the current DGS- *round* can also be requested here via appropriate getters. 
Furthermore, the DGS main component holds a reference to the `UDP` `TransportComponent`-protocol, to be used by the `LocalDissemination`, and the connectivityListener. The main DGS component must act as a `TriggerListener`. When triggered it gets a *weight* from the used `WeightComputation` using the required information, and starts the `DGSScheme` with that *weight*.
The main Component also acts a MessageListener, relaying relevant messages to the `DGSScheme` and the `DecentralClusterer`, as well as informing the `Trigger` of new Clustering Attempts signaled by such messages.

## The Trigger
The Trigger is a simple Trigger (d'oh!), informing all `TriggerListener`s when triggered. There are two implementations: The `NoLogicTrigger` is just that, it is supposed to work as an Eventbased trigger, to be called by workApps as described above. The `TimerBasedTrigger` has an internal timer and triggers periodically.
Which Trigger to use as default is set with `TriggerClassName(String name)` in the factory. It expects the Class' simple name.
The *interval* of the TimerBasedTrigger is set with using `TimedTriggerInterval(time)`
It can work in two different modi:
If `TriggerUseSingletonTimer(true)`, all nodes use the same timer, and trigger at the exact same moment. This is intended for testing purposes and debugging.
Otherwise each node's trigger has its own timer. These are set to trigger at each *interval* Simulatortime, with a random offset of up to 20% of the set *interval*.

## The WeightComputation
The `WeightComputation` is responsible for computing a *probability* or *weight* for a node to become CH. If these *weight*s are handled as probability or as utility- value is controlled by the `DGSScheme`, the *Weighter* is only there to generate the value in the first place.
Each *Weighter* has a `getRequiredInformationTypes()` method, that returns a List with information required to do that.
The `calculateWeight(*args*, *kwargs*)` method expects these information, in the exact same order, as *args* -List. The *kwargs* argument is supposed to hold additional config-options for the weighter, but currently unused.
Computation falls into one of two categories: Probabilistic, using a base probability, maybe refining it with additional information; or Weight-based, weighting information about the node and its environment to come up with at utility.
Which *weighter* is used as default is set via `WeightComputationClassName(String name)`, expecting the class' simple name.
Further information about the source of these algorithms can be found in the corresponding javadoc
### Probabilistic WeightComputation
All current probabilistic *Weighters* are based of the **LEACH**-algorithm. These are:
* `LEACH`
* `ALEACH`
* `DEEC`

All those share the same `LeachBaseProbability(probability)` setter. When exchanging components, this value will become the inverse of *IdealClustersize*. DEEC also has the option to work with (default) or without using Monitoring information about other nodes. To set that, use `DEECUseMonitoring(boolean)`
### Utility based WeightComputation
These are:
* `Scaled_WCA`
* `CEMCA`
* `EnLat` (taking only Energy reserves and Cellular Latency into account)

For both CECMA and Scaled_WCA an `IdealClustersize(int)` can be set. When exchanging the *Weighter* with *LEACH*-based one, this will be transfered as the inverse *LeachBaseProbability*. The EnLat algorithm also has an `MaxCellLatency(time)` settter. Nodes that have a higher latency than that value are disregarded as CH.

## The DGS -Scheme
The DGSScheme implements the actual algorithm *logic*. It is what decides weather the given *weight*s are used as probability or utility -value for CH-selection. There are two implementations, one for each approach. The default class can be set using `SchemeClassName(String name)`. Both Versions have a *radius*, which governs the number of hops the scheme tries to contact or build a Cluster for. While this value is a *static* and independent from the one set in the other version, both are set via the factory's `SchemeMessageRadius(int)` setter.

* `SOneStep` (Stochastic one Step Scheme) selects a Node as CH if the given *weight* is larger than a random value. If so, it disseminates that information via the `LocalDisseminator`. If the node was not selected as CH, the Scheme just weights for a given time or until it receives a `DeclareCHMessage`. The Timeout can be set using `setSinglePhaseSchemeTimeout(time)`.

* `DTwoSteps` (Deterministic two Step Scheme) disseminates the best *weight* it is aware of. The duration of this phase can be set using `DSchemeStep1Timeout(time)`. If the best known *weight* after this time belongs to the node itself, it becomes CH and broadcasts this fact. Elsewise, the Scheme waits up until `DSchemeStep2Timeout(time)` for a `DeclareCHMessage`.

## The LocalDissemination
The LocalDissemination class is deciding if and when a given message is disseminated. Which version is used as default can be set using `DisseminationClassName(name)`. There are currently three different versions available, all of which have a basic Duplicate Detection, based on a message's `UniqueID` in common.
* `Flooding` just broadcasts messages immediately.
* `ContentionBasedDissemination` waits up to a given `MaxDisseminationContention(time)` before sending. The contention is based on the node's energy reserves. If the Dissemination overhears another node forwarding the same message before the end of the contention time, it discards the message.
* `ProbabilisticDissemination` sends messages with a `DisseminationProbability(probability)`. To ensure that messages get at least to nodes 2 hops away, `AlwaysSendFirstHop(boolean)` defaults to true.

## The Clusterer
The Clusterer's job is to choose a cluster to join, when the node is not a CH itself. The CH-selection itself can work completely without clusterer.  There are currently two very basic clusterers, which can be set as default using `ClustererClassName(name)`.
* The `FirstHeardClusterer` just joins the first cluster that it heard a clusterID of.
* `ClosestNodeClusterer` joins the cluster of which a message was received with the least hops.

## Final Remarks
While all the modules are compatible with another, please keep in mind that the utility -based WeightComputations usually generate a much higher *weight* than the probabilistic ones. This means that a Stochastic scheme will then declare much more nodes to be CH than what might be intended.
### Integration in the `GatewaySelection`
While the DGS can operate alone, its integration as `GatewaySelectionClient` means it can be controlled via `DGSConfigurationMessage`s from the Server.
The new config option `informServe(boolean)` controls whether or not to inform the server of a node's *NodeRole*.

### List of Config options:
These can be found in the `DGSComponentFactory`. Basically all of them are statics, that should be set once before using the DGS.
*MethodName (without prepended *set*): fieldType, defaultValue, description *
 
#### Main DGS Configuration
* DGSMaxFallbackContention: long, 10s. How long the `DecentralGatewaySelectionClients` fallback selection mechanism runs, before returning a `NodeRole`.
* RemoteStartDelay: long, 10ms. How long to wait before going active, if a Configuration Message was received. This is to be used, so other Instances have time enough to also receive these messages and exchange components accordingly.
* InformServer: boolean, false. Set if the server component should be informed after a GatewaySelection
* ClustererClassName: String, FIRSTHEARDCLUSTERER. Defines the startup clusterer subcomponent.
* DisseminationClassName: Sting, FLOODING. Defines the startup Dissemination subcomponent.
* SchemeClassName: String, SONESTEP. Defines the startup DGSScheme subcomponent.
* TriggerClassName: String, TIMERBASEDTRIGGER. Defines the trigger subcomponent which is used to when initializing the DGS.
* WeightComputationClassName: String, ALEACH. Defines the startup WeightComputation subcomponent.

#### Dissemination options:
* MaxDisseminationContention: long, 3s. Max waiting time for the `ContentionBasedDissemination`, before a message is sent.
* DisseminationProbability: double, 0.75. Probability used by the `ProbabilisticDissemination`, when deciding wether to forward a message.
* AlwaysSendFirstHop: boolean, true. Set to True, if the `ProbabilisticDissemination` should always forward messages until reaching the second hop. (Otherwise the usual probability is used there.)

#### Scheme options:
* SchemeMessageRadius: int, -1. Amount of hops that Scheme messages (like CH selection messages) are allowed to travel. Negative values are treated as infinity.
* SinglePhaseSchemeTimeout: long, 20s. Time the stochastic `SOneStep` Scheme listens for better Clusterheads before finishing.
* DSchemeStep1Timeout: long, 30s. Time the deterministic `DTwoSteps` listens for (and forwards) *weights* better than the current one.
* DSchemeStep2Timeout: long, 15s. Time the stochastic `DTwoSteps` Scheme listens for a Clusterhead message.

#### Trigger options:
* TimedTriggerInterval: long, 5m. Interval that the `TimerBasedTrigger` waits between triggering a gateway selection.

#### WeightComputation options:
* LeachBaseProbability: double, 0.1. Base Probability to become a Gateway, for all Leach based Algorithms. This is basically the inverse of the intended Cluster -size.
* IdealClustersize: int, 10. The ideal number of neighbours used by the `CEMCA` and `Scaled_WCA` classes when determining weight. Should be the inverse of LeachBaseProbability.
* DEECUseMonitoring: boolean, true. Set to false, to disable `DEEC`s access to the `SiS` for weight computation.
* MaxCellLatency: long, 5ms. Sets the maximum cellular latency the `EnLat` mechanism is willing to cope with. Nodes with a higher latency will not be selected as CH.