package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.sec;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.SEC;

public class SEC_ALEACH extends SEC {

	public SEC_ALEACH() {
		super("SEC_ALEACH");
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		// Get Parameters
		double k = maxNumberOfGateways;
		double N = forNodes.size();

		// Find a k for full rounds
		double kNew = N;
		double div = 1;
		while (kNew > k) {
			kNew = N / ++div;
		}
		k = kNew;

		double lastXRounds = N / k; // 1/p with p = k/N


		// Calculate Probability
		for (INodeID v : forNodes) {
			Long lastRoundAsGateway = lastTimeGateway.get(v);
			if (lastRoundAsGateway == null || lastRoundAsGateway < round - lastXRounds) {
				double t_v;
				try {
					double energylevel = getValue(v, typeEnergylevel);
					t_v = k / (N - k * (round % (N / k))) + (energylevel / 100) * (k / N);
				} catch (InformationNotAvailableException e) {
					System.out.println("No Information available.");
					t_v = 0;
				}
				setProbability(v, t_v / 2);
			} else {
				setProbability(v, 0.0);
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw Data
		typeEnergylevel = SiSTypes.getType("ENERGY_BATTERY_LEVEL", Double.class);
		types.put(typeEnergylevel, SiSRequest.NONE);

		return types;
	}

}
