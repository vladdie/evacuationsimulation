package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;

public class DGSConfigurationMessage implements Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7781678851626155919L;

	private boolean startGatewaySelectionImmediately = false;

	private List<DGSSubComponents> dgsComponents;

	public DGSConfigurationMessage(boolean startGatewaySelectionImmediately, List<DGSSubComponents> dgsComponents) {
		this.startGatewaySelectionImmediately = startGatewaySelectionImmediately;
		this.dgsComponents = dgsComponents;
	}

	public DGSConfigurationMessage(DGSConfigurationMessage original) {
		this.startGatewaySelectionImmediately = original.isStartGatewaySelectionImmediately();
		this.dgsComponents = original.getDGSSubComponents();
	}

	@Override
	public long getSize() {
		long size = DGSSubComponents.getTransmissionSize() * dgsComponents.size() + 1;
		return size;
	}

	@Override
	public Message getPayload() {
		return this;
	}

	public boolean isStartGatewaySelectionImmediately() {
		return startGatewaySelectionImmediately;
	}

	public List<DGSSubComponents> getDGSSubComponents() {
		return dgsComponents;
	}

}
