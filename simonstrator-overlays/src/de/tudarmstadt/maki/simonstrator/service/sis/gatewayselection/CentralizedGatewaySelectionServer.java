package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.AAdvGatewaySelection;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.AAdvGatewaySelection.GatewayLogicBehavior;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.GatewaySelectionStrategy;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.StrategyImplementations;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc.DkC_ALEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.CentralGatewaySelectionMessage;

public class CentralizedGatewaySelectionServer implements GatewaySelectionServer {

	public static final int cellularCGSPort = 373;

	/**
	 * Whether to inform all Nodes of the new gateway selection, only new and
	 * previous gateways, or no node at all
	 */
	private static enum informNodes {
		ALL, GATEWAYS, NONE
	};

	/**
	 * Sets what {@link CentralGatewaySelectionClient}s should be informed about
	 * a new Gateway selection. <br>
	 * possible levels are: <br>
	 * <b>ALL</b> informs all nodes of their changed gateway affiliation and
	 * {@link NodeRole}<br>
	 * <b>GATEWAYS</b> informs only current and previous Gateways that their
	 * {@link NodeRole} changed. No information about the new gateway is
	 * transmitted to leafs.<br>
	 * <b>NONE</b> No information is send to the clients. Use this if the
	 * application takes care of informing the clients itself.
	 */
	private static final informNodes informNodesLevel = informNodes.ALL;

	private static final GatewayLogicBehavior behaviour = GatewayLogicBehavior.SUGGESTION;

	// FIXME alter this to use a different default implementation
	private Class<? extends GatewaySelectionStrategy> defaultStrategy = DkC_ALEACH.class;

	private static final String centralStrategyName = "serverGatewaySelectionStrategy";

	@TransferState({ "Host" })
	private Host host;

	@TransferState({ "CurrentGateways" })
	private Map<INodeID, List<INodeID>> currentGateways;

	@TransferState({ "Components" })
	/**
	 * Map of all components that were last in use with either Centralized or
	 * Decentralized GatewaySelection. Used to keep track of these between
	 * transitions. Should be easier to do here than in the Controller.
	 */
	private Map<String, String> components;

	private TransitionEngine transitionEngine = null;

	private MessageBasedTransport cellularTransport = null;

	private GatewaySelectionStrategy centralStrategy = null;


	@TransferState({ "Host" })
	public CentralizedGatewaySelectionServer(Host host) {
		this.host = host;

		// setup/get the component
		try {
			transitionEngine = host.getComponent(TransitionEngine.class);
		} catch (ComponentNotAvailableException e) {
			// fail! RuntimeException as this may very well be called outside of
			// the TransitionEngine
			throw new RuntimeException("No TransitionEngine available at Server!", e);
		}

		currentGateways = new HashMap<INodeID, List<INodeID>>();
		components = new HashMap<String, String>();

		if (informNodesLevel != informNodes.ALL) {
			System.err.println("CentralizedGAtewaySelection informs " + informNodesLevel + "!");
		}
	}

	@Override
	public void startMechanism(Callback cb) {
		try {
			cellularTransport = host.getTransportComponent().getProtocol(UDP.class,
					host.getNetworkComponent().getByName(NetInterfaceName.MOBILE).getLocalInetAddress(),
					CentralizedGatewaySelectionServer.cellularCGSPort);

			if (transitionEngine.proxyExists(centralStrategyName)) {
				this.centralStrategy = transitionEngine.getProxy(centralStrategyName, GatewaySelectionStrategy.class);

				// FIXME is this right?
				centralStrategy.startMechanism(new Callback() {
					@Override
					public void finished(boolean successful) {
						// should not (cannot!) fail, nothing to do here
					}
				});
			} else {

				GatewaySelectionStrategy defaultInstance = defaultStrategy.newInstance();
				defaultInstance.setSiS(host.getComponent(SiSComponent.class));
				if (defaultInstance instanceof AAdvGatewaySelection) {
					((AAdvGatewaySelection) defaultInstance).setBehaviour(behaviour);
				}

				centralStrategy = transitionEngine.createMechanismProxy(GatewaySelectionStrategy.class,
						defaultInstance, centralStrategyName);

				components.put(centralStrategyName, defaultStrategy.getSimpleName().toUpperCase());
			}

		} catch (ProtocolNotAvailableException | InstantiationException
				| IllegalAccessException | ComponentNotAvailableException e) {
			cb.finished(false);
		}

		cb.finished(true);
	}

	@Override
	public Map<INodeID, List<INodeID>> getGateways(List<INodeID> outOf, Graph dataGraph, int maxNumberOfGateways) {

		Map<INodeID, List<INodeID>> newGateways = centralStrategy.getGateways(outOf, dataGraph, maxNumberOfGateways);

		informNodes(newGateways);
		currentGateways = newGateways;

		return new HashMap<INodeID, List<INodeID>>(newGateways);
	}

	/**
	 * Inform the selected nodes about their changed affiliation or NodeRole
	 * 
	 * @param newGateways
	 */
	private void informNodes(Map<INodeID, List<INodeID>> newGateways) {

		switch (informNodesLevel) {
		case ALL:
			// re-Map the old mapping
			Map<INodeID, INodeID> perNodeOldGateway = new HashMap<INodeID, INodeID>();
			for (INodeID oldGateway : currentGateways.keySet()) {
				perNodeOldGateway.put(oldGateway, oldGateway);
				for (INodeID leaf : currentGateways.get(oldGateway)) {
					perNodeOldGateway.put(leaf, oldGateway);
				}
			}

			for (INodeID gateway : newGateways.keySet()) {
				if (!gateway.equals(perNodeOldGateway.get(gateway))) {
					sendGatewayMessage(gateway, gateway, NodeRole.CLUSTERHEAD);
				}
				for (INodeID newLeaf : newGateways.get(gateway)) {
					if (!gateway.equals(perNodeOldGateway.get(newLeaf))) {
						sendGatewayMessage(newLeaf, gateway, NodeRole.CLUSTERMEMBER);
					}
				}
			}
			break;
		case GATEWAYS:
			// inform new gateways
			for (INodeID gateway : newGateways.keySet()) {
				if (!currentGateways.containsKey(gateway)) {
					// new gateway!
					sendGatewayMessage(gateway, gateway, NodeRole.CLUSTERHEAD);
				}
			}
			// inform old gateways
			currentGateways.keySet().removeAll(newGateways.keySet());
			for (INodeID oldGateway : currentGateways.keySet()) {
				sendGatewayMessage(oldGateway, null, NodeRole.CLUSTERMEMBER);
			}
			break;
		default:
			return;
		}
	}

	private void sendGatewayMessage(INodeID receiver, INodeID gateway, NodeRole nodeRole) {
		CentralGatewaySelectionMessage msg = new CentralGatewaySelectionMessage(nodeRole, gateway);
		cellularTransport.send(msg, Oracle.getHostByID(receiver).getNetworkComponent()
				.getByName(NetInterfaceName.MOBILE).getLocalInetAddress(),
				CentralizedGatewaySelectionServer.cellularCGSPort);
	}

	@Override
	public void stopMechanism(Callback cb) {

		this.host = null;
		currentGateways = null;
		components = null;
		transitionEngine = null;
		cellularTransport = null;

		// FIXME is this correct?
		centralStrategy.stopMechanism(new Callback() {
			@Override
			public void finished(boolean successful) {
				// should not (cannot!) fail, nothing to do here
			}
		});

		centralStrategy = null;
		cb.finished(true);
	}

	@Override
	public Map<INodeID, List<INodeID>> getCurrentGateways() {
		return currentGateways;
	}

	public void setCurrentGateways(Map<INodeID, List<INodeID>> currentGateways) {
		this.currentGateways = currentGateways;
	}

	@Override
	public void setComponent(String simpleName) {
		simpleName = simpleName.toUpperCase();
		if (!components.get(centralStrategyName).equals(simpleName)) {
			// Only do a transition if the new component is really new
			Class<? extends GatewaySelectionStrategy> targetClass = StrategyImplementations.valueOf(simpleName)
					.getImplementingClass();

			transitionEngine.executeAtomicTransition(centralStrategyName, targetClass);

			components.put(centralStrategyName, simpleName);
		}

	}

	public Map<String, String> getComponents() {
		return components;
	}

	public void setComponents(Map<String, String> components) {
		this.components = components;
	}

	@Override
	public Message getConfigMessageFor(INodeID node) {
		// There is nothing to configure or sent...
		return null;
	}

	@Override
	public Host getHost() {
		return host;
	}
}
