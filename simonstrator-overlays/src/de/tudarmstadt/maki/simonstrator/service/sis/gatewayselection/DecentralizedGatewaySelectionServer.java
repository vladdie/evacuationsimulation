package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.DGSConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.DGSServerUpdateMessage;

/**
 * 
 * @author Nils Richerzhagen
 *
 */
public class DecentralizedGatewaySelectionServer implements GatewaySelectionServer, TransMessageListener, EventHandler {

	public static final int cellularDGSPort = 4719;

	private static final boolean selectGatewaysAfterStart = true;
	private static final boolean selectGatewaysAfterComponentChange = true;
	private static final boolean startSelectionUponGetGateways = false;
	// if true, a message that starts anew gateway selection is sent whenever
	// getGateways is called;

	// This should be long enough to give all nodes enough time to do the
	// transition to decentralized client.
	private static final long afterTransitionWait = 500 * Time.MILLISECOND;

	// -------- Do not alter variables below this point -------- \\

	// this is a sort of counter, used to set the according bool in the config
	// message
	private boolean startGatewaySelection = false;

	private static int startSendConfigMessagesEvent = 001;

	// allow only one ServerInstance...
	private static NetID serverNetID;

	@TransferState({ "Host" })
	private Host host;

	@TransferState({ "CurrentGateways" })
	// FIXME I'm not sure if transitions will function with this little 'cheat'
	// Basically this mapping nodes to nodes, instead of to lists
	private Map<INodeID, INodeID> currentGateways;

	@TransferState({ "Components" })
	/**
	 * Map of all components that were last in use with either Centralized or
	 * Decentralized GatewaySelection. Used to keep track of these between
	 * transitions. Should be easier to do here than in the Controller.
	 */
	private Map<String, String> components;

	MessageBasedTransport cellularTransport;

	/**
	 * To keep track on whether we need to trigger/inform the DGS-clients
	 */
	boolean componentsChanged = false;

	@TransferState({ "Host" })
	public DecentralizedGatewaySelectionServer(Host host) {
		this.host = host;
		if (serverNetID == null) {
			serverNetID = host.getNetworkComponent().getByName(NetInterfaceName.MOBILE).getLocalInetAddress();
		}
		// that's it?

		// Config printout...
		String settings = "DecentralizedGatewaySelection";
		String forced = " forces a new Gateway selection";
		String inHere = "";
		boolean setSomething = false;
		if (selectGatewaysAfterStart) {
			inHere = "start";
			setSomething = true;
		}
		if (selectGatewaysAfterComponentChange) {
			if (setSomething) {
				inHere += ", ";
			}
			inHere += "ComponentChanges";
			setSomething = true;
		}
		if (startSelectionUponGetGateways) {
			if (setSomething) {
				inHere += ", ";
			}
			inHere += "getGateways";
			setSomething = true;
		}

		if (setSomething) {
			forced += " at ";
		} else {
			settings += " never";
		}
		Monitor.log(DecentralizedGatewaySelectionServer.class, Level.INFO, "%s %s %s!", settings, forced, inHere);
		// System.out.println(settings + forced + inHere + "!");
	}

	@Override
	public void startMechanism(Callback cb) {
		if (currentGateways == null) {
			currentGateways = new HashMap<INodeID, INodeID>();
		}
		if (components == null) {
			components = new HashMap<String, String>();
			// TODO defaultComponents?
		}
		try {
			cellularTransport = host.getTransportComponent().getProtocol(UDP.class,
					serverNetID, cellularDGSPort);
			cellularTransport.setTransportMessageListener(this);
		} catch (ProtocolNotAvailableException e) {
			cb.finished(false);
		}

		componentsChanged = true;
		this.startGatewaySelection = selectGatewaysAfterStart;
		Event.scheduleWithDelay(afterTransitionWait, this, null, startSendConfigMessagesEvent);

		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		host = null;
		currentGateways = null;
		components = null;
		componentsChanged = false;

		cellularTransport.removeTransportMessageListener();
		cellularTransport = null;

		cb.finished(true);
	}

	@Override
	public Map<INodeID, List<INodeID>> getGateways(List<INodeID> outOf, Graph dataGraph, int maxNumberOfGateways) {
		// Send Message to start DGS or don't?
		// Return null or an empty map?

		if (startSelectionUponGetGateways) {
			// or only send messages to 'outOf'?
			sendConfigMessages(true);
		}
		return mapPerGateway(currentGateways);
	}

	@Override
	public Map<INodeID, List<INodeID>> getCurrentGateways() {
		// this may be outdated
		return this.mapPerGateway(currentGateways);
	}

	public void setCurrentGateways(Map<INodeID, List<INodeID>> currentGateways) {
		this.currentGateways = mapPerNode(currentGateways);
	}

	@Override
	public void setComponent(String simpleName) {
		simpleName = simpleName.toUpperCase();
		String type = DGSSubComponents.valueOf(simpleName).getType().name();
		if (!simpleName.equals(components.get(type))) {
			components.put(type, simpleName);

			this.startGatewaySelection = startGatewaySelection || selectGatewaysAfterComponentChange;
			// because this is likely called from inside a loop...
			if (!componentsChanged) {
				componentsChanged = true;
				Event.scheduleImmediately(this, null, startSendConfigMessagesEvent);
			}
		}
	}

	private void sendConfigMessages(boolean startSelection) {
		componentsChanged = false;
		startGatewaySelection = false;

		// if (startSelection) {
		// currentGateways.clear();
		// }

		DGSConfigurationMessage template = generateConfigurationMessage(startSelection);

		Monitor.log(DecentralizedGatewaySelectionServer.class, Level.DEBUG, "New Configuration is: %s",
				template.getDGSSubComponents());

		for (NetInterface connection : BootstrapEntity.getAllNetConnections()) {
			if (connection.isUp()) {
				cellularTransport.send(new DGSConfigurationMessage(template), connection.getLocalInetAddress(),
						cellularDGSPort);
			}
		}
	}

	/**
	 * creates a config message. These always include the complete set of DGS
	 * Components, as otherwise we cannot guarantee that nodes have the same
	 * components running.
	 */
	private DGSConfigurationMessage generateConfigurationMessage(boolean startSelection) {
		LinkedList<DGSSubComponents> list = new LinkedList<DGSSubComponents>();
		for (String compType : components.keySet()) {
			try {
				DGSSubComponents comp = DGSSubComponents.valueOf(components.get(compType));
				list.add(comp);
			} catch (IllegalArgumentException e) {
				// that should have been the central Strategy Component
				continue;
			}
		}
		return new DGSConfigurationMessage(startSelection, list);
	}

	public Map<String, String> getComponents() {
		return components;
	}

	public void setComponents(Map<String, String> components) {
		this.components = components;
	}

	@Override
	public Host getHost() {
		return host;
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		if(msg instanceof DGSServerUpdateMessage){
			DGSServerUpdateMessage update = (DGSServerUpdateMessage) msg;
			currentGateways.put(update.getSender(), update.getGateway());
		}
	}

	/**
	 * Helper function that translates mappings per gateway into mappings per
	 * node
	 * 
	 * @param mapPerGateway
	 * @return
	 */
	private Map<INodeID, INodeID> mapPerNode(Map<INodeID, List<INodeID>> mapPerGateway) {
		Map<INodeID, INodeID> mapPerNode = new HashMap<INodeID, INodeID>();
		for (INodeID gateway : mapPerGateway.keySet()) {
			mapPerNode.put(gateway, gateway);
			for (INodeID leaf : mapPerGateway.get(gateway)) {
				mapPerNode.put(leaf, gateway);
			}
		}
		return mapPerNode;
	}

	/**
	 * HelperFunction that translates mappings per node into mappings per
	 * Gateway
	 * 
	 * @param mapPerNode
	 * @return
	 */
	private Map<INodeID, List<INodeID>> mapPerGateway(Map<INodeID, INodeID> mapPerNode) {
		Map<INodeID, List<INodeID>> mapPerGateway = new HashMap<INodeID, List<INodeID>>();

		for (INodeID node : mapPerNode.keySet()) {
			INodeID gateway = mapPerNode.get(node);

			List<INodeID> list;
			if (mapPerGateway.containsKey(gateway)) {
				list = mapPerGateway.get(gateway);
			} else {
				list = new LinkedList<INodeID>();
				mapPerGateway.put(gateway, list);
			}
			if (!node.equals(gateway)) {
				list.add(node);
			}
		}

		return mapPerGateway;
	}

	/**
	 * Returns the NetID of this (the only!) Server/Cloud instance.
	 * 
	 * @return
	 */
	public static NetID getServerNetID() {
		return serverNetID;
	}

	@Override
	public Message getConfigMessageFor(INodeID node) {

		DGSConfigurationMessage msg = generateConfigurationMessage(false);

		return msg;
	}

	@Override
	public void eventOccurred(Object content, int type) {
		if (componentsChanged && type == startSendConfigMessagesEvent) {
			sendConfigMessages(startGatewaySelection);
		}
	}

}
