package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.BasicGraph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.IEdge;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.D1C;

/**
 * Implementation of the D1C Gateway selection algorithm {@see
 * Gateway-Selecktion in mobilen Multi-Hop-Netzwerken. Michael Walter} sorting
 * the nodes by latency and neighborhood density
 * 
 * @author Jonas Huelsmann
 *
 */
public class D1C_LAT extends D1C {

	// weighs
	double w_connectivity = 0.5;
	double w_cellular = 0.5;

	public D1C_LAT() {
		super("D1C_LAT", false);
	}

	/**
	 * Method used to acquire global simulator knowledge
	 * 
	 * @return A Map containing the requested information
	 */
	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		// Raw Infos
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();
		// get the cellular latencies
		typeLatency = SiSTypes.LATENCY_CELL;
		types.put(typeLatency, SiSRequest.NONE);
		// get the nodes location
		typeLocation = SiSTypes.PHY_LOCATION;
		types.put(typeLocation, SiSRequest.NONE);

		// Higher informations
		positionsHolder = new LimitedQueue<>(2); // Positions
		neighborhoodGraph = new BasicGraph(); // Neighborhood
		normalizedLatencies = new HashMap<INodeID, Double>(); // Normalized
																// latencies

		return types;
	}

	/**
	 * Method used to attach weight to ever node according to their cellular
	 * network latency and their neighborhood
	 * 
	 * @param forNodes
	 *            List of INodeIDs that are taken into consideration
	 */
	@Override
	protected void calculateWeights(List<INodeID> forNodes) {
		// TODO get latency
		double latency = Double.MAX_VALUE;
		double weight = 0;
		for (INodeID v : forNodes) {
			latency = normalizedLatencies.get(v);
			if (latency > 1) {
				weight = 0;
			} else {
				// making the smallest the biggest
				latency = 1 - latency;
				weight = (w_cellular * latency)
						+ (w_connectivity * calculateLCC(v));
			}

			setWeight(v, weight);
		}
	}

	/**
	 * Method that calculates a weight representing the neighborhood of a node
	 * 
	 * @param node
	 *            The node whose neighborhood should be calculated
	 * @return A double representing the neighborhood density
	 */
	private double calculateLCC(INodeID node) {

		Set<String> edges = new HashSet<String>();
		Set<INodeID> neighbors = neighborhoodGraph.getNeighbors(node);
		for (INodeID neighbor : neighbors) {
			Set<IEdge> edgesOfNeighbor = neighborhoodGraph
					.getOutgoingEdges(neighbor);
			for (IEdge edge : edgesOfNeighbor) {
				long id1 = edge.fromId().value();
				long id2 = edge.toId().value();
				if (id1 < id2) {
					edges.add(id1 + "-" + id2);
				} else {
					edges.add(id2 + "-" + id1);
				}
			}
		}
		double N = neighborhoodGraph.getNodeCount();
		double maxPossibleLinks = N * (N - 1) / 2;
		double lcc = edges.size() / maxPossibleLinks;
		return lcc;
	}
}
