package de.tudarmstadt.maki.simonstrator.service.sis.clustering;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer.SiSConsumerHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;

public abstract class AGatewaySelectionStrategy implements GatewaySelectionStrategy {

	// Simulation
	@TransferState({ "SiS" })
	private SiSComponent sis;
	private List<SiSConsumerHandle> sisHandles = new LinkedList<>();
	private long cacheTimeout;
	protected Map<SiSType<?>, SiSValueCache<?>> caches;

	public AGatewaySelectionStrategy() {
		this.caches = new LinkedHashMap<>();
		setCacheTimeout(10 * Time.SECOND);
	}

	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////
	// Simulation //
	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////

	@Override
	public SiSComponent getSiS() {
		return sis;
	}

	@Override
	public void setSiS(SiSComponent sis) {
		this.sis = sis;
	}

	@Override
	public void startMechanism(Callback cb) {
		/*
		 * Request raw observations (once)
		 */
		Map<SiSType<?>, SiSRequest> metrics = getRequiredInformation();
		for (Map.Entry<SiSType<?>, SiSRequest> metric : metrics.entrySet()) {
			SiSConsumerHandle handle = sis.get().rawObservations(metric.getKey(), metric.getValue(), null);
			sisHandles.add(handle);
		}
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		for (SiSConsumerHandle handle : sisHandles) {
			sis.get().revoke(handle);
		}
		cb.finished(true);
	}

	/**
	 * Time for how long values provided by the SiS are cached locally.
	 * 
	 * @param cacheTimeout
	 */
	public void setCacheTimeout(long cacheTimeout) {
		this.cacheTimeout = cacheTimeout;
	}

	/**
	 * Get the value for the given Type
	 * 
	 * @param node
	 * @param type
	 * @return
	 * @throws InformationNotAvailableException
	 */
	protected <T> T getValue(INodeID node, SiSType<T> type) throws InformationNotAvailableException {
		@SuppressWarnings("unchecked")
		SiSValueCache<T> cache = (SiSValueCache<T>) caches.get(type);
		if (cache == null) {
			cache = new SiSValueCache<T>(type, cacheTimeout);
			caches.put(type, cache);
		}
		return cache.getValue(node);
	}

	private class SiSValueCache<T> {

		private SiSType<T> type;

		private long cacheTimeout;

		public SiSValueCache(SiSType<T> type, long cacheTimeout) {
			this.type = type;
			this.cacheTimeout = cacheTimeout;
		}

		private Map<INodeID, Long> lastUpdate = new LinkedHashMap<>();

		private Map<INodeID, T> cachedValues = new LinkedHashMap<>();

		public T getValue(INodeID node) throws InformationNotAvailableException {
			Long upd = lastUpdate.get(node);
			if (upd == null || upd + cacheTimeout < Time.getCurrentTime()) {
				lastUpdate.put(node, Time.getCurrentTime());
				cachedValues.put(node,
						getSiS().get().localObservationOf(node, type, getRequiredInformation().get(type)));
			}
			T value = cachedValues.get(node);
			if (value == null) {
				throw new InformationNotAvailableException();
			}
			return value;
		}
	}

}
