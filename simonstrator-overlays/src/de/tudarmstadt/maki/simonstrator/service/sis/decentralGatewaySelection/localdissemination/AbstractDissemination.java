package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination;

import java.lang.reflect.InvocationTargetException;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithUniqueID;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.MessageWithHopCount;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.MessageWithTTL;

public abstract class AbstractDissemination implements LocalDissemination {

	private static int historySize = 30;

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	@TransferState({ "HeardMessages" })
	private LimitedQueue<UniqueID> heardMessages;

	public AbstractDissemination(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
		heardMessages = new LimitedQueue<UniqueID>(historySize);
		if (parent != null) {
			init();
		}
	}

	@Override
	public void onMessageReceived(Message msg) {

		if (msg instanceof MessageWithUniqueID) {
			UniqueID id = ((MessageWithUniqueID) msg).getUniqueMsgID();
			if (heardMessages.contains(id)) {
				// already heard this.
				return;
			} else {
				heardMessages.add(id);
			}
		}

		try {
			msg = msg.getClass().getConstructor(msg.getClass()).newInstance(msg);
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (msg instanceof MessageWithTTL) {
			// Not sure if this is not better done by the Msg constructor...
			int ttl = ((MessageWithTTL) msg).getTTL();
			if (ttl > 1) {
				((MessageWithTTL) msg).setTTL(ttl - 1);
			} else if (ttl == 0) {
				return;
			}
			// Negative ttl means no ttl
		}
		if (msg instanceof MessageWithHopCount) {
			((MessageWithHopCount) msg).increaseHopcount();
		}
		disseminate(msg);
	}

	protected void sendMessage(Message msg, NetID receiver) {
		if (parent != null && parent.getTransport().getNetInterface().isUp()) {
			parent.getTransport().send(msg, receiver,
					IDecentralGatewaySelectionClient.DGSPort);
		}
	}

	protected void startMechanism() {
		// nothing to do here?!
	}

	protected void stopMechanism() {
		heardMessages.clear();
		parent = null;
	}

	// ### TransferState stuff ###

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
		init();
	}

	public LimitedQueue<UniqueID> getHeardMessages() {
		LimitedQueue<UniqueID> newQueue = new LimitedQueue<UniqueID>(
				historySize);
		newQueue.addAll(heardMessages);
		return newQueue;
	}

	public void setHeardMessages(LimitedQueue<UniqueID> heardMessages) {
		this.heardMessages = heardMessages;
	}

	/**
	 * Initialization should be done in here, not in startMechanism!
	 */
	abstract void init();

}
