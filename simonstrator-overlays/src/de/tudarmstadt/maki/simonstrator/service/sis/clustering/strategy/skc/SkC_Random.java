package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.SkC;

public class SkC_Random extends SkC {

	protected Random rnd;

	public SkC_Random() {
		super("SkC_Random");
		rnd = Randoms.getRandom(SkC_Random.class);
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		for (INodeID v : forNodes) {
			setProbability(v, rnd.nextInt(forNodes.size()));
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		return Collections.emptyMap(); // No SiS interaction is required
	}

}
