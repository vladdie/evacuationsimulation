package de.tudarmstadt.maki.simonstrator.service.sis.minimal;

import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSDataCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationResolver;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;

/**
 * Provides an implementation of the {@link SiSInformationProvider} interface.
 * All business logic is implemented in the {@link SiSInformationResolver}.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class MinimalProvider implements SiSInformationProvider {

	private final SiSInformationResolver resolver;

	public MinimalProvider(SiSInformationResolver resolver) {
		this.resolver = resolver;
	}

	@Override
	public <T> SiSProviderHandle nodeState(SiSType<T> type,
			SiSDataCallback<T> dataCallback) {
		return resolver.addObservationProvider(type, dataCallback);
	}

	@Override
	public void revoke(SiSProviderHandle handle) {
		resolver.removeLocalProvider(handle);
	}

}
