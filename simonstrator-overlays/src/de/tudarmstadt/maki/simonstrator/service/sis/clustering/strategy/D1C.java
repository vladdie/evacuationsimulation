package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.BasicGraph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.ADeterministicGatewaySelection;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;

/**
 * 
 * @author Michael Walter
 *
 */
public abstract class D1C extends ADeterministicGatewaySelection {

	public D1C(String name, boolean needsNumberOfGateway) {
		super(name, needsNumberOfGateway);
	}

	@Override
	protected Map<INodeID, List<INodeID>> determineGateways(List<INodeID> outOf) {

		// Create tree and candidates list
		Map<INodeID, List<INodeID>> tree = new LinkedHashMap<>();
		LinkedList<INodeID> candidates = new LinkedList<>(positionsHolder.getLast().keySet());

		// Assign unknown nodes as gateways
		Set<INodeID> unknownNodes = new HashSet<INodeID>(outOf);
		unknownNodes.removeAll(candidates);
		for (INodeID node : unknownNodes) {
			tree.put(node, new LinkedList<INodeID>());
		}

		// D1C
		while (!candidates.isEmpty()) {

			// Calculate weights
			calculateWeights(candidates);

			// Determine GW
			Collections.sort(candidates, comperator); // Ordering: Lowest -> Highest
			INodeID newGW = candidates.removeLast(); // Select Highest

			// Assign gateway and leafs
			Set<INodeID> curNeighborhood = neighborhoodGraph.getNeighbors(newGW);
			tree.put(newGW, new LinkedList<INodeID>(curNeighborhood));

			// Remove gateway and leafs from the candidates list
			candidates.remove(newGW);
			neighborhoodGraph.removeNode(newGW);
			for (INodeID leafs : curNeighborhood) {
				candidates.remove(leafs);
				neighborhoodGraph.removeNode(leafs);
			}
		}

		return tree;
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationBySuperior() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw Infos
		typeLocation = SiSTypes.PHY_LOCATION;
		types.put(typeLocation, SiSRequest.NONE);

		// Higher Infos
		positionsHolder = new LimitedQueue<>(1); // Positions
		neighborhoodGraph = new BasicGraph(); // Neighborhood
		return types;
	}

}
