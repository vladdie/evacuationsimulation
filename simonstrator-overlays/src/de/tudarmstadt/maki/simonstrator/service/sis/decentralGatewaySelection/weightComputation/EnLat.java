package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer.AggregationFunction;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.AggregationNotPossibleException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.util.SiSRequestTripple;

/**
 * This weighting takes battery level and Latency into account
 * 
 * @author Christoph Storm
 * @version 2016.06.23
 */
public class EnLat implements WeightComputation {

	/*
	 * The value here is the LTE target latency as taken from table1 in
	 * "Downlink Packet Scheduling in LTE Cellular Networks: Key Design Issues and a Survey"
	 * by Capozzi et al.
	 * 
	 * -yeah, that was the first paper i found a value in, didn't want to
	 * search... :\
	 */
	private static double maxCellularLatency = 5 * Time.MILLISECOND;

	// Just for the TransitionEngine...
	@TransferState({ "BaseProbability" })
	private Double baseProbability = 0.1;

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	@TransferState({ "Parent" })
	public EnLat(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public void startMechanism(Callback cb) {
		// passive class
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		parent = null;
		cb.finished(true);
	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public List<SiSRequestTripple> getRequiredInformationTypes() {
		return new LinkedList<SiSRequestTripple>();
	}

	@Override
	public double calculateWeight(List<Object> args, List<Object> kwargs) {

		try {
			// get battery Level
			double batLevel = parent.getSis().get().localState(SiSTypes.ENERGY_BATTERY_LEVEL, new SiSRequest());
			batLevel *= 0.01;

			// get the cellular latency
			Map<INodeID, Double> allLatencies = parent.getSis().get().allLocalObservations(SiSTypes.LATENCY_CELL,
					new SiSRequest());
			double cellLatency = SiSTypes.LATENCY_CELL.aggregate(allLatencies.values(), AggregationFunction.AVG);
			
			if (cellLatency > maxCellularLatency || Double.isNaN(cellLatency)) {
				return 0;
			}
			
			cellLatency = cellLatency / maxCellularLatency;
			cellLatency = 1 - cellLatency;

			double weight = (batLevel + cellLatency) * 0.5d;
			return weight;

		} catch (InformationNotAvailableException | AggregationNotPossibleException e) {
			e.printStackTrace();
		}

		return 0;
	}

	/**
	 * set the maximum latency that is accepted for this node becoming a CH
	 * 
	 * @param latency
	 */
	public static void setMaxCellularLatency(double latency) {
		maxCellularLatency = latency;
	}

	@Override
	public void setBaseProbability(Double p) {
		assert p > 0 : "Probability must be larger than zero!";
		assert p <= 1 : "Probability must be smaller or equall to one!";
		baseProbability = p;
	}

	public Double getBaseProbability() {
		return baseProbability;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.ENLAT;
	}

}
