package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.D1C;

/**
 * Just a very simple test strategy, relying on a Bypass-Metric.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class D1C_HID extends D1C {

	public D1C_HID() {
		super("D1C_HID", false);
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		return Collections.emptyMap(); // No SiS interaction is required
	}

	@Override
	protected void calculateWeights(List<INodeID> forNodes) {
		for (INodeID v : forNodes) {
			setWeight(v, v.value());
		}
	}
}
