package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages;

public interface MessageWithHopCount {

	public int getHopcount();

	public void increaseHopcount();

}
