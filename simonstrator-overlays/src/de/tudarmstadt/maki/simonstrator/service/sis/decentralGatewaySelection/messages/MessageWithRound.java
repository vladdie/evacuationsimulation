package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;

public interface MessageWithRound extends Message {

	public int getRound();
}
