package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.util;

import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer.AggregationFunction;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;

public class SiSRequestTripple {

	private AggregationFunction aggr;
	private SiSType<Object> type;
	private SiSRequest request;

	public SiSRequestTripple(AggregationFunction aggr, SiSType<Object> type, SiSRequest request) {
		this.aggr = aggr;
		this.type = type;
		this.request = request;
	}

	public AggregationFunction getAggr() {
		return aggr;
	}

	public SiSType<Object> getType() {
		return type;
	}

	public SiSRequest getRequest() {
		return request;
	}
}
