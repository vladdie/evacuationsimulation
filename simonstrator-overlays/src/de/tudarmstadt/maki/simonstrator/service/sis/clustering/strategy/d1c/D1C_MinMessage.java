package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.D1C;

/**
 * Just a very simple test strategy, relying on a Bypass-Metric.
 * 
 * @author Bjoern Richerzhagen
 *
 */
@Deprecated
public class D1C_MinMessage extends D1C {

	public D1C_MinMessage() {
		super("D1C_MinMessage", false);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw data
		typeMessageSize = SiSTypes.getType("Delta_SizeBypassCloudMessageReceive", Double.class);
		types.put(typeMessageSize, SiSRequest.NONE);

		return types;
	}

	@Override
	protected void calculateWeights(List<INodeID> forNodes) {
		/*
		 * Just a sample. We assign weights based on only one SiS Metric
		 */
		for (INodeID v : forNodes) {
			try {
				setWeight(v, getValue(v, typeMessageSize));
			} catch (InformationNotAvailableException e) {
				setWeight(v, 0);
			}
		}
	}

}
