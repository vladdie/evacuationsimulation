package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;

/**
 * Listener for the Gateway Selection
 * 
 * @author Christoph Storm
 */
public interface GatewaySelectionListener {
	
	/**
	 * Called when the GatewaySelection was finished
	 * 
	 * @param Role
	 *            -the Role of this Node.
	 * @param Clusterhead
	 *            -INodeID of the Clusterhead. Can be <b>null</b> for certain
	 *            schemes.
	 */
	public void gatewaySelectionFinished(NodeRole role, INodeID clusterhead);

}
