package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithUniqueID;

public abstract class AbstractDGSMessage implements Message, MessageWithRound,
		MessageWithUniqueID {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int round;
	private UniqueID msgID;

	public AbstractDGSMessage(int round, UniqueID msgID) {
		this.round = round;
		this.msgID = msgID;
	}

	public AbstractDGSMessage(AbstractDGSMessage msg) {
		// just force the use of a copy constructor...
		round = msg.getRound();
		msgID = msg.getUniqueMsgID();
	}

	@Override
	public int getRound() {
		return round;
	}

	@Override
	public long getSize() {
		// round + uniqueID + extending messages Size
		return msgID.getTransmissionSize() + 4 + size();
	}

	/**
	 * Same behaviour as {@link #getSize}, just to force accurate size-info from
	 * all children
	 * 
	 * @return
	 */
	protected abstract long size();

	@Override
	public UniqueID getUniqueMsgID() {
		return msgID;
	}

}
