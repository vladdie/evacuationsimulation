package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller;

/**
 * Implementations of this Logic control which Mechanism, meaning which type
 * (Centralized or decentralized) and which Strategy or SubComponents should be
 * used for selecting Gateways.
 *
 */
public interface GatewaySelectionMechanismLogic {

	public void init();

	public void setGSServerController(GatewaySelectionServerController controller);
}
