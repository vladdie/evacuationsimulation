package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.MessageWithClusterID;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.MessageWithHopCount;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.MessageWithRound;

/**
 * Clusterer that joins the closest (hopwise) CH
 * 
 * @author Christoph Storm
 * 
 */
public class ClosestNodeClusterer implements DecentralClusterer {
	// TODO let DGS know that we might need more time to get several CH
	// messages...

	@TransferState({ "ClusterID" })
	private INodeID clusterID;
	// ClusterID in 'sliding' in this implementation...

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	private int lowestHop = Integer.MAX_VALUE;
	// private int round = -1;

	@TransferState({ "Parent" })
	public ClosestNodeClusterer(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public void startMechanism(Callback cb) {
		clusterID = null;
		lowestHop = Integer.MAX_VALUE;
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		parent = null;
		cb.finished(true);
	}

	@Override
	public INodeID getClusterID() {
		return clusterID;
	}

	@Override
	public void onMessageArrived(Message msg) {
		// Needs to be a CH message with round and hopcount
		if (msg instanceof MessageWithClusterID) {
			if (msg instanceof MessageWithRound) {
				int msgRound = ((MessageWithRound) msg).getRound();
				if (!(msgRound < parent.getRound())) {
					if (msg instanceof MessageWithHopCount) {
						int msgHops = ((MessageWithHopCount) msg).getHopcount();
						if (msgHops < lowestHop) {
							clusterID = ((MessageWithClusterID) msg).getClusterID();
							lowestHop = msgHops;
						}
					}
				}
			}
		}

	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setClusterID(INodeID clusterID) {
		if (parent.getHost().getId().equals(clusterID)) {
			lowestHop = 0;
		}
		this.clusterID = clusterID;
	}

	@Override
	public void finalizeClustering() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.CLOSESTNODECLUSTERER;
	}

}
