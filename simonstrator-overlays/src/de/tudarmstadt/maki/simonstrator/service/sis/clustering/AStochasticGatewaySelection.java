package de.tudarmstadt.maki.simonstrator.service.sis.clustering;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * 
 * @author Michael Walter
 *
 */
public abstract class AStochasticGatewaySelection extends AAdvGatewaySelection {

	private final Map<INodeID, Double> probabilities;
	protected Random rnd;

	public AStochasticGatewaySelection(String name, boolean needsNumberOfGateway) {
		super(name, needsNumberOfGateway);
		this.probabilities = new LinkedHashMap<>();
		rnd = Randoms.getRandom(AStochasticGatewaySelection.class);
	}

	/**
	 * Returns the currently stored probability of the given node
	 * 
	 * @param node
	 * @return
	 */
	protected double getProbability(INodeID node) {
		return probabilities.get(node);
	}

	/**
	 * Update the probability of a given node
	 * 
	 * @param nodeId
	 * @param probability
	 */
	protected void setProbability(INodeID nodeId, double probability) {
		probabilities.put(nodeId, probability);
	}

	/**
	 * This method has to calculate probabilities for (at least) the nodes included in forNodes. Depending on the update policy of the strategy, it does not
	 * need to do that on every call.
	 *
	 * @param forNodes
	 */
	protected abstract void calculateProbability(List<INodeID> forNodes);

	/**
	 * Draws exactly one node of the given nodes. Drawing is based on probabilities
	 * 
	 * @param outOf
	 *            the nodes
	 * @return
	 */
	protected INodeID drawGateway(List<INodeID> outOf) {
		return drawGateways(outOf, 1).get(0);
	}

	protected List<INodeID> drawGateways(List<INodeID> outOf, int numberOfGateways) {
		List<INodeID> gateways = new ArrayList<>(numberOfGateways);
		LinkedList<INodeID> drawable = new LinkedList<>(outOf);

		// Normalize

		// Draw
		while (numberOfGateways > 0) {
			double sum = 0;
			for (INodeID node : drawable) {
				probabilities.get(node);
				sum += probabilities.get(node);
			}
			double rand = rnd.nextDouble() * sum;

			Iterator<INodeID> i = drawable.iterator();
			while (i.hasNext()) {
				INodeID node = i.next();
				rand -= probabilities.get(node);
				if (rand <= 0) {
					gateways.add(node);
					i.remove();
					break;
				}
			}
			numberOfGateways--;
		}

		return gateways;
	}

}
