package de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

public class ClosestToGatewayMultihopClustering implements IBestFitClusterer {

	public Map<INodeID, List<INodeID>> getClusters(Map<INodeID, Location> gateways, Map<INodeID, Location> leafs) {

		Map<INodeID, List<INodeID>> result = new LinkedHashMap<>();
		for (INodeID gw : gateways.keySet()) {
			result.put(gw, new LinkedList<INodeID>());
		}

		Location locLeaf;
		double minDistance, curDistance;
		INodeID closestGateway;

		for (INodeID leaf : leafs.keySet()) {
			locLeaf = leafs.get(leaf);
			minDistance = Double.MAX_VALUE;
			closestGateway = null;
			for (INodeID curGW : gateways.keySet()) {
				curDistance = locLeaf.distanceTo(gateways.get(curGW));
				if (curDistance < minDistance) {
					minDistance = curDistance;
					closestGateway = curGW;
				}
			}
			result.get(closestGateway).add(leaf);
		}

		return result;
	}

}
