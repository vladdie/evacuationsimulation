package de.tudarmstadt.maki.simonstrator.service.sis.minimal;

import java.util.LinkedHashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSProviderErrorModel;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;

/**
 * Minimal implementation of a {@link SiSComponent}
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class MinimalSiSComponent implements SiSComponent {

	private final Host host;

	private final SiSInformationConsumer consumer;

	private final SiSInformationProvider provider;

	private final MinimalResolver resolver;

	public MinimalSiSComponent(Host host) {
		this.host = host;
		resolver = new MinimalResolver(this);
		consumer = new MinimalConsumer(resolver);
		provider = new MinimalProvider(resolver);
	}

	public void setErrorModels(Map<SiSType<?>, SiSProviderErrorModel> errorModels) {
		resolver.setErrorModels(errorModels);
	}

	@Override
	public void initialize() {
		// Nothing to do
	}

	@Override
	public void shutdown() {
		// TODO Auto-generated method stub

	}

	@Override
	public Host getHost() {
		return host;
	}

	@Override
	public SiSInformationProvider provide() {
		return provider;
	}

	@Override
	public SiSInformationConsumer get() {
		return consumer;
	}

	/**
	 * Simple factory
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class Factory implements HostComponentFactory {

		private Map<SiSType<?>, SiSProviderErrorModel> errorModels;

		public Factory() {
			this.errorModels = new LinkedHashMap<>();
		}

		@Override
		public MinimalSiSComponent createComponent(Host host) {
			MinimalSiSComponent c = new MinimalSiSComponent(host);
			c.setErrorModels(errorModels);
			return c;
		}

		/**
		 * Applies the provided error model to ALL hosts
		 * 
		 * @param errorModel
		 */
		public void addErrorModel(SiSProviderErrorModel errorModel) {
			if (errorModels.containsKey(errorModel.getSiSType())) {
				throw new AssertionError("Only one error model per type allowed!");
			}
			this.errorModels.put(errorModel.getSiSType(), errorModel);
		}

	}

}
