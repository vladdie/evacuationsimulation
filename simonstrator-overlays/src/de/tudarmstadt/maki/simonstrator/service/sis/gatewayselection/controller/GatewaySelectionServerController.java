package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller;

import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionServer;

/**
 * Implementations should be instantiated at the application's server entity
 *
 */
public interface GatewaySelectionServerController {

	public static final int controllerPort = 999;

	/**
	 * String that is used to switch to centralized GattewaySelection, when
	 * encountered in {@link #changeConfiguration(List}
	 */
	public static final String centralized = "centralized";

	/**
	 * String that is used to switch to decentralized GattewaySelection, when
	 * encountered in {@link #changeConfiguration(List}
	 */
	public static final String decentralized = "decentralized";

	/**
	 * Initializes the Controller. As some Components need to be initialized
	 * using Events, <i>the controller will <b>not</b> be ready immediately
	 * after calling this(!)</i> , but before the next minimal time unit.
	 */
	public void init();

	/**
	 * Sets the logic that controls transitions. The Controller in turn sets
	 * itself as the logics ServerController
	 * 
	 * @param logic
	 */
	public void setGatewaySelectionLogic(GatewaySelectionMechanismLogic logic);

	/**
	 * Forwards the request to the currently used {@link GatewaySelectionServer}
	 * . Returns the current selection of Gateways and associated nodes. Does
	 * not trigger a gateway selection.
	 * 
	 * @return
	 */
	public Map<INodeID, List<INodeID>> getCurrentGateways();

	/**
	 * Behaviour is different for Centralized and Decentralized Setting. <br>
	 * In Centralized mode this is guaranteed to start a new GatewaySelection,
	 * using the passed parameters. It returns the list of nodes associated to
	 * each Gateway, and may inform nodes of their changed {@link NodeRole} <br>
	 * In Decentralized mode, this may or may not start a new GatewaySelection,
	 * and returns the last centrally known Gateways. <br>
	 * 
	 * Behaviour is up to the individual implementations and settings.
	 * 
	 * @param outOf
	 *            select the gateways out of this list of INodeIDs. The list
	 *            must not be changed within this method!
	 * @param dataGraph
	 *            an <strong>optional</strong> graph object carrying all
	 *            annotated nodes. Can be used to save state as a node
	 *            annotation.
	 * @param maxNumberOfGateways
	 *            how many gateways should be picked at max
	 * @return a map with gateway nodes as keys and associated client nodes as
	 *         values. If the strategy does not associate clients to gateways,
	 *         the values of the returned map are empty lists.
	 */
	public Map<INodeID, List<INodeID>> getGateways(List<INodeID> outOf, Graph dataGraph, int maxNumberOfGateways);

	/**
	 * Used to trigger Transitions between {@link #centralized} and
	 * {@link #decentralized} components as well as their respective
	 * subComponents. <br>
	 * <i>For Transitions between centralized and decentralized servers, the
	 * first entry should be the corresponding string.</i>
	 * 
	 * @param configuration
	 */
	public void changeConfiguration(List<String> configuration);

}
