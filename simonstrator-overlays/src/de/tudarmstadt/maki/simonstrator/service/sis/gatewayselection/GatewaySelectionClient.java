package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ConnectivityListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;

/**
 * Client interface for the gateway selection.# TODO More description for TransMSg Listener & Conn. Listeenr
 * 
 * @author Nils Richerzhagen
 *
 */
public interface GatewaySelectionClient extends TransitionEnabled, TransMessageListener, ConnectivityListener {

	public enum NodeRole {
		CLUSTERHEAD, CLUSTERMEMBER, NONE
	};

	/**
	 * Is this client currently a selected Gateway?
	 * 
	 * @return
	 */
	public boolean isGateway();

	/**
	 * Which node {@link INodeID} is this' nodes gateway?
	 * 
	 * @return
	 */
	public INodeID getGateway();

	public NodeRole getNodeRole();

	/**
	 * Adds a listener that is called whenever a Gateway selection process
	 * ended.
	 * 
	 * @param listener
	 */
	public void addGatewaySelectionListener(GatewaySelectionListener listener);

	public void removeGatewaySelectionListener(GatewaySelectionListener listener);

	public List<GatewaySelectionListener> getGatewaySelectionListeners();

}
