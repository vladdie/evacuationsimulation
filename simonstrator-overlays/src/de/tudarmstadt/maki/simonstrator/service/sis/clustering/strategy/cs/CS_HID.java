package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.IClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.KppMeansClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.CS;

public class CS_HID extends CS {

	private IClusterer clusterer;

	public CS_HID() {
		super("CS_HID_KppMeans", true);
	}

	@Override
	protected List<List<INodeID>> calculateCluster(List<INodeID> nodes) {
		Map<INodeID, Location> curLocations = positionsHolder.getLast();

		// clusterer = new GridDensityClusterer();
		// clusterer = new DBScanClusterer(TempClusteringConfig.minPTS);
		clusterer = new KppMeansClusterer(maxNumberOfGateways);

		List<List<INodeID>> cluster = new ArrayList<List<INodeID>>();
		cluster.addAll(clusterer.getClusters(curLocations));

		return cluster;
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		for (INodeID v : forNodes) {
			setProbability(v, v.value());
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw Infos
		typeLocation = SiSTypes.PHY_LOCATION;
		types.put(typeLocation, SiSRequest.NONE);

		// High Infos
		positionsHolder = new LimitedQueue<>(1); // Positions

		return types;
	}

}
