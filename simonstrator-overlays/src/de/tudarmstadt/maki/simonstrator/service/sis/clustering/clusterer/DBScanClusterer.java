package de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.apache.commons.math3.ml.clustering.DBSCANClusterer;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.CLocation;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.TempClusteringConfig;

public class DBScanClusterer implements IClusterer {

	private int minPTS;
	private double eps;

	public DBScanClusterer(int minPTS) {
		this.minPTS = minPTS;
		this.eps = TempClusteringConfig.maxRange;
	}

	/**
	 * Perform a cluster analysis on the given set of nodes and locations instances.<br>
	 * Should be used instead of method cluster().
	 * 
	 * @param nodes
	 *            the set of nodes and locations
	 * @return a list of clusters
	 */
	public List<List<INodeID>> getClusters(Map<INodeID, Location> nodes) {

		Collection<CLocation> points = new HashSet<CLocation>(nodes.size());
		for (Entry<INodeID, Location> e : nodes.entrySet()) {
			points.add(new CLocation(e.getKey(), e.getValue()));
		}

		DBSCANClusterer<CLocation> dbscan = new DBSCANClusterer<>(eps, minPTS);
		List<Cluster<CLocation>> newClusters = dbscan.cluster(points);

		// Create grid
		List<List<INodeID>> clusters = new LinkedList<List<INodeID>>();
		for (Cluster<CLocation> cCluster : newClusters) {
			LinkedList<INodeID> cluster = new LinkedList<INodeID>();
			for (CLocation l : cCluster.getPoints()) {
				cluster.add(l.getNodeID());
			}
			if (!cluster.isEmpty()) {
				clusters.add(cluster);
			}
		}

		return clusters;
	}

}
