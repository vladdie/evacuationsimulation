package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.dkc;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.DkC;

public class DkC_LEACH extends DkC {

	public DkC_LEACH() {
		super("DkC_LEACH");
	}

	@Override
	protected void calculateWeights(List<INodeID> forNodes) {
		// Get Parameters
		double k = maxNumberOfGateways;
		double N = forNodes.size();
		double lastXRounds = N / k; // 1/p with p = k/N

		// Calculate Probability
		double t_v = k / (N - k * (round % (N / k)));
		for (INodeID node_v : forNodes) {
			Long lastRoundAsGateway = lastTimeGateway.get(node_v);
			if (lastRoundAsGateway == null || lastRoundAsGateway < round - lastXRounds) {
				setWeight(node_v, t_v);
			} else {
				setWeight(node_v, 0.0);
			}
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		return Collections.emptyMap(); // No SiS interaction is required
	}

}
