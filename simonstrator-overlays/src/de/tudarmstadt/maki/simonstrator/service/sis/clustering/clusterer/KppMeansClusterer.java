package de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.math3.ml.clustering.CentroidCluster;
import org.apache.commons.math3.ml.clustering.KMeansPlusPlusClusterer;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.CLocation;

public class KppMeansClusterer implements IClusterer {

	private int k;

	public KppMeansClusterer(int k) {
		this.k = k;
	}

	/**
	 * Perform a cluster analysis on the given set of nodes and locations instances.<br>
	 * Should be used instead of method cluster().
	 * 
	 * @param nodes
	 *            the set of nodes and locations
	 * @return a list of clusters
	 */
	public List<List<INodeID>> getClusters(Map<INodeID, Location> nodes) {

		Collection<CLocation> points = new HashSet<CLocation>(nodes.size());
		for(Entry<INodeID, Location> e : nodes.entrySet()) {
			points.add(new CLocation(e.getKey(), e.getValue()));
		}
		
		KMeansPlusPlusClusterer<CLocation> kppClusterer = new KMeansPlusPlusClusterer<CLocation>(this.k);
		List<CentroidCluster<CLocation>> newClusters = kppClusterer.cluster(points);
		
		// Create grid
		List<List<INodeID>> clusters = new LinkedList<List<INodeID>>();
		for (CentroidCluster<CLocation> cCluster : newClusters) {
			LinkedList<INodeID> cluster = new LinkedList<INodeID>();
			for (CLocation l : cCluster.getPoints()) {
				cluster.add(l.getNodeID());
			}
			if (!cluster.isEmpty()) {
				clusters.add(cluster);
			}
		}

		return clusters;
	}
}
