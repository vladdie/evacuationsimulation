package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.IDSpace;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.DefaultIDSpace;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient.DGSComponent;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer.ClosestNodeClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer.DecentralClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer.FirstHeardClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination.ContentionBasedDissemination;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination.Flooding;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination.LocalDissemination;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination.ProbabilisticDissemination;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme.DGSScheme;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme.DTwoSteps;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme.SOneStep;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.NoLogicTrigger;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.PeriodicTrigger;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.Trigger;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.CEMCA;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.EnLat;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.Scaled_WCA;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.WeightComputation;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach.ALEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach.DEEC;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach.LEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach.LEACH.ClusterHeadExclusion;

public class DGSComponentFactory {

	// Utility
	// 32 bits (or one java int) should actually offer enough space...
	private static final IDSpace msgIDSpace = new DefaultIDSpace(32);

	// dgs
	private static long maxFallbackContention = 10 * Time.SECOND;
	private static long remoteStartDelay = 10 * Time.MILLISECOND;
	private static boolean informServer = false;

	// Clusterer
	private static DGSSubComponents clustererClassName = DGSSubComponents.FIRSTHEARDCLUSTERER;

	// dissemination
	private static DGSSubComponents disseminationClassName = DGSSubComponents.FLOODING;
	private static long maxContention = 3 * Time.SECOND;
	private static double disseminationProbability = 0.75d;
	private static boolean stochDissAlwaysSendFirstHop = true;

	// scheme
	private static DGSSubComponents schemeClassName = DGSSubComponents.SONESTEP;
	private static int schemeRadius = -1;
	// stochastic
	private static long schemeTimeout = 20 * Time.SECOND;
	// deterministic
	private static long dStep1Timeout = 30 * Time.SECOND;
	private static long dStep2Timeout = 15 * Time.SECOND;

	// trigger
	private static DGSSubComponents triggerClassName = DGSSubComponents.TIMERBASEDTRIGGER;
	private static long triggerInterval = 5 * Time.MINUTE;

	// weighter
	private static DGSSubComponents weighterClassName = DGSSubComponents.ALEACH;
	// -aleach
	private static ClusterHeadExclusion dontUse = ClusterHeadExclusion.Last;
	private static double leachBaseProbability = 0.1;
	// -wca
	private static int idealClustersize = 10;
	// -deec
	private static boolean useMonitoring = true;
	// -enlat
	private static double maxLatency = 5 * Time.MILLISECOND;

	private static boolean notYetConfd = true;

	// public DGSComponentFactory(Host host) {
	// this.preConf();
	//
	// DecentralGatewaySelectionClient dgs = new
	// DecentralGatewaySelectionClient(host);
	// dgs.setDefaultComponent(DGSComponent.CLUSTERER, defClusterer(dgs));
	// dgs.setDefaultComponent(DGSComponent.DISSEMNINATION,
	// defDissemination(dgs));
	// dgs.setDefaultComponent(DGSComponent.SCHEME, defScheme(dgs));
	// dgs.setDefaultComponent(DGSComponent.TRIGGER, defTrigger(dgs));
	// dgs.setDefaultComponent(DGSComponent.WEIGHTCOMPUTATION,
	// defWeighter(dgs));
	//
	// dgs.setMaxFallbackContention(maxFallbackContention);
	// }

	public static DGSSubComponent generateDefaultComponent(IDecentralGatewaySelectionClient parent, DGSComponent type) {
		preConf();
		switch (type) {
		case CLUSTERER:
			return defClusterer(parent);
		case DISSEMNINATION:
			return defDissemination(parent);
		case SCHEME:
			return defScheme(parent);
		case TRIGGER:
			return defTrigger(parent);
		case WEIGHTCOMPUTATION:
			return defWeighter(parent);
		}
		return null;
	}

	public static long getMaxFallbackContention() {
		return maxFallbackContention;
	}

	public static long getRemoteStartDelay() {
		return remoteStartDelay;
	}

	public static boolean isInformServer() {
		return informServer;
	}

	// @Override
	// public HostComponent createComponent(Host host) {
	//
	// this.preConf();
	//
	// defaultDGS dgs = new defaultDGS(host);
	// dgs.setDefaultComponent(DGSComponent.CLUSTERER, defClusterer(dgs));
	// dgs.setDefaultComponent(DGSComponent.DISSEMNINATION, defDissemination(dgs));
	// dgs.setDefaultComponent(DGSComponent.SCHEME, defScheme(dgs));
	// dgs.setDefaultComponent(DGSComponent.TRIGGER, defTrigger(dgs));
	// dgs.setDefaultComponent(DGSComponent.WEIGHTCOMPUTATION, defWeighter(dgs));
	//
	// dgs.setMaxFallbackContention(maxFallbackContention);
	// return dgs;
	// }

	private static DecentralClusterer defClusterer(IDecentralGatewaySelectionClient parent) {
		switch (clustererClassName) {
		case CLOSESTNODECLUSTERER:
			// Smallest Hops
			ClosestNodeClusterer cnc = new ClosestNodeClusterer(parent);
			return cnc;
		case FIRSTHEARDCLUSTERER:
			// Earliest message arrival
			FirstHeardClusterer fhc = new FirstHeardClusterer(parent);
			return fhc;
		default:
			throw new AssertionError("Unknown Clusterer for decentral gateway selection!");
		}
	}

	private static LocalDissemination defDissemination(IDecentralGatewaySelectionClient dgs) {
		switch (disseminationClassName) {
		case FLOODING:
			// default
			Flooding ld = new Flooding(dgs);
			return ld;
		case CONTENTIONBASEDDISSEMINATION:
			// Contention based
			ContentionBasedDissemination cd = new ContentionBasedDissemination(dgs);
			return cd;
		case PROBABILISTICDISSEMINATION:
			// Probabilistic
			ProbabilisticDissemination pd = new ProbabilisticDissemination(dgs);
			return pd;
		default:
			throw new AssertionError("Unknown Disseminator for decentral gateway selection!");
		}
	}

	private static DGSScheme defScheme(IDecentralGatewaySelectionClient dgs) {
		switch (schemeClassName) {
		case SONESTEP:
			// default probabilistic
			SOneStep sos = new SOneStep(dgs);
			return sos;
		case DTWOSTEPS:
			// deterministic
			DTwoSteps dts = new DTwoSteps(dgs);
			return dts;
		default:
			throw new AssertionError("Unknown Scheme for decentral gateway selection!");
		}
	}

	private static Trigger defTrigger(IDecentralGatewaySelectionClient dgs) {
		switch (triggerClassName) {
		case TIMERBASEDTRIGGER:
			// default
			Trigger trigger = new PeriodicTrigger(dgs);
			return trigger;
		case NOLOGICTRIGGER:
			Trigger eventBased = new NoLogicTrigger(dgs);
			return eventBased;
		default:
			throw new AssertionError("Unknown Trigger for decentral gateway selection!");
		}

	}

	private static WeightComputation defWeighter(IDecentralGatewaySelectionClient dgs) {
		switch (weighterClassName) {
		case ALEACH:
			// ALEACH
			ALEACH aw = new ALEACH(dgs);
			aw.setClusterHeadExclusion(dontUse);
			aw.setBaseProbability(leachBaseProbability);
			return aw;
		case DEEC:
			// deec
			DEEC deec = new DEEC(dgs);
			deec.setBaseProbability(leachBaseProbability);
			return deec;
		case LEACH:
			LEACH leach = new LEACH(dgs);
			leach.setBaseProbability(leachBaseProbability);
			return leach;
		case CEMCA:
			// cemca
			CEMCA cemca = new CEMCA(dgs);
			cemca.setIdealClusterSize(idealClustersize);
			return cemca;
		case ENLAT:
			// Energy and Latency based
			EnLat enlat = new EnLat(dgs);

			// Because of TransitionEngine...
			enlat.setBaseProbability(leachBaseProbability);
			return enlat;
		case SCALED_WCA:
			// wca
			Scaled_WCA wca = new Scaled_WCA(dgs);
			wca.setIdealClusterSize(idealClustersize);
			return wca;
		default:
			throw new AssertionError("Unknown WeightComputation for decentral gateway selection!");
		}
	}

	private static void preConf() {
		if (notYetConfd) {
			// dissemination
			ContentionBasedDissemination.setMaxContention(maxContention);
			ProbabilisticDissemination.setProbability(disseminationProbability);
			ProbabilisticDissemination
					.setAlwaysSendOnFirstHop(stochDissAlwaysSendFirstHop);
			// scheme
			SOneStep.setRadius(schemeRadius);
			SOneStep.setTimeOut(schemeTimeout);
			DTwoSteps.setRadius(schemeRadius);
			DTwoSteps.setPhase1TimeoutOut(dStep1Timeout);
			DTwoSteps.setPhase2TimeoutOut(dStep2Timeout);
			// trigger
			PeriodicTrigger.setInterval(triggerInterval);
			// weighting
			DEEC.setUseMonitoring(useMonitoring);
			EnLat.setMaxCellularLatency(maxLatency);
		}
		notYetConfd = false;
	}

	public static UniqueID getNewMessageID() {
		return msgIDSpace.createRandomID();
	}

	////////////////////////////////////////////////////////////
	// Setters
	////////////////////////////////////////////////////////////
	// dgs
	public static void setDGSMaxFallbackContention(long time) {
		maxFallbackContention = time;
	}

	public static void setRemoteStartDelay(long time) {
		remoteStartDelay = time;
	}

	public static void setInformServer(boolean sendToServer) {
		informServer = sendToServer;
	}

	// Clusterer
	public static void setClustererClassName(String name) {
		try {
			clustererClassName = DGSSubComponents.valueOf(name.toUpperCase());
			assert clustererClassName.getType().equals(DGSComponent.CLUSTERER);
		} catch (Exception e) {
			throw new IllegalArgumentException(name + " is not a valid DGS clusterer class!");
		}
	}

	// dissemination
	public static void setDisseminationClassName(String name) {
		try {
			disseminationClassName = DGSSubComponents.valueOf(name.toUpperCase());
			assert disseminationClassName.getType().equals(DGSComponent.DISSEMNINATION);
		} catch (Exception e) {
			throw new IllegalArgumentException(name + " is not a valid DGS dissemiation class!");
		}
	}

	public static void setMaxDisseminationContention(long time) {
		maxContention = time;
	}

	public static void setDisseminationProbability(double prob) {
		assert prob > 0 : "DisseminationProbability must be larger zero!";
		assert prob <= 1 : "DisseminationProbability must be smaller or equal one!";
		disseminationProbability = prob;
	}

	public static void setAlwaysSendFirstHop(boolean send) {
		stochDissAlwaysSendFirstHop = send;
	}

	// scheme
	public static void setSchemeClassName(String name) {
		try{
			schemeClassName = DGSSubComponents.valueOf(name.toUpperCase());
			assert schemeClassName.getType().equals(DGSComponent.SCHEME);
		} catch (Exception e) {
			throw new IllegalArgumentException(name + " is not a valid DGS scheme class!");
		}
	}

	public static void setSchemeMessageRadius(int hops) {
		schemeRadius = hops;
	}
	// // stochastic
	public static void setSinglePhaseSchemeTimeout(long time) {
		schemeTimeout = time;
	}
	// // deterministic
	public static void setDSchemeStep1Timeout(long time) {
		dStep1Timeout = time;
	}

	public static void setDSchemeStep2Timeout(long time) {
		dStep2Timeout = time;
	}

	// trigger
	public static void setTriggerClassName(String name) {
		try {
			triggerClassName = DGSSubComponents.valueOf(name.toUpperCase());
			assert triggerClassName.getType().equals(DGSComponent.TRIGGER);
		} catch (Exception e) {
			throw new IllegalArgumentException(name + " is not a valid DGS Trigger class!");
		}
	}

	public static void setTimedTriggerInterval(long time) {
		triggerInterval = time;
	}

	// weighter
	public static void setWeightComputationClassName(String name) {
		try {
			weighterClassName = DGSSubComponents.valueOf(name.toUpperCase());
			assert weighterClassName.getType().equals(DGSComponent.WEIGHTCOMPUTATION);
		} catch (Exception e) {
			throw new IllegalArgumentException(name + " is not a valid DGS WeightComputation class!");
		}
	}
	// // leach
	public static void setLeachBaseProbability(double prob) {
		assert prob > 0 : "(DGS) LEACH base probability must be larger zero!";
		assert prob <= 1 : "(DGS) LEACH base probability must be smaller or equal one!";
		leachBaseProbability = prob;
	}
	// // -wca
	public static void setIdealClustersize(int num) {
		assert num > 0 : "Ideal ClusterSize must be larger zero!";
		idealClustersize = num;
	}
	// // -deec
	public static void setDEECUseMonitoring(boolean monitor) {
		useMonitoring = monitor;
	}
	// // -enlat
	public static void setMaxCellLatency(long time) {
		maxLatency = time;
	}

}
