package de.tudarmstadt.maki.simonstrator.service.sis.clustering;

import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;

/**
 * Select a number of gateways out of a list of nodes based on additional information available via the
 * {@link SiSComponent}.
 * 
 * @author Bjoern Richerzhagen including changes by Nils Richerzhagen
 *
 */
public interface GatewaySelectionStrategy extends TransitionEnabled {

	public void setSiS(SiSComponent sis);

	public SiSComponent getSiS();

	/**
	 * Has to return all {@link SiSType}s that are required for this strategy to
	 * function. Based on the available types and an optional further
	 * specification of the {@link SiSRequest}, a matching strategy is chosen.
	 *
	 * @return
	 */
	public Map<SiSType<?>, SiSRequest> getRequiredInformation();

	/**
	 * Returns a collection of INodeIDs that act as gateways within the current
	 * selection of nodes that were passed as an argument.
	 *
	 * @param outOf
	 *            select the gateways out of this list of INodeIDs. The list
	 *            must not be changed within this method!
	 * @param dataGraph
	 *            an <strong>optional</strong> graph object carrying all
	 *            annotated nodes. Can be used to save state as a node
	 *            annotation.
	 * @param maxNumberOfGateways
	 *            how many gateways should be picked at max
	 * @return a map with gateway nodes as keys and associated client nodes as
	 *         values. If the strategy does not associate clients to gateways,
	 *         the values of the returned map are empty lists.
	 */
	public Map<INodeID, List<INodeID>> getGateways(List<INodeID> outOf, Graph dataGraph, int maxNumberOfGateways);

}
