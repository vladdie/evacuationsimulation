package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.AGatewaySelectionStrategy;

/**
 * A Strategy that just assigns no gateways at all.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class NoGateways extends AGatewaySelectionStrategy {

	private static final Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

	@Override
	public Map<SiSType<?>, SiSRequest> getRequiredInformation() {
		return types;
	}

	@Override
	public Map<INodeID, List<INodeID>> getGateways(List<INodeID> outOf, Graph dataGraph, int maxNumberOfGateways) {
		LinkedHashMap<INodeID, List<INodeID>> gateways = new LinkedHashMap<>();
		for (INodeID node : outOf) {
			gateways.put(node, Collections.emptyList());
		}
		return gateways;
	}

}
