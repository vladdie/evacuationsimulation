package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;

public class GatewayControllerMessage implements Message {
	// second most useless message ever?

	/**
	 * 
	 */
	private static final long serialVersionUID = 8394242046928316767L;

	private boolean useCentralizedClient;

	public GatewayControllerMessage(boolean useCentralizedClient) {
		this.useCentralizedClient = useCentralizedClient;
	}

	public boolean isUseCentralizedClient() {
		return useCentralizedClient;
	}

	@Override
	public long getSize() {
		return 1;
	}

	@Override
	public Message getPayload() {
		return null;
	}

}
