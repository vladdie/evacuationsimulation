package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.BasicGraph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.IEdge;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.TempClusteringConfig;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.D1C;

public class D1C_FWCABP extends D1C {

	public D1C_FWCABP() {
		super("D1C_FWCABP", false);
	}

	// Parameters - Weight Factors
	private double a_1 = 0.7;
	private double a_2 = 0.2;
	private double a_3 = 0.05;
	private double a_4 = 0.05;

	// Strategy variables
	private long currentRound = -1;

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		// Raw Infos
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();
		typeLocation = SiSTypes.PHY_LOCATION;
		types.put(typeLocation, SiSRequest.NONE);
		typeEnergylevel = SiSTypes.ENERGY_BATTERY_LEVEL;
		types.put(typeEnergylevel, SiSRequest.NONE);
		typeEnergycapacity = SiSTypes.ENERGY_BATTERY_CAPACITY;
		types.put(typeEnergycapacity, SiSRequest.NONE);

		// Higher Infos
		positionsHolder = new LimitedQueue<>(2); // Positions
		neighborhoodGraph = new BasicGraph(); // Neighborhood
		normalizedEnergielevels = new HashMap<INodeID, Double>(); // Normalized energy levels

		return types;
	}

	@Override
	protected void calculateWeights(List<INodeID> forNodes) {
		if (super.round != currentRound) {
			currentRound = round;

			// Get required information
			int l = TempClusteringConfig.optimalNumberOfLeafsPerGateway;

			Map<INodeID, Double> tempWeights = new HashMap<INodeID, Double>(forNodes.size());
			double smallestWeight = Double.MAX_VALUE;

			for (INodeID v : forNodes) {
				double degree = neighborhoodGraph.getOutdegree(v);
				double degree_spread = 1 - (Math.abs(l - degree) / l);

				double lcc = calculateLCC(v);

				double normalizedEnergylevel = 1 - normalizedEnergielevels.get(v);

				double speed = calcAvgSpeed(v);

				double w_v = a_1 * degree_spread + a_2 * lcc + a_3 * normalizedEnergylevel + a_4 * speed;

				w_v = w_v * -1.0;
				if (w_v < smallestWeight) {
					smallestWeight = w_v;
				}
				tempWeights.put(v, w_v);
			}

			for (INodeID v : forNodes) {
				double w_v = tempWeights.get(v);
				w_v = w_v + Math.abs(smallestWeight);
				// FWCABP assigns the mobile station which has the smallest value
				setWeight(v, w_v);
			}

		}
	}

	private double calculateLCC(INodeID node) {

		Set<String> edges = new HashSet<String>();
		Set<INodeID> neighbors = neighborhoodGraph.getNeighbors(node);
		for (INodeID neighbor : neighbors) {
			Set<IEdge> edgesOfNeighbor = neighborhoodGraph.getOutgoingEdges(neighbor);
			for (IEdge edge : edgesOfNeighbor) {
				long id1 = edge.fromId().value();
				long id2 = edge.toId().value();
				if (id1 < id2) {
					edges.add(id1 + "-" + id2);
				} else {
					edges.add(id2 + "-" + id1);
				}
			}
		}
		double N = neighborhoodGraph.getNodeCount();
		double maxPossibleLinks = N * (N - 1) / 2;
		double lcc = edges.size() / maxPossibleLinks;
		return lcc;
	}

}
