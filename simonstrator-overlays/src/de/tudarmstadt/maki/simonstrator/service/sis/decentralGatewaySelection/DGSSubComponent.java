package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection;

import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;

/**
 * Helper Interface for DGS components. (Should have used this one sooner)
 * 
 * @version 2016.09.08
 */
public interface DGSSubComponent extends TransitionEnabled {

	public void setParent(IDecentralGatewaySelectionClient parent);

	public IDecentralGatewaySelectionClient getParent();

	/**
	 * Returns the DGSSubComponent corresponding to this implementation. This
	 * method became necessary because the DGS client should not carry this
	 * state over to other GatewaySelection clients.
	 * 
	 * @return
	 */
	public DGSSubComponents getImplementedType();

}
