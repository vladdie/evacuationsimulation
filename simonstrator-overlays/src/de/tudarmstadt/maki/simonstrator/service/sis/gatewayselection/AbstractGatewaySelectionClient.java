package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;

/**
 * Initially start with {@link NodeRole} set to NONE.
 * 
 * @author Nils Richerzhagen
 *
 */
public abstract class AbstractGatewaySelectionClient implements GatewaySelectionClient {

	@TransferState({ "Host" })
	private Host host;

	@TransferState({ "Sis" })
	private SiSComponent sis;

	@TransferState({ "NodeRole" })
	private NodeRole nodeRole;

	@TransferState({ "Gateway" })
	private INodeID currentGateway;

	@TransferState({ "GatewaySelectionListeners" })
	private List<GatewaySelectionListener> listeners;



	@TransferState({ "Host" })
	public AbstractGatewaySelectionClient(Host host) {
		this.host = host;

		this.setNodeRole(NodeRole.NONE);

		this.setGateway(null);

		this.listeners = new LinkedList<GatewaySelectionListener>();
	}

	public Host getHost() {
		return host;
	}

	public SiSComponent getSis() {
		return sis;
	}

	public void setSis(SiSComponent sis) {
		this.sis = sis;
	}

	@Override
	public NodeRole getNodeRole() {
		return nodeRole;
	}

	public void setNodeRole(NodeRole nodeRole) {
		this.nodeRole = nodeRole;
	}

	public boolean isGateway() {
		return getNodeRole() == NodeRole.CLUSTERHEAD;
	}

	public void setGateway(INodeID currentGateway) {
		this.currentGateway = currentGateway;
	}

	@Override
	public INodeID getGateway() {
		return this.currentGateway;
	}

	@Override
	public void addGatewaySelectionListener(GatewaySelectionListener listener) {
		this.listeners.add(listener);
	}

	@Override
	public void removeGatewaySelectionListener(GatewaySelectionListener listener) {
		listeners.remove(listener);
	}

	@Override
	public List<GatewaySelectionListener> getGatewaySelectionListeners() {
		return listeners;
	}

	public void setGatewaySelectionListeners(List<GatewaySelectionListener> listeners) {
		this.listeners = listeners;
	}
}
