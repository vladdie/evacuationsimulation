package de.tudarmstadt.maki.simonstrator.service.sis.error;

import java.util.Random;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSDataCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInfoProperties;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider.SiSProviderHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSProviderErrorModel;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

/**
 * Generic model for location errors.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public abstract class LocationErrorModel implements SiSProviderErrorModel<Location> {

	@Override
	public SiSType<Location> getSiSType() {
		return SiSTypes.PHY_LOCATION;
	}

	/**
	 * The actual error function
	 * 
	 * @param localhost
	 * @param original
	 *            already CLONED!!
	 * @return manipulated error
	 */
	public abstract Location applyLocationError(Host localhost, Location location);

	@Override
	public SiSDataCallback<Location> createErrorModelCallback(Host localHost, SiSType<Location> type,
			SiSDataCallback<Location> originalDataSource) {
		// Simply no error model, but cone
		return new SiSDataCallback<Location>() {

			@Override
			public Location getValue(INodeID nodeID, SiSProviderHandle providerHandle)
					throws InformationNotAvailableException {
				if (nodeID.equals(localHost.getId())) {
					Location old = originalDataSource.getValue(nodeID, providerHandle);
					return applyLocationError(localHost, old.clone());
				} else {
					return originalDataSource.getValue(nodeID, providerHandle);
				}
			}

			@Override
			public Set<INodeID> getObservedNodes() {
				return originalDataSource.getObservedNodes();
			}

			@Override
			public SiSInfoProperties getInfoProperties() {
				return originalDataSource.getInfoProperties();
			}
		};
	}

	/**
	 * Just testing
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class NoLocationError extends LocationErrorModel {

		/**
		 * no configuration
		 */
		public NoLocationError() {
			//
		}

		@Override
		public Location applyLocationError(Host localhost, Location location) {
			return location;
		}

	}

	/**
	 * Just Random offset
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class RandomLocationError extends LocationErrorModel {

		private double maxError;

		private Random rnd = Randoms.getRandom(RandomLocationError.class);

		/**
		 * 
		 * @param maxError
		 */
		@XMLConfigurableConstructor({ "maxError" })
		public RandomLocationError(double maxError) {
			this.maxError = maxError;
		}

		@Override
		public Location applyLocationError(Host localhost, Location location) {
			double randomErrorInBounds_X = (rnd.nextBoolean() ? -1 : 1) * rnd.nextDouble() * maxError;
			double randomErrorInBounds_Y = (rnd.nextBoolean() ? -1 : 1) * rnd.nextDouble() * maxError;
			location.setLatitude(location.getLatitude() + randomErrorInBounds_Y);
			location.setLongitude(location.getLongitude() + randomErrorInBounds_X);
			location.setAccuracy(maxError);
			return location;
		}
	}

}
