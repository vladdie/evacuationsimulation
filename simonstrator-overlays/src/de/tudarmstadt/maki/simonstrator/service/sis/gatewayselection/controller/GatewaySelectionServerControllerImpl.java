package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller;

import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.BootstrapEntity;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.CentralizedGatewaySelectionServer;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.DecentralizedGatewaySelectionServer;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionServer;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.GSClientBootstrapRequestMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.GatewayControllerMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.GatewaySelectionClientBootstrapMessage;

public class GatewaySelectionServerControllerImpl implements GatewaySelectionServerController, TransMessageListener {

	private final static String serverComponent = "GatewaySelectionServer";

	public NetID serverControllerNetID;

	private GatewaySelectionMechanismLogic logic;

	private MessageBasedTransport downConnection;

	private Host host;
	private GatewaySelectionServer server;

	private TransitionEngine transEngine;

	private String currentServer;

	public GatewaySelectionServerControllerImpl(Host host) {
		this.host = host;
	}

	@Override
	public void init() {
		try {

			serverControllerNetID = host.getNetworkComponent().getByName(NetInterfaceName.MOBILE).getLocalInetAddress();

			downConnection = host.getTransportComponent().getProtocol(UDP.class, serverControllerNetID,
					GatewaySelectionServerController.controllerPort);

			transEngine = host.getComponent(TransitionEngine.class);

		} catch (ProtocolNotAvailableException | ComponentNotAvailableException e) {
			throw new RuntimeException(e);
		}

		if (BootstrapEntity.getServerContact() != null) {
			throw new RuntimeException("There should only be one Server - Controller instance!");
		}
		BootstrapEntity.setServerContact(downConnection.getTransInfo());

		// default?
		server = new CentralizedGatewaySelectionServer(host);
		currentServer = GatewaySelectionServerController.centralized;

		server = transEngine.createMechanismProxy(GatewaySelectionServer.class, server, serverComponent);
		
		downConnection.setTransportMessageListener(this);
		
		// do we need to call this?
		Event.scheduleImmediately(new EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				logic.init();
			}
		}, null, 0);

		// Monitor.log(GatewaySelectionServerControllerImpl.class, Level.INFO,
		// "GatewaySelectionController
		// started");
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		if (msg instanceof GSClientBootstrapRequestMessage) {
			// Are there any other message types for us?
			GSClientBootstrapRequestMessage request = (GSClientBootstrapRequestMessage) msg;

			GatewaySelectionClientBootstrapMessage reply = new GatewaySelectionClientBootstrapMessage(
					new GatewayControllerMessage(GatewaySelectionServerController.centralized.equals(currentServer)),
					server.getConfigMessageFor(request.getRequester()));

			downConnection.send(reply, sender.getNetId(), sender.getPort());
		}
	}

	@Override
	public void setGatewaySelectionLogic(GatewaySelectionMechanismLogic logic) {
		this.logic = logic;
		logic.setGSServerController(this);
	}

	@Override
	public Map<INodeID, List<INodeID>> getCurrentGateways() {
		return server.getCurrentGateways();
	}

	@Override
	public Map<INodeID, List<INodeID>> getGateways(List<INodeID> outOf, Graph dataGraph, int maxNumberOfGateways) {
		return server.getGateways(outOf, dataGraph, maxNumberOfGateways);
	}

	@Override
	public void changeConfiguration(List<String> configuration) {
		if (!configuration.isEmpty()) {

			// Exchange Server component
			String arg1 = configuration.get(0).toLowerCase();
			boolean stateChanged = false;
			

			if (arg1.equals(GatewaySelectionServerController.centralized)){
				configuration.remove(0);
				if( currentServer.equals(GatewaySelectionServerController.decentralized)) {
					transEngine.executeAtomicTransition(serverComponent, CentralizedGatewaySelectionServer.class);
					stateChanged = true;
				}
			} else if (arg1.equals(GatewaySelectionServerController.decentralized)) {
				configuration.remove(0);
				if(currentServer.equals(GatewaySelectionServerController.centralized)) {
					transEngine.executeAtomicTransition(serverComponent, DecentralizedGatewaySelectionServer.class);
					stateChanged = true;
				}
			}
			if (stateChanged) {
				currentServer = arg1;
				sendConfigurationMessages();
			}

			// Let the server handle further component changes.
			for (String component : configuration) {
				server.setComponent(component);
			}
			Monitor.log(GatewaySelectionServerControllerImpl.class, Level.INFO, "%s: Configuration changed to %s: %s",
					Time.getFormattedTime(), currentServer, configuration);
		}
	}

	private void sendConfigurationMessages() {
		for (NetInterface connection : BootstrapEntity.getAllNetConnections()) {
			if (connection.isUp()) {
				GatewayControllerMessage msg = new GatewayControllerMessage(
						GatewaySelectionServerController.centralized.equals(currentServer));
				downConnection.send(msg, connection.getLocalInetAddress(), controllerPort);
			}
		}
	}

}
