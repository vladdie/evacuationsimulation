package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.SkC;

public class SkC_DEEC extends SkC {

	public SkC_DEEC() {
		super("SkC_DEEC");
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		// Get Parameters
		double k = maxNumberOfGateways;
		double N = forNodes.size();
		double lastXRounds = N / k; // 1/p with p = k/N

		// Calculate Average Energylevel
		double avgEnergy = 0;
		double curEnergy;
		Map<INodeID, Double> energyLevels = new HashMap<>();
		for (INodeID node : forNodes) {
			try {
				curEnergy = getValue(node, typeEnergylevel) * getValue(node, typeEnergycapacity);
				energyLevels.put(node, curEnergy);
				avgEnergy += curEnergy;
			} catch (InformationNotAvailableException e) {
				System.out.println("No Energy-Information available.");
				setProbability(node, 0.0);
			}
		}
		avgEnergy = avgEnergy / energyLevels.size();

		// Calculate Probability
		double p_opt = k / energyLevels.size();
		double p_v;
		INodeID v;
		for (Entry<INodeID, Double> e : energyLevels.entrySet()) {
			v = e.getKey();
			Long lastRoundAsGateway = lastTimeGateway.get(v);
			if (lastRoundAsGateway == null || lastRoundAsGateway < round - lastXRounds) {
				p_v = p_opt * e.getValue() / avgEnergy;
				double t_v = p_v / (1 - p_v * (round % (1 / p_v)));
				setProbability(v, t_v);
			} else {
				setProbability(v, 0.0);
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw Data
		typeEnergylevel = SiSTypes.getType("ENERGY_BATTERY_LEVEL", Double.class);
		types.put(typeEnergylevel, SiSRequest.NONE);
		typeEnergycapacity = SiSTypes.getType("ENERGY_BATTERY_CAPACITY", Double.class);
		types.put(typeEnergycapacity, SiSRequest.NONE);

		return types;
	}

}
