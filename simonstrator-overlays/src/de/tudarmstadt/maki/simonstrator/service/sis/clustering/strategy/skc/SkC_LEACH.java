package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.skc;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.SkC;

public class SkC_LEACH extends SkC {

	public SkC_LEACH() {
		super("SkC_LEACH");
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		// Get Parameters
		double k = maxNumberOfGateways;
		double N = forNodes.size();
		double lastXRounds = N / k; // 1/p with p = k/N

		// Calculate Probability
		double t_v = k / (N - k * (round % (N / k)));
		for (INodeID node_v : forNodes) {
			Long lastRoundAsGateway = lastTimeGateway.get(node_v);
			if (lastRoundAsGateway == null || lastRoundAsGateway < round - lastXRounds) {
				setProbability(node_v, t_v);
			} else {
				setProbability(node_v, 0.0);
			}
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		return Collections.emptyMap(); // No SiS interaction is required
	}

}
