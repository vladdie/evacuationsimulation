package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

public class DGSServerUpdateMessage implements Message {
	// FIXME is this message a good idea?

	/**
	 * 
	 */
	private static final long serialVersionUID = 8272908888032186014L;

	private INodeID sender;
	private INodeID gateway;
	
	public DGSServerUpdateMessage(INodeID sender, INodeID gateway) {
		this.sender = sender;
		this.gateway = gateway;
	}

	@Override
	public long getSize() {
		return sender.getTransmissionSize() + gateway.getTransmissionSize();
	}

	@Override
	public Message getPayload() {
		return null;
	}

	public INodeID getSender() {
		return sender;
	}

	public INodeID getGateway() {
		return gateway;
	}

}
