package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

public class GSClientBootstrapRequestMessage implements Message {
	// probably the most useless message ever...
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6871710110093694085L;

	private INodeID requester;

	public GSClientBootstrapRequestMessage(INodeID requester) {
		this.requester = requester;
	}

	@Override
	public long getSize() {
		return requester.getTransmissionSize();
	}

	@Override
	public Message getPayload() {
		return null;
	}
	
	public INodeID getRequester(){
		return requester;
	}

}
