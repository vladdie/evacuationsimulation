package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.IClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.KppMeansClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.CD;

/**
 * Just a very simple test strategy, relying on a Bypass-Metric.
 * 
 * @author Bjoern Richerzhagen
 *
 */
@Deprecated
public class CD_MinMessage extends CD {

	private IClusterer clusterer;

	public CD_MinMessage() {
		super("CD_MinMessage_KppMeans", true);

	}

	@Override
	protected List<List<INodeID>> calculateCluster(List<INodeID> nodes) {
		Map<INodeID, Location> curLocations = positionsHolder.getLast();

		// clusterer = new GridDensityClusterer();
		// clusterer = new DBScanClusterer(TempClusteringConfig.minPTS);
		clusterer = new KppMeansClusterer(maxNumberOfGateways);

		List<List<INodeID>> cluster = new ArrayList<List<INodeID>>();
		cluster.addAll(clusterer.getClusters(curLocations));

		return cluster;
	}

	@Override
	protected void calculateWeights(List<INodeID> forNodes) {
		/*
		 * Just a sample. We assign weights based on only one SiS Metric
		 */
		for (INodeID v : forNodes) {
			try {
				setWeight(v, getValue(v, typeMessageSize));
			} catch (InformationNotAvailableException e) {
				setWeight(v, 0);
			}
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw Infos
		typeMessageSize = SiSTypes.getType("Delta_SizeBypassCloudMessageReceive", Double.class);
		types.put(typeMessageSize, SiSRequest.NONE);
		typeLocation = SiSTypes.PHY_LOCATION;
		types.put(typeLocation, SiSRequest.NONE);

		// High Infos
		positionsHolder = new LimitedQueue<>(1); // Positions

		return types;
	}

}
