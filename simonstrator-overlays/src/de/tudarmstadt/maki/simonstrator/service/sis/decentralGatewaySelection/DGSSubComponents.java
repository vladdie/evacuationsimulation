package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient.DGSComponent;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer.ClosestNodeClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer.FirstHeardClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination.ContentionBasedDissemination;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination.Flooding;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination.ProbabilisticDissemination;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme.DTwoSteps;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme.SOneStep;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.NoLogicTrigger;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.PeriodicTrigger;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.CEMCA;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.EnLat;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.Scaled_WCA;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach.ALEACH;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach.DEEC;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach.LEACH;

/**
 * Helper class to have easier access to DGS subcomponent -names and -classes
 * 
 * @author Christoph Storm
 *
 */
public enum DGSSubComponents {

	// Trigger
	TIMERBASEDTRIGGER(DGSComponent.TRIGGER, "TIMERBASEDTRIGGER", PeriodicTrigger.class),

	NOLOGICTRIGGER(DGSComponent.TRIGGER, "NOLOGICTRIGGER", NoLogicTrigger.class),
	// Weighter
	// // LEACH
	ALEACH(DGSComponent.WEIGHTCOMPUTATION, "ALEACH", ALEACH.class),
	DEEC(DGSComponent.WEIGHTCOMPUTATION, "DEEC", DEEC.class),

	LEACH(DGSComponent.WEIGHTCOMPUTATION, "LEACH",
			LEACH.class),
	// // Utilitybased
	CEMCA(DGSComponent.WEIGHTCOMPUTATION, "CEMCA", CEMCA.class),

	ENLAT(DGSComponent.WEIGHTCOMPUTATION, "ENLAT", EnLat.class),

	SCALED_WCA(DGSComponent.WEIGHTCOMPUTATION, "SCALED_WCA", Scaled_WCA.class),
	// Scheme
	DTWOSTEPS(DGSComponent.SCHEME, "DTWOSTEPS", DTwoSteps.class),

	SONESTEP(DGSComponent.SCHEME, "SONESTEP",
			SOneStep.class),
	// Disseminator
	FLOODING(DGSComponent.DISSEMNINATION, "FLOODING", Flooding.class),

	CONTENTIONBASEDDISSEMINATION(
			DGSComponent.DISSEMNINATION, "CONTENTIONBASEDDISSEMINATION", ContentionBasedDissemination.class),

	PROBABILISTICDISSEMINATION(DGSComponent.DISSEMNINATION,
					"PROBABILISTICDISSEMINATION", ProbabilisticDissemination.class),
	// Clusterer
	CLOSESTNODECLUSTERER(DGSComponent.CLUSTERER, "CLOSESTNODECLUSTERER",
			ClosestNodeClusterer.class),

	FIRSTHEARDCLUSTERER(DGSComponent.CLUSTERER, "FIRSTHEARDCLUSTERER",
					FirstHeardClusterer.class);

	private DGSSubComponents(DGSComponent type, String simpleName, Class<? extends DGSSubComponent> classIdent) {
		this.type = type;
		this.simpleName = simpleName;
		this.classIdentifier = classIdent;
	};

	private final DGSComponent type;
	private final String simpleName;
	private final Class<? extends DGSSubComponent> classIdentifier;

	public DGSComponent getType() {
		return type;
	}

	public String getSimpleName() {
		return simpleName;
	}

	public Class<? extends DGSSubComponent> getImplementingClass() {
		return classIdentifier;
	}

	/**
	 * This enum also acts a {@link Transmitable} entity, where only the type is
	 * supposed to be transmitted.
	 */
	public static int getTransmissionSize() {
		/*
		 * The Idea is not to transmit the whole enum with all information, but
		 * just an identifier for the enumType, plus an identifier for the
		 * DGSComponent type. This should offer enough room for forward error
		 * correction.
		 */
		// 15 constants => 4 bits
		// 5 DGSComponents => 3 bits
		// Total: 1 bit short of a byte
		return 1;
	}
}
