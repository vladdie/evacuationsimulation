package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer.AggregationFunction;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.util.SiSRequestTripple;

/**
 * DEEC version of the LEACH weighting algorithm. Based of:
 * "Design of a distributed energy-efficient clustering algorithm for heterogeneous wireless sensor networks"
 * by Qing et al.
 * 
 * @author Christoph Storm
 * @version 2016.06.22
 */
public class DEEC extends LEACH {
	// TODO real SIS request

	private static boolean useMonitoring = true;

	// private int roundsAsCH = 0;

	@TransferState({ "Parent" })
	public DEEC(IDecentralGatewaySelectionClient parent) {
		super(parent);
		setClusterHeadExclusion(ClusterHeadExclusion.Last1OverP);
	}

	@Override
	public List<SiSRequestTripple> getRequiredInformationTypes() {
		List<SiSRequestTripple> list = new LinkedList<SiSRequestTripple>();
		if (useMonitoring) {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			SiSRequestTripple srt = new SiSRequestTripple(
					AggregationFunction.AVG,
					(SiSType) SiSTypes.ENERGY_BATTERY_LEVEL, SiSRequest.NONE);
			list.add(srt);
		}
		return list;
	}

	@Override
	public double calculateWeight(List<Object> args, List<Object> kwargs) {


		// get our own Energy
		double ownEnergy = 50d;
		// Energy is returned as value between 0 and 100;
		try {
			ownEnergy = parent.getSis().get().localState(
					SiSTypes.ENERGY_BATTERY_LEVEL, new SiSRequest());
		} catch (InformationNotAvailableException e) {
			// should not happen!
			e.printStackTrace();
		}

		// get the average Energy in this cluster...
		double avgEnergy;
		if (!args.isEmpty()) {
			avgEnergy = (Double) args.get(0);
		} else {
			avgEnergy = estimateAverageEnergy(ownEnergy);
		}

		// compute adjusted probability
		double adjProb = getBaseProbability() * ownEnergy / avgEnergy;

		// standard LEACH from here on...
		double threshold = 0;
		boolean computeThreshold = shouldComputeThreshold(adjProb);
		if (computeThreshold) {
			threshold = this.getLeachThreshold(adjProb, parent.getRound());
		}
		assert threshold <= 1;
		return threshold;
	}

	/**
	 * Value between 0 and 100!
	 * 
	 * @return
	 */
	private double estimateAverageEnergy(double ownEnergy) {

		double chOverhead = 0.2; // estimate

		// if (parent.getRole().equals(NodeRole.CLUSTERHEAD)) {
		// roundsAsCH++;
		// }
		//
		// if (parent.getRound() == 0) {
		// return ownEnergy;
		// }
		// double timeAsCH = (double) roundsAsCH / parent.getRound();
		//
		// // timeAsCH > prob --> more avgEnergy
		// double relation = timeAsCH - getBaseProbability();
		// // I'd actually use something like time of day for an estimate...
		// double avgEstimate = relation * chOverhead;
		// // Probably some crude, unbound value, do not use (or look too
		// closely
		// // at this)
		// avgEstimate = (1 + avgEstimate) * ownEnergy;
		double avgEstimate = 50d - parent.getRound() * chOverhead;

		return avgEstimate;
	}

	public static void setUseMonitoring(boolean employ) {
		useMonitoring = employ;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.DEEC;
	}

}
