package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.AStochasticGatewaySelection;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.ClosestToGatewaySinglehopClustering;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.IBestFitClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;

public abstract class SEC extends AStochasticGatewaySelection {

	public SEC(String name) {
		super(name, false);
	}

	@Override
	protected Map<INodeID, List<INodeID>> determineGateways(List<INodeID> outOf) {
		// Copy list
		Map<INodeID, Location> curLocations = positionsHolder.getLast();
		LinkedList<INodeID> nodes = new LinkedList<>(curLocations.keySet());
		LinkedList<INodeID> unknownNodes = new LinkedList<>(outOf);
		unknownNodes.removeAll(nodes);

		// Calculate probabilities
		calculateProbability(nodes);

		Map<INodeID, Location> gateways = new LinkedHashMap<>();
		Map<INodeID, Location> leafs = new LinkedHashMap<>();
		// Draw E(g) nodes
		for (INodeID node : nodes) {
			double threshold = rnd.nextDouble();
			if (threshold < getProbability(node)) {
				gateways.put(node, curLocations.get(node));
			} else {
				leafs.put(node, curLocations.get(node));
			}
		}

		// Calculate best fit cluster
		IBestFitClusterer clusterStrategy = new ClosestToGatewaySinglehopClustering();
		// IBestFitClusterer clusterStrategy = new ClosestToGatewayMultihopClustering();
		Map<INodeID, List<INodeID>> tree = clusterStrategy.getClusters(gateways, leafs);

		// The set of gateways should be never empty
		if (gateways.isEmpty()) {
			int i = rnd.nextInt(nodes.size());
			INodeID newGateway = nodes.get(i);
			gateways.put(newGateway, leafs.remove(newGateway));
		}

		// Add unknown nodes
		for (INodeID node : unknownNodes) {
			tree.put(node, new LinkedList<INodeID>());
		}

		return tree;

	}


	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationBySuperior() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw Infos
		typeLocation = SiSTypes.PHY_LOCATION;
		types.put(typeLocation, SiSRequest.NONE);

		// Higher Infos
		positionsHolder = new LimitedQueue<>(1); // Positions
		return types;
	}

}
