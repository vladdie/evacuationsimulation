package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.IClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.KppMeansClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.CLocation;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.CS;

public class CS_BECLEACH extends CS {

	// Weight Factors
	private double a_0 = 1;
	private double a_1 = 1;
	private double a_2 = 1;

	private IClusterer clusterer;

	public CS_BECLEACH() {
		super("CS_BECLEACH_KppMeans", true);
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw Infos
		typeEnergylevel = SiSTypes.ENERGY_BATTERY_LEVEL;
		types.put(typeEnergylevel, SiSRequest.NONE);
		typeLocation = SiSTypes.PHY_LOCATION;
		types.put(typeLocation, SiSRequest.NONE);
		typeEnergycapacity = SiSTypes.ENERGY_BATTERY_CAPACITY;
		types.put(typeEnergycapacity, SiSRequest.NONE);

		// High Infos
		positionsHolder = new LimitedQueue<>(1); // Positions
		normalizedEnergielevels = new HashMap<INodeID, Double>(); // Normalized energy levels

		return types;
	}

	@Override
	protected List<List<INodeID>> calculateCluster(List<INodeID> nodes) {
		Map<INodeID, Location> curLocations = positionsHolder.getLast();

		// clusterer = new GridDensityClusterer();
		// clusterer = new DBScanClusterer(TempClusteringConfig.minPTS);
		clusterer = new KppMeansClusterer(maxNumberOfGateways);

		List<List<INodeID>> cluster = new ArrayList<List<INodeID>>();
		cluster.addAll(clusterer.getClusters(curLocations));

		return cluster;
	}

	@Override
	protected void calculateProbability(List<INodeID> forNodes) {
		Map<INodeID, Location> curLocations = positionsHolder.getLast();

		// Get location of basestation
		Location locationBS = CLocation.getBaseStation();

		// Calculate center of gravity
		Location locationCoG = calcCenterOfGravity(forNodes);

		for (INodeID v : forNodes) {
			double e_v = normalizedEnergielevels.get(v);
			Location locationNode = curLocations.get(v);

			double d_CoG = locationCoG.distanceTo(locationNode);

			double d_BS = locationBS.distanceTo(locationNode);

			if (d_CoG == 0) {
				d_CoG = 1;
			}
			if (d_BS == 0) {
				d_BS = 1;
			}

			double w_v = a_0 * e_v + a_1 / d_CoG + a_2 / d_BS;
			setProbability(v, w_v);
		}
	}

	private Location calcCenterOfGravity(List<INodeID> forNodes) {
		Map<INodeID, Location> curLocations = positionsHolder.getLast();
		double x = 0;
		double y = 0;
		Location locationNode;
		for (INodeID node : forNodes) {
			locationNode = curLocations.get(node);
			x += locationNode.getLongitude();
			y += locationNode.getLatitude();
		}
		x = x / forNodes.size();
		y = y / forNodes.size();
		return new CLocation(x, y);
	}

}
