package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

public class DeclareCHMessage extends AbstractDGSMessage
		implements MessageWithClusterID, MessageWithTTL, MessageWithHopCount {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3804064179234392156L;

	private INodeID chID;
	private int ttl;
	private int hopcount;

	public DeclareCHMessage(int round, UniqueID msgID, INodeID chID, int ttl) {
		super(round, msgID);
		this.chID = chID;
		this.ttl = ttl;
		this.hopcount = 1;
	}

	public DeclareCHMessage(DeclareCHMessage msg) {
		super(msg);
		this.chID = msg.getClusterID();
		this.ttl = msg.getTTL();
		this.hopcount = msg.getHopcount();
	}

	@Override
	public Message getPayload() {
		return this;
	}

	@Override
	public void setTTL(int ttl) {
		this.ttl = ttl;
	}

	@Override
	public int getTTL() {
		return ttl;
	}

	@Override
	public INodeID getClusterID() {
		return chID;
	}

	@Override
	public long size() {
		// chID + ttl (4) + hopcount (4)
		return chID.getTransmissionSize() + 8;
	}

	@Override
	public int getHopcount() {
		return this.hopcount;
	}

	@Override
	public void increaseHopcount() {
		hopcount++;
	}

}
