package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.D1C;

/**
 * Just pick a random gateway
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class D1C_Random extends D1C {

	protected Random rnd;

	public D1C_Random() {
		super("D1C_Random", false);
		rnd = Randoms.getRandom(D1C_Random.class);
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		return Collections.emptyMap(); // No SiS interaction is required
	}

	@Override
	protected void calculateWeights(List<INodeID> forNodes) {
		for (INodeID v : forNodes) {
			setWeight(v, rnd.nextInt(forNodes.size()));
		}
	}

}
