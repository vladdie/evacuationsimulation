package de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.TempClusteringConfig;

public class ClosestToGatewaySinglehopClustering implements IBestFitClusterer {

	private double maxRange;

	public ClosestToGatewaySinglehopClustering() {
		this.maxRange = TempClusteringConfig.maxRange;
	}

	public Map<INodeID, List<INodeID>> getClusters(Map<INodeID, Location> gateways, Map<INodeID, Location> leafs) {

		// Create result tree
		// XXX: Speed by Hashmap <Leaf>-><Gateway>
		Map<INodeID, List<INodeID>> tree = new LinkedHashMap<>();
		for (INodeID gw : gateways.keySet()) {
			tree.put(gw, new LinkedList<INodeID>());
		}

		Location locLeaf;
		double minDistance, curDistance;
		INodeID closestGateway;

		boolean allConnected = false;
		while (!allConnected && !leafs.isEmpty()) {
			for (INodeID leaf : leafs.keySet()) {
				locLeaf = leafs.get(leaf);
				minDistance = Double.MAX_VALUE;
				closestGateway = null;
				for (INodeID curGW : gateways.keySet()) {
					curDistance = locLeaf.distanceTo(gateways.get(curGW));
					if (curDistance < minDistance && curDistance <= maxRange) {
						minDistance = curDistance;
						closestGateway = curGW;
					}
				}
				if (closestGateway != null) {
					tree.get(closestGateway).add(leaf);
					allConnected = true;
				} else {
					// Move current leaf to gateways
					gateways.put(leaf, leafs.remove(leaf));
					// At least one leaf is not in range of a gateway
					allConnected = false;
					// Restart
					break;
				}
			}
			if (!allConnected) {
				// Reset tree
				tree = new LinkedHashMap<>();
				for (INodeID gw : gateways.keySet()) {
					tree.put(gw, new LinkedList<INodeID>());
				}
				continue;
			}
		}

		return tree;
	}

}
