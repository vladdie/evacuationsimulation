package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.CentralGatewaySelectionMessage;

/**
 * Centralized client stub of the GatewaySelection.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CentralizedGatewaySelectionClient extends AbstractGatewaySelectionClient {

	private MessageBasedTransport cellularTransport;

	@TransferState({ "Host" })
	public CentralizedGatewaySelectionClient(Host host) {
		super(host);
	}

	@Override
	public void startMechanism(Callback cb) {
		// anything else to do?
		try {
			if (this.getSis() == null) {
				this.setSis(getHost().getComponent(SiSComponent.class));
			}
			cellularTransport = this.getHost().getTransportComponent().getProtocol(UDP.class,
					getHost().getNetworkComponent().getByName(NetInterfaceName.MOBILE).getLocalInetAddress(),
					CentralizedGatewaySelectionServer.cellularCGSPort);
			cellularTransport.setTransportMessageListener(this);
		} catch (ProtocolNotAvailableException | ComponentNotAvailableException e) {
			cb.finished(false);
		}
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		// anything else to do?

		cellularTransport.removeTransportMessageListener();

		cb.finished(true);
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {

		if (msg instanceof CentralGatewaySelectionMessage) {

			this.setGateway(((CentralGatewaySelectionMessage) msg).getGateway());
			this.setNodeRole(((CentralGatewaySelectionMessage) msg).getNodeRole());

			// inform the listeners
			for (GatewaySelectionListener listener : getGatewaySelectionListeners()) {
				listener.gatewaySelectionFinished(getNodeRole(), getGateway());
			}
		}
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// TODO Auto-generated method stub

	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		// TODO Auto-generated method stub

	}

}
