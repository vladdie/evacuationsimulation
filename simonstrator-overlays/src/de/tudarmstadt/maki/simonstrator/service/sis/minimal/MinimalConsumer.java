package de.tudarmstadt.maki.simonstrator.service.sis.minimal;

import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationResolver;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSResultCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;

/**
 * A very basic implementation of the {@link SiSInformationConsumer} interface.
 * All business logic is implemented in the {@link SiSInformationResolver}.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class MinimalConsumer implements SiSInformationConsumer {

	private final SiSInformationResolver resolver;

	public MinimalConsumer(SiSInformationResolver resolver) {
		this.resolver = resolver;
	}

	@Override
	public <T> T localState(SiSType<T> type, SiSRequest request)
			throws InformationNotAvailableException {
		assert request != null;
		T localValue = resolver.resolveLocally(null, type, request);
		if (localValue == null) {
			throw new InformationNotAvailableException();
		}
		return localValue;
	}

	@Override
	public <T> T localObservationOf(INodeID observedNode, SiSType<T> type,
			SiSRequest request) throws InformationNotAvailableException {
		assert request != null;
		T localValue = resolver.resolveLocally(observedNode, type, request);
		if (localValue == null) {
			throw new InformationNotAvailableException();
		}
		return localValue;
	}

	@Override
	public <T> T remoteObservationOf(INodeID observer, INodeID observedNode,
			SiSType<T> type, SiSRequest request)
			throws InformationNotAvailableException {
		throw new AssertionError("Not yet implemented");
	}

	@Override
	public <T> Map<INodeID, T> allLocalObservations(SiSType<T> type,
			SiSRequest request) {
		return resolver.getAllLocalObservations(type, request);
	}

	@Override
	public <T> SiSConsumerHandle aggregatedObservation(
			AggregationFunction aggFunction, SiSType<T> type,
			SiSRequest request, SiSResultCallback<T> callback) {
		return resolver.aggregatedObservation(aggFunction, type, request,
				callback);
	}

	@Override
	public <T> SiSConsumerHandle rawObservations(SiSType<T> type,
			SiSRequest request, SiSResultCallback<Map<INodeID, T>> callback) {
		return resolver.rawObservations(type, request, callback);
	}

	@Override
	public void revoke(SiSConsumerHandle handle) {
		resolver.removeConsumer(handle);
	}

}
