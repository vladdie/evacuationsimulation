package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer.IClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.LimitedQueue;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.CD;

public abstract class CD_Random extends CD {

	protected IClusterer clusterer;
	private Random rnd;

	public CD_Random(boolean needsNumberOfGateway) {
		super("CD_Random_KppMeans", needsNumberOfGateway);
		rnd = Randoms.getRandom(CD_Random.class);
	}

	@Override
	protected void calculateWeights(List<INodeID> forNodes) {
		for (INodeID v : forNodes) {
			setWeight(v, rnd.nextInt(forNodes.size()));
		}
	}

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationByStrategy() {
		Map<SiSType<?>, SiSRequest> types = new LinkedHashMap<>();

		// Raw Infos
		typeLocation = SiSTypes.PHY_LOCATION;
		types.put(typeLocation, SiSRequest.NONE);

		// High Infos
		positionsHolder = new LimitedQueue<>(1); // Positions

		return types;
	}

}
