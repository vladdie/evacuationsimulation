package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;

public class GatewaySelectionClientBootstrapMessage implements Message {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5997638889265169063L;

	private GatewayControllerMessage controllerMessage;
	private Message clientMessage;

	public GatewaySelectionClientBootstrapMessage(GatewayControllerMessage controllerMessage, Message clientMessage) {
		this.controllerMessage = controllerMessage;
		this.clientMessage = clientMessage;
	}

	@Override
	public long getSize() {
		long size = controllerMessage.getSize();
		if (clientMessage != null) {
			size += clientMessage.getSize();
		}
		return size;
	}

	@Override
	public Message getPayload() {
		// the payload is two messages!
		return null;
	}

	public Message getControllerMessage() {
		return controllerMessage;
	}

	public Message getClientMessage() {
		return clientMessage;
	}

}
