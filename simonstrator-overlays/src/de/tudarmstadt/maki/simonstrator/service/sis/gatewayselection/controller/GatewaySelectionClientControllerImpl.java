package de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ConnectivityListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.BootstrapEntity;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.CentralizedGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionListener;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.GSClientBootstrapRequestMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.GatewayControllerMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.GatewaySelectionClientBootstrapMessage;

/**
 * This is responsible for transitions between Central and Decentral GS Clients,
 * as well as Bootstrapping
 *
 */
public class GatewaySelectionClientControllerImpl
		implements GatewaySelectionClientController, TransMessageListener, ConnectivityListener,
		GatewaySelectionListener {
	// FIXME local wifi Bootstraping currently not supported!

	private static String clientProxy = "GatewaySelectionClient";

	private Host host;
	private TransitionEngine transEngine;
	private GatewaySelectionClient selectionClient;

	// Connection to the server
	private MessageBasedTransport cellularConnection;

	// for local bootstrapping
	private MessageBasedTransport localConnection;

	// Maintain State information in boolean (isCentralized) or Enum Form?

	private enum clientForm {
		centralized, decentralized
	};

	private clientForm currentClientstate;

	// used as abstraction between the transitionEnabled Clients and
	// Applications
	// This is used so applications can instantiate this, and immediately set
	// themselves as listeners
	private List<GatewaySelectionListener> listeners = new LinkedList<GatewaySelectionListener>();


	public GatewaySelectionClientControllerImpl(Host host) {
		this.host = host;

		try {

			transEngine = host.getComponent(TransitionEngine.class);

			cellularConnection = host.getTransportComponent()
					.getProtocol(UDP.class,
							host.getNetworkComponent().getByName(NetInterfaceName.MOBILE).getLocalInetAddress(),
							GatewaySelectionServerController.controllerPort);

			localConnection = host.getTransportComponent().getProtocol(UDP.class,
					host.getNetworkComponent().getByName(NetInterfaceName.WIFI).getLocalInetAddress(),
					GatewaySelectionServerController.controllerPort);

			host.getNetworkComponent().getByName(NetInterfaceName.MOBILE).addConnectivityListener(this);

			cellularConnection.setTransportMessageListener(this);
			localConnection.setTransportMessageListener(this);
		} catch (ProtocolNotAvailableException | ComponentNotAvailableException e) {
			throw new RuntimeException(e);
		}

		selectionClient = new CentralizedGatewaySelectionClient(host);
		selectionClient.addGatewaySelectionListener(this);
		selectionClient = transEngine.createMechanismProxy(GatewaySelectionClient.class, selectionClient, clientProxy);
		currentClientstate = clientForm.centralized;
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		if (msg instanceof GatewayControllerMessage) {
			boolean centralizedClient = ((GatewayControllerMessage) msg).isUseCentralizedClient();
			if ((centralizedClient && currentClientstate == clientForm.decentralized)
					|| (!centralizedClient && currentClientstate == clientForm.centralized)) {
				toggleClient();
			}
		} else if (msg instanceof GatewaySelectionClientBootstrapMessage) {
			GatewaySelectionClientBootstrapMessage bsMsg = (GatewaySelectionClientBootstrapMessage) msg;
			messageArrived(bsMsg.getControllerMessage(), sender, commID);

			if (bsMsg.getClientMessage() != null) {
				// update the selection client, but give it enough time to
				// initialize first
				// first...
				Event.scheduleWithDelay(1L, new EventHandler() {
					@Override
					public void eventOccurred(Object content, int type) {
						selectionClient.messageArrived((Message) content, sender, commID);
					}
				}, bsMsg.getClientMessage(), 12);
			}
		}
		// TODO else if Bootstrapping request
	}

	private void toggleClient() {
		switch (currentClientstate) {
		case centralized:
			transEngine.executeAtomicTransition(clientProxy, DecentralGatewaySelectionClient.class);
			currentClientstate = clientForm.decentralized;
			return;
		case decentralized:
			transEngine.executeAtomicTransition(clientProxy, CentralizedGatewaySelectionClient.class);
			currentClientstate = clientForm.centralized;
			return;
		}
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// Send Bootstrapping request...
		GSClientBootstrapRequestMessage req = new GSClientBootstrapRequestMessage(this.host.getId());
		cellularConnection.send(req, BootstrapEntity.getServerContact().getNetId(),
				BootstrapEntity.getServerContact().getPort());
		// use sendAndWait ?

		BootstrapEntity.register(host.getNetworkComponent().getByName(NetInterfaceName.MOBILE));
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		BootstrapEntity.deregister(host.getNetworkComponent().getByName(NetInterfaceName.MOBILE));
	}

	@Override
	public boolean isGateway() {
		return selectionClient.isGateway();
	}

	@Override
	public INodeID getGateway() {
		return selectionClient.getGateway();
	}

	@Override
	public NodeRole getNodeRole() {
		return selectionClient.getNodeRole();
	}

	@Override
	public void addGatewaySelectionListener(GatewaySelectionListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeGatewaySelectionListener(GatewaySelectionListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void gatewaySelectionFinished(NodeRole role, INodeID clusterhead) {
		for (GatewaySelectionListener listener : listeners) {
			listener.gatewaySelectionFinished(role, clusterhead);
		}
	}
}
