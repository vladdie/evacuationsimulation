package de.tudarmstadt.maki.simonstrator.service.sis.test;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSDataCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInfoProperties;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInfoProperties.SiSScope;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider.SiSProviderHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransportComponent;
import de.tudarmstadt.maki.simonstrator.service.sis.minimal.MinimalSiSComponent;

/**
 * These test methods follow a story to prevent lengthy initialization phases
 * for each method. An implementation of a {@link SiSComponent} has to fulfill
 * all tests.
 * 
 * @author Bjoern Richerzhagen
 *
 */

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SiSLocalDataTest {

	private static SiSComponent sis;

	private static double VALUE_ONE = 2;

	private static double VALUE_TWO = 4;

	private static double VALUE_OBSERVATION_OTHER_NODE = 10;

	private static SiSProviderHandle handleObservationOtherNode = null;

	private static SiSType<Double> TYPE = SiSTypes.TEST_DOUBLE;

	private static INodeID nodeOne = INodeID.get(1);

	private static INodeID nodeTwo = INodeID.get(2);

	@BeforeClass
	public static void setUpEnvironment() {
		MinimalSiSComponent.Factory f = new MinimalSiSComponent.Factory();
		sis = f.createComponent(new Host() {

			@Override
			public <T extends HostComponent> boolean removeComponent(T component) {
				return false;
			}

			@Override
			public <T extends HostComponent> void registerComponent(T component) {
				// TODO Auto-generated method stub
			}

			@Override
			public TransportComponent getTransportComponent() {
				return null;
			}

			@Override
			public NetworkComponent getNetworkComponent() {
				return null;
			}

			@Override
			public INodeID getId() {
				return nodeOne;
			}

			@Override
			public long getHostId() {
				return nodeOne.value();
			}

			@Override
			public <T extends HostComponent> List<T> getComponents(
					Class<T> componentClass)
					throws ComponentNotAvailableException {
				return null;
			}

			@Override
			public <T extends HostComponent> T getComponent(
					Class<T> componentClass)
					throws ComponentNotAvailableException {
				return null;
			}
		});
		sis.initialize();
	}

	@Before
	public void setUp() throws Exception {
		//
	}

	@After
	public void tearDown() throws Exception {
		//
	}

	@AfterClass
	public static void tearDownEnvironment() {
		sis.shutdown();
		sis = null;
	}

	@Test
	public void test001_NoLocalProvider() {
		try {
			sis.get().localState(TYPE, SiSRequest.NONE);
			Assert.fail("Information should not be available");
		} catch (InformationNotAvailableException e) {
			//
		}
	}

	@Test
	public void test002_ProvideLocalData() {
		sis.provide().nodeState(TYPE, new SiSDataCallback<Double>() {

			Set<INodeID> observedNodes = new LinkedHashSet<>();

			@Override
			public Double getValue(INodeID nodeId,
					SiSProviderHandle providerHandle)
					throws InformationNotAvailableException {
				if (nodeId.equals(nodeOne)) {
					return VALUE_ONE;
				}
				throw new InformationNotAvailableException();
			}

			@Override
			public Set<INodeID> getObservedNodes() {
				observedNodes.add(nodeOne);
				return observedNodes;
			}

			@Override
			public SiSInfoProperties getInfoProperties() {
				return SiSInfoProperties.NONE;
			}
		});
		try {
			Assert.assertEquals(VALUE_ONE,
					(double) sis.get().localState(TYPE, SiSRequest.NONE), 0d);
		} catch (InformationNotAvailableException e) {
			Assert.fail("Information not available");
		}
	}

	@Test
	public void test003_LocalProviderShouldBeAvailableNow() {
		try {
			sis.get().localState(TYPE, SiSRequest.NONE);
		} catch (InformationNotAvailableException e) {
			Assert.fail("Information should now be available");
		}
	}

	@Test
	public void test004_ProvideLocalDataWithSourceClass() {
		final SiSInfoProperties localData = new SiSInfoProperties().setScope(
				SiSScope.NODE_LOCAL).setSourceComponent(SiSLocalDataTest.class);
		sis.provide().nodeState(TYPE, new SiSDataCallback<Double>() {
			Set<INodeID> observedNodes = new LinkedHashSet<>();

			@Override
			public Double getValue(INodeID nodeId,
					SiSProviderHandle providerHandle)
					throws InformationNotAvailableException {
				if (nodeId.equals(nodeOne)) {
					return VALUE_TWO;
				}
				throw new InformationNotAvailableException();
			}

			@Override
			public Set<INodeID> getObservedNodes() {
				observedNodes.add(nodeOne);
				return observedNodes;
			}

			@Override
			public SiSInfoProperties getInfoProperties() {
				return localData;
			}
		});
		// cannot fail
		Assert.assertTrue(true);
	}

	@Test
	public void test005_RequestSourcedLocalData() {
		final SiSRequest localData = new SiSRequest().setScope(
				SiSScope.NODE_LOCAL).setSourceComponent(SiSLocalDataTest.class);
		try {
			Assert.assertEquals(VALUE_TWO,
					(double) sis.get().localState(TYPE, localData), 0d);
		} catch (InformationNotAvailableException e) {
			Assert.fail("Information not available");
		}
	}

	@Test
	public void test009_CheckLocalNodeIDs() {
		Assert.assertTrue("We are node one.",
				sis.getHost().getId().equals(nodeOne));
		Assert.assertTrue("We are not node two.", !sis.getHost().getId()
				.equals(nodeTwo));
	}

	@Test
	public void test010_RequestObservedDataForOwnNode() {
		try {
			final SiSRequest localData = new SiSRequest().setScope(
					SiSScope.NODE_LOCAL).setSourceComponent(
					SiSLocalDataTest.class);

			Assert.assertEquals(VALUE_TWO, (double) sis.get()
					.localObservationOf(nodeOne, TYPE, localData), 0d);
		} catch (InformationNotAvailableException e) {
			Assert.fail("Node one equals to our own local node - information should be available");
		}
	}

	@Test
	public void test012_RequestObservedDataForOtherNode() {
		try {
			final SiSRequest localData = new SiSRequest().setScope(
					SiSScope.NODE_LOCAL).setSourceComponent(
					SiSLocalDataTest.class);
			sis.get().localObservationOf(nodeTwo, TYPE, localData);
			Assert.fail("No information should be available as of now.");
		} catch (InformationNotAvailableException e) {
			// OK
		}
	}

	@Test
	public void test013_ProvideObservedDataForOtherNode() {
		final SiSInfoProperties localData = new SiSInfoProperties().setScope(
				SiSScope.NODE_LOCAL).setSourceComponent(SiSLocalDataTest.class);
		handleObservationOtherNode = sis.provide().nodeState(TYPE,
				new SiSDataCallback<Double>() {
					Set<INodeID> observedNodes = new LinkedHashSet<>();

					@Override
					public Double getValue(INodeID nodeId,
							SiSProviderHandle providerHandle)
							throws InformationNotAvailableException {
						if (nodeId.equals(nodeTwo)) {
							return VALUE_OBSERVATION_OTHER_NODE;
						}
						throw new InformationNotAvailableException();
					}

					@Override
					public Set<INodeID> getObservedNodes() {
						observedNodes.add(nodeTwo);
						return observedNodes;
					}

					@Override
					public SiSInfoProperties getInfoProperties() {
						return localData;
					}
				});
	}

	@Test
	public void test014_RetrieveAllObservations() {
		final SiSRequest localData = new SiSRequest().setScope(
				SiSScope.NODE_LOCAL).setSourceComponent(
				SiSLocalDataTest.class);
		Map<INodeID, Double> result = sis.get().allLocalObservations(TYPE,
				localData);
		Assert.assertNotNull(result);
		Assert.assertTrue("Two observed nodes.", result.size() == 2);
		Assert.assertNotNull(result.get(nodeTwo));
		Assert.assertEquals(VALUE_OBSERVATION_OTHER_NODE,
				(double) result.get(nodeTwo), 0d);
	}

	/*
	 * Revoke a handler
	 */

	@Test
	public void test015_RevokeObservedDataForOtherNode() {
		sis.provide().revoke(handleObservationOtherNode);
		final SiSRequest localData = new SiSRequest().setScope(
				SiSScope.NODE_LOCAL).setSourceComponent(
				SiSLocalDataTest.class);
		Map<INodeID, Double> result = sis.get().allLocalObservations(TYPE,
				localData);
		Assert.assertNotNull(result);
		Assert.assertTrue("No observations", result.size() == 1);
	}

	/*
	 * Re-enable the information provider to test the graph interface
	 */

	@Test
	public void test016_ProvideObservedDataForOtherNode() {
		final SiSInfoProperties localData = new SiSInfoProperties().setScope(
				SiSScope.NODE_LOCAL).setSourceComponent(SiSLocalDataTest.class);
		handleObservationOtherNode = sis.provide().nodeState(TYPE,
				new SiSDataCallback<Double>() {
					Set<INodeID> observedNodes = new LinkedHashSet<>();

					@Override
					public Double getValue(INodeID nodeId,
							SiSProviderHandle providerHandle)
							throws InformationNotAvailableException {
						if (nodeId.equals(nodeTwo)) {
							return VALUE_OBSERVATION_OTHER_NODE;
						}
						throw new InformationNotAvailableException();
					}

					@Override
					public Set<INodeID> getObservedNodes() {
						observedNodes.add(nodeTwo);
						return observedNodes;
					}

					@Override
					public SiSInfoProperties getInfoProperties() {
						return localData;
					}
				});
	}

}
