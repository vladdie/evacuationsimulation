package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponent;

/**
 * Implementations are responsible for the DGS scheme after the local weight is
 * computed. I.e., whether and in what format this weight is distributed, how
 * the Gateway is selected, and how this information is distributed again.
 * 
 * @author Christoph Storm
 *
 */
public interface DGSScheme extends DGSSubComponent {

	/**
	 * Starts a new round of this GS scheme. Because the way to gather
	 * information for the weight computation is expected to always stay the
	 * same, that should done by the DGS main class. This is responsible for
	 * everything after the weight computation.
	 * 
	 * @param weight
	 */
	public void startScheme(double weight);

	/**
	 * The DGS Main class takes care to redirect any inbound DGS messages here.
	 * The Scheme itself is responsible to pass eligible msgs to the
	 * dissemination.
	 * 
	 * @param msg
	 */
	public void onMessageArrived(Message msg);

	/**
	 * Fall-back method to be called the DGS main class in case that clustering
	 * was not successful. Should also be used by the scheme, when it decides
	 * this node is CH
	 * 
	 * @return
	 */
	public void onBecameClusterHead();

}
