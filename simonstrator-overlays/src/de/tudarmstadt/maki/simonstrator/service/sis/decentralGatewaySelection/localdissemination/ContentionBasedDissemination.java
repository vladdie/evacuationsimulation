package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithUniqueID;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;

/**
 * Dissemination based on contention scheme. Waits up to a maximum value, based
 * on the nodes Battery level or the given weight. <b> To function as intended
 * this Disseminator is dependend on Messages implementing
 * {@link MessageWithUniqueID}!</b>
 * 
 * @author Christoph Storm
 * @version 2016.06.24
 */
public class ContentionBasedDissemination extends AbstractDissemination {
	// TODO Force usage of 'MessageWithUniqueID'!!!

	private static long maxContention = 5 * Time.SECOND;

	private NetID broadcast;
	private List<UniqueID> messagesToSend;

	@TransferState({ "Parent" })
	public ContentionBasedDissemination(IDecentralGatewaySelectionClient parent) {
		super(parent);
		messagesToSend = new LinkedList<UniqueID>();
	}

	@Override
	public void startMechanism(Callback cb) {
		super.startMechanism();
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		super.stopMechanism();
		broadcast = null;
		messagesToSend.clear();

		cb.finished(true);
	}

	@Override
	public void onMessageReceived(Message msg) {
		
		if(msg instanceof MessageWithUniqueID){
			UniqueID msgID = ((MessageWithUniqueID) msg).getUniqueMsgID();
			if (messagesToSend.contains(msgID)) {
				// Do not send this message, someone else already did
				messagesToSend.remove(msgID);
				return;
			}
		}
		// Maybe we heard the message before?
		super.onMessageReceived(msg);
	}

	@Override
	public void disseminate(Message msg) {
		// Energy based contention?! Seems about right
		double batteryLevel = 0.5d;
		try {
			batteryLevel = getParent()
					.getSis()
					.get()
					.localState(
					SiSTypes.ENERGY_BATTERY_LEVEL, new SiSRequest());

			batteryLevel /= 100;

		} catch (InformationNotAvailableException e) {
			// should not happen!
			e.printStackTrace();
		}
		disseminate(msg, batteryLevel);
	}

	@Override
	public void disseminate(Message msg, double weight) {
		long contention = 0L;
		if (weight < 1d) {
			contention = (long) ((1 - weight) * maxContention);
		}

		messagesToSend.add(((MessageWithUniqueID) msg).getUniqueMsgID());

		Event.scheduleWithDelay(contention, new EventHandler() {

			@Override
			public void eventOccurred(Object content, int type) {
				Message msg = (Message) content;
				if (msg instanceof MessageWithUniqueID) {
					UniqueID uid = ((MessageWithUniqueID) msg).getUniqueMsgID();
					if (!messagesToSend.remove(uid)) {
						return;
					}
				}
				sendMessage(msg, broadcast);

			}
		}, msg, 062304);
	}

	public static void setMaxContention(long time) {
		maxContention = time;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.CONTENTIONBASEDDISSEMINATION;
	}

	@Override
	void init() {
		NetInterface wifi = getParent().getHost().getNetworkComponent().getByName(NetInterfaceName.WIFI);
		broadcast = wifi.getBroadcastAddress();
	}

}
