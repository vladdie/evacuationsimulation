package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponent;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.util.SiSRequestTripple;

/**
 * Implementing classes are responsible to calculate the weight or probability
 * of a Node becoming Clusterhead. To simplify further processing,
 * implementations have to take care that higher values correspond to a higher
 * chance of becoming CH.
 * 
 * @author Christoph Storm
 */
public interface WeightComputation extends DGSSubComponent {

	/**
	 * This method returns a list with all SiSTypes required of neighbouring
	 * Nodes to compute the local weight. Expected are Aggregates. If only local
	 * Data is needed, an empty list is returned.
	 * 
	 * @return Required types of neighbourhood information.
	 */
	public List<SiSRequestTripple> getRequiredInformationTypes();

	/**
	 * Calculates the weight of this node. Expects as input an Array of
	 * Aggregates in the <b>exact same order as specified by
	 * {@link getRequiredInformationTypes}</b>. <br>
	 * Implementations also <b>must expect null values</b>, as placeholder for
	 * not retrievable information. <br>
	 * Should be called only once per Gateway Selection round.
	 * <p>
	 * <b> Attention:</b> To simplify and unify the further handling, a higher
	 * value returned corresponds to a higher chance to become Clusterhead.
	 * Algorithms based on lower values for higher chance need to be adjusted
	 * correspondingly! The returned Value also should be normalized between 0
	 * and 1.
	 * 
	 * @param args
	 *            arguments as specified in {@link getRequiredInformationTypes}
	 *            and in exactly the same ordering
	 * @param kwargs
	 *            additional arguments that may be used by some implementations
	 * @return the weight or probability of this node becoming a Clusterhead.
	 *         Higher values mean better chances. Value should be normalized
	 *         between 0 and 1.
	 */
	public double calculateWeight(List<Object> args, List<Object> kwargs);

	/**
	 * This sets the base probability of this node becoming a gateway.
	 * Implementing classes may use more refined means to compute the returned
	 * <i>weight</i>, but this value is still needed for transitions to
	 * LEACH-based algorithms
	 * 
	 * @param p
	 */
	public void setBaseProbability(Double p);
}
