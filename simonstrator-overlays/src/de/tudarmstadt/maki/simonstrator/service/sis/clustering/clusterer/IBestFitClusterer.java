package de.tudarmstadt.maki.simonstrator.service.sis.clustering.clusterer;

import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

public interface IBestFitClusterer {

	public Map<INodeID, List<INodeID>> getClusters(Map<INodeID, Location> gateways, Map<INodeID, Location> leafs);

}
