package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.IEdge;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer.AggregationFunction;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.AggregationNotPossibleException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.util.SiSRequestTripple;

/**
 * This is pretty much an offshoot of WCA, based on:
 * "Connectivity, Energy and Mobility Driven Clustering Algorithm for Mobile Ad Hoc Networks"
 * by Tolba et al.
 * 
 * @author Christoph Storm
 * @version 2016.06.23
 */
public class CEMCA implements WeightComputation {
	// TODO real SiSRequest
	private final int limitNeighbourhoodToHops = 1;

	@TransferState({ "Parent" })
	private IDecentralGatewaySelectionClient parent;

	private double lastMeanDistance = 0d;
	private long lastTime = 0l;

	@TransferState({ "BaseProbability" })
	// This doesn't seem right...
	private Double idealClusterSize = 10d;

	@TransferState({ "Parent" })
	public CEMCA(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public void startMechanism(Callback cb) {
		// nothing to do here
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		parent = null;
		cb.finished(true);
	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SiSRequestTripple> getRequiredInformationTypes() {
		List<SiSRequestTripple> list = new LinkedList<SiSRequestTripple>();
		// Need the number of Nodes in total, or in this cluster...
		// Also need a 'maximum Energy' of whatever

		SiSRequestTripple maxEnergy = new SiSRequestTripple(
				AggregationFunction.MAX,
				(SiSType) SiSTypes.ENERGY_BATTERY_LEVEL, new SiSRequest());

		SiSRequestTripple nodes = new SiSRequestTripple(
				AggregationFunction.COUNT,
				(SiSType) SiSTypes.ENERGY_BATTERY_LEVEL, new SiSRequest());

		list.add(maxEnergy);
		list.add(nodes);

		return list;
	}

	@Override
	public double calculateWeight(List<Object> args, List<Object> kwargs) {
		// Failfast! If anything is wrong with args, fail immediately

		// fixed variables
		double sMax = 20d; // m/s or so...
		double maxEnergy = (double) args.get(0);
		double N = (double) args.get(1);

		// get the graph
		Graph neighbourhood = null;
		try {
			neighbourhood = parent.getSis().get().localState(SiSTypes.NEIGHBORS_WIFI, new SiSRequest());

			if (!neighbourhood.containsNode(parent.getHost().getId())) {
				Monitor.log(CEMCA.class, Level.ERROR, "Node %s is not included in wifi Graph!",
						parent.getHost().getId());
				return 0d;
			}

			neighbourhood = neighbourhood.getLocalView(parent.getHost().getId(), limitNeighbourhoodToHops, true);
		} catch (InformationNotAvailableException | IllegalStateException e2) {
			e2.printStackTrace();
		}

		// Variables for the last step;
		double s = getRelativeSpeed(neighbourhood, sMax);
		double c = getDegreeDifference(neighbourhood);
		double e = 50d;
		
		try {
			e =	parent.getSis().get().localState(
					SiSTypes.ENERGY_BATTERY_LEVEL, new SiSRequest());
		} catch (InformationNotAvailableException e1) {
			e1.printStackTrace();
		}
		
		//Computation:
		s = 1 - s / sMax;
		// The paper has it reversed, which does not make any sense...
		
		c = c / (N - idealClusterSize);
		e = e / maxEnergy;

		double weight = (s + c + e) / 3;

		return weight;
	}

	private double getDegreeDifference(Graph neighbourhood) {
		int numOfNeighbours = 0;
		if (neighbourhood != null) {
			numOfNeighbours = neighbourhood.getIncomingEdges(
					parent.getHost().getId()).size();
		}
		numOfNeighbours++;
		return Math.abs(idealClusterSize - numOfNeighbours);
	}

	private double getRelativeSpeed(Graph neighbourhood, double sMax) {

		// Can't use PHY_DISTANCE directly...
		/*
		 * Map<INodeID, Double> distances = parent.getSis().get()
		 * .allLocalObservations(SiSTypes.PHY_DISTANCE, SiSRequest.NONE);
		 */
		double meanDistance = Double.NaN;
		if (neighbourhood != null) {
			Set<IEdge> edges = neighbourhood.getIncomingEdges(parent.getHost().getId());
			List<Double> distances = new LinkedList<Double>();
			for (IEdge edge : edges) {
				if (edge.getProperty(SiSTypes.PHY_DISTANCE) != null)
					distances.add(edge.getProperty(SiSTypes.PHY_DISTANCE));
			}

			try {
				meanDistance = SiSTypes.PHY_DISTANCE.aggregate(distances, AggregationFunction.AVG);
			} catch (AggregationNotPossibleException e1) {
				// nope..
			}
		}

		long deltaT = Time.getCurrentTime() - lastTime;
		if (!Double.isNaN(meanDistance) && deltaT > 0) {
			double s = Math.abs(meanDistance - lastMeanDistance) * Time.SECOND
					/ deltaT;
			lastMeanDistance = meanDistance;
			lastTime = Time.getCurrentTime();
			return s;
		}
		return sMax;
	}

	/**
	 * For Transferstate...
	 * 
	 * @param p
	 */
	@Override
	public void setBaseProbability(Double p) {
		assert p > 0 : "Probability must be larger than zero!";
		assert p <= 1 : "Probability must be smaller or equall to one!";
		idealClusterSize = (1 / p);
		assert idealClusterSize > 0;
	}

	public Double getBaseProbability() {
		return (1d / idealClusterSize);
	}

	public void setIdealClusterSize(int clusterSize) {
		this.idealClusterSize = (double) clusterSize;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.CEMCA;
	}

}
