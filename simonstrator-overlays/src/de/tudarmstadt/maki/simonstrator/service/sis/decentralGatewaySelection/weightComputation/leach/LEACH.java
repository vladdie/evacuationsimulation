package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.leach;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.util.SiSRequestTripple;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.WeightComputation;

/**
 * Base LEACH weighting and Methods for all LEACH variants. <br>
 * Original paper:
 * "Low Energy Adaptive Clustering Hierarchy with Deterministic Cluster head Selection"
 * by Handy et al.
 * 
 * @author Christoph Storm
 * @version 2016.06.22
 */
public class LEACH implements WeightComputation {

	public enum ClusterHeadExclusion {
		Last1OverP, // Original LEACH Algorithm excludes all CHs for the next
					// 1/p (or #Nodes/#CH) Rounds
		Last, // ALEACH seems to only exclude the Last CH nodes
		None // ALL Nodes participate
	};

	private ClusterHeadExclusion useHistory = ClusterHeadExclusion.Last1OverP;

	@TransferState({ "Parent" })
	IDecentralGatewaySelectionClient parent;

	// This is a base probability of becoming CH
	@TransferState({ "BaseProbability" })
	private Double baseProbability = 0.1;

	// @TransferState({ "LastRoundAsCH" })
	private int lastRoundAsCH = Integer.MIN_VALUE + 1;

	@TransferState({ "Parent" })
	public LEACH(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public void startMechanism(Callback cb) {
		// passive class;
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		parent = null;
		cb.finished(true);
	}

	@Override
	public IDecentralGatewaySelectionClient getParent() {
		return parent;
	}

	@Override
	public void setParent(IDecentralGatewaySelectionClient parent) {
		this.parent = parent;
	}

	@Override
	public List<SiSRequestTripple> getRequiredInformationTypes() {
		return new LinkedList<SiSRequestTripple>();
	}

	@Override
	public double calculateWeight(List<Object> args, List<Object> kwargs) {

		double threshold = 0d;

		if (shouldComputeThreshold(baseProbability)) {
			threshold = getLeachThreshold(baseProbability, parent.getRound());
		}
		return threshold;
	}

	/**
	 * Returns whether or not this node should participate in the Algorithm at
	 * all.
	 */
	protected boolean shouldComputeThreshold(double probability) {

		int round = parent.getRound();

		if (parent.isGateway()) {
			this.lastRoundAsCH = round - 1;
		}

		switch (useHistory) {
		case Last1OverP:
			if (round - lastRoundAsCH < 1D / probability) {
				return false;
			}
			break;
		case Last:
			if (round - lastRoundAsCH == 1) {
				return false;
			}
			break;
		default:
			break;
		}
		return true;
	}
	
	/**
	 * Computes the standard LEACH Threshold function with the given parameters
	 * 
	 * @param baseProbability
	 * @param round
	 * @return
	 */
	protected double getLeachThreshold(double baseProbability, int round) {

		double threshold = baseProbability / (1D - baseProbability * (round % (1D / baseProbability)));

		if (threshold > 1D) {
			// Original LEACH doesn't care about values larger one >_<
			threshold = 1D;
		}
		return threshold;
	}

	@Override
	public void setBaseProbability(Double p) {
		assert p > 0 : "Probability must be larger than zero!";
		assert p <= 1 : "Probability must be smaller or equall to one!";
		baseProbability = p;
	}

	public Double getBaseProbability() {
		return baseProbability;
	}

	/**
	 * Update 2016.09.09.: May return <b>-1</b> if this node was Gateway before
	 * DGS started!
	 * 
	 * @return
	 */
	public int getLastRoundAsCH() {
		return lastRoundAsCH;
	}

	public void setLastRoundAsCH(int lastRoundAsCH) {
		this.lastRoundAsCH = lastRoundAsCH;
	}

	public ClusterHeadExclusion getClusterHeadExclusion() {
		return useHistory;
	}

	public void setClusterHeadExclusion(ClusterHeadExclusion useHistory) {
		this.useHistory = useHistory;
	}

	@Override
	public DGSSubComponents getImplementedType() {
		return DGSSubComponents.LEACH;
	}

}
