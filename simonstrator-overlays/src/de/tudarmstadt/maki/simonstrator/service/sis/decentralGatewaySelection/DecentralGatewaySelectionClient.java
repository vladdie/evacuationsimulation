package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationConsumer.SiSConsumerHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSResultCallback;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.clusterer.DecentralClusterer;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.localdissemination.LocalDissemination;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.DGSConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages.MessageWithRound;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.scheme.DGSScheme;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.Trigger;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.trigger.Trigger.TriggerListener;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.util.SiSRequestTripple;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.weightComputation.WeightComputation;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.AbstractGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.DecentralizedGatewaySelectionServer;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionListener;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.messages.DGSServerUpdateMessage;

/**
 * Default implementation of the DecentralGatewaySelection. It can start the
 * trigger at the arrival of newer DGS messages (currently solely identified by
 * the port and round), relays messages to the scheme and clusterer. It also
 * starts the weighing process and scheme and tries to do a fallback cluster
 * head selection if the scheme does not yield any results. <br>
 * Starts in started Mode.
 * 
 * @author Christoph Storm
 * @version 2016.09.12
 */
public class DecentralGatewaySelectionClient extends AbstractGatewaySelectionClient
		implements IDecentralGatewaySelectionClient {

	private static boolean useFallback = true;

	private DecentralClusterer clusterer;
	private LocalDissemination disseminator;
	private DGSScheme scheme;
	private Trigger trigger;
	private WeightComputation weighter;

	private int round = 0;
	private boolean active = false;
	private boolean online = true;
	private boolean started = true;
	// TODO change online+started to enum?
	private boolean standby = false;

	NetInterface wifi;

	private UDP transport;
	private MessageBasedTransport cellularConnection;
	private TransitionEngine transEngine;

	private Object[] weighterInfo;
	private int numOfWeighterInfosMissing;

	private long maxFallbackContention = 10 * Time.SECOND;
	private long remoteStartDelay = 10 * Time.MILLISECOND;
	private boolean informServer = false;

	private List<SiSConsumerHandle> monitoringHandles = new LinkedList<SiSConsumerHandle>();

	@TransferState({ "Host" })
	public DecentralGatewaySelectionClient(Host host) {
		super(host);

		maxFallbackContention = DGSComponentFactory.getMaxFallbackContention();
		remoteStartDelay = DGSComponentFactory.getRemoteStartDelay();
		informServer = DGSComponentFactory.isInformServer();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void onTriggered() {

		Monitor.log(DecentralGatewaySelectionClient.class, Monitor.Level.INFO,
				"DGS on Node " + getHost().getId().value() + " got Triggered.");

		if (!isOnline() || active) {
			return;
		}

		active = true;

		List<SiSRequestTripple> requiredInfo = weighter.getRequiredInformationTypes();
		numOfWeighterInfosMissing = requiredInfo.size();
		weighterInfo = new Object[numOfWeighterInfosMissing];

		/*
		 * Make this whole thing asynchronous... Allows to call the whole thing of, if we get an early "I am CH"
		 * -Message
		 */
		if (numOfWeighterInfosMissing == 0) {
			Event.scheduleImmediately(new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					getWeightAndStartScheme((List) requiredInfo);
					// Java typecasting can get a bit silly, sometimes...
				}
			}, null, 47);
		} else {
			Event.scheduleImmediately(new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					startSiSRequests(requiredInfo);
				}
			}, null, 42);
			// Anything else? No? Then wait...
		}
	}

	private void startSiSRequests(List<SiSRequestTripple> requiredInfo) {
		if (active && isOnline()) {
			int counter = 0;
			for (SiSRequestTripple rInfo : requiredInfo) {
				// TODO: Alter if SiS and/or Monitoring ever get the ability to
				// mass haul data at once
				monitoringHandles.add(getSis().get().aggregatedObservation(rInfo.getAggr(), rInfo.getType(),
						rInfo.getRequest(), new sisCB(counter, round)));

				counter++;
			}
		}
	}

	private Callback generateCallback() {
		return new Callback() {
			@Override
			public void finished(boolean successful) {
				// don't care
			}
		};
	}

	@Override
	public Trigger getTrigger() {
		return trigger;
	}

	@Override
	public DGSScheme getScheme() {
		return scheme;
	}

	@Override
	public WeightComputation getWeightComputation() {
		return weighter;
	}

	@Override
	public LocalDissemination getLocalDissemination() {
		return disseminator;
	}

	@Override
	public DecentralClusterer getClusterer() {
		return clusterer;
	}

	/**
	 * Remnant of HostComponent days. This is called upon a
	 * <i>DGSSchemeFinished</i> and now updates the local gateway variable.
	 * 
	 * @return
	 */
	private INodeID getClusterID() {
		INodeID gateway = null;
		if (clusterer != null) {
			gateway = clusterer.getClusterID();
		}
		setGateway(gateway);
		return gateway;
	}

	@Override
	public void DGSSchemeFinished(NodeRole role) {

		if (role.equals(NodeRole.NONE) && useFallback) {
			// System.out.println(Time.getFormattedTime() + " Node" +
			// host.getHostId() + " is not part of a Cluster!");

			/*
			 * Ensure that we are part of a cluster, even when not initially becoming CH!
			 * 
			 * step 1: randomize contention, or use weight(?)
			 * 
			 * step 2: when timeout without CH message, declare self ch
			 */
			long contention = (long) (maxFallbackContention * Randoms.getRandom(this).nextDouble());

			Event.scheduleWithDelay(contention, new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					if (active && isOnline()) {
						scheme.onBecameClusterHead();
						setGateway(getHost().getId());
						active = false;
						round++;
						Monitor.log(DecentralGatewaySelectionClient.class, Monitor.Level.INFO,
								"Decentral Gateway Selection on Node " + getHost().getId().value()
										+ " declared itself CH after timeout");

						informListeners(NodeRole.CLUSTERHEAD, getGateway());
					}
				}
			}, null, 404);

		} else {
			Monitor.log(DecentralGatewaySelectionClient.class, Monitor.Level.INFO,
					"Decentral Gateway Selection finished on Node " + getHost().getId().value() + " as %s",
					role.toString());

			// 'Node 3, secondary Member of 12';
			// that sounds like a Borg designation...
			active = false;
			round++;
			informListeners(role, getClusterID());
		}
	}

	private void informListeners(NodeRole role, INodeID clusterID) {
		this.setNodeRole(role);
		// currentRole = role;
		this.setGateway(clusterID);
		// currentGateway = clusterID;
		for (GatewaySelectionListener listener : getGatewaySelectionListeners()) {
			listener.gatewaySelectionFinished(role, clusterID);
		}

		// Should we update the server?
		if (informServer && DecentralizedGatewaySelectionServer.getServerNetID() != null) {
			DGSServerUpdateMessage msg = new DGSServerUpdateMessage(getHost().getId(), clusterID);
			cellularConnection.send(msg, DecentralizedGatewaySelectionServer.getServerNetID(),
					DecentralizedGatewaySelectionServer.cellularDGSPort);
		}

	}

	@Override
	public boolean isActive() {
		return active;
	}

	@Override
	public UDP getTransport() {
		return transport;
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {

		if (isOnline()) {
			if (msg instanceof MessageWithRound) {
				// Maybe use an additional marker Interface?
				handleLocalDGSMessage((MessageWithRound) msg);
			} else if (msg instanceof DGSConfigurationMessage) {
				handleConfigurationMessage((DGSConfigurationMessage) msg);
			}
		}
	}

	private void handleConfigurationMessage(DGSConfigurationMessage msg) {
		// 1. Check if we are currently active
		// 2. Exchange Components
		// 3. Start if message says so, or if we were active before
		boolean start = isActive() || msg.isStartGatewaySelectionImmediately();
		for (DGSSubComponents newComp : msg.getDGSSubComponents()) {
			ComponentTransition(newComp.getType(), newComp.getImplementingClass());
		}
		if (start) {
			// start immediately?
			// Or wait, so others can receive and act on this message as well?
			Event.scheduleWithDelay(remoteStartDelay, new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					if (isOnline()) {
						getTrigger().onTriggeredFromOutside();
					}
				}
			}, null, 01);
		}
	}

	private void handleLocalDGSMessage(MessageWithRound msg) {
		int msgRound = msg.getRound();
		/*
		 * Case1: Msg from previous round -> Disregard
		 * 
		 * Case2: Msg from this round
		 * 
		 * 2a: DGS is active, so get the message to the subcomponents
		 * 
		 * 2b: DGS is inactive, meaning we finished the round. Disregard Msg
		 *
		 * Case3: Msg from higher round, start DGS
		 */
		boolean fwd = false;
		if (active && !(msgRound < round)) {
			// 2a: get msg to subcomponents
			fwd = true;
		}
		if (msgRound > round) {
			round = msgRound;
			if (!active) {

				trigger.onTriggeredFromOutside();
				fwd = true;
			}
		}
		if (fwd == true) {
			try {
				Message newMsg = msg.getClass().getConstructor(msg.getClass()).newInstance(msg);
				if (clusterer != null)
					clusterer.onMessageArrived(newMsg);
				// Hope this works...
			} catch (Exception e) {
				e.printStackTrace();
			}
			scheme.onMessageArrived(msg);
		}
	}

	@Override
	public <T extends DGSSubComponent> void ComponentTransition(DGSComponent comp, Class<T> implementingClass) {
		// Only exchange Components if the new one is NOT the same!
		if (!getSubComponent(comp).getImplementedType().getImplementingClass().equals(implementingClass)) {
			transEngine.executeAtomicTransition(comp.toString(), implementingClass);
			active = false;
		}
	}

	private DGSSubComponent getSubComponent(DGSComponent type) {
		switch (type) {
		case CLUSTERER:
			return clusterer;
		case DISSEMNINATION:
			return disseminator;
		case SCHEME:
			return scheme;
		case TRIGGER:
			return trigger;
		case WEIGHTCOMPUTATION:
			return weighter;
		default:
			// ???
			Monitor.log(getClass(), Level.ERROR, "%s is not a viable DGS component!", type);
			return null;
		}
	}

	@Override
	public <T extends DGSSubComponent> void setDefaultComponent(DGSComponent comp, T defaultInstance) {
		// can/WILL be called before initialize!
		switch (comp) {
		case CLUSTERER:
			clusterer = (DecentralClusterer) defaultInstance;
			break;
		case DISSEMNINATION:
			disseminator = (LocalDissemination) defaultInstance;
			break;
		case SCHEME:
			scheme = (DGSScheme) defaultInstance;
			break;
		case TRIGGER:
			trigger = (Trigger) defaultInstance;
			Event.scheduleImmediately(new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					trigger.addTriggerListener((TriggerListener) content);
				}
			}, this, 0);
			break;
		case WEIGHTCOMPUTATION:
			weighter = (WeightComputation) defaultInstance;
			break;
		default:
			// ???
			Monitor.log(getClass(), Level.ERROR, "%s is not a viable DGS component!", comp);
			break;
		}
	}

	private void gotResult(int index, Object result) {
		weighterInfo[index] = result;
		numOfWeighterInfosMissing--;

		if (numOfWeighterInfosMissing == 0) {
			// got all necessary info
			getWeightAndStartScheme(Arrays.asList(weighterInfo));
			revokeSiSHandles();
		}

	}

	private void revokeSiSHandles() {
		for (SiSConsumerHandle handle : monitoringHandles) {
			getSis().get().revoke(handle);
		}
		monitoringHandles.clear();
	}

	private void getWeightAndStartScheme(List<Object> weighterArgs) {

		if (!active || !isOnline()) {
			return;
		}

		// got all necessary info
		// System.out.println("Node" + host.getHostId() + ": Will now calulate
		// its weight.");

		// TODO implement something to use the weighters additional config
		// options
		double weight = weighter.calculateWeight(weighterArgs, new LinkedList<Object>());
		assert weight >= 0 : "Weight smaller zero is not permitted!";
		assert weight <= 1 : "weight greater than one is not permitted!";

		scheme.startScheme(weight);
		// And we are done for now...
	}

	@Override
	public int getRound() {
		return round;
	}

	public void setMaxFallbackContention(long fallbackContention) {
		maxFallbackContention = fallbackContention;
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {

		if (!isOnline()) {
			online = true;
			startComponents();
		}
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		// This WILL lead to problems if we decide to change implementations
		// while offline!
		// When that becomes the case, implement a new goActive/Inactive-method
		// in the subComponents or don't shutdown components...

		if (isOnline()) {
			stopComponents();

			revokeSiSHandles();
		}
		online = false;
		active = false;
	}

	private void startComponents() {
		if (isOnline() && !standby) {

			trigger.startMechanism(generateCallback());
			trigger.setParent(this);
			trigger.addTriggerListener(this);

			disseminator.startMechanism(generateCallback());
			disseminator.setParent(this);

			if (clusterer != null) {
				clusterer.startMechanism(generateCallback());
				clusterer.setParent(this);
			}

			scheme.startMechanism(generateCallback());
			scheme.setParent(this);

			weighter.startMechanism(generateCallback());
			weighter.setParent(this);

			standby = true;
		}
	}

	private void stopComponents() {
		if (standby) {
			if (clusterer != null) {
				clusterer.stopMechanism(generateCallback());
			}
			disseminator.stopMechanism(generateCallback());
			scheme.stopMechanism(generateCallback());
			weighter.stopMechanism(generateCallback());

			trigger.removeTriggerListener(this);
			trigger.stopMechanism(generateCallback());

			standby = false;
		}
	}

	private class sisCB implements SiSResultCallback<Object> {
		private int num;
		private int startRound;

		public sisCB(int num, int startRound) {
			this.num = num;
			this.startRound = startRound;
		}

		@Override
		public void onResult(Object result, SiSConsumerHandle consumerHandle) {
			if (this.startRound == getRound()) {
				gotResult(num, result);
			} else {
				Monitor.log(DecentralGatewaySelectionClient.class, Monitor.Level.WARN,
						"SiS aggregation took too long! DGS already finished selection in the meantime.");
			}
		}

		@Override
		public void onAbort(AbortReason reason) {

			Monitor.log(DecentralGatewaySelectionClient.class, Monitor.Level.WARN,
					"DGS did not receive required Data from SiS because of %s", reason);

			if (this.startRound == getRound()) {
				gotResult(num, null);
			}
		}
	}

	@Override
	public boolean isOnline() {
		return online && started;
	}

	@Override
	public void startMechanism(Callback cb) {

		// setup stuff
		try {
			// Stuff that could fail...
			if (this.getSis() == null) {
				this.setSis(getHost().getComponent(SiSComponent.class));
			}

			wifi = getHost().getNetworkComponent().getByName(NetInterfaceName.WIFI);

			transport = getHost().getTransportComponent().getProtocol(UDP.class, wifi.getLocalInetAddress(),
					IDecentralGatewaySelectionClient.DGSPort);

			transEngine = getHost().getComponent(TransitionEngine.class);

			cellularConnection = getHost().getTransportComponent().getProtocol(UDP.class,
					getHost().getNetworkComponent().getByName(NetInterfaceName.MOBILE).getLocalInetAddress(),
					DecentralizedGatewaySelectionServer.cellularDGSPort);

			// add listeners to stuff that could fail
			wifi.addConnectivityListener(this);
			online = wifi.isUp();

			transport.setTransportMessageListener(this);

			cellularConnection.setTransportMessageListener(this);

		} catch (ComponentNotAvailableException | ProtocolNotAvailableException e) {
			cb.finished(false);
		}

		sequentialStartup(0);

		if (!online) {
			// #stupid
			Event.scheduleWithDelay(1L, new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					stopComponents();
				}
			}, null, 24);
		}

		standby = true;
		started = true;

		cb.finished(true);
	}
	
	private void sequentialStartup(int i) {
		Event.scheduleImmediately(new EventHandler() {
			@Override
			public void eventOccurred(Object content, int i) {
				DecentralGatewaySelectionClient instance = (DecentralGatewaySelectionClient) content;
				DGSComponent type = DGSComponent.values()[i];
				DGSSubComponent component = null;
				if (transEngine.proxyExists(type.toString())) {
					// proxy exists, use that!
					component = transEngine.getProxy(type.toString(), type.getComponentInterface());
					component.startMechanism(generateCallback());
					component.setParent(instance);
				} else {
					// create the proxy, and reference it
					component = DGSComponentFactory.generateDefaultComponent(instance, type);
					component = createProxy(type.getComponentInterface(), component, type.toString());
					// createProxy starts the component in an immediate
					// Event!
				}
				setDefaultComponent(type, component);

				i++;
				if (i < DGSComponent.values().length) {
					sequentialStartup(i);
				}
			}
		}, this, i);
	}

	/**
	 * Helper function. Needed because JVM gets confused with specific wildcard
	 * calls out of an (unspecified wildcard) collection.
	 * 
	 * @param proxyInterface
	 * @param defaultInstance
	 * @return
	 */
	private <T extends DGSSubComponent> DGSSubComponent createProxy(Class<T> proxyInterface,
			DGSSubComponent defaultInstance, String proxyName) {
		return transEngine.createMechanismProxy(proxyInterface, proxyInterface.cast(defaultInstance),
				proxyName);
	}

	@Override
	public void stopMechanism(Callback cb) {
		// listeners.clear();
		active = false;

		if (isOnline()) {
			stopComponents();
		}
		revokeSiSHandles();

		trigger.removeTriggerListener(this);

		wifi.removeConnectivityListener(this);
		transport.removeTransportMessageListener();
		cellularConnection.removeTransportMessageListener();

		started = false;

		cb.finished(true);
	}

}
