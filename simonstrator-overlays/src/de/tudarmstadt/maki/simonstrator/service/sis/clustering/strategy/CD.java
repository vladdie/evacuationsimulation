package de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.ADeterministicGatewaySelection;

public abstract class CD extends ADeterministicGatewaySelection {

	public CD(String name, boolean needsNumberOfGateway) {
		super(name, needsNumberOfGateway);
	}

	@Override
	protected Map<INodeID, List<INodeID>> determineGateways(List<INodeID> outOf) {

		// Copy list
		LinkedList<INodeID> nodes = new LinkedList<>(outOf);
		Set<INodeID> unknownNodes = new HashSet<>(outOf);

		// Calculate clusters
		List<List<INodeID>> clusters = calculateCluster(nodes);

		Map<INodeID, List<INodeID>> tree = new LinkedHashMap<>();

		// Calculate weights and determine gateway for each cluster
		for (List<INodeID> cluster : clusters) {
			// Remove known nodes
			unknownNodes.removeAll(cluster);
			
			// Determine gateway
			if (cluster.size() > 1) {
				calculateWeights(cluster);
				Collections.sort(cluster, comperator); // Ordering: Lowest -> Highest
			}
			INodeID newGW = cluster.remove(cluster.size() - 1); // Select Highest
			tree.put(newGW, cluster);
		}
		
		// Add unknown nodes //XXX: Falls Anzahl nicht erreicht, sollten hier verbleibende Knoten per Zufall hinzugefügt werden.
		for(INodeID node : unknownNodes) {
			tree.put(node, new LinkedList<INodeID>());
		}

		return tree;
	}

	protected abstract List<List<INodeID>> calculateCluster(List<INodeID> nodes);

	@Override
	protected Map<SiSType<?>, SiSRequest> getRequiredInformationBySuperior() {
		return new LinkedHashMap<>();
	}


}
