package de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.messages;

public interface MessageWithWeight {

	public double getWeight();

}
