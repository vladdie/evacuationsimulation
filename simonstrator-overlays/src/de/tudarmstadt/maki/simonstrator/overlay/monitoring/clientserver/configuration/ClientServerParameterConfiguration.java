package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.configuration;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent.ClientServerNodeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.cloudlet.ClientServerCloudletComponent;

/**
 * Configuration Parameters for the {@link ClientServerNodeComponent} and
 * {@link ClientServerCentralComponent} are handled here.
 * 
 * @author Nils Richerzhagen
 *
 */
public class ClientServerParameterConfiguration implements ClientServerConfiguration {

	// Data Routing
	private long DATA_UPLOAD_INTERVAL;

	private String measurementProviderToUse;
	private int numCloudConnections;
	private long _GATEWAY_SELECTION_INTERVAL;

	// Cloudlet Parameters
	private final int CLOUDLET_INITIAL_TTL = 1;
	private long CLOUDLET_ADV_INTERVAL;

	@Override
	public void configure(ClientServerComponent comp, ClientServerNodeType type) {
		switch (type) {
		case MOBILE_NODE:
			((ClientServerNodeComponent) comp).setDataUploadInterval(DATA_UPLOAD_INTERVAL);
			((ClientServerNodeComponent) comp).setMeasurementProviderToUse(measurementProviderToUse);
			break;
		case CLOUD:
			((ClientServerCentralComponent) comp).setNumCloudConnections(numCloudConnections);
			((ClientServerCentralComponent) comp).setGatewaySelectionInterval(_GATEWAY_SELECTION_INTERVAL);
			break;
		case CLOUDLET:
			((ClientServerCloudletComponent) comp).setInitialTTL(CLOUDLET_INITIAL_TTL);
			((ClientServerCloudletComponent) comp).setCloudletAdvInterval(CLOUDLET_ADV_INTERVAL);
		default:
			break;
		}

	}

	public void setDataUploadInterval(long dataUploadInterval) {
		DATA_UPLOAD_INTERVAL = dataUploadInterval;
	}

	public void setMeasurementProviderToUse(String measurementProviderToUse) {
		this.measurementProviderToUse = measurementProviderToUse;
	}
	
	public void setNumCloudConnections(int numCloudConnections) {
		assert numCloudConnections > 0 : "Cloud Connections - numberOfGateways should be larger zero.";

		this.numCloudConnections = numCloudConnections;
	}

	public void setGatewaySelectionInterval(long gatewaySelectionInterval) {
		assert gatewaySelectionInterval >= 2 * Time.MINUTE;
		this._GATEWAY_SELECTION_INTERVAL = gatewaySelectionInterval;
	}

	public void setCloudletAdvInterval(long cloudletAdvInterval) {
		this.CLOUDLET_ADV_INTERVAL = cloudletAdvInterval;
	}
}
