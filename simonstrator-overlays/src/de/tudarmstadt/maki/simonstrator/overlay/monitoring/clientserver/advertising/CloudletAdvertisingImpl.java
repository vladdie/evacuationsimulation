package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.advertising;

import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent.ClientServerNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.api.ClientServerSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.exceptions.NoKnownSinksException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.exceptions.NoStrongerSinksFound;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.advertising.CloudletAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.ITimeoutMapListener;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutMap;
import de.tudarmstadt.maki.simonstrator.service.transition.local.AtomicTransitionExecution.ComponentState;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * CloudletAdvertising mechanism of ClientServer.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 05.06.2014
 * 
 */
public class CloudletAdvertisingImpl extends ClientServerSubComponent implements CloudletAdvertising {
	/*
	 * Changeable Parameter
	 */
	private static long SINK_ADV_PERIODIC_OPERATION_INTERVAL = 15000000;

	/*
	 * Fixed Parameter
	 */
	/**
	 * Used for seeing if there is a better sink available in a sub range.
	 */
	private static final int SINK_MGMT_MIN_NUMBER_OF_SUCCESSIVE_UPDATES = 2;
	/**
	 * Used to obtain the best sink table entry.
	 */
	private static final int BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES = 2;


	// private final ClientServerNodeComponent parentNode;

	// AtomicBoolean set with initial false value
	private final AtomicBoolean _initializeMethodCall = new AtomicBoolean(true);

	/*
	 * Entry LIFETIME should be operation interval + hesitation time (SET in
	 * constructor)
	 */
	public static long SINK_TABLE_MAINTENANCE_MAX_ENTRY_LIFETIME;

	/*
	 * Random value that x percent are forwarded (rest is discarded) !! by this
	 * node !!
	 */
	public static final double FORWARDING_PROCENTUAL_VALUE = 0.7;

	/*
	 * Parameters for go back to idle if nothing received for timeout time.
	 */

	public static final int INITIAL_GRADIENT = 1;


	private TimeoutMap<INodeID, SinkTableEntry> sinkTable;
	private LinkedHashMap<INodeID, SinkTableEntry> receivedSinkAdvertisingMsgs;
	private TimeoutSet<UniqueID> sendSinkAdvMessages_duplicateCheck;

	public CloudletAdvertisingImpl(ClientServerNodeComponent parentNode) {
		super(parentNode);
		// this.parentNode = parentNode;
		SINK_TABLE_MAINTENANCE_MAX_ENTRY_LIFETIME = SINK_ADV_PERIODIC_OPERATION_INTERVAL;

		sinkTable = new TimeoutMap<INodeID, SinkTableEntry>(SINK_TABLE_MAINTENANCE_MAX_ENTRY_LIFETIME);
		receivedSinkAdvertisingMsgs = new LinkedHashMap<INodeID, CloudletAdvertisingImpl.SinkTableEntry>();
		sendSinkAdvMessages_duplicateCheck = new TimeoutSet<UniqueID>(SINK_ADV_PERIODIC_OPERATION_INTERVAL * 3);
		Monitor.log(CloudletAdvertising.class, Monitor.Level.INFO, "SinkAdvertising Initialized");
	}

	public void initialize() {
	}

	/**
	 * Starting the automata.
	 * 
	 * Called when the node went online or changed from
	 * {@link ClientServerNodeRole} LEAF to SINK. Thus,
	 * {@link ClientServerNodeComponent} should not be LEAF --> must be SINK.
	 * 
	 * State here must be INACTIVE.
	 */
	private void startComponent() {
		// Insanity
		assert getParentComponent().isSink();
		assert getParentComponent().isPresent();
		assert getComponentState() == SubComponentState.INACTIVE;

		setComponentState(SubComponentState.IDLE);
	}

	/**
	 * Used to reset the automata.
	 * 
	 * Called when host went offline or when {@link ClientServerNodeComponent}
	 * became node. May be node before going offline, thus component is already
	 * in {@link ComponentState} INACTIVE.
	 */
	private void resetComponent() {
		if (!getParentComponent().isPresent() || !getParentComponent().isSink()) {
			if (getComponentState() == SubComponentState.INACTIVE) {
				return;
			}
			setComponentState(SubComponentState.INACTIVE);
		} else {
			assert !getParentComponent().isSink() : "Should only be called when the parent node is NO sink";
		}
	}

	@Override
	public void handleIncomingCloudletAdvMsg(CloudletAdvertisingMessage msg) {
		/*
		 * Duplicate aware check. If duplicate ignore completely and just do
		 * nothing -> return;
		 */
		if (sendSinkAdvMessages_duplicateCheck.contains(msg.getUniqueMsgID())) {
			return;
		}

		/*
		 * Update sink table. If initial sinkId is already known, just update
		 * with fresh values, and don't forget to increase number of updates
		 * accordingly.
		 */
		SinkTableEntry newEntry;
		// When already contained update number of updates field, gradient, and
		// timestamp!
		if (sinkTable.containsKey(msg.getSinkId())) {
			newEntry = new SinkTableEntry(sinkTable.get(msg.getSinkId()), msg.getGradient(), msg.getSinkQuality());
		}
		// new heard sink, thus just build this new entry.
		else {
			newEntry = new SinkTableEntry(msg.getSinkId(), msg.getGradient(), msg.getSinkQuality());
		}
		sinkTable.putNow(msg.getSinkId(), newEntry);
		receivedSinkAdvertisingMsgs.put(msg.getSinkId(), newEntry);

		/*
		 * ############ Being Sink! ############
		 * 
		 * Do not make any forwarding or other stuff when being a sink! CAUTION!
		 * This is behind sink table update as it needs no comparison with the
		 * state before receiving a sinkadv msg. Instead, needs an updated table
		 * to perform.
		 */
		if (getParentComponent().isSink()) {
			/*
			 * ###### Sink Merging / Push Away ########
			 * 
			 * TODO determine values for x successive beacons (robustness
			 * against fluctuation) and pre-defined adjacency field.
			 * 
			 * - x successive beacons
			 * 
			 * - adjacent sink, initial_gradient - gradient <= pre-defined value
			 * 
			 * - sink_entry.sink_quality_field > than this.sink_quality_field
			 * 
			 * do
			 * 
			 * - if(not IDLE) do this.node_state = IDLE;
			 */
			int adjacency_field = 1;
			// LinkedList<SinkTableEntry> foundSinkTableEntries;
			try {
				// foundSinkTableEntries =
				// getSinkTableEntriesWithBetterSinkQuality(adjacency_field);
				getSinkTableEntriesWithBetterSinkQuality(adjacency_field);
			} catch (NoKnownSinksException e) {
				return;
			} catch (NoStrongerSinksFound e) {
				return;
			}

			/*
			 * Heard sink with better quality. May be an idle sink, thus check
			 * for idle.
			 */
			if (!isIdle()) {
				setComponentState(SubComponentState.IDLE);
			}

			return;
		}

	}

	/**
	 * Sink-Merging / Push-Away
	 * 
	 * @param maxRange
	 *            the initial_gradient - gradient of the entries >= maxRange
	 * @return {@link List} of {@link SinkTableEntry}'s
	 * @throws NoKnownSinksException
	 * @throws NoStrongerSinksFound
	 */
	private LinkedList<SinkTableEntry> getSinkTableEntriesWithBetterSinkQuality(int maxRange)
			throws NoKnownSinksException, NoStrongerSinksFound {
		/*
		 * If this happens then callee should be aware of handling the
		 * exception!
		 */
		if (sinkTableIsEmpty()) {
			throw new NoKnownSinksException();
		}

		LinkedList<SinkTableEntry> foundSinkTableEntries = new LinkedList<SinkTableEntry>();

		if (sinkTable.size() >= 1) {
			for (SinkTableEntry actEntry : sinkTable.getUnmodifiableMap().values()) {
				if (actEntry.getNumberOfUpdates() >= SINK_MGMT_MIN_NUMBER_OF_SUCCESSIVE_UPDATES) {
					double sub_range = ((double) INITIAL_GRADIENT) - actEntry.getCurrentGradient();
					if (sub_range <= maxRange)
						foundSinkTableEntries.add(actEntry);
				}
			}
		}
		if (foundSinkTableEntries.size() == 0)
			throw new NoStrongerSinksFound();
		return foundSinkTableEntries;
	}

	@Override
	public void addSinkTableListener(ITimeoutMapListener l) {
		sinkTable.addListener(l);
	}

	@Override
	public boolean sinkTableIsEmpty() {
		return sinkTable.size() == 0;
	}

	/**
	 * Return the sinkId of the {@link SinkTableEntry} that has the largest
	 * gradient, while at the same time has a minimum of X successive updates?
	 * If there is not such an entry do with {@link SinkTableEntry} until (with
	 * less successive updates) one is found.
	 * 
	 * @return best possible sinkId or {@link NoKnownSinksException} in case of
	 *         empty sink table!
	 * @throws NoKnownSinksException
	 */
	@Override
	public AbstractMap.SimpleEntry<INodeID, Double> getBestSinkTableEntry() throws NoKnownSinksException {
		/*
		 * If this happens then callee should be aware of handling the
		 * exception!
		 */
		if (sinkTableIsEmpty()) {
			throw new NoKnownSinksException();
		}

		SinkTableEntry bestEntry = sinkTable.getUnmodifiableMap().entrySet().iterator().next().getValue();

		/*
		 * Start with max number of successive and take less if none found.
		 */
		for (int curNumberOfUpdates = BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES; curNumberOfUpdates > 0; curNumberOfUpdates--) {
			if (sinkTable.size() >= 1) {
				for (SinkTableEntry actEntry : sinkTable.getUnmodifiableMap().values()) {
					if (actEntry.getCurrentGradient() > bestEntry.getCurrentGradient()
							&& actEntry.getNumberOfUpdates() >= curNumberOfUpdates) {
						bestEntry = actEntry;
					}
				}
			}
			if (bestEntry.getNumberOfUpdates() >= curNumberOfUpdates)
				return new SimpleEntry<INodeID, Double>(bestEntry.getSinkId(), bestEntry.getCurrentGradient());
		}
		throw new NoKnownSinksException();
	}

	/**
	 * Entries of the sink table. Containing the sinkID, received gradient, and
	 * the timestamp when the last beacon of that sink was heard.
	 * 
	 * @author Nils Richerzhagen
	 * @version 1.0, 06.06.2014
	 */
	private class SinkTableEntry {
		private static final double GRADIENT_REDUCE_FACTOR_PER_SECOND = 1 / 5;
		/*
		 * The sinkID.
		 */
		private INodeID sinkID;

		/*
		 * Gradient value of the received sink beacon. Nodes forwarding the
		 * beacon, decrease the gradient when sending!
		 */
		private int gradient;

		/*
		 * The timestamp when the last sinkBeacon was heard.
		 */
		private long lastHeardUpdateTimestamp;

		/*
		 * Number of heardUpdatesFromThatSink
		 */
		private int numberOfUpdates;

		/*
		 * The quality describing attribute
		 */
		private int sinkQuality;

		/**
		 * Constructor for initial, unknown sink table entry generation.
		 * 
		 * @param sinkID
		 * @param gradient
		 */
		public SinkTableEntry(INodeID sinkID, int gradient, int sinkQuality) {
			this.sinkID = sinkID;
			this.gradient = gradient;
			this.numberOfUpdates = 1;
			this.lastHeardUpdateTimestamp = Time.getCurrentTime();
			this.sinkQuality = sinkQuality;
		}

		/**
		 * Constructor for received entry that is already present. Thus, put new
		 * entry with increased number of updates, updated gradient, and updated
		 * timestamp.
		 * 
		 * @param entry
		 */
		public SinkTableEntry(SinkTableEntry entry, int receivedMsgGradient, int sinkQuality) {
			this.sinkID = entry.getSinkId();
			this.gradient = receivedMsgGradient;
			this.numberOfUpdates = entry.getNumberOfUpdates() + 1;
			this.sinkQuality = sinkQuality;
			this.lastHeardUpdateTimestamp = Time.getCurrentTime();
		}

		/**
		 * Returning the unique sinkId
		 * 
		 * @return
		 */
		public INodeID getSinkId() {
			return sinkID;
		}

		/**
		 * Return numberOfUpdates.
		 * 
		 * @return
		 */
		public int getNumberOfUpdates() {
			return numberOfUpdates;
		}

		/**
		 * Returning gradient of that table entry. Gradient is computed with
		 * influence factors. Thus it is currently linear reduced over time.
		 * 
		 * @return
		 */
		public double getCurrentGradient() {
			// TODO How should gradient be reduced over time?
			double timeDiff = ((double) (Time.getCurrentTime() - lastHeardUpdateTimestamp)) / Time.SECOND;
			double timeDiffLinear = timeDiff * GRADIENT_REDUCE_FACTOR_PER_SECOND;
			double gradients = (double) gradient;
			double adaptedGradient = gradients - timeDiffLinear;
			if (adaptedGradient < 0)
				return 0;
			else {
				return adaptedGradient;
			}
		}
	}

	@Override
	protected void setComponentState(SubComponentState state) {
		// TODO right actions on active inactive?
		assert this.getComponentState() != state;

		if (!_initializeMethodCall.getAndSet(false)) {
			Monitor.log(CloudletAdvertising.class, Monitor.Level.DEBUG,
					"Node " + getParentComponent().getHost().getId().value() + " ComponentState from "
							+ getComponentState().toString() + " to " + state.toString());
		}

		super.setComponentState(state);
		// this.currentComponentState.setComponentState(state);

		// System.out.println(getParentComponent().getHost().getId().value() + "
		// SinkState " +
		// currentComponentState.toString());

		switch (state) {
		case ACTIVE:
			break;
		case INACTIVE:
			break;
		case IDLE:
			break;
		default:
			break;
		}
	}

	@Override
	public void clientServerNodeRoleChanged(ClientServerNodeRole clientServerNodeRole) {
		switch (clientServerNodeRole) {
		case LEAF:
			resetComponent();
			break;
		case SINK:
			startComponent();
			break;
		case OFFLINE:
			resetComponent();
			break;
		default:
			throw new AssertionError("Unknown case.");
		}
	}
}
