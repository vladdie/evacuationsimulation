package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer;

import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;

/**
 * Marker Analyzer
 * 
 * @author Nils Richerzhagen
 *
 */
public interface CraterGlobalKnowledgeAnalyzer extends Analyzer {

	// Marker
	

}
