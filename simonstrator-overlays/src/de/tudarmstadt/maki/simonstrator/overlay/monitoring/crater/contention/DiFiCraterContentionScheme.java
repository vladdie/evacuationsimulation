package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention;

/**
 * A implementation of {@link AbstractContentionScheme} based on devices distance
 * towards the closest sink
 * 
 * @author Jonas Huelsmann
 */
public class DiFiCraterContentionScheme extends AbstractContentionScheme {

	public DiFiCraterContentionScheme(long min, long max) {
		super(min, max);
	}

	public long getHesitationTime(double gradient) {
		// low gradients (distant nodes) will send first
		double hesFactor = gradient / 10;
		long hesitationTime = MIN_DATA_ROUTING_HESITATION_TIME
				+ (long) (hesFactor * (MAX_DATA_ROUTING_HESITATION_TIME - MIN_DATA_ROUTING_HESITATION_TIME));
		return hesitationTime;
	}

}
