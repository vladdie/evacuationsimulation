package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.cloudlet;

/**
 * Cloudlet Advertising interface.
 * 
 * @author Nils Richerzhagen
 *
 */
public interface CloudletAdvertising {

	public void initialize();

	/**
	 * Send the advertising beacon
	 */
	public void sendAdvBeacon();
}
