# DataRouting transition between Broadcast and Unicast

## Basic function or idea
Nodes switch between the broadcast `DataRoutingImpl` and `DataRoutingUnicastImpl`, depending on either their own speed or the cluster-speed as computed by the Sink (e.g., the average speed of the Sink and its affiliated Leafs).
Unicast Datarouting differs from the initial Broadcast in that each Leaf maintains an entry for the nextHop to the Sink. These are updated with information included in the SinkAdvertisement messages or replies to broadcasted `DataMessageContainerLocal`. if there are several possible next hops, the one with higher gradient is favored. Leafs try to forward their `DataMessageContainerLocal` as unicast to this nextHop. In case that they do not receive an `UnicastReply` from this nextHop within a given time span, Leafs broadcast the message again as they would have in default DataRouting.

## Implementation
Upon initialization each `CraterNodeComponent` gets a hold of the host's `LocationSensor` if it exists, and adds a `CraterLocationListener` that periodically computes the Node's average speed based on its location.
Depending on the configuration, this listener only stores the last value of the speed-estimate, or triggers the Nodes own DataRouting decision mechanism, which may toggle between the Routing implementations.
If set to do so, each Node adds its newest speed-estimate to sent `DataMessageContainerLocal`s.

At Sinks, the `AbstractDataRouting` parent-class to the Routing Versions holds a reference to a `ClusterSpeedComputation`, and updates this with other node's speed values as they arrive. This `ClusterSpeedComputation` computes the Sink-cluster's speed periodically, every _2 * DATA_SENDING_PERIODIC_OPERATION_INTERVAL_.
The clusterSpeed can be an average of the collected node-speeds, with the possibility to remove up to a given percentage of updates from both ends of the reported speed-range beforehand. Alternatively it can return the median value. The result is handed over to the `SinkAdvertisingImpl`.
If set to do so, the SinkAdvertisement uses this value to decide whether its Leafs should switch to a different datarouting and sets an according boolean in its `SinkAdvertisingMessage`s. Default setting is broadcast routing.

## Transitions between BroadCast and Unicast Routing
Currently either each Node itself, or the Sink on behalf of all its Leafs, trigger the transition. Both do this based on the same function.
If the input speed is smaller than a *slowSpeed - slowSpeedOffset*, the Nodes switch to Unicast Routing, if it is larger than *slowSpeed + slowSpeedOffset* they switch to Broadcast Routing.
Alternatively one may toggle between these two by calling `toggleDataRouting()` at the `CraterNodeComponent`. If BroadcastRouting is currently in use at a Node or not, can be found out by calling `isBroadcastRouting()`.

## Config options/ Using Unicast Routing
All of these are set in `system_parameters.xml` and affect the `CraterParameterConfiguration`.

* computeSpeedAt: *String = CRATERNODE, SINKADV, OFF*	Where the decision to toggle routing is made. Default is *OFF*
* locationUpdateInterval: *Time* 						Interval that the `CraterLocationListener` gets its location updates.

* slowSpeed: *double*									Center value of the hysteresis that decides about toggling the datarouting.
* slowSpeedOffset: *double*								DataRouting is toggled at slowSpeed -+ slowSpeedOffset to Unicast and Broadcast routing respectively.

* sendSpeedEstimates: *boolean* 						Whether SpeedEstimates are to be send with DataMessages. Defaults to true.
* speedComputationStrategy: *String = AVG, MEDIAN*		How the Cluster speed is calculated.
* removeOutliers: *double*								Up to this value *range = [0.0, 0.5)* of the highest and lowest node-speeds are removed before computation.

* replyTimeout: *Time* 									Sets the timeout for receiving unicast replies for unicast DataMessages.


# Sink Advertisement interval adaptation

## Basic Idea
Instead of Broadcasting the `SinkAdvertisingMessage` and `CloudletAdvertisementMessage` in fixed periods, set the time until the next advertisement dynamically -dependent on the ClusterSpeed.

## Implementation
The `SinkAdvertisingImpl` and `CloudletAdvertisingImpl` now expect a `sinkAdvIntervalModifier` in their constructor.
This sinkAdvIntervalModifier has a getNextInterval method that takes a speed as input and returns a duration for the next Interval.
Current implementation are `DefaultInterval`, `LinearDecreaseInterval`, and `StaticInterval`. `StaticInterval` is the default, and always returns the set Sink-Advertisement Interval, if not configured with a different value. `DefaultInterval` returns a value depending on how long it takes at the given speed to cross 45m, while `LinearDecreaseInterval` is configurable and allows to set a maxInterval which is returned at a speed of 0 m/s and minInterval which is returned at maxSpeed. Slower speeds return a value based on the linear slope between these points, while higher than maxSpeed values result in minInterval.
The speed these classes expect should be the cluster speed as detailed in the Unicast part.

## Config options / Utilizing the Advertisement adaptation
All of these are set in `system_parameters.xml` and affect the `CraterParameterConfiguration`. Several of these are shared with the DataRouting toggling ability.

* sinkAdvIntervalModifier: *String = DEFAULT, LINEAR_DECREASE*	Sets the `sinkAdvIntervalModifier` used to compute the next sinkAdvInterval.
**NOTE:** DEFAULT is **NOT** the default setting. The default Crater behaviour is achieved by using the `StaticInterval`, which is automatically selected if this String is not configured for either DEFAULT or LINEAR_DECREASE. (Stupid, I know. CarryOver from a quickFix)
* cloudletShouldAdaptInterval: *boolean* 							Whether Cloudlets should adapt their advInterval too
* cloudletIntervalDivisor: *double*								Value that the generated interval for Cloudlets is divided by. Driven by the default setting of 3 Cloudlet Advertisements per Sink Advertisement.

**Parameters unique to `LinearDecreaseInterval`**
* minInterval: *Time*		The minimum interval between consecutive advertisements.
* maxInterval: *Time*		The maximum interval between consecutive advertisements.
* maxSpeed: *double*		The speed at which the minimum Interval is in effect.

**Entries shared with DataRouting transition:**
* computeSpeedAt: *String = CRATERNODE, SINKADV, OFF*	Where the speed computations take place. Default is *OFF*. **Needs to be *SINKADV* for this this to take effect!**
* sendSpeedEstimates: *boolean* 						Whether SpeedEstimates are to be send with DataMessages. **Needs to be true!**

* locationUpdateInterval: *Time* 						Interval that the `CraterLocationListener` gets its location updates.
* speedComputationStrategy: *String = AVG, MEDIAN*		How the Cluster speed is calculated.
* removeOutliers: *double*								Up to this value *range = [0.0, 0.5)* of the highest and lowest node-speeds are removed before computation.