package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 20.06.2014
 *
 */
public interface MessageWithSinkId {

	/**
	 * 
	 * @return
	 */
	public INodeID getSinkId();
	
}
