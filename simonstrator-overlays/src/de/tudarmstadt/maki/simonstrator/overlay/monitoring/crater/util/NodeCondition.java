package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util;

import java.lang.reflect.Field;
import java.util.Arrays;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.SiSTypeNotFoundException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.UnknownComparatorType;

/**
 * A representation of a condition for a node
 * 
 * @author Marc Schiller
 * @version 1.0, May 30, 2016
 */
public abstract class NodeCondition {

	// Storage
	private String parameter;

	private String comparator;

	private Double doubleValue = Double.NEGATIVE_INFINITY;

	private String value;

	private Location location;
	/*
	 * private double longitude;
	 * 
	 * private double latitude;
	 */

	/**
	 * Constructor
	 * 
	 * @throws SiSTypeNotFoundException
	 *             if the wanted SiSType does not exist
	 * @throws UnknownComparatorType
	 *             if the comparator does not exist
	 */
	public NodeCondition(String parameter, String comparator, String value)
			throws SiSTypeNotFoundException, UnknownComparatorType {
		this.setParameter(parameter);
		this.setComparator(comparator);
		this.setValue(value);
	}

	/**
	 * Set the parameter that should be compared
	 * 
	 * @param parameter
	 *            The SiS class name that should be compared
	 * @throws SiSTypeNotFoundException
	 *             if the wanted SiSType does not exist
	 */
	private void setParameter(String parameter)
			throws SiSTypeNotFoundException {
		for (Field field : SiSTypes.class.getFields()) {
			if (field.getName().equals(parameter)) {
				this.parameter = parameter;
				break;
			}
		}
		if (this.parameter == null) {
			throw new SiSTypeNotFoundException();
		}
	}

	/**
	 * Set the operation of the comparison
	 * 
	 * @param comparator
	 *            The type of comparison (<,>,=,<=,>=,<>)
	 * @throws UnknownComparatorType
	 *             if the comparator does not exist
	 */
	private void setComparator(String comparator) throws UnknownComparatorType {
		if (Arrays.asList(Utils.VALID_COMPARATORS).contains(comparator)) {
			this.comparator = comparator;
		} else {
			throw new UnknownComparatorType();
		}
	}

	/**
	 * Set the value to be compared to, if type is a String
	 * 
	 * @param value
	 *            The string value
	 */
	private void setValue(String value) {
		try {
			this.doubleValue = Double.valueOf(value);
		} catch (NumberFormatException ex) {
			this.value = value;
		}
	}

	/**
	 * Getter for the comparator
	 * 
	 * @return the comparator
	 */
	public String getComparator() {
		return this.comparator;
	}

	/**
	 * Get the attribute of the comparison
	 * 
	 * @return the SiSType as String
	 */
	public String getParameter() {
		return this.parameter;
	}

	/**
	 * Get the Value if it is a String
	 * 
	 * @return the value that should be compared to, null if it is Double
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * Get the Value if it is a double
	 * 
	 * @return the value that should be compared to, Double.NEGATIVE_INFINITY if
	 *         it is String
	 */
	public Double getDoubleValue() {
		return this.doubleValue;
	}

	/**
	 * only used if it is a position
	 * 
	 * @param position
	 */
	public void setLocation(Location position) {
		this.location = position;
	}

	/**
	 * @return a new Location as a {@link PositionVector} based
	 *         {@link #longitude} and {@link #latitude}. FIXME maybe for further
	 *         work {@link PositionVector} is not the correct choice, then it
	 *         needs to be edited.
	 */
	public Location createLocation() {
		return location;
	}

	/**
	 * Pretty print this object
	 */
	public String toString() {
		String retString = "Parameter: " + this.parameter + " / Comparator: "
				+ this.comparator + " / Value: ";

		if (this.value != null) {
			retString += this.value + " (STRING)";
		} else {
			retString += this.doubleValue + " (Double)";
		}

		return retString;
	}
}