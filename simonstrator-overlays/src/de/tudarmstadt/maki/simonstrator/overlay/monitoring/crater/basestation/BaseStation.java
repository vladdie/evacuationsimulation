package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.basestation;

import de.tudarmstadt.maki.simonstrator.api.Host;


/**
 * A interface that simulates the base station behavior in a network. Meaning only a
 * limited number of nodes is going to be sink node. All nodes check if they are
 * sink by asking this class if connection is possible. <br>
 * 
 * - The {@link BaseStation} then allows a certain number X of connections at
 * maximum<br>
 * 
 * -- in a random manner but favoring nodes (sinks) that already are connected a
 * longer time than the other.<br>
 * 
 * -- To not have the same node to be affiliated to the BS (i.e. being sink and
 * able to upload their stuff) a maximum uninterruptedly connection time is used
 * after which exceedance the sink loses its connection.<br>
 * 
 * - Upload check interval auf allen Knoten (an LTE
 * bzw. UMTS check angleichen)
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 06.08.2014
 * 
 */
@Deprecated
public interface BaseStation {

	/**
	 * Returns if the given host is allowed (able) to connect to the BS, thus become a sink or not.
	 * @param host
	 * @return
	 */
	public boolean canConnectToBS(Host host);
	
	/**
	 * To be set once in the beginning of the simulation!
	 * @param maxAllowedConnections
	 */
	public void setMaxConnections(int maxAllowedConnections);

}
