package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * Entries of the sink table. Containing the sinkID, received gradient, and the
 * timestamp when the last beacon of that sink was heard.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 06.06.2014
 */
public class SinkTableEntry {
	private static final double GRADIENT_REDUCE_FACTOR_PER_SECOND = 1 / 5;
	/*
	 * The sinkID.
	 */
	private INodeID sinkID;

	/*
	 * Gradient value of the received sink beacon. Nodes forwarding the beacon,
	 * decrease the gradient when sending!
	 */
	private double gradient;

	/*
	 * The timestamp when the last sinkBeacon was heard.
	 */
	private long lastHeardUpdateTimestamp;

	/*
	 * Number of heardUpdatesFromThatSink
	 */
	private int numberOfUpdates;

	/*
	 * The quality describing attribute
	 */
	private int sinkQuality;

	/*
	 * Boolean for static sinks
	 */
	private boolean isStatic;

	/**
	 * Constructor for initial, unknown sink table entry generation.
	 * 
	 * @param sinkID
	 * @param gradient
	 */
	public SinkTableEntry(INodeID sinkID, int gradient, int sinkQuality,
			boolean isStatic) {
		this.sinkID = sinkID;
		this.gradient = gradient;
		this.numberOfUpdates = 1;
		this.lastHeardUpdateTimestamp = Time.getCurrentTime();
		this.sinkQuality = sinkQuality;
		this.isStatic = isStatic;
	}

	/**
	 * Constructor for received entry that is already present. Thus, put new
	 * entry with increased number of updates, updated gradient, and updated
	 * timestamp.
	 * 
	 * @param entry
	 */
	public SinkTableEntry(SinkTableEntry entry, double receivedMsgGradient,
			int sinkQuality) {
		this.sinkID = entry.getSinkId();
		this.gradient = receivedMsgGradient;
		this.numberOfUpdates = entry.getNumberOfUpdates() + 1;
		this.sinkQuality = sinkQuality;
		this.lastHeardUpdateTimestamp = Time.getCurrentTime();
		this.isStatic = entry.getSinkMobility();
	}

	/**
	 * Returning the unique sinkId
	 * 
	 * @return
	 */
	public INodeID getSinkId() {
		return sinkID;
	}

	/**
	 * Return numberOfUpdates.
	 * 
	 * @return
	 */
	public int getNumberOfUpdates() {
		return numberOfUpdates;
	}

	/**
	 * Returning the sinkQuality field.
	 * 
	 * @return
	 */
	public int getSinkQuality() {
		return sinkQuality;
	}

	/**
	 * Returning true if sink is mobile
	 *
	 * @ return true if sink is mobile
	 * 
	 * @author Jonas Huelsmann
	 */
	public boolean getSinkMobility() {
		return !isStatic;
	}

	/**
	 * Returning gradient of that table entry. Gradient is computed with
	 * influence factors. Thus it is currently linear reduced over time.
	 * 
	 * @return
	 */
	public double getCurrentGradient() {
		// TODO How should gradient be reduced over time?
		double timeDiff = ((double) (Time.getCurrentTime() - lastHeardUpdateTimestamp))
				/ Time.SECOND;
		double timeDiffLinear = timeDiff * GRADIENT_REDUCE_FACTOR_PER_SECOND;
		double gradients = (double) gradient;
		double adaptedGradient = gradients - timeDiffLinear;
		if (adaptedGradient < 0)
			return 0;
		else {
			return adaptedGradient;
		}
	}
}
