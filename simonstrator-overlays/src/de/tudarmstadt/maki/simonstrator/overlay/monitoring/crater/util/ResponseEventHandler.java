package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;

/**
 * Event handler that enables other CraterComponents to force the
 * {@link DataRouting} to send out DataBuffers earlier than the next scheduled
 * periodic operation triggers.
 */
public class ResponseEventHandler implements EventHandler {

	private long nextTimeToSend = Long.MAX_VALUE;
	private CraterNodeComponent parent;

	public ResponseEventHandler(CraterNodeComponent parent) {
		this.parent = parent;
	}

	@Override
	public void eventOccurred(Object content, int type) {
		if (Time.getCurrentTime() >= nextTimeToSend) {

			if (parent.getCraterNodeRole().equals(CraterNodeRole.LEAF)
					// This is only here to send responses. If there are none anymore
					// -because we forwarded another message and pigy-packed our data-,
					// do nothing
					&& !parent.getDataRouting().getLocalBuffer().getResponseObjects().isEmpty()) {
				parent.getDataRouting().sendBufferContent();
			} else if (parent.getCraterNodeRole().equals(CraterNodeRole.SINK)) {
				parent.getDataRouting().uploadBufferContent();
			}
			reset();
		}

	}

	/**
	 * Sets the delay until the Handler will trigger the {@link DataRouting}'s
	 * sending or upload methods.<br>
	 * If there is another time set before that delay runs out, data will only
	 * be sent at the earlier time.
	 * 
	 * @param delay
	 */
	public void setDelayToSend(long delay) {
		long time = Time.getCurrentTime() + delay;
		if (time < nextTimeToSend) {
			nextTimeToSend = time;
			Event.scheduleWithDelay(delay, this, null, 0);
		}
		// else do nothing, data that should be sent by this point will just be
		// sent earlier
	}

	/**
	 * Resets the internal mechanism. This should only be called by
	 * {@link DataRouting #craterNodeRoleChanged}
	 */
	public void reset() {
		nextTimeToSend = Long.MAX_VALUE;
	}

	/**
	 * Returns the next time this Handler will trigger datasending. Should only
	 * be used by {@link DataRouting #getTimeOfNextScheduledDataMessage()}
	 * 
	 * @return
	 */
	public long getNextTimeToSend() {
		return nextTimeToSend;
	}

}
