package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.SinkAdvertising;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.configuration.CloudConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * This class encapsulates the GatewayLogic, and moves it away from the
 * CraterCentralComponent. (Meaning that all of this was originally part of the
 * CentralComponent!) It is supposed to be as non-intrusive as possible. Also
 * meaning that all config calls are now redirected here by the central
 * component. <i>This class uses global knowledge.</i>
 * 
 * @version 2016.06.30
 */
public class CraterGatewaySelection {

	private long _GATEWAY_SELECTION_INTERVAL = -1;

	private boolean active = false;
	private CraterCentralComponent parent;
	private GatewayLogic gatewayLogic;
	private PeriodicOperation<CraterCentralComponent, Object> periodicGatewaySelectionCalculation;
	private long timeLastTriggered = -1L;

	public CraterGatewaySelection(CraterCentralComponent parent) {
		this.parent = parent;
	}

	public void startWithDelay(long delay) {
		active = true;
		inializePeriodicOperations(delay);
	}

	public void stop() {// TODO delete this!
	}

	/**
	 * Initialization of the periodic operations for the different internal
	 * parts of the {@link SinkAdvertising} mechanism.
	 */
	private void inializePeriodicOperations(long delay) {
		if (!active) {
			return;
		}
		periodicGatewaySelectionCalculation = new PeriodicOperation<CraterCentralComponent, Object>(parent,
				Operations.getEmptyCallback(), _GATEWAY_SELECTION_INTERVAL) {

			@Override
			public Object getResult() {
				return null;
			}

			@Override
			protected void executeOnce() {
				calculateGateways();
			}
		};
		periodicGatewaySelectionCalculation.scheduleWithDelay(delay);
	}

	/**
	 * Calculate Gateways with current {@link GatewayLogic} set in the
	 * configuration.
	 * 
	 * Send out the {@link CloudConfigurationMessage}s resulting from the
	 * respective {@link GatewayLogic} used.
	 */
	private void calculateGateways() {
		timeLastTriggered = Time.getCurrentTime();
		OverlayContacts overlayContacts = new OverlayContacts();
		for (Host host : Oracle.getAllHosts()) {
			try {
				CraterNodeComponent craterComp = (CraterNodeComponent) host.getComponent(CraterNodeComponent.class);
				OverlayContact overlayContact = craterComp.getLocalOverlayContact();

				// Node is offline, do not consider for that purpose.
				if (!craterComp.isPresent()) {
					continue;
				}
				if (overlayContact != null) {
					overlayContacts.add(overlayContact);
				}
			} catch (ComponentNotAvailableException e) {
				// central entity
			}
		}

		if (!overlayContacts.isEmpty()) {
			Collection<CloudConfigurationMessage> cloudConfigMsgs = gatewayLogic
					.calculateAndInformGateways(overlayContacts, parent.getMaxCloudConnections());
			for (CloudConfigurationMessage cloudConfigMsg : cloudConfigMsgs) {
				parent.sendViaCellular(cloudConfigMsg);
			}
		}
	}

	public void setGatewaySelectionInterval(long interval) {
		assert interval > 0L;
		_GATEWAY_SELECTION_INTERVAL = interval;
	}

	public boolean isActive() {
		return active;
	}

	public long getTimeLastTriggered() {
		return timeLastTriggered;
	}

	/**
	 * Configures the gateway logic for this cloud component.
	 * 
	 * @param gatewayLogic
	 */
	public void setGatewayLogic(GatewayLogic gatewayLogic) {
		assert gatewayLogic != null;
		this.gatewayLogic = gatewayLogic;
	}

}
