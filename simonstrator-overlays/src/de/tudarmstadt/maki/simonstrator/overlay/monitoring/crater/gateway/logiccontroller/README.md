# GatewayLogic Controller

These control the Gateway Selection Process utilized by Crater.

## CentralGatewayLogicController
Previous base Crater behaviour is now encapsulated in the `CentralGatewayLogicController`. It periodically requests a new Gateway selection from the set `GatewayLogic`. This is compatible with the use of a `GatewayTransitionController`. The CentralGatewayLogicController is part of the `CraterCentralComponent` and as such configured via the `CraterGatewayConfiguration`.
Options are limited to the GatewayLogic to use and the Interval.

## DecentralizedGatewayLogicController
This is an Interface between Crater and the `DecentralGatewaySelection` (DGS). It acts as a `GatewaySelectionListener` for the DGS and informs the CraterNode of the Role determined by the DGS. The DecentralizedGatewayLogicController is only present if Crater is `setUseDGS(true)` via the `CraterParameterConfiguration`. This controller is a currently a passive class. It does not act of itself. The only configuration option it has is `setStartWithDGS(boolean)`, which sets whether the DGS should be active or not upon Node initialization.

## Overlord
The Overlord controls which of the GatewayLogicControllers should be active, and so which Selection process -centralized or decentralized- to use. It also controls which DGS- Components should be utilized, and is part of the `CraterCentralComponent`
The Overlord consists of two Classes. Both of these need a a reference set to the other OverlordComponent, which is set by the CraterGatewayConfiguration.

### OverlordController
The `OverlordController` includes all the mechanics such as sending ControlMessages to the CraterNodes. Whether the DGS should be triggered to calculate a new ClusterHead immediately after the switch, can be set with `setStartDGSImmediately(boolean)`. A reference to the `CentralGatewayLogicController` should be set using `setCentralGatewayController` prior to initializing.
There is currently only the DefaultOverlord, which should be sufficient for most cases.

### OverlordLogic
The `OverlordLogic` includes the actual logic for for deciding when to switch and which DGS Components to use.
The Logic Component should be configured by the XML-parser, and set to the CraterGatewayConfiguration via `setOverlordLogic(logic)`.
Currently there is only CSV-reader, which expects the CSV-file as Constructor-parameter.
#### CSV File
The File expected by the `OverlordCSVreader` should be structured in at least two columns per line. The first column specifies the time of the change. The second Column states whether the *central* or *decentral* version should be used. All following Columns should state which `DGSSubComponents` should be used. It is sufficient to state components that changed since the last version.

