package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver;

import java.util.Collection;
import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.analyzer.AccuracyAndCompletenessAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.gateway.GatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerBackendMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.configuration.CloudConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataMessageContainerUploadImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataObjectImpl;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Central simple data collection entity that directly forwards to analyzers for statistics.
 * 
 * - calculates Gateways in current network situation
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 06.08.2014
 * 
 */
public class ClientServerCentralComponent extends ClientServerComponent {

	private GatewayLogic gatewayLogic;

	private PeriodicOperation<ClientServerCentralComponent, Object> periodicGatewaySelectionCalculation;

	private int numCloudConnections = -1;

	private long _GATEWAY_SELECTION_INTERVAL = -1;

	/**
	 * Ensure that simulator has initialized all components so that message is received.
	 */
	private static final long _GATEWAY_SELECTION_DELAY = 20 * Time.SECOND;

	private OverlayContacts overlayContacts;

	public ClientServerCentralComponent(Host host) {
		super(host, ClientServerNodeType.CLOUD);
		Monitor.log(ClientServerCentralComponent.class, Monitor.Level.INFO,
				"ClientServerCentralComponent: ID: " + host.getId());
	}

	@Override
	public void initialize() {
		super.initialize();
		assert numCloudConnections != -1;
		assert _GATEWAY_SELECTION_INTERVAL != -1;
		inializePeriodicOperations();
	}

	/**
	 * Set the maximum allowed number of connections to the {@link ClientServerCentralComponent} via the configuration.
	 * 
	 * @param maxConnectionsToServer
	 */
	public void setNumCloudConnections(int numCloudConnections) {
		this.numCloudConnections = numCloudConnections;
	}

	/**
	 * 
	 * @param gatewaySelectionInterval
	 */
	public void setGatewaySelectionInterval(long gatewaySelectionInterval) {
		this._GATEWAY_SELECTION_INTERVAL = gatewaySelectionInterval;
	}

	/**
	 * Initialization of the periodic operations for the different internal
	 * parts of the mechanism.
	 */
	private void inializePeriodicOperations() {
		periodicGatewaySelectionCalculation = new PeriodicOperation<ClientServerCentralComponent, Object>(this,
				Operations.getEmptyCallback(), _GATEWAY_SELECTION_INTERVAL) {

			@Override
			public Object getResult() {
				return null;
			}

			@Override
			protected void executeOnce() {
				calculateGateways();
			}
		};
		periodicGatewaySelectionCalculation.scheduleWithDelay(_GATEWAY_SELECTION_DELAY);

	}

	/**
	 * Configures the gateway logic for this cloud component.
	 * 
	 * @param gatewayLogic
	 */
	public void setGatewayLogic(GatewayLogic gatewayLogic) {
		assert gatewayLogic != null;
		this.gatewayLogic = gatewayLogic;
	}

	/**
	 * Calculate Gateways with current {@link GatewayLogic} set in the configuration.
	 * 
	 * Send out the {@link CloudConfigurationMessage}s resulting from the respective {@link GatewayLogic} used.
	 */
	public void calculateGateways() {
		overlayContacts = new OverlayContacts();
		for (Host host : Oracle.getAllHosts()) {
			try {
				ClientServerNodeComponent clientServerComp = (ClientServerNodeComponent) host.getComponent(ClientServerNodeComponent.class);
				OverlayContact overlayContact = clientServerComp.getLocalOverlayContact();

				// Node is offline, do not consider for that purpose.
				if (!clientServerComp.isPresent()) {
					continue;
				}
				if (overlayContact != null) {
					overlayContacts.add(overlayContact);
				}
			} catch (ComponentNotAvailableException e) {
				// central entity
			}
		}

		if (!overlayContacts.isEmpty()) {
			Collection<CloudConfigurationMessage> cloudConfigMsgs = gatewayLogic.calculateAndInformGateways(overlayContacts,
					numCloudConnections);
			for (CloudConfigurationMessage cloudConfigMsg : cloudConfigMsgs) {
				sendViaCellular(cloudConfigMsg);
			}
		}
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		assert msg instanceof ClientServerCellMessage || msg instanceof ClientServerBackendMessage;

		// System.out.println("Message arrived on central entity");
		if (msg instanceof DataMessageContainerUploadImpl) {
			DataMessageContainerUploadImpl dataContainer = (DataMessageContainerUploadImpl) msg;
			/*
			 * - #A.1# upload msg internals to analyzer for duplicate detection of real data.
			 */
			if (hasDataRoutingAnalyzer()) {
				getDataRoutingAnalyzer().uploadMsgReceived(dataContainer.getUniqueMsgID());
				// // #A.1#
				LinkedList<UniqueID> receivedNormalDataObjectIDs = new LinkedList<UniqueID>();
				for (DataObjectImpl<Double> actDataObject : dataContainer.getNormalDataList()) {
					receivedNormalDataObjectIDs.add(actDataObject.getUniqueDataObjectID());
				}

				getDataRoutingAnalyzer().onMessageContentReceivedAtServer(receivedNormalDataObjectIDs,
						dataContainer.getAggregationDupInsensitiveDataList(), dataContainer.getAggregationDupSensitiveDataList());
			}
			if (hasAccuracyAndCompletenessAnalyzer()) {
				getAccuracyAndCompletenessAnalyzer()
						.onDuplicateSensitiveAggregationDataObjectReceived(dataContainer.getAggregationDupSensitiveDataList());
			}
		}

		// Analyzer
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onReceivedOverlayMessage((OverlayMessage) msg, getHost());
		}
	}

	/*
	 * Analyzing convenience methods.
	 */
	private boolean _checkedForDataRoutingAnalyzer = false;

	private DataRoutingAnalyzer _dataRoutingAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasDataRoutingAnalyzer() {
		if (_dataRoutingAnalyzer != null) {
			return true;
		} else if (_checkedForDataRoutingAnalyzer) {
			return false;
		}
		getDataRoutingAnalyzer();
		return _dataRoutingAnalyzer != null;
	}

	public DataRoutingAnalyzer getDataRoutingAnalyzer() {
		if (!_checkedForDataRoutingAnalyzer) {
			try {
				_dataRoutingAnalyzer = Monitor.get(DataRoutingAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				_dataRoutingAnalyzer = null;
			}
			_checkedForDataRoutingAnalyzer = true;
		}
		return _dataRoutingAnalyzer;
	}

	/*
	 * Analyzing convenience methods.
	 */
	private boolean _checkedForAccuracyAndCompletenessAnalyzer = false;

	private AccuracyAndCompletenessAnalyzer _accuracyAndCompletenessAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasAccuracyAndCompletenessAnalyzer() {
		if (_accuracyAndCompletenessAnalyzer != null) {
			return true;
		} else if (_checkedForAccuracyAndCompletenessAnalyzer) {
			return false;
		}
		getAccuracyAndCompletenessAnalyzer();
		return _accuracyAndCompletenessAnalyzer != null;
	}

	public AccuracyAndCompletenessAnalyzer getAccuracyAndCompletenessAnalyzer() {
		if (!_checkedForAccuracyAndCompletenessAnalyzer) {
			try {
				_accuracyAndCompletenessAnalyzer = Monitor.get(AccuracyAndCompletenessAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				_accuracyAndCompletenessAnalyzer = null;
			}
			_checkedForAccuracyAndCompletenessAnalyzer = true;
		}
		return _accuracyAndCompletenessAnalyzer;
	}
}
