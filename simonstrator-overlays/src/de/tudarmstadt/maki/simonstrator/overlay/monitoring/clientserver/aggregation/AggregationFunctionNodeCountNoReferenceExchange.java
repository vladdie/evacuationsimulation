package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation;

/**
 * Nodecount aggregation function. <br>
 * 
 * <b>CAUTION: This function does not care about how often a node participated
 * in the value, thus when a node adds its value more than once it is also
 * counted for the nodecount attribute.<br>
 * There is not the exchange of the currently used reference value as it is normally and implemented in {@link AggregationFunctionNodeCount}.</b>
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 26.08.2014
 * 
 */
public class AggregationFunctionNodeCountNoReferenceExchange extends AbstractDuplicateSensitiveAggregationFunction {

	public AggregationFunctionNodeCountNoReferenceExchange(AggregationFunctionEntry toAddEntry) {
		super(toAddEntry);
	}

	public AggregationFunctionNodeCountNoReferenceExchange(AbstractDuplicateSensitiveAggregationFunction toCloneAggregationFunction) {
		super(toCloneAggregationFunction);
	}

	@Override
	public Double getValue() {
		return (double) this.getIncludedEntriesIDs().size();
	}

	@Override
	public String getDescription() {
		return DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.toString();
	}

	@Override
	public String getId() {
		return DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId();
	}

	@Override
	public AbstractDuplicateSensitiveAggregationFunction clone() {
		return new AggregationFunctionNodeCountNoReferenceExchange(this);
	}

}
