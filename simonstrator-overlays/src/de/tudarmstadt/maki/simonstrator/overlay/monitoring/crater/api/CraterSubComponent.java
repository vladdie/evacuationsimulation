package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.NoSinkAdvertising;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.SinkAdvertising;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.PerNodeRoleTimesAnalyzer;

/**
 * Class to maintain the {@link SubComponentState} of a {@link CraterSubComponent} like e.g. the {@link SinkAdvertising}
 * and the {@link NoSinkAdvertising}
 * 
 * Only for {@link CraterNodeComponent}.
 * 
 * @author Nils Richerzhagen
 *
 */
public abstract class CraterSubComponent {

	private CraterNodeComponent parentComponent;

	private SubComponentState componentState;

	public CraterSubComponent(CraterNodeComponent parentComponent) {
		this.parentComponent = parentComponent;
	}

	public CraterNodeComponent getParentComponent() {
		return parentComponent;
	}

	protected void setComponentState(SubComponentState componentState) {
		this.componentState = componentState;

		if (hasPerNodeStateTimeAnalyzer()) {
			getPerNodeStateTimeAnalyzer().onSubComponentStateChange(this, componentState);
		}
	}

	public SubComponentState getComponentState() {
		return componentState;
	}

	public boolean isInactive() {
		return getComponentState() == SubComponentState.INACTIVE;
	}

	public boolean isActive() {
		return this.getComponentState() == SubComponentState.ACTIVE;
	}

	public boolean isIdle() {
		return this.getComponentState() == SubComponentState.IDLE;
	}

	public enum SubComponentState {
		/**
		 * Inactive
		 */
		INACTIVE,

		/**
		 * Active
		 */
		ACTIVE,

		/**
		 * Idle
		 */
		IDLE
	}

	/*
	 * Analyzing convenience methods.
	 */

	private boolean checkedForPerNodeStateTimeAnalyzer = false;

	private PerNodeRoleTimesAnalyzer perNodeStateTimeAnalyzer = null;

	/**
	 * True, if a {@link PerNodeRoleTimesAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasPerNodeStateTimeAnalyzer() {
		if (perNodeStateTimeAnalyzer == null && !checkedForPerNodeStateTimeAnalyzer) {
			getPerNodeStateTimeAnalyzer();
		}
		return perNodeStateTimeAnalyzer != null;
	}

	public PerNodeRoleTimesAnalyzer getPerNodeStateTimeAnalyzer() {
		if (!checkedForPerNodeStateTimeAnalyzer) {
			try {
				perNodeStateTimeAnalyzer = Monitor.get(PerNodeRoleTimesAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				perNodeStateTimeAnalyzer = null;
			}
			checkedForPerNodeStateTimeAnalyzer = true;
		}
		return perNodeStateTimeAnalyzer;
	}

}
