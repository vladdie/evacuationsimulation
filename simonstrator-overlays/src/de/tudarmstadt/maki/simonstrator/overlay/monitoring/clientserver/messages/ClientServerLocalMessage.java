package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.api.ClientServerMessage;

/**
 * Marker interface for ClientServer Messages that are used within the network
 * (Sink Adv, Node Beaconing, Monitoring data msgs)
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 05.06.2014
 *
 */
public interface ClientServerLocalMessage extends ClientServerMessage {
	/*
	 * Marker interface!
	 */
}
