package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.api.ClientServerMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataMessageContainerUploadImpl;

/**
 * Marker interface for ClientServer Messages that are used for upload from
 * sinks to server like {@link DataMessageContainerUploadImpl}
 * 
 * Marker interface for all messages that are exchanged between cloud and local
 * node and not directly between local nodes to allow assertion checks
 *
 * @author Nils Richerzhagen
 * @version 1.0, 18.08.2014
 *
 */
public interface ClientServerCellMessage extends ClientServerMessage {
	/*
	 * Marker interface
	 */
}
