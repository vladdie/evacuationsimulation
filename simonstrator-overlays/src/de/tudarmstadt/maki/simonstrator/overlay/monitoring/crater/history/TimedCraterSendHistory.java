package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * Implementation of {@link SendHistory}, which keeps track of the
 * messages send within the last n time units. n is set in the constructor
 * 
 * @author Jonas Huelsmann
 * 
 */
public class TimedCraterSendHistory extends SendHistory {

	private TimeoutSet<UniqueID> HISTORY;


	public TimedCraterSendHistory(long ttl) {
		super(ttl);
		HISTORY = new TimeoutSet<UniqueID>(ttl);
	}


	/**
	 * Method that checks if a msgID was in the TreeSet and adds it if it was
	 * not.
	 * 
	 * @param msgID
	 *            The ID of a message
	 * 
	 * @return true if msgID was in the TreeSet, false else
	 */
	private boolean isAnEntry(UniqueID msgID) {

		if (HISTORY.contains(msgID)) {
			HISTORY.remove(msgID);
			HISTORY.addNow(msgID);
			return true;
		} else {
			HISTORY.addNow(msgID);
			return false;
		}

	}

	@Override
	public boolean sendable(UniqueID msgID) {
		if (isAnEntry(msgID)) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public Set<UniqueID> sendable(Collection<UniqueID> msgIDs) {
		Set<UniqueID> temp = new TreeSet<UniqueID>();
		UniqueID msgID;
		for (Iterator<UniqueID> iter = msgIDs.iterator(); iter.hasNext();) {
			msgID = iter.next();
			if (sendable(msgID))
				temp.add(msgID);
		}
		return temp;

	}

	@Override
	public boolean isInHistory(UniqueID msgID) {
		return HISTORY.contains(msgID);
	}

	@Override
	public void resetHistory() throws Exception {
		throw new Exception(
				"resetHistory is not ment to be called at a TimedCraterSendHistory");

	}

}
