package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;

/**
 * A message that has a unique ID, for duplicate detection!
 * Do not mix the UniqueID of a message with the sinkID, sinkID is for easy the nodeID.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 06.06.2014
 *
 */
public interface MessageWithUniqueID extends Message {

	/**
	 * The unique message ID.
	 * @return
	 */
	public UniqueID getUniqueMsgID();
}
