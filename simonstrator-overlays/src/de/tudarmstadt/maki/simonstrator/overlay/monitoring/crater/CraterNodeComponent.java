package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater;

import java.util.AbstractMap.SimpleEntry;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.NodeInformation;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.battery.BatterySensor;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.latency.LatencyRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.latency.LatencySensor;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.latency.LatencySensor.LatencyListener;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.NoSinkAdvertisingImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.SinkAdvertising;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.SinkAdvertisingImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.sinksendinginterval.SinkAdvIntervalModifier;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.ContentionScheme.ContentionSchemeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.NoKnownSinksException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history.SendHistory.HistoryType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.CloudletAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.NoSinkMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.SinkAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.configuration.CloudConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.UnicastReply;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.CellularResponseMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestInquiryMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestMessageCellular;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestMessageLocal;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataRouting;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataRoutingImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataRoutingUnicastImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.RequestRouting;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.RequestRoutingImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference.SinkPreference.SinkPreferenceType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterLocationListener;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterNodeRoleListener;
import de.tudarmstadt.maki.simonstrator.service.latency.ProbeHandlerImpl;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionListener;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller.GatewaySelectionClientController;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller.GatewaySelectionClientControllerImpl;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutMap;

/**
 * Mobile entity of Crater.
 *
 * @author Nils Richerzhagen
 * @version 1.0, 06.08.2014 -> rev.: 2016.07.22
 * 
 */
public class CraterNodeComponent extends CraterComponent
		implements NodeInformation, IPeerStatusListener, LatencyListener, RequestInvokable, GatewaySelectionListener {

	// TODO move to config or so, rename
	// Basically to toggle where certain decisions (like switching between
	// routing) take place
	public enum ResponisbleComponent {
		CRATERNODE, SINKADV, OFF
	};

	private SinkAdvertisingImpl sinkAdvertising;
	private NoSinkAdvertisingImpl noSinkAdvertising;
	private DataRouting dataRouting;

	private RequestRouting requestRouting;

	// interface to loadgenerator, and SiS
	private CraterRequestResolver requestResolver;

	private static final String routingProxy = "CraterDataRoutingProxy";

	private CraterLocationListener speedListener;

	private CraterNodeRole nodeRole = CraterNodeRole.OFFLINE;

	private List<CraterNodeRoleListener> nodeRoleListeners = new LinkedList<CraterNodeRoleListener>();

	// FIXME Not Needed Anymore, once Client/Server Monitoring is there
	@Deprecated
	private boolean withCraterFunctionality = true;

	@Deprecated
	private String measurementProviderToUse;

	/*
	 * Config Parameters
	 */
	// Sink Advertising Parameters
	private int INITIAL_TTL;
	private long SINK_ADV_PERIODIC_OPERATION_INTERVAL;
	// No Sink Advertising Parameters
	private long NO_SINK_ADV_INT;
	private SinkPreferenceType sinkPref;
	// Data Routing
	private long DATA_SENDING_PERIODIC_OPERATION_INTERVAL;
	private long DATA_UPLOAD_INTERVAL;
	private HistoryType sendHistoryType;
	private long sendHistoryValue;
	private ContentionSchemeType contentionType;
	private SinkAdvIntervalModifier intervalDeterminator;

	private TimeoutMap<UniqueID, DataMessageContainerLocalImpl> dataMessages = new TimeoutMap<UniqueID, DataMessageContainerLocalImpl>(
			3000000);

	private TimeoutMap<UniqueID, SinkAdvertisingMessage> sinkAdvertisingMessages = new TimeoutMap<UniqueID, SinkAdvertisingMessage>(
			3000000);

	private TimeoutMap<UniqueID, NoSinkMessage> noSinkMessages = new TimeoutMap<UniqueID, NoSinkMessage>(3000000);

	// TODO
	private LatencySensor latencyMeasurementService;
	private LatencyRequest latencyRequest;
	private BatterySensor batSensor;

	private static ResponisbleComponent computeSpeedAt = ResponisbleComponent.OFF;
	private static long locationUpdateInterval = Time.MINUTE;

	// for unicastRouting...
	private boolean usesBroadcastRouting = true;

	private double slowSpeed = 0.8; // m/s
	private double slowSpeedOffset = 0.2; // m/s

	// for distributed Gateway Selection
	private boolean useGatewaySelectionClient = false;
	private GatewaySelectionClientController gscController;

	// for Request Response
	private int requestTTL = 3;
	private double responseHesitationFactor = 0.1;

	public CraterNodeComponent(Host host) {
		super(host, CraterNodeType.MOBILE_NODE);

		try {
			batSensor = host.getComponent(BatterySensor.class);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("There must be an EnergyComponent configured for mobile nodes.");
		}

		Monitor.log(CraterNodeComponent.class, Monitor.Level.INFO, "CraterNodeComponent: ID: " + host.getId());
	}

	public String getMeasurementProviderToUse() {
		return measurementProviderToUse;
	}

	@Override
	public void initialize() {
		super.initialize();

		this.addPeerStatusListener(this);

		dataRouting = new DataRoutingImpl(this, DATA_SENDING_PERIODIC_OPERATION_INTERVAL, DATA_UPLOAD_INTERVAL,
				sendHistoryType, sendHistoryValue, contentionType);

		// dataRouting is now transitionable (is that even a word?)
		try {
			TransitionEngine transitionEngine = this.getHost().getComponent(TransitionEngine.class);
			dataRouting = transitionEngine.createMechanismProxy(DataRouting.class, dataRouting, routingProxy);

			transitionEngine.addTransitionListener(routingProxy, new TransitionListener() {
						@Override
						public void executedTransition() {
							usesBroadcastRouting = !usesBroadcastRouting;
						}

						@Override
						public void startingTransition() {
					// Probably nothing to do here!
						}
					});

		} catch (ComponentNotAvailableException e) {
			// Shamelessly copying BR's warn message...
			// Maybe set WarnLevel to Debug?! Run without
			// transEngine may be intentional
			Monitor.log(CraterNodeComponent.class, Level.WARN,
					"DataRouting was initialized without a TransitionEngine. Using default component instead of proxy.");
		}

		if (withCraterFunctionality) {
			sinkAdvertising = new SinkAdvertisingImpl(this, INITIAL_TTL,
					SINK_ADV_PERIODIC_OPERATION_INTERVAL, sinkPref, intervalDeterminator);
			noSinkAdvertising = new NoSinkAdvertisingImpl(this, NO_SINK_ADV_INT);


			/*
			 * Adding of sink table listeners
			 */
			sinkAdvertising.addSinkTableListener(noSinkAdvertising);
			sinkAdvertising.addSinkTableListener(dataRouting);

			/*
			 * Adding of node role listeners
			 */
			this.addCraterNodeRoleListener(noSinkAdvertising);
			this.addCraterNodeRoleListener(sinkAdvertising);
			this.addCraterNodeRoleListener(dataRouting);
		} else {
			sinkAdvertising = new SinkAdvertisingImpl(this, INITIAL_TTL,
					SINK_ADV_PERIODIC_OPERATION_INTERVAL, sinkPref, intervalDeterminator);

			/*
			 * Adding of sink table listeners
			 */
			sinkAdvertising.addSinkTableListener(dataRouting);

			/*
			 * Adding of node state listeners
			 */
			this.addCraterNodeRoleListener(sinkAdvertising);
			this.addCraterNodeRoleListener(dataRouting);
		}

		sinkAdvertising.initialize();
		if (withCraterFunctionality) {
			noSinkAdvertising.initialize();
		}
		// dataRouting.initialize();

		try {
			// Try to hold of the host's location sensor and set out own
			// listener.
			LocationSensor locSensor = getHost().getComponent(LocationSensor.class);
			LocationRequest reqConf = locSensor.getLocationRequest();

			reqConf.setInterval(locationUpdateInterval);
			reqConf.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

			/*
			 * Listener that transforms location updates into node speed
			 * estimates if computeSpeedAt sais Craternode, the listener calls
			 * updateSpeedEstimate() on here.
			 */
			speedListener = new CraterLocationListener(this, computeSpeedAt.equals(ResponisbleComponent.CRATERNODE));
			locSensor.requestLocationUpdates(reqConf, speedListener);
		} catch (ComponentNotAvailableException e1) {
			// well, then there's nothing to do here
		}

		// DGS handling
		if (useGatewaySelectionClient) {

			gscController = new GatewaySelectionClientControllerImpl(getHost());
			gscController.addGatewaySelectionListener(this);
		}

		// RequestRouting
		requestRouting = new RequestRoutingImpl(this, requestTTL);
		requestRouting.init();
		addCraterNodeRoleListener(requestRouting);

		// RequestResolver
		requestResolver = new CraterRequestResolver(this, responseHesitationFactor);
		requestResolver.init();
		addCraterNodeRoleListener(requestResolver);

		// Start as offline host? True?
		assert this.isPresent() == false;

		Monitor.log(CraterNodeComponent.class, Monitor.Level.INFO,
				"CraterNode: Initalization finished on Node " + getHost().getId().value());

		// FIXME
		try {
			latencyMeasurementService = getHost().getComponent(LatencySensor.class);
			latencyRequest = latencyMeasurementService.getLatencyRequest(20 * Time.SECOND);
			latencyMeasurementService.requestLatencyUpdates(new ProbeHandlerImpl(getCloudContact()), this, latencyRequest);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("LatencySensor not available.");
		}

	}

	/**
	 * Toggles DataRouting between broadcast and unicast
	 */
	public void toggleDataRouting() {
		if (!this.isPresent()) {
			return;
		}
		try {
			TransitionEngine transitionEngine = this.getHost().getComponent(
					TransitionEngine.class);

			if (usesBroadcastRouting) {
				transitionEngine.executeAtomicTransition(routingProxy,
						DataRoutingUnicastImpl.class);

				// System.out.println("Host:" + getHost().getHostId() +
				// "switched to Unicast");
			} else {
				transitionEngine.executeAtomicTransition(routingProxy,
						DataRoutingImpl.class);

				// System.out.println("Host:" + getHost().getHostId() +
				// "switched to Broadcast");
			}

			// usesBroadcastRouting = !usesBroadcastRouting;

		} catch (ComponentNotAvailableException e) {
			e.printStackTrace();
		}
	}

	// TODO more generic subcomponent getter and list of subcomponent here
	public SinkAdvertisingImpl getSinkAdvertising() {
		return sinkAdvertising;
	}

	// TODO More generic SubComponent getter and list of subcomponent here!
	public NoSinkAdvertisingImpl getNoSinkAdvertising() {
		return noSinkAdvertising;
	}

	/**
	 * Returns the current {@link CraterNodeRole} of the component.
	 * 
	 * @return
	 */
	public final CraterNodeRole getCraterNodeRole() {
		return this.nodeRole;
	}

	/**
	 * 
	 * Returns true if the node is a sink. Convenience Method.
	 * 
	 * @return
	 */
	public final boolean isSink() {
		return this.nodeRole.equals(CraterNodeRole.SINK);
	}

	/**
	 * Consistent handling of the {@link CraterNodeRole} functionality switches within this method. Further reactions on
	 * functionality switches from/to sink to node and vice versa can be implemented in onBecameSink and onBecameNode.
	 * 
	 * On changes of {@link CraterNodeRole} always switch the {@link CraterNodeState} to IDLE. Only if the node is
	 * OFFLINE do ignore the switch of the CraterNodeState.
	 * 
	 * @param
	 */
	public final void setCraterNodeRole(CraterNodeRole nodeRole) {

		/*
         * Insanity checks - this call should come from ChurnModel trying to set nodes offline. However, those nodes are
		 * by default offline, thus ignore.
		 */
        assert this.nodeRole != nodeRole : "NodeRole must not be the same as the stored value";

        this.nodeRole = nodeRole;

        if (hasPerNodeHasNoSinkAnalyzer())
        {
            getPerNodeHasNoSinkTimeAnalyzer().onCraterNodeRoleChanged(this, nodeRole);
        }
        if (hasPerNodeStateTimeAnalyzer())
        {
            getPerNodeStateTimeAnalyzer().onCraterNodeRoleChanged(this, nodeRole);
        }
        // System.out.println(getHost().getId().value() + " became " + nodeRole.toString());

        Monitor.log(CraterNodeComponent.class, Monitor.Level.INFO,
                "Node " + getHost().getId().value() + " became " + nodeRole.toString());

        for (CraterNodeRoleListener l : nodeRoleListeners)
        {
            l.craterNodeRoleChanged(this.nodeRole);
        }
    }

    /**
     * Add a {@link CraterNodeRoleListener}.
     *
     * @param l
     */
    private void addCraterNodeRoleListener(CraterNodeRoleListener l)
    {
        nodeRoleListeners.add(l);
    }

    /**
     * Middleware-method for noSinkAdvertising to obtain info if sinkTable
     * (maintained by sinkAdvertising mechnism) is empty.
     *
     * @return <code>true</code> if sink table is empty, <code>false</code> if
     * not.
     */
    public boolean sinkTableIsEmpty()
    {
        return sinkAdvertising.sinkTableIsEmpty();
    }

    /**
     * Middleware-method for {@link DataRoutingOLD} to obtain nodes's best
     * sinkId.
     *
     * @return
     * @throws NoKnownSinksException
     */
    public Entry<INodeID, Double> getBestSinkTableEntry() throws NoKnownSinksException
    {
        if (sinkAdvertising == null)
        {
            throw new NoKnownSinksException();
        }
        return sinkAdvertising.getBestSinkTableEntry();
    }

    /**
     * Indicator method that {@link DataRoutingOLD} can inform
     * {@link SinkAdvertising} that {@link DataMessage} has been received.
     */
    public void hasReceivedDataMessageAsSink()
    {
        sinkAdvertising.hasReceivedDataMessageAsSink();
    }

    /**
     * Incoming packets... - forward to the respective mechanisms (SinkAdv,
     * NodeBeaconing, DataPacket)
     */
    @Override
    public void messageArrived(Message msg, TransInfo sender, int commID)
    {
        assert msg instanceof CraterCellMessage || msg instanceof CraterLocalMessage;

        if (msg instanceof SinkAdvertisingMessage)
        {

            SinkAdvertisingMessage sAdvMsg;
            /*
			 * Copying of received msg. In this step also decreasing TTL and
			 * gradient!
			 */
			if (msg instanceof CloudletAdvertisingMessage) {
				sAdvMsg = new CloudletAdvertisingMessage(
						(CloudletAdvertisingMessage) msg);
			} else {
				sAdvMsg = new SinkAdvertisingMessage(
						(SinkAdvertisingMessage) msg);
			}
			// to sink advertisment
			Monitor.log(CraterNodeComponent.class, Monitor.Level.INFO,
					"CraterNode: SinkAdvertisingMessage on Node " + getHost().getId().value() + " from " + sender.toString());
			sinkAdvertising.handleIncomingSinkAdvMsg(sAdvMsg);
			sinkAdvertisingMessages.putNow(sAdvMsg.getUniqueMsgID(), sAdvMsg);

			/*
			 * Check if we are sink, check if sink is allowed to dictate
			 * routing, check if sink is our sink, check if routing is different
			 * from ours
			 */
			try {
				if (!this.isSink() && computeSpeedAt.equals(ResponisbleComponent.SINKADV)
						&& sinkAdvertising.getBestSinkTableEntry().getKey().equals(sAdvMsg.getSinkId())
						&& sAdvMsg.isSendUnicast() == usesBroadcastRouting) {
					// If so, toggle routing
					toggleDataRouting();

				}
			} catch (NoKnownSinksException e) {
				// really shouldn't happen, we just received a SinkUpdate!
				e.printStackTrace();
			}

			// Incase of unicast routing...
			dataRouting.handleIncomingMessage(sAdvMsg);
		}

		else if (msg instanceof NoSinkMessage) {
			NoSinkMessage noSinkMsg = new NoSinkMessage((NoSinkMessage) msg);
			/*
			 * If the node is no sink - ignore beacon but mark that node beacon
			 * has been received to not send own 'noSink' beacon in the next
			 * interval
			 */
			switch (getCraterNodeRole()) {
			case LEAF:
				// to NodeBeaconing
				Monitor.log(CraterNodeComponent.class, Monitor.Level.INFO, "CraterNode: NoSinkMessage on Node "
						+ getHost().getId().value() + " from " + sender.toString() + " forwarding to NoSinkAdvertising");
				noSinkAdvertising.handleIncomingNoSinkMessage(noSinkMsg);
				noSinkMessages.putNow(noSinkMsg.getUniqueMsgID(), noSinkMsg);
				break;

			case SINK:
				// to sink advertisement
				Monitor.log(CraterNodeComponent.class, Monitor.Level.INFO, "CraterNode: NoSinkMessage on Node "
						+ getHost().getId().value() + " from " + sender.toString() + " forwarding to SinkAdvertising");
				sinkAdvertising.handleIncomingNoSinkMsg(noSinkMsg);
				break;

			default:
				break;
			}
		}

		else if (msg instanceof DataMessageContainerLocalImpl || msg instanceof UnicastReply) {

			dataRouting.handleIncomingMessage((CraterLocalMessage) msg);

			if (msg instanceof DataMessageContainerLocalImpl) {
				Monitor.log(CraterNodeComponent.class, Monitor.Level.INFO, "CraterNode: CraterDataContainerMsg on Node "
						+ getHost().getId().value() + " from " + sender.toString() + " forwarding to DataRouting");

				DataMessageContainerLocalImpl oMsg = (DataMessageContainerLocalImpl) msg;

				DataMessageContainerLocalImpl dataMessageContainer = new DataMessageContainerLocalImpl(oMsg);
				dataMessages.putNow(dataMessageContainer.getUniqueMsgID(), dataMessageContainer);

				/*
				 * New! Case1: Check if msg was unicast directed at us, if so
				 * send an answer.
				 * 
				 * Case2: It was a broadcast, but we are in Unicast. Send an
				 * answer if own gradient is larger than the msg's one -it could
				 * have been a failfast Broadcast
				 * 
				 * Use original msg, not copy constructed one! Done here to be
				 * independent of actual DataRouting and because of access to
				 * the commID
				 */
				// Case1: Should'nt actually receive any unicast not directed at
				// us.
				if ((oMsg.getReceiver() != null && oMsg.getReceiver().getNodeID().equals(getHost().getId())) ||
				// Case2:
						(oMsg.getReceiver() == null && !usesBroadcastRouting)) {

					// Sanity check, needed for reasons...o.O
					if (!oMsg.getSender().getNetID(netTypeLocal).equals(sender)) {
						// Caused by standard broadcasting node using this ones
						// senderID when forwarding
						return;
					}

					/*
					 * Check if we are sink, and the intended one, or a Leave of
					 * the target Sink. If so, send an Ack back.
					 */
					double curGrad = 0;
					boolean sendAnswer = false;
					switch (getCraterNodeRole()) {
					case SINK:
						if (oMsg.getSinkId().equals(getHost().getId())) {
							curGrad = SinkAdvertisingImpl.INITIAL_GRADIENT;
							sendAnswer = true;
						}
						break;
					case LEAF:
						try {
							SimpleEntry<INodeID, Double> bestSink = sinkAdvertising.getBestSinkTableEntry();
							curGrad = bestSink.getValue();
							if (oMsg.getSinkId().equals(bestSink.getKey())
									&& (oMsg.getReceiver() != null || oMsg
											.getGradient() < curGrad)) {
								sendAnswer = true;
							}
						} catch (NoKnownSinksException e) {
							// Nothing to do here
						}
						break;
					default:
						break;

					}
					if (sendAnswer) {
						UnicastReply ack = new UnicastReply(getLocalOverlayContact(), oMsg.getSender(), curGrad);
						if (oMsg.getReceiver() != null) {
							// was directed at us, send as 'reply' to trigger
							// UDP-callback
							sendLocalReply(ack, commID);
						} else {
							// was broadcast, send as 'normal' message
							this.sendLocalUnicast(ack, null, 0);
						}
					}
				}
			}
		}

		else if (msg instanceof CloudConfigurationMessage) {
			Monitor.log(CraterNodeComponent.class, Monitor.Level.INFO,
					"CloudConfigurationMessage received on Node " + getHost().getId().value() + " from " + sender.toString());
			handleCloudConfigurationMessage((CloudConfigurationMessage) msg, commID);
		}
		// else if (msg instanceof GLConfigurationMessage) {
		// Monitor.log(CraterNodeComponent.class, Monitor.Level.INFO,
		// "GLConfigurationMessage received on Node "
		// + getHost().getId().value() + " from " + sender.toString());
		// if (useGatewaySelectionClient) {
		// this.dgLogicController.handleIncommingMessage((GLConfigurationMessage)
		// msg);
		// }
		// }
		else if (msg instanceof CellularResponseMessage) {
			// forward to requestResolver
			requestResolver.handleIncommingMessage(msg);
		}

		else if (msg instanceof RequestMessageLocal || msg instanceof RequestMessageCellular) {
			requestRouting.handleIncommingRequestMessage(msg);
		}

		else if (msg instanceof RequestInquiryMessage) {
			requestRouting.handleIncommingRequestInquiry((RequestInquiryMessage) msg);
		}

		else {
			throw new AssertionError("Received Message of unknown type " + msg.getClass());
		}

		// Analyzer
		if (hasMessageAnalyzer()) {
			if (msg instanceof CraterCellMessage) {
				getMessageAnalyzer().onReceivedOverlayMessage((CraterCellMessage) msg, getHost());
			}
			if (msg instanceof CraterLocalMessage) {
				getMessageAnalyzer().onReceivedOverlayMessage((CraterLocalMessage) msg, getHost());
			}
		}
	}

	/**
	 * Used to handle an in coming {@link CloudConfigurationMessage}. Perform
	 * actions according to the content of the message.
	 * 
	 * @param msg
	 * @param commID
	 */
	protected void handleCloudConfigurationMessage(CloudConfigurationMessage msg, int commID) {
		assert this.getCraterNodeRole() != msg
				.getNodeRole() : "Cloud configuration for node role should always include a change of the role of the node";

		setCraterNodeRole(msg.getNodeRole());
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		if (source instanceof CraterCentralComponent) {
			return;
		}

		assert source instanceof CraterNodeComponent;

		switch (peerStatus) {
		case ABSENT:
			setCraterNodeRole(CraterNodeRole.OFFLINE);
			break;

		case PRESENT:
			setCraterNodeRole(CraterNodeRole.LEAF);
			break;
		default:
			break;
		}
	}

	/**
	 * {@link CraterNodeComponent} role.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	public enum CraterNodeRole {
		SINK,

		LEAF,

		OFFLINE
	}

	@Override
	public String getNodeDescription() {
		return "";
	}

	/**
	 * Check the current state of the TimeoutMap containing recently received Data Messages for messages, that haven't
	 * timed out yet
	 * 
	 * @return Set of DataMessageContainerLocalImpl that haven't timed out yet
	 */
	private Set<DataMessageContainerLocalImpl> getAliveDataMessages() {

		Map<UniqueID, DataMessageContainerLocalImpl> currentMessages = dataMessages.getUnmodifiableMap();

		Set<DataMessageContainerLocalImpl> msgs = new HashSet<DataMessageContainerLocalImpl>(
				currentMessages.values());

		return msgs;
	}

	/**
	 * Check for recently received Data Messages, that haven't timed out yet and return the senders of those messages
	 * 
	 * @return Set of OverlayContact representing the senders of data messages
	 */

	public Set<OverlayContact> getSendersDataMessages() {
		Set<OverlayContact> senders = new HashSet<OverlayContact>();

		for (DataMessageContainerLocalImpl msg : getAliveDataMessages()) {
			senders.add(msg.getSender());

		}

		return senders;
	}

	/**
	 * Check the current state of the TimeoutMap containing recently received No-Sink-Messages for messages, that
	 * haven't timed out yet
	 * 
	 * @return Set of NoSinkMessage that haven't timed out yet
	 */
	private Set<NoSinkMessage> getAliveNoSinkMessages() {
		Map<UniqueID, NoSinkMessage> currentMessages = noSinkMessages.getUnmodifiableMap();

		Set<NoSinkMessage> msgs = new HashSet<NoSinkMessage>(currentMessages.values());

		return msgs;
	}

	/**
	 * Check for recently received NoSinkMessages, that haven't timed out yet and return the senders of those messages
	 * 
	 * @return Set of OverlayContact representing the senders of no-sink-messages
	 */

	public Set<OverlayContact> getSendersNoSinkMessages() {

		Set<OverlayContact> senders = new HashSet<OverlayContact>();
		for (NoSinkMessage msg : getAliveNoSinkMessages()) {
			senders.add(msg.getSender());
		}
		return senders;
	}

	/**
	 * Check the current state of the TimeoutMap containing recently received Sink-Advertisement-Messages for messages,
	 * that haven't timed out yet
	 * 
	 * @return Set of SinkAdvertisingMessage that haven't timed out yet
	 */

	private Set<SinkAdvertisingMessage> getAliveSinkAdMessages() {
		Map<UniqueID, SinkAdvertisingMessage> currentMessages = sinkAdvertisingMessages.getUnmodifiableMap();

		Set<SinkAdvertisingMessage> msgs = new HashSet<SinkAdvertisingMessage>(currentMessages.values());
		return msgs;
	}

	/**
	 * Check for recently received SinkMessages, that haven't timed out yet and return the senders of those messages
	 * 
	 * @return Set of OverlayContact representing the senders of sink-messages
	 */
	public Set<OverlayContact> getSendersSinkAdMessages() {
		Set<OverlayContact> senders = new HashSet<OverlayContact>();
		for (SinkAdvertisingMessage msg : getAliveSinkAdMessages()) {
			senders.add(msg.getSender());
		}
		return senders;
	}

	// Schema:
	// Bedingung ? Zutreffend : nicht Zutreffend;
	@Override
	public int getNodeColor(int dimension) {

		switch (dimension) {
		case 0: // Type/State (leaf/inactive, leaf/active, sink...)
			switch (getCraterNodeRole()) {
			case LEAF:
				switch (noSinkAdvertising.getComponentState()) {
				case IDLE:
					return 0;
				case ACTIVE:
					return 1;
				case INACTIVE:

					break;
				default:
					break;
				}
				break;
			case SINK:
				switch (sinkAdvertising.getComponentState()) {
				case IDLE:
					return 2;
				case ACTIVE:
					return 3;
				case INACTIVE:

					break;
				default:
					break;
				}
				break;
			case OFFLINE:
				return -1;
			default:
				throw new AssertionError("Unknonw CraterNodeState.");
			}
		default:
			return -1;
		}
	}

	@Override
	public void updatedLatency(OverlayContact contact, long latency) {
		// Currently not used to update anything. Just to have the Latency
		// Service running without having no listener.

		// SiSComponent sis;
		// try {
		// sis = getHost().getComponent(SiSComponent.class);
		// // SiSRequest request = new
		// SiSRequest().setScope(SiSScope.NODE_LOCAL);
		// SiSRequest request = new SiSRequest();
		// double currentMeasuredLatency =
		// sis.get().localObservationOf(getCloudContact().getNodeID(),
		// SiSTypes.LATENCY_CELL,
		// request);
		// System.out.println("SiS CellularLatency: " + currentMeasuredLatency);
		// } catch (ComponentNotAvailableException |
		// InformationNotAvailableException e) {
		// System.out.println("No info about that on node " +
		// getHost().getId().toString());
		// e.printStackTrace();
		// }
	}

	/**
	 * Called by a modified location Listener if each CraterNode should toggle
	 * DataRouting depending on its own speed
	 * 
	 * @param speed
	 */
	public void updateSpeedEstimate(double speed) {
		if (!isPresent()) {
			return;
		}
		// TODO add logic, MA or similar filterFunction
		if ((speed < slowSpeed - slowSpeedOffset) && usesBroadcastRouting) {
			toggleDataRouting();
		} else if ((speed > slowSpeed + slowSpeedOffset) && !usesBroadcastRouting) {
			toggleDataRouting();
		}
	}

	@Override
	public void gatewaySelectionFinished(NodeRole role, INodeID clusterhead) {
		// NodeRole != CraterNodeRole
		
		if(this.nodeRole.equals(CraterNodeRole.OFFLINE)){
			return;
		}
		
		if(role == NodeRole.CLUSTERHEAD && this.nodeRole != CraterNodeRole.SINK){
			this.setCraterNodeRole(CraterNodeRole.SINK);
		}
		else if (role.equals(NodeRole.CLUSTERMEMBER) && !this.nodeRole.equals(CraterNodeRole.LEAF)) {
			setCraterNodeRole(CraterNodeRole.LEAF);
		}
	}

	public ResponisbleComponent getComputeSpeedAt() {
		return computeSpeedAt;
	}

	@Override
	public int getNodeColorDimensions() {
		return dimDesc.length;
	}

	private static final String[] dimDesc = { "Type/State" };

	private static final String[][] colorDesc = { { "leaf/idle", "leaf/active", "sink/idle", "sink/active" } };

	@Override
	public String[] getNodeColorDimensionDescriptions() {
		return dimDesc;
	}

	@Override
	public String[] getNodeColorDescriptions(int dimension) {
		return colorDesc[dimension];
	}

	/**
	 * Getter for the DataRouting (proxy) Interface
	 * 
	 * @return the DataRouting
	 */
	public DataRouting getDataRouting() {
		return dataRouting;
	}

	public CraterLocationListener getLocationListener() {
		return speedListener;
	}

	public boolean isBroadcastRouting() {
		return usesBroadcastRouting;
	}

	public double getSlowSpeed() {
		return slowSpeed;
	}

	public double getSlowSpeedOffset() {
		return slowSpeedOffset;
	}

	/**
	 * ------ Configuration Setters -------------
	 * 
	 * 
	 * From below this comment only configuration Setters should be used.
	 */

	/**
	 * Set by the configuration.
	 * 
	 * @param noSinkAdvInt
	 */
	public void setNoSinkAdvInt(long noSinkAdvInt) {
		this.NO_SINK_ADV_INT = noSinkAdvInt;
	}

	/**
	 * Set by the configuration.
	 * 
	 * @param sinkAdvInterval
	 */
	public void setSinkAdvInterval(long sinkAdvInterval) {
		this.SINK_ADV_PERIODIC_OPERATION_INTERVAL = sinkAdvInterval;
	}

	/**
	 * Set by the configuration.
	 * 
	 * @param initialTTL
	 */
	public void setInitialTTL(int initialTTL) {
		this.INITIAL_TTL = initialTTL;
	}

	/**
	 * Set by the configuration.
	 * 
	 * @param dateSendingPeriodicInterval
	 */
	public void setDataSendingPeriodicInterval(long dateSendingPeriodicInterval) {
		this.DATA_SENDING_PERIODIC_OPERATION_INTERVAL = dateSendingPeriodicInterval;
	}

	/**
	 * Set by the configuration.
	 * 
	 * @param dataUploadInterval
	 */
	public void setDataUploadInterval(long dataUploadInterval) {
		DATA_UPLOAD_INTERVAL = dataUploadInterval;
	}

	/**
	 * Set by the configuration.
	 * 
	 * @param measurementProviderToUse
	 */
	@Deprecated
	public void setMeasurementProviderToUse(String measurementProviderToUse) {
		this.measurementProviderToUse = measurementProviderToUse;
	}

	/**
	 * Sets the defined type of History
	 * 
	 * @param sendHistoryType
	 * @author Jonas Huelsmann
	 */
	public void setHistoryType(HistoryType sendHistoryType) {
		this.sendHistoryType = sendHistoryType;

	}

	/**
	 * Sets the Value for the History
	 * 
	 * @param sendHistoryType
	 * @author Jonas Huelsmann
	 */
	public void setHistoryValue(long sendHistoryValue) {
		this.sendHistoryValue = sendHistoryValue;

	}

	/**
	 * Sets the defined type of the ContentionScheme
	 * 
	 * @param sendHistoryType
	 * @author Jonas Huelsmann
	 */
	public void setContentionType(ContentionSchemeType contentionType) {
		this.contentionType = contentionType;

	}

	/**
	 * Sets the defined type of the SinkPreference
	 * 
	 * @param sendHistoryType
	 * @author Jonas Huelsmann
	 */
	public void setSinkPreferenceType(SinkPreferenceType sinkPreferenceType) {
		sinkPref = sinkPreferenceType;

	}

	/**
	 * Sets where the speedUpdates are used to alter between BroadCast and
	 * UnicastRouting
	 * 
	 * @param computeAt
	 */
	public void setComputeSpeedAt(ResponisbleComponent computeAt) {
		computeSpeedAt = computeAt;
	}

	/**
	 * CenterValue of the hysterese responsible to toggle DataRouting
	 * 
	 * @param slowSpeed
	 */
	public void setSlowSpeed(double slowSpeed) {
		this.slowSpeed = slowSpeed;
	}

	public void setSlowSpeedOffset(double slowSpeedOffset) {
		this.slowSpeedOffset = slowSpeedOffset;
	}

	/**
	 * How often the speedUpdates are taken.
	 * 
	 * @param interval
	 */
	public void setLocationUpdateInterval(long interval) {
		locationUpdateInterval = interval;
	}

	/**
	 * Sets which modifier-class is used by the SinkAdvertisement to adapt the
	 * adv-interval
	 * 
	 * @param intervalDeterminator
	 */
	public void setSinkAdvIntervalModifier(SinkAdvIntervalModifier intervalDeterminator) {
		this.intervalDeterminator = intervalDeterminator;
	}

	/**
	 * Sets whether or not to use Gateway Clients
	 */
	public void setUseGatewaySelectionClient(boolean useGSC) {
		this.useGatewaySelectionClient = useGSC;
	}

	@Override
	public void invokeRequest(Request request) {
		request = new Request(request);
		request.prepareForSending(getHost().getId());
		requestRouting.sendRequest(request);

		// Inform the analyzer
		if (getRequestResolver().hasRequestResponseAnalyzer()) {
			getRequestResolver().getRequestResponseAnalyzer().onRequestInvoked(request);
		}
	}

	public CraterRequestResolver getRequestResolver() {
		return this.requestResolver;
	}

	public RequestRouting getRequestRouting() {
		return this.requestRouting;
	}

	public void setRequestTTL(int ttl) {
		this.requestTTL = ttl;
	}

	public void setResponseHesitationFactor(double factor) {
		this.responseHesitationFactor = factor;
	}

	/**
	 * Return the battery percentage.
	 * 
	 * @return battery percentage as double value.
	 */
	public double getCurrentBatteryPercentage() {
		return batSensor.getCurrentPercentage();
	}
}