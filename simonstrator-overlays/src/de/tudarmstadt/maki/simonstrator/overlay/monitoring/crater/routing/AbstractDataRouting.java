package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.ResponisbleComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.DuplicateInsensitiveAggregationFunction.DuplicateInsensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.DuplicateSensitiveAggregationFunction.DuplicateSensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attribute.Attributes;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attributegenerator.AttributeGenerator;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attributegenerator.measurementprovider.IMeasurementProvider;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.AbstractContentionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.ContentionScheme.ContentionSchemeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.DefaultCraterContentionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.DiFiCraterContentionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.MoDiFiCraterContentionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.AggregationFunctionNotKnownException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.MergeNotPossibleException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history.SendHistory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history.SendHistory.HistoryType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainer.MergePoint;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupInsensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupSensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.ResponseObject;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.SinkUploadContainer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers.AggregationDataBufferEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers.LocalBufferEntryLists;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers.NormalDataBufferEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers.UploadBufferEntryLists;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.operations.PeriodicSendDataOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.operations.PeriodicUploadOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.ResponseEventHandler;

/**
 * Abstract class for DataRouting
 * 
 * @author Nils Richerzhagen
 * @author Christoph Storm
 * 
 * @version 2016.07.21
 */
public abstract class AbstractDataRouting implements DataRouting {

	/*
	 * Config Parameters
	 */
	private static long DATA_SENDING_PERIODIC_OPERATION_INTERVAL;
	private static long DATA_UPLOAD_INTERVAL;
	private static HistoryType sendHistoryType;
	private static long sendHistoryValue;
	private static ContentionSchemeType contentionType;

	private static boolean sendSpeedEstimates = true;
	/*
	 * Other fixed parameters
	 */
	protected static final long RECEIVED_DATA_MESSAGES_TIMEOUT = 3 * Time.MINUTE;

	protected static final long MIN_DATA_ROUTING_HESITATION_TIME = 0 * Time.SECOND;
	protected static long MAX_DATA_ROUTING_HESITATION_TIME = -1;

	protected static long sinkMaxResponseForwardingDelay = 5 * Time.SECOND;

	// FIXME really unused?
	protected static final long DATA_SENDING_PERIODIC_OPERATION_START_DELAY = 30 * Time.SECOND;

	// FIXME really unused?
	protected static long DATA_UPLOAD_START_DELAY = 30 * Time.SECOND;

	protected static final int DATA_CONTAINER_MESSAGE_FORWARDING_EVENT = 40;
	protected static final int DATA_CONTAINER_MESSAGE_SENDING_EVENT = 41;


	// Transferstate annotation
	@TransferState(value = { "ParentNode" })
	private CraterNodeComponent parentNode;

	@TransferState(value = { "History" })
	private SendHistory history;

	// declared here as every subclass of this is going to use 'em anyway
	@TransferState(value = { "PeriodicSendDataOperation" })
	private PeriodicSendDataOperation periodicSendDataOperation;

	@TransferState(value = { "PeriodicUploadOperation" })
	private PeriodicUploadOperation periodicUploadOperation;

	@TransferState(value = { "SendingScheduler" })
	private ResponseEventHandler sendingScheduler;

	/*
	 * Buffers for own data that should be send or uploaded!
	 * 
	 * CAUTION! These should be filled by workload app! Maybe with listener
	 * concept of DataRouting hearing from Workload app if data is incoming.
	 */
	@TransferState(value = { "LocalBuffer" })
	private LocalBufferEntryLists localBuffer;

	/*
	 * Buffers for uploading of received ContainerMessage contents.
	 */
	@TransferState(value = { "UploadBuffer" })
	private UploadBufferEntryLists uploadBuffer;

	@TransferState(value = { "LastSpeedUpdateTime" })
	private Long lastSpeedUpdateTime = -1L;

	@TransferState(value = { "ClusterSpeedComp" })
	private ClusterSpeedComputation clusterSpeedComp;


	/**
	 * Constructor for transfer
	 * 
	 * @param parentNode
	 * @param history
	 * @param localBuffer
	 * @param uploadBuffer
	 * @param periodicSendDataOperation
	 *            -this triggers {@link sendBufferContent}
	 * @param periodicUploadOperation
	 *            -this triggers {@link uploadBufferContent}
	 * @param ResponseEventHandler
	 *            sendingScheduler
	 * @param Long
	 *            lastSpeedUpdateTime
	 * @param ClusterSpeedComputation
	 *            clusterSpeedComp
	 */
	public AbstractDataRouting(CraterNodeComponent parentNode, SendHistory history, LocalBufferEntryLists localBuffer,
			UploadBufferEntryLists uploadBuffer,
			PeriodicSendDataOperation periodicSendDataOperation,
			PeriodicUploadOperation periodicUploadOperation,
			ResponseEventHandler sendingScheduler, Long lastSpeedUpdateTime,
			ClusterSpeedComputation clusterSpeedComp) {
		this.parentNode = parentNode;
		this.history = history;
		this.localBuffer = localBuffer;
		this.uploadBuffer = uploadBuffer;
		this.periodicSendDataOperation = periodicSendDataOperation;
		this.periodicUploadOperation = periodicUploadOperation;
		this.sendingScheduler = sendingScheduler;
		this.lastSpeedUpdateTime = lastSpeedUpdateTime;
		this.clusterSpeedComp = clusterSpeedComp;
	}

	/**
	 * initial constructor...
	 */
	public AbstractDataRouting(CraterNodeComponent parentNode, long DATA_SENDING_PERIODIC_OPERATION_INTERVAL,
			long DATA_UPLOAD_INTERVAL, HistoryType sendHistoryType, long sendHistoryValue,
			ContentionSchemeType contentionType) {
		this.parentNode = parentNode;
		AbstractDataRouting.DATA_SENDING_PERIODIC_OPERATION_INTERVAL = DATA_SENDING_PERIODIC_OPERATION_INTERVAL;
		AbstractDataRouting.DATA_UPLOAD_INTERVAL = DATA_UPLOAD_INTERVAL;
		AbstractDataRouting.sendHistoryType = sendHistoryType;
		AbstractDataRouting.sendHistoryValue = sendHistoryValue;
		AbstractDataRouting.contentionType = contentionType;
		this.localBuffer = new LocalBufferEntryLists();
		this.uploadBuffer = new UploadBufferEntryLists();
		this.history = SendHistory.createHistory(sendHistoryType, sendHistoryValue);
		this.sendingScheduler = new ResponseEventHandler(parentNode);
		clusterSpeedComp = new ClusterSpeedComputation(parentNode,
				parentNode.getComputeSpeedAt().equals(ResponisbleComponent.SINKADV),
				2 * DATA_SENDING_PERIODIC_OPERATION_INTERVAL);
	}

	/**
	 * Initialization of periodic operations used in {@link DataRouting}
	 */
	protected void initializePeriodicOperations() {
		periodicSendDataOperation = new PeriodicSendDataOperation(parentNode, Operations.getEmptyCallback(),
				DATA_SENDING_PERIODIC_OPERATION_INTERVAL);

		periodicUploadOperation = new PeriodicUploadOperation(parentNode, Operations.getEmptyCallback(),
				DATA_UPLOAD_INTERVAL);
	}

	/**
	 * Method that initializes the used contention scheme
	 * 
	 * @author Jonas Huelsmann
	 */
	protected static AbstractContentionScheme createContentionScheme(CraterNodeComponent cnc) {
		switch (contentionType) {
		case DIFI:
			return new DiFiCraterContentionScheme(MIN_DATA_ROUTING_HESITATION_TIME, MAX_DATA_ROUTING_HESITATION_TIME);
		case MODIFI:
			return new MoDiFiCraterContentionScheme(MIN_DATA_ROUTING_HESITATION_TIME, MAX_DATA_ROUTING_HESITATION_TIME);
		default:
			return new DefaultCraterContentionScheme(MIN_DATA_ROUTING_HESITATION_TIME, MAX_DATA_ROUTING_HESITATION_TIME,
					cnc);
		}
	}

	/**
	 * Method to set the maximum hesitation time. The maximum hesitation time
	 * will only be set once and can not be changed in the running process.
	 * 
	 * @param time
	 *            The Maximum data routing hesitation time to be set
	 * 
	 * @author Jonas Huelsmann
	 */
	public static void setMaxHesitation(long time) {

		if (MAX_DATA_ROUTING_HESITATION_TIME == -1) {
			MAX_DATA_ROUTING_HESITATION_TIME = time;
		} else {
			//
		}

	}

	@Override
	public void uploadBufferContent() {
		switch (parentNode.getCraterNodeRole()) {
		case SINK:

			boolean hasAddedData = false;

			if (!uploadBuffer.getNormalDataObjects().isEmpty()
					|| !uploadBuffer.getAggregationDupInsensitiveData().isEmpty()
					|| !uploadBuffer.getAggregationDupSensitiveData().isEmpty()) {
				hasAddedData = true;
			}

			SinkUploadContainer uploadMsg = new SinkUploadContainer(parentNode.getLocalOverlayContact(),
					parentNode.getCloudContact(),
					new LinkedList<DataObjectImpl<Double>>(uploadBuffer.getNormalDataObjects().values()),
					new LinkedList<DataObjectAggregationDupInsensitiveImpl>(
							uploadBuffer.getAggregationDupInsensitiveData()),
					new LinkedList<DataObjectAggregationDupSensitiveImpl>(
							uploadBuffer.getAggregationDupSensitiveData()));

			if (!uploadBuffer.getResponseObjects().isEmpty()) {
				// seems like a nonsese duplicated condition, but has to do with
				// 'hasAddedData'
				while (!uploadBuffer.getResponseObjects().isEmpty()) {

					ResponseObject obj = uploadBuffer.getResponseObjects()
							.poll();
					if (obj.getValidity() > Time.getCurrentTime()) {
						uploadMsg.putResponse(obj);
					}
				}
				hasAddedData = true;
			}

			if (hasDataRoutingAnalyzer()) {
				/*
				 * Give all ID's to the analyzer of the buffers here as the
				 * buffer entries are changed afterwards by the content added
				 * with the own data.
				 */
				LinkedList<LinkedList<UniqueID>> uploadBufferEntryIDs = new LinkedList<LinkedList<UniqueID>>();
				for (DataObjectAggregationDupSensitiveImpl actDupSensUploadBufferObject : uploadBuffer
						.getAggregationDupSensitiveData()) {
					uploadBufferEntryIDs.add(actDupSensUploadBufferObject.getIncludedEntriesIDs());
				}

				getDataRoutingAnalyzer().onMessageContentUploadedAtSink(
						new LinkedList<UniqueID>(uploadBuffer.getNormalDataObjects().keySet()),
						new LinkedList<DataObjectAggregationDupInsensitiveImpl>(
								uploadBuffer.getAggregationDupInsensitiveData()),
						uploadBufferEntryIDs);
			}

			fillBuffersWithAttributeGeneratorData();
			// No own data to upload.

			if (!localBuffer.getNormalData().isEmpty()) {
				NormalDataBufferEntry firstEntry = localBuffer.getNormalData().removeFirst();
				uploadMsg = new SinkUploadContainer(uploadMsg, parentNode.getHost().getId(),
						firstEntry.getAttributeId(), firstEntry.getValue());
				hasAddedData = true;
			}
			if (!localBuffer.getDuplicateInsensitiveAggregateData().isEmpty()) {
				AggregationDataBufferEntry firstEntry = localBuffer.getDuplicateInsensitiveAggregateData()
						.removeFirst();
				AggregationFunctionEntry toAddEntry = new AggregationFunctionEntry(parentNode.getHost().getId(),
						firstEntry.getValue());
				try {
					uploadMsg = new SinkUploadContainer(uploadMsg, firstEntry.getAggregationFunctionId(),
							firstEntry.getAttributeId(), toAddEntry);
				} catch (AggregationFunctionNotKnownException e) {
					e.printStackTrace();
				}
				hasAddedData = true;
			}
			if (!localBuffer.getDuplicateSensitiveAggregateData().isEmpty()) {
				AggregationDataBufferEntry firstEntry = localBuffer.getDuplicateSensitiveAggregateData()
						.removeFirst();
				AggregationFunctionEntry toAddEntry = new AggregationFunctionEntry(parentNode.getHost().getId(),
						firstEntry.getValue());
				try {
					uploadMsg = new SinkUploadContainer(uploadMsg, firstEntry.getAggregationFunctionId(),
							firstEntry.getAttributeId(), toAddEntry);
				} catch (AggregationFunctionNotKnownException e) {
					e.printStackTrace();
				}
				hasAddedData = true;
			}
			if (!localBuffer.getResponseObjects().isEmpty()) {

				while (!localBuffer.getResponseObjects().isEmpty()) {
					uploadMsg.putResponse(localBuffer.getResponseObjects().poll());
				}
				hasAddedData = true;
			}

			if (!hasAddedData) {
				System.out.println("Should not happen");
			}

			if (hasAddedData) {
				parentNode.sendViaCellular(uploadMsg);
				// /*
				// * - #A.1# upload msg internals to analyzer for duplicate
				// * detection of real data.
				// */
				if (hasDataRoutingAnalyzer()) {
					getDataRoutingAnalyzer().uploadMsgSend(uploadMsg.getUniqueMsgID(), parentNode);
				}

				// System.out.println("Cluster" +
				// parentNode.getHost().getHostId() + "_speed is " + speed + "
				// m/s");

				uploadBuffer.clear();

				// reset history if it is a default type
				if (sendHistoryType == HistoryType.DEFAULT) {
					try {
						history.resetHistory();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			break;

		default:
			break;
		}
	}

	/**
	 * Handles adding Data to the appropriate buffers as well as timing of to
	 * send cellular msgs. E.g., for Responses. <br>
	 * <b>Make sure that this message was intended for us!</b>
	 * 
	 * @param msg
	 */
	protected void onReceivedDataContainerAsSink(DataMessageContainerLocalImpl msg) {
		/*
		 * Indicator for SinkAdvertising that DataMessageContainer has been
		 * received by Sink. This message was also considered for that sink!
		 */
		getParentNode().hasReceivedDataMessageAsSink();
		Monitor.log(this.getClass(), Monitor.Level.DEBUG,
				"Sink: Message from " + msg.getSender().getNodeID() + " at " + msg.getSinkId());

		// #3.1#
		/*
		 * Add normal data to upload buffer.
		 */
		LinkedList<DataObjectImpl<Double>> normalDataOfMsg = msg.getNormalDataList();
		for (DataObjectImpl<Double> actReceivedNormalDataObject : normalDataOfMsg) {
			/*
			 * Now checks in the history for duplicates instead of current
			 * Buffer
			 * 
			 * @author Jonas Huelsmann
			 */
			if (!getHistory().sendable(actReceivedNormalDataObject.getUniqueDataObjectID())) {
				if (hasDataRoutingAnalyzer()) {
					getDataRoutingAnalyzer().onNormalDataDuplicateDetectedAtSink();
				}
			} else {
				getUploadBuffer().getNormalDataObjects().put(actReceivedNormalDataObject.getUniqueDataObjectID(),
						actReceivedNormalDataObject);
				if (hasDataRoutingAnalyzer()) {
					// For latency Orig - to - Sink of NormalData
					getDataRoutingAnalyzer()
							.onNormalDataObjectReceivedAtSink(actReceivedNormalDataObject.getUniqueDataObjectID());
				}
				// special treatment for speeds
				if (actReceivedNormalDataObject.getAttributeId() == Attributes.NODESPEED.getAttributeId()) {
					getClusterSpeedComp().addSpeedUpdate(actReceivedNormalDataObject);
				}
			}
		}

		/*
		 * Add duplicate insensitive data to upload buffer.
		 */
		LinkedList<DataObjectAggregationDupInsensitiveImpl> dupInsenAggDataOfMsg = msg
				.getAggregationDupInsensitiveDataList();
		// go through msg content
		for (DataObjectAggregationDupInsensitiveImpl actReceivedDupInsenAggDataObject : dupInsenAggDataOfMsg) {
			boolean entryExisted = false;
			// go through current upload buffer
			for (DataObjectAggregationDupInsensitiveImpl actUploadBufferDataObject : getUploadBuffer()
					.getAggregationDupInsensitiveData()) {
				// if upload buffer has same object (attributeId &
				// aggregation function) add received msg data to
				// already contained object.
				if (actReceivedDupInsenAggDataObject.ofSameType(actUploadBufferDataObject)) {
					actUploadBufferDataObject.putValue(actReceivedDupInsenAggDataObject.getAggregateValue());
					entryExisted = true;
				}
			}
			// if not contained add it now.
			if (!entryExisted) {
				getUploadBuffer().getAggregationDupInsensitiveData().add(actReceivedDupInsenAggDataObject);
			}
		}

		/*
		 * Add duplicate sensitive data to upload buffer.
		 */
		LinkedList<DataObjectAggregationDupSensitiveImpl> dupSensAggDataOfMsg = msg
				.getAggregationDupSensitiveDataList();
		// go through msg content
		for (DataObjectAggregationDupSensitiveImpl actReceivedDupSensAggDataObject : dupSensAggDataOfMsg) {
			boolean entryExisted = false;
			// to through current upload buffer
			for (DataObjectAggregationDupSensitiveImpl actUploadBufferDataObject : getUploadBuffer()
					.getAggregationDupSensitiveData()) {
				// if upload buffer object and received object are
				// of
				// same type merge the received object into the
				// upload
				// buffer object.
				if (actReceivedDupSensAggDataObject.ofSameType(actUploadBufferDataObject)) {
					try {
						actUploadBufferDataObject.mergeValues(actReceivedDupSensAggDataObject, MergePoint.IN_NETWORK);
					} catch (MergeNotPossibleException e) {
						e.printStackTrace();
					}
					entryExisted = true;
				}
			}
			// if not contained any same entry add it now
			if (!entryExisted) {
				getUploadBuffer().getAggregationDupSensitiveData().add(actReceivedDupSensAggDataObject);
			}

			// For latency Orig - to - Sink of DupSensEntries
			if (hasDataRoutingAnalyzer())
				getDataRoutingAnalyzer()
						.onDupSensEntriesReceivedAtSink(actReceivedDupSensAggDataObject.getIncludedEntriesIDs());
		}
		////////////////////////////////////////////////////////////
		// Response Handling
		// v 2016.07.21
		////////////////////////////////////////////////////////////
		if (!msg.getQueryResponseList().isEmpty()) {
			// add responses to uploadBuffer
			getUploadBuffer().getResponseObjects().addAll(msg.getQueryResponseList());

			if (this.getTimeOfNextScheduledDataMessage() > Time.getCurrentTime() + sinkMaxResponseForwardingDelay) {
				this.getSendingScheduler().setDelayToSend(sinkMaxResponseForwardingDelay);
			}
		}
	}

	/**
	 * Just for know until buffers are filled by workload apps.
	 * 
	 * Must be exchanged with some real data. E.g. Position, node state etc.
	 * Once the SiS is included and the monitoring is able to handle requests
	 * this must be exchanged.y
	 * 
	 * @return
	 */
	@Deprecated
	protected void fillBuffersWithAttributeGeneratorData() {
		try {
			AttributeGenerator attributeGenerator = parentNode.getHost().getComponent(AttributeGenerator.class);
			LinkedList<IMeasurementProvider> measurementProvs = attributeGenerator.getProvs();
			for (IMeasurementProvider actMeasurementProvider : measurementProvs) {
				if (actMeasurementProvider.getIdentifier().equals(parentNode.getMeasurementProviderToUse())) {
					NormalDataBufferEntry normalData = new NormalDataBufferEntry(
							Attributes.StartupDelay.getAttributeId(), actMeasurementProvider.getNextValue());
					localBuffer.getNormalData().add(normalData);
					AggregationDataBufferEntry dupInsens = new AggregationDataBufferEntry(
							DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.getId(),
							Attributes.StartupDelay.getAttributeId(), actMeasurementProvider.getNextValue());
					localBuffer.getDuplicateInsensitiveAggregateData().add(dupInsens);
					AggregationDataBufferEntry dupSens = new AggregationDataBufferEntry(
							DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId(),
							Attributes.StartupDelay.getAttributeId(), actMeasurementProvider.getNextValue());
					localBuffer.getDuplicateSensitiveAggregateData().add(dupSens);
				}
			}
		} catch (ComponentNotAvailableException e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * Unified method to fill the given DataMessageContainer with bufferData
	 * 
	 * @param dataMessageContainer
	 * @param firstSend
	 *            true if not forwarding; currently unused legacy
	 * @return
	 */
	protected DataMessageContainerLocalImpl fillWithBufferEntries(DataMessageContainerLocalImpl dataMessageContainer,
			boolean firstSend) {

		boolean hasAddedData = false;
		dataMessageContainer.removeOutdatedResponses();

		if (!localBuffer.getNormalData().isEmpty()) {
			NormalDataBufferEntry firstEntry = localBuffer.getNormalData().removeFirst();
			dataMessageContainer = new DataMessageContainerLocalImpl(dataMessageContainer,
					parentNode.getHost().getId(), firstEntry.getAttributeId(), firstEntry.getValue(), firstSend);
			// FIXME constructor for analyzer, can use normal
			// constructor again.
			hasAddedData = true;
		}
		/*
		 * This adds the nodes last speedEstimate Done at the last possible
		 * moment to guarantee a freshest possible value
		 */
		if (sendSpeedEstimates && parentNode.getLocationListener() != null) {

			if (lastSpeedUpdateTime < parentNode.getLocationListener().getLastTimestamp()) {
				dataMessageContainer = new DataMessageContainerLocalImpl(dataMessageContainer,
						parentNode.getHost().getId(), Attributes.NODESPEED.getAttributeId(),
						parentNode.getLocationListener().getLastSpeedEstimate());
				lastSpeedUpdateTime = parentNode.getLocationListener().getLastTimestamp();
			}
		}
		if (!localBuffer.getDuplicateInsensitiveAggregateData().isEmpty()) {
			AggregationDataBufferEntry firstEntry = localBuffer.getDuplicateInsensitiveAggregateData()
					.removeFirst();
			try {
				AggregationFunctionEntry toAddEntry = new AggregationFunctionEntry(parentNode.getHost().getId(),
						firstEntry.getValue());
				dataMessageContainer = new DataMessageContainerLocalImpl(dataMessageContainer,
						firstEntry.getAggregationFunctionId(), firstEntry.getAttributeId(), toAddEntry);
				hasAddedData = true;
			} catch (AggregationFunctionNotKnownException e) {
				// Was not able as aggregationfunctionId is unknown.
				e.printStackTrace();
			}
		}
		if (!localBuffer.getDuplicateSensitiveAggregateData().isEmpty()) {
			AggregationDataBufferEntry firstEntry = localBuffer.getDuplicateSensitiveAggregateData().removeFirst();
			try {
				AggregationFunctionEntry toAddEntry = new AggregationFunctionEntry(parentNode.getHost().getId(),
						firstEntry.getValue());
				dataMessageContainer = new DataMessageContainerLocalImpl(dataMessageContainer,
						firstEntry.getAggregationFunctionId(), firstEntry.getAttributeId(), toAddEntry, firstSend);
				hasAddedData = true;
			} catch (AggregationFunctionNotKnownException e) {
				// Was not able as aggregationfunctionId is unknown.
				e.printStackTrace();
			}
		}

		if (!localBuffer.getResponseObjects().isEmpty()) {
			this.fillContainerMessageWithResponses(dataMessageContainer);
			hasAddedData = true;
		}

		if (!hasAddedData) {
			System.out.println("Should not happen");
		}

		return dataMessageContainer;
	}

	@Override
	public void gotEmpty() {
		assert parentNode.isPresent();
		// assert activeComponent : "Transferable DataRouting should not be
		// directly set as TimeOutMapSizeListener";
		/*
		 * Maybe called when sink or leaf.
		 * 
		 * Leaf: should not be stopped, should now be stopped.
		 * 
		 * Sink: should be stopped.
		 */
		switch (parentNode.getCraterNodeRole()) {
		case LEAF:
			periodicSendDataOperation.stop();
			break;
		case SINK:
			assert periodicSendDataOperation.isStopped();
			break;
		case OFFLINE:
			throw new AssertionError();
		default:
			throw new AssertionError();
		}
		// if (!parentNode.isSink()) {
		// periodicSendDataOperation.stop();
		// }
		// assert periodicSendDataOperation.isStopped();
	}

	@Override
	public void gotFilled() {
		assert parentNode.isPresent();
		assert periodicSendDataOperation.isStopped();
		// assert activeComponent : "DataRouting should not be directly set as
		// TimeOutMapSizeListener";

		switch (parentNode.getCraterNodeRole()) {
		case LEAF:
			periodicSendDataOperation.start();
			break;
		case SINK:
			// nothing to do.
			break;
		case OFFLINE:
			throw new AssertionError();
		default:
			throw new AssertionError();
		}
		// if (parentNode.getCraterNodeRole() == CraterNodeRole.LEAF) {
		// periodicSendDataOperation.start();
		// }
	}

	@Override
	public void craterNodeRoleChanged(CraterNodeRole craterNodeRole) {
		switch (craterNodeRole) {
		case LEAF:
			periodicUploadOperation.stop();
			clusterSpeedComp.stop();
			/*
			 * Sink table is filled.
			 */
			if (!parentNode.sinkTableIsEmpty() && periodicSendDataOperation.isStopped()) {
				periodicSendDataOperation.start();
			}
			/*
			 * Sink table is empty when becoming leaf.
			 */
			else if (!parentNode.sinkTableIsEmpty()) {
				// Nothing to do.
			} else {
				periodicSendDataOperation.stop();
				// Should the sendingScheduler also be reset here?
			}
			break;
		case SINK:
			// needed as otherwise potential old data is uploaded (leading to
			// duplicates and more important large latencies) in statistics.
			uploadBuffer.clear();

			if (periodicUploadOperation.isStopped()) {
				periodicUploadOperation.start();
				clusterSpeedComp.start();
			}
			periodicSendDataOperation.stop();
			break;
		case OFFLINE:
			periodicUploadOperation.stop();
			periodicSendDataOperation.stop();
			sendingScheduler.reset();

			// do not hold onto old information
			localBuffer.clear();
			uploadBuffer.clear();
			clusterSpeedComp.stop();
			break;
		default:
			throw new AssertionError("Unknown case.");
		}
	}

	public CraterNodeComponent getParentNode() {
		return parentNode;
	}

	public SendHistory getHistory() {
		return history;
	}

	public static long getDATA_SENDING_PERIODIC_OPERATION_INTERVAL() {
		return DATA_SENDING_PERIODIC_OPERATION_INTERVAL;
	}

	public static long getDATA_UPLOAD_INTERVAL() {
		return DATA_UPLOAD_INTERVAL;
	}

	public static HistoryType getSendHistoryType() {
		return sendHistoryType;
	}

	public static long getSendHistoryValue() {
		return sendHistoryValue;
	}

	public static ContentionSchemeType getContentionType() {
		return contentionType;
	}

	public PeriodicSendDataOperation getPeriodicSendDataOperation() {
		return periodicSendDataOperation;
	}

	public PeriodicUploadOperation getPeriodicUploadOperation() {
		return periodicUploadOperation;
	}

	@Override
	public LocalBufferEntryLists getLocalBuffer() {
		return localBuffer;
	}

	public UploadBufferEntryLists getUploadBuffer() {
		return uploadBuffer;
	}

	public static void setSendSpeedEstimates(boolean sendIt) {
		sendSpeedEstimates = sendIt;
	}

	public static void setSinkMaxResponseForwardingDelay(long delay) {
		sinkMaxResponseForwardingDelay = delay;
	}

	@Override
	public long getTimeOfNextScheduledDataMessage() {

		switch (this.getParentNode().getCraterNodeRole()) {
		case LEAF:
			return Math.min(sendingScheduler.getNextTimeToSend(),
					getPeriodicSendDataOperation().getNextExecutionTime());
		case SINK:
			return Math.min(sendingScheduler.getNextTimeToSend(), getPeriodicUploadOperation().getNextExecutionTime());
		default:
			// Offline
			return Long.MAX_VALUE;
		}
	}

	/**
	 * Adds any Replies from the localBuffer to this message
	 * 
	 * @param msg
	 */
	protected void fillContainerMessageWithResponses(DataMessageContainerLocalImpl msg) {
		while (!localBuffer.getResponseObjects().isEmpty()) {
			msg.putResponse(localBuffer.getResponseObjects().poll());
		}
	}

	@Override
	public ResponseEventHandler getSendingScheduler() {
		return sendingScheduler;
	}

	public Long getLastSpeedUpdateTime() {
		return lastSpeedUpdateTime;
	}

	public ClusterSpeedComputation getClusterSpeedComp() {
		return clusterSpeedComp;
	}

	@Override
	public void stopMechanism(Callback cb) {
		// Do not reset static entries!

		parentNode = null;
		history = null;
		localBuffer = null;
		uploadBuffer = null;
		// Do not stop these!
		// They are transfered *as is* to the new DataRouting!
		periodicSendDataOperation = null;
		periodicUploadOperation = null;
		sendingScheduler = null;
		clusterSpeedComp = null;
	}

	/*
	 * Analyzing convenience methods.
	 */
	// FIXME or is it better to use transferstate annotations to mimize Monitor
	// calls? Alternative implement a stop/delete func in here
	private boolean checkedForDataRoutingAnalyzer = false;

	private DataRoutingAnalyzer dataRoutingAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasDataRoutingAnalyzer() {
		if (dataRoutingAnalyzer != null) {
			return true;
		} else if (checkedForDataRoutingAnalyzer) {
			return false;
		}
		getDataRoutingAnalyzer();
		return dataRoutingAnalyzer != null;
	}

	public DataRoutingAnalyzer getDataRoutingAnalyzer() {
		if (!checkedForDataRoutingAnalyzer) {
			try {
				dataRoutingAnalyzer = Monitor.get(DataRoutingAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				dataRoutingAnalyzer = null;
			}
			checkedForDataRoutingAnalyzer = true;
		}
		return dataRoutingAnalyzer;
	}

}
