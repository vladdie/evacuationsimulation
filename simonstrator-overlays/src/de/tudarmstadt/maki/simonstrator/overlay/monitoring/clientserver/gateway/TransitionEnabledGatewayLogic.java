package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.gateway;

import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.GatewaySelectionStrategy;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Bridge to the {@link TransitionEnabled} implementations of a {@link GatewaySelectionStrategy} (part of the SiS).
 * 
 * TODO make default strategy configurable.
 * 
 * TODO trigger transitions (only local!)
 * 
 * @author Nils Richerzhagen
 *
 */
public class TransitionEnabledGatewayLogic extends AbstractGatewayLogic {

	private GatewaySelectionStrategy gatewaySelection;

	private final static String COMP_NAME = "ClientServerGatewayLogic";

	// private GatewayLogicBehaviour gatewayLogicBehaviour;

	public TransitionEnabledGatewayLogic(ClientServerComponent comp, GatewaySelectionStrategy defaultStrategy) {
		super(comp);

		// this.gatewayLogicBehaviour = gatewayLogicBehaviour;

		try {
			defaultStrategy.setSiS(comp.getHost().getComponent(SiSComponent.class));
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("SiS is required.");
		}

		try {
			TransitionEngine tEngine = comp.getHost().getComponent(TransitionEngine.class);
			gatewaySelection = tEngine.createMechanismProxy(GatewaySelectionStrategy.class, defaultStrategy, COMP_NAME);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("TransitionEngine is required.");
		}
	}

	@Override
	public Map<INodeID, List<INodeID>> pickGateways(OverlayContacts potentialNodes, int maxNumberOfGateways) {
		Map<INodeID, List<INodeID>> gateways = gatewaySelection.getGateways(potentialNodes.getIdList(), null,
				maxNumberOfGateways);
		return gateways;
	}

}
