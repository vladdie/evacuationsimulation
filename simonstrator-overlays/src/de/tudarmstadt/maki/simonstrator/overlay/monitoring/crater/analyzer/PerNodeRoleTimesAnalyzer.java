/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer;

import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.NoSinkAdvertising;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.SinkAdvertising;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api.CraterSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api.CraterSubComponent.SubComponentState;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterNodeRoleListener;

/**
 * Analyzer for estimation of the times {@link CraterNodeComponent}s are in which {@link CraterNodeRole}. Beside that
 * estimation of the active times of the {@link SinkAdvertising} and the {@link NoSinkAdvertising} components.
 * 
 * @author Nils Richerzhagen
 * 
 */
public interface PerNodeRoleTimesAnalyzer extends Analyzer {

	/**
	 * Called by the {@link CraterNodeComponent} when the role changed. The {@link CraterNodeRoleListener} is not used
	 * as it does not return a {@link CraterNodeComponent} which is needed here for ID reasons.
	 * 
	 * @param comp
	 * @param craterNodeRole
	 */
	public void onCraterNodeRoleChanged(CraterNodeComponent comp, CraterNodeRole craterNodeRole);
	
	/**
	 * Called by the {@link CraterSubComponent} whenever the {@link SubComponentState} changes.
	 * 
	 * @param subComp
	 */
	public void onSubComponentStateChange(CraterSubComponent subComp, SubComponentState componentState);

}
