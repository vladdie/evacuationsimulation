package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.configuration;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterCellMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;

@Deprecated
public class GLConfigurationMessage extends AbstractOverlayMessage implements CraterCellMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3402054421437202608L;

	private boolean startDGS;
	private boolean startImmediately;
	private boolean stopDGS;
	private List<DGSSubComponents> newComponents;

	public GLConfigurationMessage(OverlayContact sender, OverlayContact receiver, boolean startDGS,
			boolean startImmediately, boolean stopDGS, List<DGSSubComponents> newComponents) {
		super(sender, receiver);
		this.startDGS = startDGS;
		this.startImmediately = startImmediately;
		this.stopDGS = stopDGS;
		this.newComponents = new LinkedList<DGSSubComponents>(newComponents);
	}

	public boolean isStartDGS() {
		return startDGS;
	}

	public boolean isStartImmediately() {
		return startImmediately;
	}

	public boolean isStopDGS() {
		return stopDGS;
	}

	public List<DGSSubComponents> getComponentList() {
		return newComponents;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public long getSize() {
		// super + 3* boolean(together 1 or so), + ComponentList (1 per entry?)
		return super.getSize() + newComponents.size() * DGSSubComponents.getTransmissionSize();
	}

}
