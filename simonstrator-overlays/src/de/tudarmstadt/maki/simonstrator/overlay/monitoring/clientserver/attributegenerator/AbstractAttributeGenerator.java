package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.attributegenerator;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;

public abstract class AbstractAttributeGenerator implements HostComponent{

	Host host;
	
	public AbstractAttributeGenerator(Host host) {
		this.host = host;
	}

	@Override
	public Host getHost() {
		return host;
	}
}
