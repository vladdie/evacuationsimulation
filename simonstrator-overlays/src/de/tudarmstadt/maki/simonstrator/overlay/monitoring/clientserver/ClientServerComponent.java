package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.api.IOverlayMessageAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.cloudlet.ClientServerCloudletComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerBackendMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerLocalMessage;

/**
 * Base class for ClientServer components (on central entity and mobile
 * entities).
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 06.08.2014
 * 
 */
public abstract class ClientServerComponent extends AbstractClientServerComponent implements TransMessageListener {

	/**
	 * Transport protocol for ad hoc communication between mobile devices and edge infrastructure devices.
	 */
	private MessageBasedTransport localTransport;

	/**
	 * Transport protocol for cellular communication.
	 */
	private MessageBasedTransport cellTransport;

	/**
	 * Transport protocol in-between backend entities, e.g. {@link ClientServerCloudletComponent}
	 * <-> {@link ClientServerCentralComponent}
	 */
	private MessageBasedTransport backendTransport;

	private BasicOverlayContact ownContact;

	/**
	 * Used for communication via the cellular network. E.g. from {@link ClientServerNodeComponent}s (acting as sinks) to the
	 * {@link ClientServerCentralComponent}.
	 */
	public static NetInterfaceName netTypeCell = NetInterfaceName.MOBILE;

	/**
	 * Used for communication via the mobile ad hoc network using (currently) the Wi-Fi interface. E.g. communication
	 * between {@link ClientServerNodeComponent}s.
	 */
	public static NetInterfaceName netTypeLocal = NetInterfaceName.WIFI;

	/**
	 * Used for backend communication. E.g. from {@link ClientServerCloudletComponent}s to the {@link ClientServerCentralComponent}.
	 */
	public static NetInterfaceName netTypeBackend = NetInterfaceName.ETHERNET;

	protected final int localPort = 32;

	private final int uploadPort = 64;

	private final ClientServerNodeType nodeType;

	/**
	 * Local reference to cloud contact {@link ClientServerCentralComponent}
	 */
	private OverlayContact cloudContact = null;

	private static IOverlayMessageAnalyzer _messageAnalyzer = null;

	private static boolean _messageAnalyzerInit = false;

	public ClientServerComponent(Host host, ClientServerNodeType nodeType) {
		super(host);
		this.nodeType = nodeType;
		// FIXME NodeType support in ClientServer, use node Type for some
		// insanity
		// checks!
	}

	/**
	 * There might be other types added later.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	public enum ClientServerNodeType {
		/**
		 * As implemented by {@link ClientServerNodeComponent}
		 */
		MOBILE_NODE,
		/**
		 * As implemented by {@link ClientServerCentralComponent}
		 */
		CLOUD,

		/**
		 * As implemented by {@link ClientServerCloudletComponent}
		 */
		CLOUDLET;
	}

	@Override
	public void initialize() {
		super.initialize();

		// Create OverlayContact
		ownContact = new BasicOverlayContact(getHost().getId());

		NetInterface cellular = getHost().getNetworkComponent().getByName(netTypeCell);
		NetInterface wifi = getHost().getNetworkComponent().getByName(netTypeLocal);

		assert cellular != null
				|| (cellular == null && nodeType == ClientServerNodeType.CLOUDLET) : "Mobile Nodes have to have a cellular NET";

		assert wifi != null || (wifi == null && this instanceof ClientServerCentralComponent) : "Central Server has to have no Wi-Fi.";

		try {
			if (cellular != null) {
				cellTransport = getAndBindUDP(cellular.getLocalInetAddress(), uploadPort, null);
				cellTransport.setTransportMessageListener(this);
				ownContact.addTransInfo(netTypeCell, cellTransport.getTransInfo());
			}
			if (wifi != null) {
				localTransport = getAndBindUDP(wifi.getLocalInetAddress(), localPort, null);
				localTransport.setTransportMessageListener(this);
				ownContact.addTransInfo(netTypeLocal, localTransport.getTransInfo());
			}
			// else {
			// assert cloudContact == null && ownUploadContact != null;
			// cloudContact = ownUploadContact;
			// }
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError("Protocol Binding failed!");
		}

		/*
		 * Initialize the backend transport between cloud and cloudlets (ETHERNET)
		 */
		NetInterface backend = getHost().getNetworkComponent().getByName(netTypeBackend);
		assert backend != null
				|| (backend == null && nodeType == ClientServerNodeType.MOBILE_NODE) : "Cloudlets have to have a backend NET";
		if (backend != null) {
			try {
				backendTransport = getAndBindUDP(backend.getLocalInetAddress(), uploadPort, null);
				backendTransport.setTransportMessageListener(this);
				// Add cloud transport to our own trans info
				ownContact.addTransInfo(netTypeBackend, backendTransport.getTransInfo());
			} catch (ProtocolNotAvailableException e) {
				throw new AssertionError("Cloudlets have to have a backend interface!");
			}
		}

	}

	@Override
	public OverlayContact getLocalOverlayContact() {
		return ownContact;
	}

	/**
	 * Send the given message to the cloud (or from the cloud to the clients), using the "Cellular" interface.
	 * 
	 * @param m
	 */
	public void sendViaCellular(OverlayMessage m) {
		assert m instanceof ClientServerCellMessage;
		// Only unicasts
		assert m.getReceiver() != null && m.getSender() != null;
		assert m.getReceiver().equals(getCloudContact()) || m.getSender().equals(getCloudContact());
		cellTransport.send(m, m.getReceiver().getNetID(netTypeCell), m.getReceiver().getPort(netTypeCell));
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onSentOverlayMessage(m, getHost(), netTypeCell);
		}
	}

	/**
	 * Send the given message via local ad hoc broadcast. Between {@link ClientServerNodeComponent}s.
	 * 
	 * @param m
	 */
	public void sendLocalBroadcast(OverlayMessage m) {
		assert m instanceof ClientServerLocalMessage;
		// FIXME Additional assert statements?
		localTransport.send(m, localTransport.getNetInterface().getBroadcastAddress(), localTransport.getLocalPort());
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onSentOverlayMessage(m, getHost(), netTypeLocal);
		}
	}

	/**
	 * Sends the given message to the {@link ClientServerCentralComponent} via backend communication.
	 * 
	 * @param m
	 */
	public void sendViaBackend(OverlayMessage m) {
		assert m instanceof ClientServerBackendMessage;
		// Only unicasts
		assert m.getReceiver() != null && m.getSender() != null;
		assert m.getReceiver().equals(getCloudContact()) || m.getSender().equals(getLocalOverlayContact());
		backendTransport.send(m, m.getReceiver().getNetID(netTypeBackend), m.getReceiver().getPort(netTypeBackend));
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onSentOverlayMessage(m, getHost(), netTypeBackend);
		}

	}

	/**
	 * Returns the servers's overlay contact.
	 * 
	 * @return
	 */
	public OverlayContact getCloudContact() {
		if (cloudContact == null) {
			if (Oracle.isSimulation()) {
				for (Host host : Oracle.getAllHosts()) {
					try {
						ClientServerCentralComponent cloud = host.getComponent(ClientServerCentralComponent.class);
						cloudContact = cloud.getLocalOverlayContact();
					} catch (ComponentNotAvailableException e) {
						// don't care
					}
				}
				if (cloudContact == null) {
					throw new AssertionError("Cloud not found.");
				}
			} else {
				throw new AssertionError("No Simulation.");
				/**
				 * Find Cloud via IP and Port
				 */
				// TransInfo cloudTransInfo = cloudTransport.getTransInfo(
				// cloudTransport.getNetInterface().getByName(serverIP),
				// cloudPort);
				// cloudContact = new BasicOverlayContact(INodeID.get(-1),
				// cloudTransport.getNetInterface().getName(),
				// cloudTransInfo);
			}
		}
		return cloudContact;
	}

	/**
	 * Returns the {@link ClientServerNodeType} of this node.
	 * 
	 * @return
	 */
	public ClientServerNodeType getNodeType() {
		return nodeType;
	}

	/*
	 * Analyzers
	 */

	public IOverlayMessageAnalyzer getMessageAnalyzer() {
		return _messageAnalyzer;
	}

	public boolean hasMessageAnalyzer() {
		if (!_messageAnalyzerInit) {
			try {
				_messageAnalyzer = Monitor.get(IOverlayMessageAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				//
			}
			_messageAnalyzerInit = true;
		}
		return _messageAnalyzer != null;
	}

}
