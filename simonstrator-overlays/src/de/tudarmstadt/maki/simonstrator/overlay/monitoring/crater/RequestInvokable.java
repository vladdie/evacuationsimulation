package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request;

// TODO: maybe move to API package

/**
 * interface for {@link Host}s in order to be able to invoke {@link RequestContainer}.
 * 
 * @author Simon
 * @version 1.0, 16.06.2016
 */
public interface RequestInvokable {

	/**
	 * resolves the given request in the overlay.
	 * 
	 * @param request
	 */
	public void invokeRequest(Request request);
	// Christoph Storm: Changed to Request, so this can be seamlessly used in
	// Overlay
}
