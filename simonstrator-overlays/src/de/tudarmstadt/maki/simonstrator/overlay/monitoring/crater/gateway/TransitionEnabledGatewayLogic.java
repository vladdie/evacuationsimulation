package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway;

import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.GatewaySelectionStrategy;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller.GatewaySelectionServerController;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Bridge to the {@link TransitionEnabled} implementations of a {@link GatewaySelectionStrategy} (part of the SiS).
 * 
 * TODO make default strategy configurable.
 * 
 * TODO trigger transitions (only local!)
 * 
 * @author Nils Richerzhagen
 *
 */
public class TransitionEnabledGatewayLogic extends AbstractGatewayLogic {

	private GatewaySelectionServerController gatewayController;
	// private GatewayLogicBehaviour gatewayLogicBehaviour;

	public TransitionEnabledGatewayLogic(CraterComponent comp, GatewaySelectionServerController gatewayController) {
		super(comp);

		this.gatewayController = gatewayController;

		Event.scheduleImmediately(new EventHandler() {
			@Override
			public void eventOccurred(Object content, int type) {
				gatewayController.init();
			}
		}, null, 0);
	}

	@Override
	public Map<INodeID, List<INodeID>> pickGateways(OverlayContacts potentialNodes, int maxNumberOfGateways) {
		Map<INodeID, List<INodeID>> gateways = gatewayController.getGateways(potentialNodes.getIdList(), null,
				maxNumberOfGateways);

		// The gatewayController takes care to inform the clients, so we return
		// an empty list.
		gateways.clear();
		// FIXME Change this, if the gateways are not to be informed by the
		// Gateway Server!
		return gateways;
	}

}
