package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.cloudlet;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.advertising.CloudletAdvertisingMessage;

/**
 * Cloudlet Advertising. Currently only periodic from the beginning on. Thus, similar to a normal AP that radiates the
 * whole time.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CloudletAdvertisingImpl implements CloudletAdvertising {

	private final ClientServerCloudletComponent parentComp;

	public static final int INITIAL_GRADIENT = 10;

	private int CURRENT_TTL;

	private long _CLOUDLET_ADV_INTERVAL;

	private PeriodicOperation<ClientServerCloudletComponent, Object> periodicCloudletAdvOperation;

	public CloudletAdvertisingImpl(ClientServerCloudletComponent parentComp, int INITIAL_TTL, long CLOUDLET_ADV_INTERVAL) {
		this.parentComp = parentComp;
		this.CURRENT_TTL = INITIAL_TTL;
		this._CLOUDLET_ADV_INTERVAL = CLOUDLET_ADV_INTERVAL;
	}

	@Override
	public void sendAdvBeacon() {
		/*
		 * Insanity checks
		 */
		// TODO Any?

		Monitor.log(CloudletAdvertisingImpl.class, Monitor.Level.INFO, "Sending CloudletAdvBeacon");
		CloudletAdvertisingMessage cloudletAdvMsg = new CloudletAdvertisingMessage(parentComp.getLocalOverlayContact(),
				parentComp.getHost().getId(), CURRENT_TTL);
		parentComp.sendLocalBroadcast(cloudletAdvMsg);

	}


	@Override
	public void initialize() {
		/*
		 * Start periodic operations
		 */
		periodicCloudletAdvOperation = new PeriodicOperation<ClientServerCloudletComponent, Object>(parentComp,
				Operations.getEmptyCallback(), _CLOUDLET_ADV_INTERVAL) {
			
			@Override
			public Object getResult() {
				return null;
			}
			
			@Override
			protected void executeOnce() {
				sendAdvBeacon();
			}
		};
		periodicCloudletAdvOperation.start();
	}

}
