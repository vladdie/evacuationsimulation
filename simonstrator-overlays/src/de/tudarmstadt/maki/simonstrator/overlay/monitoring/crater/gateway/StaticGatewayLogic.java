package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Selects at the beginning g Gateways (maxNumberOfGateways) of the nodes randomly. Those nodes are then used as
 * gateways for the whole simulation time!
 * 
 * @author Nils Richerzhagen
 *
 */
public class StaticGatewayLogic extends AbstractGatewayLogic {

	private Map<INodeID, List<INodeID>> gateways = new HashMap<INodeID, List<INodeID>>();

	public StaticGatewayLogic(CraterComponent comp) {
		super(comp);
	}

	@Override
	public Map<INodeID, List<INodeID>> pickGateways(OverlayContacts potentialNodes, int maxNumberOfGateways) {

		/**
		 * In first method invocation pick gateways and stay with the decision during runtime
		 */
		if (gateways.size() < maxNumberOfGateways) {

			for (INodeID potentialNode : potentialNodes.getIdList()) {
				/*
				 * Fill with new nodes as long as still the max number is not achieved.
				 */
				if (!gateways.containsKey(potentialNode)) {
					gateways.put(potentialNode, null);
				}
				if (gateways.size() >= maxNumberOfGateways) {
					break;
				}
			}
		} else {
			List<INodeID> potentialNodesIdList = potentialNodes.getIdList();
			Map<INodeID, List<INodeID>> currentPossibleGateways = new HashMap<INodeID, List<INodeID>>();

			for (INodeID gateway : gateways.keySet()) {
				if (potentialNodesIdList.contains(gateway)) {
					currentPossibleGateways.put(gateway, null);
				}
			}

			return currentPossibleGateways;
		}
		return gateways;
	}
}
