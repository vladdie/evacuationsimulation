package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.ResponisbleComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.sinksendinginterval.SinkAdvIntervalModifier;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api.CraterSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.NoKnownSinksException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.NoStrongerSinksFound;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.CloudletAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.NoSinkMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.SinkAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference.AbstractSinkPreference;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference.DefaultPreferenceGradient;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference.DefaultPreferenceQuality;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference.SinkPreference.SinkPreferenceType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference.SinkTableEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference.StrictStaticPreferenceGradient;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference.StrictStaticPreferenceQuality;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.NotADistribution;
import de.tudarmstadt.maki.simonstrator.service.transition.local.AtomicTransitionExecution.ComponentState;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.ITimeoutMapListener;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutMap;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * SinkAdvertising mechanism of CRATER.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 05.06.2014
 * 
 */
public class SinkAdvertisingImpl extends CraterSubComponent implements SinkAdvertising, EventHandler {
	/*
	 * Changeable Parameter
	 */
	private static long SINK_ADV_PERIODIC_OPERATION_INTERVAL;

	public static int INITIAL_TTL;

	/*
	 * Fixed Parameter
	 */
	/**
	 * Used for seeing if there is a better sink available in a sub range.
	 */
	private static final int SINK_MGMT_MIN_NUMBER_OF_SUCCESSIVE_UPDATES = 2;
	/**
	 * Used to obtain the best sink table entry.
	 */
	private static final int BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES = 2;

	private long _MIN_SINK_ADV_HES_TIME = 0 * Time.SECOND;
	private long _MAX_SINK_ADV_HES_TIME = 10 * Time.SECOND;

	// private final CraterNodeComponent parentNode;

	// AtomicBoolean set with initial false value
	private final AtomicBoolean _initializeMethodCall = new AtomicBoolean(true);

	/*
	 * Entry LIFETIME should be operation interval + hesitation time (SET in constructor)
	 */
	public static long SINK_TABLE_MAINTENANCE_MAX_ENTRY_LIFETIME;

	/*
	 * Random value that x percent are forwarded (rest is discarded) !! by this node !!
	 */
	public static final double FORWARDING_PROCENTUAL_VALUE = 0.7;

	/*
	 * Parameters for go back to idle if nothing received for timeout time.
	 */
	private static long _RETURN_TO_IDLE_THRESHOLD = 3 * Time.MINUTE;

	private boolean _heardNoSinkMsgDuringGoBackToIdle = false;
	private boolean _heardDataMessageDuringGoBackToIdle = false;

	// use static sink preference
	private SinkPreferenceType sinkPreference;
	private AbstractSinkPreference sinkPreferenceImpl;

	public static final int INITIAL_GRADIENT = 10;
	public static int CURRENT_TTL;
	public static final int MAX_TTL = 10;

	private final int SINK_ADV_EVENT_ID = 22;

	private PeriodicOperation<CraterNodeComponent, Object> periodicSinkAdvOperation;
	private PeriodicOperation<CraterNodeComponent, Object> periodicGoBackToIdleOperation;

	private TimeoutMap<INodeID, SinkTableEntry> sinkTable;
	private LinkedHashMap<INodeID, SinkTableEntry> receivedSinkAdvertisingMsgs;
	private TimeoutSet<UniqueID> sendSinkAdvMessages_duplicateCheck;

	private SinkAdvIntervalModifier intervalDeterminator;

	// Needed to 'hack'/ change the interval of sinkAdvMsgs...
	private NotADistribution advInterval;

	private double lastSpeedUpdate = -1;

	private boolean speedChanged = false;

	// Value for Request Response Robustness...
	// The currently known active requestHashes will be inserted into every n-th
	// sinkAdv msg
	// FIXME setter or set manually!
	private static int requestHashesInEachNThAdvMsg = 5;
	private int requestHashesCountDown = -1;

	public SinkAdvertisingImpl(CraterNodeComponent parentNode, int INITIAL_TTL,
			long SINK_ADV_PERIODIC_OPERATION_INTERVAL, SinkPreferenceType stype,
			SinkAdvIntervalModifier intervalDeterminator) {
		super(parentNode);
		SinkAdvertisingImpl.INITIAL_TTL = INITIAL_TTL;
		CURRENT_TTL = INITIAL_TTL;
		SinkAdvertisingImpl.SINK_ADV_PERIODIC_OPERATION_INTERVAL = SINK_ADV_PERIODIC_OPERATION_INTERVAL;
		// SINK_TABLE_MAINTENANCE_MAX_ENTRY_LIFETIME =
		// SINK_ADV_PERIODIC_OPERATION_INTERVAL + 2 * _MAX_SINK_ADV_HES_TIME;
		SINK_TABLE_MAINTENANCE_MAX_ENTRY_LIFETIME = intervalDeterminator.getMaxInterval() + 2 * _MAX_SINK_ADV_HES_TIME;
		sinkPreference = stype;
		this.intervalDeterminator = intervalDeterminator;
		sinkTable = new TimeoutMap<INodeID, SinkTableEntry>(SINK_TABLE_MAINTENANCE_MAX_ENTRY_LIFETIME);
		receivedSinkAdvertisingMsgs = new LinkedHashMap<INodeID, SinkTableEntry>();
		sendSinkAdvMessages_duplicateCheck = new TimeoutSet<UniqueID>(SINK_ADV_PERIODIC_OPERATION_INTERVAL * 3);
		requestHashesCountDown = requestHashesInEachNThAdvMsg;
		Monitor.log(SinkAdvertising.class, Monitor.Level.INFO, "SinkAdvertising Initialized");
	}

	public void initialize() {
		inializePeriodicOperations();
		initializeSinkPreference();

	}

	/**
	 * Method to initialize the Sink preference to be used
	 * 
	 * @author Jonas Huelsmann
	 */
	private void initializeSinkPreference() {
		switch (sinkPreference) {
		case DEFAULT:
			sinkPreferenceImpl = new DefaultPreferenceGradient(
					BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES,
					sinkTable.getUnmodifiableMap());
			break;
		case QUALITY:
			sinkPreferenceImpl = new DefaultPreferenceQuality(
					BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES,
					sinkTable.getUnmodifiableMap());
			break;
		case STATIC_GRADIENT:
			sinkPreferenceImpl = new StrictStaticPreferenceGradient(
					BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES,
					sinkTable.getUnmodifiableMap());
			break;
		case STATIC_QUALITY:
			sinkPreferenceImpl = new StrictStaticPreferenceQuality(
					BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES,
					sinkTable.getUnmodifiableMap());
			break;
		default:
			sinkPreferenceImpl = new DefaultPreferenceGradient(
					BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES,
					sinkTable.getUnmodifiableMap());
			break;

		}
	}
	/**
	 * Initialization of the periodic operations for the different internal parts of the {@link SinkAdvertising}
	 * mechanism.
	 */
	private void inializePeriodicOperations() {

		// This is a hack, not a distribution...
		advInterval = new NotADistribution(SINK_ADV_PERIODIC_OPERATION_INTERVAL);

		periodicSinkAdvOperation = new PeriodicOperation<CraterNodeComponent, Object>(getParentComponent(),
				Operations.getEmptyCallback(),
				advInterval, Time.MICROSECOND) {

			@Override
			public Object getResult() {
				return null;
			}

			@Override
			protected void executeOnce() {
				if (isInactive()) {
					return;
				}
				/*
				 * Use EVENT for tH time! of backoff timers
				 */
				receivedSinkAdvertisingMsgs.clear();
				scheduleHesitationTime();
				updateSinkAdvTiming();
			}

		};

		periodicGoBackToIdleOperation = new PeriodicOperation<CraterNodeComponent, Object>(getParentComponent(),
				Operations.getEmptyCallback(), _RETURN_TO_IDLE_THRESHOLD) {

			@Override
			public Object getResult() {
				return null;
			}

			@Override
			protected void executeOnce() {
				if (isInactive()) {
					return;
				}
				/*
				 * Transition mechansims for Hybrid to Centralized. Going back to IDLE state if sink heard no data or
				 * noSink messages for pre-defined interval.
				 */
				if (!_heardDataMessageDuringGoBackToIdle && !_heardNoSinkMsgDuringGoBackToIdle && getParentComponent().isSink()) {
					if (!isIdle()) {
						setComponentState(SubComponentState.IDLE);
					}
					// getParentComponent().setCraterNodeState(CraterNodeState.IDLE);
				}
				_heardDataMessageDuringGoBackToIdle = false;
				_heardNoSinkMsgDuringGoBackToIdle = false;
			}
		};

	}

	/**
	 * Starting the automata.
	 * 
	 * Called when the node went online or changed from {@link CraterNodeRole} LEAF to SINK. Thus,
	 * {@link CraterNodeComponent} should not be LEAF --> must be SINK.
	 * 
	 * State here must be INACTIVE.
	 */
	private void startComponent() {
		// Insanity
		assert getParentComponent().isSink();
		assert getParentComponent().isPresent();
		assert getComponentState() == SubComponentState.INACTIVE;

		setComponentState(SubComponentState.IDLE);
	}

	/**
	 * Used to reset the automata.
	 * 
	 * Called when host went offline or when {@link CraterNodeComponent} became node. May be node before going offline,
	 * thus component is already in {@link ComponentState} INACTIVE.
	 */
	private void resetComponent() {
		if (!getParentComponent().isPresent() || !getParentComponent().isSink()) {
			if (getComponentState() == SubComponentState.INACTIVE) {
				return;
			}
			setComponentState(SubComponentState.INACTIVE);
		} else {
			assert !getParentComponent().isSink() : "Should only be called when the parent node is NO sink";
		}
	}

	@Override
	public void handleIncomingSinkAdvMsg(SinkAdvertisingMessage msg) {
		/*
		 * Duplicate aware check. If duplicate ignore completely and just do nothing -> return;
		 */
		if (sendSinkAdvMessages_duplicateCheck.contains(msg.getUniqueMsgID())) {
			return;
		}

		////////////////////
		// Update: 12.08.16
		// Handle updating the msg's know request hashes for robustness.
		//////////////////// here on..
		if (msg.isIncludesRequestHashes()) {
			getParentComponent().getRequestResolver().handleSinkMessageRequestHashes(msg.getRequestHashes());

			msg.getRequestHashes().addAll(getParentComponent().getRequestResolver().getAllVallidRequestHashes());
		}

		/*
		 * ############### Being no Sink! ###############
		 */
		if (!getParentComponent().isSink()) {
			Monitor.log(NoSinkAdvertising.class, Monitor.Level.INFO,
 "SinkAdvertising: " + msg.getUniqueMsgID().toString()
					+ " on Node " + getParentComponent().getHost().getId().value());
			/*
			 * Forward:
			 * 
			 * (only if not already send that sink adv message in past time)
			 * 
			 * - if msg's TTL field allows forwarding -> deadMeassage
			 * 
			 * -- if sink table is empty, thus only known sink!
			 * 
			 * -- when this is the only known sink, thus best sinkId is equal to current msg sinkId & sinkTable size = 1
			 * & TTL field allows this, of course
			 * 
			 * -- only if own best sink has a lower gradient than the new received gradient and TTL is still valid.
			 * CAUTION: On using copy-constructor in CraterNode receivedMessage method the TTL and gradient already
			 * decreased!
			 */
			if (!msg.deadMessage()) {
				try {
					if (sinkTableIsEmpty()) {
						forwardAdvBeacon(msg);
					} else if (sinkTable.size() == 1 && getBestSinkTableEntry().getKey() == msg.getSinkId()) {
						forwardAdvBeacon(msg);
					} else if (sinkTable.get(getBestSinkTableEntry().getKey()).getCurrentGradient() < msg.getGradient()) {
						forwardAdvBeacon(msg);
					}
				} catch (NoKnownSinksException e) {
					e.printStackTrace();
				}
			}
		}

		/*
		 * Update sink table. If initial sinkId is already known, just update with fresh values, and don't forget to
		 * increase number of updates accordingly.
		 */
		SinkTableEntry newEntry;
		// When already contained update number of updates field, gradient, and
		// timestamp!
		if (sinkTable.containsKey(msg.getSinkId())) {
			newEntry = new SinkTableEntry(sinkTable.get(msg.getSinkId()), msg.getGradient(), msg.getSinkQuality());
		}
		// new heard sink, thus just build this new entry.
		else {
			if (msg instanceof CloudletAdvertisingMessage) {
				newEntry = new SinkTableEntry(msg.getSinkId(),
						msg.getGradient(), msg.getSinkQuality(), true);
			} else {
				newEntry = new SinkTableEntry(msg.getSinkId(),
						msg.getGradient(), msg.getSinkQuality(), false);
			}
		}
		sinkTable.putNow(msg.getSinkId(), newEntry);
		receivedSinkAdvertisingMsgs.put(msg.getSinkId(), newEntry);

		/*
		 * ############ Being Sink! ############
		 * 
		 * Do not make any forwarding or other stuff when being a sink! CAUTION! This is behind sink table update as it
		 * needs no comparison with the state before receiving a sinkadv msg. Instead, needs an updated table to
		 * perform.
		 */
		if (getParentComponent().isSink()) {
			/*
			 * ###### Sink Merging / Push Away ########
			 * 
			 * TODO determine values for x successive beacons (robustness against fluctuation) and pre-defined adjacency
			 * field.
			 * 
			 * - x successive beacons
			 * 
			 * - adjacent sink, initial_gradient - gradient <= pre-defined value
			 * 
			 * - sink_entry.sink_quality_field > than this.sink_quality_field
			 * 
			 * do
			 * 
			 * - if(not IDLE) do this.node_state = IDLE;
			 */
			int adjacency_field = 1;
			// LinkedList<SinkTableEntry> foundSinkTableEntries;
			try {
				// foundSinkTableEntries =
				// getSinkTableEntriesWithBetterSinkQuality(adjacency_field);
				getSinkTableEntriesWithBetterSinkQuality(adjacency_field);
			} catch (NoKnownSinksException e) {
				return;
			} catch (NoStrongerSinksFound e) {
				return;
			}

			/*
			 * Heard sink with better quality. May be an idle sink, thus check for idle.
			 */
			if (!isIdle()) {
				setComponentState(SubComponentState.IDLE);
			}

			return;
		}

	}

	@Override
	public void hasReceivedDataMessageAsSink() {
		_heardDataMessageDuringGoBackToIdle = true;
	}

	/**
	 * Sink-Merging / Push-Away
	 * 
	 * @param maxRange
	 *            the initial_gradient - gradient of the entries >= maxRange
	 * @return {@link List} of {@link SinkTableEntry}'s
	 * @throws NoKnownSinksException
	 * @throws NoStrongerSinksFound
	 */
	private LinkedList<SinkTableEntry> getSinkTableEntriesWithBetterSinkQuality(int maxRange)
			throws NoKnownSinksException, NoStrongerSinksFound {
		/*
		 * If this happens then callee should be aware of handling the exception!
		 */
		if (sinkTableIsEmpty()) {
			throw new NoKnownSinksException();
		}

		int currentSinkQuality = getSinkQuality();

		LinkedList<SinkTableEntry> foundSinkTableEntries = new LinkedList<SinkTableEntry>();

		if (sinkTable.size() >= 1) {
			for (SinkTableEntry actEntry : sinkTable.getUnmodifiableMap().values()) {
				if (actEntry.getNumberOfUpdates() >= SINK_MGMT_MIN_NUMBER_OF_SUCCESSIVE_UPDATES) {
					if (actEntry.getSinkQuality() >= currentSinkQuality) {
						double sub_range = ((double) INITIAL_GRADIENT) - actEntry.getCurrentGradient();
						if (sub_range <= maxRange)
							foundSinkTableEntries.add(actEntry);
					}
				}
			}
		}
		if (foundSinkTableEntries.size() == 0)
			throw new NoStrongerSinksFound();
		return foundSinkTableEntries;
	}

	/**
	 * Changes the TTL accordingly to the input. If nextTTL value is larger than MAX_TTL it is set to MAX_TTL. If
	 * nextTTL is smaller one it is set to one.
	 * 
	 * @param nextTTL
	 */
	private void cellDivision(int nextTTL) {
		// FIXME Use
		if (nextTTL > MAX_TTL)
			CURRENT_TTL = MAX_TTL;
		else if (nextTTL < 1)
			CURRENT_TTL = 1;
		else
			CURRENT_TTL = nextTTL;
	}

	@Override
	public void forwardAdvBeacon(SinkAdvertisingMessage msg) {
		// TODO how to determine procentual value
		// 1 - percentual value that is desired to be forwarded
		if (getParentComponent().getRandom().nextDouble() >= (1 - FORWARDING_PROCENTUAL_VALUE)) {
			// Now ads the correct senderID
			getParentComponent()
					.sendLocalBroadcast(new SinkAdvertisingMessage(getParentComponent().getLocalOverlayContact(), msg));
			// getParentComponent().sendSinkAdvertisingMessage(msg);
			sendSinkAdvMessages_duplicateCheck.addNow(msg.getUniqueMsgID());
		}
	}

	@Override
	public void addSinkTableListener(ITimeoutMapListener l) {
		sinkTable.addListener(l);
	}

	/**
	 * Method just to get out of scope from periodic operation to call 'this' and to get scope to event handler.
	 */
	public void scheduleHesitationTime() {
		Event.scheduleWithDelay(getHesitationTime(), this, null, SINK_ADV_EVENT_ID);
	}

	@Override
	public void handleIncomingNoSinkMsg(NoSinkMessage msg) {
		assert getParentComponent().isPresent() == true : "Node should be present";

		/*
		 * Called from outside. Thus, when inactive just ignore.
		 */
		if (isInactive()) {
			return;
		}

		/*
		 * Insanity
		 * 
		 * Should be sink at this point, as in all other ways the component must be inactive.
		 */
		assert getParentComponent().isSink();

		/*
		 * According to state diagram. When IDLE do change to ACTIVE mode as sink is triggered.
		 */
		if (isIdle()) {
			setComponentState(SubComponentState.ACTIVE);
		}
		_heardNoSinkMsgDuringGoBackToIdle = true;
	}

	@Override
	public int getSinkQuality() {
		// TODO Sink quality based on what?
		return (int) getParentComponent().getCurrentBatteryPercentage();
	}

	@Override
	public void sendAdvBeacon() {
		/*
		 * Insanity checks
		 */
		assert getParentComponent().isPresent() == true : "Node should be present";
		assert !isInactive() : "Should not try to send data when INACTIVE";
		assert getParentComponent().isSink() : "Should be sink, when sendingAdvBeacons.";
		/*
		 * ########## HESITATION TIME expired ##########
		 * 
		 * No sink --> No sending of adv beacons
		 */
		if (!getParentComponent().isSink())
			return;

		switch (getComponentState()) {
		case IDLE:
			/*
			 * Node is sink and State = IDLE - not triggered to ACTIVE by incoming 'noSink' beacon, thus do nothing
			 */
			return;
		case ACTIVE:
			/*
			 * Node is sink and State = ACTIVE - send out adv beacon like presented in concept
			 */

			if (!receivedSinkAdvertisingMsgs.isEmpty()) {
				/*
				 * At least one other sink is better suited, thus send its beacon earlier during own hesitation time. -
				 * So do not send own beacon - Return back to IDLE state!
				 * 
				 * + other sink should be in direct neighborhood, or adjacency to this node. Adapt via
				 * BETTER_SINK_MAX_RANGE field.
				 * 
				 * TODO Check how connectivity is affected by this, as it reduces the going messages in the network
				 * significant compared to just a normal return;!
				 * 
				 * TODO determine better sink range
				 */
				int BETTER_SINK_MAX_RANGE = 1;
				boolean heardBetterAdjacentSink = false;
				for (SinkTableEntry actReceivedEntry : receivedSinkAdvertisingMsgs.values()) {
					// double actCurGrad =
					// actReceivedEntry.getCurrentGradient();
					if (actReceivedEntry.getCurrentGradient() >= INITIAL_GRADIENT - BETTER_SINK_MAX_RANGE) {
						heardBetterAdjacentSink = true;
					}
				}
				if (heardBetterAdjacentSink) {
					setComponentState(SubComponentState.IDLE);
				}
				return;
			}
			/*
			 * Has not received any other sink beacons during hesitation time.
			 */

			// Hysteresis for broadcast / Unicast decision. Toggle our own
			// component directly.
			boolean sendUnicast = false;
			if (getParentComponent().getComputeSpeedAt().equals(ResponisbleComponent.SINKADV) && lastSpeedUpdate >= 0) {
				sendUnicast = !getParentComponent().isBroadcastRouting();

				double lowerThreshold = getParentComponent().getSlowSpeed() - getParentComponent().getSlowSpeedOffset();
				double upperThreshold = getParentComponent().getSlowSpeed() + getParentComponent().getSlowSpeedOffset();

				if (lastSpeedUpdate < lowerThreshold && !sendUnicast) {
					sendUnicast = true;
					getParentComponent().toggleDataRouting();
				} else if (lastSpeedUpdate > upperThreshold && sendUnicast) {
					sendUnicast = false;
					getParentComponent().toggleDataRouting();
				}
			}

			boolean sendRequestHashes = false;
			Set<Integer> requestHashes = new HashSet<Integer>();
			if (requestHashesCountDown - 1 == 0) {
				sendRequestHashes = true;
				requestHashesCountDown = requestHashesInEachNThAdvMsg;

				// get requestHashes!
				requestHashes = getParentComponent().getRequestResolver().getAllVallidRequestHashes();

			} else if (requestHashesInEachNThAdvMsg > 0) {
				requestHashesCountDown--;
			}

			Monitor.log(NoSinkAdvertising.class, Monitor.Level.INFO, "SinkAdv: Sending advBeacon");
			SinkAdvertisingMessage sinkAdvMsg = new SinkAdvertisingMessage(getParentComponent().getLocalOverlayContact(),
					getParentComponent().getHost().getId(), INITIAL_GRADIENT, CURRENT_TTL, getSinkQuality(),
					sendUnicast, sendRequestHashes, requestHashes);
			// getParentComponent().sendSinkAdvertisingMessage(sinkAdvMsg);
			getParentComponent().sendLocalBroadcast(sinkAdvMsg);
			sendSinkAdvMessages_duplicateCheck.addNow(sinkAdvMsg.getUniqueMsgID());

			break;
		case INACTIVE:
			throw new AssertionError("Component should not be inactive at this stage.");
		default:
			throw new EnumConstantNotPresentException(Enum.class, getComponentState().toString());
		}
	}

	@Override
	public boolean sinkTableIsEmpty() {
		return sinkTable.size() == 0;
	}

	@Override
	public long getHesitationTime() {
		// TODO other Attributes than Battery?
		double weightBattery = 0.4;
		double weightRandomness = 0.6;
		int randomValue = 0 + (int) (getParentComponent().getRandom().nextDouble() * ((100 - 0) + 1));
		double hesFactor = (weightBattery * (100 - getParentComponent().getCurrentBatteryPercentage())
				+ weightRandomness * randomValue)
				/ 100;
		long hesitationTime = _MIN_SINK_ADV_HES_TIME + (long) (hesFactor * (_MAX_SINK_ADV_HES_TIME - _MIN_SINK_ADV_HES_TIME));
		return hesitationTime;
	}

	@Override
	public void eventOccurred(Object content, int type) {
		/*
		 * Hesitation time expired. But only trigger sending method when component is active.
		 */
		if (type == SINK_ADV_EVENT_ID && isActive()) {
			sendAdvBeacon();
		}
	}

	/**
	 * Return the sinkId of the {@link SinkTableEntry} that has the largest gradient, while at the same time has a
	 * minimum of X successive updates? If there is not such an entry do with {@link SinkTableEntry} until (with less
	 * successive updates) one is found.
	 * 
	 * @return best possible sinkId or {@link NoKnownSinksException} in case of empty sink table!
	 * @throws NoKnownSinksException
	 */
	@Override
	public AbstractMap.SimpleEntry<INodeID, Double> getBestSinkTableEntry() throws NoKnownSinksException {
		/*
		 * If this happens then callee should be aware of handling the exception!
		 */
		if (sinkTableIsEmpty()) {
			throw new NoKnownSinksException();
		}
		// set the actual sink table
		sinkPreferenceImpl.setNewSinkTable(sinkTable.getUnmodifiableMap());
		// get the best sink
		AbstractMap.SimpleEntry<INodeID, Double> temp = sinkPreferenceImpl
				.getBestSink();

		if (temp == null) {
			throw new NoKnownSinksException();
		} else {
			return temp;
		}




	}

	// SinkTableEntry was moved to the package sinkpreference

	/**
	 * Manually updating a SinkEntry that was already present This also
	 * increases the numOfUpdates used in SinkPreference!
	 * 
	 * @param sinkID
	 * @param gradient
	 */
	public void updateSinkEntry(INodeID sinkID, double gradient) {
		// sanity check
		// can only update something that was in here already
		if (sinkTable.containsKey(sinkID)) {
			SinkTableEntry sinkEntry = sinkTable.get(sinkID);
			sinkEntry = new SinkTableEntry(sinkEntry, gradient, sinkEntry.getSinkQuality());
			sinkTable.putNow(sinkID, sinkEntry);
		} // Update 01.07.2016
			// This should be used by the DecentralGatewaySelection repectively
			// the LogicController
			// Only mobile nodes are taken into account?!
//		else {
//			SinkTableEntry sinkEntry = new SinkTableEntry(sinkID, (int) gradient, 100, true);
//			sinkTable.putNow(sinkID, sinkEntry);
//		}
	}

	@Override
	protected void setComponentState(SubComponentState state) {
		// TODO right actions on active inactive?
		assert this.getComponentState() != state;

		if (!_initializeMethodCall.getAndSet(false)) {
			Monitor.log(SinkAdvertising.class, Monitor.Level.DEBUG, "Node " + getParentComponent().getHost().getId().value()
					+ " ComponentState from " + getComponentState().toString() + " to " + state.toString());
		}

		super.setComponentState(state);
		// this.currentComponentState.setComponentState(state);

		// System.out.println(getParentComponent().getHost().getId().value() + " SinkState " +
		// currentComponentState.toString());

		switch (state) {
		case ACTIVE:
			assert getParentComponent().isSink();

			if (periodicSinkAdvOperation.isStopped()) {
				advInterval.setInterval(SINK_ADV_PERIODIC_OPERATION_INTERVAL);
				periodicSinkAdvOperation.start();
				requestHashesCountDown = requestHashesInEachNThAdvMsg;
			}

			lastSpeedUpdate = -1;

			break;

		case INACTIVE:
			/*
			 * Should if not already happened stop the periodic sink adv.
			 */
			periodicSinkAdvOperation.stop();
			periodicGoBackToIdleOperation.stop();
			break;
		case IDLE:
			assert getParentComponent().isSink();

			if (periodicGoBackToIdleOperation.isStopped()) {
				periodicGoBackToIdleOperation.start();
			}
			periodicSinkAdvOperation.stop();
			break;
		default:
			break;
		}
	}

	@Override
	public void craterNodeRoleChanged(CraterNodeRole craterNodeRole) {
		switch (craterNodeRole) {
		case LEAF:
			resetComponent();
			break;
		case SINK:
			startComponent();
			break;
		case OFFLINE:
			resetComponent();
			break;
		default:
			throw new AssertionError("Unknown case.");
		}
	}

	public void setLastSpeedUpdate(double speedUpdate) {
		this.lastSpeedUpdate = speedUpdate;
		speedChanged = true;
	}


	/**
	 * Called from inside executeOnce of the periodicSinkAdvOperation (i.e., the
	 * scope of periodicOperartion), to alter the nextInterval before the
	 * PeriodicOperation has read it.
	 */
	public void updateSinkAdvTiming() {
		// get SpeedUpdates
		if (lastSpeedUpdate < 0 || !speedChanged) {
			return;
		}
		else {
			assert lastSpeedUpdate < 8;
			// Value between maxVal and minVal, decreasing for
			// speedrange between 0m/s and 8m/s
			// long nextInterval = SINK_ADV_PERIODIC_OPERATION_INTERVAL;
			long nextInterval = intervalDeterminator.getNextInterval(lastSpeedUpdate);

			advInterval.setInterval(nextInterval);
			speedChanged = false;

			// System.out.println("Sink" +
			// this.getParentComponent().getHost().getHostId() + " set
			// sinkADVInterval to "
			// + Time.getFormattedTime(nextInterval));
		}
	}

	public static void setRequestHashesInEachNThAdvMsg(int n) {
		requestHashesInEachNThAdvMsg = n;
	}
}