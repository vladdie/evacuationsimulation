package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater;

import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.PerNodeHasNoSinkTimesAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.PerNodeRoleTimesAnalyzer;

/**
 * Abstract Base-Class for a crater component. Provides basic implementations of node status.
 *
 * @author Nils Richerzhagen
 * @version 1.0, 05.06.2014
 */
public abstract class AbstractCraterComponent extends AbstractOverlayNode
{

    private Random random;

    /**
     * @param host
     * @param nodeID
     */
    public AbstractCraterComponent(Host host)
    {
        super(host);
        this.random = Randoms.getRandom(this);
    }

    @Override
    public void wentOnline(Host host, NetInterface netInterface)
    {
        setPeerStatus(PeerStatus.PRESENT);
        // System.out.println(host.getId().value() + " went online");
    }

    @Override
    public void wentOffline(Host host, NetInterface netInterface)
    {
        setPeerStatus(PeerStatus.ABSENT);
        // System.out.println(host.getId().value() + " went offline");
    }

    public Random getRandom()
    {
        return random;
    }

	/*
     * Analyzing convenience methods.
	 */

    private boolean checkedForPerNodeStateTimeAnalyzer = false;

    private PerNodeRoleTimesAnalyzer perNodeStateTimeAnalyzer = null;

    /**
     * True, if a {@link PerNodeRoleTimesAnalyzer} is provided.
     *
     * @return
     */
    public boolean hasPerNodeStateTimeAnalyzer()
    {
        if (perNodeStateTimeAnalyzer == null && !checkedForPerNodeStateTimeAnalyzer)
            getPerNodeStateTimeAnalyzer();

        return perNodeStateTimeAnalyzer != null;
    }

    public PerNodeRoleTimesAnalyzer getPerNodeStateTimeAnalyzer()
    {
        if (!checkedForPerNodeStateTimeAnalyzer)
        {
            try
            {
                perNodeStateTimeAnalyzer = Monitor.get(PerNodeRoleTimesAnalyzer.class);
            } catch (AnalyzerNotAvailableException e)
            {
                perNodeStateTimeAnalyzer = null;
            }
            checkedForPerNodeStateTimeAnalyzer = true;
        }
        return perNodeStateTimeAnalyzer;
    }

    private boolean checkedForPerNodeHasNoSinkTimeAnalyzer = false;

    private PerNodeHasNoSinkTimesAnalyzer perNodeHasNoSinkTimeAnalyzer = null;

    /**
     * True, if a {@link PerNodeHasNoSinkTimesAnalyzer} is provided.
     *
     * @return
     */
    public boolean hasPerNodeHasNoSinkAnalyzer()
    {
        if (perNodeHasNoSinkTimeAnalyzer == null && !checkedForPerNodeHasNoSinkTimeAnalyzer)
            getPerNodeHasNoSinkTimeAnalyzer();

        return perNodeHasNoSinkTimeAnalyzer != null;
    }

    public PerNodeHasNoSinkTimesAnalyzer getPerNodeHasNoSinkTimeAnalyzer()
    {
        if (!checkedForPerNodeHasNoSinkTimeAnalyzer)
        {
            try
            {
                perNodeHasNoSinkTimeAnalyzer = Monitor.get(PerNodeHasNoSinkTimesAnalyzer.class);
            } catch (AnalyzerNotAvailableException e)
            {
                perNodeHasNoSinkTimeAnalyzer = null;
            }
            checkedForPerNodeHasNoSinkTimeAnalyzer = true;
        }
        return perNodeHasNoSinkTimeAnalyzer;
    }

}
