package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse;

import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;

/**
 * This message is used to request (get) request based on their hash. Receivers
 * should rebroadcast this requested {@link Request}, if they know it.
 * 
 * @author Christoph Storm
 *
 */
public class RequestInquiryMessage extends AbstractOverlayMessage implements CraterLocalMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2582283387741100838L;

	private Set<Integer> requestInquiries;

	public RequestInquiryMessage(OverlayContact sender, Set<Integer> requests) {
		super(sender, null);
		this.requestInquiries = requests;
	}

	public Set<Integer> getRequestInquiries() {
		return requestInquiries;
	}

	@Override
	public long getSize() {
		return super.getSize() + requestInquiries.size() * 4;
	}


	@Override
	public Message getPayload() {
		return this;
	}

}