#About the incorporation of the `gatewayselection` package:

To use the `gatewaySelection` and the respective controllers:

* CraterGatewayConfiguration:
	* `setStrategy` to TRANSITION
	* `setGatewaySelectionLogic` to a `GatewaySelectionMechanismLogic` instance. (Currently that is only the `CSVreaderLogic`)
	
* CraterParameterConfiguration:
	* `setUseGatewaySelectionClient` to true, to let CraterNodes instantiate the ClientController.
	
## Remarks:
The `TransitionEnabledGatewayLogic` does no longer return a map of Gateways and Nodes. So the method `calculateAndInformGateways` in `AbstractGatewayLogic` does not generate a Collection of `CloudConfigurationMessage`s.
Instead, the `GatewaySelectionServerController` takes care to inform nodes of their role.

The reason is that otherwise it could come to badly timed and contradictory orders, when employing the `DecentralizedGatewaySelection`.