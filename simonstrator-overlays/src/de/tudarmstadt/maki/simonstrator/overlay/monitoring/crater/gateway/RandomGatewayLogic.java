package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * For testing: randomly picks g Gateways (maxNumberOfGateways) of the nodes of
 * the list of potential nodes and uses these nodes as a gateway.
 * 
 * @author Nils Richerzhagen
 *
 */
public class RandomGatewayLogic extends AbstractGatewayLogic {

	private Random random;

	private final CraterComponent comp;

	public RandomGatewayLogic(CraterComponent comp) {
		super(comp);
		this.comp = comp;
		random = Randoms.getRandom(this);
	}

	@Override
	public Map<INodeID, List<INodeID>> pickGateways(OverlayContacts potentialNodes, int maxNumberOfGateways) {
		Map<INodeID, List<INodeID>> gateways = new HashMap<INodeID, List<INodeID>>();

		/*
		 * More Gateways possible than nodes!
		 */
		if (maxNumberOfGateways >= potentialNodes.size()) {
			for (INodeID potentialNode : potentialNodes.getIdList()) {
				gateways.put(potentialNode, null);
			}
			return gateways;
		}

		for (int i = 0; i < maxNumberOfGateways; i++) {
			INodeID randomNodeId = potentialNodes.getIdList().get(random.nextInt(potentialNodes.size()));
			while (randomNodeId.equals(comp.getLocalOverlayContact().getNodeID()) || gateways.containsKey(randomNodeId)) {
				randomNodeId = potentialNodes.getIdList().get(random.nextInt(potentialNodes.size()));
			}
			gateways.put(randomNodeId, null);
		}
		// // 10% of nodes is to few --> choose at least one node as GW.
		// if (potentialNodes.size() / 10 <= 0) {
		// INodeID randomNodeId =
		// potentialNodes.getIdList().get(random.nextInt(potentialNodes.size()));
		// while (randomNodeId.equals(comp.getLocalOverlayContact().getNodeID())
		// || gateways.containsKey(randomNodeId)) {
		// randomNodeId =
		// potentialNodes.getIdList().get(random.nextInt(potentialNodes.size()));
		// }
		// gateways.put(randomNodeId, null);
		// }
		return gateways;
	}
}
