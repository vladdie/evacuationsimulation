package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing;

import java.util.LinkedList;
import java.util.Map.Entry;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageCallback;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.ContentionScheme.ContentionSchemeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.NoKnownSinksException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history.SendHistory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history.SendHistory.HistoryType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.SinkAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.UnicastReply;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers.LocalBufferEntryLists;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers.UploadBufferEntryLists;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.operations.PeriodicSendDataOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.operations.PeriodicUploadOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.ResponseEventHandler;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * Copy of DataRoutingImp, altered to try and unicast DataMessages towards a
 * nexthop.
 * 
 * @author Christoph Storm
 */
public class DataRoutingUnicastImpl extends AbstractDataRouting implements EventHandler {

	@TransferState(value = { "ReceivedDataMessagesContainer" })
	private TimeoutSet<UniqueID> receivedDataMessagesContainer;

	@TransferState(value = { "AlreadySendOutBufferDataViaForwardedPacket" })
	private boolean alreadySendOutBufferDataViaForwardedPacket = false;

	private static long REPLY_TIMEOUT = 4 * Time.SECOND;

	// used for sanity checks
	private boolean activeComponent = false;

	/*
	 * Simple triple with a single *confirmed* next hop per sink is *way* easier
	 * than a construct that maps several nextHops (ordered after their gradient
	 * and if the link is symmetric, time of last contact, etc) to each sink...
	 * (And it only took me one day to figure this out, D'OH!)
	 */
	// Stores SinkID, NextHop OverlayContact, nextHopGradient
	private Triple nextHopForSink = new Triple();

	// used to for certain checks
	private INodeID lastSinkTriedToReach = null;

	// copy incase that the last unicast got lost, should not get larger than 2
	private LinkedList<DataMessageContainerLocalImpl> lastSendDataMsgs = new LinkedList<DataMessageContainerLocalImpl>();

	/**
	 * 
	 * @param getParentNode()
	 */
	public DataRoutingUnicastImpl(CraterNodeComponent parentNode, long DATA_SENDING_PERIODIC_OPERATION_INTERVAL,
			long DATA_UPLOAD_INTERVAL, HistoryType sendHistoryType, long sendHistoryValue,
			ContentionSchemeType contentionType) {
		super(parentNode, DATA_SENDING_PERIODIC_OPERATION_INTERVAL, DATA_UPLOAD_INTERVAL, sendHistoryType,
				sendHistoryValue, contentionType);
		receivedDataMessagesContainer = new TimeoutSet<UniqueID>(RECEIVED_DATA_MESSAGES_TIMEOUT);
		// activeComponent = true;
	}

	/**
	 * TransferState annoted Constructor to be used use by the TransitionEngine
	 * 
	 * @param parentNode
	 * @param history
	 * @param receivedDataMessagesContainer
	 * @param alreadySendOutBufferDataViaForwardedPacket
	 * @param localBuffer
	 * @param uploadBuffer
	 * @param periodicSendDataOperation
	 * @param periodicUploadOperation
	 * @param ResponseEventHandler
	 *            sendingScheduler
	 * @param Long
	 *            lastSpeedUpdateTime
	 * @param ClusterSpeedComputation
	 *            clusterSpeedComp
	 */
	@TransferState(value = { "ParentNode", "History", "ReceivedDataMessagesContainer",
			"AlreadySendOutBufferDataViaForwardedPacket",
 "LocalBuffer",
			"UploadBuffer", "PeriodicSendDataOperation",
			"PeriodicUploadOperation", "SendingScheduler", "LastSpeedUpdateTime", "ClusterSpeedComp" })
	public DataRoutingUnicastImpl(CraterNodeComponent parentNode, SendHistory history,
			TimeoutSet<UniqueID> receivedDataMessagesContainer,
			Boolean alreadySendOutBufferDataViaForwardedPacket, LocalBufferEntryLists localBuffer,
			UploadBufferEntryLists uploadBuffer,
			PeriodicSendDataOperation periodicSendDataOperation,
			PeriodicUploadOperation periodicUploadOperation,
			ResponseEventHandler sendingScheduler, Long lastSpeedUpdateTime,
			ClusterSpeedComputation clusterSpeedComp) {
		super(parentNode, history, localBuffer, uploadBuffer,
				periodicSendDataOperation, periodicUploadOperation,
				sendingScheduler, lastSpeedUpdateTime, clusterSpeedComp);
		this.receivedDataMessagesContainer = receivedDataMessagesContainer;
		this.alreadySendOutBufferDataViaForwardedPacket = alreadySendOutBufferDataViaForwardedPacket.booleanValue();
	}

	@Override
	public void initialize() {
		initializePeriodicOperations();
	}


	@Override
	public void handleIncomingMessage(CraterLocalMessage cMsg) {
		assert activeComponent : "Stopped DataRouting Components should not handle messages!";
		if (cMsg instanceof DataMessageContainerLocalImpl) {
			DataMessageContainerLocalImpl msg = (DataMessageContainerLocalImpl) cMsg;
			/*
			 * 
			 * - #3# [sink == true]
			 * 
			 * - #3# sinkId of msg == sinkId of this node
			 * 
			 * -- #3.1# [sink == true] put to packet contents to upload buffers
			 * 
			 * - #4# [sink == false]
			 * 
			 * - #4# own best sinkId == msg.sinkId
			 * 
			 * -- #4.1# [sink == false] forward (under conditions); exchange
			 * gradient with own best
			 */
			switch (getParentNode().getCraterNodeRole()) {
			case SINK:

				// #3#
				if (msg.getSinkId() == getParentNode().getHost().getId()) {

					onReceivedDataContainerAsSink(msg);
					/*
					 * Analyzing
					 * 
					 * - #A.1# Received as sink multiple times a
					 * CraterDataContainerMessage which was intended for that
					 * sink.
					 * 
					 * - msg id contained, isSink == true, sinkId =
					 * msg.CraterNodeId
					 */
					if (hasDataRoutingAnalyzer()) {
						// #A.1#
						if (receivedDataMessagesContainer.contains(msg.getUniqueMsgID())) {
							getDataRoutingAnalyzer().onSuccessiveContainerMessageReceivedBySink(msg.getUniqueMsgID());
						} else if (!receivedDataMessagesContainer.contains(msg.getUniqueMsgID())) {
							getDataRoutingAnalyzer().onContainerMessageReceivedBySink(msg.getUniqueMsgID(),
									msg.getSinkId());
						}
					}
					receivedDataMessagesContainer.addNow(msg.getUniqueMsgID());
					return;
				}
				break;
			case LEAF:
				// #4#
				if (receivedDataMessagesContainer.contains(msg.getUniqueMsgID()))
					return;

				receivedDataMessagesContainer.addNow(msg.getUniqueMsgID());

				Entry<INodeID, Double> bestSinkTableEntry;
				try {
					bestSinkTableEntry = getParentNode().getBestSinkTableEntry();
					// #4# unicast, just forward this, regardless of previous
					// gradient!
					if (msg.getSinkId() == bestSinkTableEntry.getKey()) {
						// #4.1#
						OverlayContact receiver = null;
						// check if we do have a nextHop
						if (bestSinkTableEntry.getKey().equals(nextHopForSink.sinkID)) {
							receiver = nextHopForSink.nextHop;
						}
						DataMessageContainerLocalImpl exchangedGradientMsg = new DataMessageContainerLocalImpl(
								getParentNode().getLocalOverlayContact(), receiver, msg,
								bestSinkTableEntry.getValue());
						Event.scheduleImmediately(getParentNode().getDataRouting(), exchangedGradientMsg,
								DATA_CONTAINER_MESSAGE_FORWARDING_EVENT);
						return;
					}
				} catch (NoKnownSinksException e) {
					/*
					 * I have no own sinks.
					 */
					return;
				}
				break;
			default:
				break;
			}
		} else if (cMsg instanceof UnicastReply) {
			UnicastReply ack = (UnicastReply) cMsg;
			/*
			 * This should only be called when we did a broadcast, elsewise the
			 * callback is triggered. This means we might not have a current
			 * nextHop, but we might receive several replies.
			 */

			// TODO there is a slight chance of receiving a sinkAdvMsg in
			// between datasending and answer, changing the affiliated Sink.
			// Disregard for now.

			if (ack.getGradient() > nextHopForSink.gradient) {
				// we did not receive a better answer yet
				nextHopForSink.sinkID = lastSinkTriedToReach;
				nextHopForSink.nextHop = ack.getSender();
				nextHopForSink.gradient = ack.getGradient();
				// also update SinkTableEntry!!!
				// ...but only if this was for the same sink
				getParentNode().getSinkAdvertising().updateSinkEntry(
						lastSinkTriedToReach, ack.getGradient());
				return;
			}
		} else if (cMsg instanceof SinkAdvertisingMessage) {
			SinkAdvertisingMessage sAdv = (SinkAdvertisingMessage) cMsg;
			/*
			 * New SinkAdvertising #1 check if this is our (new) Sink
			 * 
			 * #2 if so, check if this is our current nextHop or better -if so,
			 * update the nextHopEntries
			 */

			// SinkAdvertising should have updated the table right before this
			// one was called
			Entry<INodeID, Double> bestSink;
			try {
				bestSink = getParentNode().getBestSinkTableEntry();
			} catch (NoKnownSinksException e) {
				// Got a SinkAdv but doesn't know any sink? Should not happen!
				return;
			}
			if (!bestSink.getKey().equals(sAdv.getSinkId())) {
				// #1: not for us... -disregard
				return;
			} else {
				if (bestSink.getValue() >= nextHopForSink.gradient || sAdv.getSender().equals(nextHopForSink.nextHop)) {
					// update nextHop only if it is better than what we
					// currently got, or if it was our currentNext hop.
					nextHopForSink.sinkID = sAdv.getSinkId();
					nextHopForSink.nextHop = sAdv.getSender();
					nextHopForSink.gradient = sAdv.getGradient();
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void sendBufferContent() {
		assert activeComponent : "Stopped DataRouting Component should not send Buffers!";
		switch (getParentNode().getCraterNodeRole()) {
		case LEAF:
			/*
			 * Send out new data if conditions
			 * 
			 * - #1# has not already put some of own data to going-through message
			 * 
			 * - #2# at least one buffer has data inside to send out. We do not want to send empty message!
			 */
			// #1#
			if (alreadySendOutBufferDataViaForwardedPacket) {
				// Was originally reset by nested Operation after execution of
				// this method
				alreadySendOutBufferDataViaForwardedPacket = false;
				return;
			}
			alreadySendOutBufferDataViaForwardedPacket = false;
			fillBuffersWithAttributeGeneratorData();
			// #2#
			if (getLocalBuffer().getNormalData().isEmpty()
					|| getLocalBuffer().getDuplicateInsensitiveAggregateData().isEmpty()
					|| getLocalBuffer().getDuplicateSensitiveAggregateData().isEmpty())
				return;

			try {
				Entry<INodeID, Double> bestSinkEntry = getParentNode().getBestSinkTableEntry();
				OverlayContact receiver = null;
				if (bestSinkEntry.getKey().equals(nextHopForSink.sinkID)) {
					receiver = nextHopForSink.nextHop;
				}

				DataMessageContainerLocalImpl dataMessageContainer = new DataMessageContainerLocalImpl(
						getParentNode().getLocalOverlayContact(), receiver,
						bestSinkEntry.getKey(), bestSinkEntry.getValue());

				dataMessageContainer = fillWithBufferEntries(dataMessageContainer, true);

				if (receiver != null) {
					getParentNode().sendLocalUnicast(dataMessageContainer, new unicastCallback(), REPLY_TIMEOUT);
					lastSendDataMsgs.offer(dataMessageContainer);
				} else {
					getParentNode().sendLocalBroadcast(dataMessageContainer);
				}
				lastSinkTriedToReach = bestSinkEntry.getKey();

				if (hasDataRoutingAnalyzer()) {
					getDataRoutingAnalyzer().onContainerMessageSend(dataMessageContainer.getUniqueMsgID(),
							dataMessageContainer.getSinkId());
				}

			} catch (NoKnownSinksException e) {
				// Currently has no Sink, thus can not send data!
				return;
			}
			break;

		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void forwardDataMessage(DataMessageContainerLocalImpl msg) {
		/*
		 * At this point msg is dupFree only when received via unicast,
		 * Exchanged gradient, maybe a receiver.
		 * 
		 * CAUTION - This method is exclusively responsible for adding own
		 * buffer content to such a msg.
		 * 
		 * CAUTION! Do not make payload/packet larger than max. MTU.
		 * 
		 * FIXME what brings that? (-- own scheduled msg gradient (best gradient
		 * at the time when msg was intended to be send) for that sinkId is
		 * greater than received gradient)
		 */
		// #1#
		/*
		 * Forward data
		 * 
		 * #1# without additional buffer if no data in buffers.
		 * 
		 * #2# with additonal data if any buffer has data inside.
		 */
		fillBuffersWithAttributeGeneratorData();
		boolean sendUnicast = false;
		if (msg.getReceiver() != null) {
			sendUnicast = true;
		}
		// #1#
		if (getLocalBuffer().getNormalData().isEmpty()
				|| getLocalBuffer().getDuplicateInsensitiveAggregateData().isEmpty()
				|| getLocalBuffer().getDuplicateSensitiveAggregateData().isEmpty()) {

			// still add replies
			fillContainerMessageWithResponses(msg);

			if (sendUnicast) {
				getParentNode().sendLocalUnicast(msg, new unicastCallback(), REPLY_TIMEOUT);
				lastSendDataMsgs.offer(msg);
			} else {
				getParentNode().sendLocalBroadcast(msg);
			}
			return;
		}

		// #2#
		msg = fillWithBufferEntries(msg, false);

		alreadySendOutBufferDataViaForwardedPacket = true;
		// getParentNode().sendDataMessage(msg);
		if (sendUnicast) {
			getParentNode().sendLocalUnicast(msg, new unicastCallback(), REPLY_TIMEOUT);
			lastSendDataMsgs.offer(msg);
		} else {
			getParentNode().sendLocalBroadcast(msg);
		}
		lastSinkTriedToReach = msg.getSinkId();
	}

	@Override
	public void eventOccurred(Object content, int type) {
		assert activeComponent : "Events should not directly reference stopped DataRouting Components";
		if (getParentNode().getCraterNodeRole() == CraterNodeRole.OFFLINE || !getParentNode().isPresent()) {
			assert getParentNode().getCraterNodeRole() == CraterNodeRole.OFFLINE;
			assert !getParentNode().isPresent();
			return;
		}

		if (type == DATA_CONTAINER_MESSAGE_FORWARDING_EVENT && content instanceof DataMessageContainerLocalImpl) {
			DataMessageContainerLocalImpl msg = (DataMessageContainerLocalImpl) content;
			forwardDataMessage(msg);
		}

		if (type == DATA_CONTAINER_MESSAGE_SENDING_EVENT && content instanceof DataMessageContainerLocalImpl) {
			DataMessageContainerLocalImpl msg = (DataMessageContainerLocalImpl) content;
			// getParentNode().sendDataMessage(msg);
			if (msg.getReceiver() != null) {
				getParentNode().sendLocalUnicast(msg, new unicastCallback(), REPLY_TIMEOUT);
				lastSendDataMsgs.offer(msg);
			} else {
				getParentNode().sendLocalBroadcast(msg);
			}
			if (hasDataRoutingAnalyzer()) {
				getDataRoutingAnalyzer().onContainerMessageSend(msg.getUniqueMsgID(), msg.getSinkId());
			}
		}
	}

	public TimeoutSet<UniqueID> getReceivedDataMessagesContainer() {
		return receivedDataMessagesContainer;
	}

	// called *get*Already instead of *is*Already because the TransitionEngine
	// searches for *get*-Methods
	public boolean getAlreadySendOutBufferDataViaForwardedPacket() {
		return alreadySendOutBufferDataViaForwardedPacket;
	}

	@Override
	public void startMechanism(Callback cb) {
		activeComponent = true;

		// Note: Initialize() is called only after first startMechanism!
		if (this.getPeriodicSendDataOperation() == null) {
			// first call
			initializePeriodicOperations();
		}

		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {

		super.stopMechanism(new Callback() {
			@Override
			public void finished(boolean successful) {
				// do nothing
			}
		});

		activeComponent = false;

		// own class fields
		receivedDataMessagesContainer = null;
		lastSinkTriedToReach = null;
		nextHopForSink = null;
		lastSendDataMsgs.clear();
		lastSendDataMsgs = null;

		cb.finished(true);
	}

	private void receivedReply(UnicastReply ack) {
		if (!activeComponent) {
			return;
		}

		// This is only called when we received a reply to a unicast
		if (lastSinkTriedToReach != null) {
			// should always be the case, considering we sent something...

			if (lastSinkTriedToReach.equals(nextHopForSink.sinkID)) {
				// only update if the message we sent was for the current
				// bestSink
				nextHopForSink.gradient = ack.getGradient();
				// also update SinkTableEntry!!!
				// ...but only if this was for the same sink
				getParentNode().getSinkAdvertising().updateSinkEntry(nextHopForSink.sinkID, ack.getGradient());
			}
			// remove from list.
			lastSendDataMsgs.poll();
		}
	}

	/**
	 * Called when we did not hear an ack for a DataMessage- unicast. Strategy
	 * is failfast, fallback to broadcast.
	 */
	private void timedOut() {
		if (!activeComponent) {
			return;
		}
		// no longer nextHop
		nextHopForSink.sinkID = null;
		nextHopForSink.nextHop = null;
		nextHopForSink.gradient = 0L;

		// Fallback to standard broadcast behaviour
		DataMessageContainerLocalImpl failed = lastSendDataMsgs.poll();
		if (failed != null) {
			// Shouldn't happen to be null, but anyway
			failed = new DataMessageContainerLocalImpl(failed.getSender(),
					null, failed, failed.getGradient());
			getParentNode().sendLocalBroadcast(failed);

			// System.out.println("Node" + getParentNode().getHost().getHostId()
			// + " timed out!");
		}
	}

	/**
	 * Sets the timeout for unicast- DataMessages.
	 * 
	 * @param timeout
	 */
	public static void setREPLY_TIMEOUT(long timeout) {
		REPLY_TIMEOUT = timeout;
	}

	private class Triple {
		// FIXME make this nicer...
		INodeID sinkID;
		OverlayContact nextHop;
		double gradient;
	}

	private class unicastCallback implements TransMessageCallback {

		@Override
		public void receive(Message reply, TransInfo source, int commId) {
			if (reply instanceof UnicastReply) {
				UnicastReply ack = (UnicastReply) reply;
				receivedReply(ack);
			}
		}

		@Override
		public void messageTimeoutOccured(int commId) {
			timedOut();
		}

	}

}
