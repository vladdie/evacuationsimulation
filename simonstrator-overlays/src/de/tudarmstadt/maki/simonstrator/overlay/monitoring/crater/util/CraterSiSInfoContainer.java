package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;

public class CraterSiSInfoContainer {

	// Maping from Node to Map of SiSType and Values
	private HashMap<SiSType<?>, HashMap<INodeID, Object>> infoContainer = new HashMap<SiSType<?>, HashMap<INodeID, Object>>();
	// TODO way for Data to become outdated?!
	// ALternatively do per Type+NodeID Timestamps somehow

	private Host host;

	public CraterSiSInfoContainer(Host host) {
		this.host = host;
	}

	public void put(SiSType<?> type, INodeID node, Object value) {

		if (node.equals(host.getId())) {
			throw new AssertionError("Monitoring should not provide data of own host.");
		}
		HashMap<INodeID, Object> values;
		if (infoContainer.containsKey(type)) {
			values = infoContainer.get(type);
		} else {
			values = new HashMap<INodeID, Object>();
			infoContainer.put(type, values);
		}
		values.put(node, value);
	}

	public Set<INodeID> getAllObservedNodesForType(SiSType<?> type) {
		HashSet<INodeID> resultSet = new HashSet<INodeID>();
		if (infoContainer.containsKey(type)) {
			resultSet.addAll(infoContainer.get(type).keySet());
		}
		return resultSet;
	}

	public boolean containsType(SiSType<?> type) {
		return infoContainer.keySet().contains(type);
	}

	public <T> T get(SiSType<T> type, INodeID node) throws InformationNotAvailableException {

		try {
			T result = type.getType().cast(infoContainer.get(type).get(node));
			return result;
		} catch (Exception e) {
			// NullpointerException or ClassCastException, both meaning we don't
			// have the correct value
			throw new InformationNotAvailableException();
		}
	}

	public void clear() {
		infoContainer.clear();
	}

}
