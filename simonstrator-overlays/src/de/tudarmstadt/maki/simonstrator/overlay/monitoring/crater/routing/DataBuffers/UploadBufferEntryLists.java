package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers;

import java.util.LinkedHashMap;
import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupInsensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupSensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.ResponseObject;

/**
 * 'Buffer' for lists of uploadData buffers... Eases use of transitionEngine for
 * DataRouting -instances
 * 
 * @author Christoph Storm
 *
 */
public class UploadBufferEntryLists {

	private LinkedHashMap<UniqueID, DataObjectImpl<Double>> normalDataObjects = new LinkedHashMap<UniqueID, DataObjectImpl<Double>>();
	private LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveData = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();
	private LinkedList<DataObjectAggregationDupSensitiveImpl> aggregationDupSensitiveData = new LinkedList<DataObjectAggregationDupSensitiveImpl>();

	/**
	 * Now this also stores responseObjects that should be sent
	 */
	private LinkedList<ResponseObject> responseObjects = new LinkedList<ResponseObject>();

	public LinkedHashMap<UniqueID, DataObjectImpl<Double>> getNormalDataObjects() {
		return normalDataObjects;
	}

	public LinkedList<DataObjectAggregationDupInsensitiveImpl> getAggregationDupInsensitiveData() {
		return aggregationDupInsensitiveData;
	}

	public LinkedList<DataObjectAggregationDupSensitiveImpl> getAggregationDupSensitiveData() {
		return aggregationDupSensitiveData;
	}

	public LinkedList<ResponseObject> getResponseObjects() {
		return responseObjects;
	}

	public void clear() {
		normalDataObjects.clear();
		aggregationDupInsensitiveData.clear();
		aggregationDupSensitiveData.clear();
		responseObjects.clear();
	}

}
