package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.sinksendinginterval;

/**
 * Dummy/ static interval
 * 
 * @author Christoph Storm
 *
 */
public class StaticInterval implements SinkAdvIntervalModifier {

	private long interval;

	public StaticInterval(long interval) {
		this.interval = interval;
	}

	@Override
	public long getNextInterval(double speedOfNodes) {
		return interval;
	}

	@Override
	public long getMaxInterval() {
		return interval;
	}

}
