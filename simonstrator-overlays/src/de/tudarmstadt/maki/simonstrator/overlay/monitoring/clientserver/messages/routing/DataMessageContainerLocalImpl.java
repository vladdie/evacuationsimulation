package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerFactory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.DuplicateInsensitiveAggregationFunction.DuplicateInsensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.DuplicateSensitiveAggregationFunction.DuplicateSensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.exceptions.AggregationFunctionNotKnownException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.MessageWithSinkId;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.MessageWithUniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.routing.DataRoutingImpl;

/**
 * {@link DataMessageContainer} implementation for local (ad-hoc) communication
 * in the network between nodes. It includes the normal normal data in
 * {@link DataObject}, duplicate insensitive aggregates in
 * {@link DataObjectAggregationDupInsensitiveImpl}, and duplice sensitve
 * aggregates in {@link DataObjectAggregationDupSensitiveImpl}.
 * 
 * Furthermore the basic fields: {@link UniqueID}, sinkId (long), and the
 * gradient (double).
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 * 
 */
public class DataMessageContainerLocalImpl extends AbstractOverlayMessage implements ClientServerLocalMessage, MessageWithSinkId,
		MessageWithUniqueID, DataMessageContainer {

	private static final long serialVersionUID = 1L;

	/**
	 * In byte!
	 */
	private static final int MAX_MESSAGE_PAYLOAD = 1800;

	private UniqueID uniqueMsgId;

	private INodeID clientServerSinkId;

	private double currentGradient;

	private LinkedList<DataObjectImpl<Double>> normalData = new LinkedList<DataObjectImpl<Double>>();
	private LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveData = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();
	private LinkedList<DataObjectAggregationDupSensitiveImpl> aggregationDupSensitiveData = new LinkedList<DataObjectAggregationDupSensitiveImpl>();

	/**
	 * Copy-constructor for messageArrived at {@link ClientServerNodeComponent}.
	 * 
	 * @param msg
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg) {
		super(msg.getSender(), null, msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.clientServerSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();
	}

	/**
	 * Initial constructor for {@link DataMessageContainer} in sending out in
	 * {@link DataRoutingImpl}.
	 * 
	 * CAUTION! Only use if afterwards content is added, otherwise an empty msg
	 * is send out!
	 * 
	 * @param sender
	 * @param clientServerNodeId
	 * @param currentGradient
	 * @param attributeId
	 * @param value
	 */
	public DataMessageContainerLocalImpl(OverlayContact sender, INodeID clientServerSinkId, double currentGradient) {
		super(sender, null);
		this.uniqueMsgId = ClientServerFactory.getRandomMessageId();
		this.clientServerSinkId = clientServerSinkId;
		this.currentGradient = currentGradient;
	}

	/**
	 * Copy-constructor + additional {@link DataObject} for data sending in
	 * {@link DataRoutingImpl}.
	 * 
	 * @param msg
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg, INodeID actNodeId, int attributeId, double value) {
		super(msg.getSender(), null, msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.clientServerSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();

		DataObjectImpl<Double> normalDataObject = new DataObjectImpl<Double>(attributeId, value, actNodeId);
		normalData.add(normalDataObject);

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onNormalDataObjectAddedInNetwork(normalDataObject.getUniqueDataObjectID());
		}
	}

	/**
	 * For first send analyzer purposes.
	 * 
	 * @param msg
	 * @param actNodeId
	 * @param attributeId
	 * @param value
	 * @param firstSend
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg, INodeID actNodeId, int attributeId, double value,
			boolean firstSend) {
		super(msg.getSender(), null, msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.clientServerSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();

		DataObjectImpl<Double> normalDataObject = new DataObjectImpl<Double>(attributeId, value, actNodeId);
		normalData.add(normalDataObject);

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onNormalDataObjectAddedInNetwork(normalDataObject.getUniqueDataObjectID());
			getDataRoutingAnalyzer().onNormalDataObjectAddedFirstTime(normalDataObject.getUniqueDataObjectID());
		}
	}

	/**
	 * Copy-constructor + additional
	 * {@link DataObjectAggregationDupInsensitiveImpl} or
	 * {@link DataObjectAggregationDupSensitiveImpl} according to the given
	 * aggregationFunctionId for data sending in {@link DataRoutingImpl}.
	 * 
	 * @param msg
	 *            - the original message.
	 * @param aggregationFunctionId
	 *            - the identifier of the aggregation function.
	 * @param attributeId
	 * @param toAddEntry
	 *            - in case of {@link DataObjectAggregationDupInsensitiveImpl}
	 *            adding full <key> clientServerNodeId, <value> value entry. In
	 *            case of {@link DataObjectAggregationDupSensitiveImpl} adding
	 *            only the <value> of the entry as key is irrelevant.
	 * @throws AggregationFunctionNotKnownException
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg, String aggregationFunctionId, int attributeId,
			AggregationFunctionEntry toAddEntry, boolean firstTime) throws AggregationFunctionNotKnownException {
		super(msg.getSender(), null, msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.clientServerSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();

		if (DuplicateSensitiveAggregationFunctionsDescriptions.MEAN.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT_NOREF.getId().equals(aggregationFunctionId)) {

			putDupSensitiveData(aggregationFunctionId, attributeId, toAddEntry);
			// Analyzing method.
			if (hasDataRoutingAnalyzer()) {
				getDataRoutingAnalyzer().onAggregationDupSensitiveEntryAddedFirstTime(toAddEntry);
			}
		} else if (DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.getId().equals(aggregationFunctionId)
				|| DuplicateInsensitiveAggregationFunctionsDescriptions.MIN.getId().equals(aggregationFunctionId)) {

			putDupInsensitiveData(aggregationFunctionId, attributeId, toAddEntry.getValue());
		} else {
			throw new AggregationFunctionNotKnownException(aggregationFunctionId);
		}
	}

	/**
	 * Copy-constructor + additional
	 * {@link DataObjectAggregationDupInsensitiveImpl} or
	 * {@link DataObjectAggregationDupSensitiveImpl} according to the given
	 * aggregationFunctionId for data sending in {@link DataRoutingImpl}.
	 * 
	 * @param msg
	 *            - the original message.
	 * @param aggregationFunctionId
	 *            - the identifier of the aggregation function.
	 * @param attributeId
	 * @param toAddEntry
	 *            - in case of {@link DataObjectAggregationDupInsensitiveImpl}
	 *            adding full <key> clientServerNodeId, <value> value entry. In
	 *            case of {@link DataObjectAggregationDupSensitiveImpl} adding
	 *            only the <value> of the entry as key is irrelevant.
	 * @throws AggregationFunctionNotKnownException
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg, String aggregationFunctionId, int attributeId,
			AggregationFunctionEntry toAddEntry) throws AggregationFunctionNotKnownException {
		super(msg.getSender(), null, msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.clientServerSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();

		if (DuplicateSensitiveAggregationFunctionsDescriptions.MEAN.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT_NOREF.getId().equals(aggregationFunctionId)) {

			putDupSensitiveData(aggregationFunctionId, attributeId, toAddEntry);
		} else if (DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.getId().equals(aggregationFunctionId)
				|| DuplicateInsensitiveAggregationFunctionsDescriptions.MIN.getId().equals(aggregationFunctionId)) {

			putDupInsensitiveData(aggregationFunctionId, attributeId, toAddEntry.getValue());
		} else {
			throw new AggregationFunctionNotKnownException(aggregationFunctionId);
		}
	}

	/**
	 * Constructor for forwarding of a received
	 * {@link DataMessageContainerLocalImpl} with no additional data but updated
	 * gradient.
	 * 
	 * @param msg
	 * @param currentGradient
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg, double currentGradient) {
		super(msg.getSender(), null, msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.clientServerSinkId = msg.getSinkId();
		this.currentGradient = currentGradient;
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();
	}

	/**
	 * Add value either to existing aggregate or add new
	 * {@link DataObjectAggregationDupInsensitiveImpl} with the new aggregate
	 * for the specific attributeId.
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param value
	 * @throws AggregationFunctionNotKnownException
	 */
	private void putDupInsensitiveData(String aggregationFunctionId, int attributeId, double value)
			throws AggregationFunctionNotKnownException {
		boolean entryExisted = false;
		for (DataObjectAggregationDupInsensitive<Double> actDataObject : aggregationDupInsensitiveData) {
			if (actDataObject.getAggregationFunctionId().equals(aggregationFunctionId) && actDataObject.getAttributeId() == attributeId) {
				actDataObject.putValue(value);
				entryExisted = true;
			}
		}

		if (!entryExisted) {
			DataObjectAggregationDupInsensitiveImpl aggregationDupInsensitvieDataObejct = new DataObjectAggregationDupInsensitiveImpl(
					aggregationFunctionId, attributeId, value);
			this.aggregationDupInsensitiveData.add(aggregationDupInsensitvieDataObejct);
		}

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onDuplicateInsensitiveAggregationDataAddedInNetwork(aggregationFunctionId, attributeId, value);
		}
	}

	/**
	 * Add value either to existing aggregate or add new
	 * {@link DataObjectAggregationDupSensitiveImpl} with the new aggregate for
	 * the specific attributeId.
	 * 
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param toAddEntry
	 *            - <key> clientServerNodeId, <value> value
	 * @throws AggregationFunctionNotKnownException
	 */
	private void putDupSensitiveData(String aggregationFunctionId, int attributeId, AggregationFunctionEntry toAddEntry)
			throws AggregationFunctionNotKnownException {
		boolean entryExisted = false;
		for (DataObjectAggregationDupSensitiveImpl actDataObject : aggregationDupSensitiveData) {
			if (actDataObject.getAggregationFunctionId().equals(aggregationFunctionId) && actDataObject.getAttributeId() == attributeId) {
				actDataObject.putValue(toAddEntry, MergePoint.IN_NETWORK);
				entryExisted = true;
			}
		}

		if (!entryExisted) {
			DataObjectAggregationDupSensitiveImpl aggregationDupSensitvieDataObejct = new DataObjectAggregationDupSensitiveImpl(
					aggregationFunctionId, attributeId, toAddEntry);
			this.aggregationDupSensitiveData.add(aggregationDupSensitvieDataObejct);
		}

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onDuplicateSensitiveAggregationDataAddedInNetwork(toAddEntry.getUniqueEntryID());
		}

	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public UniqueID getUniqueMsgID() {
		return uniqueMsgId;
	}

	@Override
	public INodeID getSinkId() {
		return clientServerSinkId;
	}

	public double getGradient() {
		return currentGradient;
	}

	@Override
	public double getFilledPercentage() {
		return (double) getSize() / (double) MAX_MESSAGE_PAYLOAD;
	}

	@Override
	public long getSize() {
		/*
		 * Normal data: attributeId +2, nodeId +2, value +8
		 * 
		 * Dup Insens: short attributeId +2, byte aggregationFunction, double
		 * currentValue +8
		 * 
		 * Dup Sens: short attributeId +2, byte aggregationFunction, map<double
		 * values, short nodeId> +10
		 */
		long bytesNormalData = normalData.size() * 14;
		long bytesAggDupInsens = aggregationDupInsensitiveData.size() * 10;
		long bytesAggDupSens = 0;
		for (DataObjectAggregationDupSensitiveImpl actDupSensitiveObject : aggregationDupSensitiveData) {
			bytesAggDupSens += actDupSensitiveObject.getMessageSize();
//			bytesAggDupSens += 2; // attributeID
//			bytesAggDupSens += actDupSensitiveObject.getIncludedEntriesIDs().size() * 10; // +10
//																							// for
//																							// each
//																							// entry
//																							// in
//																							// aggFunc
		}
		return super.getSize() + bytesNormalData + bytesAggDupInsens + bytesAggDupSens;
	}

	@Override
	public LinkedList<DataObjectImpl<Double>> getNormalDataList() {
		return normalData;
	}

	@Override
	public LinkedList<DataObjectAggregationDupInsensitiveImpl> getAggregationDupInsensitiveDataList() {
		return aggregationDupInsensitiveData;
	}

	@Override
	public LinkedList<DataObjectAggregationDupSensitiveImpl> getAggregationDupSensitiveDataList() {
		return aggregationDupSensitiveData;
	}

	/*
	 * Analyzing convenience methods.
	 */
	private boolean checkedForDataRoutingAnalyzer = false;

	private DataRoutingAnalyzer dataRoutingAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	private boolean hasDataRoutingAnalyzer() {
		if (dataRoutingAnalyzer != null) {
			return true;
		} else if (checkedForDataRoutingAnalyzer) {
			return false;
		}
		getDataRoutingAnalyzer();
		return dataRoutingAnalyzer != null;
	}

	private DataRoutingAnalyzer getDataRoutingAnalyzer() {
		if (!checkedForDataRoutingAnalyzer) {
			try {
				dataRoutingAnalyzer = Monitor.get(DataRoutingAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				dataRoutingAnalyzer = null;
			}
			checkedForDataRoutingAnalyzer = true;
		}
		return dataRoutingAnalyzer;
	}
}
