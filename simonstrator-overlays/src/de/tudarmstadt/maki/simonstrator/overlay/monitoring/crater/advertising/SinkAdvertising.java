package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising;

import java.util.AbstractMap;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.NoKnownSinksException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.NoSinkMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.SinkAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterNodeRoleListener;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutMap;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.ITimeoutMapListener;

/**
 * Mechanisms for basic sink advertisement.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 04.06.2014
 * 
 */
public interface SinkAdvertising extends CraterNodeRoleListener {
	
	public void initialize();

	/**
	 * Handles incoming {@link SinkAdvertisingMessage} that is dispatched in
	 * {@link CraterNodeComponent} to this module. - maintaining sink table
	 * 
	 * @param msg
	 */
	public void handleIncomingSinkAdvMsg(SinkAdvertisingMessage msg);

	/**
	 * Node is Sink and thus must handle {@link NoSinkMessage} - if IDLE -> get
	 * ACTIVE and start sink advertisement
	 * 
	 * @param msg
	 */
	public void handleIncomingNoSinkMsg(NoSinkMessage msg);

	/**
	 * Send advertising beacon after tH ran out. Only if params of
	 * {@link CraterNodeSettingsImpl} - {@link CraterNodeState} = <b>ACTIVE</b>
	 * and isSink = true. Also watch for waiting on other sink beacons in
	 * interval and other specs defined in the conceptional design
	 */
	public void sendAdvBeacon();

	/**
	 * Forward received beacon according to conceptual design.
	 */
	public void forwardAdvBeacon(SinkAdvertisingMessage msg);

	/**
	 * @return <code>true</code> if sink table is empty. <code>false</code> if
	 *         sink table contains entries.
	 */
	public boolean sinkTableIsEmpty();

	/**
	 * Computes the hesitation time of that node.
	 * 
	 * @return hesitationTime in {@link Time#MILLISECOND}.
	 */
	public long getHesitationTime();

	/**
	 * Add listener to {@link TimeoutMap} that need to be informed whenever the
	 * sinkTable got empty <b>(from size >= 1 to size = 0)</b> or was filled <b>(from size = 0 to size >= 1)</b>
	 * 
	 * @param l
	 */
	public void addSinkTableListener(ITimeoutMapListener l);
	
	/**
	 * Returning a value [0,100] representing the quality of the sink.
	 * @return
	 */
	public int getSinkQuality();
	
	/**
	 * Return the sinkId of the {@link SinkTableEntry} that has the largest
	 * gradient, while at the same time has a minimum of X successive updates?
	 * If there is not such an entry do with {@link SinkTableEntry} until (with
	 * less successive updates) one is found.
	 * 
	 * @return best possible sinkTable entry<id, currentGradient> or {@link NoKnownSinksException} in case of
	 *         empty sink table!
	 * @throws NoKnownSinksException
	 */
	public AbstractMap.SimpleEntry<INodeID, Double> getBestSinkTableEntry() throws NoKnownSinksException;
	
	/**
	 * Indicator method that {@link DataRoutingOLD} can inform
	 * {@link SinkAdvertising} that {@link DataMessage} has been received.
	 */
	public void hasReceivedDataMessageAsSink();
	
}
