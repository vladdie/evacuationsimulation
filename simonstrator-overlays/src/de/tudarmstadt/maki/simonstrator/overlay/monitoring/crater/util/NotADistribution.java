package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util;

import de.tudarmstadt.maki.simonstrator.api.util.Distribution;

/**
 * Fake 'Distribution' to be used with PeriodicOperations, allowing the
 * application more control over timed events.
 * 
 * @author Christoph Storm
 */
public class NotADistribution implements Distribution {

	private long interval;

	public NotADistribution(long interval) {
		this.interval = interval;
	}

	/**
	 * Sets the interval to the new value. Next time the PeriodicOperation is
	 * rescheduling itself, it will use this interval.
	 * 
	 * @param value
	 */
	public void setInterval(long value) {
		interval = value;
	}

	@Override
	public double returnValue() {
		return interval;
	}
};