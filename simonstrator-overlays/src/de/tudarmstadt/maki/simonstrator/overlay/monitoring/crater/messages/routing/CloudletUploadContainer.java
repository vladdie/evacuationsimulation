package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.AggregationFunctionNotKnownException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterBackendMessage;

/**
 * 
 * @author Nils Richerzhagen
 *
 */
public class CloudletUploadContainer extends DataMessageContainerUploadImpl implements CraterBackendMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4117555473188938326L;

	public CloudletUploadContainer(OverlayContact sender, OverlayContact receiver,
			LinkedList<DataObjectImpl<Double>> normalData,
			LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveData,
			LinkedList<DataObjectAggregationDupSensitiveImpl> aggregationDupSensitiveData) {
		super(sender, receiver, normalData, aggregationDupInsensitiveData, aggregationDupSensitiveData);
	}

	/**
	 * Copy constructor + Normal Data
	 * 
	 * @param msg
	 * @param actNodeId
	 * @param attributeId
	 * @param value
	 */
	public CloudletUploadContainer(DataMessageContainerUploadImpl msg, INodeID actNodeId, int attributeId,
			double value) {
		super(msg, actNodeId, attributeId, value);
	}

	/**
	 * Copy constructor + aggDubSen or aggDubInsens
	 * 
	 * @param msg
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param toAddEntry
	 * @throws AggregationFunctionNotKnownException
	 */
	public CloudletUploadContainer(DataMessageContainerUploadImpl msg, String aggregationFunctionId,
			int attributeId, AggregationFunctionEntry toAddEntry) throws AggregationFunctionNotKnownException {
		super(msg, aggregationFunctionId, attributeId, toAddEntry);
	}



}
