package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages;

/**
 * Interface for a message with gradient.
 * @author Nils Richerzhagen
 * @version 1.0, 20.06.2014
 *
 */
public interface MessageWithGradient {

	/**
	 * Returns the gradient of that message.
	 * @return
	 */
	public int getGradient();

	
}
