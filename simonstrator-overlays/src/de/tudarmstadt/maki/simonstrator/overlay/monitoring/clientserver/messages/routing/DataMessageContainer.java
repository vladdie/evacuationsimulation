package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing;

import java.util.LinkedList;

/**
 * Interface for the container data message. This message includes normal data,
 * dup. sen. aggregates and dup. insens. aggregates.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 * 
 */
public interface DataMessageContainer {
	/**
	 * Should be called before data is added. <code>true</code> when the message
	 * size received the max MTU. (or near to it 90%) <code>false</code> when
	 * more data can be added.
	 * 
	 * @return
	 */
	public double getFilledPercentage();

	/**
	 * Returning the list of {@link DataObject}'s.
	 * 
	 * @return
	 */
	public LinkedList<DataObjectImpl<Double>> getNormalDataList();

	/**
	 * Returning the list of {@link DataObjectAggregationDupInsensitiveImpl}'s.
	 * 
	 * @return
	 */
	public LinkedList<DataObjectAggregationDupInsensitiveImpl> getAggregationDupInsensitiveDataList();

	/**
	 * Returning the list of {@link DataObjectAggregationDupSensitiveImpl}'s.
	 * 
	 * @return
	 */
	public LinkedList<DataObjectAggregationDupSensitiveImpl> getAggregationDupSensitiveDataList();

	public enum MergePoint {
		IN_NETWORK,

		CENTRAL,

		DURING_CLONE,

		IN_ANALYZER
	}
}
