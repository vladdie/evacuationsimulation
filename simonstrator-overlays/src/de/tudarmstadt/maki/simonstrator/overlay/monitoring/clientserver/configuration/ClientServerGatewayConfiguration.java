package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.configuration;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent.ClientServerNodeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.gateway.RandomGatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.gateway.StaticGatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.gateway.TransitionEnabledGatewayLogic;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.GatewaySelectionStrategy;

/**
 * Configuration for Gateway-Election Purposes.
 * 
 * 
 * @author Nils Richerzhagen
 *
 */
public class ClientServerGatewayConfiguration implements ClientServerConfiguration {

	private GatewaySelectionStrategy defaultStrategy = null;

	public enum GatewayStrategy {
		RANDOM, STATIC, TRANSITION
	}

	private GatewayStrategy selectedStrategy = GatewayStrategy.RANDOM;

	// private GatewayLogicBehaviour selectedLogicBehaviour = null;

	@Override
	public void configure(ClientServerComponent comp, ClientServerNodeType type) {
		switch (type) {
		case CLOUD:
			switch (selectedStrategy) {
			case RANDOM:
				((ClientServerCentralComponent) comp).setGatewayLogic(new RandomGatewayLogic(comp));
				break;

			case STATIC:
				((ClientServerCentralComponent) comp).setGatewayLogic(new StaticGatewayLogic(comp));
				break;

			case TRANSITION:
				assert defaultStrategy != null;
				// assert selectedLogicBehaviour != null;
				((ClientServerCentralComponent) comp).setGatewayLogic(new TransitionEnabledGatewayLogic(comp, defaultStrategy));
				break;

			default:
				throw new AssertionError("No valid GatewaySelectionStrategy chosen.");
			}
			break;
		default:
			// nothing to do
			break;
		}
	}

	/**
	 * Sets the default strategy to be used by the cloud. Used in {@link TransitionEnabledGatewayLogic}.
	 * 
	 * @param defaultStrategy
	 */
	public void setDefault(GatewaySelectionStrategy defaultStrategy) {
		this.defaultStrategy = defaultStrategy;
	}

	/**
	 * Which strategy scheme to use {@link GatewayStrategy}.
	 * 
	 * @param strategy
	 */
	public void setStrategy(String strategy) {
		selectedStrategy = GatewayStrategy.valueOf(strategy);
		assert selectedStrategy != null;
	}

	// /**
	// * Which {@link GatewayLogicBehaviour} to use in the {@link TransitionEnabledGatewayLogic}.
	// *
	// * @param selectedLogicBehaviour
	// */
	// public void setLogicBehaviour(String logicBehaviour) {
	// this.selectedLogicBehaviour = GatewayLogicBehaviour.valueOf(logicBehaviour);
	// assert selectedLogicBehaviour != null;
	// }

}
