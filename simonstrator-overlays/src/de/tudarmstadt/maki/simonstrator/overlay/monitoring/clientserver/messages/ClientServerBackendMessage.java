package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.api.ClientServerMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.cloudlet.ClientServerCloudletComponent;

/**
 * Marker interface for ClientServer Messages that are used for communication
 * between {@link ClientServerCentralComponent} and
 * {@link ClientServerCloudletComponent}s.
 * 
 *
 * @author Nils Richerzhagen
 *
 */
public interface ClientServerBackendMessage extends ClientServerMessage {
	/*
	 * Marker Interface
	 */
}
