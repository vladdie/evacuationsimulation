package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation;

/**
 * Interface for duplication <strong>insensitive</strong> aggregates.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 *
 * @param <T>
 */
public interface DuplicateInsensitiveAggregationFunction<T>{

	public enum DuplicateInsensitiveAggregationFunctionsDescriptions{
		MAX("MAX", "Maximum value of all inputs."), MIN("MIN", "Minimum value of all inputs.");

		private final String representation;
		private final String id;

		private DuplicateInsensitiveAggregationFunctionsDescriptions(String id, String represenation) {
			this.representation = represenation;
			this.id = id;
		}

		@Override
		public String toString() {
			return representation;
		}
		
		public String getId(){
			return id;
		}
	}
	
	/**
	 * Add a value to the aggregate. As this one is duplication insensitive do not care about node id etc.
	 * @param value
	 */
	public void putValue(T value);
	
	/**
	 * Returning the current aggregate value.
	 * @return
	 */
	public T getAggregateValue();
	
	/**
	 * Returning the description of the aggregation function.
	 * @return
	 */
	public String getDescription();
	
	/**
	 * 
	 * @return
	 */
	public String getId();
}
