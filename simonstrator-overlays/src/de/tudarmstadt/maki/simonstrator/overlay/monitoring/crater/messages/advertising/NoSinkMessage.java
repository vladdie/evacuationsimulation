package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterFactory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithUniqueID;

/**
 * 'noSink' Beacons initiated by nodes.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 05.06.2014
 *
 */
public class NoSinkMessage extends AbstractOverlayMessage implements CraterLocalMessage, MessageWithUniqueID{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5901943598752116996L;
	
	private UniqueID uniqueId;

	/**
	 * Constructor for NoSinkMessage
	 * @param sender
	 */
	public NoSinkMessage(OverlayContact sender) {
		super(sender, null);
		this.uniqueId = CraterFactory.getRandomMessageId();
	}
	
	/**
	 * Copy-constructor. Currently not needed, but maybe when TTL should be in here it is needed.
	 * @param receivedMsg
	 */
	public NoSinkMessage(NoSinkMessage receivedMsg){
		super(receivedMsg.getSender(), null);
		this.uniqueId = receivedMsg.getUniqueMsgID();
	}
	
	@Override
	public long getSize() {
		return super.getSize();
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public UniqueID getUniqueMsgID() {
		return uniqueId;
	}
	
}
