package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.configuration;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent.ClientServerNodeType;

/**
 * Helper class for the configuration of {@link ClientServerComponent}s. This
 * configuration is passed to the Factory for ClientServerNodes to ensure a
 * proper configuration of all hosts.
 * 
 * @author Nils Richerzhagen
 *
 */
public interface ClientServerConfiguration {

	public void configure(ClientServerComponent comp, ClientServerNodeType type);
}
