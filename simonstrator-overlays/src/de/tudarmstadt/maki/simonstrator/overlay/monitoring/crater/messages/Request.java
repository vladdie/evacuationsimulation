package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.SiSTypeNotFoundException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.UnknownTypeException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.NodeCondition;

/**
 * A loader for the RequestType
 * 
 * @author Marc Schiller
 * @version 1.0, May 30, 2016
 */
public class Request {

	private final static String REGEX = ",";

	// The list of valid Request Types
	public static final String[] REQUEST_TYPES = { "PERIODIC", "ONE-SHOT", "EVENT" };

	// Is this request prepared for sending (source is set, validity calculated)
	private boolean prepared = false;

	// Storage

	private INodeID source;

	private long interval = 0;
	private long validity = 0;

	private String attribute = "";
	private HashSet<String> receiver = new HashSet<String>();
	// private INodeID[] receiver = {};
	private String type = "";

	private HashSet<NodeCondition> target = new HashSet<>();

	// FIXME: Is this constructor obsolete? As below there is the same without "interval".
	// /**
	// * Constructor for (periodic) requests. If it should be one-shot, pass
	// * nothing for "interval" and "validity".
	// *
	// * @param attribute
	// * the attribute that should be requested
	// * @param type
	// * the type of the request
	// * @param validity
	// * How long is the sender interested in replies to this request
	// * @param receiver
	// * the receivers for this req either ALL or list of IDs
	// * @throws SiSTypeNotFoundException
	// * if the wanted SiSType does not exist
	// * @throws UnknownTypeException
	// * if the comparator does not exist
	// */
	// @XMLConfigurableConstructor({ "attribute", "type", "validity", "receiver" })
	// public Request(String attribute, String type, String validity, String receiver)
	// throws SiSTypeNotFoundException, UnknownTypeException {
	// this.setAttribute(attribute);
	// this.setReceiver(receiver);
	// this.setType(type);
	// this.setValidity(validity);
	// }

	/**
	 * Constructor for (periodic) requests. If it should be one-shot, pass
	 * nothing for "interval" and "validity".
	 * 
	 * @param attribute
	 *            the attribute that should be requested
	 * @param type
	 *            the type of the request
	 * @param validity
	 *            How long is the sender interested in replies to this request
	 * @param interval
	 *            in which interval the targets should send replies
	 * @param receiver
	 *            the receivers for this req either ALL or list of IDs
	 * @throws SiSTypeNotFoundException
	 *             if the wanted SiSType does not exist
	 * @throws UnknownTypeException
	 *             if the comparator does not exist
	 */
	@XMLConfigurableConstructor({ "attribute", "type", "validity", "interval", "receiver" })
	public Request(String attribute, String type, String validity, String interval, String receiver)
			throws SiSTypeNotFoundException, UnknownTypeException {
		this.setAttribute(attribute);
		this.setInterval(interval);
		this.setReceiver(receiver);
		this.setType(type);
		this.setValidity(validity);
	}

	/**
	 * Copy Constructor
	 * 
	 * @param otherRequest
	 */
	public Request(Request otherRequest) {
		this.attribute = otherRequest.getAttribute();
		this.interval = otherRequest.getInterval();
		this.type = otherRequest.getType();
		this.validity = otherRequest.getValidity();
		this.receiver = otherRequest.getReceiver();
	}

	// Getter for data
	public String getAttribute() {
		return attribute;
	}

	public long getInterval() {
		return interval;
	}

	public HashSet<String> getReceiver() {
		return receiver;
	}

	public INodeID getSource() {
		return source;
	}

	public HashSet<NodeCondition> getTarget() {
		return target;
	}

	public String getType() {
		return type;
	}

	public long getValidity() {
		return validity;
	}

	/**
	 * Set the Attribute that should be requested
	 * 
	 * @param attribute
	 *            The SiS type
	 * @throws SiSTypeNotFoundException
	 *             if the wanted SiSType does not exist
	 */
	private void setAttribute(String attribute) throws SiSTypeNotFoundException {
		for (Field field : SiSTypes.class.getFields()) {
			if (field.getName().equals(attribute)) {
				this.attribute = attribute;
				break;
			}
		}
		if (this.attribute == null) {
			throw new SiSTypeNotFoundException();
		}
	}

	/**
	 * Set the interval for periodic requests
	 * 
	 * @param interval
	 *            as time defined in {@link Time}
	 */
	private void setInterval(String interval) {
		this.interval = Time.parseTime(interval);
	}

	private void setReceiver(String receiver) {
		String[] splittedReceiver = receiver.split(REGEX);
		for (String actReceiver : splittedReceiver) {
			this.receiver.add(actReceiver);
		}
	}

	/**
	 * Set the sender of this Request
	 * 
	 * @param src
	 *            the Source of the Request
	 */
	private void setSource(INodeID src) {
		this.source = src;
	}

	/**
	 * Add a target to the list of targets (the nodes from whom data is
	 * requested)
	 * 
	 * @param target
	 */
	public void setTarget(NodeCondition target) {
		this.target.add(target);
	}

	/**
	 * Set the type of the request (One-Shot, Periodic, Event)
	 * 
	 * @param type
	 * @throws UnknownTypeException
	 */
	private void setType(String type) throws UnknownTypeException {
		if (Arrays.asList(REQUEST_TYPES).contains(type)) {
			this.type = type;
		} else {
			throw new UnknownTypeException();
		}
	}

	/**
	 * Set the validity for periodic requests
	 * 
	 * @param validtiy
	 *            as time defined in {@link Time}
	 */
	private void setValidity(String validity) {
		this.validity = Time.parseTime(validity);
	}

	/**
	 * Pretty print
	 */
	public String toString() {
		String retString = "Attribute: " + this.attribute + " / Type: " + this.type + " / Receiver: ";

		String temp = "";
		for (String recv : this.receiver) {
			retString += temp + recv;
			temp = ", ";
		}

		switch (this.type) {
		case "PERIODIC":
			retString += " / Validity: " + this.validity + " / Interval: " + this.interval;
			break;
		case "ONE-SHOT":
			retString += retString;
			break;
		case "EVENT":
			retString += "";
			break;
		default:
			// Should never happen
			return "Error";
		}

		retString += "\n";

		for (NodeCondition tgt : this.target) {
			retString += "\t\tTarget: " + tgt.toString() + "\n";
		}

		return retString;
	}

	/**
	 * Calculate the Hash for this Request
	 * 
	 */
	@Override
	public int hashCode() {
		// Christoph: Previous version resulted in non-unique hashes
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attribute == null) ? 0 : attribute.hashCode());
		result = prime * result + (int) (interval ^ (interval >>> 32));
		result = prime * result + receiver.hashCode();
		result = prime * result + ((source == null) ? 0 : source.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + (int) (validity ^ (validity >>> 32));
		return result;
	}


	/**
	 * Prepare the Request for sending, setting source and calculate validity
	 * 
	 * @param src
	 */
	public void prepareForSending(INodeID src) {
		if (!prepared) {
			setSource(src);
			long validity = getValidity();
			validity += Time.getCurrentTime();
			this.validity = validity;
			prepared = true;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Request other = (Request) obj;
		if (attribute == null) {
			if (other.attribute != null)
				return false;
		} else if (!attribute.equals(other.attribute))
			return false;
		if (interval != other.interval)
			return false;
		if (!receiver.equals(other.receiver))
			return false;
		if (source == null) {
			if (other.source != null)
				return false;
		} else if (!source.equals(other.source))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (validity != other.validity)
			return false;
		return true;
	}

	// TODO: Handle Eventbased config
}
