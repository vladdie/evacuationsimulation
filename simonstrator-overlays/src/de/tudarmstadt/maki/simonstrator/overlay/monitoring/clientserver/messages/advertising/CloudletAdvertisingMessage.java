package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.advertising;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.AdvertisingMessage;

/**
 * A message for Cloudlets, placed on infrastructure entites such as AccessPoints or Cellular Towers, to advertise the
 * potential for leaves to connect.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CloudletAdvertisingMessage extends AdvertisingMessage {

	private static final long serialVersionUID = 1524805157163178654L;

	/**
	 * {@link CloudletAdvertisingMessage}s do always have the highest Quality, as they are opposed to mobile nodes
	 * placed on the fixed infrastructure entities.
	 */
	private final static int cloudletQuality = 100;

	/**
	 * Constructor for Cloudlet Advertising Message
	 * 
	 * @param sender
	 * @param clientServerNodeId
	 * @param gradient
	 * @param TTL
	 */
	public CloudletAdvertisingMessage(OverlayContact sender, INodeID clientServerNodeId, int TTL) {
		super(sender, clientServerNodeId, TTL, cloudletQuality);
	}

	/**
	 * Forwarding Constructor for Cloudlet Advertising Message
	 * 
	 * @param receivedMessage
	 */
	public CloudletAdvertisingMessage(CloudletAdvertisingMessage receivedMessage) {
		super(receivedMessage);
	}
}
