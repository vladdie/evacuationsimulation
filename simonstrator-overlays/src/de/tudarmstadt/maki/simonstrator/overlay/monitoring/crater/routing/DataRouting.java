package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing;

import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers.LocalBufferEntryLists;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.operations.PeriodicSendDataOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.operations.PeriodicUploadOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterNodeRoleListener;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.ResponseEventHandler;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.ITimeoutMapListener;

/**
 * Interfaces for DataRouting in Crater.
 * @author Nils Richerzhagen
 * @version 1.0, 06.06.2014
 * 
 */
public interface DataRouting extends CraterNodeRoleListener,
		ITimeoutMapListener, TransitionEnabled, EventHandler {
	
	/**
	 * CAUTON: Should not be called from the outside, when using a
	 * TransitionEngine to switch implementations.
	 * <p>
	 * Instead use the {@link TransitionEnabled.startMechanism} and check
	 * whether this is the very first call.
	 */
	@Deprecated
	public void initialize();

	/**
	 * Handle incoming {@link DataMessageContainerLocalImpl} that is directed
	 * from {@link CraterNodeComponent} to the {@link DataRouting}. Also handles
	 * replies in case of Unicast Transmissions.
	 * 
	 * @param msg
	 */
	public void handleIncomingMessage(CraterLocalMessage msg);

	/**
	 * In case of being node, Send own buffer content.
	 * 
	 * CAUTION! Do not make payload/packet larger than max. MTU.
	 */
	public void sendBufferContent();
	
	/**
	 * In case of being sink, upload data to infrastructure.
	 */
	public void uploadBufferContent();

	/**
	 * Forwarding of {@link DataMessageContainerLocalImpl} after hesiation time
	 * expires.
	 * 
	 * CAUTION - This method is exclusively responsible for adding own buffer
	 * content to such a msg.
	 * 
	 * CAUTION! Do not make payload/packet larger than max. MTU.
	 * 
	 * @param msg
	 */
	public void forwardDataMessage(DataMessageContainerLocalImpl msg);

	/**
	 * Returns the time the {@link PeriodicSendDataOperation} (or
	 * {@link PeriodicUploadOperation} when Sink) will trigger the next upload.
	 * 
	 * @return
	 */
	public long getTimeOfNextScheduledDataMessage();

	/**
	 * Make the Buffer accessible to other entities that may want to add
	 * information
	 * 
	 * @return
	 */
	public LocalBufferEntryLists getLocalBuffer();

	/**
	 * Returns an EventHandler that can be used to schedule additional non
	 * periodic Data sending events
	 * 
	 * @return
	 */
	public ResponseEventHandler getSendingScheduler();

}