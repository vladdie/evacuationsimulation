package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising;

import java.util.HashSet;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;

/**
 * A message for Cloudlets, placed on infrastructure entites such as AccessPoints or Cellular Towers, to advertise the
 * potential for leaves to connect.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CloudletAdvertisingMessage extends SinkAdvertisingMessage {

	private static final long serialVersionUID = 1524805157163178654L;

	/**
	 * {@link CloudletAdvertisingMessage}s do always have the highest Quality, as they are opposed to mobile nodes
	 * placed on the fixed infrastructure entities.
	 */
	private final static int cloudletQuality = 100;

	/**
	 * Constructor for Cloudlet Advertising Message
	 * 
	 * @param sender
	 * @param craterNodeId
	 * @param gradient
	 * @param TTL
	 */
	public CloudletAdvertisingMessage(OverlayContact sender, INodeID craterNodeId, int gradient, int TTL) {
		super(sender, craterNodeId, gradient, TTL, cloudletQuality, false, false, new HashSet<Integer>());
	}

	/**
	 * Forwarding Constructor for Cloudlet Advertising Message
	 * 
	 * @param receivedMessage
	 */
	public CloudletAdvertisingMessage(CloudletAdvertisingMessage receivedMessage) {
		super(receivedMessage);
	}
}
