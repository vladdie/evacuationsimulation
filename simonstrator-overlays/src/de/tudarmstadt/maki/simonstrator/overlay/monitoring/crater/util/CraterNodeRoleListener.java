package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api.CraterSubComponent;

/**
 * Listener is informed when the {@link CraterNodeRole} is changed. Enables real-time maintaining of
 * {@link CraterSubComponent}s and other listeners.
 * 
 * @author Nils Richerzhagen
 *
 */
public interface CraterNodeRoleListener {

	public void craterNodeRoleChanged(CraterNodeRole craterNodeRole);
}
