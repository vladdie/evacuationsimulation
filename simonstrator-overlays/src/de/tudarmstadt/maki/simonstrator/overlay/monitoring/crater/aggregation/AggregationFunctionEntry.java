package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterFactory;

public class AggregationFunctionEntry {
	
	private UniqueID entryID;
	
	private INodeID key;
	
	private Double value;
	
	/**
	 * Initial constructor.
	 * @param key
	 * @param value
	 */
	public AggregationFunctionEntry(INodeID key, Double value) {
		this.key = key;
		this.value = value;
		this.entryID = CraterFactory.getRandomAggregationFunctionEntryID();
	}
	
	/**
	 * Clone constructor.
	 * @param toCloneEntry
	 */
	public AggregationFunctionEntry(AggregationFunctionEntry toCloneEntry){
		this.key = toCloneEntry.getKey();
		this.value = toCloneEntry.value;
		this.entryID = toCloneEntry.getUniqueEntryID();
	}
	
	public INodeID getKey() {
		return key;
	}
	
	public Double getValue() {
		return value;
	}
	
	public UniqueID getUniqueEntryID() {
		return entryID;
	}
}
