package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.configuration;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent.CraterNodeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway.RandomGatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway.StaticGatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway.TransitionEnabledGatewayLogic;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller.GatewaySelectionMechanismLogic;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller.GatewaySelectionServerController;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.controller.GatewaySelectionServerControllerImpl;

/**
 * Configuration for Gateway-Selection Purposes.
 * 
 * 
 * @author Nils Richerzhagen
 *
 */
public class CraterGatewayConfiguration implements CraterConfiguration {

	GatewaySelectionMechanismLogic controllerLogic = null;
	// TODO!

	public enum GatewayStrategy {
		RANDOM, STATIC, TRANSITION
	}

	private GatewayStrategy selectedStrategy = GatewayStrategy.RANDOM;

	// private GatewayLogicBehavior selectedLogicBehaviour = null;

	@Override
	public void configure(CraterComponent comp, CraterNodeType type) {
		// TODO what a mess
		switch (type) {
		case CLOUD:
			switch (selectedStrategy) {
			case RANDOM:
				((CraterCentralComponent) comp).setGatewayLogic(new RandomGatewayLogic(comp));
				break;

			case STATIC:
				((CraterCentralComponent) comp).setGatewayLogic(new StaticGatewayLogic(comp));
				break;

			case TRANSITION:
				assert controllerLogic != null;

				GatewaySelectionServerController controller = new GatewaySelectionServerControllerImpl(comp.getHost());

				controller.setGatewaySelectionLogic(controllerLogic);
				TransitionEnabledGatewayLogic logic = new TransitionEnabledGatewayLogic(comp, controller);

				((CraterCentralComponent) comp).setGatewayLogic(logic);
				break;

			default:
				throw new AssertionError("No valid GatewaySelectionStrategy chosen.");
			}

			break;
		default:
			// nothing to do
			break;
		}
	}

	/**
	 * Which strategy scheme to use {@link GatewayStrategy}.
	 * 
	 * @param strategy
	 */
	public void setStrategy(String strategy) {
		selectedStrategy = GatewayStrategy.valueOf(strategy);
		assert selectedStrategy != null;
	}

	/**
	 * By which {@link GatewaySelectionMechanismLogic} the
	 * {@link TransitionEnabledGatewayLogic} is selecting the Mechanism for
	 * Gateway selection.
	 * 
	 * @param controllerLogic
	 */
	public void setGatewaySelectionLogic(GatewaySelectionMechanismLogic controllerLogic) {
		this.controllerLogic = controllerLogic;
	}

}
