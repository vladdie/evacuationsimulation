package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway.logiccontroller;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.configuration.GLConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.DGSSubComponents;
import de.tudarmstadt.maki.simonstrator.service.sis.decentralGatewaySelection.IDecentralGatewaySelectionClient;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionClient.NodeRole;
import de.tudarmstadt.maki.simonstrator.service.sis.gatewayselection.GatewaySelectionListener;

/**
 * This is the mobile Gateway Logic Controller. It 'controls' (as in: Executes
 * orders by the Overlord) the {@link IDecentralGatewaySelectionClient} running on each
 * mobile Node.
 * 
 * @author Christoph Storm
 * @version 2016.07.04
 */
@Deprecated
public class DecentralizedGatewayLogicController implements GatewaySelectionListener {

	private static boolean startWithDGS = false;

	private CraterNodeComponent parent;
	private boolean active = false;
	private IDecentralGatewaySelectionClient dgs;

	List<DGSSubComponents> dgsComponents = new LinkedList<DGSSubComponents>();

	public DecentralizedGatewayLogicController(CraterNodeComponent parent) {
		this.parent = parent;
	}

	public void init() {
		// try {
		// FIXME
		// dgs = parent.getHost().getComponent(DecentralGatewaySelectionClient.class);
			dgs.addGatewaySelectionListener(this);

			active = true;
			if (!startWithDGS) {
				// Possible that the DGS has not been initialized, yet
				Event.scheduleWithDelay(1L, new EventHandler() {
					@Override
					public void eventOccurred(Object content, int type) {
						stop();
					}
				}, null, 01);
			}
		// } catch (ComponentNotAvailableException e) {
		// // Should not happen
		// throw new AssertionError("DecentralLogicController needs a DecentralGatewaySelection!");
		// }
	}

	public void handleIncommingMessage(CraterCellMessage cellMsg) {

		if (cellMsg instanceof GLConfigurationMessage) {
			// ConfigMessage for us!
			GLConfigurationMessage msg = (GLConfigurationMessage) cellMsg;

			if (msg.isStartDGS()) {
				assert msg.isStartDGS() != msg.isStopDGS() : "Can't start and stop DGS simultaniously!";
				start(msg.isStartImmediately());
			}

			if (active) {
				// Should only do transitions if DGS is started
				changeComponents(new ArrayList<DGSSubComponents>(msg.getComponentList()));
				// Local Components should now be up to date.
				dgsComponents = msg.getComponentList();
			}

			if (msg.isStopDGS()) {
				// This must come after subcomponent exchanges!
				stop();
			}
		}
	}

	private void changeComponents(List<DGSSubComponents> newComps) {
		/*
		 * Originally that list only stated *changed* components. Now it
		 * includes *all* currently used components.
		 */
		// Only force a transition if something changed...
		newComps.removeAll(dgsComponents);
		boolean reActivate = dgs.isActive();
		for (DGSSubComponents newComp : newComps) {
			dgs.ComponentTransition(newComp.getType(), newComp.getImplementingClass());
		}
		if (reActivate) {
			// ComponentTransition stops the Selection process.
			// Start anew.
			dgs.getTrigger().onTriggeredFromOutside();
		}
	}

	public void start(boolean selectImmideately) {
		if (!active) {
			// FIXME
			// dgs.start();
			active = true;
			if (selectImmideately) {
				// give other nodes the possibility to receive similar messages
				// first...(10ms should be enough)
				Event.scheduleWithDelay(10L, new EventHandler() {
					@Override
					public void eventOccurred(Object content, int type) {
						if (!dgs.isActive()) {
							dgs.getTrigger().onTriggeredFromOutside();
						}
					}
				}, null, 012);
			}
		}
	}

	public void stop() {
		// FIXME
		// dgs.shutdown();
		active = false;
	}

	public boolean isActive() {
		return active;
	}

	@Override
	public void gatewaySelectionFinished(NodeRole Role, INodeID Clusterhead) {

		switch (Role) {
		case CLUSTERHEAD:
			if (!parent.getCraterNodeRole().equals(CraterNodeRole.SINK)) {
				parent.setCraterNodeRole(CraterNodeRole.SINK);
				break;
			}
		case CLUSTERMEMBER:
			if (!parent.getCraterNodeRole().equals(CraterNodeRole.LEAF)) {
				parent.setCraterNodeRole(CraterNodeRole.LEAF);
				parent.getSinkAdvertising(); // FIXME What for?
				// TODO something with clusterID?
				break;
			}
		default:
			// nothing
		}
	}

	public static void setStartWithDGS(boolean withDGS) {
		startWithDGS = withDGS;
	}

}
