package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;

/**
 * Implementation of {@link SendHistory}, which keeps track of the last n
 * messages send. n is set in the constructor
 * 
 * @author Jonas Huelsmann
 * 
 */
public class SizedCraterSendHistory extends SendHistory {
	private LinkedList<UniqueID> HISTORY;
	
	public SizedCraterSendHistory(long size) {
		super(size);
		HISTORY = new LinkedList<UniqueID>();
	}

	/*
	 * Method that checks if a sting is an entry of this history. If aString is
	 * in the history it will be moved to the end of the history
	 * 
	 * @param msgID Potential entry
	 * 
	 * @return false if aString is not within the History, true if it is
	 */
	protected boolean isAnEntry(UniqueID msgID) {
		if (HISTORY.contains(msgID)) {
			// move aString to the end of the List
			HISTORY.remove(msgID);
			HISTORY.add(msgID);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean isInHistory(UniqueID msgID) {
		return HISTORY.contains(msgID);
	}

	/**
	 * Method that adds an entry to the History
	 * 
	 * @param msgID
	 *            Entry to be added
	 * 
	 * @return true if successfully added
	 */
	protected boolean addToHistory(UniqueID msgID) {
		if (HISTORY.size() < HISTORY_SIZE) {
			return HISTORY.add(msgID);
		} else {
			HISTORY.removeFirst();
			return HISTORY.add(msgID);
		}

	}

	@Override
	public boolean sendable(UniqueID msgID) {
		if (isAnEntry(msgID)) {
			return false;
		} else {
			addToHistory(msgID);
			return true;
		}

	}

	@Override
	public Set<UniqueID> sendable(Collection<UniqueID> msgIDs) {
		Set<UniqueID> temp = new TreeSet<UniqueID>();
		UniqueID msgID;
		for (Iterator<UniqueID> iter = msgIDs.iterator(); iter.hasNext();) {
			msgID = iter.next();
			if (sendable(msgID))
				temp.add(msgID);
		}
		return temp;
	}

	@Override
	public void resetHistory() throws Exception {
		throw new Exception(
				"resetHistory is not ment to be called at a SizedCraterSendHistory");

	}
	
}
