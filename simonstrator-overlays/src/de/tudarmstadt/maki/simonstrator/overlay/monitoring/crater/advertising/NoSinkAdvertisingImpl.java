package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising;

import java.util.concurrent.atomic.AtomicBoolean;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.PerNodeHasNoSinkTimesAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.PerNodeRoleTimesAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api.CraterSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.NoSinkMessage;
import de.tudarmstadt.maki.simonstrator.service.transition.local.AtomicTransitionExecution.ComponentState;

/**
 * Implementation of the NoSinkAdvertising mechanism.
 *
 * @author Nils Richerzhagen
 * @version 1.0, 06.06.2014
 *
 */
public class NoSinkAdvertisingImpl extends CraterSubComponent implements NoSinkAdvertising, EventHandler {

	private static long NO_SINK_ADVERTISING_INTERVAL;

	private static final long NO_SINK_ADVERTISING_RESTART_TIME = 2 * Time.MINUTE;

	private static final long _MIN_NO_SINK_ADV_HES_TIME = 0 * Time.SECOND;
	private static final long _MAX_NO_SINK_ADV_HES_TIME = 10 * Time.SECOND;

	private final int NO_SINK_ADV_EVENT_ID = 30;
	private final int NO_SINK_ADV_RESTART_MECHANISM_EVENT_ID = 31;

	// private CraterNodeComponent parentNode;

	private boolean _heardNoSinkAdvMsgDuringHesTime;
	private boolean changesDuringRestartTime;
	private boolean _heardNoSinkAdvMsgDuringRestartTime;

	private PeriodicOperation<CraterNodeComponent, Object> periodicNoSinkAdvertising;

	// AtomicBoolean set with initial false value
	private final AtomicBoolean _initializeMethodCall = new AtomicBoolean(true);

	/**
	 * Constructor for the NoSinkAdvertising mechanism.
	 *
	 * @param parentNode
	 */
	public NoSinkAdvertisingImpl(CraterNodeComponent parentNode, long NO_SINK_ADVERTISING_INTERVAL) {
		super(parentNode);
		// this.parentNode = parentNode;
		NoSinkAdvertisingImpl.NO_SINK_ADVERTISING_INTERVAL = NO_SINK_ADVERTISING_INTERVAL;
	}

	public void initialize() {
		initializePeriodicOperations();
		setComponentState(SubComponentState.INACTIVE);
	}

	/**
	 * Initialization of the periodic operations for the different internal parts of the {@link NoSinkAdvertising}
	 * mechanism.
	 */
	private void initializePeriodicOperations() {
		periodicNoSinkAdvertising = new PeriodicOperation<CraterNodeComponent, Object>(getParentComponent(),
				Operations.getEmptyCallback(),
				NO_SINK_ADVERTISING_INTERVAL) {

			@Override
			public Object getResult() {
				return null;
			}

			@Override
			protected void executeOnce() {
				if (isInactive()) {
					return;
				}
				_heardNoSinkAdvMsgDuringHesTime = false;
				scheduleHesitationTime();
			}
		};
	}

	/**
	 * Used to reset the automata.
	 *
	 * Called when host went offline or when {@link CraterNodeComponent} became sink. May be sink before going offline,
	 * thus component is already in {@link ComponentState} INACTIVE.
	 */
	private void resetComponent() {
		if (!getParentComponent().isPresent() || getParentComponent().isSink()) {
			if (isInactive()) {
				return;
			}
			setComponentState(SubComponentState.INACTIVE);
		} else {
			assert getParentComponent().isSink() : "Should only be called when the parent node is a sink or went Offline";
		}

	}

	/**
	 * Starting the automata.
	 *
	 * Called when the node went online or changed from {@link CraterNodeRole} SINK to LEAF. Thus,
	 * {@link CraterNodeComponent} should not be sink.
	 *
	 * When the sink table of the node is empty start in ACTIVE mode when sink table is filled start in IDLE mode.
	 *
	 * State here must be INACTIVE.
	 */
	private void startComponent() {
		// Insanity Checks
		assert !getParentComponent().isSink();
		assert getParentComponent().isPresent();
		assert isInactive();

		/*
		 * Becoming node - start NoSinkAdvertising mechanism by changing state to ACTIVE only when no sink table entry
		 */
		if (getParentComponent().sinkTableIsEmpty()) {
			assert !isActive();
			// getCurrentComponentState() != SubComponentState.ACTIVE;

			setComponentState(SubComponentState.ACTIVE);
		} else {
			assert !isIdle();
			// getCurrentComponentState() != SubComponentState.IDLE;

			setComponentState(SubComponentState.IDLE);
		}
	}

	/**
	 * Just a scope gaining method. To be able to have the {@link NoSinkAdvertising} as scope and this as event handler.
	 */
	public void scheduleHesitationTime() {
		Event.scheduleWithDelay(getHesitationTime(), this, null, NO_SINK_ADV_EVENT_ID);
	}

	@Override
	public long getHesitationTime() {
		// TODO other factors?
		double weightBattery = 0.4;
		double weightRandomness = 0.6;
		int randomValue = 0 + (int) (getParentComponent().getRandom().nextDouble() * ((100 - 0) + 1));
		double hesFactor = (weightBattery * (100 - getParentComponent().getCurrentBatteryPercentage())
				+ weightRandomness * randomValue)
				/ 100;
		long hesitationTime = _MIN_NO_SINK_ADV_HES_TIME
				+ (long) (hesFactor * (_MAX_NO_SINK_ADV_HES_TIME - _MIN_NO_SINK_ADV_HES_TIME));

		assert hesitationTime >= 0 : "Hesitation Time must be positive";

		return hesitationTime;
	}

	@Override
	public void handleIncomingNoSinkMessage(NoSinkMessage msg) {
		changesDuringRestartTime = true;

		assert !getParentComponent().isSink() : "Should only receive NoSinkMessage here when being no Sink";

		_heardNoSinkAdvMsgDuringHesTime = true;
		_heardNoSinkAdvMsgDuringRestartTime = true;
	}

	/**
	 * Schedules the restart event and resets all internal flags for restart time config.
	 */
	private void scheduleRestartTime() {
		Monitor.log(NoSinkAdvertising.class, Monitor.Level.INFO,
				"NoSinkAdv: Node " + getParentComponent().getHost().getId().value() + " schedule RESTART EVENT");
		Event.scheduleWithDelay(NO_SINK_ADVERTISING_RESTART_TIME, this, null, NO_SINK_ADV_RESTART_MECHANISM_EVENT_ID);
		changesDuringRestartTime = false;
		_heardNoSinkAdvMsgDuringRestartTime = false;
	}

	@Override
	public void sendNoSinkBeacon() {
		assert isActive();

		Monitor.log(NoSinkAdvertising.class, Level.INFO,
				"Periodic NoSinkAdv on Node " + getParentComponent().getHost().getId().value());
		/*
		 * When heard noSinkAdvertising message during hesitation time do nothing and set back own state to IDLE.
		 */
		if (_heardNoSinkAdvMsgDuringHesTime) {
			// getParentComponent().setCraterNodeState(CraterNodeState.IDLE);
			assert !isIdle();

			setComponentState(SubComponentState.IDLE);
			_heardNoSinkAdvMsgDuringHesTime = false;
			return;
		}
		/*
		 * When sink table is not empty do nothing. Thus, sinkadv msg received in last time. Can go back to IDLE state,
		 * but should be gone back via listener!
		 */
		if (!getParentComponent().sinkTableIsEmpty()) {
			// parentNode.setCraterNodeState(CraterNodeState.IDLE);
			assert !isIdle();

			setComponentState(SubComponentState.IDLE);
			_heardNoSinkAdvMsgDuringHesTime = false;
			return;
		}

		// TODO add percentage!?
		// if (parentNode.getRandom().nextDouble() >= 0.3) {
		Monitor.log(NoSinkAdvertising.class, Monitor.Level.INFO,
				"NoSinkAdv: Node " + getParentComponent().getHost().getId().value() + " send NoSinkMessage");
		getParentComponent().sendLocalBroadcast(new NoSinkMessage(getParentComponent().getLocalOverlayContact()));
		_heardNoSinkAdvMsgDuringHesTime = false;
		// }
	}

	@Override
	public void gotEmpty() {

		if (isInactive()) {
			return;
		}

		/*
		 * If no sink and sink table just emptied start no sink adv.
		 */
		if (!getParentComponent().isSink()) {
			changesDuringRestartTime = true;

			/*
			 * May be ACTIVE before by Restart Event, thus check for ACTIVE.
			 */
			if (!isActive()) {
				setComponentState(SubComponentState.ACTIVE);
			}

		}

		if (hasStateAnalyzer()) {
			getNoSinkAnalyzer().onSinkTableGotEmpty(getParentComponent());
		}
	}

	@Override
	public void gotFilled() {

		if (isInactive()) {
			return;
		}

		if (!getParentComponent().isSink()) {
			changesDuringRestartTime = true;

			/*
			 * This one might be called when node is in restartTime (IDLE) and during that the sink table got filled.
			 */
			if (!isIdle()) {
				setComponentState(SubComponentState.IDLE);
			}

		}
		if (hasStateAnalyzer()) {
			getNoSinkAnalyzer().onSinkTableGotFilled(getParentComponent());
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		/*
		 * Events may be in queue.
		 * 
		 * Return when now INACTIVE due to node gone offline or changed role to sink.
		 */
		if (isInactive() || getParentComponent().isSink()) {
			return;
		}

		/*
		 * Hesitation time expired but only trigger sending method when still ACTIVE.
		 */
		if (type == NO_SINK_ADV_EVENT_ID) {

			if (getParentComponent().isSink()) {
				Monitor.log(NoSinkAdvertising.class, Monitor.Level.INFO,
						"NoSinkAdv: Event for sending NoSinkMessage stopped as node is sink now");
				return;
			}

			if (isActive()) {
				sendNoSinkBeacon();
			}
		}
		/*
		 * Restart NoSinkAdv Mechanism
		 */
		else if (type == NO_SINK_ADV_RESTART_MECHANISM_EVENT_ID) {
			Monitor.log(NoSinkAdvertising.class, Monitor.Level.INFO,
					"NoSinkAdv: Node " + getParentComponent().getHost().getId().value() + " RESTART EVENT Occured");

			/*
			 * Only when not being sink & not became active during restart time!
			 */
            if (getParentComponent().isSink() || isActive())
            {
                return;
            }

			/*
			 * Changes during restart time, still not sink (s.o.), no known sinks, and no sinkAdvMsg during that time
			 * interval (no other adjacent nodes having connectivity problems)
			 * 
			 * Is this needed - Not done by sinkTableIsEmpty()?
			 * 
			 * No is not done by sinkTableIsEmpty, as this is only called once. When the sinkTable remains empty. Maybe
			 * called directly after sink table got empty, thus check for component ACTIVE.
			 */
            if (changesDuringRestartTime && getParentComponent().sinkTableIsEmpty()
                    && !_heardNoSinkAdvMsgDuringRestartTime && !isActive())
            {
                setComponentState(SubComponentState.ACTIVE);
            } else if (!changesDuringRestartTime)
            {
				/*
				 * No changes during restart time interval & parent node is no sink (s.o.) -> Restart the
				 * noSinkAdvMechanism.
				 */
				assert !isActive();
				setComponentState(SubComponentState.ACTIVE);

			} else if (isIdle()) {
				/*
				 * Is still idle & no sink (s.o.) Schedule RESTART time again.
				 */
				scheduleRestartTime();
			}
		} else {
			throw new AssertionError("Unknown Event Type");
		}
	}

	@Override
	public void craterNodeRoleChanged(CraterNodeRole craterNodeRole) {
		switch (craterNodeRole) {
		case LEAF:
			startComponent();
			break;
		case SINK:
			resetComponent();
			break;
		case OFFLINE:
			resetComponent();
			break;
		default:
			throw new AssertionError("Unknown case.");
		}
	}

	@Override
	protected void setComponentState(SubComponentState state) {
		assert this.getComponentState() != state;

		if (!_initializeMethodCall.getAndSet(false)) {
			Monitor.log(NoSinkAdvertising.class, Monitor.Level.DEBUG, "Node " + getParentComponent().getHost().getId().value()
					+ " ComponentState from " + getComponentState().toString() + " to " + state.toString());
		}

		super.setComponentState(state);

		switch (state) {
		case ACTIVE:
			assert getParentComponent().isPresent();

			// Start Beaconing
			periodicNoSinkAdvertising.start();
			break;

		case INACTIVE:
			/*
			 * Node is eiter offline or sink.
			 */
                periodicNoSinkAdvertising.stop();
                break;
            case IDLE:
                assert getParentComponent().isPresent();
                periodicNoSinkAdvertising.stop();
                scheduleRestartTime();
                break;
            default:
                break;
        }
    }

	/*
	 * Analyzing convenience methods.
	 */
	private boolean checkedForHasNoSinkAnalyzerAnalyzer = false;
	private PerNodeHasNoSinkTimesAnalyzer perNodeHasNoSinkAnalyzer = null;

    /**
     * True, if a {@link PerNodeRoleTimesAnalyzer} is provided.
     *
     * @return
     */
    public boolean hasStateAnalyzer()
    {
        if (perNodeHasNoSinkAnalyzer == null && !checkedForHasNoSinkAnalyzerAnalyzer)
        {
            getNoSinkAnalyzer();
        }
        return perNodeHasNoSinkAnalyzer != null;
    }

	public PerNodeHasNoSinkTimesAnalyzer getNoSinkAnalyzer() {
		if (!checkedForHasNoSinkAnalyzerAnalyzer) {
			try {
				perNodeHasNoSinkAnalyzer = Monitor.get(PerNodeHasNoSinkTimesAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				perNodeHasNoSinkAnalyzer = null;
			}
			checkedForHasNoSinkAnalyzerAnalyzer = true;
		}
		return perNodeHasNoSinkAnalyzer;
	}
}
