package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectImpl;

/**
 * Component that holds updates of other Node's speed updates. It also computes
 * the clusterSpeed, which is no longer done at the Routing <br>
 * This is only active when the node in question is a Sink and the
 * Unicast/Broadcast decision is done on the sink.
 * 
 * @author Christoph Storm
 * @version 1.0 1016.07.21
 *
 */
public class ClusterSpeedComputation {

	// What value to use/compute when generating the Node-ClusterSpeed as sink
	// or cloudlet
	public enum computationStrategy {
		AVG, MEDIAN
	}

	private static computationStrategy speedComputationStrategy = computationStrategy.AVG;
	private static double removeOutliers = 0.1;

	private CraterNodeComponent parent;
	private boolean isNotDummy;
	private LinkedList<DataObjectImpl<Double>> nodeSpeeds = new LinkedList<DataObjectImpl<Double>>();

	private PeriodicOperation<CraterNodeComponent, Object> computeSpeedOperation;

	public ClusterSpeedComputation(CraterNodeComponent parent, boolean isNotDummy, long interval) {
		this.parent = parent;
		this.isNotDummy = isNotDummy;
		init(interval);
	}

	private void init(long interval) {
		if (isNotDummy) {
			// TODO implement!
			computeSpeedOperation = new PeriodicOperation<CraterNodeComponent, Object>(parent,
					Operations.getEmptyCallback(), interval) {
				@Override
				protected void executeOnce() {
					double speed = computeLastClusterSpeed();
					nodeSpeeds.clear();
					parent.getSinkAdvertising().setLastSpeedUpdate(speed);
				}
				@Override
				public Object getResult() {
					return null;
				}
			};
		}
		else {
			// generate a dummy do-NothingOperation
			computeSpeedOperation = new PeriodicOperation<CraterNodeComponent, Object>(parent,
					Operations.getEmptyCallback(), 0) {
				@Override
				protected void executeOnce() {
					this.stop();
				}

				@Override
				public Object getResult() {
					return null;
				}
			};
		}
	}

	/**
	 * Computes this sinks cluster-speed via the given strategy and leaf-speeds
	 * 
	 * @param strat
	 * @param removeOutliers
	 *            at most this percentage of entries are removed from the high
	 *            and low end of received speed-updates before computing the
	 *            cluster speed
	 * @return the cluster's speed
	 * 
	 * @author Christoph Storm
	 */
	public double computeLastClusterSpeed() {
		double lastClusterSpeed = 0d;
		// size estimate
		int num = nodeSpeeds.size() + 1;
		ArrayList<Double> speeds = new ArrayList<Double>(num);
		HashSet<UniqueID> ids = new HashSet<UniqueID>();

		for (DataObjectImpl<Double> next : nodeSpeeds) {
			// Count each update only once!
			if (!ids.contains(next.getUniqueDataObjectID())) {
				speeds.add(next.getValue());
				ids.add(next.getUniqueDataObjectID());
			}
		}

		// add own speed...
		speeds.add(parent.getLocationListener().getLastSpeedEstimate());

		// speeds now includes all speed updates
		num = speeds.size();
		Collections.sort(speeds);

		if (removeOutliers > 0) {
			// remove outliers

			int toRemove = (int) (num * removeOutliers);

			// removing symmetrical around center
			while (toRemove > 0) {
				speeds.remove(0);
				speeds.remove(speeds.size() - 1);
				toRemove--;
			}
			num = speeds.size();
		}
		switch (speedComputationStrategy) {
		case AVG:
			for (double d : speeds) {
				lastClusterSpeed += d;
			}
			lastClusterSpeed = lastClusterSpeed / num;

		case MEDIAN:
			lastClusterSpeed = speeds.get(num / 2);
		}
		return lastClusterSpeed;
	}

	public void addSpeedUpdate(DataObjectImpl<Double> speed) {
		if (isNotDummy) {
			nodeSpeeds.add(speed);
		}
	}

	public void addSpeedUpdates(List<DataObjectImpl<Double>> speeds) {
		if (isNotDummy) {
			nodeSpeeds.addAll(speeds);
		}
	}

	public void start() {
		computeSpeedOperation.scheduleWithDelay(computeSpeedOperation.getInterval());
		// TODO anything else?
	}

	public void stop() {
		nodeSpeeds.clear();
		computeSpeedOperation.stop();
		// TODO implement!
	}

	public static void setRemoveOutliers(double ro) {
		assert ro >= 0 : "Outlier value must be larger than or equal 0!";
		assert ro <= 0.5 : "Outlier value must be smaler than or equal 0.5";
		removeOutliers = ro;
	}

	public static void setSpeedComputationStrategy(computationStrategy scs) {
		speedComputationStrategy = scs;
	}

}
