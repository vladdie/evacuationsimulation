package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.configuration;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent.CraterNodeType;

/**
 * Helper class for the configuration of {@link CraterComponent}s. This
 * configuration is passed to the Factory for CraterNodes to ensure a proper
 * configuration of all hosts.
 * 
 * @author Nils Richerzhagen
 *
 */
public interface CraterConfiguration {

	public void configure(CraterComponent comp, CraterNodeType type);
}
