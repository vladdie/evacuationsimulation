package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation;

public abstract class AbstractDuplicateInsensitiveAggregationFunction implements DuplicateInsensitiveAggregationFunction<Double> {

	private double currentAggregateValue;

	public AbstractDuplicateInsensitiveAggregationFunction(Double value) {
		this.currentAggregateValue = value;
	}

	@Override
	public Double getAggregateValue() {
		return currentAggregateValue;
	}

	/**
	 * <b>Caution</b> only to be used by the
	 * {@link DuplicateInsensitiveAggregationFunction} itself to refresh the
	 * AggregateValue whenever needed.
	 * 
	 * @param value
	 */
	protected void setAggregateValue(Double value) {
		this.currentAggregateValue = value;
	}
}
