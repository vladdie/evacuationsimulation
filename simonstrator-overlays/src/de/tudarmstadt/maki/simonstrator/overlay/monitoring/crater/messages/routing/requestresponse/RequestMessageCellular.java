package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterFactory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithUniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request;

public class RequestMessageCellular extends AbstractOverlayMessage implements CraterCellMessage, MessageWithUniqueID {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4554401138822874466L;

	private Request request;
	private UniqueID id;

	public RequestMessageCellular(OverlayContact sender, OverlayContact receiver, Request request, int ttl) {
		super(sender, receiver);
		this.request = request;
		id = CraterFactory.getMessageIDFromLong((long) request.hashCode());
	}

	public RequestMessageCellular(OverlayContact sender, OverlayContact receiver, RequestMessageLocal receivedMsg) {
		super(sender, receiver, receivedMsg);
		this.request = receivedMsg.getRequest();
		this.id = receivedMsg.getUniqueMsgID();
	}

	public RequestMessageCellular(OverlayContact sender, OverlayContact receiver, RequestMessageCellular receivedMsg) {
		super(sender, receiver, receivedMsg);
		this.request = receivedMsg.getRequest();
		this.id = receivedMsg.getUniqueMsgID();
	}

	public Request getRequest() {
		return request;
	}

	@Override
	public long getSize() {
		long size = super.getSize();

		size += id.getTransmissionSize();

		// How large is a request?
		// strings (?) attribute + type (4 each? == +8)
		// longs interval + validity; 8 each = +16
		// NodeConditions ???
		// -Parameter, comparator, value
		// +4 +2 +16
		// INodeID sender...

		size += 8; // attribute + type;
		size += 16; // interval + validity
		size += request.getTarget().size() * 22;
		size += request.getSource().getTransmissionSize();

		return size;
	}

	@Override
	public Message getPayload() {
		return this;
	}

	@Override
	public UniqueID getUniqueMsgID() {
		return id;
	}

}
