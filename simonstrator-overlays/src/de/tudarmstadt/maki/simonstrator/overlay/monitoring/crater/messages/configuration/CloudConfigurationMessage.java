package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.configuration;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterCellMessage;

/**
 * Cloud configuring the {@link CraterNodeComponent}s in the network of a role
 * change from Sink to Leaf.
 * 
 * FIXME Maybe also include additional Ctrl-Information that influences lokal
 * behavior.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CloudConfigurationMessage extends AbstractOverlayMessage implements
		CraterCellMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5551715404703916221L;

	private CraterNodeRole nodeRole;
	
	public CloudConfigurationMessage(OverlayContact sender, OverlayContact receiver, CraterNodeRole nodeRole) {
		super(sender, receiver);
		this.nodeRole = nodeRole;

	}

	@Override
	public Message getPayload() {
		return null;
	}

	public CraterNodeRole getNodeRole() {
		return nodeRole;
	}

	@Override
	public long getSize() {
		// FIXME Size
		return super.getSize();
	}
}
