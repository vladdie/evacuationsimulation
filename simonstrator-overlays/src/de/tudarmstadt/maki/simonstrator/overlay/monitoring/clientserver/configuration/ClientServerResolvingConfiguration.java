package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.configuration;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent.ClientServerNodeType;

/**
 * 
 * Configuration for the resolving strategies within ClientServer.
 * 
 * TODO Which resolving strategies are to be used in ClientServer?
 * 
 * FULL Global Knowledge (directly accessing the relevant components?)
 * 
 * Request Global Knowledge: distributing request via global knowledge?
 * collecting data via clientServer?
 * 
 * Collecting global knowledge: vice versa to the upper one
 * 
 * Reconfigurable Monitoring: Distributing and handling request in network,
 * collecting data
 * 
 * 
 * 
 * @author Nils Richerzhagen
 *
 */
public class ClientServerResolvingConfiguration implements ClientServerConfiguration {

	@Override
	public void configure(ClientServerComponent comp, ClientServerNodeType type) {
		// TODO

		switch (type) {
		case CLOUD:

			break;

		case MOBILE_NODE:

			break;
		default:
			throw new AssertionError("No valid node type.");
		}
	}

}
