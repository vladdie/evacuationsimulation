package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.DuplicateSensitiveAggregationFunction;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.MergeNotPossibleException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainer.MergePoint;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 * 
 * @param <T>
 */
public interface DataObjectAggregationDupSensitive<T> extends DataObjectAggregation<T> {

	/**
	 * 
	 * @param toAddEntry
	 *            - <key> craterNodeId, <value> T value
	 */
	public void putValue(AggregationFunctionEntry toAddEntry, MergePoint mergePoint);

	/**
	 * Returns a {@link LinkedList} of all {@link AggregationFunctionEntry}s
	 * ever added to the included {@link DuplicateSensitiveAggregationFunction}.
	 * 
	 * <b>CAUTION</b> use this only for merge!
	 * 
	 * @return
	 */
	public LinkedList<AggregationFunctionEntry> getAllIncludedEntriesForMerge();

	/**
	 * Returns a {@link LinkedList} of the {@link AggregationFunctionEntry}s
	 * currently used to calculate the aggregate of the
	 * {@link DuplicateSensitiveAggregationFunction}.
	 * 
	 * Thus, the included entries are the freshest entries from each node heard.
	 * 
	 * @return
	 */
	public LinkedList<AggregationFunctionEntry> getCurrentUsedEntries();

	/**
	 * For statistics.
	 * 
	 * @return
	 */
	public LinkedList<UniqueID> getIncludedEntriesIDs();

	/**
	 * Merge the values of the given {@link DataObjectAggregationDupSensitive}
	 * to the current object.
	 * 
	 * @param toMergeDataObject
	 * @param mergePoint
	 *            - where is the data merged 'SINK' - 'CENTRAL'
	 * @throws MergeNotPossibleException
	 */
	public void mergeValues(DataObjectAggregationDupSensitive<T> toMergeDataObject, MergePoint mergePoint) throws MergeNotPossibleException;

	/**
	 * Returns the size of that {@link DataObjectAggregationDupSensitiveImpl} in
	 * a message.
	 * 
	 * @return
	 */
	public long getMessageSize();
}
