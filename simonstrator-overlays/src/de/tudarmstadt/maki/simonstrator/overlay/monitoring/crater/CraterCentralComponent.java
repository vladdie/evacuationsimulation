package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.AccuracyAndCompletenessAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.RequestResponseAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.cloudlet.CraterCloudletComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway.CraterGatewaySelection;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.gateway.GatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterBackendMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerUploadImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.ResponseObject;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.CellularResponseMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestMessageCellular;

/**
 * Central simple data collection entity that directly forwards to analyzers for statistics.
 * 
 * - calculates Gateways in current network situation
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 06.08.2014
 * 
 */
public class CraterCentralComponent extends CraterComponent {

	private int numCloudConnections = -1;

	private CraterGatewaySelection cgLogicController;
	// private OverlordController gatewayOverlord;

	/**
	 * Ensure that simulator has initialized all components so that message is received.
	 */
	private static final long _GATEWAY_SELECTION_DELAY = 20 * Time.SECOND;
	// TODO move this to OverlordController

	// private OverlayContacts overlayContacts;

	public CraterCentralComponent(Host host) {
		super(host, CraterNodeType.CLOUD);
		cgLogicController = new CraterGatewaySelection(this);
		Monitor.log(CraterCentralComponent.class, Monitor.Level.INFO, "CraterCentralComponent: ID: " + host.getId());
	}

	@Override
	public void initialize() {
		super.initialize();
		assert numCloudConnections != -1;
		// assert _GATEWAY_SELECTION_INTERVAL != -1;
		// inializePeriodicOperations();
		cgLogicController.startWithDelay(_GATEWAY_SELECTION_DELAY);
	}

	/**
	 * Set the maximum allowed number of connections to the {@link CraterCentralComponent} via the configuration.
	 * 
	 * @param numCloudConnections
	 */
	public void setNumCloudConnections(int numCloudConnections) {
		this.numCloudConnections = numCloudConnections;
	}

	/**
	 * 
	 * @param gatewaySelectionInterval
	 */
	public void setGatewaySelectionInterval(long gatewaySelectionInterval) {
		cgLogicController.setGatewaySelectionInterval(gatewaySelectionInterval);
	}

	/**
	 * Configures the gateway logic for this cloud component.
	 * 
	 * @param gatewayLogic
	 */
	public void setGatewayLogic(GatewayLogic gatewayLogic) {
		assert gatewayLogic != null;
		cgLogicController.setGatewayLogic(gatewayLogic);
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		assert msg instanceof CraterCellMessage || msg instanceof CraterBackendMessage;

		// System.out.println("Message arrived on central entity");
		if (msg instanceof DataMessageContainerUploadImpl) {
			DataMessageContainerUploadImpl dataContainer = (DataMessageContainerUploadImpl) msg;

			///////////////////////////////////////////////////
			// #A.0 -> ResponseDataHandling
			handleResponses(dataContainer.getQueryResponseList());

			/*
			 * - #A.1# upload msg internals to analyzer for duplicate detection of real data.
			 */
			if (hasDataRoutingAnalyzer()) {
				getDataRoutingAnalyzer().uploadMsgReceived(dataContainer.getUniqueMsgID());
				// // #A.1#
				LinkedList<UniqueID> receivedNormalDataObjectIDs = new LinkedList<UniqueID>();
				for (DataObjectImpl<Double> actDataObject : dataContainer.getNormalDataList()) {
					receivedNormalDataObjectIDs.add(actDataObject.getUniqueDataObjectID());
				}

				getDataRoutingAnalyzer().onMessageContentReceivedAtServer(receivedNormalDataObjectIDs,
						dataContainer.getAggregationDupInsensitiveDataList(), dataContainer.getAggregationDupSensitiveDataList());
			}
			if (hasAccuracyAndCompletenessAnalyzer()) {
				getAccuracyAndCompletenessAnalyzer()
						.onDuplicateSensitiveAggregationDataObjectReceived(dataContainer.getAggregationDupSensitiveDataList());
			}
		}
		else if (msg instanceof RequestMessageCellular) {
			handleRequestMessage((RequestMessageCellular) msg);
		}

		// Analyzer
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onReceivedOverlayMessage((OverlayMessage) msg, getHost());
		}
	}

	private void handleRequestMessage(RequestMessageCellular receivedMsg) {
		// received an request, disseminate to each Sink, except the one that
		// uploaded it
		for (Host host : Oracle.getAllHosts()) {
			try {
				CraterComponent node = host.getComponent(CraterComponent.class);

				if (!node.getLocalOverlayContact().equals(receivedMsg.getSender())) {

					boolean fwd = false;
					if ((node instanceof CraterNodeComponent && ((CraterNodeComponent) node).isSink())
							|| node instanceof CraterCloudletComponent) {
						fwd = true;
					}

					if (fwd) {
						RequestMessageCellular reqMsg = new RequestMessageCellular(getCloudContact(),
								node.getLocalOverlayContact(), receivedMsg);
						this.sendViaCellular(reqMsg);
					}
				}

			} catch (ComponentNotAvailableException e) {
				// do nothing
			}
		}
	}

	private void handleResponses(List<ResponseObject> responses) {
		// sort responses by receiver
		HashMap<INodeID, List<ResponseObject>> perNodeResponses = new HashMap<INodeID, List<ResponseObject>>();
		for (ResponseObject response : responses) {
			if (perNodeResponses.containsKey(response.getRequesterID())) {
				perNodeResponses.get(response.getRequesterID()).add(response);
			} else {
				LinkedList<ResponseObject> list = new LinkedList<ResponseObject>();
				list.add(response);
				perNodeResponses.put(response.getRequesterID(), list);
			}

			// for Analyzer
			if (hasRequestResponseAnalyzer()) {
				getRequestResponseAnalyzer().onResponseReceivedAtCentralEnetity(response);
			}

		}

		// send responses to the receivers
		for (INodeID receiver : perNodeResponses.keySet()) {
			// CHEAT! Use global knowledge to get the overlay contact
			try {
				OverlayContact reccon = Oracle.getHostByID(receiver).getComponent(CraterNodeComponent.class)
						.getLocalOverlayContact();

				CellularResponseMessage rmsg = new CellularResponseMessage(getCloudContact(), reccon,
						perNodeResponses.get(receiver));
				// FIXME Only send if node is online?
				this.sendViaCellular(rmsg);

			} catch (ComponentNotAvailableException e) {
				// Could as well throw a RuntimeException here, this is not
				// supposed to happen!
			}
		}
	}

	public int getMaxCloudConnections() {
		return numCloudConnections;
	}

	public CraterGatewaySelection getCentralGatewayLogicController() {
		return cgLogicController;
	}

	/*
	 * Analyzing convenience methods.
	 */
	private boolean _checkedForDataRoutingAnalyzer = false;

	private DataRoutingAnalyzer _dataRoutingAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasDataRoutingAnalyzer() {
		if(_dataRoutingAnalyzer == null && !_checkedForDataRoutingAnalyzer)
		{
			getDataRoutingAnalyzer();
		}
		return _dataRoutingAnalyzer != null;
	}

	public DataRoutingAnalyzer getDataRoutingAnalyzer() {
		if (!_checkedForDataRoutingAnalyzer) {
			try {
				_dataRoutingAnalyzer = Monitor.get(DataRoutingAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				_dataRoutingAnalyzer = null;
			}
			_checkedForDataRoutingAnalyzer = true;
		}
		return _dataRoutingAnalyzer;
	}

	/*
	 * Analyzing convenience methods.
	 */
	private boolean _checkedForAccuracyAndCompletenessAnalyzer = false;

	private AccuracyAndCompletenessAnalyzer _accuracyAndCompletenessAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasAccuracyAndCompletenessAnalyzer() {
		if (_accuracyAndCompletenessAnalyzer == null && !_checkedForAccuracyAndCompletenessAnalyzer) {
			getAccuracyAndCompletenessAnalyzer();
		}
		return _accuracyAndCompletenessAnalyzer != null;
	}

	public AccuracyAndCompletenessAnalyzer getAccuracyAndCompletenessAnalyzer() {
		if (!_checkedForAccuracyAndCompletenessAnalyzer) {
			try {
				_accuracyAndCompletenessAnalyzer = Monitor.get(AccuracyAndCompletenessAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				_accuracyAndCompletenessAnalyzer = null;
			}
			_checkedForAccuracyAndCompletenessAnalyzer = true;
		}
		return _accuracyAndCompletenessAnalyzer;
	}

	private boolean _checkedForRequestResponseAnalyzer = false;
	private RequestResponseAnalyzer _requestAnalyzer = null;

	public boolean hasRequestResponseAnalyzer() {
		if (_requestAnalyzer != null) {
			return true;
		} else if (_checkedForRequestResponseAnalyzer) {
			return false;
		}
		getRequestResponseAnalyzer();
		return _requestAnalyzer != null;
	}

	public RequestResponseAnalyzer getRequestResponseAnalyzer() {
		if (!_checkedForRequestResponseAnalyzer) {
			try {
				_requestAnalyzer = Monitor.get(RequestResponseAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				_requestAnalyzer = null;
			}
			_checkedForRequestResponseAnalyzer = true;
		}
		return _requestAnalyzer;
	}
}