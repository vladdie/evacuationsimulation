package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationListener;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;

/**
 * Barebone implementation of a location listener. Is supposed to (eventually)
 * hold estimated speed(s), and/or last locations plus a few helper functions.
 * 
 * @author Christoph Storm
 */
public class CraterLocationListener implements LocationListener {

	private CraterNodeComponent parent;
	private boolean updateParent;
	private long lastTimestamp;
	private Location lastLocation;
	private double lastSpeed = 0;

	public CraterLocationListener(CraterNodeComponent host, boolean updateParent) {
		this.parent = host;
		this.updateParent = updateParent;
	}

	@Override
	public void onLocationChanged(Host host, Location location) {

		long updateTime = Time.getCurrentTime();

		if (lastLocation != null) {
			double distance = location.distanceTo(lastLocation);
			long tDiff = (updateTime - lastTimestamp) / Time.SECOND;
			double curSpeed = distance / tDiff;
			lastSpeed = curSpeed;

			if (updateParent) {
				parent.updateSpeedEstimate(curSpeed);
			}
		}
		lastTimestamp = updateTime;
		lastLocation = location;
	}

	public double getLastSpeedEstimate() {
		return lastSpeed;
	}

	public long getLastTimestamp() {
		return lastTimestamp;
	}

}
