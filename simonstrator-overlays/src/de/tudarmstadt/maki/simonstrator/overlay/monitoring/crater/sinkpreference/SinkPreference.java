package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference;

import java.util.AbstractMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * Interface different criteria for selecting a sink
 * 
 * @author Nils Richerzhagen
 *
 */
public interface SinkPreference {

	/**
	 * Method that return the best sink from the sink table
	 * 
	 * @return The best sink depending on the set criteria
	 */
	public AbstractMap.SimpleEntry<INodeID, Double> getBestSink();

	/**
	 * Method to change the sink table used to determine the best sink
	 * 
	 * @param unmodifiableSinkTable
	 *            The new sink table
	 */
	public void setNewSinkTable(Map<INodeID, SinkTableEntry> unmodifiableSinkTable);

	/**
	 * Enumeration, containing all implemented types of the AbstactSinkPreference
	 */
	public enum SinkPreferenceType {
		// NOTE: all implemented originally sort first by the number of heared
		// updates
		/*
		 * The Default to choose the best sink, which is highest GRADIENT
		 */
		DEFAULT,
		/*
		 * Choose the sink with the best QUALITY
		 */
		QUALITY,
		/*
		 * Choose a static sink if available. GRADIENT is the second criteria the sinks are sorted by.
		 */
		STATIC_GRADIENT,
		/*
		 * Choose a static sink if available. QUALITY is the second criteria the sinks are sorted by.
		 */
		STATIC_QUALITY;
	}
}
