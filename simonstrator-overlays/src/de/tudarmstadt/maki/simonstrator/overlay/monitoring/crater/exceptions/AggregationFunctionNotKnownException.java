package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 *
 */
public class AggregationFunctionNotKnownException extends Exception{

	/**
	 * The used aggregation function is not known by the Object. 
	 */
	private static final long serialVersionUID = 1L;

	public AggregationFunctionNotKnownException(String description) {
		super("The used aggregation function is not known: " + description);
	}
}
