package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerFactory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.DuplicateInsensitiveAggregationFunction.DuplicateInsensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.DuplicateSensitiveAggregationFunction.DuplicateSensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.exceptions.AggregationFunctionNotKnownException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.MessageWithUniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.routing.DataRoutingImpl;

/**
 * {@link DataMessageContainer} implementation only for the one-hop upload
 * communication from sinks to the cellular infrastructure. It includes the
 * normal normal data in {@link DataObject}, duplicate insensitive aggregates in
 * {@link DataObjectAggregationDupInsensitiveImpl}, and duplice sensitve
 * aggregates in {@link DataObjectAggregationDupSensitiveImpl}.
 * 
 * Furthermore the basic fields: sinkId (long)
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 07.08.2014
 * 
 */
public class DataMessageContainerUploadImpl extends AbstractOverlayMessage implements DataMessageContainer, MessageWithUniqueID, ClientServerCellMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private UniqueID uniqueMsgId;

	private LinkedList<DataObjectImpl<Double>> normalData = new LinkedList<DataObjectImpl<Double>>();
	private LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveData = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();
	private LinkedList<DataObjectAggregationDupSensitiveImpl> aggregationDupSensitiveData = new LinkedList<DataObjectAggregationDupSensitiveImpl>();

	/**
	 * Initial constructor to add complete buffers of that sink into the {@link DataMessageContainerUploadImpl}.
	 * 
	 * @param sender
	 * @param receiver
	 * @param normalData
	 * @param aggregationDupInsensitiveData
	 * @param aggregationDupSensitiveData
	 */
	public DataMessageContainerUploadImpl(OverlayContact sender, OverlayContact receiver, LinkedList<DataObjectImpl<Double>> normalData,
			LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveData,
			LinkedList<DataObjectAggregationDupSensitiveImpl> aggregationDupSensitiveData) {
		super(sender, receiver);
		this.uniqueMsgId = ClientServerFactory.getRandomMessageId();
		this.normalData = normalData;
		this.aggregationDupInsensitiveData = aggregationDupInsensitiveData;
		this.aggregationDupSensitiveData = aggregationDupSensitiveData;
	}
	
	/**
	 * To add normale data in form of {@link DataObjectImpl}.
	 * 
	 * @param msg
	 * @param actNodeId
	 * @param attributeId
	 * @param value
	 */
	public DataMessageContainerUploadImpl(DataMessageContainerUploadImpl msg, INodeID actNodeId, int attributeId, double value) {
		super(msg.getSender(), msg.getReceiver(), msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();

		DataObjectImpl<Double> normalDataObject = new DataObjectImpl<Double>(attributeId, value, actNodeId);
		normalData.add(normalDataObject);

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onNormalDataObjectAddedAtSink(normalDataObject.getUniqueDataObjectID());
		}
	}

	/**
	 * Copy-constructor + additional
	 * {@link DataObjectAggregationDupInsensitiveImpl} or
	 * {@link DataObjectAggregationDupSensitiveImpl} according to the given
	 * aggregationFunctionId for data sending in {@link DataRoutingImpl}.
	 * 
	 * @param msg
	 *            - the original message.
	 * @param aggregationFunctionId
	 *            - the identifier of the aggregation function.
	 * @param attributeId
	 * @param toAddEntry
	 *            - in case of {@link DataObjectAggregationDupInsensitiveImpl}
	 *            adding full <key> clientServerNodeId, <value> value entry. In
	 *            case of {@link DataObjectAggregationDupSensitiveImpl} adding
	 *            only the <value> of the entry as key is irrelevant.
	 * @throws AggregationFunctionNotKnownException
	 */
	public DataMessageContainerUploadImpl(DataMessageContainerUploadImpl msg, String aggregationFunctionId, int attributeId,
			AggregationFunctionEntry toAddEntry) throws AggregationFunctionNotKnownException {
		super(msg.getSender(), msg.getReceiver(), msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();

		if (DuplicateSensitiveAggregationFunctionsDescriptions.MEAN.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT_NOREF.getId().equals(aggregationFunctionId)) {

			putDupSensitiveData(aggregationFunctionId, attributeId, toAddEntry);
		} else if (DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.getId().equals(aggregationFunctionId)
				|| DuplicateInsensitiveAggregationFunctionsDescriptions.MIN.getId().equals(aggregationFunctionId)) {

			putDupInsensitiveData(aggregationFunctionId, attributeId, toAddEntry.getValue());
		} else {
			throw new AggregationFunctionNotKnownException(aggregationFunctionId);
		}
	}

	/**
	 * Add value either to existing aggregate or add new
	 * {@link DataObjectAggregationDupInsensitiveImpl} with the new aggregate
	 * for the specific attributeId.
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param value
	 * @throws AggregationFunctionNotKnownException
	 */
	private void putDupInsensitiveData(String aggregationFunctionId, int attributeId, double value)
			throws AggregationFunctionNotKnownException {
		boolean entryExisted = false;
		for (DataObjectAggregationDupInsensitive<Double> actDataObject : aggregationDupInsensitiveData) {
			if (actDataObject.getAggregationFunctionId().equals(aggregationFunctionId) && actDataObject.getAttributeId() == attributeId) {
				actDataObject.putValue(value);
				entryExisted = true;
			}
		}

		if (!entryExisted) {
			DataObjectAggregationDupInsensitiveImpl aggregationDupInsensitvieDataObejct = new DataObjectAggregationDupInsensitiveImpl(
					aggregationFunctionId, attributeId, value);
			this.aggregationDupInsensitiveData.add(aggregationDupInsensitvieDataObejct);
		}

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onDuplicateInsensitiveAggregationDataAddedAtSink(aggregationFunctionId, attributeId, value);
		}
	}

	/**
	 * Add value either to existing aggregate or add new
	 * {@link DataObjectAggregationDupSensitiveImpl} with the new aggregate for
	 * the specific attributeId.
	 * 
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param toAddEntry
	 *            - <key> clientServerNodeId, <value> value
	 * @throws AggregationFunctionNotKnownException
	 */
	private void putDupSensitiveData(String aggregationFunctionId, int attributeId, AggregationFunctionEntry toAddEntry)
			throws AggregationFunctionNotKnownException {
		boolean entryExisted = false;
		for (DataObjectAggregationDupSensitiveImpl actDataObject : aggregationDupSensitiveData) {
			if (actDataObject.getAggregationFunctionId().equals(aggregationFunctionId) && actDataObject.getAttributeId() == attributeId) {
				actDataObject.putValue(toAddEntry, MergePoint.IN_NETWORK);
				entryExisted = true;
			}
		}

		if (!entryExisted) {
			DataObjectAggregationDupSensitiveImpl aggregationDupSensitvieDataObejct = new DataObjectAggregationDupSensitiveImpl(
					aggregationFunctionId, attributeId, toAddEntry);
			this.aggregationDupSensitiveData.add(aggregationDupSensitvieDataObejct);
		}

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onDuplicateSensitiveAggregationEntryAddedAtSink(toAddEntry.getUniqueEntryID());
		}

	}
	
	@Override
	public long getSize() {
		/*
		 * Normal data: attributeId +2, nodeId +2, value +8
		 * 
		 * Dup Insens: short attributeId +2, byte aggregationFunction, double currentValue +8
		 * 
		 * Dup Sens: short attributeId +2, byte aggregationFunction, map<double values, short nodeId> +10
		 */
		long bytesNormalData = normalData.size() * 14;
		long bytesAggDupInsens = aggregationDupInsensitiveData.size() * 10;
		long bytesAggDupSens = 0;
		for (DataObjectAggregationDupSensitiveImpl actDupSensitiveObject : aggregationDupSensitiveData) {
			bytesAggDupSens += 2; // attributeID
			bytesAggDupSens += actDupSensitiveObject.getIncludedEntriesIDs().size() * 10; // +10 for each entry in aggFunc
		}
		return super.getSize() + bytesNormalData + bytesAggDupInsens + bytesAggDupSens;
	}
	
	@Override
	public double getFilledPercentage() {
		return 0;
	}

	@Override
	public LinkedList<DataObjectImpl<Double>> getNormalDataList() {
		return normalData;
	}

	@Override
	public LinkedList<DataObjectAggregationDupInsensitiveImpl> getAggregationDupInsensitiveDataList() {
		return aggregationDupInsensitiveData;
	}

	@Override
	public LinkedList<DataObjectAggregationDupSensitiveImpl> getAggregationDupSensitiveDataList() {
		return aggregationDupSensitiveData;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public UniqueID getUniqueMsgID() {
		return uniqueMsgId;
	}
	
	/*
	 * Analyzing convenience methods.
	 */
	private boolean checkedForDataRoutingAnalyzer = false;

	private DataRoutingAnalyzer dataRoutingAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	private boolean hasDataRoutingAnalyzer() {
		if (dataRoutingAnalyzer != null) {
			return true;
		} else if (checkedForDataRoutingAnalyzer) {
			return false;
		}
		getDataRoutingAnalyzer();
		return dataRoutingAnalyzer != null;
	}

	private DataRoutingAnalyzer getDataRoutingAnalyzer() {
		if (!checkedForDataRoutingAnalyzer) {
			try {
				dataRoutingAnalyzer = Monitor.get(DataRoutingAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				dataRoutingAnalyzer = null;
			}
			checkedForDataRoutingAnalyzer = true;
		}
		return dataRoutingAnalyzer;
	}
}
