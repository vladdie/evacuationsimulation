package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.sinksendinginterval;

import de.tudarmstadt.maki.simonstrator.api.Time;

/**
 * Computes the given interval so that the average-speed node covers 45m before
 * the end of the interval. For Nodespeeds slower than 0.5 m/s, the interval
 * length is increased by 3s for each 0.1 m/s slower until a max of 100 seconds
 * 
 * @author Christoph Storm
 *
 */
public class DefaultInterval implements SinkAdvIntervalModifier {
	// TODO rename

	@Override
	public long getNextInterval(double speedOfNodes) {

		if (speedOfNodes < 0.2) {
			return 100 * Time.SECOND;
		} else if (speedOfNodes < 0.5) {
			// base 90s interval;
			// adds a 3s delay for each 0.1 m/s slower than 0.5 m/s
			double base = (0.5 - speedOfNodes) * 30;
			return (90 + (long) base) * Time.SECOND;
		} else {
			// speed somewhere between .5 m/s and 8 m/s
			// reaches 15s interval at a speed of 3m/s
			double base = 45 / speedOfNodes;
			return (long) base * Time.SECOND;
		}
	}

	@Override
	public long getMaxInterval() {
		return 100 * Time.SECOND;
	}

}
