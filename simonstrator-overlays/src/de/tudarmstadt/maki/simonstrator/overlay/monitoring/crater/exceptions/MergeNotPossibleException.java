package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupSensitive;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 14.07.2014
 *
 */
public class MergeNotPossibleException extends Exception{
	/**
	 * The merge of the {@link DataObjectAggregationDupSensitive} is not possible.. 
	 */
	private static final long serialVersionUID = 1L;

	public MergeNotPossibleException() {
		super("The merge of the two objects is not possible.");
	}
}
