package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising;

import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterFactory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithGradient;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithTTL;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithUniqueID;

/**
 * Class for sink advertising mechanism messages.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 09.06.2014
 * 
 */
public class SinkAdvertisingMessage extends AbstractOverlayMessage
		implements CraterLocalMessage, MessageWithUniqueID, MessageWithTTL, MessageWithGradient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1908742962821118559L;

	private UniqueID uniqueMsgId;

	private int gradient;

	private int TTL;

	private INodeID craterNodeId;

	private int sinkQuality;

	private boolean sendUnicast; // 1 bit?

	private boolean includesRequestHashes;
	private Set<Integer> requestHashes;

	/**
	 * Constructor for sink advertising message.
	 * 
	 * @param sender
	 * @param receiver
	 * @param gradient
	 * @param TTL
	 */
	public SinkAdvertisingMessage(OverlayContact sender, INodeID craterNodeId, int gradient, int TTL, int sinkQuality,
			boolean sendUnicast, boolean includesRequestHashes, Set<Integer> requestHashes) {
		super(sender, null);
		this.uniqueMsgId = CraterFactory.getRandomMessageId();
		this.craterNodeId = craterNodeId;
		this.gradient = gradient;
		this.TTL = TTL;
		this.sinkQuality = sinkQuality;
		this.sendUnicast = sendUnicast;
		this.includesRequestHashes = includesRequestHashes;
		this.requestHashes = requestHashes;
	}

	/**
	 * Constructor for forwarding. Decreases automatically TTL and gradient both
	 * by 1!
	 * 
	 * @param sender
	 * @param receivedMsg
	 */
	public SinkAdvertisingMessage(SinkAdvertisingMessage receivedMsg) {
		super(receivedMsg.getSender(), null, receivedMsg);
		this.uniqueMsgId = receivedMsg.uniqueMsgId;
		this.TTL = receivedMsg.TTL - 1;
		this.gradient = receivedMsg.gradient - 1;
		this.craterNodeId = receivedMsg.getSinkId();
		this.sinkQuality = receivedMsg.getSinkQuality();
		this.sendUnicast = receivedMsg.isSendUnicast();
		this.includesRequestHashes = receivedMsg.isIncludesRequestHashes();
		this.requestHashes = receivedMsg.getRequestHashes();
		assert TTL >= 0;
	}

	/**
	 * Yet another constructor, to be called directly before forwarding. Does
	 * not reduce ttl or gradient, as the other one already does that at
	 * arrival.
	 * 
	 * @param sender
	 *            -the sending node
	 * @param receivedMsg
	 */
	public SinkAdvertisingMessage(OverlayContact sender, SinkAdvertisingMessage receivedMsg) {
		super(sender, null, receivedMsg);
		this.uniqueMsgId = receivedMsg.uniqueMsgId;
		this.TTL = receivedMsg.TTL;
		this.gradient = receivedMsg.gradient;
		this.craterNodeId = receivedMsg.getSinkId();
		this.sinkQuality = receivedMsg.getSinkQuality();
		this.sendUnicast = receivedMsg.isSendUnicast();
		this.includesRequestHashes = receivedMsg.isIncludesRequestHashes();
		this.requestHashes = receivedMsg.getRequestHashes();
		assert TTL >= 0;
	}

	@Override
	public long getSize() {
		// In Protocol: 4 bit gradient 0-10; 4 bit TTL 0-10, short craterNodeId
		// (65536), min 7 bit sinkQuality 0-100
		long size = super.getSize() + 1 + 2 + 1;

		// +1 for two bools: sendUnicast && includesRequestHashes (remainder is
		// requestHash delimeter)
		size++;

		// +4 per included request Hash
		size += requestHashes.size() * 4;

		return size;
	}

	/**
	 * Returning the sinkId = the node id in this case.
	 * 
	 * @return
	 */
	public INodeID getSinkId() {
		return craterNodeId;
	}

	/**
	 * Returning the sinkQuality. Value between [0 and 100] indicating the
	 * 'strongness' of the sink.
	 * 
	 * @return
	 */
	public int getSinkQuality() {
		return this.sinkQuality;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	public boolean isSendUnicast() {
		return sendUnicast;
	}

	@Override
	public int getGradient() {
		return this.gradient;
	}

	@Override
	public boolean deadMessage() {
		return TTL <= 0;
	}

	@Override
	public UniqueID getUniqueMsgID() {
		return uniqueMsgId;
	}

	public boolean isIncludesRequestHashes() {
		return includesRequestHashes;
	}

	public Set<Integer> getRequestHashes() {
		return requestHashes;
	}
}
