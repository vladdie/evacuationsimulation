package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.cloudlet;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.configuration.CloudConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.CloudletUploadContainer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataObjectAggregationDupInsensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataObjectAggregationDupSensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataObjectImpl;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * Component that is placed on infrastructure entities in the "edge network". Thus this is located on Wi-Fi Access
 * Points, Cellular Towers etc.
 * 
 * @author Nils Richerzhagen
 */
public class ClientServerCloudletComponent extends ClientServerComponent {

	CloudletAdvertisingImpl cloudletAdvertising;
	private int INITIAL_TTL;
	private long _CLOUDLET_ADV_INTERVAL;

	private TimeoutSet<UniqueID> receivedDataMessagesContainer = new TimeoutSet<UniqueID>(3 * Time.MINUTE);

	/*
	 * Buffers for detecting duplicates or already uploaded data of received ContainerMessage contents.
	 */
	private LinkedList<UniqueID> uploadDetector_receivedNormalDataObjects = new LinkedList<UniqueID>();

	public ClientServerCloudletComponent(Host host) {
		super(host, ClientServerNodeType.CLOUDLET);
		Monitor.log(ClientServerCloudletComponent.class, Monitor.Level.INFO, "ClientServerCloudlet: ID: " + host.getId());
	}

	@Override
	public void initialize() {
		super.initialize();

		cloudletAdvertising = new CloudletAdvertisingImpl(this, INITIAL_TTL, _CLOUDLET_ADV_INTERVAL);

		cloudletAdvertising.initialize();
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		assert msg instanceof ClientServerCellMessage || msg instanceof ClientServerLocalMessage;


		if (msg instanceof DataMessageContainerLocalImpl) {
			// copy constructor
			handleIncomingDataMessage(new DataMessageContainerLocalImpl((DataMessageContainerLocalImpl) msg));
		}

		else if (msg instanceof CloudConfigurationMessage) {
			/*
			 * nothing, currently
			 * 
			 * Maybe later TTL configuration, or frequency of adv
			 */
		}

		else {
			throw new AssertionError("Received Message of unknown type " + msg.getClass());
		}

		// Analyzer
		if (hasMessageAnalyzer()) {
			if (msg instanceof ClientServerCellMessage) {
				getMessageAnalyzer().onReceivedOverlayMessage((ClientServerCellMessage) msg, getHost());
			}
			if (msg instanceof ClientServerLocalMessage) {
				getMessageAnalyzer().onReceivedOverlayMessage((ClientServerLocalMessage) msg, getHost());
			}
		}
	}

	/**
	 * Handle incoming {@link DataMessageContainerLocalImpl} to check for duplicates and upload directly!
	 * 
	 * @param msg
	 */
	private void handleIncomingDataMessage(DataMessageContainerLocalImpl msg) {

		LinkedList<DataObjectImpl<Double>> uploadBuffer_receivedNormalDataObjects;

		// MSG is for this host
		if (msg.getSinkId() == this.getHost().getId()) {

			Monitor.log(ClientServerCloudletComponent.class, Monitor.Level.DEBUG,
					"Cloudlet: Message from " + msg.getSender().getNodeID() + " at " + msg.getSinkId());

			uploadBuffer_receivedNormalDataObjects = new LinkedList<DataObjectImpl<Double>>();

			/*
			 * Normal Data -- Detect and Upload
			 */
			LinkedList<DataObjectImpl<Double>> normalDataOfMsg = msg.getNormalDataList();
			for (DataObjectImpl<Double> actReceivedNormalDataObject : normalDataOfMsg) {
				if (uploadDetector_receivedNormalDataObjects.contains(actReceivedNormalDataObject.getUniqueDataObjectID())) {
					// duplicate --> do not upload
					if (hasDataRoutingAnalyzer()) {
						getDataRoutingAnalyzer().onNormalDataDuplicateDetectedAtSink();
					}
				} else {
					// no duplicate do upload
					uploadDetector_receivedNormalDataObjects.add(actReceivedNormalDataObject.getUniqueDataObjectID());
					uploadBuffer_receivedNormalDataObjects.add(actReceivedNormalDataObject);

					if (hasDataRoutingAnalyzer()) {
						// For latency Orig - to - Sink of NormalData
						getDataRoutingAnalyzer()
								.onNormalDataObjectReceivedAtSink(actReceivedNormalDataObject.getUniqueDataObjectID());
					}
				}
			}

			/*
			 * Analyzing
			 * 
			 * - #A.1# Received as sink multiple times a
			 * ClientServerDataContainerMessage which was intended for that
			 * sink.
			 * 
			 * - msg id contained, isSink == true, sinkId =
			 * msg.ClientServerNodeId
			 */
			if (hasDataRoutingAnalyzer()) {
				// #A.1#
				if (receivedDataMessagesContainer.contains(msg.getUniqueMsgID())) {
					getDataRoutingAnalyzer().onSuccessiveContainerMessageReceivedBySink(msg.getUniqueMsgID());
				} else if (!receivedDataMessagesContainer.contains(msg.getUniqueMsgID())) {
					getDataRoutingAnalyzer().onContainerMessageReceivedBySink(msg.getUniqueMsgID(), msg.getSinkId());
				}
			}

			receivedDataMessagesContainer.addNow(msg.getUniqueMsgID());

			CloudletUploadContainer uploadMsg = new CloudletUploadContainer(this.getLocalOverlayContact(),
					this.getCloudContact(), new LinkedList<DataObjectImpl<Double>>(uploadBuffer_receivedNormalDataObjects),
					new LinkedList<DataObjectAggregationDupInsensitiveImpl>(msg.getAggregationDupInsensitiveDataList()),
					new LinkedList<DataObjectAggregationDupSensitiveImpl>(msg.getAggregationDupSensitiveDataList()));

			this.sendViaBackend(uploadMsg);

			if (hasDataRoutingAnalyzer()) {
				getDataRoutingAnalyzer().uploadMsgSend(uploadMsg.getUniqueMsgID(), this);
			}

			// Clear Buffers
			uploadBuffer_receivedNormalDataObjects.clear();
		}
	}

	/**
	 * To be set by config.
	 * 
	 * @param initialTTL
	 */
	public void setInitialTTL(int initialTTL) {
		INITIAL_TTL = initialTTL;
	}

	/**
	 * To be set by config.
	 * 
	 * @param cloudletAdvInterval
	 */
	public void setCloudletAdvInterval(long cloudletAdvInterval) {
		this._CLOUDLET_ADV_INTERVAL = cloudletAdvInterval;
	}

	/*
	 * Analyzing convenience methods.
	 */
	private boolean checkedForDataRoutingAnalyzer = false;

	private DataRoutingAnalyzer dataRoutingAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasDataRoutingAnalyzer() {
		if (dataRoutingAnalyzer != null) {
			return true;
		} else if (checkedForDataRoutingAnalyzer) {
			return false;
		}
		getDataRoutingAnalyzer();
		return dataRoutingAnalyzer != null;
	}

	public DataRoutingAnalyzer getDataRoutingAnalyzer() {
		if (!checkedForDataRoutingAnalyzer) {
			try {
				dataRoutingAnalyzer = Monitor.get(DataRoutingAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				dataRoutingAnalyzer = null;
			}
			checkedForDataRoutingAnalyzer = true;
		}
		return dataRoutingAnalyzer;
	}

}
