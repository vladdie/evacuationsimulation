package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;

/**
 * Used as unicast Ack to DataMessages. Contains a Gradient to update the
 * DataMessage's sender grad field.
 * 
 * @author Christoph Storm
 */
public class UnicastReply extends AbstractOverlayMessage
		implements CraterLocalMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5368036995846975658L;
	private double gradient;
	// TODO I should probably ad a sinkID just in case...

	public UnicastReply(OverlayContact sender, OverlayContact receiver, double gradient) {
		super(sender, receiver);
		this.gradient = gradient;
	}

	@Override
	public long getSize() {
		// Size of parentMsg + the time-updated gradient.
		return super.getSize() + 8;
	}

	@Override
	public Message getPayload() {
		return this;
	}

	public double getGradient() {
		return gradient;
	}

}
