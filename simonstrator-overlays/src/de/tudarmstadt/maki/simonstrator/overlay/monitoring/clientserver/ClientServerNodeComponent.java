package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.NodeInformation;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.advertising.CloudletAdvertising;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.advertising.CloudletAdvertisingImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.advertising.CloudletAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.configuration.CloudConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.routing.DataRouting;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.routing.DataRoutingImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.util.ClientServerNodeRoleListener;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutMap;

/**
 * Mobile entity of ClientServer.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 06.08.2014
 * 
 */
public class ClientServerNodeComponent extends ClientServerComponent implements NodeInformation, IPeerStatusListener {

	private DataRouting dataRouting;
	private CloudletAdvertising cloudletAdvertising;

	private ClientServerNodeRole nodeRole = ClientServerNodeRole.OFFLINE;

	private List<ClientServerNodeRoleListener> nodeRoleListeners = new LinkedList<ClientServerNodeRoleListener>();

	@Deprecated
	private String measurementProviderToUse;

	/*
	 * Config Parameters
	 */
	// Data Routing
	private long DATA_UPLOAD_INTERVAL;

	private TimeoutMap<UniqueID, DataMessageContainerLocalImpl> dataMessages = new TimeoutMap<UniqueID, DataMessageContainerLocalImpl>(
			3000000);

	private TimeoutMap<UniqueID, CloudletAdvertisingMessage> cloudletAdvertisingMessages = new TimeoutMap<UniqueID, CloudletAdvertisingMessage>(
			3000000);

	public ClientServerNodeComponent(Host host) {
		super(host, ClientServerNodeType.MOBILE_NODE);
		Monitor.log(ClientServerNodeComponent.class, Monitor.Level.INFO,
				"ClientServerNodeComponent: ID: " + host.getId());
	}

	public String getMeasurementProviderToUse() {
		return measurementProviderToUse;
	}

	@Override
	public void initialize() {
		super.initialize();

		this.addPeerStatusListener(this);

		dataRouting = new DataRoutingImpl(this, DATA_UPLOAD_INTERVAL);
		cloudletAdvertising = new CloudletAdvertisingImpl(this);

		cloudletAdvertising.addSinkTableListener(dataRouting);

		/*
		 * Adding of node state listeners
		 */
		this.addClientServerNodeRoleListener(dataRouting);
		this.addClientServerNodeRoleListener(cloudletAdvertising);

		dataRouting.initialize();
		cloudletAdvertising.initialize();

		// Start as offline host? True?
		assert this.isPresent() == false;

		Monitor.log(ClientServerNodeComponent.class, Monitor.Level.INFO,
				"ClientServerNode: Initalization finished on Node " + getHost().getId().value());

	}

	/**
	 * Returns the current {@link ClientServerNodeRole} of the component.
	 * 
	 * @return
	 */
	public final ClientServerNodeRole getClientServerNodeRole() {
		return this.nodeRole;
	}

	/**
	 * 
	 * Returns true if the node is a sink. Convenience Method.
	 * 
	 * @return
	 */
	public final boolean isSink() {
		return this.nodeRole.equals(ClientServerNodeRole.SINK);
	}

	/**
	 * Consistent handling of the {@link ClientServerNodeRole} functionality
	 * switches within this method. Further reactions on functionality switches
	 * from/to sink to node and vice versa can be implemented in onBecameSink
	 * and onBecameNode.
	 * 
	 * On changes of {@link ClientServerNodeRole} always switch the
	 * {@link ClientServerNodeState} to IDLE. Only if the node is OFFLINE do
	 * ignore the switch of the ClientServerNodeState.
	 * 
	 * @param
	 */
	public final void setClientServerNodeRole(ClientServerNodeRole nodeRole) {

		/*
		 * Insanity checks - this call should come from ChurnModel trying to set nodes offline. However, those nodes are
		 * by default offline, thus ignore.
		 */
		assert this.nodeRole != nodeRole : "NodeRole must not be the same as the stored value";

		this.nodeRole = nodeRole;

		if (hasPerNodeHasNoSinkAnalyzer()) {
			getPerNodeHasNoSinkTimeAnalyzer().onClientServerNodeRoleChanged(this, nodeRole);
		}
		if (hasPerNodeStateTimeAnalyzer()) {
			getPerNodeStateTimeAnalyzer().onClientServerNodeRoleChanged(this, nodeRole);
		}
		// System.out.println(getHost().getId().value() + " became " + nodeRole.toString());

		Monitor.log(ClientServerNodeComponent.class, Monitor.Level.INFO,
				"Node " + getHost().getId().value() + " became " + nodeRole.toString());

		for (ClientServerNodeRoleListener l : nodeRoleListeners) {
			l.clientServerNodeRoleChanged(this.nodeRole);
		}
	}

	/**
	 * Add a {@link ClientServerNodeRoleListener}.
	 * 
	 * @param l
	 */
	private void addClientServerNodeRoleListener(ClientServerNodeRoleListener l) {
		nodeRoleListeners.add(l);
	}

	/**
	 * Set by the configuration.
	 * 
	 * @param dataUploadInterval
	 */
	public void setDataUploadInterval(long dataUploadInterval) {
		DATA_UPLOAD_INTERVAL = dataUploadInterval;
	}

	/**
	 * Set by the configuration.
	 * 
	 * @param measurementProviderToUse
	 */
	@Deprecated
	public void setMeasurementProviderToUse(String measurementProviderToUse) {
		this.measurementProviderToUse = measurementProviderToUse;
	}

	/**
	 * Incoming packets... - forward to the respective mechanisms (SinkAdv,
	 * NodeBeaconing, DataPacket)
	 */
	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		assert msg instanceof ClientServerCellMessage || msg instanceof ClientServerLocalMessage;

		if (msg instanceof CloudletAdvertisingMessage) {
			CloudletAdvertisingMessage cloudletMsg = (CloudletAdvertisingMessage) msg;
			Monitor.log(ClientServerNodeComponent.class, Monitor.Level.INFO,
					"ClientServerNode: CloudletAdvertisingMessage on Node "
					+ getHost().getId().value() + " from " + sender.toString());
			cloudletAdvertising.handleIncomingCloudletAdvMsg(cloudletMsg);
			cloudletAdvertisingMessages.putNow(cloudletMsg.getUniqueMsgID(), cloudletMsg);
		}

		else if (msg instanceof CloudConfigurationMessage) {
			Monitor.log(ClientServerNodeComponent.class, Monitor.Level.INFO,
					"CloudConfigurationMessage received on Node " + getHost().getId().value() + " from " + sender.toString());
			handleCloudConfigurationMessage((CloudConfigurationMessage) msg, commID);
		}

		else {
			throw new AssertionError("Received Message of unknown type " + msg.getClass());
		}

		// Analyzer
		if (hasMessageAnalyzer()) {
			if (msg instanceof ClientServerCellMessage) {
				getMessageAnalyzer().onReceivedOverlayMessage((ClientServerCellMessage) msg, getHost());
			}
			if (msg instanceof ClientServerLocalMessage) {
				getMessageAnalyzer().onReceivedOverlayMessage((ClientServerLocalMessage) msg, getHost());
			}
		}
	}

	/**
	 * Used to handle an in coming {@link CloudConfigurationMessage}. Perform
	 * actions according to the content of the message.
	 * 
	 * @param msg
	 * @param commID
	 */
	protected void handleCloudConfigurationMessage(CloudConfigurationMessage msg, int commID) {
		assert this.getClientServerNodeRole() != msg
				.getNodeRole() : "Cloud configuration for node role should always include a change of the role of the node";

		setClientServerNodeRole(msg.getNodeRole());
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		if (source instanceof ClientServerCentralComponent) {
			return;
		}

		assert source instanceof ClientServerNodeComponent;

		switch (peerStatus) {
		case ABSENT:
			setClientServerNodeRole(ClientServerNodeRole.OFFLINE);

			// if (hasPerNodeHasNoSinkAnalyzer()) {
			// getPerNodeHasNoSinkTimeAnalyzer().onWentOffline();
			// }
			break;

		case PRESENT:
			setClientServerNodeRole(ClientServerNodeRole.LEAF);

			// if (hasPerNodeHasNoSinkAnalyzer()) {
			// getPerNodeHasNoSinkTimeAnalyzer().onWentOnline(this.getHost().getId());
			// }
			break;
		default:
			break;
		}
	}

	/**
	 * {@link ClientServerNodeComponent} role.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	public enum ClientServerNodeRole {
		SINK,

		LEAF,

		OFFLINE
	}

	@Override
	public String getNodeDescription() {
		return "";
	}

	// Schema:
	// Bedingung ? Zutreffend : nicht Zutreffend;
	@Override
	public int getNodeColor(int dimension) {

		switch (dimension) {
		case 0: // Type/State (leaf/inactive, leaf/active, sink...)
			switch (getClientServerNodeRole()) {
			case LEAF:
				return 0;
			case SINK:
				return 1;
			case OFFLINE:
				return -1;
			default:
				throw new AssertionError("Unknown ClientServerNodeState.");
			}
		default:
			return -1;
		}
	}

	@Override
	public int getNodeColorDimensions() {
		return dimDesc.length;
	}

	private static final String[] dimDesc = { "Type/State" };

	private static final String[][] colorDesc = { { "leaf", "sink" } };

	@Override
	public String[] getNodeColorDimensionDescriptions() {
		return dimDesc;
	}

	@Override
	public String[] getNodeColorDescriptions(int dimension) {
		return colorDesc[dimension];
	}

	/**
	 * Check the current state of the TimeoutMap containing recently received
	 * Data Messages for messages, that haven't timed out yet
	 * 
	 * @return Vector of DataMessageContainerLocalImpl that haven't timed out
	 *         yet
	 */
	public Vector<DataMessageContainerLocalImpl> getAliveDataMessages() {

		Map<UniqueID, DataMessageContainerLocalImpl> currentMessages = dataMessages.getUnmodifiableMap();

		Vector<DataMessageContainerLocalImpl> msgs = new Vector<DataMessageContainerLocalImpl>(
				currentMessages.values());

		return msgs;
	}

	/**
	 * Check for recently received Data Messages, that haven't timed out yet and
	 * return the senders of those messages
	 * 
	 * @return Vector of OverlayContact representing the senders of data
	 *         messages
	 */

	public Vector<OverlayContact> getSendersDataMessages() {
		Vector<OverlayContact> senders = new Vector<OverlayContact>();

		for (DataMessageContainerLocalImpl msg : getAliveDataMessages()) {
			senders.add(msg.getSender());

		}

		return senders;
	}

}
