package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 *
 */
public class AggregationFunctionMax extends AbstractDuplicateInsensitiveAggregationFunction{
	
	public AggregationFunctionMax(Double value){
		super(value);
	}
	
	@Override
	public void putValue(Double value) {
		if(getAggregateValue() < value){
			setAggregateValue(value);
		}
	}

	@Override
	public String getDescription() {
		return DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.toString();
	}

	@Override
	public String getId() {
		return DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.getId();
	}

}
