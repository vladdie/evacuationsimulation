package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.common.IDSpace;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.overlay.DefaultIDSpace;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent.CraterNodeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.cloudlet.CraterCloudletComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.configuration.CraterConfiguration;

/**
 * One Factory is used to ease configuration and sanity checks for
 * {@link CraterComponent}s. The {@link CraterComponent}s are configured via the
 * {@link CraterConfiguration} instances, used to get valid configurations.
 * Refer to crater.configuration for a list of configurations available in
 * Crater.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CraterFactory implements HostComponentFactory {

	private static final IDSpace messageIdSpace = new DefaultIDSpace(64);
	private static final IDSpace aggregationFunctionEntryIDSpace = new DefaultIDSpace(64);
	private static final IDSpace dataObjectIDSpace = new DefaultIDSpace(32);
	
	/**
	 * Node type to be created
	 */
	private CraterNodeType nodeType;

	/**
	 * configuration-class that configures the nodes
	 */
	private List<CraterConfiguration> configurations = new LinkedList<CraterConfiguration>();

	/**
	 * To ensure that there is only one cloud
	 */
	private boolean createdCloud = false;

	private boolean locked = false;

	public CraterFactory() {
		//
	}

	@Override
	public HostComponent createComponent(Host host) {
		if (nodeType == null) {
			throw new AssertionError("There must be a node type specified!");
		}
		if (configurations.isEmpty()) {
			throw new AssertionError("At least one configuration must be specified!");
		}

		if (!locked) {
			locked = true;
		}
		CraterComponent comp;

		switch (nodeType) {
		case MOBILE_NODE:
			comp = new CraterNodeComponent(host);
			break;
		case CLOUD:
			if (createdCloud) {
				throw new AssertionError("Only one cloud instance at a time is supported!");
			}
			comp = new CraterCentralComponent(host);
			createdCloud = true;
			break;
		case CLOUDLET:
			comp = new CraterCloudletComponent(host);
			break;
		default:
			throw new AssertionError("Unknown NodeType.");
		}

		/*
		 * Configures component by adding modules or setting parameters.
		 */
		for (CraterConfiguration configuration : configurations) {
			configuration.configure(comp, nodeType);
		}
		return comp;
	}

	/**
	 * For the XML-Configuration
	 * 
	 * @param nodeType
	 */
	public void setNodeType(String nodeType) {
		assert nodeType != null;
		this.setNodeTypeDirect(CraterNodeType.valueOf(nodeType.toUpperCase()));
	}

	/**
	 * Overloaded method to use directly with {@link CraterNodeType}.
	 * 
	 * @param nodeType
	 */
	public void setNodeTypeDirect(CraterNodeType nodeType) {
		assert nodeType != null;
		this.nodeType = nodeType;
	}
	
	/**
	 * Global configuration. Multiple configurations can be concatenated, they
	 * will be executed in order. Later configurations can overwrite previous
	 * ones.
	 * 
	 * Configurations will be applies to ALL nodes!
	 * 
	 * @param configuration
	 */
	public void setConfiguration(CraterConfiguration configuration) {
		if (locked) {
			throw new AssertionError("Began creating nodes. Adding configurations is no longer allowed.");
		}
		Monitor.log(CraterFactory.class, Level.INFO, "Adding a CraterConfiguration:\n%s", configuration);
		configurations.add(configuration);
	}

	/**
	 * Returns a {@link UniqueID} of the messageIdSpace.
	 *
	 * @return {@link UniqueID}
	 */
	// FIXME ID SPACE NEEDED?
	@Deprecated
	public static UniqueID getRandomMessageId() {
		return messageIdSpace.createRandomID();
	}

	/**
	 * Returns a {@link UniqueID} based of the given long
	 * 
	 * @param id
	 * @return
	 * 
	 * @version 2016.06.22
	 */
	public static UniqueID getMessageIDFromLong(long id) {
		return messageIdSpace.createID(id);
	}

	/**
	 * Returns a {@link UniqueID} of the AggregationFunkctionEntryID Space.
	 *
	 * @return {@link UniqueID}
	 */
	// FIXME ID SPACE NEEDED?
	@Deprecated
	public static UniqueID getRandomAggregationFunctionEntryID() {
		return aggregationFunctionEntryIDSpace.createRandomID();
	}

	// FIXME ID SPACE NEEDED?
	@Deprecated
	public static UniqueID getRandomDataObjectID() {
		return dataObjectIDSpace.createRandomID();
	}
}
