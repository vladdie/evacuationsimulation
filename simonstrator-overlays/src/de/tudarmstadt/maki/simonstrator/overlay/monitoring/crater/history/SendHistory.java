package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history;

import java.util.Collection;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;

/**
 * Abstract History, provides general attributes and methods that all histories
 * have in common.
 * 
 * @author Jonas Huelsmann
 * 
 */
public abstract class SendHistory {

	protected long HISTORY_SIZE;

	public SendHistory(long size) {
		HISTORY_SIZE = size;
	}

	/**
	 * Method that checks if a message is in the history or not This method will
	 * also add the msgID to the history
	 * 
	 * @param msgID
	 *            the ID of a message
	 * @return true if the msgID was not yet in the buffer, else false
	 */
	public abstract boolean sendable(UniqueID msgID);

	/**
	 * Method that checks if a collection of messages are in the history or not
	 * This method will also add the msgIDs to the history which have not been
	 * there yet
	 * 
	 * @param msgIDs
	 *            A Collection of message IDs
	 * @return A Set of message IDs which had not been in the buffer yet
	 */
	public abstract Set<UniqueID> sendable(Collection<UniqueID> msgIDs);

	/**
	 * Method that checks for an entry in the history NOTE!: This method is for
	 * debugging purpose, it does not change the buffer in any way
	 * 
	 * @param msgID
	 *            the ID of a message
	 * @return true if a message's ID is in the history
	 */
	public abstract boolean isInHistory(UniqueID msgID);

	/**
	 * This Method will reset the history
	 * 
	 * @throws an
	 *             exception if used with an other class than
	 *             DefaultCraterSendHistory
	 */
	public abstract void resetHistory() throws Exception;

	/**
	 * Method that initializes the type of history used as well as its length /
	 * duration defined by sendHistoryValue
	 * 
	 * @author Jonas Huelsmann
	 * @param historyType TODO
	 */
	public static SendHistory createHistory(HistoryType historyType, long historyValue) {
		switch (historyType) {
		case TIME:
			return new TimedCraterSendHistory(historyValue);
		case SIZE:
			return new SizedCraterSendHistory(historyValue);
		case DEFAULT:
			return new DefaultCraterSendHistory(historyValue);
		default:
			return new NoCraterSendHistory(1);
		}
	}

	/**
	 * Enumeration, that lists the implemented history types
	 *
	 */
	public enum HistoryType {
		/*
		 * A history where entries will be erased after a certain time
		 */
		TIME,
		/*
		 * A history where the oldest entries will be erased after a certain
		 * amount of entries are in the history
		 */
		SIZE,
		/*
		 * Default history, no entries will be saved. sendable() will be
		 * answered with true
		 */
		NONE,
		/*
		 * Default history which will detect duplicates but needs to be reset to
		 * behave like the original buffer
		 */
		DEFAULT;
	}
}


