Routing consists of the following tasks:

- Data packet (i) forwarding, (iii) sending, (ii) uploading
- [sink = true] upload from packet buffer to infrastructure
- [sink = false] forward/send packet buffer to current associated sink
- different packet types: normal data packet, aggr. dup. sens., aggr. dup. insens.
- - thus distinguish buffers for that three types
- - resolve dup. already in buffers! 