package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing;

import java.util.HashMap;
import java.util.Map.Entry;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.AbstractContentionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.ContentionScheme.ContentionSchemeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.NoKnownSinksException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history.SendHistory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history.SendHistory.HistoryType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers.LocalBufferEntryLists;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers.UploadBufferEntryLists;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.operations.PeriodicSendDataOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.operations.PeriodicUploadOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.ResponseEventHandler;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

public class DataRoutingImpl extends AbstractDataRouting {

	private AbstractContentionScheme contention;

	@TransferState(value = { "ReceivedDataMessagesContainer" })
	private TimeoutSet<UniqueID> receivedDataMessagesContainer;

	private HashMap<UniqueID, Double> heardDataMessageContainerDuringHesitationTime;

	@TransferState(value = { "AlreadySendOutBufferDataViaForwardedPacket" })
	private boolean alreadySendOutBufferDataViaForwardedPacket = false;

	// used for sanity checks
	private boolean activeComponent = false;


	/**
	 * 
	 * @param parentNode
	 */
	public DataRoutingImpl(CraterNodeComponent parentNode, long DATA_SENDING_PERIODIC_OPERATION_INTERVAL,
			long DATA_UPLOAD_INTERVAL, HistoryType sendHistoryType,
			long sendHistoryValue, ContentionSchemeType contentionType) {
		super(parentNode, DATA_SENDING_PERIODIC_OPERATION_INTERVAL, DATA_UPLOAD_INTERVAL, sendHistoryType,
				sendHistoryValue, contentionType);
		receivedDataMessagesContainer = new TimeoutSet<UniqueID>(RECEIVED_DATA_MESSAGES_TIMEOUT);
		heardDataMessageContainerDuringHesitationTime = new HashMap<UniqueID, Double>();
		contention = createContentionScheme(parentNode);

	}

	/**
	 * TransferState annoted Constructor to be used use by the TransitionEngine
	 * 
	 * @param parentNode
	 * @param history
	 * @param receivedDataMessagesContainer
	 * @param alreadySendOutBufferDataViaForwardedPacket
	 * @param localBuffer
	 * @param uploadBuffer
	 * @param periodicSendDataOperation
	 * @param periodicUploadOperation
	 * @param ResponseEventHandler
	 *            sendingScheduler
	 * @param lastSpeedUpdateTime
	 * @param ClusterSpeedComputation
	 *            clusterSpeedComp
	 */
	@TransferState(value = { "ParentNode", "History", "ReceivedDataMessagesContainer",
			"AlreadySendOutBufferDataViaForwardedPacket",
 "LocalBuffer",
			"UploadBuffer", "PeriodicSendDataOperation",
			"PeriodicUploadOperation", "SendingScheduler", "LastSpeedUpdateTime", "ClusterSpeedComp" })
	public DataRoutingImpl(CraterNodeComponent parentNode, SendHistory history,
			TimeoutSet<UniqueID> receivedDataMessagesContainer,
			Boolean alreadySendOutBufferDataViaForwardedPacket, LocalBufferEntryLists localBuffer,
			UploadBufferEntryLists uploadBuffer,
			PeriodicSendDataOperation periodicSendDataOperation,
			PeriodicUploadOperation periodicUploadOperation,
			ResponseEventHandler sendingScheduler, Long lastSpeedUpdateTime,
			ClusterSpeedComputation clusterSpeedComp) {
		super(parentNode, history, localBuffer, uploadBuffer,
				periodicSendDataOperation, periodicUploadOperation,
				sendingScheduler, lastSpeedUpdateTime, clusterSpeedComp);
		contention = createContentionScheme(parentNode);
		this.receivedDataMessagesContainer = receivedDataMessagesContainer;
		this.heardDataMessageContainerDuringHesitationTime = new HashMap<UniqueID, Double>();
		this.alreadySendOutBufferDataViaForwardedPacket = alreadySendOutBufferDataViaForwardedPacket.booleanValue();
	}

	@Override
	public void initialize() {
		initializePeriodicOperations();
	}

	@Override
	public void handleIncomingMessage(CraterLocalMessage cMsg) {
		assert activeComponent : "Stopped DataRouting Components should not handle messages!";
		if (!(cMsg instanceof DataMessageContainerLocalImpl)) {
			return;
		}
		DataMessageContainerLocalImpl msg = (DataMessageContainerLocalImpl) cMsg;
		/*
		 * 
		 * - #3# [sink == true]
		 * 
		 * - #3# sinkId of msg == sinkId of this node
		 * 
		 * -- #3.1# [sink == true] put to packet contents to upload buffers
		 * 
		 * - #4# [sink == false]
		 * 
		 * - #4# on intermediate nodes! See if has been received by the node itself previously, thus being processed
		 * already!
		 * 
		 * - #4# own best sinkId == msg.sinkId & own best sink gradient > msg.gradient
		 * 
		 * -- #4.1# [sink == false] forward (under conditions); exchange gradient with own best
		 */

		/*
		 * Add to heard msg's only when msg has not been heard before or when heard before also has greater gradient. So
		 * for one container the received one with the greatest gradient is stored.
		 */
		if (heardDataMessageContainerDuringHesitationTime.containsKey(msg.getUniqueMsgID())) {
			if (heardDataMessageContainerDuringHesitationTime.get(msg.getUniqueMsgID()) < msg.getGradient()) {
				heardDataMessageContainerDuringHesitationTime.put(msg.getUniqueMsgID(), msg.getGradient());
			}
		} else {
			heardDataMessageContainerDuringHesitationTime.put(msg.getUniqueMsgID(), msg.getGradient());
		}

		switch (getParentNode().getCraterNodeRole()) {
		case SINK:

			// #3#
			if (msg.getSinkId() == getParentNode().getHost().getId()) {

				onReceivedDataContainerAsSink(msg);

				/*
				 * Analyzing
				 * 
				 * - #A.1# Received as sink multiple times a CraterDataContainerMessage which was intended for that
				 * sink.
				 * 
				 * - msg id contained, isSink == true, sinkId = msg.CraterNodeId
				 */
				if (hasDataRoutingAnalyzer()) {
					// #A.1#
					if (receivedDataMessagesContainer.contains(msg.getUniqueMsgID())) {
						getDataRoutingAnalyzer().onSuccessiveContainerMessageReceivedBySink(msg.getUniqueMsgID());
					} else if (!receivedDataMessagesContainer.contains(msg.getUniqueMsgID())) {
						getDataRoutingAnalyzer().onContainerMessageReceivedBySink(msg.getUniqueMsgID(), msg.getSinkId());
					}
				}
				receivedDataMessagesContainer.addNow(msg.getUniqueMsgID());
				return;
			}
			break;
		case LEAF:

			// #4#
			if (receivedDataMessagesContainer.contains(msg.getUniqueMsgID()))
				return;

			receivedDataMessagesContainer.addNow(msg.getUniqueMsgID());

			Entry<INodeID, Double> bestSinkTableEntry;
			try {
				bestSinkTableEntry = getParentNode().getBestSinkTableEntry();
				// #4#
				if (msg.getSinkId() == bestSinkTableEntry.getKey() && bestSinkTableEntry.getValue() > msg.getGradient()) {
					// #4.1#
					/*
					 * TODO How to overcome large amount of duplicates at last hop where grad = 9 without losing 20
					 * percent of delivery ratio when not forwarding if value is larger than 8.9 e.g.
					 */
					// if(bestSinkTableEntry.getValue() == 9){
					// if (parentNode.getRandom().nextDouble() >= 0.9) {
					// return;
					// }
					// }

					// FIXME Original behaviour of dataforwarding was to carry
					// on the initial sender ID. Is this intended?
					DataMessageContainerLocalImpl exchangedGradientMsg = new DataMessageContainerLocalImpl(
							msg.getSender(), null, msg,
							bestSinkTableEntry.getValue());
					Event.scheduleWithDelay(
							getHesitationTime(bestSinkTableEntry.getValue()),
							getParentNode().getDataRouting(), exchangedGradientMsg,
							DATA_CONTAINER_MESSAGE_FORWARDING_EVENT);
					heardDataMessageContainerDuringHesitationTime.clear();
					return;
				}
			} catch (NoKnownSinksException e) {
				/*
				 * I have no own sinks.
				 */
				return;
			}
			break;
		default:
			break;
		}
	}

	/**
	 * Returning the current calculated hesitation time. The logic used is
	 * defined by the contentionType
	 * 
	 * @param gradient
	 *            A gradient
	 * @return the time to hesitate
	 */
	private long getHesitationTime(double gradient) {
		return contention.getHesitationTime(gradient);
	}

	@Override
	public void sendBufferContent() {
		assert activeComponent : "Stopped DataRouting Component should not send Buffers!";
		switch (getParentNode().getCraterNodeRole()) {
		case LEAF:
			/*
			 * Send out new data if conditions
			 * 
			 * - #1# has not already put some of own data to going-through message
			 * 
			 * - #2# at least one buffer has data inside to send out. We do not want to send empty message!
			 */
			// #1#
			if (alreadySendOutBufferDataViaForwardedPacket) {
				// Was originally reset by nested Operation after execution of
				// this method
				alreadySendOutBufferDataViaForwardedPacket = false;
				return;
			}
			alreadySendOutBufferDataViaForwardedPacket = false;
			fillBuffersWithAttributeGeneratorData();
			// #2#
			if (getLocalBuffer().getNormalData().isEmpty()
					|| getLocalBuffer().getDuplicateInsensitiveAggregateData().isEmpty()
					|| getLocalBuffer().getDuplicateSensitiveAggregateData().isEmpty())
				return;

			try {

				Entry<INodeID, Double> bestSinkEntry = getParentNode().getBestSinkTableEntry();

				DataMessageContainerLocalImpl dataMessageContainer = new DataMessageContainerLocalImpl(
						getParentNode().getLocalOverlayContact(), null, bestSinkEntry.getKey(),
						bestSinkEntry.getValue());

				dataMessageContainer = fillWithBufferEntries(dataMessageContainer, true);

				// parentNode.sendDataMessage(dataMessageContainer);
				getParentNode().sendLocalBroadcast(dataMessageContainer);
				if (hasDataRoutingAnalyzer()) {
					getDataRoutingAnalyzer().onContainerMessageSend(dataMessageContainer.getUniqueMsgID(),
							dataMessageContainer.getSinkId());
				}
				// Event.scheduleWithDelay((long)
				// (parentNode.getRandom().nextDouble() * 2.5 * Time.SECOND),
				// this, dataMessageContainer,
				// DATA_CONTAINER_MESSAGE_SENDING_EVENT);
				// FIXME What is impact of JITTER?
			} catch (NoKnownSinksException e) {
				// Currently has no Sink, thus can not send data!
				return;
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void forwardDataMessage(DataMessageContainerLocalImpl msg) {

		/*
		 * At this point msg is dupFree (best effort), exchanged gradient.
		 * 
		 * Forwarding of {@link CraterDataContainerMessage} after hesitation time expires only if NOT..
		 * 
		 * - #1# other nodes re-sent the packet earlier, thus should be better applicable! So do not forward.
		 * 
		 * then
		 * 
		 * - #2# add own buffer content
		 * 
		 * CAUTION - This method is exclusively responsible for adding own buffer content to such a msg.
		 * 
		 * CAUTION! Do not make payload/packet larger than max. MTU.
		 * 
		 * FIXME what brings that? (-- own scheduled msg gradient (best gradient at the time when msg was intended to be
		 * send) for that sinkId is greater than received gradient)
		 */
		// #1#
		if (heardDataMessageContainerDuringHesitationTime.containsKey(msg.getUniqueMsgID())) {
			// if (msg.getGradient() >
			// heardDataMessageContainerDuringHesitationTime.get(msg.getUniqueMsgID()))
			// {
			// System.out.println("Heard DataContainer faster other DataContainerMessage.");
			// parentNode.sendDataMessage(msg);
			// }
			return;
		} else {
			/*
			 * Forward data
			 * 
			 * #1# without additional buffer if no data in buffers.
			 * 
			 * #2# with additonal data if any buffer has data inside.
			 */
			fillBuffersWithAttributeGeneratorData();
			// #1#
			if (getLocalBuffer().getNormalData().isEmpty()
					|| getLocalBuffer().getDuplicateInsensitiveAggregateData().isEmpty()
					|| getLocalBuffer().getDuplicateSensitiveAggregateData().isEmpty()) {
				// parentNode.sendDataMessage(msg);

				// Fill with replies before sending
				fillContainerMessageWithResponses(msg);

				getParentNode().sendLocalBroadcast(msg);
				return;
			}

			// #2#
			msg = fillWithBufferEntries(msg, false);

			alreadySendOutBufferDataViaForwardedPacket = true;
			// parentNode.sendDataMessage(msg);
			getParentNode().sendLocalBroadcast(msg);
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		assert activeComponent : "Events should not directly reference stopped DataRouting Components";
		// FIXME!!
		if (getParentNode().getCraterNodeRole() == CraterNodeRole.OFFLINE || !getParentNode().isPresent()) {
			assert getParentNode().getCraterNodeRole() == CraterNodeRole.OFFLINE;
			assert !getParentNode().isPresent();
			return;
		}

		if (type == DATA_CONTAINER_MESSAGE_FORWARDING_EVENT && content instanceof DataMessageContainerLocalImpl) {
			DataMessageContainerLocalImpl msg = (DataMessageContainerLocalImpl) content;
			forwardDataMessage(msg);
		}

		if (type == DATA_CONTAINER_MESSAGE_SENDING_EVENT && content instanceof DataMessageContainerLocalImpl) {
			DataMessageContainerLocalImpl msg = (DataMessageContainerLocalImpl) content;
			// parentNode.sendDataMessage(msg);
			getParentNode().sendLocalBroadcast(msg);
			if (hasDataRoutingAnalyzer()) {
				getDataRoutingAnalyzer().onContainerMessageSend(msg.getUniqueMsgID(), msg.getSinkId());
			}
		}
	}

	public TimeoutSet<UniqueID> getReceivedDataMessagesContainer() {
		return receivedDataMessagesContainer;
	}

	// called *get*Already instead of *is*Already because the TransitionEngine
	// searches for *get*-Methods
	public boolean getAlreadySendOutBufferDataViaForwardedPacket() {
		return alreadySendOutBufferDataViaForwardedPacket;
	}

	@Override
	public void startMechanism(Callback cb) {
		activeComponent = true;

		if (getPeriodicSendDataOperation() == null) {
			// first call
			initialize();
		}

		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {

		super.stopMechanism(new Callback() {
			@Override
			public void finished(boolean successful) {
				// do nothing
			}
		});
		activeComponent = false;

		contention = null;
		receivedDataMessagesContainer = null;
		heardDataMessageContainerDuringHesitationTime = null;

		cb.finished(true);
	}

}