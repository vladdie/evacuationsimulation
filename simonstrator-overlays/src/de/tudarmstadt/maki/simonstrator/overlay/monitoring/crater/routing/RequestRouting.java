package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterRequestResolver;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestInquiryMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestMessageCellular;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestMessageLocal;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterNodeRoleListener;

/**
 * Interface for Request-Routing This is different from DataRouting as Requests
 * need to be distributed *everywhere*, not only in direction of the Sink
 * 
 */
public interface RequestRouting extends CraterNodeRoleListener {

	public void init();

	/**
	 * Handles the incoming Message, f.e., forwards it to locally, sends it to
	 * the Cloud and forwards the included Request to the
	 * {@link CraterRequestResolver}.
	 * 
	 * Messages should be either {@link RequestMessageLocal} or
	 * {@link RequestMessageCellular}.
	 * 
	 * @param msg
	 */
	public void handleIncommingRequestMessage(Message msg);

	/**
	 * Put the request into the appropriate message-type and send-away
	 * 
	 * @param request
	 */
	public void sendRequest(Request request);

	/**
	 * Upon receiving of such a message, the req-Routing should query the local
	 * req-Resolver for the wanted Requests, and rebroadcast them.
	 * 
	 * @param inquiryMsg
	 */
	// FIXME: Maybe move to request resolver, don't know where this fits better
	public void handleIncommingRequestInquiry(RequestInquiryMessage inquiryMsg);

}
