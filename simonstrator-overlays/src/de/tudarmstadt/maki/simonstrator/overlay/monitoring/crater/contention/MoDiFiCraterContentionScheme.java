package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention;

/**
 * An implementation of {@link AbstractContentionScheme} based on devices distance towards the closest sink and a random
 * factor to reduce the probability of a collision
 * 
 * @author Jonas Huelsmann
 */
public class MoDiFiCraterContentionScheme extends AbstractContentionScheme {

	public MoDiFiCraterContentionScheme(long min, long max) {
		super(min, max);
	}

	@Override
	public long getHesitationTime(double gradient) {
		// low gradients (distant nodes) will send first
		// some Randomness is added

		double randomValue = (this.getRandom().nextDouble());
		double hesFactor = (gradient - randomValue) / 10;
		if (hesFactor < 0)
			hesFactor = 0;
		long hesitationTime = MIN_DATA_ROUTING_HESITATION_TIME
				+ (long) (hesFactor * (MAX_DATA_ROUTING_HESITATION_TIME - MIN_DATA_ROUTING_HESITATION_TIME));
		return hesitationTime;
	}

}
