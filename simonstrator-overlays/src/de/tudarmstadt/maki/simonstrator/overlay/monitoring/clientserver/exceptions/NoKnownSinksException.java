package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.exceptions;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 09.06.2014
 *
 */
public class NoKnownSinksException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoKnownSinksException() {
		super("There is no Sink in the sinkTable. Catch this error!");
	}

	

}
