package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing;

import java.util.LinkedList;

/**
 * Interface for the container data message. This message includes normal data,
 * dup. sen. aggregates and dup. insens. aggregates. <br>
 * Update 19.07.2016: Now also includes response data for specific requests
 * 
 * @author Nils Richerzhagen
 * @version 1.05, 19.07.2016
 * 
 */
public interface DataMessageContainer {
	/**
	 * Should be called before data is added.
	 * 
	 * @return
	 * 		the filled percentage of the container.
	 */
	public double getFilledPercentage();

	/**
	 * Returning the list of {@link DataObject}'s.
	 * 
	 * @return
	 */
	public LinkedList<DataObjectImpl<Double>> getNormalDataList();

	/**
	 * Returning the list of {@link DataObjectAggregationDupInsensitiveImpl}'s.
	 * 
	 * @return
	 */
	public LinkedList<DataObjectAggregationDupInsensitiveImpl> getAggregationDupInsensitiveDataList();

	/**
	 * Returning the list of {@link DataObjectAggregationDupSensitiveImpl}'s.
	 * 
	 * @return
	 */
	public LinkedList<DataObjectAggregationDupSensitiveImpl> getAggregationDupSensitiveDataList();

	public enum MergePoint {
		IN_NETWORK,

		CENTRAL,

		DURING_CLONE,

		IN_ANALYZER
	}

	/**
	 * Returning the list of {@link ResponseObject}s which includes responses to
	 * LoadGenerator requests
	 * 
	 * @return
	 */
	public LinkedList<ResponseObject> getQueryResponseList();
}
