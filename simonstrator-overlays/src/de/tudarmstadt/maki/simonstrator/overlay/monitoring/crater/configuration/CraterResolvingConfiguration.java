package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.configuration;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent.CraterNodeType;

/**
 * 
 * Configuration for the resolving strategies within Crater.
 * 
 * TODO Which resolving strategies are to be used in Crater?
 * 
 * FULL Global Knowledge (directly accessing the relevant components?)
 * 
 * Request Global Knowledge: distributing request via global knowledge?
 * collecting data via crater?
 * 
 * Collecting global knowlede: vice versa to the upper one
 * 
 * Rekonfigurable Monitoring: Distributing and handling request in network,
 * collecting data
 * 
 * 
 * 
 * @author Nils Richerzhagen
 *
 */
public class CraterResolvingConfiguration implements CraterConfiguration {

	@Override
	public void configure(CraterComponent comp, CraterNodeType type) {
		// TODO

		switch (type) {
		case CLOUD:

			break;

		case MOBILE_NODE:

			break;
		default:
			throw new AssertionError("No valid node type.");
		}
	}

}
