package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention;

/**
 * Basic contention-based scheme.
 * 
 * @author Nils Richerzhagen
 *
 */
public interface ContentionScheme {

	/**
	 * Method that calculates the hesitation time needed for the sender-based contention scheme
	 * 
	 * @param gradient
	 *            The node's gradient
	 * @return A long which is further processed for sender-based contention scheme
	 */
	public long getHesitationTime(double gradient);


	/**
	 * Enumeration that contains all implementations of the sender-based contention scheme
	 */
	public enum ContentionSchemeType {
		/*
		 * Default contention scheme, depending on gradient, battery and randomness
		 */
		DEFAULT,
		/*
		 * Distant First - the nodes with a big distance towards a sink (small gradient) will send first. Intended to
		 * reduce network overload due to piggybacking
		 */
		DIFI,
		/*
		 * Modified Distant First - DIFI with some randomness
		 */
		MODIFI;
	}
}
