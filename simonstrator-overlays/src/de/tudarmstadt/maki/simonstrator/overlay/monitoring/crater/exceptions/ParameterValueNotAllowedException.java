package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 09.06.2014
 *
 */
public class ParameterValueNotAllowedException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This parameter value should not occur here.
	 * @param <T>
	 */
	public <T> ParameterValueNotAllowedException(String className) {
		super("This parameter value should not occur. Occured in " + className);
	}
	
}
