package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

public class ResponseEntry {
	private INodeID nodeID; // doubles as UID
	private Object value;

	public ResponseEntry(INodeID nodeID, Object value) {
		this.nodeID = nodeID;
		this.value = value;

		// For Analyzer
		creationTime = Time.getCurrentTime();
	}

	public INodeID getIdentifier() {
		return nodeID;
	}

	public Object getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nodeID == null) ? 0 : nodeID.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseEntry other = (ResponseEntry) obj;
		if (nodeID == null) {
			if (other.nodeID != null)
				return false;
		} else if (!nodeID.equals(other.nodeID))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	// //////////////////////////////////////////////////////////
	// For analyzer only
	private final long creationTime;

	@Deprecated
	/**
	 * Should only be called by the Analyzer!
	 * @return
	 */
	public long getCreationTime() {
		return creationTime;
	}
}
