package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.configuration;

import javax.naming.ConfigurationException;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent.CraterNodeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.ResponisbleComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.SinkAdvertisingImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.sinksendinginterval.SinkAdvIntervalModifier;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.sinksendinginterval.StaticInterval;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.cloudlet.CraterCloudletComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention.ContentionScheme.ContentionSchemeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history.SendHistory.HistoryType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.AbstractDataRouting;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.ClusterSpeedComputation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataRoutingUnicastImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.RequestRoutingImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference.SinkPreference.SinkPreferenceType;

/**
 * Configuration Parameters for the {@link CraterNodeComponent} and
 * {@link CraterCentralComponent} are handled here.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CraterParameterConfiguration implements CraterConfiguration {

	// No Sink Advertising Parameters
	private long NO_SINK_ADV_INT;

	// Sink Advertising Parameters
	private int INITIAL_TTL;
	private long SINK_ADV_PERIODIC_OPERATION_INTERVAL;
	private SinkPreferenceType sinkPreferenceType;

	private SinkAdvIntervalModifier intervalDeterminator;

	// Data Routing
	private long DATA_SENDING_PERIODIC_OPERATION_INTERVAL;
	private long DATA_UPLOAD_INTERVAL;

	private String measurementProviderToUse;
	private int numCloudConnections;
	private long _GATEWAY_SELECTION_INTERVAL;

	private HistoryType sendHistoryType;
	private long sendHistoryValue;
	private ContentionSchemeType contentionType;

	private double removeOutliers;
	private ClusterSpeedComputation.computationStrategy speedComputationStrategy;

	// Cloudlet Parameters
	private int CLOUDLET_INITIAL_TTL;
	private long CLOUDLET_ADV_INTERVAL;

	private boolean cloudletSouldAdaptInterval;
	private double cloudletIntervalDivisor;

	// CraterNodeComponent Parameters
	private ResponisbleComponent computeSpeedAt = ResponisbleComponent.OFF;
	private long locationUpdateInterval = Long.MAX_VALUE;

	private double slowSpeed;
	private double slowSpeedOffset;

	// Parameter for DGS utilization
	private boolean useGSC = false;
	// private boolean startWithDGS;

	// Parameteer for Request Responses
	private long sinkMaxResponseForwardingDelay;
	private int requestTTL = 3;
	private double responseHesitationFactor = 0.1;
	// Used as static in RequestRouting
	private String requestDisseminationType;
	private double requestDisseminationvalue;
	// For Request Robustness
	private int sinkAdvertisementRequestHashesInterval;

	@Override
	public void configure(CraterComponent comp, CraterNodeType type) {
		switch (type) {
		case MOBILE_NODE:
			((CraterNodeComponent) comp).setNoSinkAdvInt(NO_SINK_ADV_INT);
			((CraterNodeComponent) comp).setSinkAdvInterval(SINK_ADV_PERIODIC_OPERATION_INTERVAL);
			((CraterNodeComponent) comp).setInitialTTL(INITIAL_TTL);
			((CraterNodeComponent) comp).setDataSendingPeriodicInterval(DATA_SENDING_PERIODIC_OPERATION_INTERVAL);
			((CraterNodeComponent) comp).setDataUploadInterval(DATA_UPLOAD_INTERVAL);
			((CraterNodeComponent) comp).setMeasurementProviderToUse(measurementProviderToUse);
			((CraterNodeComponent) comp).setHistoryType(sendHistoryType);
			((CraterNodeComponent) comp).setHistoryValue(sendHistoryValue);
			((CraterNodeComponent) comp).setContentionType(contentionType);
			((CraterNodeComponent) comp)
					.setSinkPreferenceType(sinkPreferenceType);
			((CraterNodeComponent) comp).setComputeSpeedAt(computeSpeedAt);
			((CraterNodeComponent) comp).setLocationUpdateInterval(locationUpdateInterval);
			((CraterNodeComponent) comp).setSlowSpeed(slowSpeed);
			((CraterNodeComponent) comp).setSlowSpeedOffset(slowSpeedOffset);
			((CraterNodeComponent) comp).setSinkAdvIntervalModifier(intervalDeterminator);
			((CraterNodeComponent) comp).setUseGatewaySelectionClient(useGSC);
			((CraterNodeComponent) comp).setRequestTTL(requestTTL);
			((CraterNodeComponent) comp).setResponseHesitationFactor(responseHesitationFactor);
			break;
		case CLOUD:
			((CraterCentralComponent) comp).setNumCloudConnections(numCloudConnections);
			((CraterCentralComponent) comp).setGatewaySelectionInterval(_GATEWAY_SELECTION_INTERVAL);
			break;
		case CLOUDLET:
			((CraterCloudletComponent) comp).setInitialTTL(CLOUDLET_INITIAL_TTL);
			((CraterCloudletComponent) comp).setCloudletAdvInterval(CLOUDLET_ADV_INTERVAL);
			((CraterCloudletComponent) comp)
					.setDataSendingInterval(DATA_SENDING_PERIODIC_OPERATION_INTERVAL);
			((CraterCloudletComponent) comp).setCloudletShouldAdaptInterval(cloudletSouldAdaptInterval);
			((CraterCloudletComponent) comp).setRemoveOutliers(removeOutliers);
			((CraterCloudletComponent) comp).setSpeedComputationStrategy(speedComputationStrategy);
			((CraterCloudletComponent) comp).setIntervalDeterminator(intervalDeterminator);
			((CraterCloudletComponent) comp).setCloudletIntervalDivisor(cloudletIntervalDivisor);
		default:
			break;
		}

	}

	public void setNoSinkAdvInt(long noSinkAdvInt) {
		NO_SINK_ADV_INT = noSinkAdvInt;
	}

	public void setSinkAdvInt(long sinkAdvInterval) {
		SINK_ADV_PERIODIC_OPERATION_INTERVAL = sinkAdvInterval;
		if (intervalDeterminator == null) {
			intervalDeterminator = new StaticInterval(SINK_ADV_PERIODIC_OPERATION_INTERVAL);
		}
	}

	public void setInitialTTL(int initialTTL) {
		INITIAL_TTL = initialTTL;
	}

	public void setDataSendingPeriodicInterval(long dateSendingPeriodicInterval) {
		DATA_SENDING_PERIODIC_OPERATION_INTERVAL = dateSendingPeriodicInterval;
	}

	public void setDataUploadInterval(long dataUploadInterval) {
		DATA_UPLOAD_INTERVAL = dataUploadInterval;
	}

	public void setMeasurementProviderToUse(String measurementProviderToUse) {
		this.measurementProviderToUse = measurementProviderToUse;
	}
	
	public void setNumCloudConnections(int numCloudConnections) {
		assert numCloudConnections > 0 : "Cloud Connections - numberOfGateways should be larger zero.";

		this.numCloudConnections = numCloudConnections;
	}

	public void setGatewaySelectionInterval(long gatewaySelectionInterval) {
		assert gatewaySelectionInterval >= 2 * Time.MINUTE;
		this._GATEWAY_SELECTION_INTERVAL = gatewaySelectionInterval;
	}

	public void setCloudletAdvInterval(long cloudletAdvInterval) {
		this.CLOUDLET_ADV_INTERVAL = cloudletAdvInterval;
	}

	public void setCloudletInitialTTL(int cloudletInitialTTL) {
		this.CLOUDLET_INITIAL_TTL = cloudletInitialTTL;
	}

	/**
	 * Setter for sendHistoryType
	 * 
	 * @param sendHistoryType
	 * @throws ConfigurationException
	 * @Author Jonas Huelsmann
	 */
	public void setSendHistoryType(String sendHistoryType)
			throws ConfigurationException {
		sendHistoryType = sendHistoryType.toUpperCase();
		try {
			// check if set type fits the enumeration
			this.sendHistoryType = HistoryType.valueOf(sendHistoryType);
		} catch (IllegalArgumentException e) {
			throw new ConfigurationException("The HistoryType "
					+ sendHistoryType + " is unknown.");
		}
		if (this.sendHistoryType == null) {
			throw new ConfigurationException("The HistoryType "
					+ sendHistoryType + " is unknown.");
		}
	}

	/**
	 * Setter for sendHistoryValue
	 * 
	 * @param sendHistoryValue
	 * @author Jonas Huelsmann
	 */
	public void setSendHistoryValue(long sendHistoryValue) {
		this.sendHistoryValue = sendHistoryValue;
	}

	/**
	 * Setter for contentionSchemeType
	 * 
	 * @param contentionType
	 * @throws ConfigurationException
	 * @author Jonas Huelsmann
	 */
	public void setContentionSchemeType(String contentionType)
			throws ConfigurationException {
		contentionType = contentionType.toUpperCase();
		try {
			// check if set type fits the enumeration
			this.contentionType = ContentionSchemeType.valueOf(contentionType);
		} catch (IllegalArgumentException e) {
			throw new ConfigurationException("The ContentionSchemeType "
					+ contentionType + " is unknown.");
		}
		if (this.sendHistoryType == null) {
			throw new ConfigurationException("The ContentionSchemeType "
					+ contentionType + " is unknown.");
		}
	}

	/**
	 * Setter for sinkPreferenceType
	 * 
	 * @param sinkPreferenceType
	 * @throws ConfigurationException
	 * @author Jonas Huelsmann
	 */
	public void setSinkPreferenceType(String sinkPreferenceType)
			throws ConfigurationException {
		sinkPreferenceType = sinkPreferenceType.toUpperCase();
		try {
			// check if set type fits the enumeration
			this.sinkPreferenceType = SinkPreferenceType
					.valueOf(sinkPreferenceType);
		} catch (IllegalArgumentException e) {
			throw new ConfigurationException("The SinkPreferenceType "
					+ sinkPreferenceType + " is unknown.");
		}
		if (this.sendHistoryType == null) {
			throw new ConfigurationException("The SinkPreferenceType "
					+ sinkPreferenceType + " is unknown.");
		}
	}

	public void setMaxDataRoutingHesitationTime(
			long maxDataRoutingHesitationTime) {
		AbstractDataRouting.setMaxHesitation(maxDataRoutingHesitationTime);
	}

	public void setComputeSpeedAt(String computeSpeedAt) throws ConfigurationException {
		computeSpeedAt = computeSpeedAt.toUpperCase();
		try {
			this.computeSpeedAt = ResponisbleComponent.valueOf(computeSpeedAt);
		} catch (IllegalArgumentException e) {
			throw new ConfigurationException("The ResponisbleComponentType " + computeSpeedAt + " is unknown.");
		}
	}

	public void setLocationUpdateInterval(long interval) {
		locationUpdateInterval = interval;
	}

	public void setSlowSpeed(double slowSpeed) {
		this.slowSpeed = slowSpeed;
	}

	public void setSlowSpeedOffset(double slowSpeedOffset) {
		this.slowSpeedOffset = slowSpeedOffset;
	}

	public void setSinkAdvIntervalModifier(SinkAdvIntervalModifier intervalDeterminator) {
		this.intervalDeterminator = intervalDeterminator;
	}

	public void setCloudletShouldAdaptInterval(boolean shouldAdaptInterval) {
		this.cloudletSouldAdaptInterval = shouldAdaptInterval;
	}

	public void setCloudletIntervalDivisor(double intervalDivisor) {
		this.cloudletIntervalDivisor = intervalDivisor;
	}

	public void setSendSpeedEstimates(boolean sendIt) {
		AbstractDataRouting.setSendSpeedEstimates(sendIt);
	}

	public void setSpeedComputationStrategy(String scs) throws ConfigurationException {
		scs = scs.toUpperCase();
		try {
			this.speedComputationStrategy = ClusterSpeedComputation.computationStrategy.valueOf(scs);
			ClusterSpeedComputation.setSpeedComputationStrategy(speedComputationStrategy);
		} catch (IllegalArgumentException e) {
			throw new ConfigurationException("SpeedComputation can only be of Type AVG or MEDIAN");
		}
	}

	public void setRemoveOutliers(double ro) {
		assert ro >= 0 : "Outlier value must be larger than or equal 0!";
		assert ro <= 0.5 : "Outlier value must be smaler than or equal 0.5";
		this.removeOutliers = ro;
		ClusterSpeedComputation.setRemoveOutliers(ro);
	}

	public void setReplyTimeout(long timeout) {
		DataRoutingUnicastImpl.setREPLY_TIMEOUT(timeout);
	}

	public void setUseGatewaySelectionClient(boolean useDGS) {
		this.useGSC = useDGS;
	}

	// public void setStartWithDGS(boolean startWithDGS) {
	// this.startWithDGS = startWithDGS;
	// DecentralizedGatewayLogicController.setStartWithDGS(startWithDGS);
	// }

	public void setSinkMaxResponseForwardingDelay(long delay) {
		this.sinkMaxResponseForwardingDelay = delay;
		AbstractDataRouting.setSinkMaxResponseForwardingDelay(sinkMaxResponseForwardingDelay);
	}

	public void setRequestTTL(int ttl) {
		this.requestTTL = ttl;
	}

	public void setResponseHesitationFactor(double factor) {
		this.responseHesitationFactor = factor;
	}

	public void setRequestDisseminationType(String type) {
		requestDisseminationType = type;
		RequestRoutingImpl.setRequestDisseminationType(type);
	}

	public void setRequestDisseminationValue(double value) {
		requestDisseminationvalue = value;
		RequestRoutingImpl.setRequestDisseminationValue(value);
	}

	public void setSinkAdvertisementRequestHashesInterval(int n) {
		sinkAdvertisementRequestHashesInterval = n;
		SinkAdvertisingImpl.setRequestHashesInEachNThAdvMsg(n);
	}
}