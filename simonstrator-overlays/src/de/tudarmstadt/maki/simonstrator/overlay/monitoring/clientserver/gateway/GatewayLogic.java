package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.gateway;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.configuration.CloudConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.GatewaySelectionStrategy;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Gateway logic at the cloud {@link ClientServerCentralComponent} used in
 * ClientServer.
 * 
 * @author Nils Richerzhagen
 *
 */
public interface GatewayLogic {

	public ClientServerComponent getComponent();

	/**
	 * Returns the {@link CloudConfigurationMessage}s that need to be sent in
	 * Order to configure the nodes in the network according to the gateway
	 * selection taken place.
	 * 
	 * @param potentialNodes
	 *            the nodes that may become a gateway.
	 * @return
	 */
	public Collection<CloudConfigurationMessage> calculateAndInformGateways(OverlayContacts potentialNodes,
			int maxNumberOfGateways);

	/**
	 * Performs the {@link GatewaySelectionStrategy}.
	 * 
	 * @param potentialNodes
	 * @return
	 */
	public Map<INodeID, List<INodeID>> pickGateways(OverlayContacts potentialNodes, int maxNumberOfGateways);

}
