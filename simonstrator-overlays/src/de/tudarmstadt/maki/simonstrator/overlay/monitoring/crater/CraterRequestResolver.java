package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSDataCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInfoProperties;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInfoProperties.SiSScope;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider.SiSProviderHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.RequestResponseAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.ResponseObject;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.CellularResponseMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestInquiryMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterNodeRoleListener;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterSiSInfoContainer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.ResponseEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.ResponseEventHandler;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.Utils;

/**
 * This class is responsible for pretty much everything w.r.t. Loadgen/ Crater
 * data requests and the following SiS connection It should e.g., take care to
 * hold location based requests, to answer them once it moves into the area.
 * 
 * @author Christoph Storm
 * @version 0.65 -2016.08.03
 */
public class CraterRequestResolver implements CraterNodeRoleListener,
		EventHandler {
	// TODO This should probably implement MonitoringComponent
	// TODO way for infoContainer entries to expire

	private static final int evaluateRequestEvent = 144;
	private static final int removeRequestEvent = 4512;

	private CraterNodeComponent parent;
	private SiSComponent sis;
	private CraterSiSInfoContainer infoContainer;

	private HashMap<SiSType<?>, CraterMonitoringDataCallback<?>> callbacksPerType;

	private double hesitationfactor = 0.1;

	/**
	 * This is here (again) to check whether requests are still valid or have
	 * been removed.<br>
	 * Should also allow checking against *similar* requests
	 */
	private HashSet<Request> requests = new HashSet<Request>();

	/**
	 * Stores all known valid requests, for bootstrappint of new nodes, etc
	 */
	private HashMap<Integer, Request> allValidRequests = new HashMap<Integer, Request>();

	public CraterRequestResolver(CraterNodeComponent parent, double hesitationfactor) {
		this.parent = parent;
		this.hesitationfactor = hesitationfactor;
	}

	public void init() {

		try {
			sis = parent.getHost().getComponent(SiSComponent.class);
		} catch (ComponentNotAvailableException e) {
			// really should not happen
			throw new RuntimeException("No SiS available!");
		}
		infoContainer = new CraterSiSInfoContainer(parent.getHost());
		callbacksPerType = new HashMap<SiSType<?>, CraterMonitoringDataCallback<?>>();
	}

	public void handleIncommingMessage(Message msg) {
		if (msg instanceof CellularResponseMessage) {
			for (ResponseObject response : ((CellularResponseMessage) msg).getResponses()) {
				handleResponse(response);
			}
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void handleResponse(ResponseObject response) {
		/*
		 * 1. get the Info to an analyzer
		 * 
		 * 2. make properties available to SiS
		 */

		// #1 Analyzer
		toAnalyzerResponseReceived(response);

		Monitor.log(getClass(), Level.DEBUG, "Node %s received response to request #%d",
				parent.getHost().getId().toString(), response.getRequestHash());

		// #2
		for (SiSType<?> type : response.getResponseTypes()) {
			Set<ResponseEntry> entries = response.getResponsesForType(type);
			for (ResponseEntry entry : entries) {

				CraterMonitoringDataCallback callback;

				if (!infoContainer.containsType(type)) {
					callback = new CraterMonitoringDataCallback(type);
					SiSProviderHandle sisHandle = sis.provide().nodeState(type, callback);
					callback.setSiSProviderHandle(sisHandle);

					callbacksPerType.put(type, callback);
				} else {
					callback = callbacksPerType.get(type);
				}
				infoContainer.put(type, entry.getIdentifier(), entry.getValue());
				// TODO find a way to do per Node Timestamp updates!
				callback.getInfoProperties().setLastUpdateTimestamp();
			}
		}
	}

	/**
	 * Adds a new Request to be handled
	 * 
	 * @param request
	 */
	public void addRequest(Request request) {

		if (request.getValidity() > Time.getCurrentTime()) {

			// It's possible that this node went offline, and receives its own
			// request later on via bootstrapping
			if (request.getSource().equals(parent.getHost().getId())) {
				// Do NOT answer your own requests!
				return;
			}

			requests.add(request);

			allValidRequests.put(request.hashCode(), request);
			Event.scheduleWithDelay(request.getValidity() - Time.getCurrentTime(), this, request.hashCode(),
					removeRequestEvent);

			evaluateRequest(request);
		}
	}

	/**
	 * Checks whether or not the Request can be answered by this node, and how
	 * to further treat the request. For Example Periodic Requests may need to
	 * be rechecked according to their Interval.
	 * 
	 * @param request
	 */
	private void evaluateRequest(Request request) {

		if (request.getValidity() > Time.getCurrentTime()) {

			// TODO how to handle Event-based requests?

			// Check if node is a receiver
			if (Arrays.asList(request.getReceiver()).contains("ALL")
					|| Arrays.asList(request.getReceiver()).contains(parent.getHost().getId().toString())) {

				if (Utils.matchNodeAgainstSetOfConditions(parent.getHost().getId(), request.getTarget())
						&& parent.isActive()) {
					// TODO way to discern between TargetConditions that may be
					// fulfilled in the future (e.g., Location) and those that
					// will
					// never be fulfilled (e.g., ID)

					getInfo(request);
				}
				// If it was periodic, reAdd after current interval
				if (request.getType().equals(Request.REQUEST_TYPES[0]) &&
				// Alt: Type out "PERIODIC" instead of relying on array index
						Time.getCurrentTime() + request.getInterval() < request.getValidity()) {

					Event.scheduleWithDelay(request.getInterval(), this, request, evaluateRequestEvent);
				} else {
					// TODO what about event based requests?
					requests.remove(request);
				}
			}
		}
		else {
			requests.remove(request);
		}
	}

	/**
	 * Tries to gather the requested Information and put it into a
	 * {@link ResponseObject}
	 * 
	 * @param request
	 */
	private void getInfo(Request request) {

		Monitor.log(getClass(), Level.DEBUG, "Node %s answers request #%d", parent.getHost().getId().toString(),
				request.hashCode());

		// One response per request!
		
		// get the required information, if we have it.
		ResponseObject response = new ResponseObject(request.getSource(), request.hashCode(), request.getValidity());

		SiSType<?> type = Utils.getSiSTypeFromString(request.getAttribute());
		// current requests only hold one requestType
		Object info = null;

		try {
			info = sis.get().localState(type, SiSRequest.NONE);
		} catch (InformationNotAvailableException e) {
			// no Info available, now what?

			Monitor.log(getClass(), Level.DEBUG,
					"Node %s was unable to get requested info!", parent
							.getHost().getId().toString());

			return;
		}
		response.addResponseValue(type, parent.getHost().getId(), info);
		// Add properties to buffer!
		parent.getDataRouting().getLocalBuffer().getResponseObjects().add(response);

		prepareSending(request);

		///////////////////////
		// for analyzer: Do not log unsuccessful response creations
		toAnalyzerResponseGenerated(response, parent.getHost().getId());
	}

	/**
	 * Prepares the nonPeriodic, out-of-line, Data forwarding of the requested
	 * Information
	 * 
	 * @param request
	 */
	private void prepareSending(Request request) {
		// Removed ETA assessment, just send it out after hesitation time
		
		// 'hesistation time' of about 10% to wait for more answers to
		// send...
		long timeToSend;
		if (request.getType().equals(Request.REQUEST_TYPES[0])) {
			timeToSend = (long) (request.getInterval() * hesitationfactor);
		} else {
			// TODO handling of EVENT based requests!
			timeToSend = request.getValidity() - Time.getCurrentTime();
			timeToSend *= hesitationfactor;
		}

		if (parent.getDataRouting().getTimeOfNextScheduledDataMessage() > Time
				.getCurrentTime() + timeToSend) {

			ResponseEventHandler scheduler = parent.getDataRouting()
					.getSendingScheduler();

			scheduler.setDelayToSend(timeToSend);
		}
	}

	@Override
	public void craterNodeRoleChanged(CraterNodeRole craterNodeRole) {
		if (craterNodeRole.equals(CraterNodeRole.OFFLINE)) {
			infoContainer.clear();
			requests.clear();
			allValidRequests.clear();

			// there is no more info, revoke info-providing access
			for (CraterMonitoringDataCallback<?> callback : callbacksPerType.values()) {
				sis.provide().revoke(callback.getSiSHandle());
			}
			callbacksPerType.clear();
		}
	}

	@Override
	public void eventOccurred(Object content, int type) {
		if (type == evaluateRequestEvent && requests.contains((Request) content)) {
			evaluateRequest((Request) content);
		} else if (type == removeRequestEvent) {
			allValidRequests.remove((Integer) content);
		}
	}

	public Set<Integer> getAllVallidRequestHashes() {
		return new HashSet<Integer>(allValidRequests.keySet());
	}

	/**
	 * Returns the request with the given hash, if it is known and valid.
	 * Returns <b>null</b> otherwise
	 * 
	 * @param requestHash
	 */
	public Request getRequestForHash(int requestHash) {
		return allValidRequests.get(requestHash);
	}

	public void handleSinkMessageRequestHashes(Set<Integer> requestHashes) {
		// A: HashSet includes hashes of unknown Requests
		// -> inquire requests for these hashes from remote nodes

		// B: HashSet is missing hash of known Requests
		// -> (re)send request

		Set<Integer> missing = new HashSet<Integer>(requestHashes);
		missing.removeAll(allValidRequests.keySet());
		// missing now only includes request that are (known to be) unknown
		if (!missing.isEmpty()) {
			// A!
			// generate Get! Message and sound out!
			RequestInquiryMessage inquiry = new RequestInquiryMessage(parent.getLocalOverlayContact(), missing);
			parent.sendLocalBroadcast(inquiry);
		}

		missing = getAllVallidRequestHashes();
		missing.removeAll(requestHashes);
		// Now, missing includes known RequestHashes unknown to the sender
		if (!missing.isEmpty()) {
			// B!
			// send out RequestMessages
			// TODO single message for several Requests?
			for (int i : missing) {
				Request request = allValidRequests.get(i);
				parent.getRequestRouting().sendRequest(request);
			}
		}
	}


	// No Idea if this is correct... u_u
	private class CraterMonitoringDataCallback<T> implements SiSDataCallback<T> {

		private SiSType<T> type;

		private SiSProviderHandle sisHandle;

		private final SiSInfoProperties properties = new SiSInfoProperties().setScope(SiSScope.NODE_LOCAL);

		protected CraterMonitoringDataCallback(SiSType<T> type) {
			this.type = type;
		}

		@Override
		public T getValue(INodeID nodeID, SiSProviderHandle providerHandle) throws InformationNotAvailableException {
			return infoContainer.get(type, nodeID);
		}

		@Override
		public Set<INodeID> getObservedNodes() {
			return infoContainer.getAllObservedNodesForType(type);
		}

		@Override
		public SiSInfoProperties getInfoProperties() {
			return properties;
		}

		public void setSiSProviderHandle(SiSProviderHandle sisHandle) {
			this.sisHandle = sisHandle;
		}

		public SiSProviderHandle getSiSHandle() {
			return sisHandle;
		}

	}

	/////////////////////////////////////////////////////////////////////////
	// Analyzing convenience methods.
	// Blatant copy of the DataRouting methods
	///////////////////////////////////////////////////////////////////////////

	private boolean checkedForRequestResponseAnalyzer = false;

	private RequestResponseAnalyzer requestResponseAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasRequestResponseAnalyzer() {
		if (requestResponseAnalyzer != null) {
			return true;
		} else if (checkedForRequestResponseAnalyzer) {
			return false;
		}
		getRequestResponseAnalyzer();
		return requestResponseAnalyzer != null;
	}

	public RequestResponseAnalyzer getRequestResponseAnalyzer() {
		if (!checkedForRequestResponseAnalyzer) {
			try {
				requestResponseAnalyzer = Monitor.get(RequestResponseAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				requestResponseAnalyzer = null;
			}
			checkedForRequestResponseAnalyzer = true;
		}
		return requestResponseAnalyzer;
	}

	private void toAnalyzerResponseGenerated(ResponseObject response, INodeID responder) {
		if (hasRequestResponseAnalyzer()) {
			getRequestResponseAnalyzer().onResponseGeneratedForRequest(response, responder);
		}
	}

	private void toAnalyzerResponseReceived(ResponseObject response) {
		if (hasRequestResponseAnalyzer()) {
			getRequestResponseAnalyzer().onResponseReceivedAtRequester(response);
		}
	}

}
