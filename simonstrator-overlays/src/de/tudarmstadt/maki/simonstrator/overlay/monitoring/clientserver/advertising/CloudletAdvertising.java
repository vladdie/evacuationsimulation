package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.advertising;

import java.util.AbstractMap;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.exceptions.NoKnownSinksException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.advertising.CloudletAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.util.ClientServerNodeRoleListener;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.ITimeoutMapListener;

public interface CloudletAdvertising extends ClientServerNodeRoleListener {
	public void initialize();

	/**
	 * Handles incoming {@link CloudletAdvertisingMessage} that is dispatched in
	 * {@link ClientServerNodeComponent} to this module. - maintaining sink
	 * table
	 * 
	 * @param msg
	 */
	public void handleIncomingCloudletAdvMsg(CloudletAdvertisingMessage msg);

	/**
	 * @return <code>true</code> if sink table is empty. <code>false</code> if
	 *         sink table contains entries.
	 */
	public boolean sinkTableIsEmpty();

	/**
	 * Add listener to {@link TimeoutMap} that need to be informed whenever the
	 * sinkTable got empty <b>(from size >= 1 to size = 0)</b> or was got filled
	 * <b>(from size = 0 to size >= 1)</b>
	 * 
	 * @param l
	 */
	public void addSinkTableListener(ITimeoutMapListener l);

	/**
	 * Return the sinkId of the {@link SinkTableEntry} that has the largest
	 * gradient, while at the same time has a minimum of X successive updates?
	 * If there is not such an entry do with {@link SinkTableEntry} until (with
	 * less successive updates) one is found.
	 * 
	 * @return best possible sinkTable entry<id, currentGradient> or
	 *         {@link NoKnownSinksException} in case of empty sink table!
	 * @throws NoKnownSinksException
	 */
	public AbstractMap.SimpleEntry<INodeID, Double> getBestSinkTableEntry() throws NoKnownSinksException;
}
