package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attributegenerator;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attributegenerator.measurementprovider.IMeasurementProvider;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attributegenerator.measurementprovider.IMeasurementProviderFactory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.NoSuchIdentifierException;

public class AttributeGenerator extends AbstractAttributeGenerator {
	
	LinkedList<IMeasurementProvider> provs;

	public AttributeGenerator(Host host, LinkedList<IMeasurementProvider> provs) {
		super(host);
		this.provs = provs;
	}

	@Override
	public void initialize() {

	}
	
	@Override
	public void shutdown() {
		// not needed
	}

	public IMeasurementProvider getMeasurementProvider(String identifier) throws NoSuchIdentifierException{
		for (IMeasurementProvider actMeasurementProvider : provs) {
			if(actMeasurementProvider.getIdentifier().equals(identifier)){
				return actMeasurementProvider;
			}
		}
		throw new NoSuchIdentifierException(identifier);
	}
	
	public LinkedList<IMeasurementProvider> getProvs() {
		return provs;
	}

	/**
	 * Factory for {@link AttributeGenerator}
	 * 
	 * @author Nils Richerzhagen
	 * @version 1.0, 21.08.2014
	 * 
	 */
	public static class Factory implements HostComponentFactory {

		LinkedList<IMeasurementProviderFactory> provFs = new LinkedList<IMeasurementProviderFactory>();

		@Override
		public HostComponent createComponent(Host host) {
			LinkedList<IMeasurementProvider> provs = new LinkedList<IMeasurementProvider>(); 
			for (IMeasurementProviderFactory provF : provFs)
				provs.add(provF.getMeasurementProviderFor(host));

			return new AttributeGenerator(host, provs);
		}

		public void setMeasurementProvider(IMeasurementProviderFactory f) {
			provFs.add(f);
		}

	}
}
