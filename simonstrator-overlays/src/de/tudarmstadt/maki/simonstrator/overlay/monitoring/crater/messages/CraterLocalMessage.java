package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api.CraterMessage;

/**
 * Marker interface for Crater Messages that are used within the network (Sink
 * Adv, Node Beaconing, Monitoring data msgs)
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 05.06.2014
 *
 */
public interface CraterLocalMessage extends CraterMessage {
	/*
	 * Marker interface!
	 */
}
