package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.api;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.analyzer.PerNodeRoleTimesAnalyzer;

/**
 * Class to maintain the {@link SubComponentState} of a
 * {@link ClientServerSubComponent} like e.g. the and the
 * 
 * Only for {@link ClientServerNodeComponent}.
 * 
 * @author Nils Richerzhagen
 *
 */
public abstract class ClientServerSubComponent {

	private ClientServerNodeComponent parentComponent;

	private SubComponentState componentState;

	public ClientServerSubComponent(ClientServerNodeComponent parentComponent) {
		this.parentComponent = parentComponent;
	}

	public ClientServerNodeComponent getParentComponent() {
		return parentComponent;
	}

	protected void setComponentState(SubComponentState componentState) {
		this.componentState = componentState;

		if (hasPerNodeStateTimeAnalyzer()) {
			getPerNodeStateTimeAnalyzer().onSubComponentStateChange(this, componentState);
		}
	}

	public SubComponentState getComponentState() {
		return componentState;
	}

	public boolean isInactive() {
		return getComponentState() == SubComponentState.INACTIVE;
	}

	public boolean isActive() {
		return this.getComponentState() == SubComponentState.ACTIVE;
	}

	public boolean isIdle() {
		return this.getComponentState() == SubComponentState.IDLE;
	}

	public enum SubComponentState {
		/**
		 * Inactive
		 */
		INACTIVE,

		/**
		 * Active
		 */
		ACTIVE,

		/**
		 * Idle
		 */
		IDLE
	}

	/*
	 * Analyzing convenience methods.
	 */

	private boolean checkedForPerNodeStateTimeAnalyzer = false;

	private PerNodeRoleTimesAnalyzer perNodeStateTimeAnalyzer = null;

	/**
	 * True, if a {@link PerNodeRoleTimesAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasPerNodeStateTimeAnalyzer() {
		if (perNodeStateTimeAnalyzer != null) {
			return true;
		} else if (checkedForPerNodeStateTimeAnalyzer) {
			return false;
		}
		getPerNodeStateTimeAnalyzer();
		return perNodeStateTimeAnalyzer != null;
	}

	public PerNodeRoleTimesAnalyzer getPerNodeStateTimeAnalyzer() {
		if (!checkedForPerNodeStateTimeAnalyzer) {
			try {
				perNodeStateTimeAnalyzer = Monitor.get(PerNodeRoleTimesAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				perNodeStateTimeAnalyzer = null;
			}
			checkedForPerNodeStateTimeAnalyzer = true;
		}
		return perNodeStateTimeAnalyzer;
	}

}
