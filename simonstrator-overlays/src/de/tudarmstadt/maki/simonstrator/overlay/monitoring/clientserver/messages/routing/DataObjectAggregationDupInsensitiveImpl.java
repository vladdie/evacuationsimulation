package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.AggregationFunctionMax;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.AggregationFunctionMin;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.DuplicateInsensitiveAggregationFunction;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.DuplicateInsensitiveAggregationFunction.DuplicateInsensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.exceptions.AggregationFunctionNotKnownException;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 30.06.2014
 */
public class DataObjectAggregationDupInsensitiveImpl implements DataObjectAggregationDupInsensitive<Double> {

	private DuplicateInsensitiveAggregationFunction<Double> aggregationFunction;
	private int attributeId;

	/**
	 * Initializing constructor for a DataObject with duplicate insensitive
	 * aggregation.
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param value
	 * @throws AggregationFunctionNotKnownException
	 */
	public DataObjectAggregationDupInsensitiveImpl(String aggregationFunctionId, int attributeId, Double value)
			throws AggregationFunctionNotKnownException {
		this.attributeId = attributeId;

		if (aggregationFunctionId.equals(DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.getId())) {
			aggregationFunction = new AggregationFunctionMax(value);
		} else if (aggregationFunctionId.equals(DuplicateInsensitiveAggregationFunctionsDescriptions.MIN.getId())) {
			aggregationFunction = new AggregationFunctionMin(value);
		} else {
			throw new AggregationFunctionNotKnownException(aggregationFunctionId);
		}
	}

	@Override
	public void putValue(Double value) {
		aggregationFunction.putValue(value);
	}

	@Override
	public int getAttributeId() {
		return attributeId;
	}

	@Override
	public String getAggregationFunctionId() {
		return aggregationFunction.getId();
	}
	
	@Override
	public String getAggregationFunctionDescription() {
		return aggregationFunction.getDescription();
	}

	@Override
	public Double getAggregateValue() {
		return aggregationFunction.getAggregateValue();
	}

	@Override
	public boolean ofSameType(DataObjectAggregation<Double> toCompareAggregationObject) {
		if (this.getAggregationFunctionId().equals(toCompareAggregationObject.getAggregationFunctionId())
				&& this.getAttributeId() == toCompareAggregationObject.getAttributeId())
			return true;
		else 
			return false;
	}
}
