package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;

/**
 * A implementation of {@link AbstractContentionScheme} based on devices remaining
 * energy
 * 
 * @author Jonas Huelsmann
 */
public class DefaultCraterContentionScheme extends AbstractContentionScheme {

	private final CraterNodeComponent parentComponent;

	public DefaultCraterContentionScheme(long min, long max, CraterNodeComponent parentComponent) {
		super(min, max);
		this.parentComponent = parentComponent;
	}

	/**
	 * Returning the current calculated hesitation time. For last hop, a randomness is included as gradient is mostly
	 * the same there. Meaning the nodes with gradient 9 forward.
	 */
	public long getHesitationTime(double gradient) {
		if (gradient > 8) {
			double weightRandomness = 0.4;
			double weightGradient = 0;
			double weightBattery = 0.6;
			int randomValue = 0 + (int) (this.getRandom().nextDouble() * ((100 - 0) + 1));
			double hesFactor = (weightBattery
					* (100 - parentComponent.getCurrentBatteryPercentage())
					+ weightRandomness * randomValue + weightGradient
					* (100 - gradient)) / 100;
			long hesitationTime = MIN_DATA_ROUTING_HESITATION_TIME
					+ (long) (hesFactor * (MAX_DATA_ROUTING_HESITATION_TIME - MIN_DATA_ROUTING_HESITATION_TIME));
			return hesitationTime;
		} else {
			double weightGradient = 0.7;
			double weightBattery = 0.3;
			double hesFactor = (weightBattery
					* (100 - parentComponent.getCurrentBatteryPercentage()) + weightGradient
					* (100 - gradient)) / 100;
			long hesitationTime = MIN_DATA_ROUTING_HESITATION_TIME
					+ (long) (hesFactor * (MAX_DATA_ROUTING_HESITATION_TIME - MIN_DATA_ROUTING_HESITATION_TIME));
			return hesitationTime;
		}
	}

}
