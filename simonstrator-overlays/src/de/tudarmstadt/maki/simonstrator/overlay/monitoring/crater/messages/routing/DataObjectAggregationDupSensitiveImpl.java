package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionMean;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionNodeCount;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionNodeCountNoReferenceExchange;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.DuplicateSensitiveAggregationFunction;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.DuplicateSensitiveAggregationFunction.DuplicateSensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.AggregationFunctionNotKnownException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.MergeNotPossibleException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainer.MergePoint;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 * 
 */
public class DataObjectAggregationDupSensitiveImpl implements DataObjectAggregationDupSensitive<Double> {

	private DuplicateSensitiveAggregationFunction<Double> aggregationFunction;
	private int attributeId;

	/**
	 * Initializing constructor for a DataObject with duplicate sensitive
	 * aggregation.
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param toAddEntry
	 * @throws AggregationFunctionNotKnownException
	 */
	public DataObjectAggregationDupSensitiveImpl(String aggregationFunctionId, int attributeId, AggregationFunctionEntry toAddEntry)
			throws AggregationFunctionNotKnownException {
		this.attributeId = attributeId;

		if (aggregationFunctionId.equals(DuplicateSensitiveAggregationFunctionsDescriptions.MEAN.getId())) {
			aggregationFunction = new AggregationFunctionMean(toAddEntry);
		} else if (aggregationFunctionId.equals(DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId())) {
			aggregationFunction = new AggregationFunctionNodeCount(toAddEntry);
		} else if (aggregationFunctionId.equals(DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT_NOREF.getId())) {
			aggregationFunction = new AggregationFunctionNodeCountNoReferenceExchange(toAddEntry);
		} else {
			throw new AggregationFunctionNotKnownException(aggregationFunctionId);
		}
	}

	/**
	 * Clone constructor.
	 * 
	 * @param attributeId
	 * @param aggregationFunction
	 */
	public DataObjectAggregationDupSensitiveImpl(int attributeId, DuplicateSensitiveAggregationFunction<Double> aggregationFunction) {
		this.attributeId = attributeId;
		this.aggregationFunction = aggregationFunction;
	}

	@Override
	public int getAttributeId() {
		return attributeId;
	}

	@Override
	public String getAggregationFunctionId() {
		return aggregationFunction.getId();
	}

	@Override
	public String getAggregationFunctionDescription() {
		return aggregationFunction.getDescription();
	}

	@Override
	public Double getAggregateValue() {
		return aggregationFunction.getValue();
	}

	@Override
	public void putValue(AggregationFunctionEntry toAddEntry, MergePoint mergePoint) {
		aggregationFunction.putValue(toAddEntry, mergePoint);
	}

	@Override
	public LinkedList<AggregationFunctionEntry> getAllIncludedEntriesForMerge() {
		return aggregationFunction.getAllIncludedEntriesForMerge();
	}

	@Override
	public LinkedList<AggregationFunctionEntry> getCurrentUsedEntries() {
		return aggregationFunction.getUsedEntries();
	}

	@Override
	public LinkedList<UniqueID> getIncludedEntriesIDs() {
		return aggregationFunction.getIncludedEntriesIDs();
	}

	@Override
	public boolean ofSameType(DataObjectAggregation<Double> toCompareAggregationObject) {
		if (this.getAggregationFunctionId().equals(toCompareAggregationObject.getAggregationFunctionId())
				&& this.getAttributeId() == toCompareAggregationObject.getAttributeId())
			return true;
		else
			return false;
	}

	@Override
	public void mergeValues(DataObjectAggregationDupSensitive<Double> toMergeDataObject, MergePoint mergePoint)
			throws MergeNotPossibleException {
		// Check again for same type.
		if (ofSameType(toMergeDataObject)) {
			for (AggregationFunctionEntry actEntry : toMergeDataObject.getAllIncludedEntriesForMerge()) {
				this.putValue(actEntry, mergePoint);
			}
		} else
			throw new MergeNotPossibleException();

	}

	public DataObjectAggregationDupSensitiveImpl clone() {
		DataObjectAggregationDupSensitiveImpl cloneObject = new DataObjectAggregationDupSensitiveImpl(this.getAttributeId(),
				this.aggregationFunction.clone());
		return cloneObject;
	}

	@Override
	public long getMessageSize() {
		// * Dup Sens: short attributeId +2, byte aggregationFunction,
		// map<double
		// * values, short nodeId> +10
		long bytesAggDupSens = 0;

		bytesAggDupSens += 2; // attributeID
		if (aggregationFunction.getId().equals(DuplicateSensitiveAggregationFunctionsDescriptions.MEAN.getId())) {
			// + 10 for each current entry in aggFunc
			bytesAggDupSens += aggregationFunction.getUsedEntries().size() * 10;
			// +8 for each reference storing entry
			bytesAggDupSens += aggregationFunction.getIncludedEntriesIDs().size() * 8;
		} else if (aggregationFunction.getId().equals(DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId())) {
			// +2 for each nodeID stored in node count
			bytesAggDupSens += aggregationFunction.getUsedEntries().size() * 2;
			// +2 for each nodeID in reference storing
			bytesAggDupSens += aggregationFunction.getIncludedEntriesIDs().size() * 2;
		} else if (aggregationFunction.getId().equals(DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT_NOREF.getId())) {
			/*
			 * 2 for number of nodes without reference storing, thus only
			 * counter ++ and for that a short should be sufficient.
			 */
			bytesAggDupSens += 2;
		} else {
			try {
				throw new AggregationFunctionNotKnownException(aggregationFunction.getId());
			} catch (AggregationFunctionNotKnownException e) {
				e.printStackTrace();
			}
		}
		return bytesAggDupSens;
	}
}
