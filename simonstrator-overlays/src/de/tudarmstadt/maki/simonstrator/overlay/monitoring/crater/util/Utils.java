package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util;

import java.lang.reflect.ParameterizedType;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;

/**
 * Utils for handling and executing conditions on nodes
 * 
 * @author Marc Schiller
 *
 */
public class Utils {
	
	private Utils() {
		
	}

	// A list of all valid comparators
	public static final String[] VALID_COMPARATORS = { ">", ">=", "=", "==",
			"<", "<=", "!=", "<>" };

	/**
	 * Check if a host fulfills the the given condition
	 * 
	 * @param nodeId
	 *            the nodeID to check
	 * @param condition
	 *            the condition that should be matched
	 * @return if the node matches the condition
	 */
	public static boolean compareHostToCondition(INodeID nodeId,
			NodeCondition condition) {
		// Fetch the SiSType that should be compared
		SiSType<?> sistype = getSiSTypeFromString(condition.getParameter());

		if (sistype == null)
			return false;

		// Get the return type of the attribute
		Class<?> typeOfSis = getGenericTypeFromName(condition.getParameter());

		if (typeOfSis == null) {
			return false;
		}

		try {
			// Get the value from the node
			Object value = Oracle.getHostByID(nodeId)
					.getComponent(SiSComponent.class).get()
					.localState(sistype, SiSRequest.NONE);

			if (typeOfSis.equals(Double.class)) {
				// Compare Double SiSType
				Double dbl = (Double) value;

				int result = Double.compare(dbl, condition.getDoubleValue());

				switch (condition.getComparator()) {
				case "=":
				case "==":
					return (result == 0);
				case ">":
					return (result > 0);
				case ">=":
					return (result >= 0);
				case "<":
					return (result < 0);
				case "<=":
					return (result <= 0);
				case "<>":
				case "!=":
					return (result != 0);
				default:
					return false;
				}
			} else if (typeOfSis.equals(String.class)) {
				// Compare String SiSTType
				String str = (String) value;

				switch (condition.getComparator()) {
				case "=":
				case "==":
					return str.equals(condition.getValue());
				case "!=":
				case "<>":
					return !str.equals(condition.getValue());
				default:
					return false;
				}
			} else if (typeOfSis.equals(Location.class)) {
				Location loc = (Location) value;

				switch (condition.getComparator()) {
				case "=":
				case "==":
					return loc
							.distanceTo(condition.createLocation()) == condition
									.getDoubleValue();
				case "!=":
					return loc
							.distanceTo(condition.createLocation()) != condition
									.getDoubleValue();
				case "<":
					return loc
							.distanceTo(condition.createLocation()) < condition
									.getDoubleValue();
				case ">":
					return loc
							.distanceTo(condition.createLocation()) > condition
									.getDoubleValue();
				case "<=":
					return loc
							.distanceTo(condition.createLocation()) <= condition
									.getDoubleValue();
				case ">=":
					return loc
							.distanceTo(condition.createLocation()) >= condition
									.getDoubleValue();
				default:
					return false;
				}
			} else {
				System.out.println("Unhandled comparison");
				return false;
			}
		} catch (InformationNotAvailableException
				| ComponentNotAvailableException e) {
			// Can't get info so it does not match the condition
			return false;
		}
	}

	/**
	 * Get the SiSType Object from SiSTypes by a String name
	 * 
	 * @param sistype
	 *            the name of the SiSType to fetch
	 * @return the SiSType or null if it doesn't exist
	 */
	public static SiSType<?> getSiSTypeFromString(String sistype) {
		try {
			// Reflection magic :D
			return (SiSType<?>) SiSTypes.class.getField(sistype)
					.get(SiSTypes.class);
		} catch (NoSuchFieldException | SecurityException
				| IllegalArgumentException | IllegalAccessException e) {
			// Requested field does not exist or access is not allowed (e.g
			// private field)
			return null;
		}

	}

	/**
	 * Get the generic type of SiSType field
	 * 
	 * @param the
	 *            name of the SiSType to fetch
	 * @return the Class of the generic SiSType or null if the SiSTye doesn't
	 *         exist
	 */
	public static Class<?> getGenericTypeFromName(String sisType) {
		try {
			// More reflection magic
			return (Class<?>) ((ParameterizedType) SiSTypes.class
					.getField(sisType).getGenericType())
							.getActualTypeArguments()[0];
		} catch (NoSuchFieldException | SecurityException e) {
			return null;
		}
	}

	/**
	 * Match a Set of conditions against a Node
	 * 
	 * @param nodeID
	 *            the NodeID of the node to be checked
	 * @param conditions
	 *            a set of conditions to be checked
	 * @return if the node matches ALL given conditions
	 */
	public static boolean matchNodeAgainstSetOfConditions(INodeID nodeID,
			Set<? extends NodeCondition> conditions) {

		for (NodeCondition cond : conditions) {
			// Early return if one condition is not met
			if (!compareHostToCondition(nodeID, cond)) {
				return false;
			}
		}

		// All conditions are met -> success
		return true;
	}
}
