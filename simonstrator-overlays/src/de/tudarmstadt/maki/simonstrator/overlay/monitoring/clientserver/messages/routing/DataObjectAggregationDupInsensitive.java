package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 *
 * @param <T>
 */
public interface DataObjectAggregationDupInsensitive<T> extends DataObjectAggregation<T>{

	/**
	 * Put a new value to the duplicate insensitve object. According to the
	 * internally stored aggregation function this value will be processed.
	 * 
	 * @param value
	 */
	public void putValue(T value);
	
}
