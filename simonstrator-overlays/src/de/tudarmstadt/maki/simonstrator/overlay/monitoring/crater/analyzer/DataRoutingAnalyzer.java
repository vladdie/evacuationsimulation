/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.cloudlet.CraterCloudletComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerLocalImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainerUploadImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObject;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupInsensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupSensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectImpl;

/**
 * Analyzer for the in network data routing of Crater.
 * 
 * @author Nils Richerzhagen
 * 
 */
public interface DataRoutingAnalyzer extends Analyzer {

	/**
	 * {@link DataMessageContainerUploadImpl} send from {@link CraterNodeComponent} or {@link CraterCloudletComponent}.
	 * 
	 * @param uniqueId
	 * @param comp
	 */
	public void uploadMsgSend(UniqueID uniqueId, CraterComponent comp);

	/**
	 * {@link DataMessageContainerUploadImpl} rec by central entity.
	 * 
	 * @param uniqueId
	 */
	public void uploadMsgReceived(UniqueID uniqueId);

	/**
	 * Called whenever an initial data message is send.
	 * 
	 * @param uniqueMsgId
	 */
	public void onContainerMessageSend(UniqueID uniqueMsgId, INodeID sinkId);

	/**
	 * Called whenever a data message is received by a sink.
	 * 
	 * @param uniqueMsgId
	 * @param sinkId
	 */
	public void onContainerMessageReceivedBySink(UniqueID uniqueMsgId, INodeID sinkId);

	/**
	 * Called whenever a sink nodes received for more than the first an incoming
	 * data message.
	 */
	public void onSuccessiveContainerMessageReceivedBySink(UniqueID uniqueMsgId);

	/**
	 * Called whenever a node adds or includes normal data to a
	 * {@link DataMessageContainerLocalImpl} in form of a new {@link DataObject}
	 * .
	 * 
	 * @param dataObject
	 */
	public void onNormalDataObjectAddedInNetwork(UniqueID dataObjectID);

	/**
	 * Called whenever a node adds or includes aggregation duplicate insensitive
	 * data to a {@link DataMessageContainerLocalImpl}.
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param value
	 */
	public void onDuplicateInsensitiveAggregationDataAddedInNetwork(String aggregationFunctionId, int attributeId, double value);

	/**
	 * Called whenever a node adds or includes aggregation duplicate sensitive
	 * data to a {@link DataMessageContainerLocalImpl}.
	 * @param addedEntry
	 */
	public void onDuplicateSensitiveAggregationDataAddedInNetwork(UniqueID addedEntry);

	/**
	 * Called whenever a node is sending a new
	 * {@link DataMessageContainerLocalImpl} with added data in form of a new
	 * {@link DataObject}. Use this to see if really only first added data is
	 * duplicates.
	 * 
	 * @param dataObject
	 */
	public void onNormalDataObjectAddedFirstTime(UniqueID dataObjectID);

	/**
	 * Called whenever a node is sending a new
	 * {@link DataMessageContainerLocalImpl} with added data in form of a new
	 * {@link DataObjectAggregationDupSensitiveImpl}. Use this to see if really
	 * only first added data is duplicates.
	 * 
	 * @param addedEntry
	 */
	public void onAggregationDupSensitiveEntryAddedFirstTime(AggregationFunctionEntry addedEntry);

	/**
	 * Called whenever a sink uploads its received
	 * {@link DataMessageContainerLocalImpl} and gives the including data to the
	 * analyzer. This is the data added after sinks try to resolve duplicates by
	 * comparing with the current buffer the sink has.
	 * 
	 * @param normalDataList
	 * @param aggregationDupInsensitiveDataList
	 * @param aggregationDupSensitiveDataList
	 */
	public void onMessageContentUploadedAtSink(LinkedList<UniqueID> normalDataList,
			LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveDataList,
			LinkedList<LinkedList<UniqueID>> aggregationDupSensitiveEntryIDList);

	// LinkedList<DataObjectAggregationDupSensitiveImpl>
	// aggregationDupSensitiveDataList);

	/**
	 * Called whenever the server receives a
	 * {@link DataMessageContainerUploadImpl} message which includes data.
	 * 
	 * @param normalDataList
	 * @param aggregationDupInsensitiveDataList
	 * @param aggregationDupSensitiveDataList
	 */
	public void onMessageContentReceivedAtServer(LinkedList<UniqueID> normalDataList,
			LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveDataList,
			LinkedList<DataObjectAggregationDupSensitiveImpl> aggregationDupSensitiveDataList);

	/**
	 * Called by the individual aggregation functions, when data is added or
	 * merged at sink level.
	 */
	public void onAggregationDupSensitiveDuplicateDetectedAtSink();

	/**
	 * Called by the individual aggregation functions when data is added or
	 * merged at central level.
	 */
	public void onAggregationDupSensitiveDuplicateDetectedAtServer();

	/**
	 * Called when the local upload buffer of a sink already contains a specific
	 * {@link DataObjectImpl}.
	 */
	public void onNormalDataDuplicateDetectedAtSink();

	/**
	 * Called whenever a sink adds or includes normal data to a
	 * {@link DataMessageContainerLocalImpl} in form of a new {@link DataObject}
	 * .
	 * 
	 * @param dataObject
	 */
	public void onNormalDataObjectAddedAtSink(UniqueID dataObjectID);

	/**
	 * Called whenever a sink adds or includes aggregation duplicate insensitive
	 * data to a {@link DataMessageContainerLocalImpl}.
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param value
	 */
	public void onDuplicateInsensitiveAggregationDataAddedAtSink(String aggregationFunctionId, int attributeId, double value);

	/**
	 * Called whenever a sink adds or includes aggregation duplicate sensitive
	 * data to a {@link DataMessageContainerLocalImpl}.
	 * 
	 * @param addedEntryID
	 */
	public void onDuplicateSensitiveAggregationEntryAddedAtSink(UniqueID addedEntryID);

	
	/******************************************************************************************
	 ******************************* LATENCY related methods. *********************************
	 *******************************************************************************************/
	
	/**
	 * For latency calculation from origin to sink.
	 * 
	 * @param dataObjectID
	 */
	public void onNormalDataObjectReceivedAtSink(UniqueID dataObjectID);
	
	/**
	 * For latency calculation from origin to sink.
	 * 
	 * @param 
	 */
	public void onDupSensEntriesReceivedAtSink(LinkedList<UniqueID> includedEntriesIDs);

}
