package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterCentralComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api.CraterMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.cloudlet.CraterCloudletComponent;

/**
 * Marker interface for Crater Messages that are used for communication between {@link CraterCentralComponent} and
 * {@link CraterCloudletComponent}s.
 * 
 *
 * @author Nils Richerzhagen
 *
 */
public interface CraterBackendMessage extends CraterMessage {
	/*
	 * Marker Interface
	 */
}
