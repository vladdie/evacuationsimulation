# Latency optimization in mobile networks

_Based on the Bachelor Thesis 'Latency optimization in mobile networks' by Jonas Huelsmann

##The AbstractSinkPreference

###Introduction to Static Network Access Points and the AbstractSinkPreference
When CRATER was introduced static network access points like Wireless LAN routers had not been taken into consideration. Leaves selected their sink regarding their hop distance towards them or the sink's quality from their sink table.
With the introduction of the static network access points to CRATER there had not been added any kind of preference making use of the sink's type. The interface `SinkPreference` now provides the methods needed for a concrete sink preference implementations fitting the individual needs.

###Implementation and extending of the AbstractSinkPreference and refactoring of SinkTableEntry.
`AbstractSinkPreference` is constructed with an int value called `BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES` and a Map<INodeID, SinkTableEntry> called `sinkTable`. `BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES` is the number a sink should be heard. All sinks heard at least `BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES` amount of times will be treated equally. If there are no such sinks the sinks heard less amount of times will also be taken into consideration when a sink is selected.
`AbstractSinkPreference` provides the method 'setSinkTable' to update `sinkTable` after an instance of AbstractSinkPreference is established.
The method `getBestSink` selects the best sink from `sinkTable` and returns it as an AbstractMap.SimpleEntry<INodeID, Double>. The concrete implementation of `getBestSink` in the subclasses of `AbstractSinkPreference` determine the metrics used tos decide which sink is to be considered as the best sink.
In addition, the `SinkPreference` contains an enumeration called `SinkPreferenceType` listing all concrete subclasses of `SinkPreference`.
The class `SinkTableEntry` which was a private class inside the `SinkAdvertisingImpl` class before hat been moved to the sinkpreference package and was made into an public class to make it accessible for the `AbstractSinkPreference`.
Furthermore a boolean called `isStatic` was added and the constructors have been updated to require that value for construction. The method `getSinkMobility` was added to get the information if a `SinkTableEntry` is mobile ('true') or static ('false').

###Subclass implementation
The subclass of `AbstractSinkPreference` called `DefaultPreferenceGradient` is used to select the sink with the lowest gradient and therefore the sink with the shortest hop-distance from the leaf. Mobile nodes and static network access points are treated equally: beside the amount a sink was heard only the gradient determines if a sink is better then another.

The subclass `DefaultPreferenceQuality` will select the sink from `sinkTable` with the best quality. Only the number a sink was heard and the sink's quality are taken into consideration, when selecting the best sink. Note, that Static Network Access Points tend to have a good quality and therefore will be chosen more frequently.

The subclass `StrictStaticPreferenceGradient` will select the sink with the lowest gradient strictly preferring Static Network Access Points. Static Access Points will always be preferred if they have been heard the required amount of times: a Static Access Point will be considered better than a mobile one even so it has a much worse gradient!

The subclass `StrictStaticPreferenceQuality` will behave like the `StrictStaticPreferenceGradient` using the sink's quality instead of its gradient.

###Using the AbstractSinkPreference
To use the `AbstractSinkPreference` the value of `CRA_SINK_PREF` in the `system_parameters.xml` needs to be set to the desired value from the `SinkPreferenceType` enumeration in the `SinkPreference` interface.
New subclasses of `AbstractSinkPreference` should be added to the `SinkPreferenceType`, as well as to the method `initializeSinkPreference` in the class `SinkAdvertisingImpl`, which is used to set the used subclass according to `CRA_SINK_PREF`. 

** Note: `SinkTableEntry` is also used in the `SinkAdvertingImpl` class. Changes of the `SinkTableEntry` class may affect the SinkAdvertingImpl. **

