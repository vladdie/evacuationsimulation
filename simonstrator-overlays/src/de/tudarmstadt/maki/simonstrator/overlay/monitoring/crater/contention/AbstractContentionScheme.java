package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.contention;

import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Randoms;

/**
 * Abstract contention scheme for sender-based contention
 * 
 * @author Jonas Huelsmann
 *
 */
public abstract class AbstractContentionScheme implements ContentionScheme {

	protected final long MIN_DATA_ROUTING_HESITATION_TIME;
	protected final long MAX_DATA_ROUTING_HESITATION_TIME;
	private final Random random;

	public AbstractContentionScheme(long min, long max) {
		MIN_DATA_ROUTING_HESITATION_TIME = min;
		MAX_DATA_ROUTING_HESITATION_TIME = max;
		this.random = Randoms.getRandom(this);
	}

	@Override
	public abstract long getHesitationTime(double gradient);

	public Random getRandom() {
		return random;
	}

}
