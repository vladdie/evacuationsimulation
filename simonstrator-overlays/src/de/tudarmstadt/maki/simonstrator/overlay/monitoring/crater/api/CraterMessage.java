package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.api;

import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;

/**
 * Marker Interface to see which messages are from the Overlay "Crater".
 * 
 * @author Nils Richerzhagen
 *
 */
public interface CraterMessage extends OverlayMessage {
	// marker
}
