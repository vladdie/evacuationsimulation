/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer;

import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterNodeRoleListener;

/**
 * This analyzer is used to obtain the times a Node has had no sink connection during simulation time.
 * 
 * @author Nils Richerzhagen
 */
public interface PerNodeHasNoSinkTimesAnalyzer extends Analyzer {

	/**
	 * Called whenever the sinkTable is emptied. Thus, beginning of noSinkTime.
	 * 
	 * @param craterNodeId
	 */
	public void onSinkTableGotEmpty(CraterNodeComponent comp);

	/**
	 * Called whenever the sinkTable is filled again. Thus, end of noSinkTime.
	 * 
	 * @param craterNodeId
	 */
	public void onSinkTableGotFilled(CraterNodeComponent comp);

	/**
	 * Called by the {@link CraterNodeComponent} when the role changed. The {@link CraterNodeRoleListener} is not used
	 * as it does not return a {@link CraterNodeComponent} which is needed here for ID reasons.
	 * 
	 * @param comp
	 * @param craterNodeRole
	 */
	public void onCraterNodeRoleChanged(CraterNodeComponent comp, CraterNodeRole craterNodeRole);

}
