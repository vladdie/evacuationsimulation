package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.analyzer;

import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;

/**
 * Marker Analyzer
 * 
 * @author Nils Richerzhagen
 *
 */
public interface ClientServerGlobalKnowledgeAnalyzer extends Analyzer {

	// Marker
	

}
