package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.ResponseObject;

public class CellularResponseMessage extends AbstractOverlayMessage implements CraterCellMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8222776635246390072L;

	private List<ResponseObject> responses;

	public CellularResponseMessage(OverlayContact sender, OverlayContact receiver, List<ResponseObject> responses) {
		super(sender, receiver);
		this.responses = responses;
	}

	public List<ResponseObject> getResponses() {
		return responses;
	}

	@Override
	public Message getPayload() {
		return this;
	}

	@Override
	public long getSize() {
		long size = super.getSize();

		for (ResponseObject response : responses) {
			size += response.getTransmissionSize();
		}
		return size;
	}

}
