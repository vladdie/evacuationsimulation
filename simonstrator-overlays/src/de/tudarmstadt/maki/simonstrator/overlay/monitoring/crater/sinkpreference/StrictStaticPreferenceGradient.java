package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * Implementation of {@link AbstractSinkPreference} which gets a static sink if possible, with the highest possible
 * gradient and a decent amount of updates
 * 
 * @author Jonas Huelsmann
 *
 */
public class StrictStaticPreferenceGradient extends AbstractSinkPreference {

	public StrictStaticPreferenceGradient(int MIN_NUMBER_OF_UPDATES, Map<INodeID, SinkTableEntry> unmodifiableSinkTable) {
		super(MIN_NUMBER_OF_UPDATES, unmodifiableSinkTable);
	}

	@Override
	public SimpleEntry<INodeID, Double> getBestSink() {

		SinkTableEntry bestEntry;

		if (sinkTable.size() > 0) {
			bestEntry = sinkTable.entrySet().iterator().next().getValue();
		} else {
			return null;
		}

		/*
		 * Start with max number of successive and take less if none found.
		 */
		for (int curNumberOfUpdates = BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES; curNumberOfUpdates > 0; curNumberOfUpdates--) {
			if (sinkTable.size() >= 1) {
				// for sinkPreferenceActive

				for (SinkTableEntry actEntry : sinkTable.values()) {
					// best sink is not yet static - new checked entry is
					if (!actEntry.getSinkMobility() && bestEntry.getSinkMobility()
							&& actEntry.getNumberOfUpdates() >= curNumberOfUpdates) {
						bestEntry = actEntry;
					}
					// if both are static get the better one
					if (!actEntry.getSinkMobility() && !bestEntry.getSinkMobility()) {
						if (actEntry.getCurrentGradient() > bestEntry.getCurrentGradient()
								&& actEntry.getNumberOfUpdates() >= curNumberOfUpdates) {
							bestEntry = actEntry;
						}
					}
					// is sink is not static yet - new entry is not either
					if (actEntry.getSinkMobility() && bestEntry.getSinkMobility()) {
						if (actEntry.getCurrentGradient() > bestEntry.getCurrentGradient()
								&& actEntry.getNumberOfUpdates() >= curNumberOfUpdates) {
							bestEntry = actEntry;
						}
					}
				}
				if (bestEntry.getNumberOfUpdates() >= curNumberOfUpdates)
					return new SimpleEntry<INodeID, Double>(bestEntry.getSinkId(), bestEntry.getCurrentGradient());
			}
		}
		return null;
	}
}
