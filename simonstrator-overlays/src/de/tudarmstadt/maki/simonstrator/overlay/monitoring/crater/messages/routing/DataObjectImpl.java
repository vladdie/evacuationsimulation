package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterFactory;

/**
 * A normal data object for uncompressed data sending.
 * @author Nils Richerzhagen
 * @version 1.0, 30.06.2014
 *
 * @param <T>
 */
public class DataObjectImpl <T> implements DataObject<T> {

	private UniqueID uniqueObjectID;
	private int attributeId;
	private T value;
	private INodeID nodeId;
	
	/**
	 * Default constructor for initialization of a DataObject.
	 * @param attributeId
	 * @param value
	 */
	public DataObjectImpl(int attributeId, T value, INodeID nodeId) {
		this.attributeId = attributeId;
		this.value = value;
		this.nodeId = nodeId;
		this.uniqueObjectID = CraterFactory.getRandomDataObjectID();
	}
	
	@Override
	public int getAttributeId() {
		return attributeId;
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public INodeID getNodeId() {
		return nodeId;
	}

	@Override
	public UniqueID getUniqueDataObjectID() {
		return uniqueObjectID;
	}
}
