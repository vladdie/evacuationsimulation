package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;

/**
 * Implementation of {@link SendHistory}, which will not detect any
 * duplicates
 * 
 * @author Jonas Huelsmann
 *
 */
public class NoCraterSendHistory extends SendHistory {

	public NoCraterSendHistory(int size) {
		super(size);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean sendable(UniqueID msgID) {
		return true;
	}

	@Override
	public Set<UniqueID> sendable(Collection<UniqueID> msgIDs) {
		Set<UniqueID> temp = new TreeSet<UniqueID>();
		temp.addAll(msgIDs);
		return temp;
	}

	@Override
	public boolean isInHistory(UniqueID msgID) {
		return false;
	}

	@Override
	public void resetHistory() throws Exception {
		throw new Exception(
				"resetHistory is not ment to be called at a NoCraterSendHistory");

	}

}
