package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainer.MergePoint;

/**
 * Interface for duplication sensitive aggregates. For that reason the nodeId is
 * stored, meaning every node is only allowed to put its value in once.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 01.07.2014
 * 
 * @param <T>
 */
public interface DuplicateSensitiveAggregationFunction<T> {

	public enum DuplicateSensitiveAggregationFunctionsDescriptions {
		MEAN("MEAN", "Mean value of all inputs."), NODECOUNT("NODECOUNT", "Number of nodes participating in Aggregate."), NODECOUNT_NOREF(
				"NODECOUNT_NOREF",
				"Number of nodes participating in Aggregate without exchange of reference when one node added its attribute more often. Thus nodes may be counted multiple times.");

		private final String representation;
		private final String id;

		private DuplicateSensitiveAggregationFunctionsDescriptions(String id, String represenation) {
			this.representation = represenation;
			this.id = id;
		}

		@Override
		public String toString() {
			return representation;
		}

		public String getId() {
			return id;
		}
	}

	/**
	 * Add a value to the aggregate. Duplicate entries are detected via the
	 * nodeId, as every node is only allowed to put in its value once.
	 * 
	 * @param toAddEntry
	 *            - <key> craterNodeId of the node that put the value in,
	 *            <value> the value of the attribute
	 */
	public void putValue(AggregationFunctionEntry toAddEntry, MergePoint mergePoint);

	/**
	 * Returning the aggregate value.
	 * 
	 * @return
	 */
	public T getValue();

	/**
	 * Returning the description of the aggregation function.
	 * 
	 * @return
	 */
	public String getDescription();

	/**
	 * Returning the ID of the aggregation function.
	 * 
	 * @return
	 */
	public String getId();

	/**
	 * Returning the currently used entries in this
	 * {@link DuplicateSensitiveAggregationFunction}.
	 * 
	 * @return
	 */
	public LinkedList<AggregationFunctionEntry> getUsedEntries();

	/**
	 * Used solely for merging.
	 * 
	 * @return
	 */
	public LinkedList<AggregationFunctionEntry> getAllIncludedEntriesForMerge();

	/**
	 * For statistics!
	 * 
	 * @return
	 */
	public LinkedList<UniqueID> getIncludedEntriesIDs();

	/**
	 * Cloning of the {@link DuplicateSensitiveAggregationFunction}.
	 * 
	 * @return
	 */
	public DuplicateSensitiveAggregationFunction<T> clone();
}
