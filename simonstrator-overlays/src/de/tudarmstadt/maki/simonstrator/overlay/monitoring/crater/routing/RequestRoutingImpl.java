package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing;

import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent.CraterNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.Request;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestInquiryMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestMessageCellular;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestMessageLocal;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.RequestRoutingDissemination.DisseminationType;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

public class RequestRoutingImpl implements RequestRouting {

	// FIXME adjust as fitting. This controls how long request IDs are stored.
	private static long hystoryTimeOut = 20 * Time.MINUTE;

	private static DisseminationType disseminationType = DisseminationType.BROADCAST;
	private static double disseminationValue = 0.75d;

	private CraterNodeComponent parent;

	private TimeoutSet<UniqueID> history;
	private RequestRoutingDissemination dissemination;

	private int ttl;

	boolean canSend = false;

	public RequestRoutingImpl(CraterNodeComponent parent, int ttl) {
		this.parent = parent;
		this.ttl = ttl;

		history = new TimeoutSet<UniqueID>(hystoryTimeOut);
		dissemination = new RequestRoutingDissemination(parent, disseminationType, disseminationValue);
	}

	@Override
	public void init() {
		// nothing to do...
	}

	@Override
	public void handleIncommingRequestMessage(Message receivedMsg) {
		
		RequestMessageLocal rqMsgLocal;
		boolean receivedViaCell = false;
		if(receivedMsg instanceof RequestMessageCellular){
			rqMsgLocal = new RequestMessageLocal(parent.getLocalOverlayContact(), null,
					(RequestMessageCellular) receivedMsg, ttl);
			receivedViaCell = true;
		}
		else if (receivedMsg instanceof RequestMessageLocal) {
			rqMsgLocal = new RequestMessageLocal(parent.getLocalOverlayContact(), (RequestMessageLocal) receivedMsg);
		}
		else{
			return;
		}
		
		if (canSend && !history.contains(rqMsgLocal.getUniqueMsgID())) {
			history.addNow(rqMsgLocal.getUniqueMsgID());

			// System.out.println("Node " + parent.getHost().getHostId() + "
			// forwards a RequestMsg!");

			// update RequestResolver with the request
			parent.getRequestResolver().addRequest(rqMsgLocal.getRequest());

		/*
		 * #1 Local message ->Broadcast #1A isSink -> also upload to cellTower
		 * 
		 * #2 Cell message -> Broadcast as local
		 * 
		 * 
		 * => So Broadcast anyway
		 */

			if (rqMsgLocal.getTTL() > 0) {
				dissemination.disseminate(rqMsgLocal);
			}

			if (parent.isSink() && !receivedViaCell) {

				RequestMessageCellular rqMsgCell = new RequestMessageCellular(parent.getLocalOverlayContact(),
						parent.getCloudContact(), rqMsgLocal);

				parent.sendViaCellular(rqMsgCell);
			}

			/////////////////////////////
			// Analyzer stuff
			toAnalyzerRequestReceived(rqMsgLocal.getRequest(), parent.getHost().getId());
		}
	}

	@Override
	public void sendRequest(Request request) {
		// generate local message and call handleIncommingMessage
		// System.out.println("Node " + parent.getHost().getId() + " received an
		// Request!");

		RequestMessageLocal rmsg = new RequestMessageLocal(parent.getLocalOverlayContact(), null, request, ttl);

		parent.sendLocalBroadcast(rmsg);

		if (parent.isSink() && !history.contains(rmsg.getUniqueMsgID())) {
			RequestMessageCellular rqMsgCell = new RequestMessageCellular(parent.getLocalOverlayContact(),
					parent.getCloudContact(), rmsg);
			parent.sendViaCellular(rqMsgCell);

			// add the msg to the history
			history.addNow(rmsg.getUniqueMsgID());
		}

		// Also update RequestResolver with the request?? NO
	}

	@Override
	public void craterNodeRoleChanged(CraterNodeRole craterNodeRole) {
		if (craterNodeRole.equals(CraterNodeRole.OFFLINE)) {
			canSend = false;

			// 'clear' history to mimic new Node behaviour
			history = new TimeoutSet<UniqueID>(hystoryTimeOut);

		} else {
			canSend = true;
			// anything else?
		}
	}

	@Override
	public void handleIncommingRequestInquiry(RequestInquiryMessage inquiryMsg) {
		Set<Integer> knownRequests = parent.getRequestResolver().getAllVallidRequestHashes();
		for (int hash : inquiryMsg.getRequestInquiries()) {
			if (knownRequests.contains(hash)) {
				Request request = parent.getRequestResolver().getRequestForHash(hash);
				sendRequest(request);
			}
		}

	}

	/////////////////////////////////
	// For Configurator

	/**
	 * Sets the RequestRouting-Dissemination Type to use.
	 * 
	 * @param type
	 *            One of <i>BROADCAST, PROBABILISTIC, CONTENTION</i>
	 */
	public static void setRequestDisseminationType(String type) {
		disseminationType = DisseminationType.valueOf(type.toUpperCase());
	}

	/**
	 * Value that controls the behaviour of the RequestRouting-Dissemination.
	 * <br>
	 * Unused for BROADCAST
	 * 
	 * @param value
	 *            -PROBABILISTIC uses this as sending probability <br>
	 *            -CONTENTION interprets it as maximum waiting time, to see if
	 *            some other node sent a message
	 */
	public static void setRequestDisseminationValue(double value) {
		disseminationValue = value;
	}

	//////////////////////////////////////////////////////
	// Analyzing stuff
	//////////////////////////////////////////////////////
	private void toAnalyzerRequestReceived(Request request, INodeID receiver) {
		if (parent.getRequestResolver().hasRequestResponseAnalyzer()) {
			parent.getRequestResolver().getRequestResponseAnalyzer().onRequestMessageReceived(request, receiver);
		}
	}
}
