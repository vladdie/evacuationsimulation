package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference;

import java.util.AbstractMap.SimpleEntry;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * Implementation of {@link AbstractSinkPreference} which gets the sink which has the highest gradient and if possible
 * was heard at least BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES amount of times
 * 
 * @author Jonas Huelsmann
 *
 */
public class DefaultPreferenceGradient extends AbstractSinkPreference {

	public DefaultPreferenceGradient(int MIN_NUMBER_OF_UPDATES, Map<INodeID, SinkTableEntry> unmodifiableSinkTable) {
		super(MIN_NUMBER_OF_UPDATES, unmodifiableSinkTable);
	}

	@Override
	public SimpleEntry<INodeID, Double> getBestSink() {
		SinkTableEntry bestEntry;
		if (sinkTable.size() > 0) {
			bestEntry = sinkTable.entrySet().iterator().next().getValue();
		} else {
			return null;
		}

		for (int curNumberOfUpdates = BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES; curNumberOfUpdates > 0; curNumberOfUpdates--) {
			if (sinkTable.size() >= 1) {

				for (SinkTableEntry actEntry : sinkTable.values()) {
					if (actEntry.getCurrentGradient() > bestEntry.getCurrentGradient()
							&& actEntry.getNumberOfUpdates() >= curNumberOfUpdates) {
						bestEntry = actEntry;
					}
				}

			}
			if (bestEntry.getNumberOfUpdates() >= curNumberOfUpdates)
				return new SimpleEntry<INodeID, Double>(bestEntry.getSinkId(), bestEntry.getCurrentGradient());
		}
		return null;
	}

}
