package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing;

import de.tudarmstadt.maki.simonstrator.api.Message;

/**
 * Marker interface that it is clear that a message is belonging to the routing
 * effort.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 19.06.2014
 * 
 */
public interface DataMessage extends Message {
	/*
	 * Marker interface for packet classifier.
	 */
}
