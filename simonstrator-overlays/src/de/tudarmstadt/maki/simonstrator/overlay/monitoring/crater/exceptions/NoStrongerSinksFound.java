package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions;

/**
 * Exception indicating that there is no known 'stronger' sink currently.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 15.06.2014
 *
 */
public class NoStrongerSinksFound extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoStrongerSinksFound() {
		super("There is no known sink that is stronger. Catch this error!");
	}
}
