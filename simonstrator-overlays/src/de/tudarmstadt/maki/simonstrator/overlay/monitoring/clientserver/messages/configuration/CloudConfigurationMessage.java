package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.configuration;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent.ClientServerNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.ClientServerCellMessage;

/**
 * Cloud configuring the {@link ClientServerNodeComponent}s in the network of a role
 * change from Sink to Leaf.
 * 
 * FIXME Maybe also include additional Ctrl-Information that influences lokal
 * behavior.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CloudConfigurationMessage extends AbstractOverlayMessage implements
		ClientServerCellMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5551715404703916221L;

	private ClientServerNodeRole nodeRole;
	
	public CloudConfigurationMessage(OverlayContact sender, OverlayContact receiver, ClientServerNodeRole nodeRole) {
		super(sender, receiver);
		this.nodeRole = nodeRole;

	}

	@Override
	public Message getPayload() {
		return null;
	}

	public ClientServerNodeRole getNodeRole() {
		return nodeRole;
	}

	@Override
	public long getSize() {
		// FIXME Size
		return super.getSize();
	}
}
