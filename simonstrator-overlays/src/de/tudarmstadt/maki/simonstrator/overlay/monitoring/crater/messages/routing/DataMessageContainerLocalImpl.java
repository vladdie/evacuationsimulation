package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterFactory;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.DuplicateInsensitiveAggregationFunction.DuplicateInsensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.DuplicateSensitiveAggregationFunction.DuplicateSensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.exceptions.AggregationFunctionNotKnownException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithSinkId;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.MessageWithUniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataRoutingImpl;

/**
 * {@link DataMessageContainer} implementation for local (ad-hoc) communication
 * in the network between nodes. It includes the normal data in
 * {@link DataObject}, duplicate insensitive aggregates in
 * {@link DataObjectAggregationDupInsensitiveImpl}, and duplice sensitve
 * aggregates in {@link DataObjectAggregationDupSensitiveImpl}.
 * 
 * Furthermore the basic fields: {@link UniqueID}, sinkId (long), and the
 * gradient (double).
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 * 
 */
// FIXME chs: Support for unicast Addresses!
public class DataMessageContainerLocalImpl extends AbstractOverlayMessage implements CraterLocalMessage, MessageWithSinkId,
		MessageWithUniqueID, DataMessageContainer {

	private static final long serialVersionUID = 1L;

	/**
	 * In byte!
	 */
	private static final int MAX_MESSAGE_PAYLOAD = 1800;

	private UniqueID uniqueMsgId;

	private INodeID craterSinkId;

	private double currentGradient;

	private LinkedList<DataObjectImpl<Double>> normalData = new LinkedList<DataObjectImpl<Double>>();
	private LinkedList<DataObjectAggregationDupInsensitiveImpl> aggregationDupInsensitiveData = new LinkedList<DataObjectAggregationDupInsensitiveImpl>();
	private LinkedList<DataObjectAggregationDupSensitiveImpl> aggregationDupSensitiveData = new LinkedList<DataObjectAggregationDupSensitiveImpl>();

	// New 2016.07.19
	// Container now include specific responses to Data requests
	// Map is just used for convenience reasons
	private HashMap<Integer, ResponseObject> responses = new HashMap<Integer, ResponseObject>();

	/**
	 * Copy-constructor for messageArrived at {@link CraterNodeComponent}.
	 * 
	 * @param msg
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg) {
		super(msg.getSender(), msg.getReceiver(), msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.craterSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();
		this.responses = msg.getResponseMap();
	}

	/**
	 * Initial constructor for {@link DataMessageContainer} in sending out in
	 * {@link DataRoutingImpl}.
	 * 
	 * CAUTION! Only use if afterwards content is added, otherwise an empty msg
	 * is send out!
	 * 
	 * @param sender
	 * @param craterSinkId
	 * @param currentGradient
	 */
	public DataMessageContainerLocalImpl(OverlayContact sender, OverlayContact receiver, INodeID craterSinkId,
			double currentGradient) {
		super(sender, receiver);
		this.uniqueMsgId = CraterFactory.getRandomMessageId();
		this.craterSinkId = craterSinkId;
		this.currentGradient = currentGradient;
	}

	/**
	 * Copy-constructor + additional {@link DataObject} for data sending in
	 * {@link DataRoutingImpl}.
	 * 
	 * @param msg
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg, INodeID actNodeId, int attributeId, double value) {
		super(msg.getSender(), msg.getReceiver(), msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.craterSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();
		this.responses = msg.getResponseMap();

		DataObjectImpl<Double> normalDataObject = new DataObjectImpl<Double>(attributeId, value, actNodeId);
		normalData.add(normalDataObject);

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onNormalDataObjectAddedInNetwork(normalDataObject.getUniqueDataObjectID());
		}
	}

	/**
	 * For first send analyzer purposes.
	 * 
	 * @param msg
	 * @param actNodeId
	 * @param attributeId
	 * @param value
	 * @param firstSend
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg, INodeID actNodeId, int attributeId, double value,
			boolean firstSend) {
		super(msg.getSender(), msg.getReceiver(), msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.craterSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();
		this.responses = msg.getResponseMap();

		DataObjectImpl<Double> normalDataObject = new DataObjectImpl<Double>(attributeId, value, actNodeId);
		normalData.add(normalDataObject);

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onNormalDataObjectAddedInNetwork(normalDataObject.getUniqueDataObjectID());
			getDataRoutingAnalyzer().onNormalDataObjectAddedFirstTime(normalDataObject.getUniqueDataObjectID());
		}
	}

	/**
	 * Copy-constructor + additional
	 * {@link DataObjectAggregationDupInsensitiveImpl} or
	 * {@link DataObjectAggregationDupSensitiveImpl} according to the given
	 * aggregationFunctionId for data sending in {@link DataRoutingImpl}.
	 * 
	 * @param msg
	 *            - the original message.
	 * @param aggregationFunctionId
	 *            - the identifier of the aggregation function.
	 * @param attributeId
	 * @param toAddEntry
	 *            - in case of {@link DataObjectAggregationDupInsensitiveImpl}
	 *            adding full <key> craterNodeId, <value> value entry. In case
	 *            of {@link DataObjectAggregationDupSensitiveImpl} adding only
	 *            the <value> of the entry as key is irrelevant.
	 * @throws AggregationFunctionNotKnownException
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg, String aggregationFunctionId, int attributeId,
			AggregationFunctionEntry toAddEntry, boolean firstTime) throws AggregationFunctionNotKnownException {
		super(msg.getSender(), msg.getReceiver(), msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.craterSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();
		this.responses = msg.getResponseMap();

		if (DuplicateSensitiveAggregationFunctionsDescriptions.MEAN.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT_NOREF.getId().equals(aggregationFunctionId)) {

			putDupSensitiveData(aggregationFunctionId, attributeId, toAddEntry);
			// Analyzing method.
			if (hasDataRoutingAnalyzer()) {
				getDataRoutingAnalyzer().onAggregationDupSensitiveEntryAddedFirstTime(toAddEntry);
			}
		} else if (DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.getId().equals(aggregationFunctionId)
				|| DuplicateInsensitiveAggregationFunctionsDescriptions.MIN.getId().equals(aggregationFunctionId)) {

			putDupInsensitiveData(aggregationFunctionId, attributeId, toAddEntry.getValue());
		} else {
			throw new AggregationFunctionNotKnownException(aggregationFunctionId);
		}
	}

	/**
	 * Copy-constructor + additional
	 * {@link DataObjectAggregationDupInsensitiveImpl} or
	 * {@link DataObjectAggregationDupSensitiveImpl} according to the given
	 * aggregationFunctionId for data sending in {@link DataRoutingImpl}.
	 * 
	 * @param msg
	 *            - the original message.
	 * @param aggregationFunctionId
	 *            - the identifier of the aggregation function.
	 * @param attributeId
	 * @param toAddEntry
	 *            - in case of {@link DataObjectAggregationDupInsensitiveImpl}
	 *            adding full <key> craterNodeId, <value> value entry. In case
	 *            of {@link DataObjectAggregationDupSensitiveImpl} adding only
	 *            the <value> of the entry as key is irrelevant.
	 * @throws AggregationFunctionNotKnownException
	 */
	public DataMessageContainerLocalImpl(DataMessageContainerLocalImpl msg, String aggregationFunctionId, int attributeId,
			AggregationFunctionEntry toAddEntry) throws AggregationFunctionNotKnownException {
		super(msg.getSender(), msg.getReceiver(), msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.craterSinkId = msg.getSinkId();
		this.currentGradient = msg.getGradient();
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();
		this.responses = msg.getResponseMap();

		if (DuplicateSensitiveAggregationFunctionsDescriptions.MEAN.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId().equals(aggregationFunctionId)
				|| DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT_NOREF.getId().equals(aggregationFunctionId)) {

			putDupSensitiveData(aggregationFunctionId, attributeId, toAddEntry);
		} else if (DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.getId().equals(aggregationFunctionId)
				|| DuplicateInsensitiveAggregationFunctionsDescriptions.MIN.getId().equals(aggregationFunctionId)) {

			putDupInsensitiveData(aggregationFunctionId, attributeId, toAddEntry.getValue());
		} else {
			throw new AggregationFunctionNotKnownException(aggregationFunctionId);
		}
	}

	/**
	 * Constructor for forwarding of a received
	 * {@link DataMessageContainerLocalImpl} with no additional data but updated
	 * gradient, sender, and receiver.
	 * 
	 * @param msg
	 * @param currentGradient
	 */
	public DataMessageContainerLocalImpl(OverlayContact sender, OverlayContact receiver,
			DataMessageContainerLocalImpl msg, double currentGradient) {
		super(sender, receiver, msg);
		this.uniqueMsgId = msg.getUniqueMsgID();
		this.craterSinkId = msg.getSinkId();
		this.currentGradient = currentGradient;
		this.normalData = msg.getNormalDataList();
		this.aggregationDupInsensitiveData = msg.getAggregationDupInsensitiveDataList();
		this.aggregationDupSensitiveData = msg.getAggregationDupSensitiveDataList();
		this.responses = msg.getResponseMap();
	}

	/**
	 * Add value either to existing aggregate or add new
	 * {@link DataObjectAggregationDupInsensitiveImpl} with the new aggregate
	 * for the specific attributeId.
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param value
	 * @throws AggregationFunctionNotKnownException
	 */
	private void putDupInsensitiveData(String aggregationFunctionId, int attributeId, double value)
			throws AggregationFunctionNotKnownException {
		boolean entryExisted = false;
		for (DataObjectAggregationDupInsensitive<Double> actDataObject : aggregationDupInsensitiveData) {
			if (actDataObject.getAggregationFunctionId().equals(aggregationFunctionId) && actDataObject.getAttributeId() == attributeId) {
				actDataObject.putValue(value);
				entryExisted = true;
			}
		}

		if (!entryExisted) {
			DataObjectAggregationDupInsensitiveImpl aggregationDupInsensitiveDataObject = new DataObjectAggregationDupInsensitiveImpl(
					aggregationFunctionId, attributeId, value);
			this.aggregationDupInsensitiveData.add(aggregationDupInsensitiveDataObject);
		}

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onDuplicateInsensitiveAggregationDataAddedInNetwork(aggregationFunctionId, attributeId, value);
		}
	}

	/**
	 * Add value either to existing aggregate or add new
	 * {@link DataObjectAggregationDupSensitiveImpl} with the new aggregate for
	 * the specific attributeId.
	 * 
	 * 
	 * @param aggregationFunctionId
	 * @param attributeId
	 * @param toAddEntry
	 *            - <key> craterNodeId, <value> value
	 * @throws AggregationFunctionNotKnownException
	 */
	private void putDupSensitiveData(String aggregationFunctionId, int attributeId, AggregationFunctionEntry toAddEntry)
			throws AggregationFunctionNotKnownException {
		boolean entryExisted = false;
		for (DataObjectAggregationDupSensitiveImpl actDataObject : aggregationDupSensitiveData) {
			if (actDataObject.getAggregationFunctionId().equals(aggregationFunctionId) && actDataObject.getAttributeId() == attributeId) {
				actDataObject.putValue(toAddEntry, MergePoint.IN_NETWORK);
				entryExisted = true;
			}
		}

		if (!entryExisted) {
			DataObjectAggregationDupSensitiveImpl aggregationDupSensitiveDataObject = new DataObjectAggregationDupSensitiveImpl(
					aggregationFunctionId, attributeId, toAddEntry);
			this.aggregationDupSensitiveData.add(aggregationDupSensitiveDataObject);
		}

		// Analyzing method.
		if (hasDataRoutingAnalyzer()) {
			getDataRoutingAnalyzer().onDuplicateSensitiveAggregationDataAddedInNetwork(toAddEntry.getUniqueEntryID());
		}

	}

	/**
	 * Adds a single ResponseObject to this message, if it was not already
	 * present
	 * 
	 * @param response
	 */
	public void putResponse(ResponseObject response) {
		if (responses.containsKey(response.getRequestHash())) {
			ResponseObject data = responses.get(response.getRequestHash());
			data.addResponse(response);
		} else {
			responses.put(response.getRequestHash(), response);
		}
	}

	/**
	 * Unified method to remove outdated responses from this message before
	 * forwarding
	 */
	public void removeOutdatedResponses() {
		Iterator<ResponseObject> iter = responses.values().iterator();
		while (iter.hasNext()) {
			ResponseObject response = iter.next();
			if (response.getValidity() < Time.getCurrentTime()) {
				iter.remove();
			}
		}
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public UniqueID getUniqueMsgID() {
		return uniqueMsgId;
	}

	@Override
	public INodeID getSinkId() {
		return craterSinkId;
	}

	public double getGradient() {
		return currentGradient;
	}

	@Override
	public double getFilledPercentage() {
		return (double) getSize() / (double) MAX_MESSAGE_PAYLOAD;
	}

	@Override
	public long getSize() {
		/*
		 * Normal data: attributeId +2, nodeId +2, value +8
		 * 
		 * Dup Insens: short attributeId +2, byte aggregationFunction, double
		 * currentValue +8
		 * 
		 * Dup Sens: short attributeId +2, byte aggregationFunction, map<double
		 * values, short nodeId> +10
		 */
		long bytesNormalData = normalData.size() * 14;
		long bytesAggDupInsens = aggregationDupInsensitiveData.size() * 10;
		long bytesAggDupSens = 0;
		for (DataObjectAggregationDupSensitiveImpl actDupSensitiveObject : aggregationDupSensitiveData) {
			bytesAggDupSens += actDupSensitiveObject.getMessageSize();
//			bytesAggDupSens += 2; // attributeID
//			bytesAggDupSens += actDupSensitiveObject.getIncludedEntriesIDs().size() * 10; // +10
//																							// for
//																							// each
//																							// entry
//																							// in
//																							// aggFunc
		}
		return super.getSize() + bytesNormalData + bytesAggDupInsens + bytesAggDupSens;
	}

	@Override
	public LinkedList<DataObjectImpl<Double>> getNormalDataList() {
		return normalData;
	}

	@Override
	public LinkedList<DataObjectAggregationDupInsensitiveImpl> getAggregationDupInsensitiveDataList() {
		return aggregationDupInsensitiveData;
	}

	@Override
	public LinkedList<DataObjectAggregationDupSensitiveImpl> getAggregationDupSensitiveDataList() {
		return aggregationDupSensitiveData;
	}

	/*
	 * Analyzing convenience methods.
	 */
	private boolean checkedForDataRoutingAnalyzer = false;

	private DataRoutingAnalyzer dataRoutingAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	private boolean hasDataRoutingAnalyzer() {
		if (dataRoutingAnalyzer == null && !checkedForDataRoutingAnalyzer)
		{
			getDataRoutingAnalyzer();
		}
		return dataRoutingAnalyzer != null;
	}

	private DataRoutingAnalyzer getDataRoutingAnalyzer() {
		if (!checkedForDataRoutingAnalyzer) {
			try {
				dataRoutingAnalyzer = Monitor.get(DataRoutingAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				dataRoutingAnalyzer = null;
			}
			checkedForDataRoutingAnalyzer = true;
		}
		return dataRoutingAnalyzer;
	}

	@Override
	public LinkedList<ResponseObject> getQueryResponseList() {
		return new LinkedList<ResponseObject>(responses.values());
	}

	protected HashMap<Integer, ResponseObject> getResponseMap() {
		return responses;
	}
}
