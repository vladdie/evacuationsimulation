package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.sinksendinginterval;

import de.tudarmstadt.maki.simonstrator.api.Time;

/**
 * Simple linearly decreasing the interval from maxInterval at a speed of 0 m/s
 * to minInterval at maxSpeed. Values higher than maxSpeed result in minInterval
 * 
 * @author Christoph Storm
 *
 */
public class LinearDecreaseInterval implements SinkAdvIntervalModifier {

	private long minInterval = 5 * Time.SECOND;
	private long maxInterval = 90 * Time.SECOND;
	private double maxSpeed = 8; // m/s

	@Override
	public long getNextInterval(double speedOfNodes) {

		if (speedOfNodes >= maxSpeed) {
			return minInterval;
		} else {
			double slope = (minInterval - maxInterval) / maxSpeed;
			return (long) (speedOfNodes * slope) + maxInterval;
		}
	}

	public void setMinInterval(long minInterval) {
		this.minInterval = minInterval;
	}

	public void setMaxInterval(long maxInterval) {
		this.minInterval = maxInterval;
	}

	public void setMaxSpeed(double maxSpeed) {
		this.maxSpeed = maxSpeed;
	}

	@Override
	public long getMaxInterval() {
		return maxInterval;
	}

}
