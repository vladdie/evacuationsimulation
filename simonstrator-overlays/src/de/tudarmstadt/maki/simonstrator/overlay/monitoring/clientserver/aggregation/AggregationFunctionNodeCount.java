package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation;

/**
 * Duplicate Sensitive Nodecount!!
 * 
 * Thus, each host is only allowed to add its entry once.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 26.08.2014
 *
 */
public class AggregationFunctionNodeCount extends AbstractDuplicateSensitiveAggregationFunction{

	public AggregationFunctionNodeCount(AggregationFunctionEntry toAddEntry) {
		super(toAddEntry);
	}
	
	public AggregationFunctionNodeCount(AbstractDuplicateSensitiveAggregationFunction toCloneAggregationFunction) {
		super(toCloneAggregationFunction);
	}

	@Override
	public Double getValue() {
		return (double) this.getUsedEntries().size();
	}

	@Override
	public String getDescription() {
		return DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.toString();
	}

	@Override
	public String getId() {
		return DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId();
	}

	@Override
	public AbstractDuplicateSensitiveAggregationFunction clone() {
		return new AggregationFunctionNodeCount(this);
	}

}
