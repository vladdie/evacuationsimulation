package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerFactory;

public abstract class AdvertisingMessage extends AbstractOverlayMessage
		implements ClientServerLocalMessage, MessageWithGradient, MessageWithUniqueID, MessageWithTTL {

	private static final long serialVersionUID = -6002672191961903733L;

	private UniqueID uniqueMsgId;

	private int gradient;

	private INodeID clientServerNodeId;

	private int sinkQuality;

	public AdvertisingMessage(OverlayContact sender, INodeID clientServerNodeId, int gradient, int sinkQuality) {
		this.uniqueMsgId = ClientServerFactory.getRandomMessageId();
		this.clientServerNodeId = clientServerNodeId;
		this.gradient = gradient;
		this.sinkQuality = sinkQuality;
	}

	public AdvertisingMessage(AdvertisingMessage receivedMsg) {
		this.uniqueMsgId = receivedMsg.uniqueMsgId;
		this.gradient = receivedMsg.gradient - 1;
		this.clientServerNodeId = receivedMsg.getSinkId();
		this.sinkQuality = receivedMsg.getSinkQuality();
		assert gradient >= 0;
	}

	@Override
	public long getSize() {
		// In Protocol: 4 bit gradient 0-10; 4 bit TTL 0-10, short
		// clientServerNodeId
		// (65536), min 7 bit sinkQuality 0-100
		return super.getSize() + 1 + 2 + 1;
	}

	/**
	 * Returning the sinkId = the node id in this case.
	 * 
	 * @return
	 */
	public INodeID getSinkId() {
		return clientServerNodeId;
	}

	/**
	 * Returning the sinkQuality. Value between [0 and 100] indicating the
	 * 'strongness' of the sink.
	 * 
	 * @return
	 */
	public int getSinkQuality() {
		return this.sinkQuality;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public int getGradient() {
		return this.gradient;
	}

	@Override
	public boolean deadMessage() {
		if (gradient <= 0)
			return true;
		else
			return false;
	}

	@Override
	public UniqueID getUniqueMsgID() {
		return uniqueMsgId;
	}
}
