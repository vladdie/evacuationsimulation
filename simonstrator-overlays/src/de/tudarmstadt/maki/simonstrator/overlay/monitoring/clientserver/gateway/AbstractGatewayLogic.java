package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.gateway;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent.ClientServerNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.configuration.CloudConfigurationMessage;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Base class for {@link GatewayLogic}s to handle the change of gateways. Update
 * only the nodes in ClientServer that are changed (Sink to Leaf and vice
 * versa).
 * 
 * @author Nils Richerzhagen
 *
 */
public abstract class AbstractGatewayLogic implements GatewayLogic {

	private OverlayContacts oldGateways;

	private ClientServerComponent comp;

	// AtomicBoolean set with initial false value
	private static final AtomicBoolean _firstMethodCall = new AtomicBoolean(true);

	public AbstractGatewayLogic(ClientServerComponent comp) {
		this.comp = comp;
	}

	@Override
	public ClientServerComponent getComponent() {
		return comp;
	}

	/**
	 * http://stackoverflow.com/questions/4217013/how-to-force-derived-class-to-
	 * call-super-method-like-android-does
	 */
	public final Collection<CloudConfigurationMessage> calculateAndInformGateways(OverlayContacts potentialNodes,
			int maxNumberOfGateways) {

		// Method call from child class!
		Map<INodeID, List<INodeID>> selectedGateways = pickGateways(potentialNodes, maxNumberOfGateways);

		Collection<OverlayContact> selectedGatewayContacts = new LinkedList<OverlayContact>();

		for (Map.Entry<INodeID, List<INodeID>> selectedGateway : selectedGateways.entrySet()) {
			OverlayContact gatewayContact = potentialNodes.get(selectedGateway.getKey());
			selectedGatewayContacts.add(gatewayContact);
		}

		/*
		 * Add logic for state change messages
		 */
		List<CloudConfigurationMessage> cloudConfigurationMessages = new LinkedList<>();
		cloudConfigurationMessages = packStateChangeMessages(new OverlayContacts(selectedGatewayContacts));
		return cloudConfigurationMessages;
	}

	/**
	 * Pack all {@link CloudConfigurationMessage}s for the nodes in the network
	 * where the state changes. Thus nodes are updated of whom the roles are
	 * changed (Sink to Leaf and vice versa).
	 * 
	 * @param newGateways
	 *            the nodes that are the new chosen gateways.
	 * @return
	 */
	private List<CloudConfigurationMessage> packStateChangeMessages(OverlayContacts selectedGateways) {

		List<CloudConfigurationMessage> messages = new LinkedList<>();
		if (_firstMethodCall.getAndSet(false)) {

			oldGateways = new OverlayContacts(selectedGateways);

			/*
			 * --------- Selected Gateways -----------
			 * 
			 * Message to all selected Gateways
			 */
			for (OverlayContact selectedGateway : selectedGateways) {
				messages.add(new CloudConfigurationMessage(getComponent().getLocalOverlayContact(), selectedGateway,
						ClientServerNodeRole.SINK));
			}
		} else {
			// assert oldGateways.isEmpty() == false : "OldGateways is empty."; Not needed, if e.g. the static gw
			// selection is not able to set a gw anymore! (Example DAMON!!)

			/*
			 * --------- Old Gateways --------------
			 * 
			 * Remove all gateways that are part of the new elected gateways.
			 * Purpose: send Configuration messages to the nodes that change
			 * role from sink back to leaf
			 */
			for (OverlayContact oldGateway : oldGateways) {
				if (selectedGateways.contains(oldGateway)) {
					continue;
				}
				CloudConfigurationMessage msg = new CloudConfigurationMessage(comp.getLocalOverlayContact(), oldGateway,
						ClientServerNodeRole.LEAF);
				messages.add(msg);
			}

			/*
			 * --------- Selected Gateways -----------
			 * 
			 * Message only to selected gateways that have not been gateway in
			 * step before.
			 */
			for (OverlayContact selectedGateway : selectedGateways) {
				if (oldGateways.contains(selectedGateway)) {
					continue;
				}
				CloudConfigurationMessage msg = new CloudConfigurationMessage(comp.getLocalOverlayContact(), selectedGateway,
						ClientServerNodeRole.SINK);
				messages.add(msg);
			}

			oldGateways = new OverlayContacts(selectedGateways);
		}
		return messages;
	}
}
