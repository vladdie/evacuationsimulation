package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.NoSinkMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.CraterNodeRoleListener;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.ITimeoutMapListener;

/**
 * Mechanism for node beaconing that it can not directly upload.
 * @author Nils Richerzhagen
 * @version 1.0, 04.06.2014
 *
 */
public interface NoSinkAdvertising extends ITimeoutMapListener, CraterNodeRoleListener {
	
	public void initialize();

	/**
	 * Handle an incoming node beacon.
	 * - start beaconing only if not sink and not already active
	 * - beacon heard flag = true
	 * @param msg
	 */
	public void handleIncomingNoSinkMessage(NoSinkMessage msg);
	
	/**
	 * Send node 'noSink' beacon only if
	 * - no Sink
	 * - no other beacon than own heard during interval X
	 * - if sink table is empty
	 * - state = idle
	 * 
	 * if so change state to ACTIVE and send out beacon
	 * - in the end switch node beacon heard back to false
	 */
	public void sendNoSinkBeacon();

	/**
	 * 
	 * @return
	 */
	public long getHesitationTime();
	
}
