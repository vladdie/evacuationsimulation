package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.attribute;

/**
 * Attributes in adaptive monitoring. (ClientServer)
 * @author Nils Richerzhagen
 * @version 1.0, 30.06.2014
 * 
 */
public enum Attributes{
	StartupDelay(1000, "StartupDelay", "Descripes the amount of time the video needs to start from the moment the user started the video.", AttributeUnit.TIME);
	
	
	private final int attributeId;
	private final String description;
	private final AttributeUnit attributeUnit;
	private final String name;
	
	/**
	 * 
	 * @param attributeId
	 * @param description
	 * @param attributeUnit
	 */
	private Attributes(int attributeId, String name, String description, AttributeUnit attributeUnit) {
		this.attributeId = attributeId;
		this.description = description;
		this.attributeUnit = attributeUnit;
		this.name = name;
	}
	
	/**
	 * Returns the attributeId.
	 * @return
	 */
	public int getAttributeId(){
		return attributeId;
	}

	/**
	 * Returns the descriptions of the attribute.
	 * @return
	 */
	public String getDescriptionForId(){
		return description;
	}

	/**
	 * Returns the unit of the attribute.
	 * @return
	 */
	public AttributeUnit getUnitForId(){
		return attributeUnit;
	}
	
	/**
	 * Returns the name of the Attribute.
	 * @return
	 */
	public String getNameForId(){
		return name;
	}
	
	/**
	 * Units for Attributes.
	 * 
	 * @author Nils Richerzhagen
	 * @version 1.0, 30.06.2014
	 * 
	 */
	public enum AttributeUnit {
		NONE(""), TIME("us"), DATA("byte"), DATA_PER_SEC("byte/s"), UNKNOWN("unknown");

		private final String representation;

		private AttributeUnit(String representation) {
			this.representation = representation;
		}

		@Override
		public String toString() {
			return representation;
		}
	}
}
