package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.common.IDSpace;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.overlay.DefaultIDSpace;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerComponent.ClientServerNodeType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.cloudlet.ClientServerCloudletComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.configuration.ClientServerConfiguration;

/**
 * One Factory is used to ease configuration and sanity checks for
 * {@link ClientServerComponent}s. The {@link ClientServerComponent}s are
 * configured via the {@link ClientServerConfiguration} instances, used to get
 * valid configurations. Refer to clientserver.configuration for a list of
 * configurations available in ClientServer.
 * 
 * @author Nils Richerzhagen
 *
 */
public class ClientServerFactory implements HostComponentFactory {

	private static final IDSpace messageIdSpace = new DefaultIDSpace(64);
	private static final IDSpace aggregationFunctionEntryIDSpace = new DefaultIDSpace(64);
	private static final IDSpace dataObjectIDSpace = new DefaultIDSpace(32);
	
	/**
	 * Node type to be created
	 */
	private ClientServerNodeType nodeType;

	/**
	 * configuration-class that configures the nodes
	 */
	private List<ClientServerConfiguration> configurations = new LinkedList<ClientServerConfiguration>();

	/**
	 * To ensure that there is only one cloud
	 */
	private boolean createdCloud = false;

	private boolean locked = false;

	public ClientServerFactory() {
		//
	}

	@Override
	public HostComponent createComponent(Host host) {
		if (nodeType == null) {
			throw new AssertionError("There must be a node type specified!");
		}
		if (configurations.isEmpty()) {
			throw new AssertionError("At least one configuration must be specified!");
		}

		if (!locked) {
			locked = true;
		}
		ClientServerComponent comp;

		switch (nodeType) {
		case MOBILE_NODE:
			comp = new ClientServerNodeComponent(host);
			break;
		case CLOUD:
			if (createdCloud) {
				throw new AssertionError("Only one cloud instance at a time is supported!");
			}
			comp = new ClientServerCentralComponent(host);
			createdCloud = true;
			break;
		case CLOUDLET:
			comp = new ClientServerCloudletComponent(host);
			break;
		default:
			throw new AssertionError("Unknown NodeType.");
		}

		/*
		 * Configures component by adding modules or setting parameters.
		 */
		for (ClientServerConfiguration configuration : configurations) {
			configuration.configure(comp, nodeType);
		}
		return comp;
	}

	/**
	 * For the XML-Configuration
	 * 
	 * @param nodeType
	 */
	public void setNodeType(String nodeType) {
		assert nodeType != null;
		this.setNodeTypeDirect(ClientServerNodeType.valueOf(nodeType.toUpperCase()));
	}

	/**
	 * For direct code interaction
	 * 
	 * @param nodeType
	 */
	public void setNodeTypeDirect(ClientServerNodeType nodeType) {
		assert nodeType != null;
		this.nodeType = nodeType;
	}
	
	/**
	 * Global configuration. Multiple configurations can be concatenated, they
	 * will be executed in order. Later configurations can overwrite previous
	 * ones.
	 * 
	 * Configurations will be applies to ALL nodes!
	 * 
	 * @param configuration
	 */
	public void setConfiguration(ClientServerConfiguration configuration) {
		if (locked) {
			throw new AssertionError("Began creating nodes. Adding configurations is no longer allowed.");
		}
		Monitor.log(ClientServerFactory.class, Level.INFO, "Adding a ClientServerConfiguration:\n%s", configuration);
		configurations.add(configuration);
	}

	/**
	 * Returns a {@link UniqueID} of the messageIdSpace.
	 *
	 * @return {@link UniqueID}
	 */
	// FIXME ID SPACE NEEDED?
	@Deprecated
	public static UniqueID getRandomMessageId() {
		return messageIdSpace.createRandomID();
	}

	/**
	 * Returns a {@link UniqueID} of the AggregationFunkctionEntryID Space.
	 *
	 * @return {@link UniqueID}
	 */
	// FIXME ID SPACE NEEDED?
	@Deprecated
	public static UniqueID getRandomAggregationFunctionEntryID() {
		return aggregationFunctionEntryIDSpace.createRandomID();
	}

	// FIXME ID SPACE NEEDED?
	@Deprecated
	public static UniqueID getRandomDataObjectID() {
		return dataObjectIDSpace.createRandomID();
	}
}
