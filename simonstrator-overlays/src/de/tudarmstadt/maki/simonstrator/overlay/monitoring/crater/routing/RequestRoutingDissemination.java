package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing;

import java.util.Iterator;
import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.requestresponse.RequestMessageLocal;

/**
 * Dissemination to be used by {@link RequestRouting}. This comes with three
 * different 'algorithms': <br>
 * - <b>BROADCAST</b> just broadcasts any messages without further checks <br>
 * - <b>PROBABILISTIC</b> only broadcasts with a certain probability <br>
 * - <b>CONTENTION</b> broadcasts if no other node did so before the
 * contentionTime. <b>Attention:</b> Contention dissemination only works with
 * {@link RequestMessageLocal}s.
 * 
 * @author Christoph Storm
 * @version 2016.07.27
 */
public class RequestRoutingDissemination {

	public enum DisseminationType {
		BROADCAST, PROBABILISTIC, CONTENTION
	};

	private CraterNodeComponent parent;
	private Dissemination dis;

	/**
	 * @param parent
	 *            CraterNodeComponent
	 * @param type
	 *            of Dissemination to be used
	 * @param value
	 *            The given Value is read as sending-probability for
	 *            PROBABILISTIC, and as maximum waiting-time for CONTENTION.
	 */
	public RequestRoutingDissemination(CraterNodeComponent parent, DisseminationType type, double value) {
		this.parent = parent;
		dis = getDissemination(type, value);
	}

	/**
	 * Disseminates the message according to the set DisseminationType
	 * 
	 * @param msg
	 */
	public void disseminate(OverlayMessage msg) {
		dis.disseminate(msg);
	}

	private Dissemination getDissemination(DisseminationType type, double value) {
		switch (type) {
		case PROBABILISTIC:
			return new Probabilistic(value);
		case CONTENTION:
			return new Contention((long) value);
		default:
			return new Broadcast();
		}
	}

	private interface Dissemination {
		public void disseminate(OverlayMessage msg);
	}

	/**
	 * Directly broadcasts the message
	 */
	private class Broadcast implements Dissemination {
		@Override
		public void disseminate(OverlayMessage msg) {
			parent.sendLocalBroadcast(msg);
		}
	}

	/**
	 * Broadcasts the message with a given probability, i.e., when a random
	 * value is below said probability
	 */
	private class Probabilistic implements Dissemination {
		private double probability;

		public Probabilistic(double probability) {
			assert probability > 0d;
			assert probability <= 1d;
			this.probability = probability;
		}

		@Override
		public void disseminate(OverlayMessage msg) {
			double random = Randoms.getRandom(RequestRoutingDissemination.class).nextDouble();
			if (random < probability) {
				parent.sendLocalBroadcast(msg);
			}
		}
	}

	/**
	 * This really only works with RequestMessageLocals! It waits for up to a
	 * certain time for faster nodes, then broadcasts the message if no one else
	 * did
	 */
	private class Contention implements Dissemination, TransMessageListener {

		private long maxContention;
		private LinkedList<RequestMessageLocal> messagesToSend = new LinkedList<RequestMessageLocal>();
		private SiSComponent sis;

		public Contention(long contention) {
			assert contention > 0;
			this.maxContention = contention;

			OverlayContact craterContact = parent.getLocalOverlayContact();
			try {
				UDP transport = parent.getHost().getTransportComponent().getProtocol(UDP.class,
						craterContact.getNetID(NetInterfaceName.WIFI), craterContact.getPort(NetInterfaceName.WIFI));
				transport.setTransportMessageListener(this);

				sis = parent.getHost().getComponent(SiSComponent.class);

			} catch (ProtocolNotAvailableException | ComponentNotAvailableException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		}

		@Override
		public void disseminate(OverlayMessage msg) {
			messagesToSend.add((RequestMessageLocal) msg);

			long contention = maxContention;

			try {
				// Value between 0.0 and 1.0
				double energy = sis.get().localState(SiSTypes.ENERGY_BATTERY_LEVEL, SiSRequest.NONE) / 100d;

				// value between 0, and maxContention
				contention = (long) (contention * (1d - energy));

			} catch (InformationNotAvailableException e) {
				// Not a threatening exception, but should not happen.
				e.printStackTrace();
			}

			Event.scheduleWithDelay(contention, new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					RequestMessageLocal msg = (RequestMessageLocal) content;
					if (parent.isActive() && messagesToSend.remove(msg)) {
						// only send if the message was still to be send
						parent.sendLocalBroadcast(msg);
					} else if (!parent.isActive()) {
						messagesToSend.clear();
					}

				}
			}, msg, 0);
		}

		@Override
		public void messageArrived(Message msg, TransInfo sender, int commID) {

			// Check if it was a RequestMessage, and if we were to send it, too

			if (msg instanceof RequestMessageLocal) {

				UniqueID uID = ((RequestMessageLocal) msg).getUniqueMsgID();

				Iterator<RequestMessageLocal> iter = messagesToSend.iterator();
				while (iter.hasNext()) {
					RequestMessageLocal nextMsg = iter.next();
					if (nextMsg.getUniqueMsgID().equals(uID)) {
						// someone sent earlier than we, remove!
						iter.remove();
						return;
					}
				}
			}

		}

	}
}
