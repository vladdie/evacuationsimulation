package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.cloudlet;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.sinksendinginterval.SinkAdvIntervalModifier;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.advertising.CloudletAdvertisingMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.NotADistribution;

/**
 * Cloudlet Advertising. Currently only periodic from the beginning on. Thus, similar to a normal AP that radiates the
 * whole time.
 * 
 * @author Nils Richerzhagen
 *
 */
public class CloudletAdvertisingImpl implements CloudletAdvertising {

	private final CraterCloudletComponent parentComp;

	public static final int INITIAL_GRADIENT = 10;

	private int CURRENT_TTL;

	private long _CLOUDLET_ADV_INTERVAL;

	private double lastSpeedUpdate = -1;
	private boolean speedChanged = false;
	private NotADistribution advInterval;

	private SinkAdvIntervalModifier intervalDeterminator;
	private double intervalDivisor;

	private PeriodicOperation<CraterCloudletComponent, Object> periodicCloudletAdvOperation;

	public CloudletAdvertisingImpl(CraterCloudletComponent parentComp, int INITIAL_TTL, long CLOUDLET_ADV_INTERVAL,
			SinkAdvIntervalModifier intervalDeterminator, double intervalDivisor) {
		this.parentComp = parentComp;
		this.CURRENT_TTL = INITIAL_TTL;
		this._CLOUDLET_ADV_INTERVAL = CLOUDLET_ADV_INTERVAL;
		this.intervalDeterminator = intervalDeterminator;
		this.intervalDivisor = intervalDivisor;
	}

	@Override
	public void sendAdvBeacon() {
		/*
		 * Insanity checks
		 */
		// TODO Any?

		Monitor.log(CloudletAdvertisingImpl.class, Monitor.Level.INFO, "Sending CloudletAdvBeacon");
		CloudletAdvertisingMessage cloudletAdvMsg = new CloudletAdvertisingMessage(parentComp.getLocalOverlayContact(),
				parentComp.getHost().getId(), INITIAL_GRADIENT, CURRENT_TTL);
		parentComp.sendLocalBroadcast(cloudletAdvMsg);

	}


	@Override
	public void initialize() {

		// This is a hack, not a distribution...
		advInterval = new NotADistribution(_CLOUDLET_ADV_INTERVAL);

		/*
		 * Start periodic operations
		 */
		periodicCloudletAdvOperation = new PeriodicOperation<CraterCloudletComponent, Object>(parentComp,
				Operations.getEmptyCallback(), advInterval, Time.MICROSECOND) {
			
			@Override
			public Object getResult() {
				return null;
			}
			
			@Override
			protected void executeOnce() {
				sendAdvBeacon();
				updateAdvTiming();
			}
		};
		periodicCloudletAdvOperation.start();
	}

	/**
	 * Carbon copy of the same method in SinkAdvertisementImpl <br>
	 * Called from inside executeOnce of the periodicSinkAdvOperation (i.e., the
	 * scope of periodicOperartion), to alter the nextInterval before the
	 * PeriodicOperation has read it.
	 */
	public void updateAdvTiming() {
		// get SpeedUpdates
		if (lastSpeedUpdate < 0 || !speedChanged) {
			return;
		} else {
			long nextInterval = intervalDeterminator.getNextInterval(lastSpeedUpdate);

			// Cloudlets were supposed to send more frequently than mobile Sinks
			nextInterval = (long) (nextInterval / intervalDivisor);
			// Max error due to not properly rounding <1us...

			advInterval.setInterval(nextInterval);
			speedChanged = false;

			// System.out.println("Sink" +
			// parentComp.getHost().getHostId() + " set
			// cloudletADVInterval to "
			// + Time.getFormattedTime(nextInterval));
		}
	}

	public void setLastSpeedUpdate(double speedUpdate) {
		this.lastSpeedUpdate = speedUpdate;
		speedChanged = true;
	}

}
