package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation.DuplicateInsensitiveAggregationFunction.DuplicateInsensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attribute.Attributes;

/**
 * Interface for aggregation data objects used in {@link DataMessageContainer}.
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 * 
 */
public interface DataObjectAggregation<T> {

	/**
	 * Returning the Id of the {@link Attributes} this aggregation object
	 * contains.
	 * 
	 * @return
	 */
	public int getAttributeId();

	/**
	 * Returning the ID as defined in
	 * {@link DuplicateInsensitiveAggregationFunctionsDescriptions}
	 * 
	 * @return
	 */
	public String getAggregationFunctionId();

	/**
	 * Returning the description of the internal
	 * {@link DuplicateInsensitiveAggregationFunctionsDescriptions}.
	 * 
	 * @return
	 */
	public String getAggregationFunctionDescription();

	/**
	 * Returning the current aggregate.
	 * 
	 * @return
	 */
	public T getAggregateValue();

	/**
	 * Attribute and aggregation function are the same in both objects. Thus the
	 * entries can be merged!
	 * 
	 * @param toCompareAggregationObject
	 * @return
	 */
	public boolean ofSameType(DataObjectAggregation<T> toCompareAggregationObject);

}
