package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.sinkpreference;

import java.util.AbstractMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

/**
 * Abstract method to implement different criteria for selecting a sink
 * 
 * @author Jonas Huelsmann
 *
 */
public abstract class AbstractSinkPreference implements SinkPreference {

	protected int BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES;
	protected Map<INodeID, SinkTableEntry> sinkTable;

	public AbstractSinkPreference(int BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES,
			Map<INodeID, SinkTableEntry> unmodifiableSinkTable) {
		this.BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES = BEST_SINK_SORTING_MIN_NUMBER_OF_SUCCESSIVE_UPDATES;
		sinkTable = unmodifiableSinkTable;

	}

	/**
	 * Method to change the sink table used to determine the best sink
	 * 
	 * @param unmodifiableSinkTable
	 *            The new sink table
	 */
	@Override
	public void setNewSinkTable(Map<INodeID, SinkTableEntry> unmodifiableSinkTable) {
		sinkTable = unmodifiableSinkTable;
	}

	@Override
	public abstract AbstractMap.SimpleEntry<INodeID, Double> getBestSink();

}
