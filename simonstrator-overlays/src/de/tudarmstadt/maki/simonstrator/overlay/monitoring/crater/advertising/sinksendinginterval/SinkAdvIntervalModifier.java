package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.advertising.sinksendinginterval;

/**
 * sinkAdvIntervalModifiers are to be used to compute the next interval of the
 * sinkAdvertisement. <br>
 *
 * <b>NOTE:</b> There is currently only one instance of the interface
 * implementation generated, which used by all Sink/Cloudlet -Advertisements!
 * 
 * @author Christoph Storm
 */
public interface SinkAdvIntervalModifier {

	/**
	 * returns an interval-duration for the given speed of nodes in this cluster
	 * 
	 * @param speedOfNodes
	 * @return
	 */
	public long getNextInterval(double speedOfNodes);

	/**
	 * Returns the maximum Interval duration that this Modifier generates
	 * 
	 * @return
	 */
	public long getMaxInterval();

}
