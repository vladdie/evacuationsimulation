package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.util.ResponseEntry;

/**
 * This is a Response -Object that carries response values of potentially
 * several nodes for a specific {@link Request} by a specific node. The
 * ResponseObject already supports several requested SiSTypes per Request.
 * <i>Originally an Error field was to be supported for nodes unable to acquire
 * the requested info. This is no longer supported</i> Nodes that do not have
 * the info should simply not answer, or put in a 'null'-value, if that carries
 * any sort of additional Information/ significance.
 */
public class ResponseObject implements Transmitable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3393720722910014972L;

	private INodeID requester;
	private int requestHash;
	private long validity;

	// Map instead of array for easier mapping (d'oh) of values to request-type
	// for when several Types are requested simultaneously (futureproof'd)
	private Map<SiSType<?>, Set<ResponseEntry>> responseValues;

	public ResponseObject(INodeID requester, int requestHash, long validity) {
		this.requester = requester;
		this.requestHash = requestHash;
		this.validity = validity;
		responseValues = new HashMap<SiSType<?>, Set<ResponseEntry>>();
	}

	public INodeID getRequesterID() {
		return requester;
	}

	public int getRequestHash() {
		return requestHash;
	}

	public long getValidity() {
		return validity;
	}

	public Map<SiSType<?>, Set<ResponseEntry>> getAllResponseValues() {
		return responseValues;
	}

	public Set<SiSType<?>> getResponseTypes() {
		return responseValues.keySet();
	}

	public Set<ResponseEntry> getResponsesForType(SiSType<?> type) {
		return responseValues.get(type);
	}

	/**
	 * Adds an Entry for the given SiSType, NodeID and Value
	 * 
	 * @param type
	 * @param nodeID
	 * @param value
	 */
	public <T> void addResponseValue(SiSType<T> type, INodeID nodeID, Object value) {

		ResponseEntry entry = new ResponseEntry(nodeID, value);
		Set<ResponseEntry> list;
		if (responseValues.containsKey(type)) {
			list = responseValues.get(type);
			for (ResponseEntry data : list) {
				if (data.equals(entry)) {
					// already present
					return;
				}
			}
		} else {
			list = new HashSet<ResponseEntry>();
			responseValues.put(type, list);
		}
		list.add(entry);
	}

	/**
	 * Convenience merger method
	 * 
	 * @param other
	 * @return <i>true</i> if the other ResponseObject was merged, <i>false</i>
	 *         if it belonged to a different Request. <br>
	 *         A returned <i>true</i> does not imply that this ResponseObject
	 *         <i>changed</i> in size or otherwise because of the merger. All it
	 *         does state is that this <i>now</i> includes the data of the other
	 *         ResponseObject. But the data could already have been included
	 *         before.
	 */
	public boolean addResponse(ResponseObject other){
		if (other.getRequestHash() == this.requestHash && other.getRequesterID().equals(this.requester)) {
			for(SiSType<?> type : other.getResponseTypes()){
				responseValues.get(type).addAll(other.getResponsesForType(type));
			}
			return true;
		}
		return false;
	}


	@Override
	public int getTransmissionSize() {
		/*
		 * INodeID requester (8) + int requestHash (4) + long validity (8) +
		 * errors -> SiSType; which basically acts as Enum. So (1) per entry? +
		 * Map responseValues
		 */
		int nodeIDsize = requester.getTransmissionSize();
		int size = nodeIDsize;
		size += 12;

		for (SiSType<?> type : responseValues.keySet()) {
			switch (type.getType().getSimpleName()) {
			case "Double":
				// 8 per entry-value + 8 per entry-nodeID + 1 for type in map
				size += responseValues.get(type).size() * (8 + nodeIDsize) + 1;
				break;
			case "Location":
				// Location + 1 for type
				Set<ResponseEntry> list = responseValues.get(type);
				int locationSize = 0;

				// get the first location for the used size...
				for (ResponseEntry entry : list) {
					if (entry.getValue() != null) {
						locationSize = ((Location) entry.getValue()).getTransmissionSize();
						break;
					}
				}

				int sizePerEntry = locationSize + nodeIDsize;
				size += sizePerEntry * list.size() + 1;
				break;
			case "Graph":
				// nope
				// TODO iterate over every node and every edge in this graph to
				// get all information instances connected to these
				// Just never send one of those, please?! :/
				break;
			default:
				// 12? No Idea, do nothing...
			}
		}

		return size;
	}

}
