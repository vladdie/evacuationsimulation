package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation;

import java.util.HashMap;
import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataMessageContainer.MergePoint;

/**
 * @author Nils Richerzhagen
 * @version 1.0, 26.08.2014
 */
public abstract class AbstractDuplicateSensitiveAggregationFunction implements DuplicateSensitiveAggregationFunction<Double>
{

    /*
     * For duplicate detection a full reference to the included values,
     * respective nodeId's must be available. Otherwise it is not possible to
     * kick out entries at a sink!
     */
    private HashMap<INodeID, AggregationFunctionEntry> currentUsedEntries = new HashMap<INodeID, AggregationFunctionEntry>();
    // solely used for merging!
    private LinkedList<AggregationFunctionEntry> includedEntries = new LinkedList<AggregationFunctionEntry>();

    private LinkedList<UniqueID> includedEntriesIDs = new LinkedList<UniqueID>();

    public AbstractDuplicateSensitiveAggregationFunction(AggregationFunctionEntry toAddEntry)
    {
        includedEntries.add(toAddEntry);
        includedEntriesIDs.add(toAddEntry.getUniqueEntryID());
        currentUsedEntries.put(toAddEntry.getKey(), toAddEntry);
    }

    public AbstractDuplicateSensitiveAggregationFunction(AbstractDuplicateSensitiveAggregationFunction toCloneAggregationFunction)
    {
        this.currentUsedEntries = new HashMap<INodeID, AggregationFunctionEntry>(toCloneAggregationFunction.currentUsedEntries);
        this.includedEntriesIDs = new LinkedList<UniqueID>(toCloneAggregationFunction.includedEntriesIDs);
        this.includedEntries = new LinkedList<AggregationFunctionEntry>(toCloneAggregationFunction.includedEntries);
    }


    /**
     * Cloning.
     *
     * @return
     */
    @Override
    public abstract AbstractDuplicateSensitiveAggregationFunction clone();

    @Override
    public void putValue(AggregationFunctionEntry toAddEntry, MergePoint mergePoint)
    {
        if (includedEntriesIDs.contains(toAddEntry.getUniqueEntryID()))
        {
            if (hasDataRoutingAnalyzer())
            {
                switch (mergePoint)
                {
                    case IN_NETWORK:
                        getDataRoutingAnalyzer().onAggregationDupSensitiveDuplicateDetectedAtSink();
                        break;
                    case CENTRAL:
                        getDataRoutingAnalyzer().onAggregationDupSensitiveDuplicateDetectedAtServer();
                        break;
                    case DURING_CLONE:
                        throw new Error("Duplicate detection cannot happen during clone");
                    case IN_ANALYZER:
                        break;
                }
            }
        } else
        {
            // replace a 'older' used entry with the new entry for that specific
            // node even if there is no such old entry use the received.
            currentUsedEntries.put(toAddEntry.getKey(), toAddEntry);
            includedEntries.add(toAddEntry);
            includedEntriesIDs.add(toAddEntry.getUniqueEntryID());
        }
    }

    @Override
    public LinkedList<AggregationFunctionEntry> getUsedEntries()
    {
        return new LinkedList<AggregationFunctionEntry>(currentUsedEntries.values());
    }

    @Override
    public LinkedList<AggregationFunctionEntry> getAllIncludedEntriesForMerge()
    {
        return includedEntries;
    }

    @Override
    public LinkedList<UniqueID> getIncludedEntriesIDs()
    {
        return includedEntriesIDs;
    }

    /*
     * Analyzing convenience methods.
     */
    private boolean checkedForDataRoutingAnalyzer = false;

    private DataRoutingAnalyzer dataRoutingAnalyzer = null;

    /**
     * True, if a {@link DataRoutingAnalyzer} is provided.
     *
     * @return
     */
    public boolean hasDataRoutingAnalyzer()
    {
        if (dataRoutingAnalyzer == null && !checkedForDataRoutingAnalyzer)
        {
            getDataRoutingAnalyzer();
        }
        return dataRoutingAnalyzer != null;
    }

    public DataRoutingAnalyzer getDataRoutingAnalyzer()
    {
        if (!checkedForDataRoutingAnalyzer)
        {
            try
            {
                dataRoutingAnalyzer = Monitor.get(DataRoutingAnalyzer.class);
            } catch (AnalyzerNotAvailableException e)
            {
                dataRoutingAnalyzer = null;
            }
            checkedForDataRoutingAnalyzer = true;
        }
        return dataRoutingAnalyzer;
    }
}
