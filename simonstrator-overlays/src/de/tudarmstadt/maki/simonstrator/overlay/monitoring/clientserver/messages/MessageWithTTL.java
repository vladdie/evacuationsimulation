package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages;

/**
 * Interface for a message with TTL
 * A sink advertisement message e.g.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 05.06.2014
 * 
 */
public interface MessageWithTTL{

	/**
	 * Indicator if TTL field is 0.
	 * @return <code>true</code> if TTL = 0, thus msg should not be forwarded.
	 * <code>false</code> if TTL > 0, thus msg can still be forwarded.
	 */
	public boolean deadMessage();

}
