package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageCallback;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.api.IOverlayMessageAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.cloudlet.CraterCloudletComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterBackendMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterCellMessage;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.CraterLocalMessage;

/**
 * Base class for Crater components (on central entity and mobile entities).
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 06.08.2014
 * 
 */
public abstract class CraterComponent extends AbstractCraterComponent implements TransMessageListener {

	/**
	 * Transport protocol for ad hoc communication between mobile devices and edge infrastructure devices.
	 */
	private MessageBasedTransport localTransport;

	/**
	 * Transport protocol for cellular communication.
	 */
	private MessageBasedTransport cellTransport;

	/**
	 * Transport protocol in-between backend entities, e.g. {@link CraterCloudletComponent}
	 * <-> {@link CraterCentralComponent}
	 */
	private MessageBasedTransport backendTransport;

	private BasicOverlayContact ownContact;

	/**
	 * Used for communication via the cellular network. E.g. from {@link CraterNodeComponent}s (acting as sinks) to the
	 * {@link CraterCentralComponent}.
	 */
	public static NetInterfaceName netTypeCell = NetInterfaceName.MOBILE;

	/**
	 * Used for communication via the mobile ad hoc network using (currently) the Wi-Fi interface. E.g. communication
	 * between {@link CraterNodeComponent}s.
	 */
	public static NetInterfaceName netTypeLocal = NetInterfaceName.WIFI;

	/**
	 * Used for backend communication. E.g. from {@link CraterCloudletComponent}s to the {@link CraterCentralComponent}.
	 */
	public static NetInterfaceName netTypeBackend = NetInterfaceName.ETHERNET;

	protected final int localPort = 32;

	private final int uploadPort = 64;

	private final CraterNodeType nodeType;

	/**
	 * Local reference to cloud contact {@link CraterCentralComponent}
	 */
	private OverlayContact cloudContact = null;

	private static IOverlayMessageAnalyzer _messageAnalyzer = null;

	private static boolean _messageAnalyzerInit = false;

	public CraterComponent(Host host, CraterNodeType nodeType) {
		super(host);
		this.nodeType = nodeType;
		// FIXME NodeType support in Crater, use node Type for some insanity
		// checks!
	}

	/**
	 * There might be other types added later.
	 * 
	 * @author Nils Richerzhagen
	 *
	 */
	public enum CraterNodeType {
		/**
		 * As implemented by {@link CraterNodeComponent}
		 */
		MOBILE_NODE,
		/**
		 * As implemented by {@link CraterCentralComponent}
		 */
		CLOUD,

		/**
		 * As implemented by {@link CraterCloudletComponent}
		 */
		CLOUDLET;
	}


	@Override
	public void initialize() {
		super.initialize();

		// Create OverlayContact
		ownContact = new BasicOverlayContact(getHost().getId());

		NetInterface cellular = getHost().getNetworkComponent().getByName(netTypeCell);
		NetInterface wifi = getHost().getNetworkComponent().getByName(netTypeLocal);
		NetInterface backend = getHost().getNetworkComponent().getByName(netTypeBackend);

		/* Changed NetInterface checks to ^ (XOR) operator to have complete guaranty for the right configuration.
		 * When using OR there wouldn't be errors if a component has more interfaces then intended, e.g. the Cloud has Wifi.
		 */
		assert cellular != null
				^ nodeType == CraterNodeType.CLOUDLET : "NetInterface 'cellular' not set up properly for  " + nodeType;
		assert wifi != null
				^ nodeType == CraterNodeType.CLOUD : "NetInterface 'wifi' not set up properly for  " + nodeType;
		assert backend != null
				^ nodeType == CraterNodeType.MOBILE_NODE : "NetInterface 'backend/ethernet' not set up properly for  " + nodeType;

		try {
			if (cellular != null) {
				cellTransport = getAndBindUDP(cellular.getLocalInetAddress(), uploadPort, null);
				cellTransport.setTransportMessageListener(this);
				ownContact.addTransInfo(netTypeCell, cellTransport.getTransInfo());
			}
			if (wifi != null) {
				localTransport = getAndBindUDP(wifi.getLocalInetAddress(), localPort, null);
				localTransport.setTransportMessageListener(this);
				ownContact.addTransInfo(netTypeLocal, localTransport.getTransInfo());
			}
			// else {
			// assert cloudContact == null && ownUploadContact != null;
			// cloudContact = ownUploadContact;
			// }
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError("Protocol Binding failed!");
		}

		/*
		 * Initialize the backend transport between cloud and cloudlets (ETHERNET)
		 */
		if (backend != null) {
			try {
				backendTransport = getAndBindUDP(backend.getLocalInetAddress(), uploadPort, null);
				backendTransport.setTransportMessageListener(this);
				// Add cloud transport to our own trans info
				ownContact.addTransInfo(netTypeBackend, backendTransport.getTransInfo());
			} catch (ProtocolNotAvailableException e) {
				throw new AssertionError("Cloudlets have to have a backend interface!");
			}
		}

	}

	@Override
	public OverlayContact getLocalOverlayContact() {
		return ownContact;
	}

	/**
	 * Send the given message to the cloud (or from the cloud to the clients), using the "Cellular" interface.
	 * 
	 * @param m
	 */
	public void sendViaCellular(OverlayMessage m) {
		assert m instanceof CraterCellMessage;
		// Only unicasts
		assert m.getReceiver() != null && m.getSender() != null;
		assert m.getReceiver().equals(getCloudContact()) || m.getSender().equals(getCloudContact());
		cellTransport.send(m, m.getReceiver().getNetID(netTypeCell), m.getReceiver().getPort(netTypeCell));
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onSentOverlayMessage(m, getHost(), netTypeCell);
		}
	}

	/**
	 * Send the given message via local ad hoc broadcast. Between {@link CraterNodeComponent}s.
	 * 
	 * @param m
	 */
	public void sendLocalBroadcast(OverlayMessage m) {
		assert m instanceof CraterLocalMessage;
		// FIXME Additional assert statements?
		localTransport.send(m, localTransport.getNetInterface().getBroadcastAddress(), localTransport.getLocalPort());
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onSentOverlayMessage(m, getHost(), netTypeLocal);
		}
	}

	/**
	 * Send the given message via local ad hoc unicast to a specific receiver
	 * {@link CraterNodeComponent}, and waits for an answer.
	 * 
	 * @param m
	 */
	public void sendLocalUnicast(OverlayMessage m, TransMessageCallback senderCallback, long timeout) {
		assert m instanceof CraterLocalMessage;
		assert m.getReceiver() != null;
		// This check is a bit redundant as the UDP implementation internally
		// only uses the sendAndWait method...
		if (senderCallback != null && timeout > 0) {
			localTransport.sendAndWait(m, m.getReceiver().getNetID(netTypeLocal), localTransport.getLocalPort(),
					senderCallback, timeout);
		} else {
			localTransport.send(m, m.getReceiver().getNetID(netTypeLocal), localTransport.getLocalPort());
		}
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onSentOverlayMessage(m, getHost(), netTypeLocal);
		}
	}

	/**
	 * Send the given reply message directly to a specific receiver
	 * {@link CraterNodeComponent}
	 * 
	 * @param m
	 */
	public void sendLocalReply(OverlayMessage m, int commID) {
		assert m instanceof CraterLocalMessage;
		assert m.getReceiver() != null;
		localTransport.sendReply(m, m.getReceiver().getNetID(netTypeLocal), localTransport.getLocalPort(), commID);
		/*
		 * if (hasMessageAnalyzer()) {
		 * getMessageAnalyzer().onSentOverlayMessage(m, getHost(),
		 * netTypeLocal); }
		 */
	}

	/**
	 * Sends the given message to the {@link CraterCentralComponent} via backend communication.
	 * 
	 * @param m
	 */
	public void sendViaBackend(OverlayMessage m) {
		assert m instanceof CraterBackendMessage;
		// Only unicasts
		assert m.getReceiver() != null && m.getSender() != null;
		assert m.getReceiver().equals(getCloudContact()) || m.getSender().equals(getLocalOverlayContact());
		backendTransport.send(m, m.getReceiver().getNetID(netTypeBackend), m.getReceiver().getPort(netTypeBackend));
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onSentOverlayMessage(m, getHost(), netTypeBackend);
		}

	}

	/**
	 * Returns the servers's overlay contact.
	 * 
	 * @return
	 */
	public OverlayContact getCloudContact() {
		if (cloudContact == null) {
			if (Oracle.isSimulation()) {
				for (Host host : Oracle.getAllHosts()) {
					try {
						CraterCentralComponent cloud = host.getComponent(CraterCentralComponent.class);
						cloudContact = cloud.getLocalOverlayContact();
						break;
					} catch (ComponentNotAvailableException e) {
						// don't care
					}
				}
				if (cloudContact == null) {
					throw new AssertionError("Cloud not found.");
				}
			} else {
				throw new AssertionError("No Simulation.");
				/**
				 * Find Cloud via IP and Port
				 */
				// TransInfo cloudTransInfo = cloudTransport.getTransInfo(
				// cloudTransport.getNetInterface().getByName(serverIP),
				// cloudPort);
				// cloudContact = new BasicOverlayContact(INodeID.get(-1),
				// cloudTransport.getNetInterface().getName(),
				// cloudTransInfo);
			}
		}
		return cloudContact;
	}

	/**
	 * Returns the {@link CraterNodeType} of this node.
	 * 
	 * @return
	 */
	public CraterNodeType getNodeType() {
		return nodeType;
	}

	/*
	 * Analyzers
	 */

	public IOverlayMessageAnalyzer getMessageAnalyzer() {
		return _messageAnalyzer;
	}

	public boolean hasMessageAnalyzer() {
		if (!_messageAnalyzerInit) {
			try {
				_messageAnalyzer = Monitor.get(IOverlayMessageAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				//
			}
			_messageAnalyzerInit = true;
		}
		return _messageAnalyzer != null;
	}

}
