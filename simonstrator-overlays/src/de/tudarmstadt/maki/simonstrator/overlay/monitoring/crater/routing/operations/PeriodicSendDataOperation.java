package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.operations;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.CraterNodeComponent;

/**
 * Simply refactored the Operation into its own class, to allow a seamless (as
 * in: Use the very same damn OP, not a new one each time) transition between
 * Routing Implementations
 */
public class PeriodicSendDataOperation extends PeriodicOperation<CraterNodeComponent, Object> {

	private long nextExecutionTime = Long.MAX_VALUE;

	public PeriodicSendDataOperation(CraterNodeComponent component, OperationCallback<Object> callback,
			long interval) {
		super(component, callback, interval);
		// TODO Auto-generated constructor stub
	}

	@Override
	public Object getResult() {
		return null;
	}

	@Override
	protected void executeOnce() {
		nextExecutionTime = Time.getCurrentTime() + this.getInterval();
		this.getComponent().getDataRouting().sendBufferContent();
	}

	/**
	 * Returns the next time this operation will Execute
	 * 
	 * @return
	 */
	public long getNextExecutionTime() {
		if (this.isStopped()) {
			return Long.MAX_VALUE;
		}
		return nextExecutionTime;
	}
}
