package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.util;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent.ClientServerNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.api.ClientServerSubComponent;

/**
 * Listener is informed when the {@link ClientServerNodeRole} is changed. Enables real-time maintaining of
 * {@link ClientServerSubComponent}s and other listeners.
 * 
 * @author Nils Richerzhagen
 *
 */
public interface ClientServerNodeRoleListener {

	public void clientServerNodeRoleChanged(ClientServerNodeRole clientServerNodeRole);
}
