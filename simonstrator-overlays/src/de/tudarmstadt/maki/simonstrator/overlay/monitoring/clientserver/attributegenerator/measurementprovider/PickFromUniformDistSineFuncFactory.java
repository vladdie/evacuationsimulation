package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.attributegenerator.measurementprovider;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.util.Distribution;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

public class PickFromUniformDistSineFuncFactory implements IMeasurementProviderFactory {

	protected String identifier;

	private Distribution dist;

	long waveLength;
	
	@XMLConfigurableConstructor({ "identifier", "dist", "waveLength" })
	public PickFromUniformDistSineFuncFactory(String identifier, Distribution dist, long waveLength) {
		super();
		this.identifier = identifier;
		this.dist = dist;
		this.waveLength = waveLength;
	}
	
	@Override
	public IMeasurementProvider getMeasurementProviderFor(Host host) {
		return new PickFromUniformDistSineFunc(dist.returnValue());
	}
	
	
	private class PickFromUniformDistSineFunc implements IMeasurementProvider{

		private double offset;
		
		public PickFromUniformDistSineFunc(double offset) {
			this.offset = offset;
		}
		
		@Override
		public String getIdentifier() {
			return identifier;
		}

		@Override
		public double getNextValue() {
			long time = Time.getCurrentTime();
			double value = 2 + offset
					+ Math.sin((time / (double) waveLength) * 2 * Math.PI);
			return value;
		}
		
	}

}
