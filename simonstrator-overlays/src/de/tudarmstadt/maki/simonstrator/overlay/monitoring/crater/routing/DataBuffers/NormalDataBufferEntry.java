package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 10.07.2014
 * 
 */
public class NormalDataBufferEntry {
	private int attributeId;
	private double value;

	public NormalDataBufferEntry(int attributeId, double value) {
		this.attributeId = attributeId;
		this.value = value;
	}

	public int getAttributeId() {
		return attributeId;
	}

	public double getValue() {
		return value;
	}
}