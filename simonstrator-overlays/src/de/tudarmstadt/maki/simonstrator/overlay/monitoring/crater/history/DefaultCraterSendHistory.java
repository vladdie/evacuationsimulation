package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.history;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;

/**
 * Implementation of {@link SendHistory}, which will only detect
 * duplicates if they are in the buffer
 * 
 * @author Jonas Huelsmann
 * 
 */
public class DefaultCraterSendHistory extends SendHistory {

	private LinkedList<UniqueID> HISTORY;


	public DefaultCraterSendHistory(long ttl) {
		super(ttl);
		HISTORY = new LinkedList<UniqueID>();
	}


	/**
	 * Method that checks if a msgID was in the TreeSet and adds it if it was
	 * not.
	 * 
	 * @param msgID
	 *            The ID of a message
	 * 
	 * @return true if msgID was in the TreeSet, false else
	 */
	private boolean isAnEntry(UniqueID msgID) {

		if (HISTORY.contains(msgID)) {
			HISTORY.remove(msgID);
			HISTORY.add(msgID);
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Method that adds an entry to the History
	 * 
	 * @param msgID
	 *            Entry to be added
	 * 
	 * @return true if successfully added
	 */
	protected boolean addToHistory(UniqueID msgID) {
		if (HISTORY.size() < HISTORY_SIZE) {
			return HISTORY.add(msgID);
		} else {
			HISTORY.removeFirst();
			return HISTORY.add(msgID);
		}

	}

	@Override
	public boolean sendable(UniqueID msgID) {
		if (isAnEntry(msgID)) {
			return false;
		} else {
			addToHistory(msgID);
			return true;
		}
	}


	@Override
	public Set<UniqueID> sendable(Collection<UniqueID> msgIDs) {
		Set<UniqueID> temp = new TreeSet<UniqueID>();
		UniqueID msgID;
		for (Iterator<UniqueID> iter = msgIDs.iterator(); iter.hasNext();) {
			msgID = iter.next();
			if (sendable(msgID))
				temp.add(msgID);
		}
		return temp;

	}

	@Override
	public boolean isInHistory(UniqueID msgID) {
		return HISTORY.contains(msgID);
	}

	@Override
	public void resetHistory() {
		HISTORY.clear();

	}

}
