package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.api;

import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;

/**
 * Marker Interface to see which messages are from the Overlay "ClientServer".
 * 
 * @author Nils Richerzhagen
 *
 */
public interface ClientServerMessage extends OverlayMessage {
	// marker
}
