package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation;

import java.util.LinkedList;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 01.07.2014
 * 
 */
public class AggregationFunctionMean extends AbstractDuplicateSensitiveAggregationFunction{
	
	public AggregationFunctionMean(AggregationFunctionEntry toAddEntry) {
		super(toAddEntry);
	}

	public AggregationFunctionMean(AggregationFunctionMean toCloneAggregationFunction) {
		super(toCloneAggregationFunction);
	}


	public AggregationFunctionMean clone() {
		return new AggregationFunctionMean(this);
	}

	@Override
	public Double getValue() {
		double overallValue = 0;
		
		LinkedList<AggregationFunctionEntry> currentUsedEntries = this.getUsedEntries();

		for (AggregationFunctionEntry actEntry : currentUsedEntries) {
			overallValue += actEntry.getValue();
		}
		return (overallValue / currentUsedEntries.size());
	}

	@Override
	public String getDescription() {
		return DuplicateSensitiveAggregationFunctionsDescriptions.MEAN.toString();
	}

	@Override
	public String getId() {
		return DuplicateSensitiveAggregationFunctionsDescriptions.MEAN.getId();
	}
}
