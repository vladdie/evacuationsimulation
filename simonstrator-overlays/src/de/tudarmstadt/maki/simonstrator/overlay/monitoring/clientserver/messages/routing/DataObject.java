package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.attribute.Attributes;

/**
 * Interface for a {@link DataObject} in a {@link DataMessageContainerOLD}.
 * @author Nils Richerzhagen
 * @version 1.0, 30.06.2014
 *
 */
public interface DataObject <T> {

	/**
	 * Returning the id that is mapped to {@link Attributes}.
	 * @return
	 */
	public int getAttributeId();

	/**
	 * Returning the value of the data.
	 * @return
	 */
	public T getValue();
	
	/**
	 * Returning the nodeId that is source of the data.
	 * @return
	 */
	public INodeID getNodeId();
	
	/**
	 * For analyzing purposes.
	 * @return
	 */
	public UniqueID getUniqueDataObjectID();
}
