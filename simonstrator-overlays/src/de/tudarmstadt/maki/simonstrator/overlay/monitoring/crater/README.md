# The adaptive monitoring system "Crater"

## Respective Papers:
## (1) ftp://ftp.kom.tu-darmstadt.de/papers/RSR+15-1.pdf
## (2) WoWMoM 2016
## For more details, please contact Nils Richerzhagen - nils.richerzhagen@kom.tu-darmstadt.de

_Current Version 06.04.2016

# In PROGRESS not finished!
Crater consists of the following main components

Check maxConnections handling in AAdvGatewaySelection. If Crater should handle with maxCellularConnections there must be the GatewayLogicBehavior set to CUTTING.