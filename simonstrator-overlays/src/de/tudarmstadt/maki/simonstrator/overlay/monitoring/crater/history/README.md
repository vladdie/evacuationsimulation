# Latency optimization in mobile networks

_Based on the Bachelor Thesis 'Latency optimization in mobile networks' by Jonas Huelsmann

## The CraterSendHistory

###Introduction to Duplicate Detection with the CraterSendHistory
Duplicated messages are a common problem in MANETS. Due to the flexible routing used duplicates can occur with ease and may become hard to detect. In CRATER a duplicate detection had been realized at the nodes which hold the role of a sink. Sinks had a buffer which was used to upload the information gathered by this Sink in a periodic interval in a new message container. Duplicates that arrive within the same period the original message was heard had been dropped while those arriving in later periods had not been detected. The introduced class CraterSendHistory decouple the function of duplicate detection from the buffer used before. While the buffer still has the function of packing several received messages into a new message container the duplicate detection is now over several periods.

###Implementation of the CraterSendHistory
The `SendHistory` is is constructed with a long value called `HISTORY_SIZE` representing the "size" of the history. Depending on what concrete implementation of the history is used the "size" has different interpretations.
`SendHistory` has the method `sendable` which takes an `UniqueID` of a message and and return boolean. The return value is meant to be 'true' if the `UniqueID` was not yet heard by the `SendHistory`. The given `UniqueID` will be added to the history automatically. Furthermore the entry in the history of a `UniqueID` is updated if the ID was in the history already, making the entry leaf the history later.
If the `UniqueID` should not modify the history (for example for debug purposes) the method 'isInHistory' can be used. It checks the history for an `UniqueID` without modifying it.
CraterSendHistory had an overloaded version of `sendable` which takes `Collection<UniqueID>` and returns a Set of `UniqueID`'s which had not yet been in the history.
Furthermore an method called `resetHistory` as well as an enumeration called `HistoryType` exist in the class `SendHistory`.
The method `resetHistory` is used for the concrete implementation `DefaultCraterSendHistory` and resets the history every time it is called. In other implementations than the `DefaultCraterSendHistory` the method should throw an exception.
`HistoryTypes` is used to list all concrete implementations of `CraterSendHistory`.

###Subclass implementation
Four subclasses had been implemented so far. The `DefaultCraterSendHistory` is meant represent the functionality of CRATER before the implementation of the `SendHistory`. Duplicates are detected within a buffer upload period like before, the method `resetHistory` is used to clear the history synchronous to the buffer.

The `SizedCraterSendHistory` realizes a `SendHistory` which is limited in size. The `HISTORY_SIZE` value of the `SendHistory` is interpreted as the number of entries saved in the `SizedCraterSendHistory`. If new entries enter the history which would exceed the histories size those who had been added first will leaf the history first.

The `TimedCraterSendHistory` realizes a `SendHistory` which is not limited by size rather then by time. The `HISTORY_SIZE` value of `SendHistory` is interpreted as the time an entry is staying in the history.
Entries which have been longer in the history than the value of `HISTORY_SIZE` will be removed from the history the next time a `sendable` method is called.

The `NoCraterSendHistory` had been added to be a reference for the other histories. It provides no duplicate detection and will answer `sendable` always with true, will return the given collection as a set for the overloaded version of `sendable` and will answer `isInHistory` always with false since no history exists.

###Using the CraterSendHistory
To use the `SendHistory` the value of `CRA_SEND_HISTORY_TYP` in the `system_parameters.xml` needs to be set to the desired value from the `HistoryType` enumeration in the `SendHistory` class.
The `HISTORY_SIZE` can be set by setting the `CRA_SEND_HISTORY_VALUE` in the `system_parameters.xml` to the desired value.
Furthermore `CRA_DATA_MAX_HES_TIME` can be used to set the interval in which the buffers upload their content.
New concrete subclasses of the `SendHistory` should be added to the enumeration and also to the `createHistory` method in the `DataRoutingImpl` class in the package routing.
