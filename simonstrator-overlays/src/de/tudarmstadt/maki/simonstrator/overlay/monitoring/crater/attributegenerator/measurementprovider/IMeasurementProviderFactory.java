package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attributegenerator.measurementprovider;

import de.tudarmstadt.maki.simonstrator.api.Host;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 21.08.2014
 * 
 */
public interface IMeasurementProviderFactory {

	public IMeasurementProvider getMeasurementProviderFor(Host host);
}
