package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.exceptions;

public class NoSuchIdentifierException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * This parameter value should not occur here.
	 * @param <T>
	 */
	public NoSuchIdentifierException(String identifier) {
		super("This identifier is unknown " + identifier);
	}
}
