package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.attributegenerator.measurementprovider;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 21.08.2014
 *
 */
public interface IMeasurementProvider {

	public String getIdentifier();
	
	public double getNextValue();
}
