package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.aggregation;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 02.07.2014
 *
 */
public class AggregationFunctionMin extends AbstractDuplicateInsensitiveAggregationFunction{

	public AggregationFunctionMin(Double value) {
		super(value);
	}
	
	@Override
	public void putValue(Double value) {
		if(getAggregateValue() > value){
			this.setAggregateValue(value);
		}
	}

	@Override
	public String getDescription() {
		return DuplicateInsensitiveAggregationFunctionsDescriptions.MIN.toString();
	}

	@Override
	public String getId() {
		return DuplicateInsensitiveAggregationFunctionsDescriptions.MIN.getId();
	}
}
