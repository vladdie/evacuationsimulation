package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers;

/**
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 05.07.2014
 * 
 */
public class AggregationDataBufferEntry {
	private String aggregationFunctionId;
	private int attributeId;
	private double value;

	public AggregationDataBufferEntry(String aggregationFunctionId, int attributeId, double value) {
		this.aggregationFunctionId = aggregationFunctionId;
		this.attributeId = attributeId;
		this.value = value;
	}

	public String getAggregationFunctionId() {
		return aggregationFunctionId;
	}

	public int getAttributeId() {
		return attributeId;
	}

	public double getValue() {
		return value;
	}
}