# Latency optimization in mobile networks

_Based on the Bachelor Thesis 'Latency optimization in mobile networks' by Jonas Huelsmann

## The CraterContentionScheme 

###Introduction to the Sender-based Contention Scheme
The `ContentionScheme` interface in the package contention provides the functions needed for realizing Sender-based Contention Scheme used in CRATER.
The sender-based contention scheme is used to reduce the network overhead and therefore to reduce the likelihood of collisions. To provide those functions a device of the system has to wait a certain time before sending its information. When the device hears another message while it is waiting to send its own, it has to wait even more to prevent a collision.The calculation of the hesitation time can follow different rules to realize different strategies or data priorities. 

###Implementation of the Sender-based Contention Scheme
The functions needed for the sender-based contention scheme are realized in the interface `ContentionScheme` and the abstract class `AbstractContentionScheme`.
`ContentionScheme` provides the needed information for calculation of the hesitation time, like the minimum and maximum time to hesitate.
It furthermore has a link to its parent node to have access to that node's specific information like its battery power.
The `getHesitationTime` method takes a double value and calculates the hesitation time, represented by a long value. The concrete calculation must be implemented by the inheriting class.
A enumeration, listing all classes inheriting from `ContentionScheme` is also provided by the interface.

###Subclass implementation
The subclasses which had been implemented are `DefaultCraterContentionScheme`, `DiFiCraterContentionScheme` and `MoDiFiCraterContentionScheme`.
`DefaultCraterContentionScheme` behaves like the original contention scheme implemented in CRATER. The hesitation time is calculated depending on a nodes gradient and its remaining battery power.
`DiFiCraterContentionScheme` is meant to archive long message routes by giving distant nodes the ability to send first.
With longer message routes the piggy-backing ability of CRATER can be used more efficiently.
`MoDiFiCraterContentionScheme` is a variant of `DiFiCraterContentionScheme` which uses the distance of a node towards the nearest sink as well as a random factor to be more reliable when it comes to collision prevention.

###Using the CraterContentionScheme 
To use the CraterContentionScheme the value of `CRA_CONTENTION_TYPE` in the `system_parameters.xml` needs to be set to the desired value from the `ContentionSchemeType` enumeration in the `ContentenScheme` class.
New concrete subclasses of the `ContentionScheme` should be added to the enumeration and also to the `createContentionScheme` method in the `DataRoutingImpl` class in the package routing.