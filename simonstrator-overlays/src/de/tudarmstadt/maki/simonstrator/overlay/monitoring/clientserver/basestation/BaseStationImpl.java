package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.basestation;

import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutMap;

@Deprecated
public class BaseStationImpl implements BaseStation {
	private static int MAX_CONNECTED_ENTITIES = 30;

	private static final long MAX_LAST_UPDATE_TIME = 200 * Time.SECOND;

	private static final long MIN_AFFILIATION_TIME = 5 * Time.MINUTE;

	private static final long MAX_AFFILIATION_TIME = 20 * Time.MINUTE;
	
	private Random rand;
	
	/**
	 * Singleton instance of default simulator.
	 */
	private static BaseStationImpl singleton;
	
	private BaseStationImpl() {
		this.singleton = this;
		rand = Randoms.getRandom(BaseStationImpl.class);
	}
	
	/**
	 * Returns the single instance of the BaseStation
	 * 
	 * @return
	 */
	public static BaseStationImpl getInstance() {
		if (singleton == null)
			singleton = new BaseStationImpl();
		return singleton;
	}
	
	@Override
	public void setMaxConnections(int maxAllowedConnections) {
		this.MAX_CONNECTED_ENTITIES = maxAllowedConnections;
	}
	
	/*
	 * Map of connected Hosts. With timestamp when they connected. Timout for
	 * hearing no updates by this host.
	 */
	private static TimeoutMap<Host, Long> connectedHosts = new TimeoutMap<Host, Long>(MAX_LAST_UPDATE_TIME);
	
//	/*
//	 * Map of connected Hosts. With timestamp when they connected. Timout for
//	 * hearing no updates by this host.
//	 */
//	private static TimeoutSet<Host> lastConnectedHosts = new TimeoutSet<Host>(MAX_LAST_UPDATE_TIME);

	@Override
	public boolean canConnectToBS(Host host) {
		// Still space left in list, just add entry! FiFo manner.
		if (connectedHosts.size() < MAX_CONNECTED_ENTITIES) {
			connectedHosts.putNow(host, Time.getCurrentTime());
			return true;
		}
		if(connectedHosts.containsKey(host)){
			// Reached max affiliation time?
			long timeBeingSink = Time.getCurrentTime()
					- connectedHosts.get(host);
			if(timeBeingSink > MAX_AFFILIATION_TIME){
				connectedHosts.remove(host);
				return false;
			}
			else{
				if(allowedToConnect(host)){
					connectedHosts.putNow(host, connectedHosts.get(host));
					return true;
				}
				else{
					connectedHosts.remove(host);
					return false;
				}
			}		
		}
		else{
			// Not already connected and max allowed number is reached, thus block until slot is free.
			return false;
		}
	}
	
	private boolean allowedToConnect(Host host){
		long timeBeingSink = Time.getCurrentTime() - connectedHosts.get(host);
		if(timeBeingSink < MIN_AFFILIATION_TIME){
			return true;
		}
		else{
			double random = rand.nextDouble();
			if(random <= 0.8){
				return true;
			}
			else{
				return false;
			}
		}
	}
}
