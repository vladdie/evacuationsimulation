package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.analyzer;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.DataObjectAggregationDupSensitiveImpl;

/**
 * Analyzer to determine in a interval-based fashion accuracy and the number of
 * nodes participating in the result.
 * 
 * Uses a {@link DataObjectAggregationDupSensitiveImpl} received at the central entity.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 22.08.2014
 * 
 */
public interface AccuracyAndCompletenessAnalyzer extends Analyzer {
	
	/**
	 * 
	 * @param dupSensAggDataObject
	 */
	public void onDuplicateSensitiveAggregationDataObjectReceived(LinkedList<DataObjectAggregationDupSensitiveImpl> dupSensAggDataObject);

}
