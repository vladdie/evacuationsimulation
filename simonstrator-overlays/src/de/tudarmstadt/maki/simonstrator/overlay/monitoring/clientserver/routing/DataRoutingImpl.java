package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.routing;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.ClientServerNodeComponent.ClientServerNodeRole;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.AggregationFunctionEntry;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.DuplicateInsensitiveAggregationFunction.DuplicateInsensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.aggregation.DuplicateSensitiveAggregationFunction.DuplicateSensitiveAggregationFunctionsDescriptions;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.analyzer.DataRoutingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.attribute.Attributes;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.attributegenerator.AttributeGenerator;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.attributegenerator.measurementprovider.IMeasurementProvider;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.exceptions.AggregationFunctionNotKnownException;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataObjectAggregationDupInsensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataObjectAggregationDupSensitiveImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.DataObjectImpl;
import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.messages.routing.SinkUploadContainer;

public class DataRoutingImpl implements DataRouting {
	/*
	 * Config Parameters
	 */
	private static long DATA_UPLOAD_INTERVAL;

	/*
	 * Other fixed parameters
	 */
	private ClientServerNodeComponent parentNode;

	/*
	 * Buffers for own data that should be send or uploaded!
	 * 
	 * CAUTION! These should be filled by workload app! Maybe with listener concept of DataRouting hearing from Workload
	 * app if data is incoming.
	 */
	private LinkedList<NormalDataBufferEntry> buffer_normalData = new LinkedList<NormalDataBufferEntry>();
	private LinkedList<AggregationDataBufferEntry> buffer_duplicateSensitiveAggregateData = new LinkedList<AggregationDataBufferEntry>();
	private LinkedList<AggregationDataBufferEntry> buffer_duplicateInsensitiveAggregateData = new LinkedList<AggregationDataBufferEntry>();

	private PeriodicOperation<ClientServerNodeComponent, Object> periodicUploadOperation;

	/**
	 * 
	 * @param parentNode
	 */
	public DataRoutingImpl(ClientServerNodeComponent parentNode, long DATA_UPLOAD_INTERVAL) {
		this.parentNode = parentNode;
		DataRoutingImpl.DATA_UPLOAD_INTERVAL = DATA_UPLOAD_INTERVAL;
	}

	public void initialize() {
		initializePeriodicOperations();
	}

	/**
	 * Initialization of periodic operations used in {@link DataRoutingImpl}
	 */
	private void initializePeriodicOperations() {
		periodicUploadOperation = new PeriodicOperation<ClientServerNodeComponent, Object>(parentNode, Operations.getEmptyCallback(),
				DATA_UPLOAD_INTERVAL) {

			@Override
			public Object getResult() {
				return null;
			}

			@Override
			protected void executeOnce() {
				uploadBufferContent();
			}
		};
	}

	@Override
	public void uploadBufferContent() {
		switch (parentNode.getClientServerNodeRole()) {
		case SINK:

			boolean hasAddedData = false;

			fillBuffersWithAttributeGeneratorData();
			// No own data to upload.

			SinkUploadContainer uploadMsg = new SinkUploadContainer(parentNode.getLocalOverlayContact(),
					parentNode.getCloudContact(),
					new LinkedList<DataObjectImpl<Double>>(), new LinkedList<DataObjectAggregationDupInsensitiveImpl>(),
					new LinkedList<DataObjectAggregationDupSensitiveImpl>());

			if (!buffer_normalData.isEmpty()) {
				NormalDataBufferEntry firstEntry = buffer_normalData.removeFirst();
				uploadMsg = new SinkUploadContainer(uploadMsg, parentNode.getHost().getId(),
						firstEntry.getAttributeId(), firstEntry.getValue());
				hasAddedData = true;
			}
			if (!buffer_duplicateInsensitiveAggregateData.isEmpty()) {
				AggregationDataBufferEntry firstEntry = buffer_duplicateInsensitiveAggregateData.removeFirst();
				AggregationFunctionEntry toAddEntry = new AggregationFunctionEntry(parentNode.getHost().getId(),
						firstEntry.getValue());
				try {
					uploadMsg = new SinkUploadContainer(uploadMsg, firstEntry.getAggregationFunctionId(),
							firstEntry.getAttributeId(), toAddEntry);
				} catch (AggregationFunctionNotKnownException e) {
					e.printStackTrace();
				}
				hasAddedData = true;
			}
			if (!buffer_duplicateSensitiveAggregateData.isEmpty()) {
				AggregationDataBufferEntry firstEntry = buffer_duplicateSensitiveAggregateData.removeFirst();
				AggregationFunctionEntry toAddEntry = new AggregationFunctionEntry(parentNode.getHost().getId(),
						firstEntry.getValue());
				try {
					uploadMsg = new SinkUploadContainer(uploadMsg, firstEntry.getAggregationFunctionId(),
							firstEntry.getAttributeId(), toAddEntry);
				} catch (AggregationFunctionNotKnownException e) {
					e.printStackTrace();
				}
				hasAddedData = true;
			}
			if (!hasAddedData) {
				System.out.println("Should not happen");
			}

			if (hasAddedData) {
				parentNode.sendViaCellular(uploadMsg);
				// /*
				// * - #A.1# upload msg internals to analyzer for duplicate
				// * detection of real data.
				// */
				if (hasDataRoutingAnalyzer()) {
					getDataRoutingAnalyzer().uploadMsgSend(uploadMsg.getUniqueMsgID(), parentNode);
				}
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void gotEmpty() {

		assert parentNode.isPresent();
		/*
		 * Maybe called when sink or leaf.
		 * 
		 * Leaf: should not be stopped, should now be stopped.
		 * 
		 * Sink: should be stopped.
		 */
		switch (parentNode.getClientServerNodeRole()) {
		case LEAF:
			break;
		case SINK:
			break;
		case OFFLINE:
			throw new AssertionError();
		default:
			throw new AssertionError();
		}
		// if (!parentNode.isSink()) {
		// periodicSendDataOperation.stop();
		// }
		// assert periodicSendDataOperation.isStopped();
	}

	@Override
	public void gotFilled() {

		assert parentNode.isPresent();

		switch (parentNode.getClientServerNodeRole()) {
		case LEAF:
			break;
		case SINK:
			// nothing to do.
			break;
		case OFFLINE:
			throw new AssertionError();
		default:
			throw new AssertionError();
		}
		// if (parentNode.getClientServerNodeRole() ==
		// ClientServerNodeRole.LEAF) {
		// periodicSendDataOperation.start();
		// }
	}

	/**
	 * Just for know until buffers are filled by workload apps.
	 * 
	 * Must be exchanged with some real data. E.g. Position, node state etc. Once the SiS is included and the monitoring
	 * is able to handle requests this must be exchanged.y
	 * 
	 * @return
	 */
	@Deprecated
	private void fillBuffersWithAttributeGeneratorData() {
		try {
			AttributeGenerator attributeGenerator = parentNode.getHost().getComponent(AttributeGenerator.class);
			LinkedList<IMeasurementProvider> measurementProvs = attributeGenerator.getProvs();
			for (IMeasurementProvider actMeasurementProvider : measurementProvs) {
				if (actMeasurementProvider.getIdentifier().equals(parentNode.getMeasurementProviderToUse())) {
					NormalDataBufferEntry normalData = new NormalDataBufferEntry(Attributes.StartupDelay.getAttributeId(),
							actMeasurementProvider.getNextValue());
					buffer_normalData.add(normalData);
					AggregationDataBufferEntry dupInsens = new AggregationDataBufferEntry(
							DuplicateInsensitiveAggregationFunctionsDescriptions.MAX.getId(),
							Attributes.StartupDelay.getAttributeId(), actMeasurementProvider.getNextValue());
					buffer_duplicateInsensitiveAggregateData.add(dupInsens);
					AggregationDataBufferEntry dupSens = new AggregationDataBufferEntry(
							DuplicateSensitiveAggregationFunctionsDescriptions.NODECOUNT.getId(),
							Attributes.StartupDelay.getAttributeId(), actMeasurementProvider.getNextValue());
					buffer_duplicateSensitiveAggregateData.add(dupSens);
				}
			}
		} catch (ComponentNotAvailableException e) {
			e.printStackTrace();
			return;
		}
	}

	/**
	 * 
	 * @author Nils Richerzhagen
	 * @version 1.0, 10.07.2014
	 * 
	 */
	public class NormalDataBufferEntry {
		private int attributeId;
		private double value;

		public NormalDataBufferEntry(int attributeId, double value) {
			this.attributeId = attributeId;
			this.value = value;
		}

		public int getAttributeId() {
			return attributeId;
		}

		public double getValue() {
			return value;
		}
	}

	/**
	 * 
	 * @author Nils Richerzhagen
	 * @version 1.0, 05.07.2014
	 * 
	 */
	public class AggregationDataBufferEntry {
		private String aggregationFunctionId;
		private int attributeId;
		private double value;

		public AggregationDataBufferEntry(String aggregationFunctionId, int attributeId, double value) {
			this.aggregationFunctionId = aggregationFunctionId;
			this.attributeId = attributeId;
			this.value = value;
		}

		public String getAggregationFunctionId() {
			return aggregationFunctionId;
		}

		public int getAttributeId() {
			return attributeId;
		}

		public double getValue() {
			return value;
		}
	}

	/*
	 * Analyzing convenience methods.
	 */
	private boolean checkedForDataRoutingAnalyzer = false;

	private DataRoutingAnalyzer dataRoutingAnalyzer = null;

	/**
	 * True, if a {@link DataRoutingAnalyzer} is provided.
	 * 
	 * @return
	 */
	public boolean hasDataRoutingAnalyzer() {
		if (dataRoutingAnalyzer != null) {
			return true;
		} else if (checkedForDataRoutingAnalyzer) {
			return false;
		}
		getDataRoutingAnalyzer();
		return dataRoutingAnalyzer != null;
	}

	public DataRoutingAnalyzer getDataRoutingAnalyzer() {
		if (!checkedForDataRoutingAnalyzer) {
			try {
				dataRoutingAnalyzer = Monitor.get(DataRoutingAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				dataRoutingAnalyzer = null;
			}
			checkedForDataRoutingAnalyzer = true;
		}
		return dataRoutingAnalyzer;
	}

	@Override
	public void clientServerNodeRoleChanged(ClientServerNodeRole clientServerNodeRole) {
		switch (clientServerNodeRole) {
		case LEAF:
			periodicUploadOperation.stop();
			break;
		case SINK:
			// needed as otherwise potential old data is uploaded (leading to
			// duplicates and more important large latencies) in statistics.
			if (periodicUploadOperation.isStopped()) {
				periodicUploadOperation.start();
			}
			break;
		case OFFLINE:
			periodicUploadOperation.stop();
			break;
		default:
			throw new AssertionError("Unknown case.");
		}
	}
}
