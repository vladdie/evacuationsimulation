package de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.routing.DataBuffers;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.crater.messages.routing.ResponseObject;

/**
 * 'Buffer' for lists of localData buffers... Eases use of transitionEngine for
 * DataRouting -instances
 * 
 * @author Christoph Storm
 *
 */
public class LocalBufferEntryLists {
	// TODO chs: Constructor with state info (local/upload) to minimize memory
	// footprint

	/**
	 * NormalDatabuffer, only used in local context
	 */
	private LinkedList<NormalDataBufferEntry> normalData = new LinkedList<NormalDataBufferEntry>();

	private LinkedList<AggregationDataBufferEntry> duplicateSensitiveAggregateData = new LinkedList<AggregationDataBufferEntry>();
	private LinkedList<AggregationDataBufferEntry> duplicateInsensitiveAggregateData = new LinkedList<AggregationDataBufferEntry>();

	/**
	 * Now this also stores responseObjects that should be sent
	 */
	private LinkedList<ResponseObject> responseObjects = new LinkedList<ResponseObject>();

	public LinkedList<NormalDataBufferEntry> getNormalData() {
		return normalData;
	}

	public LinkedList<AggregationDataBufferEntry> getDuplicateSensitiveAggregateData() {
		return duplicateSensitiveAggregateData;
	}

	public LinkedList<AggregationDataBufferEntry> getDuplicateInsensitiveAggregateData() {
		return duplicateInsensitiveAggregateData;
	}

	public LinkedList<ResponseObject> getResponseObjects() {
		return responseObjects;
	}

	/**
	 * Clears all BufferLists of this instance
	 */
	public void clear() {
		normalData.clear();
		duplicateSensitiveAggregateData.clear();
		duplicateInsensitiveAggregateData.clear();
		responseObjects.clear();
	}

}
