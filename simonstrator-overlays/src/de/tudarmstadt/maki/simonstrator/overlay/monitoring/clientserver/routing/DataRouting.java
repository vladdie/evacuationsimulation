package de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.routing;

import de.tudarmstadt.maki.simonstrator.overlay.monitoring.clientserver.util.ClientServerNodeRoleListener;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.ITimeoutMapListener;

/**
 * Interfaces for DataRouting in ClientServer.
 * 
 * @author Nils Richerzhagen
 * @version 1.0, 06.06.2014
 * 
 */
public interface DataRouting extends ClientServerNodeRoleListener, ITimeoutMapListener
{
	
	public void initialize();

	/**
	 * In case of being sink, upload data to infrastructure.
	 */
	public void uploadBufferContent();

}
