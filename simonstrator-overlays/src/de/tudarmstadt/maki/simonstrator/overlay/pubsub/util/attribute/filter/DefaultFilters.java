package de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute.filter;

import java.util.HashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.OperatorImplementation;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.AbstractPubSubComponent;

/**
 * Default Implementation of Filters as used in the
 * {@link AbstractPubSubComponent} if no filters are overwritten by either the
 * overlay or the application. They try to work on as many types as possible,
 * but might silently (or not so silently) fail on unknown types.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public final class DefaultFilters {

	private static final Map<FilterOperator, OperatorImplementation> filters = new HashMap<FilterOperator, OperatorImplementation>();

	/**
	 * Initialization with default implementations
	 */
	static {
		filters.put(FilterOperator.PREFIX, new PrefixFilter());
		filters.put(FilterOperator.POSTFIX, new PostfixFilter());
		filters.put(FilterOperator.CONTAINS, new ContainsFilter());

		filters.put(FilterOperator.LT, new LessThanFilter());
		filters.put(FilterOperator.LTE, new LessThanEqualFilter());

		filters.put(FilterOperator.GT, new GreaterThanFilter());
		filters.put(FilterOperator.GTE, new GreaterThanEqualFilter());

		filters.put(FilterOperator.EQ, new EqualFilter());
		filters.put(FilterOperator.NEQ, new NotEqualFilter());
	}

	/**
	 * Returns the implementation of a given FilterOperator.
	 * 
	 * @param operator
	 * @return
	 */
	public static <T> OperatorImplementation get(FilterOperator operator,
			Class<T> type) {
		OperatorImplementation implementation = filters.get(operator);
		if (implementation == null) {
			throw new AssertionError("Filter not (yet) available!");
		}
		return (OperatorImplementation) implementation;
	}


	/**
	 * Prefix-Matching, only defined on strings
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private static class PrefixFilter implements OperatorImplementation {
		@Override
		public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
			String prefix = (String) filter.getValue();
			String str = (String) candidate.getValue();
			return str.startsWith(prefix);
		}
	}

	/**
	 * Postfix-Matching,only defined on strings
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private static class PostfixFilter implements OperatorImplementation {
		@Override
		public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
			String postfix = (String) filter.getValue();
			String str = (String) candidate.getValue();
			return str.endsWith(postfix);
		}
	}

	/**
	 * Contains-Matching
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private static class ContainsFilter implements OperatorImplementation {
		@Override
		public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
			String filterValue = (String) filter.getValue();
			String candidateValue = (String) candidate.getValue();
			return candidateValue.contains(filterValue);
		}
	}

	/**
	 * Less-Than Matching
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private static class LessThanFilter implements OperatorImplementation {
		@Override
		public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
			Number fltrValue = (Number) filter.getValue();
			Number candValue = (Number) candidate.getValue();
			return candValue.doubleValue() < fltrValue.doubleValue();
		}
	}

	/**
	 * Less-Than-Equal Matching
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private static class LessThanEqualFilter implements OperatorImplementation {
		@Override
		public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
			Number fltrValue = (Number) filter.getValue();
			Number candValue = (Number) candidate.getValue();
			return candValue.doubleValue() <= fltrValue.doubleValue();
		}
	}

	/**
	 * Greater-Than Matching
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private static class GreaterThanFilter implements OperatorImplementation {
		@Override
		public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
			Number fltrValue = (Number) filter.getValue();
			Number candValue = (Number) candidate.getValue();
			return candValue.doubleValue() > fltrValue.doubleValue();
		}
	}

	/**
	 * Greater-Than-Equal Matching
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private static class GreaterThanEqualFilter implements
			OperatorImplementation {
		@Override
		public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
			Number fltrValue = (Number) filter.getValue();
			Number candValue = (Number) candidate.getValue();
			return candValue.doubleValue() >= fltrValue.doubleValue();
		}
	}

	/**
	 * Equal Matching
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private static class EqualFilter implements OperatorImplementation {
		@Override
		public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
			return filter.getValue().equals(candidate.getValue());
		}
	}

	/**
	 * Not-Equal Matching
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private static class NotEqualFilter implements OperatorImplementation {
		@Override
		public boolean matches(Attribute<?> filter, Attribute<?> candidate) {
			return !filter.getValue().equals(candidate.getValue());
		}
	}

}
