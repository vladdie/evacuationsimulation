package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.Rate;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.handover.HandoverSensor;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.handover.HandoverSensor.HandoverListener;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.DirectorAssistedUploadStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring.MonitoredAttribute.AttributeListener;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring.MonitoredAttribute.UpdateImmediacy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * TODO
 * <p>
 * TODO probably it would be best, if there is a periodic SEND operation, that sends all
 * @author Julian Wulfheide
 */
public class MonitoringSource implements AttributeListener {

    private final PubSubComponent pubSub;
    // TODO get rid of this dependency
    private final DirectorAssistedUploadStrategy uploadStrategy;

    private Topic ATTR_TOPIC_MONITORING;

    private Attribute<Long> ATTR_SRC_ID;
    private Attribute<List> ATTR_NEIGHBORS;
    private Attribute<Boolean> ATTR_AP_CONNECTED;
    private Attribute<Long> ATTR_REMAINING_UPLOAD;

    private List<MonitoredAttribute<?>> attributes = new ArrayList<>();

    private Map<String, Attribute> changedAttributes = new HashMap<>();

    /**
     * Flag that indicates that attribute updates should not be sent out. This is used to gather some attributes in the
     * beginning and not sending out one by one.
     */
    private boolean gatherAttributes = true;

    public MonitoringSource(PubSubComponent component, DirectorAssistedUploadStrategy uploadStrategy) {
        this.pubSub = component;
        this.uploadStrategy = uploadStrategy;

        ATTR_TOPIC_MONITORING = pubSub.createTopic(MonitoringAttributes.TOPIC_ROOT);

        // Sent to sink
        ATTR_SRC_ID = pubSub.createAttribute(Long.class, MonitoringAttributes.SRC_ID, getPubSub().getHost().getId().value());
        ATTR_NEIGHBORS = pubSub.createAttribute(List.class, MonitoringAttributes.NEIGHBORS, new ArrayList()); // TODO Do I get Generics in here?
        ATTR_AP_CONNECTED = pubSub.createAttribute(Boolean.class, MonitoringAttributes.AP_CONNECTED, false);
        ATTR_REMAINING_UPLOAD = pubSub.createAttribute(Long.class, MonitoringAttributes.REMAINING_UPLOAD, 0L);
    }

    public void startMonitoring() {

        /**
         * TODO explain attribute stuff
         */

        /**
         * List of neighbors
         */
        attributes.add(new PollingAttribute<List>(getPubSub(), ATTR_NEIGHBORS, this) {
            @Override
            public List<Long> getValue() {
                return uploadStrategy.getNeighborIDs();
            }

            @Override
            public UpdateImmediacy getUpdateImmediacy(List oldValue, List newValue) {
                return UpdateImmediacy.IMMEDIATELY;
            }
        });

        /**
         * AP state
         */
        attributes.add(new ListenerAttribute<Boolean>(ATTR_AP_CONNECTED, this) {
            private HandoverListener listenerProxy;
            private HandoverSensor handoverSensor;

            @Override
            protected void setup() {
                try {
                    handoverSensor = getPubSub().getHost().getComponent(HandoverSensor.class);
                    value = handoverSensor.isConnectedToAccessPoint(); // Trigger first update from hand

                    /**
                     * Java 8 Lambda instead of anonymous class of
                     * {@link de.tudarmstadt.maki.simonstrator.api.component.sensor.handover.HandoverSensor.HandoverListener}
                     */
                    this.listenerProxy = (boolean isConnectedToAP) -> {
                        update(isConnectedToAP);
                    };
                    handoverSensor.addHandoverListener(this.listenerProxy);
                } catch (ComponentNotAvailableException e) {
                    value = false; // not AP connected when no handover sensor was found
                }
            }

            @Override
            public void stopMonitoring() {
                if (handoverSensor != null) {
                    handoverSensor.removeHandoverListener(listenerProxy);
                }
            }

            @Override
            public UpdateImmediacy getUpdateImmediacy(Boolean oldValue, Boolean newValue) {
                return UpdateImmediacy.IMMEDIATELY;
            }
        });

        /**
         * Location
         */
//        attributes.add(new PollingAttribute<Location>(getPubSub(), ATTR_LOCATION, this) {
//            SiSComponent sis = null;
//
//            @Override
//            public Location getValue() {
//                try {
//                    // Is it bad to fetch this every time?
//                    if (sis == null) {
//                        sis = getPubSub().getHost().getComponent(SiSComponent.class);
//                    }
//                    return sis.get().localState(SiSTypes.PHY_LOCATION, SiSRequest.NONE);
//                } catch (ComponentNotAvailableException e) {
//                } catch (InformationNotAvailableException e) {
//                    throw new AssertionError("Fetching Location should work.");
//                }
//                return null;
//            }
//
//            @Override
//            UpdateImmediacy getUpdateImmediacy(Location oldValue, Location newValue) {
//                // TOD check if distance between old and new > X
//                return UpdateImmediacy.IMMEDIATELY;
//            }
//        });

        /**
         * Bandwidth (remaining up)
         */
        attributes.add(new PollingAttribute<Long>(getPubSub(), ATTR_REMAINING_UPLOAD, this) {
            private NetInterface activeInterface;

            @Override
            protected void setup() {
                activeInterface = getPubSub().getHost().getNetworkComponent().getByName(NetworkComponent.NetInterfaceName.MOBILE);
                assert activeInterface != null;
            }

            @Override
            public Long getValue() {
                return activeInterface.getMaxBandwidth().getUpBW() - activeInterface.getCurrentBandwidth().getUpBW();
            }

            @Override
            public UpdateImmediacy getUpdateImmediacy(Long oldValue, Long newValue) {
                if (oldValue == null)
                    oldValue = 0L;
                long difference = Math.abs(oldValue - newValue);
                if (difference < Rate.kbit_s) {
                    return UpdateImmediacy.NEVER;
                } else {
                    return UpdateImmediacy.IMMEDIATELY;
                }
            }
        });

        // Mark all attributes as changed for initial update
        for (MonitoredAttribute mAttribute : attributes) {
            if (mAttribute.value != null) {
                Attribute attribute = mAttribute.attribute;
                changedAttributes.put(attribute.getName(), attribute.create(mAttribute.value));
            }
        }

        // Send initial update
        Event.scheduleWithDelay(2 * Time.SECOND, (content, type) -> {
            gatherAttributes = false;
            sendUpdate();
        }, null, 0);

    }

    @Override
    public <T> boolean onAttributeUpdate(MonitoredAttribute<T> mAttribute, T oldValue, T newValue) {
        Attribute attribute = mAttribute.attribute;
        UpdateImmediacy updateImmideacy = mAttribute.getUpdateImmediacy(oldValue, newValue);
        if (updateImmideacy == UpdateImmediacy.NEVER) {
            return false;
        }

        changedAttributes.put(attribute.getName(), attribute.create(newValue));
        if (updateImmideacy == UpdateImmediacy.IMMEDIATELY) {
            sendUpdate();
        }
        return true;
    }

    public void stopMonitoring() {
        for (MonitoredAttribute attribute : attributes) {
            attribute.stopMonitoring();
        }
    }

    private void sendUpdate() {
        if (gatherAttributes)
            // Don't send an update in gather phase
            return;

        List<Attribute<?>> attributes = new LinkedList<>();
        attributes.add(ATTR_SRC_ID);

        // Include all attributes that have changed since last update
        for (Attribute attribute : changedAttributes.values()) {
            attributes.add(attribute);
        }

        Notification n = getPubSub().createNotification(ATTR_TOPIC_MONITORING, attributes, null);
        System.out.println(Time.getFormattedTime() + " MSource (" + getPubSub().getHost().getId() + ") sending report: " + n);
        // TODO make sure this is sent over WiFi if node has AP connection
        getPubSub().publish(n);

        changedAttributes.clear();
    }

    public PubSubComponent getPubSub() {
        return pubSub;
    }

    public void addMonitoredAttribute(MonitoredAttribute attribute) {
        attribute.setAttributeListener(this);
        attributes.add(attribute);
    }

    public void removeMonitoredAttribute(MonitoredAttribute attribute) {
        attribute.removeAttributeListener(this);
        attributes.remove(attribute);
    }
}
