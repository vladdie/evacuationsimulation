package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;

/**
 * TODO
 * @author Julian Wulfheide
 */
public abstract class MonitoredAttribute<T> {
    protected Attribute<T> attribute;
    protected T value = null;
    protected AttributeListener listener;

    /**
     * Indicates how severe an update (i.e. the difference between an old value of an attribute and the new value of the
     * same attribute are. This is necessary because updates of different attributes have a varying severity. For
     * example, if the attribute "current upload bandwidth" changes from 450kbps to 451kbps, an update is not needed
     * right away and can either be delayed and sent out when the next crucial update comes up (NEXT_UPDATE) or omitted
     * completely (NEVER).
     */
    public enum UpdateImmediacy {
        /**
         * Discard the update (never send it out).
         */
        NEVER,
        /**
         * Keep the update but don't send it out until an update is sent anyways.
         * TODO rename
         */
        NEXT_UPDATE,
        /**
         * Send out an update immediately.
         */
        IMMEDIATELY
    }

    public MonitoredAttribute(Attribute<T> attribute, AttributeListener listener) {
        this.attribute = attribute;
        this.listener = listener;
        setup();
    }

    public void setAttributeListener(AttributeListener listener) {
        this.listener = listener;
    }

    public void removeAttributeListener(AttributeListener listener) {
        if(this.listener != null && this.listener.equals(listener)) {
            this.listener = null;
        }
    }

    public void update(T newValue) {
        if (listener != null) {
            listener.onAttributeUpdate(this, value, newValue);
            value = newValue;
        }
    }

    protected abstract void setup();

    public abstract T getValue();

    public abstract UpdateImmediacy getUpdateImmediacy(T oldValue, T newValue);

    public abstract void stopMonitoring();

    public interface AttributeListener {
        /**
         * TODO
         * @param attribute
         * @param oldValue
         * @param newValue
         * @param <T>
         * @return true if the update is "accepted", meaning the current value is now considered to be the "old value"
         */
        <T> boolean onAttributeUpdate(MonitoredAttribute<T> attribute, T oldValue, T newValue);
    }
}
