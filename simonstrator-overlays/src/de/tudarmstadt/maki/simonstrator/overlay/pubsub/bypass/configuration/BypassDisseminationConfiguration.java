package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.gossip.GossipDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.AbstractLocalTransport.WiFiAdHocTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.AbstractUploadStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.UploadStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.UploadStrategy.UploadStrategyType;

/**
 * Configures Bypass to use a {@link CloudControlStrategy} and its settings.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class BypassDisseminationConfiguration implements BypassConfiguration {

	private LocalDistributionType defaultDistribution = null;

	private UploadStrategyType defaultUpload = null;

	public static final int directDisseminationPort = 7701;

	public static boolean enableGeofencing = false;

	public static boolean onlyLocalPublications = false;

	public static boolean enableCloudForLocal = true;

	public static boolean enableSelfHealing = false;

	private double gossipAlpha = -1;

	@Override
	public void configureClient(BypassClientComponent comp) {
		// Set default
		boolean useLocalTransport = false;
		if (defaultDistribution != null) {
			AbstractLocalDissemination instance = AbstractLocalDissemination.getInstanceFor(defaultDistribution, comp,
					directDisseminationPort);
			if (gossipAlpha != -1 && instance instanceof GossipDistribution) {
				((GossipDistribution) instance).setAlpha(gossipAlpha);
			}
			comp.setLocalDistribution(instance);
			useLocalTransport = useLocalTransport || (defaultDistribution != LocalDistributionType.NONE);
			if (BypassDisseminationConfiguration.enableSelfHealing) {
				instance.setProxyName(BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION);
				instance.setSelfHeal(true);
			}
		}
		if (defaultUpload != null) {
			UploadStrategy instance = AbstractUploadStrategy.getInstanceFor(defaultUpload, comp);
			comp.setUploadStrategy(instance);
			useLocalTransport = useLocalTransport || (defaultUpload != UploadStrategyType.DIRECT);
		}
		if (useLocalTransport) {
			comp.setLocalTransport(new WiFiAdHocTransport(comp));
		}
	}

	@Override
	public void configureCloud(BypassCloudComponent comp) {
		// nothing to do here.
	}


	/*
	 * Configuration
	 */

	/**
	 * Set the default local distribution to be used.
	 * 
	 * @param staticDistribution
	 */
	public void setDefaultDistribution(String defaultDistribution) {
		this.defaultDistribution = LocalDistributionType.valueOf(defaultDistribution);
		assert this.defaultDistribution != null;
	}

	/**
	 * Set the default upload scheme to be used
	 * 
	 * @param staticDistribution
	 */
	public void setDefaultUpload(String defaultUpload) {
		this.defaultUpload = UploadStrategyType.valueOf(defaultUpload);
		assert this.defaultUpload != null;
	}

	/**
	 * If true, local publications are subject to geofencing (they are not
	 * forwarded by clients who are not interest in the contents)
	 * 
	 * @param enableGeofencing
	 */
	public void setEnableGeofencing(boolean enableGeofencing) {
		BypassDisseminationConfiguration.enableGeofencing = enableGeofencing;
	}

	/**
	 * If true, local publications are disseminated via the dissemination
	 * protocol, whereas all other publications are simply sent to the cloud.
	 * 
	 * In the current implementation, local publications are NOT sent to the
	 * cloud anymore.
	 * 
	 * @param onlyLocalPublications
	 */
	public void setOnlyLocalPublications(boolean onlyLocalPublications) {
		BypassDisseminationConfiguration.onlyLocalPublications = onlyLocalPublications;
	}

	/**
	 * Default: true. All publications are sent to the cloud. If this is set to
	 * false, location-based publications that are issued to the location of the
	 * client are NOT sent to the cloud but only distributed via ad hoc
	 * dissemination.
	 * 
	 * @param enableCloudForLocal
	 */
	public void setEnableCloudForLocal(boolean enableCloudForLocal) {
		BypassDisseminationConfiguration.enableCloudForLocal = enableCloudForLocal;
	}

	/**
	 * If set to true, self healing for local ad hoc dissemination based on
	 * incoming messages is enabled.
	 * 
	 * @param enableSelfHealing
	 */
	public void setEnableSelfHealing(boolean enableSelfHealing) {
		BypassDisseminationConfiguration.enableSelfHealing = enableSelfHealing;
	}

	public void setGossipAlpha(double gossipAlpha) {
		this.gossipAlpha = gossipAlpha;
	}

}
