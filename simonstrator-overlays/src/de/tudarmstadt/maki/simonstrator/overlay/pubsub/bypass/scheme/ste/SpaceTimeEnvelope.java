package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste;

import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.Polygon;
import org.dyn4j.geometry.Vector2;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

/**
 * The Space-Time Envelope as described by A. Brimicombe and Y. Li in "Mobile
 * Space-Time Envelopes for Location-Based Services"
 * 
 * Updated by BR to reduce the number of Vector2 instances used on every update.
 * 
 * @author Julian Zobel
 */
public class SpaceTimeEnvelope {

	private final Vector2 center = new Vector2();
	private double radius;
	private final Vector2 vector = new Vector2();
	private Circle centerCircle = null;
	private Polygon envelopeCone;
	private Circle envelopeCircle;

	// The delta to the previous location
	private Vector2 translationDelta = new Vector2();

	/**
	 * Build a Space Time Envelope
	 */
	public SpaceTimeEnvelope(double radiusOfInterest) {
		this.radius = radiusOfInterest;
	}

	/**
	 * Re-use STE object for updates
	 * 
	 * @param centerLocation
	 * @param radiusOfInterest
	 * @param movementVector
	 * @param alpha
	 */
	public void updateEnvelope(Location centerLocation, Vector2 movementVector, double alpha) {
		translationDelta.set(centerLocation.getLongitude() - center.x, centerLocation.getLatitude() - center.y);
		center.set(centerLocation.getLongitude(), centerLocation.getLatitude());
		vector.set(movementVector);

		if (centerCircle == null) {
			// Prevent unnecessary object creation
			centerCircle = new Circle(radius);
			centerCircle.translate(center);
		} else {
			centerCircle.translate(translationDelta);
		}

		// make envelope as long as the node could travel within a minute with
		// the given velocity
		// --> length of vector is velocity in m/s, multiply by 60 seconds to
		// receive distance
		// add alpha multiplier to vector (later adaption of envelope size)
		vector.multiply(60 * alpha);

		// build extension shape only if threshold velocity is exceeded
		if (vector.getMagnitude() > radius) {
			buildExtensionShape();
		}
	}

	/*
	 * Reusable objects for the extension shape
	 */
	private Vector2 outerCenterEnvelopePoint = new Vector2();

	private Vector2 vleft = new Vector2();
	private Vector2 vright = new Vector2();
	private Vector2 centerLeft = new Vector2();
	private Vector2 centerRight = new Vector2();
	private Vector2 l1 = new Vector2();
	private Vector2 l2 = new Vector2();
	private Vector2 outerLeftEnvelopePoint = new Vector2();
	private Vector2 outerRightEnvelopePoint = new Vector2();

	/**
	 * Build the extension shape of the envelope, i.e. the extended subscription
	 * space. The extension is a conical shape along the given direction vector.
	 * The elongation is defined by the length of the given vector (i.e. speed)
	 */
	public void buildExtensionShape() {
		// outer point in movement direction
		outerCenterEnvelopePoint.set(center);
		outerCenterEnvelopePoint.add(vector);

		// outer circle
		if (envelopeCircle == null || envelopeCircle.getRadius() != vector.getMagnitude()) {
			envelopeCircle = new Circle(vector.getMagnitude());
			envelopeCircle.translate(center);
		} else {
			envelopeCircle.translate(translationDelta);
		}

		// vectors orthogonal to the direction vector
		// also: prevent instance creation at all cost.
		vleft.set(-vector.y, vector.x);
		vright.set(vector.y, -vector.x);
		vleft.normalize();
		vleft.multiply(radius);
		vright.normalize();
		vright.multiply(radius);

		// points in inner circle on orthogonal vector to direction vector
		centerLeft.set(center).add(vleft);
		centerRight.set(center).add(vright);

		// compute outer points for the cone
		l1.set(vector).rotate(Math.toRadians(15));
		l2.set(vector).rotate(Math.toRadians(-15));

		outerLeftEnvelopePoint.set(centerLeft).add(l1);
		outerRightEnvelopePoint.set(centerRight).add(l2);


		// build the envelope
		Vector2[] envelopeVertices = { centerRight, outerRightEnvelopePoint,
				outerCenterEnvelopePoint, outerLeftEnvelopePoint, centerLeft };
		envelopeCone = new Polygon(envelopeVertices);
	}

	public Polygon getEnvelopeCone() {
		return envelopeCone;
	}

	public Circle getEnvelopeCircle() {
		return envelopeCircle;
	}

	public Circle getCenterCircle() {
		return centerCircle;
	}


	/**
	 * Check for the complete envelope (center circle + extension) for a point
	 * to be inside
	 * 
	 * @param point
	 *            A given point that is checked for its location inside the STE
	 * @return True if the point is inside the area, false otherwise.
	 */
	public boolean containsPoint(Vector2 p) {
		return centerContainsPoint(p) || envelopeContainsPoint(p);
	}
	
	public boolean centerContainsPoint(Vector2 p) {
		return centerCircle.contains(p);
	}
	
	public boolean envelopeContainsPoint(Vector2 p) {
		if (envelopeCircle == null || envelopeCone == null) {
			return false;
		}

		return envelopeCircle.contains(p) && envelopeCone.contains(p);
	}
}
