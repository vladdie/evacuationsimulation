package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport;

import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;

/**
 * Interface used for the Transition-Enabled LocalTransportComponent. This
 * component masks the utilized TransportInstance and allows the usage of e.g.,
 * Bluetooth or WiFi
 * 
 * @author Bjoern Richerzhagen
 * @version Sep 19, 2014
 */
public interface LocalTransport extends TransitionEnabled {

	/**
	 * Sets the {@link TransMessageListener} for the given port
	 * 
	 * @param listener
	 * @param port
	 */
	public void setTransMessageListener(TransMessageListener listener, int port);

	/**
	 * Removes the TransMessageListener listening on the given port.
	 * 
	 * @param port
	 */
	public void removeTransMessageListener(int port);

	/**
	 * Used by the LocalDistribution to send overlay messages to the given
	 * target port.
	 * 
	 * @param msg
	 */
	public void send(OverlayMessage msg, int senderPort, int receiverPort);

	/**
	 * Name of the Net-Interface
	 * 
	 * @return
	 */
	public NetInterfaceName getUtilizedNetInterfaceName();

}
