package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.NoGateways;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * The Bypass-internal mirror of the {@link NoGateways} strategy provided by the
 * SiS. This one leads to better performance in large simulation runs, as a
 * number of proxy calls can be prevented.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class NoGatewayLogic implements BypassGatewayLogic {

	private BypassPubSubComponent comp;

	public NoGatewayLogic(BypassPubSubComponent comp) {
		this.comp = comp;
	}

	@Override
	public BypassPubSubComponent getComponent() {
		return comp;
	}

	@Override
	public Collection<CloudNotifyMessage> packMessages(Notification n,
			OverlayContacts subscribers, int numberOfGateways) {
		List<CloudNotifyMessage> messages = new LinkedList<>();
		for (OverlayContact subscriber : subscribers) {
			// only one subscriber
			messages.add(new CloudNotifyMessage(comp.getLocalOverlayContact(),
					subscriber, n));
		}
		return messages;
	}

}
