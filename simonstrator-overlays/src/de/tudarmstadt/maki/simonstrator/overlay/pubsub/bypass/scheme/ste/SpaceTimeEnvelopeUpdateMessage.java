package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste;

import org.dyn4j.geometry.Vector2;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.BypassCloudMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationUpdateMsg;

/**
 * A message carrying new information about a node's position, direction, and
 * velocity.
 * 
 * @author Julian Zobel
 *
 */
public class SpaceTimeEnvelopeUpdateMessage extends LocationUpdateMsg
		implements BypassCloudMessage, BypassSchemeMessage {

	private static final long serialVersionUID = 1L;

	private Vector2 movementVector;

	public SpaceTimeEnvelopeUpdateMessage(OverlayContact sender, OverlayContact receiver, Location location,
			Vector2 movementVector, SchemeName schemeName) {
		super(sender, receiver, location, schemeName);
		this.movementVector = movementVector;
	}

	@Override
	public long getSize() {
		// two times location due to movementVector
		return super.getSize() + getLocation().getTransmissionSize();
	}

	public Vector2 getMovementVector() {
		return movementVector;
	}

	@Override
	public String toString() {
		return getClass().getSimpleName() + " from " + getSender().toString() + ": " + getLocation().toString()
				+ " vec: " + getMovementVector().toString();
	}

}
