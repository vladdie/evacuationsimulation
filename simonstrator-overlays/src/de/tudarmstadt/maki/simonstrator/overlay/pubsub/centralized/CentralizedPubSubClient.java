package de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized;

import java.util.LinkedHashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.AbstractPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized.msg.NotificationMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized.msg.SubscriptionMessage;

/**
 * Forwards all requests to the centralized pub/sub broker.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class CentralizedPubSubClient extends AbstractPubSubComponent implements TransMessageListener {

	protected final int port;

	private UDP udp = null;

	private OverlayContact localContact = null;

	private final Map<Subscription, PubSubListener> listeners = new LinkedHashMap<Subscription, PubSubListener>();

	/*
	 * TODO: where and how to set the server? Might be best to provide a
	 * Bootstrap-Component that enables transparent retrieval of bootstrap
	 * information for each component. The relevant information then needs to be
	 * fed into the bootstrap-component directly by the respective runtime
	 */
	private TransInfo serverTransInfo = null;

	private String serverAddressString;

	public CentralizedPubSubClient(Host host, int port) {
		super(host);
		this.port = port;
	}

	@Override
	public void initialize() {
		super.initialize();
		NetInterface net = getHost().getNetworkComponent().getByName(CentralizedPubSubFactory.netInterface);
		NetID localNet = net.getLocalInetAddress();
		try {
			udp = getAndBindUDP(localNet, port, new CentralizedPubSubSerializer());
			udp.setTransportMessageListener(this);
			localContact = new BasicOverlayContact(getHost().getId(), CentralizedPubSubFactory.netInterface,
					localNet, port);
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError("Protocol Binding failed!");
		}
	}

	@Override
	public OverlayContact getLocalOverlayContact() {
		return localContact;
	}

	public TransInfo getServerTransInfo() {
		if (serverTransInfo == null) {
			if (serverAddressString == null) {
				// Cheating on the simulator
				OverlayContact server = CentralizedPubSubFactory.server.getLocalOverlayContact();
				serverTransInfo = udp.getTransInfo(server.getNetID(CentralizedPubSubFactory.netInterface),
						server.getPort(CentralizedPubSubFactory.netInterface));
			} else {
				serverTransInfo = udp.getTransInfo(udp.getNetInterface().getByName(serverAddressString));
			}
		}
		return serverTransInfo;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddressString = serverAddress;
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		setPeerStatus(PeerStatus.PRESENT);
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		setPeerStatus(PeerStatus.ABSENT);
	}

	@Override
	public void publish(Notification notification) {
		Message pubMsg = new NotificationMessage(notification);
		udp.send(pubMsg, getServerTransInfo().getNetId(), getServerTransInfo().getPort());
	}

	@Override
	public void advertise(Filter filter) {
		throw new UnsupportedOperationException("Advertisements are not supported by the centralized pub/sub.");
	}

	@Override
	public void unadvertise(Filter filter) {
		throw new UnsupportedOperationException("Advertisements are not supported by the centralized pub/sub.");
	}

	@Override
	public void subscribe(Subscription sub, PubSubListener listener) {
		Message subMsg = new SubscriptionMessage(sub, localContact);
		listeners.put(sub, listener);
		udp.send(subMsg, getServerTransInfo().getNetId(), getServerTransInfo().getPort());
	}

	@Override
	public void unsubscribe(Subscription sub, PubSubListener listener) {
		listeners.remove(sub);
		Message subMsg = new SubscriptionMessage(sub, localContact, true);
		udp.send(subMsg, getServerTransInfo().getNetId(), getServerTransInfo().getPort());
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		System.out.println("A Message Arrived from " + sender.toString());
		if (msg instanceof NotificationMessage) {
			Notification notification = ((NotificationMessage) msg).getNotification();
			for (Subscription sub : listeners.keySet()) {
				if (matches(notification, sub)) {
					listeners.get(sub).onNotificationArrived(sub, notification);
				}
			}
		}

	}

}
