package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.NodeType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassConfiguration;

/**
 * To ease configuration and sanity checks, we maintain one common factory for
 * the Bypass node types. The components are then configured via
 * {@link BypassConfiguration} instances, that help in maintaining valid
 * configurations. Refer to the package bypass.configuration for a list of
 * configuration available in Bypass.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class BypassFactory implements HostComponentFactory {

	/**
	 * IP of the cloud server for real deployments
	 */
	private String cloudIp = null;

	/**
	 * Port of the cloud server for real deployments
	 */
	private int cloudPort = -1;

	/**
	 * Node type to be created
	 */
	private NodeType nodeType;

	/**
	 * configuration-class that configures the nodes
	 */
	private List<BypassConfiguration> configurations = new LinkedList<BypassConfiguration>();

	/**
	 * To ensure that there is only one cloud
	 */
	private boolean createdCloud = false;

	private boolean locked = false;

	public static long REMOTE_SUBSCRIPTION_EXPIRES_AFTER = -1;

	public BypassFactory() {
		//
	}

	@Override
	public BypassPubSubComponent createComponent(Host host) {
		if (nodeType == null) {
			throw new AssertionError("You need to specify a node type!");
		}
		if (configurations.isEmpty()) {
			throw new AssertionError(
					"You need to specify at least one configuration!");
		}
		if (!locked) {
			locked = true;
		}

		BypassPubSubComponent comp;

		switch (nodeType) {
		case CLIENT: {
			if (cloudIp != null && cloudPort != -1) {
				/*
				 * Real world deployment. We use different local ports to listen
				 * to incoming cloud messages to support multiple clients on one
				 * physical device.
				 */
				comp = new BypassClientComponent(host, cloudIp, cloudPort,
						getCloudListenPort());
			} else {
				comp = new BypassClientComponent(host);
			}
			/*
			 * This call configures the component by adding modules or setting
			 * parameters.
			 */
			for (BypassConfiguration configuration : configurations) {
				configuration.configureClient((BypassClientComponent) comp);
			}
			break;
		}

		case CLOUD: {
			if (createdCloud) {
				throw new AssertionError(
						"Only one cloud instance at a time is supported!");
			}
			comp = new BypassCloudComponent(host);
			/*
			 * This call configures the component by adding modules or setting
			 * parameters.
			 */
			for (BypassConfiguration configuration : configurations) {
				configuration.configureCloud((BypassCloudComponent) comp);
			}
			createdCloud = true;
			break;
		}

		default:
			throw new AssertionError("Unknown type.");
		}


		return comp;
	}

	/**
	 * Port used to listen to incoming messages from the cloud. Random in the
	 * range of 6450 till 6999
	 * 
	 * @return
	 */
	private int getCloudListenPort() {
		return 6450 + Randoms.getRandom(BypassFactory.class).nextInt(550);
	}

	/**
	 * For the XML-Configuration
	 * 
	 * @param nodeType
	 */
	public void setNodeType(String nodeType) {
		assert nodeType != null;
		this.setNodeTypeDirect(NodeType.valueOf(nodeType.toUpperCase()));
	}

	/**
	 * For direct code interaction
	 * 
	 * @param nodeType
	 */
	public void setNodeTypeDirect(NodeType nodeType) {
		assert nodeType != null;
		this.nodeType = nodeType;
	}

	/**
	 * For real-world deployments
	 * 
	 * @param cloudIP
	 */
	public void setCloudIP(String cloudIP) {
		this.cloudIp = cloudIP;
	}

	/**
	 * For real-world deployments
	 * 
	 * @param cloudPort
	 */
	public void setCloudPort(int cloudPort) {
		this.cloudPort = cloudPort;
	}

	/**
	 * Global configuration. Multiple configurations can be concatenated, they
	 * will be executed in order. Later configurations can overwrite previous
	 * ones. Configurations will be applied to ALL nodes.
	 * 
	 * @param configuration
	 */
	public void setConfiguration(BypassConfiguration configuration) {
		if (locked) {
			throw new AssertionError(
					"Began creating nodes. Adding configurations is no longer allowed.");
		}
		Monitor.log(BypassFactory.class, Level.INFO,
				"Adding a BypassConfiguration:\n%s", configuration);
		configurations.add(configuration);
	}

}
