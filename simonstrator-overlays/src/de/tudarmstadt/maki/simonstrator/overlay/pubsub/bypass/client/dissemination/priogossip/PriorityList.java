package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.priogossip;

import java.util.LinkedList;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.PubSubNotificationForwardingAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.priogossip.PriogossipMessage.messageType;

/**
 * A prioritized list, acting like a message buffer, for the priority gossip
 * dissemination.
 * 
 * @author Julian Zobel, Sanja Huhle, Martin Wende
 *
 */
public class PriorityList {

	private LinkedList<RatedMessage> buffer = new LinkedList<>();
	private boolean debugMode = false;
	private PriogossipDistribution dist;

	public PriorityList(PriogossipDistribution p) {
		this.dist = p;
	}

	/**
	 * Add a message to the priority list. Message may be deleted due to
	 * prioritization.
	 * 
	 * @param msg
	 *            The PriogossipMessage, that should be appended to the list.
	 * 
	 * @return Returns true, if the message was added to the list, false
	 *         otherwise.
	 */
	public boolean add(PriogossipMessage msg) {
		// check for max hops
		// if (msg.getTimeToLive() > 0) {
			insert(msg);
			return true;
		// }

		// return false;
	}

	/**
	 * Determine if a message can be sent or is discarded
	 * 
	 * @param n
	 *            A random double.
	 * @return true - if message will be sent or false - if message will be
	 *         discarded.
	 */
	private boolean sendOrDiscardNextMessage(Double n) {
		if (buffer.size() == 0)
			return false;

		// get the type of the first message
		messageType type = buffer.getFirst().getType();

		switch (type) {
		case NEEDRESOURCE:
			if (n < 0.8)
				return true;
			break;
		case OFFERRESOURCE:
			if (n < 0.8)
				return true;
			break;
		case MESSAGE:
			if (n < 0.70)
				return true;
			break;
		case INFO:
			if (n < 0.85)
				return true;
			break;
		case WARNING:
			if (n < 0.9)
				return true;
			break;
		case EMERGENCY: // use FLOODING in this case
			return true;
		default:
			if (n < 0.72)
				return true;
		}

		if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
			Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
					dist.getComponent().getHost(),
					buffer.getFirst().getMessage().getNotification(),
					buffer.getFirst().getMessage().getSender().getNodeID(), 0);
		}
		this.pop(); // discard message

		return false;
	}

	/**
	 * Insert a prioritized gossip message into the buffer queue.
	 * 
	 * @param msg
	 *            The PriogossipMessage, that is inserted into the buffer queue.
	 */
	private void insert(PriogossipMessage msg) {

		// acquire the rating for this message and store it in a rated message
		// object
		RatedMessage ratedMessage = new RatedMessage(msg, rate(msg));

		// insert the message into the queue by iterating over all elements and
		// insert it BEHIND the first element, that has a higher rating. Latter
		// elements are shifted one position back (shift to the right)
		for (int i = buffer.size(); i >= 0; i--) {
			if (i != 0) {
				if (ratedMessage.getRating() > buffer.get(i - 1).getRating()) {
					// message has a lower rating, so jump to next iteration
					continue;
				} else {
					// next rating is higher, so insert it behind this message
					buffer.add(i, ratedMessage);

					if (debugMode) {
						System.out.println("S= " + buffer.size() + ", "
								+ msg.getType() + " r = "
								+ ratedMessage.getRating() + " add at " + i);
					}
					break;
				}
			} else { // insert the message at the beginning
				buffer.add(0, ratedMessage);
				if (debugMode) {
					System.out.println("S= " + buffer.size() + ", "
							+ msg.getType() + " r = "
							+ ratedMessage.getRating() + " add at " + i);
				}
				break;
			}
		}
	}

	/**
	 * Rating function for a prioritized gossip message
	 * 
	 * @param msg
	 *            Those to prioritized message.
	 * 
	 * @return the rate of the prioritized message.
	 */
	private int rate(PriogossipMessage msg) {
		int rate = 0;

		messageType msgType = msg.getType();

		switch (msgType) {
		case WARNING:
			rate = rate + 80;
			break;
		case INFO:
			rate = rate + 70;
			break;
		case EMERGENCY:
			rate = rate + 40;
			break;
		case ALIVE:
			rate = rate + 10;
			break;
		case SEARCHPERSON:
			rate = rate - 10;
			break;
		case NEEDRESOURCE:
			rate = rate - 20;
			break;
		case OFFERRESOURCE:
			rate = rate - 30;
			break;
		case MESSAGE:
			rate = rate - 40;
			break;
		default:
			rate = 0;
			break;
		}

		if (msg.hasLowBattery()) {
			rate = rate + 25;
		}

		return rate;
	}

	/**
	 * Returns and then removes the first element of the list.
	 * 
	 * @return The first message in the buffer to be sent.
	 */
	public PriogossipMessage pop() {
		return buffer.pollFirst().getMessage();
	}

	/**
	 * Returns the next message that is to be sent, or null if the message is
	 * discarded
	 * 
	 * @param n
	 *            a random double
	 * @return the next prioritized message or null, if nothing to send
	 */
	public PriogossipMessage getNextMessage(Double n) {

		if (sendOrDiscardNextMessage(n)) {
			// send the message
			return pop();
		} else {
			// message was discarded
			return null;
		}
	}

}
