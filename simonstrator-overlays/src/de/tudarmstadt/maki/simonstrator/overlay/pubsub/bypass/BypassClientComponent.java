package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.NodeInformation;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.LocationPubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubInfoComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.BypassMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.LocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.none.NoLocalDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway.LocalGatewayBehavior;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway.NoGatewayBehavior;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.AbstractLocalTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.AbstractLocalTransport.NoAdHocTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.LocalTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.LocalTransportTransition;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.BypassCloudMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyGatewayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudSubscribeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudUnsubscribeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassDisseminationConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassGatewayConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ClientSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.basic.BasicSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.DirectUploadStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.UploadStrategy;

/**
 * Component running on the client (mobile device). Contains a number of
 * transition-enabled proxies for some core components within Bypass.
 * 
 * @author Bjoern Richerzhagen
 * @version May 2, 2014
 */
public class BypassClientComponent extends BypassPubSubComponent implements
 NodeInformation, LocationPubSubComponent, PubSubInfoComponent {

	/**
	 * Transition-enabled {@link ClientSubscriptionScheme} used by Bypass
	 */
	private ClientSubscriptionScheme subscriptionScheme = new BasicSubscriptionScheme(this);

	/**
	 * Transition-enabled component for the direct local dissemination of events
	 * (by its creator).
	 */
	private LocalDissemination localDistribution = new NoLocalDistribution(this,
			BypassDisseminationConfiguration.directDisseminationPort);

	/**
	 * Transition-enabled component for local gateways
	 */
	private LocalGatewayBehavior localGatewayBehavior = new NoGatewayBehavior(this);

	/**
	 * Transition-enabled component for the distribution strategy used by the
	 * gateway
	 */
	private LocalDissemination localGatewayDistribution = new NoLocalDistribution(this,
			BypassGatewayConfiguration.port);

	/**
	 * Transition-enabled component for the local phy layer
	 */
	private LocalTransport localTransport = new NoAdHocTransport(this);
	
	/**
	 * Transition-enabled {@link UploadStrategy} for new events
	 */
	private UploadStrategy uploadStrategy = new DirectUploadStrategy(this);

	/**
	 * For the visualization
	 */
	protected long _actedAsGatewayTimestamp = 0;

	/**
	 * For last-minute analyzers of GW-functionality. Encapsulate in Analyzer
	 * lateron
	 */
	public double _actedAsGatewayCounter = 0;

	/**
	 * Acted as gateway but was no subscriber (just a relay)
	 */
	public double _actedAsGatewayCounterNoSubscriber = 0;

	public double _gatewaySumClients = 0;

	public BypassClientComponent(Host host) {
		super(host, NodeType.CLIENT);
	}

	/**
	 * For real-world deployments
	 * 
	 * @param host
	 * @param peerId
	 * @param cloudIP
	 * @param cloudPort
	 *            port the cloud listens on
	 * @param cloudPortListen
	 *            port we use to receive messages from the cloud
	 */
	public BypassClientComponent(Host host, String cloudIP,
			int cloudPort, int cloudPortListen) {
		super(host, NodeType.CLIENT, cloudIP, cloudPort,
				cloudPortListen);
		System.out.println("CreatedBypassComponent: ID: " + host.getId()
				+ " cloudPortListen: " + cloudPortListen);
	}

	@Override
	public void initialize() {
		super.initialize();
		TransitionEngine tEngine = getTransitionEngine();

		if (BypassSubscriptionSchemeConfiguration.disableStateTransfer) {
			/*
			 * If state transfer is disabled, all clients need to resubscribe.
			 * This is done within this TransitionListener
			 */
			subscriptionScheme = tEngine.createMechanismProxy(ClientSubscriptionScheme.class, subscriptionScheme,
					BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME);
			tEngine.addTransitionListener(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME, new TransitionListener() {

				private Map<Subscription, Set<PubSubListener>> localSubs = null;

				@Override
				public void startingTransition() {
					// save ref to local subs
					localSubs = getSubscriptionScheme().getLocalSubscriptions();
				}

				@Override
				public void executedTransition() {
					// Resubscribe with all local subscriptions
					assert localSubs != null;
					for (Entry<Subscription, Set<PubSubListener>> entry : localSubs.entrySet()) {
						for (PubSubListener listener : entry.getValue()) {
							subscribe(entry.getKey(), listener);
								}
							}
				}
			});
		} else {
			subscriptionScheme = tEngine.createMechanismProxy(ClientSubscriptionScheme.class, subscriptionScheme,
					BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME);
		}

		localTransport = tEngine.createMechanismProxy(LocalTransport.class, localTransport,
				BypassPubSubComponent.PROXY_LOCAL_PHY);
		tEngine.registerTransition(PROXY_LOCAL_PHY, new LocalTransportTransition());

		localDistribution = tEngine.createMechanismProxy(LocalDissemination.class, localDistribution,
				BypassPubSubComponent.PROXY_LOCAL_DISSEMINATION);

		localGatewayDistribution = tEngine.createMechanismProxy(LocalDissemination.class, localGatewayDistribution,
				BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION);

		localGatewayBehavior = tEngine.createMechanismProxy(LocalGatewayBehavior.class, localGatewayBehavior,
				BypassPubSubComponent.PROXY_LOCAL_GATEWAY);

		uploadStrategy = tEngine.createMechanismProxy(UploadStrategy.class, uploadStrategy,
				BypassPubSubComponent.PROXY_UPLOAD_SCHEME);
	}

	@Override
	public void startComponent() {

		/*
		 * FIXME implement start and stop handling for mechanisms. Simple
		 * solution: check for isActive within the mechanisms and when sending
		 * messages (e.g., scheme update messages).
		 */

		super.startComponent();
	}

	@Override
	public void stopComponent() {

		super.stopComponent();
	}

	@Override
	public Subscription createSubscription(Topic topic, Filter filter,
			LocationRequest locationRequest, double radiusOfInterest) {
		return getSubscriptionScheme().createSubscription(topic, filter,
				locationRequest, radiusOfInterest);
	}

	@Override
	public Notification createNotification(Topic topic, List<Attribute<?>> attributes, Location location,
			double radiusOfInterest, byte[] payload) {
		return getSubscriptionScheme().createNotification(topic, attributes, location, radiusOfInterest, payload);
	}

	@Override
	public Notification createLocalNotification(Topic topic, List<Attribute<?>> attributes, double radiusOfInterest,
			byte[] payload) {
		return getSubscriptionScheme().createLocalNotification(topic, attributes, radiusOfInterest, payload);
	}

	/**
	 * Returns the transition-enabled proxy of the
	 * {@link AbstractLocalTransport}. To be used by the
	 * {@link LocalDissemination}s, if configured.
	 * 
	 * @return
	 */
	public LocalTransport getLocalTransport() {
		return localTransport;
	}

	/**
	 * Returns the transition-enabled proxy of the {@link LocalGatewayBehavior}.
	 * 
	 * @return
	 */
	public LocalDissemination getLocalDistribution() {
		return localDistribution;
	}

	/**
	 * Returns the {@link ClientSubscriptionScheme}-proxy utilized by this
	 * client.
	 */
	public ClientSubscriptionScheme getSubscriptionScheme() {
		return subscriptionScheme;
	}

	/**
	 * Currently active local distribution
	 * 
	 * @return
	 */
	public LocalDistributionType getActiveLocalDistribution() {
		return localDistribution.getLocalDistributionType();
	}

	@Override
	public void publish(Notification notification) {
		/*
		 * Self-notifications are no longer triggered here. Instead, a
		 * localDistribution might trigger self-notifications (they currently
		 * all do this, with the exception of the NoLocalDissemination).
		 */
		boolean isLocationBasedLocalPublication = false;
		if (notification instanceof LocationBasedNotification) {
			isLocationBasedLocalPublication = ((LocationBasedNotification) notification).isPublisherLocation();
		}

		/*
		 * Notify the upload strategy to send the notification to the
		 * cloud/cloudlet
		 */
		if (!BypassDisseminationConfiguration.enableCloudForLocal) {
			// if local publication && local dissemination: do not send to cloud
			if (!isLocationBasedLocalPublication
					|| localDistribution.getLocalDistributionType() == LocalDistributionType.NONE) {
				uploadStrategy.uploadNotification(notification);
			}
		} else {
			// just upload every notification
			uploadStrategy.uploadNotification(notification);
		}

		/*
		 * Notify local dissemination strategy, this might also lead to
		 * self-notifications. If only local publications are to be disseminated
		 * via dissemination protocols, we check this here as well.
		 */
		if (BypassDisseminationConfiguration.onlyLocalPublications) {
			if (isLocationBasedLocalPublication) {
				localDistribution.notify(notification, null);
			}
		} else {
			localDistribution.notify(notification, null);
		}

	}

	@Override
	public void subscribe(Subscription sub, PubSubListener listener) {
		// add to own storage
		boolean firstListener = getSubscriptionScheme().onSubscribeAtClient(sub, listener);

		/*
		 * CLONE and send to cloud. Cloning is very important in this context,
		 * as the updates of the subscription filters would otherwise propagate
		 * to every node. Only, if we are not in adhoc mode.
		 */
		if (!isInAdhocMode() && firstListener) {
			CloudSubscribeMessage csub = new CloudSubscribeMessage(getLocalOverlayContact(), getBrokerContact(),
					sub.clone());
			sendViaMobileNetwork(csub);
		}

		/*
		 * If local forwarding: distribute subs locally? This is up to the
		 * respective local dissemination strategy
		 */
		if (firstListener) {
			localDistribution.subscribe(sub);
		}
	}

	@Override
	public void unsubscribe(Subscription sub, PubSubListener listener) {
		// remove from own storage
		boolean completeUnsubscribe = getSubscriptionScheme().onUnsubscribeAtClient(sub, listener);
		// send to cloud
		if (!isInAdhocMode() && completeUnsubscribe) {
			CloudUnsubscribeMessage unsub = new CloudUnsubscribeMessage(getLocalOverlayContact(), getBrokerContact(),
					sub);
			sendViaMobileNetwork(unsub);
		}
		/*
		 * TODO if local forwarding: distribute locally?
		 */
	}

	/**
	 * Handles an incoming cloud notification message
	 * 
	 * @param msg
	 * @param commID
	 */
	protected void handleCloudNotification(CloudNotifyMessage msg, int commID) {

		if (msg instanceof CloudNotifyGatewayMessage) {
			/*
			 * Gateway notification is a subclass of a CloudNotifyMessage
			 */
			CloudNotifyGatewayMessage gwMsg = (CloudNotifyGatewayMessage) msg;
			// Distribute to clients
			localGatewayBehavior.notify(gwMsg.getNotification(),
					gwMsg.getSubscribers());

			/*
			 * FIXME dirty visualization hack (removed for performance of batch
			 * simulations)
			 */
			_actedAsGatewayTimestamp = Time.getCurrentTime();
			_actedAsGatewayCounter++;
			_gatewaySumClients += gwMsg.getSubscribers().size();


			if (!gwMsg.isNotifyGateway()) {
				// do not inform the local app
				_actedAsGatewayCounterNoSubscriber++;
				return;
			}
		}

		// notify app listeners
		getSubscriptionScheme().notifySelf(msg.getNotification());

	}

	/**
	 * This is triggered by the local distribution strategy
	 * 
	 * @param n
	 * @return true, if the notification was delivered to the local app
	 */
	public boolean handleLocalNotification(Notification n) {
		return getSubscriptionScheme().notifySelf(n);
	}

	@Override
	public void handleIncomingMessage(BypassMessage msg, TransInfo sender, int commID) {
		assert msg instanceof BypassCloudMessage;
		
		if (msg instanceof CloudNotifyMessage) {
			handleCloudNotification((CloudNotifyMessage) msg, commID);
		}
	}

	/**
	 * Allows to overwrite the {@link LocalTransport} implementation used as a
	 * default when registering the mechanism at the {@link TransitionEngine}.
	 * Only to be called prior to initialization.
	 * 
	 * @param localTransport
	 */
	public void setLocalTransport(LocalTransport localTransport) {
		this.localTransport = localTransport;
	}

	/**
	 * Allows to overwrite the {@link LocalDissemination} implementation used as
	 * a default when registering the mechanism at the {@link TransitionEngine}.
	 * Only to be called prior to initialization.
	 * 
	 * @param localDistribution
	 */
	public void setLocalDistribution(LocalDissemination localDistribution) {
		this.localDistribution = localDistribution;
	}

	/**
	 * Allows to overwrite the {@link LocalGatewayBehavior} used for the proxy
	 * creation.
	 * 
	 * @param localGatewayBehavior
	 */
	public void setLocalGatewayBehavior(LocalGatewayBehavior localGatewayBehavior) {
		this.localGatewayBehavior = localGatewayBehavior;
	}

	/**
	 * Allows to overwrite the {@link LocalDissemination} that is used by a
	 * {@link LocalGatewayBehavior}.
	 * 
	 * @param localGatewayDistribution
	 */
	public void setLocalGatewayDistribution(LocalDissemination localGatewayDistribution) {
		this.localGatewayDistribution = localGatewayDistribution;
	}

	/**
	 * Allows to overwrite the {@link UploadStrategy} implementation used as a
	 * default when registering the mechanism at the {@link TransitionEngine}.
	 * Only to be called prior to initialization.
	 * 
	 * @param uploadStrategy
	 */
	public void setUploadStrategy(UploadStrategy uploadStrategy) {
		this.uploadStrategy = uploadStrategy;
	}

	/**
	 * Allows to overwrite the {@link ClientSubscriptionScheme} used by this
	 * client.
	 * 
	 * @param subscriptionScheme
	 */
	public void setSubscriptionScheme(ClientSubscriptionScheme subscriptionScheme) {
		this.subscriptionScheme = subscriptionScheme;
	}

	/*
	 * PubSubComponentInfo-Interface
	 */

	private LocationSensor _locationSensor = null;

	@Override
	public boolean isSubscribedTo(Notification notification) {
		// return subscriptionScheme.isSubscribedTo(notification);

		/*
		 * This is the "GlobalKnowledge" matcher used by analyzers during
		 * evaluation. Not scheme-specific in the current realization of the
		 * analyzing-framework.
		 * 
		 * ATTENTION: this is used during analyzing and for the visualization.
		 * It should <strong>match as intended by the application</strong>,
		 * meaning: if a scheme uses a grid, for example, but the application
		 * issued a circular RoI, we should only return true here if the
		 * notification falls within the circular RoI (basically, this is ground
		 * truth as expected by the application).
		 * 
		 * The method should not be used within the overlay itself! Instead, use
		 * getSubscribers().
		 * 
		 */

		Set<Subscription> localSubs = subscriptionScheme.getLocalSubscriptions().keySet();
		for (Subscription subscription : localSubs) {
			// first, match attributes
			if (matches(notification, subscription)) {
				// next, check for location-based stuff
				if (subscription instanceof LocationBasedSubscription) {
					if (notification instanceof LocationBasedNotification) {
						LocationBasedNotification lNot = (LocationBasedNotification) notification;
						LocationBasedSubscription lSub = (LocationBasedSubscription) subscription;
						if (_locationSensor == null) {
							try {
								_locationSensor = getHost().getComponent(LocationSensor.class);
							} catch (ComponentNotAvailableException e) {
								throw new AssertionError();
							}
						}
						if (_locationSensor.getLastLocation().distanceTo(
								lNot.getLocation()) <= lNot.getRadiusOfInterest() + lSub.getRadiusOfInterest()) {
							return true;
						} else {
							// no match. continue.
							continue;
						}
					} else {
						/*
						 * non-LBS notification is never matched by LBS
						 * subscription, as we consider the subscription to be
						 * "more specific" (e.g., as if it would have an
						 * additional attribute). Just continue.
						 */
						continue;
					}
				} else {
					// matched, default non-LBS behavior
					return true;
				}
			}
		}
		return false;
	}

	/*
	 * NodeInformation interface used by the visualization
	 */

	@Override
	public String getNodeDescription() {
		return "";
	}

	@Override
	public int getNodeColor(int dimension) {
		switch (dimension) {
		case 0: // local distribution
			return getActiveLocalDistribution().ordinal() - 1;

		case 1: // local Phy
			return getLocalTransport().getUtilizedNetInterfaceName() != null ? getLocalTransport()
					.getUtilizedNetInterfaceName().ordinal()
					: NetInterfaceName.MOBILE.ordinal();

		case 2: // Gateway
			return _actedAsGatewayTimestamp + Time.SECOND > Time.getCurrentTime() ? 0 : -1;

		case 3: // GW-Dissemination
			return localGatewayDistribution.getLocalDistributionType().ordinal() - 1;

		case 4: // Subscription Scheme
			return getSubscriptionScheme().getName().ordinal();

		default:
			return -1;
		}
	}

	@Override
	public int getNodeColorDimensions() {
		return dimDesc.length;
	}

	private static String[] dimDesc;

	private static String[][] colorDesc;
	
	static {
		dimDesc = new String[] { "Dir-Diss", "Phy", "GW", "GW-Diss", "SubS" };
		colorDesc = new String[dimDesc.length][];
		// local Dissemination Schemes + GW
		LocalDistributionType[] types = LocalDistributionType.values();
		colorDesc[0] = new String[types.length - 1];
		colorDesc[3] = new String[types.length - 1];
		for (int i = 0; i < types.length - 1; i++) {
			colorDesc[0][i] = types[i + 1].name();
			colorDesc[3][i] = types[i + 1].name();
		}
		// local Phy Schemes
		NetInterfaceName[] phys = NetInterfaceName.values();
		colorDesc[1] = new String[phys.length];
		for (int i = 0; i < phys.length; i++) {
			colorDesc[1][i] = phys[i].name();
		}
		// Gateway
		colorDesc[2] = new String[] { "Gateway" };

		// Subscription Scheme
		SchemeName[] schemes = SubscriptionScheme.SchemeName.values();
		colorDesc[4] = new String[schemes.length];
		for (int i = 0; i < schemes.length; i++) {
			colorDesc[4][i] = schemes[i].name();
		}
	}

	@Override
	public String[] getNodeColorDimensionDescriptions() {
		return dimDesc;
	}

	@Override
	public String[] getNodeColorDescriptions(int dimension) {
		return colorDesc[dimension];
	}

}
