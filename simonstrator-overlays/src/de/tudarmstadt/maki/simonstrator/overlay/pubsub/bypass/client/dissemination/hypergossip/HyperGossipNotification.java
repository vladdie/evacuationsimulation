package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;

public class HyperGossipNotification extends DefaultLocalMessage implements BypassLocalMessage {

	public HyperGossipNotification(OverlayContact sender) {
		super(sender, null);
	}

	private static final long serialVersionUID = 1L;

}
