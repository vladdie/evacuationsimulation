package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;

/**
 * Gateway-related metrics
 * 
 * @author Bjoern Richerzhagen
 *
 */
public abstract class MGateway extends AbstractMetric<MetricValue<Double>> {

	public MGateway(String name, String description,
			de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricUnit unit) {
		super(name, description, unit);
	}

	@Override
	public void initialize(List<Host> hosts) {
		for (Host host : hosts) {
			try {
				BypassClientComponent client = host.getComponent(BypassClientComponent.class);
				addHost(host, new GatewayMetricValue(client));
			} catch (ComponentNotAvailableException e) {
				//
			}
		}
	}

	protected abstract double getMValue(BypassClientComponent comp);

	private class GatewayMetricValue implements MetricValue<Double> {

		private final BypassClientComponent comp;

		public GatewayMetricValue(BypassClientComponent comp) {
			this.comp = comp;
		}

		@Override
		public Double getValue() {
			return getMValue(comp);
		}

		@Override
		public boolean isValid() {
			return comp.isPresent();
		}

	}

	/**
	 * Number of times a node acted as gateway.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class MGatewayUsageCount extends MGateway {

		public MGatewayUsageCount() {
			super("MGatewayUsageCount", "Number of times a node acted as a gateway", MetricUnit.NONE);
		}

		@Override
		protected double getMValue(BypassClientComponent comp) {
			return comp._actedAsGatewayCounter;
		}

	}

	/**
	 * Number of clients served by a gateway (counter)
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class MGatewayServedClientsCount extends MGateway {

		public MGatewayServedClientsCount() {
			super("MGatewayServedClientsCount", "Total number of clients served by this gateway", MetricUnit.NONE);
		}

		@Override
		protected double getMValue(BypassClientComponent comp) {
			return comp._gatewaySumClients;
		}

	}

	/**
	 * Number of times the node served as a gateway but not as a subscriber
	 * (relay only)
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class MGatewayRelayOnlyCount extends MGateway {

		public MGatewayRelayOnlyCount() {
			super("MGatewayRelayOnlyCount", "Number of times the node acted only as a relay (not a subscriber)",
					MetricUnit.NONE);
		}

		@Override
		protected double getMValue(BypassClientComponent comp) {
			return comp._actedAsGatewayCounterNoSubscriber;
		}

	}

}
