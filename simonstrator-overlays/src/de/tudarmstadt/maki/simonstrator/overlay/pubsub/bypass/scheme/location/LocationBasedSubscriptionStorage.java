package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.SubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Storage for location-based subscriptions using space-time-envelopes. This
 * extends {@link SubscriptionStorage} which takes care of handling default
 * subscriptions and notifications.
 * 
 * @author Julian Zobel, Bjoern Richerzhagen
 *
 */
public class LocationBasedSubscriptionStorage extends SubscriptionStorage {

	private LinkedHashMap<OverlayContact, Set<LocationBasedSubscription>> locationBasedSubsBySubscriber = new LinkedHashMap<>();

	private LinkedHashMap<INodeID, Location> lastKnownLocation = new LinkedHashMap<>();

	public LocationBasedSubscriptionStorage(BypassPubSubComponent comp) {
		super(comp);
	}

	/**
	 * For convenience during state transfers
	 * 
	 * @param storage
	 */
	@Override
	public void importSubscriptions(SubscriptionStorage storage) {
		// copy non-LBS subscriptions
		super.importSubscriptions(storage);

		if (storage instanceof LocationBasedSubscriptionStorage) {
			LocationBasedSubscriptionStorage lbsStorage = (LocationBasedSubscriptionStorage) storage;
			/*
			 * copy LBS subscriptions (NOT locations, this is done during state
			 * transfer.
			 */
			for (Entry<OverlayContact, Set<LocationBasedSubscription>> subscriber : lbsStorage.locationBasedSubsBySubscriber
					.entrySet()) {
				for (LocationBasedSubscription subscription : subscriber.getValue()) {
					addSubscription(subscriber.getKey(), subscription);
				}
			}
		}
	}

	/**
	 * Remove a subscription
	 * 
	 * @param subscriber
	 * @param subscription
	 * 
	 */
	@Override
	public boolean removeSubscription(OverlayContact subscriber, Subscription subscription) {
		if (subscription instanceof LocationBasedSubscription) {
			return removeLBSSubscription(subscriber, (LocationBasedSubscription) subscription);
		} else {
			return super.removeSubscription(subscriber, subscription);
		}
	}

	/**
	 * Add a subscription to the storage
	 * 
	 * @param contact
	 *            The overlay contact of that subscription
	 * @param xsub
	 *            The subscription to be added
	 */
	@Override
	public void addSubscription(OverlayContact subscriber, Subscription subscription) {
		if (subscription instanceof LocationBasedSubscription) {
			if (!locationBasedSubsBySubscriber.containsKey(subscriber)) {
				locationBasedSubsBySubscriber.put(subscriber, new LinkedHashSet<>());
			}
			locationBasedSubsBySubscriber.get(subscriber).add((LocationBasedSubscription) subscription);
		} else {
			super.addSubscription(subscriber, subscription);
		}
	}

	/**
	 * Returns the set of subscriptions that were issued by an overlay contact
	 * 
	 * @param contact
	 *            The overlay contact
	 * @return Set of subscriptions from this overlay contact
	 */
	@Override
	public Set<Subscription> getSubscriptionsFromContact(OverlayContact contact) {
		Set<Subscription> subs = super.getSubscriptionsFromContact(contact);
		if (locationBasedSubsBySubscriber.containsKey(contact)) {
			subs.addAll(locationBasedSubsBySubscriber.get(contact));
		}
		return subs;
	}

	/**
	 * Returns the set of subscribers to the given {@link Notification}.
	 * 
	 * @param notification
	 * @return
	 */
	@Override
	public OverlayContacts getSubscribers(Notification notification) {
		OverlayContacts subscribers = super.getSubscribers(notification);
		if (notification instanceof LocationBasedNotification) {
			subscribers.addAll(getLBSSubscribers((LocationBasedNotification) notification));
		}
		return subscribers;
	}

	/**
	 * Return all subscribers in this storage
	 * 
	 * @return OverlayContacts inside the storage
	 */
	@Override
	public OverlayContacts getSubscribers() {
		OverlayContacts subscribers = super.getSubscribers();
		subscribers.addAll(locationBasedSubsBySubscriber.keySet());
		return subscribers;
	}

	/**
	 * A set of all LBS-subscribed {@link OverlayContact}s
	 * 
	 * @return
	 */
	public OverlayContacts getLBSSubscribers() {
		OverlayContacts subscribers = new OverlayContacts();
		subscribers.addAll(locationBasedSubsBySubscriber.keySet());
		return subscribers;
	}

	@Override
	public int getMatchComplexity(Notification notification) {
		if (notification instanceof LocationBasedNotification) {
			return getLBSMatchComplexity((LocationBasedNotification) notification);
		}
		return super.getMatchComplexity(notification);
	}

	/**
	 * Return the complexity of matching an LBS notification
	 * 
	 * @return
	 */
	protected int getLBSMatchComplexity(LocationBasedNotification notification) {
		/*
		 * Here: number of clients
		 */
		return locationBasedSubsBySubscriber.size();
	}

	/**
	 * Remove a subscription from the storage. If we have no more location-based
	 * subscriptions of a given client, we also remove the last known location,
	 * assuming that it will soon be inaccurate.
	 * 
	 * @param contact
	 *            The overlay contact of that subscription
	 * @param xsub
	 *            The subscription to be removed
	 */
	protected boolean removeLBSSubscription(OverlayContact subscriber, LocationBasedSubscription xsub) {
		if (locationBasedSubsBySubscriber.containsKey(subscriber)) {
			locationBasedSubsBySubscriber.get(subscriber).remove(xsub);
			if (locationBasedSubsBySubscriber.get(subscriber).isEmpty()) {
				// no longer a LBS-Subscriber
				locationBasedSubsBySubscriber.remove(subscriber);
				lastKnownLocation.remove(subscriber.getNodeID());
				onNoLongerLBSSubscriber(subscriber);
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns all overlay contacts that have a location match on the given
	 * location-based notification. Use the last known locations stored in this
	 * storage to match location-based notifications.
	 * 
	 * @param notification
	 *            The location-based notification
	 * @return OverlayContacts to this notification
	 */
	protected OverlayContacts getLBSSubscribers(LocationBasedNotification notification) {
		OverlayContacts subscribers = new OverlayContacts();

		for (Entry<OverlayContact, Set<LocationBasedSubscription>> entry : locationBasedSubsBySubscriber.entrySet()) {
			for (LocationBasedSubscription subscription : entry.getValue()) {
				if (getComponent().matches(notification, subscription)) {
					if (lastKnownLocation.containsKey(entry.getKey().getNodeID())
							&& matchesLBS(notification, subscription, entry.getKey())) {
						subscribers.add(entry.getKey());
					}
				}
			}
		}

		return subscribers;
	}

	/**
	 * Allows extension of the matching-function for location-based
	 * subscriptions.
	 * 
	 * @param notification
	 * @param subscription
	 * @param lastKnownClientLocation
	 *            (may be null)
	 * @return
	 */
	protected boolean matchesLBS(LocationBasedNotification notification, LocationBasedSubscription subscription,
			OverlayContact subscriber) {
		Location lastKnownLocation = getLastKnownLocation(subscriber);
		if (lastKnownLocation != null
				&& lastKnownLocation.distanceTo(notification.getLocation()) <= subscription.getRadiusOfInterest()
						+ notification.getRadiusOfInterest()) {
			return true;
		}
		return false;
	}

	/**
	 * This method is invoked if the node is no longer a subscriber within the
	 * LBS. This means: we no longer need to maintain state for the client (last
	 * position, etc.) and should delete outdated state.
	 * 
	 * @param subscriber
	 */
	protected void onNoLongerLBSSubscriber(OverlayContact subscriber) {
		/*
		 * Overwrite, if needed (e.g., for STE, we would delete the vectors)
		 */
	}

	/**
	 * Returns all current LBS subscriptions by the given subscriber.
	 * 
	 * @param subscriber
	 * @return
	 */
	public Set<LocationBasedSubscription> getLocationBasedSubscriptionsBy(OverlayContact subscriber) {
		if (!locationBasedSubsBySubscriber.containsKey(subscriber)) {
			return new LinkedHashSet<>();
		}
		return locationBasedSubsBySubscriber.get(subscriber);
	}

	/**
	 * Return a last known location for the client, null otherwise. Could be
	 * used for state transfer etc.
	 * 
	 * @param subscriber
	 * @return
	 */
	public Location getLastKnownLocation(OverlayContact subscriber) {
		return lastKnownLocation.get(subscriber.getNodeID());
	}

	/**
	 * Return the last known location of the node with the given ID
	 * 
	 * @param nodeId
	 * @return
	 */
	public Location getLastKnownLocation(INodeID nodeId) {
		return lastKnownLocation.get(nodeId);
	}

	/**
	 * This is needed by the SiS if we feed the last known locations into it.
	 * 
	 * @return
	 */
	public Set<INodeID> getNodeIdsWithKnownLocations() {
		return lastKnownLocation.keySet();
	}

	/**
	 * Updates the last known position (context) of a client.
	 * 
	 * @param client
	 *            the subscriber
	 * @param updatedLocation
	 *            the new location.
	 */
	public void updateLocationOfClient(OverlayContact client, Location updatedLocation) {
		lastKnownLocation.put(client.getNodeID(), updatedLocation);
	}
}
