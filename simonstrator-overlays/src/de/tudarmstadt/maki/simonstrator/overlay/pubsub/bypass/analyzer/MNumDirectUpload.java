package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer.MNumDirectUpload.MVNumDirectUpload;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.DirectorAssistedUploadStrategy;

import java.util.List;

/**
 * TODO
 * @author Julian Wulfheide
 */
public class MNumDirectUpload extends AbstractMetric<MVNumDirectUpload> {

    public MNumDirectUpload() {
        super("Total count for chunks that have been uploaded directly", MetricUnit.NONE);
    }

    @Override
    public void initialize(List<Host> hosts) {
        for (Host host : hosts) {
            addHost(host, new MVNumDirectUpload(host));
        }
    }

    public class MVNumDirectUpload implements de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue<Integer> {

        private final Host host;
        DirectorAssistedUploadStrategy uls = null;

        public MVNumDirectUpload(Host host) {
            this.host = host;
        }

        @Override
        public Integer getValue() {
            try {
                uls = host.getComponent(DirectorAssistedUploadStrategy.class);
                return uls.getNumDirectUpload();
            } catch (ComponentNotAvailableException e) {
                uls = null;
                return 0;
            }
        }

        @Override
        public boolean isValid() {
            return uls != null && (uls.getNumDirectUpload() + uls.getNumForwarded() > 0);
        }
    }
}
