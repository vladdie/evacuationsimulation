package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.BypassMessage;

public interface BypassCloudMessage extends BypassMessage {
	// marker
}
