package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;

public class SubscriptionGridCell {

	/**
	 * Minimum bounding rectangle as the Area of Influence. Notifications inside
	 * this area are routed to all clients inside the rectangle.
	 */
	private BoundingRectangle mbr;

	/**
	 * Contains the subset of subscribers to this {@link Subscription} that
	 * subscribed using a grid-based subscription.
	 */
	private LinkedHashSet<OverlayContact> gridBasedSubscribers;

	/**
	 * Constructor
	 * 
	 * @param subscribers
	 * @param rectangle
	 */
	public SubscriptionGridCell(BoundingRectangle rectangle) {
		gridBasedSubscribers = new LinkedHashSet<OverlayContact>();
		mbr = rectangle;
	}

	/**
	 * Remove an overlay contact from the subscriptions
	 */
	public boolean removeSubscriber(OverlayContact contact) {
		return gridBasedSubscribers.remove(contact);
	}

	/**
	 * Add an overlay contact to the subscriptions
	 */
	public void addSubscriber(OverlayContact contact) {
		gridBasedSubscribers.add(contact);
	}

	/**
	 * Return the current mbr
	 * 
	 * @return Bounding rectangle
	 */
	public BoundingRectangle getMBR() {
		return mbr;
	}

	/**
	 * Return the hash set containing all subscribed contacts
	 * 
	 * @return
	 */
	public Set<OverlayContact> getSubscribers() {
		return Collections.unmodifiableSet(gridBasedSubscribers);
	}
}
