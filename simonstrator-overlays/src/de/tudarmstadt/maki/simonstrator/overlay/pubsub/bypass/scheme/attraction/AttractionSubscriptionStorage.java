package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.ChannelBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.multi.MultiSchemeBroker;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Storage for subscription bubbles and the contacts that are inside them
 * 
 * @author Julian Zobel
 *
 */
public class AttractionSubscriptionStorage extends LocationBasedSubscriptionStorage {

	private Set<AttractionPoint> attractionPoints;

	private Map<AttractionPoint, Set<OverlayContact>> attractionPointSubscribers;

	private Set<OverlayContact> unassociatedSubscribers;

	private AssignmentListener assignmentListener;

	/**
	 * Create a subscription storage holding all attraction points and their
	 * subscribers
	 * 
	 * @param comp
	 *            Local overlay contact of the broker
	 * @param attractionPoints
	 *            initial set of attraction points
	 */
	public AttractionSubscriptionStorage(BypassPubSubComponent comp) {
		super(comp);
		this.attractionPoints = new LinkedHashSet<>();
		this.attractionPointSubscribers = new LinkedHashMap<>();
		this.unassociatedSubscribers = new LinkedHashSet<>();
	}

	/**
	 * Update association of APs for a specific subscriber. This is called as a
	 * consequence of an incoming {@link AttractionSubscriptionMsg}.
	 * 
	 * @param subscriber
	 * @param associatedAttractionPoints
	 */
	public void updateAttractionPointAssociation(OverlayContact subscriber,
			Set<AttractionPoint> associatedAttractionPoints, boolean notifyListener) {
		for (Map.Entry<AttractionPoint, Set<OverlayContact>> association : attractionPointSubscribers.entrySet()) {
			if (associatedAttractionPoints.contains(association.getKey())) {
				// yes (set)
				association.getValue().add(subscriber);
			} else {
				// no
				association.getValue().remove(subscriber);
			}
		}
		if (associatedAttractionPoints.isEmpty()) {
			unassociatedSubscribers.add(subscriber);
		} else {
			unassociatedSubscribers.remove(subscriber);
		}
		if (notifyListener && assignmentListener != null) {
			assignmentListener.updateAttractionPointAssociation(subscriber, associatedAttractionPoints);
		}
	}

	/**
	 * Whether the given location is within the reach of an attraction point.
	 * This is used for the {@link MultiSchemeBroker}.
	 * 
	 * @param location
	 * @return
	 */
	public AttractionPoint isWithinAttractionPointReach(Location location) {
		for (AttractionPoint ap : attractionPoints) {
			if (ap.distanceTo(location) <= ap.getRadius()) {
				return ap;
			}
		}
		return null;
	}

	/**
	 * As this is only to be used by the {@link MultiSchemeBroker}, we support
	 * only one listener at a time.
	 * 
	 * @param assignmentListener
	 */
	public void setAssignmentListener(AssignmentListener assignmentListener) {
		this.assignmentListener = assignmentListener;
	}

	@Override
	protected int getLBSMatchComplexity(LocationBasedNotification notification) {
		if (notification instanceof ChannelBasedNotification) {
			return ((ChannelBasedNotification) notification).getChannels().size();
		}
		if (BypassSubscriptionSchemeConfiguration.attractionEnableLBSHybrid) {
			return attractionPoints.size() + unassociatedSubscribers.size();
		}
		return attractionPoints.size();
	}

	@Override
	protected void onNoLongerLBSSubscriber(OverlayContact subscriber) {
		super.onNoLongerLBSSubscriber(subscriber);
		// remove attraction point associations
		updateAttractionPointAssociation(subscriber, new LinkedHashSet<>(), true);
		unassociatedSubscribers.remove(subscriber);
	}

	@Override
	protected OverlayContacts getLBSSubscribers(LocationBasedNotification notification) {

		// find relevant Attraction Points
		OverlayContacts candidates = new OverlayContacts();
		if (notification instanceof ChannelBasedNotification) {
			// channel-based matching
			for (Map.Entry<AttractionPoint, Set<OverlayContact>> association : attractionPointSubscribers.entrySet()) {
				AttractionPoint ap = association.getKey();
				if (((ChannelBasedNotification) notification).getChannels().contains(ap.getName())) {
					candidates.addAll(association.getValue());
				}
			}
		} else {
			// location-based matching
			for (Map.Entry<AttractionPoint, Set<OverlayContact>> association : attractionPointSubscribers.entrySet()) {
				AttractionPoint ap = association.getKey();
				if (ap.distanceTo(notification.getLocation()) <= ap.getRadius() + notification.getRadiusOfInterest()) {
					candidates.addAll(association.getValue());
				}
			}
		}

		// filter for the content of the LBS-Subs
		OverlayContacts subscribers = new OverlayContacts();
		for (OverlayContact candidate : candidates) {
			Set<LocationBasedSubscription> subs = getLocationBasedSubscriptionsBy(candidate);
			for (LocationBasedSubscription sub : subs) {
				if (getComponent().matches(notification, sub)) {
					subscribers.add(candidate);
					break;
				}
			}
		}

		// IFF in hybrid mode, check for LBS matches as well
		if (BypassSubscriptionSchemeConfiguration.attractionEnableLBSHybrid) {
			for (OverlayContact candidate : unassociatedSubscribers) {
				Set<LocationBasedSubscription> subs = getLocationBasedSubscriptionsBy(candidate);
				for (LocationBasedSubscription sub : subs) {
					if (getComponent().matches(notification, sub)) {
						if (getLastKnownLocation(candidate) != null
								&& matchesLBS(notification, sub, candidate)) {
							subscribers.add(candidate);
						}
					}
				}
			}
		}

		return subscribers;
	}

	/**
	 * AP-Subscribers
	 * 
	 * @return
	 */
	public Map<AttractionPoint, Set<OverlayContact>> getAttractionPointSubscribers() {
		return attractionPointSubscribers;
	}

	/**
	 * Subscribers that are NOT assigned to an attraction point.
	 * 
	 * @return
	 */
	public Set<OverlayContact> getUnassociatedSubscribers() {
		return unassociatedSubscribers;
	}

	/**
	 * All {@link AttractionPoint}
	 * 
	 * @return
	 */
	public Set<AttractionPoint> getAttractionPoints() {
		return attractionPoints;
	}

	/**
	 * Adds a new attraction point to the storage and creates the respective
	 * channel.
	 * 
	 * @param attractionPoint
	 *            containing a radius
	 */
	public void addAttractionPoints(Set<AttractionPoint> attractionPoints) {
		this.attractionPoints.addAll(attractionPoints);
		for (AttractionPoint attractionPoint : attractionPoints) {
			this.attractionPointSubscribers.put(attractionPoint, new LinkedHashSet<>());
		}
	}

	/**
	 * Removes an attraction point and the associated channel.
	 * 
	 * @param attractionPoint
	 */
	public void removeAttractionPoints(Set<AttractionPoint> attractionPoints) {
		this.attractionPoints.removeAll(attractionPoints);
		for (AttractionPoint attractionPoint : attractionPoints) {
			this.attractionPointSubscribers.remove(attractionPoint);
		}
	}

	/**
	 * Enables external elements to register as Listener to updated assignments.
	 * This is only to be used by the {@link MultiSchemeBroker}!
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public interface AssignmentListener {
		/**
		 * Called, whenever a client reports a new association.
		 * 
		 * @param subscriber
		 * @param associatedAttractionPoints
		 */
		public void updateAttractionPointAssociation(OverlayContact subscriber,
				Set<AttractionPoint> associatedAttractionPoints);
	}

}
