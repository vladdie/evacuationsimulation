package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste;

import de.tudarmstadt.maki.simonstrator.api.component.transition.MechanismState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;

/**
 * Broker-side implementation of the Space-Time-Envelope scheme
 * 
 * @author Julian Zobel
 *
 */
public class SpaceTimeEnvelopeSchemeBroker extends AbstractLocationBasedSubscriptionScheme.Broker {

	/**
	 * Storage for client subscriptions
	 */
	private SpaceTimeEnvelopeSubscriptionStorage storage;

	@MechanismState("Alpha")
	private double alpha;

	@TransferState({ "Component" })
	public SpaceTimeEnvelopeSchemeBroker(BypassCloudComponent component) {
		super(component, SchemeName.STE);
		this.alpha = BypassSubscriptionSchemeConfiguration.STE_ALPHA;
		this.storage = new SpaceTimeEnvelopeSubscriptionStorage(component, alpha);
	}

	@Override
	protected boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {
		if (msg instanceof SpaceTimeEnvelopeUpdateMessage) {
			SpaceTimeEnvelopeUpdateMessage umsg = (SpaceTimeEnvelopeUpdateMessage) msg;
			storage.updateLocationOfClient(umsg.getSender(), umsg.getLocation());
			storage.updateMovementVectorOfClient(umsg.getSender(), umsg.getLocation(), umsg.getMovementVector());
		} else {
			return false;
		}
		return true;
	}

	@Override
	public SpaceTimeEnvelopeSubscriptionStorage getStorage() {
		return storage;
	}

	public double getAlpha() {
		return alpha;
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
		storage.setAlpha(alpha);
	}

}
