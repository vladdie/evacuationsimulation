package de.tudarmstadt.maki.simonstrator.overlay.pubsub.util;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.NotificationInfo;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;

/**
 * Basic implementation of a {@link Notification}. Notifications include a
 * random ID to enable hashCode and equals - checks. In other words: if an
 * application creates two notifications with the same attributes, they will NOT
 * be equal.
 * 
 * For performance reasons, equality is solely based on this random ID and the
 * created-timestamp, as collisions should be highly unlikely. These two fields
 * are not available to the overlay or the application and should not be a
 * functional part of the overlay.
 * 
 * To enable serialization with small message sizes, you should always make use
 * of this notification. There should be no need at all to extend this type.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class BasicNotification implements Notification, KryoSerializable {

	private static final long serialVersionUID = 1L;

	private Topic topic;

	private List<Attribute<?>> attributes;

	private Map<String, Attribute<?>> attributesByName;

	private byte[] payload;

	// Analyzing, used by applications!
	private transient NotificationInfo _notificationInfo;

	private transient static final Random rnd = Randoms
			.getRandom(Notification.class);

	private int id;

	private long createdTimestamp;

	@SuppressWarnings("unused")
	private BasicNotification() {
		// for Kryo
	}

	/**
	 * A new notification. Should only be created when the corresponding
	 * PubSubComponent-Call is issued. Notifications are stateless, i.e., they
	 * are not cloned or altered once created!
	 * 
	 * @param topic
	 * @param attributes (can be null)
	 * @param payload
	 */
	public BasicNotification(Topic topic, List<Attribute<?>> attributes,
			byte[] payload) {
		this.topic = topic;
		if (attributes == null) {
			attributes = new LinkedList<Attribute<?>>();
		}
		this.attributes = attributes;
		this.attributesByName = new LinkedHashMap<>(attributes.size());
		for (Attribute<?> attribute : attributes) {
			attributesByName.put(attribute.getName(), attribute);
		}
		this.payload = payload;
		this.id = rnd.nextInt();
		this.createdTimestamp = Time.getCurrentTime();
	}

	@Override
	public Topic getTopic() {
		return topic;
	}

	@Override
	public List<Attribute<?>> getAttributes() {
		return Collections.unmodifiableList(attributes);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> Attribute<T> getAttribute(String name, Class<T> valueType) {
		Attribute<?> cand = attributesByName.get(name);
		if (cand != null && cand.getType().equals(valueType)) {
			return (Attribute<T>) cand;
		}
		return null;
	}

	@Override
	public byte[] getPayload() {
		return payload;
	}

	@Override
	public NotificationInfo _getNotificationInfo(NotificationInfo info) {
		if (info != null) {
			if (_notificationInfo != null) {
				throw new AssertionError("Already set!");
			}
			_notificationInfo = info;
		}
		return _notificationInfo;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(topic);
		str.append("   attr:{");
		for (Attribute<?> attr : attributes) {
			str.append(attr);
			str.append('|');
		}
		str.append('}');
		return str.toString();
	}

	private transient int size_cached = -1;

	@Override
	public int getTransmissionSize() {
		if (size_cached == -1) {
			size_cached = topic.getTransmissionSize();
			for (Attribute<?> attr : attributes) {
				size_cached += attr.getTransmissionSize();
			}
			if (payload != null) {
				size_cached += payload.length;
			}
			if (_notificationInfo != null) {
				// In simulations, we might not want to create byte-arrays to
				// fake a size. This object is used instead!
				size_cached += _notificationInfo.getNotificationPayloadSize();
			}
		}
		return size_cached;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ (int) (createdTimestamp ^ (createdTimestamp >>> 32));
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicNotification other = (BasicNotification) obj;
		if (createdTimestamp != other.createdTimestamp)
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public void read(Kryo kryo, Input input) {
		// String for topic
		topic = new BasicTopic(kryo.readObject(input, String.class));
		// No of attributes
		int noAttr = kryo.readObject(input, int.class);
		// Each attribute
		attributes = new LinkedList<Attribute<?>>();
		for (int i = 0; i < noAttr; i++) {
			attributes.add((Attribute<?>) kryo.readClassAndObject(input));
		}
		// Extra payload
		payload = kryo.readObjectOrNull(input, byte[].class);
		// timestamp
		createdTimestamp = kryo.readObject(input, long.class);
		// id
		id = kryo.readObject(input, int.class);
	}

	@Override
	public void write(Kryo kryo, Output output) {
		// String for topic
		kryo.writeObject(output, topic.getValue());
		// No of attributes
		kryo.writeObject(output,
				(int) (attributes.isEmpty() ? 0 : attributes.size()));
		// Each attribute
		for (Attribute<?> attribute : attributes) {
			kryo.writeClassAndObject(output, attribute);
		}
		// Extra payload
		kryo.writeObjectOrNull(output, payload, byte[].class);
		// timestamp
		kryo.writeObject(output, createdTimestamp);
		// id
		kryo.writeObject(output, id);
	}

}
