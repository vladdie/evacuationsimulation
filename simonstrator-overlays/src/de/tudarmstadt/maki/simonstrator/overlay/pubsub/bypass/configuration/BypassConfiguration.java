package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassFactory;

/**
 * A helper class for the configuration of BypassComponents. This is passed to
 * the {@link BypassFactory} to ensure proper configuration of all hosts.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface BypassConfiguration {

	/**
	 * Add modules, set parameters, etc. for the components
	 * 
	 * @param component
	 */
	public void configureCloud(BypassCloudComponent comp);

	/**
	 * Configure this client instance
	 * @param comp
	 */
	public void configureClient(BypassClientComponent comp);

}
