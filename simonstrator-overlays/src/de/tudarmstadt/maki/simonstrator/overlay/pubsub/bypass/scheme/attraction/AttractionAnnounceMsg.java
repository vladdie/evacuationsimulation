package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction;

import java.util.LinkedHashSet;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.BypassCloudMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;

/**
 * This message is sent to clients and carries all attraction points. These are
 * then used by clients to determine the channels they need to subscribe to.
 * 
 * This message is also used if the set of attraction points changes.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class AttractionAnnounceMsg extends AbstractOverlayMessage implements BypassCloudMessage, BypassSchemeMessage {

	private static final long serialVersionUID = 1L;

	private Set<AttractionPoint> attractionPoints;

	/**
	 * 
	 * @param sender
	 * @param receiver
	 * @param attractionPoints
	 *            we assume that attraction points do NOT change their location.
	 *            Instead, the broker would just create a new attraction point
	 *            and delete another one. Same holds for radius and weight,
	 *            therefore the points themselves are NOT cloned.
	 */
	public AttractionAnnounceMsg(OverlayContact sender, OverlayContact receiver,
			Set<AttractionPoint> attractionPoints) {
		super(sender, receiver);
		this.attractionPoints = new LinkedHashSet<>(attractionPoints);
	}

	/**
	 * Set of all attraction points
	 * 
	 * @return
	 */
	public Set<AttractionPoint> getAttractionPoints() {
		return attractionPoints;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public SchemeName getSchemeName() {
		return SchemeName.ATTRACTION;
	}

}
