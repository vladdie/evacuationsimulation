package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;

/**
 * Client-side implementation of the LBS scheme.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class LocationBasedSchemeClient extends AbstractLocationBasedSubscriptionScheme.Client {

	/**
	 * Constructor for client-side of the LBS scheme.
	 * 
	 * @param component
	 */
	@TransferState({ "Component" })
	public LocationBasedSchemeClient(BypassPubSubComponent component) {
		super(component, SchemeName.LBS);
	}

	@Override
	protected boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {
		// as clients, we do not expect incoming messages.
		return false;
	}

	@Override
	public Notification createLocalNotification(Topic topic, List<Attribute<?>> attributes, double radiusOfInterest,
			byte[] payload) {
		return new LocationBasedNotification(topic, attributes, payload, getCurrentLocation(), radiusOfInterest, true);
	}

	@Override
	protected void onLocationChanged(Location newLocation, Location oldLocation, long timeSinceLastUpdate,
			double distanceSinceLastUpdate) {
		/*
		 * Send an update message to our broker, carrying the new location. This
		 * method is invoked periodically on a host, if the host is a
		 * location-based subscriber. He has to send his current position to the
		 * broker in order for the broker to update its state.
		 */
		sendLocationUpdateMessage();
	}

}
