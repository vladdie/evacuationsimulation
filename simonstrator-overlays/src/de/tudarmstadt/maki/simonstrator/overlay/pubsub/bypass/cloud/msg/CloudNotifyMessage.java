package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;

/**
 * Cloud notifying a client of a new notification matching one of its
 * subscriptions. May include additional Ctrl-Information that influences the
 * local dissemination strategy of the client.
 * 
 * @author Bjoern Richerzhagen
 * @version May 3, 2014
 */
public class CloudNotifyMessage extends AbstractOverlayMessage implements
		BypassCloudMessage {

	private static final long serialVersionUID = 1L;

	private Notification n;

	protected CloudNotifyMessage() {
		// For Kryo
	}

	public CloudNotifyMessage(OverlayContact sender, OverlayContact receiver,
			Notification n) {
		super(sender, receiver);
		this.n = n;
	}

	public Notification getNotification() {
		return n;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public long getSize() {
		return super.getSize() + n.getTransmissionSize();
	}

}
