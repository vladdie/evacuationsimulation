package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.gossip;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.PubSubNotificationForwardingAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.transition.MechanismState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;

public class GossipDistribution extends AbstractLocalDissemination {

	private final static int ID_CACHE_SIZE = 20000;

	@SuppressWarnings("serial")
	LinkedHashMap<Integer, Object> seenMsgs = new LinkedHashMap<Integer, Object>() {
		@Override
		protected boolean removeEldestEntry(Map.Entry<Integer, Object> eldest) {
			return size() > ID_CACHE_SIZE;
		}
	};

	@MechanismState("Alpha")
	private double alpha = 0.7;
	
	@TransferState(value = { "Component", "Port" })
	public GossipDistribution(BypassClientComponent comp, int port) {
		super(comp, port);
	}

	/**
	 * Enables reconfiguration of alpha
	 * 
	 * @param alpha
	 */
	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

	@Override
	public LocalDistributionType getLocalDistributionType() {
		return LocalDistributionType.GOSSIP;
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		super.notify(n, subscribers); // trigger analyzer
		GossipNotificationMsg msg = new GossipNotificationMsg(
				getLocalOverlayContact(), n, getMsgId());
		seenMsgs.put(msg.getMsgId(), null);
		send(msg);
		getComponent().handleLocalNotification(n);
	}

	@Override
	protected boolean handleMessage(BypassLocalMessage msg, int commID) {
		assert getCurrentState() != DistrState.OFF;
		if (!(msg instanceof GossipNotificationMsg)) {
			return false;
		}

		GossipNotificationMsg gnm = (GossipNotificationMsg) msg;
		if (seenMsgs.containsKey(gnm.getMsgId())) {
			// Forwarding Analyzer: dropped
			if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
				Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
						getComponent().getHost(), gnm.getNotification(), gnm.getSender().getNodeID(), 0);
			}
			return true; // true = msg was processed by this scheme
		}

		// notify own node
		boolean isSubscriber = getComponent().handleLocalNotification(gnm.getNotification());
		boolean forward = getCurrentState() == DistrState.ON;

		if (isSubjectToGeofencing(gnm.getNotification())) {
			forward = forward && isSubscriber;
		}

		// forward
		if (forward) {
			/*
			 * FIXME limited the number of hops
			 */
			assert getCurrentState() == DistrState.ON;
			seenMsgs.put(gnm.getMsgId(), null);
			// alpha
			if (rnd.nextDouble() < alpha) {
				GossipNotificationMsg fw = new GossipNotificationMsg(
						getLocalOverlayContact(), gnm);
				// Forwarding Analyzer
				if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
					Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onForwardNotification(
							getComponent().getHost(), gnm.getNotification(), gnm.getSender().getNodeID(), 0);
				}
				send(fw);
			} else {
				// Forwarding Analyzer: dropped
				if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
					Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
							getComponent().getHost(), gnm.getNotification(), gnm.getSender().getNodeID(), 0);
				}
			}
		} else {
			// Forwarding Analyzer: dropped
			if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
				Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
						getComponent().getHost(), gnm.getNotification(), gnm.getSender().getNodeID(), 0);
			}
		}
		return true;
	}

}
