package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste;

import java.util.List;

import org.dyn4j.geometry.Vector2;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;

/**
 * Subscription scheme that is able to use extended subscription areas depending
 * on speed and direction of a subscriber.
 * 
 * @author Julian Zobel
 *
 */
public class SpaceTimeEnvelopeSchemeClient extends AbstractLocationBasedSubscriptionScheme.Client {

	@TransferState({ "Component" })
	public SpaceTimeEnvelopeSchemeClient(BypassPubSubComponent component) {
		super(component, SchemeName.STE);
	}

	@Override
	protected void onLocationChanged(Location newLocation, Location oldLocation, long timeSinceLastUpdate,
			double distanceSinceLastUpdate) {

		if (getComponent().isInAdhocMode()) {
			return;
		}

		/*
		 * Send an update message to our broker, carrying the new location. This
		 * method is invoked periodically on a host, if the host is a
		 * location-based subscriber. He has to send his current position to the
		 * broker in order for the broker to update its state.
		 */
		Vector2 newVector = calculateDirectionVector(oldLocation, newLocation, timeSinceLastUpdate,
				distanceSinceLastUpdate);

		getComponent().sendViaMobileNetwork(new SpaceTimeEnvelopeUpdateMessage(getComponent().getLocalOverlayContact(),
				getComponent().getBrokerContact(), newLocation, newVector, getName()));
	}

	@Override
	protected boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {
		/*
		 * We do not expect scheme messages as clients.
		 */
		return false;
	}

	@Override
	public Notification createLocalNotification(Topic topic, List<Attribute<?>> attributes, double radiusOfInterest,
			byte[] payload) {
		return new LocationBasedNotification(topic, attributes, payload, getCurrentLocation(), radiusOfInterest, true);
	}

	/**
	 * Calculates the direction and speed vector of the client. This is used by
	 * the broker to determine the size of the STE.
	 * 
	 * @param lastLocation
	 * @param currentLocation
	 * @param timeSinceLastUpdate
	 * @param distanceSinceLastUpdate
	 * @return
	 */
	private Vector2 calculateDirectionVector(Location lastLocation, Location currentLocation, long timeSinceLastUpdate,
			double distanceSinceLastUpdate) {
		// calculate the velocity of the node based on the current location
		// and time in comparison to the last saved values
		double deltaTimeSecs = timeSinceLastUpdate / (double) Time.SECOND;

		// System.out.print("DeltaTime: " + deltaTimeSecs);
		double velocity = distanceSinceLastUpdate / deltaTimeSecs;

		// Now calculate the direction vector between the last and the current
		// position, normalize it and apply the velocity (as length) to it
		Vector2 deltaVector = new Vector2(lastLocation.getLongitude(), lastLocation.getLatitude(),
				currentLocation.getLongitude(), currentLocation.getLatitude());
		deltaVector.normalize();
		deltaVector.multiply(velocity);

		return deltaVector;
	}

}
