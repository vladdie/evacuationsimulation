package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;

/**
 * A simple base class for local messages.
 *
 * @author Julian Wulfheide
 */
public class DefaultLocalMessage extends AbstractOverlayMessage implements BypassLocalMessage {

    private static final long serialVersionUID = 1L;

	private LocalDistributionType disseminationType;
	private int senderTransitionCount;

	public DefaultLocalMessage(OverlayContact sender, OverlayContact receiver) {
		super(sender, receiver);
	}

	/**
	 * Forward-constructor
	 * 
	 * @param sender
	 * @param receiver
	 * @param toForward
	 */
	public DefaultLocalMessage(OverlayContact sender, OverlayContact receiver, DefaultLocalMessage toForward) {
		super(sender, receiver, toForward);
	}

	public void setDisseminationType(LocalDistributionType disseminationType) {
		this.disseminationType = disseminationType;
	}

	@Override
	public LocalDistributionType getDisseminationType() {
		return disseminationType;
    }

	public void setSenderTransitionCount(int senderTransitionCount) {
		this.senderTransitionCount = senderTransitionCount;
	}

	@Override
	public int getSenderTransitionCount() {
		return senderTransitionCount;
	}

    @Override
    public Message getPayload() {
        return null;
    }
}
