package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Map.Entry;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;

public class HyperGossipDistribution extends AbstractLocalDissemination{
	
	private static final int EVENTTYPE_GOSSIP 		= 0;
	private static final int EVENTTYPE_REBROADCAST 	= 1;
	private static final int EVENTTYPE_BEACON	 	= 2;
	private static final int EVENTTYPE_LIFETIMEMSG 	= 3;
	private static final int EVENTTYPE_LIFETIMECANCEL = 5;
	
	private static final int EVENTTYPE_OLDMSG		= 4;
	
	
	private static final long beaconTime 			=   1 * Time.SECOND;
	private static final long msgLifeTime 			= 300 * Time.MILLISECOND;
	private static final long msgOldTime 			=  10 * Time.SECOND;			//->how long to remember messages
	private static final long rebroadcastTime 		=  20 * Time.MILLISECOND;		//how long to wait between two reBroadcasted messages
	private static final long timeToForgetNeighbor 	=   3 * Time.SECOND;			//After minimal x time w/o new message: remove neighbor > beaconTime!
	private static final long lifeTimeCancel		=  13 * Time.SECOND;
	
	// evaluation ..
	/*
	private static int _recvedLBR = 0;
	private static int _recvedBR = 0;
	private static int _recvedDATA = 0;
	private static int _removedNeighbors = 0;
	private static int _newNeighbors = 0;
	//messages not sent due to p, min p max p
	
	private static int _lastrecvedLBR = 0;
	private static int _lastrecvedBR = 0;
	private static int _lastrecvedDATA = 0;
	private static int _lastremovedNeighbors = 0;
	private static int _lastnewNeighbors = 0;
	
	private static int _myLBRPeak = 0;
	private static int _seenMsgsPeak = 0;
	private static int _canceledIDsPeak = 0;
	private static int _bufferPeak = 0;
	*/
	/** gossip probability - will the message be sent to neighbors*/
	//private double p; 
	/** rebroadcast and gossip delay */
	private long fDelay = 10*Time.MILLISECOND;
	/** used to determine partitions - when comparing similarity of received message IDs */
	private float IS_threshold = 0.3f;
	/** length of myLBR - how many broadcast messages are saved */
	private int maxLBRlength = 50;
	/** Delay used when receiving a HELLO beacon with BR */
	private long rDelay = 100*Time.MILLISECOND;
	/** was a new neighbor discovered? - if true LBR is included in next HELLO beacon. Returns to false after sending a beacon */
	private boolean sendLBR;
	/** message IDs of messages received or created here */
	private LinkedHashSet<Integer> myLBR = new LinkedHashSet<Integer>();
	/** used to determine if a message is received for the first time */
	private LinkedHashSet<Integer> seenMsgs = new LinkedHashSet<Integer>();
	/** IDs of messages that do not need to be send */
	private LinkedHashSet<Integer> canceledIDs = new LinkedHashSet<Integer>();
	/** old messages that may be reBroadcasted later. <MessageID, Message> 
	 *  Keys also function as Broadcast Received */
	private HashMap<Integer, HyperGossipDataNotification> buffer = new HashMap<Integer, HyperGossipDataNotification>();
	
	/** set of nodes within one hop distance */
	private HashMap<OverlayContact, Long> neighbors = new HashMap<OverlayContact, Long>();	//state possible
	
	/** are beacons being send */
	private boolean beaconRunning = false;
	
	/** handles the reBroadcasting process. */
	private EventHandler rebroadcastHandler = new EventHandler() {
		@Override
		public void eventOccurred(Object content, int type) {
			assert type == EVENTTYPE_REBROADCAST;
			//assert content instanceof HyperGossipNotification || content instanceof HyperGossipNotification[]; //TODO: content is HGN, but assert fails
			
			if (getCurrentState() == DistrState.OFF) {
				return;
			}

			LinkedList<HyperGossipDataNotification> msgsToSend = new LinkedList<HyperGossipDataNotification>();
			
			if(content instanceof HyperGossipBRNotification)
			{
				HyperGossipBRNotification msg = (HyperGossipBRNotification)content;
				
				Set<Integer> ids = new HashSet<Integer>();
				
				for(Integer id : buffer.keySet())
				{
					if(!msg.getBRIds().contains(id))
						ids.add(id);
				}
				
				for(Integer id : ids)
				{
					if(buffer.containsKey(id))
						msgsToSend.add(buffer.get(id));
				}
				
			}
			else if(content instanceof HyperGossipDataNotification[])
			{
				msgsToSend.addAll((Arrays.asList((HyperGossipDataNotification[])content)));
				msgsToSend.removeAll(canceledIDs);
			}
			
			//rebroadcast and schedule next reBroadcasted message
			if(msgsToSend.size() > 0)
			{
				HyperGossipDataNotification n = msgsToSend.get(0);
				HyperGossipNotification send = new HyperGossipDataNotification(getLocalOverlayContact(), 
						n.getID(), 
						n.getNotification(), 
						n.getRemainingLifeTime());
				send(send);
				
				msgsToSend.removeFirst();
				if(msgsToSend.isEmpty())
					return;
				
				Event.scheduleWithDelay(rebroadcastTime, rebroadcastHandler, msgsToSend.toArray(), EVENTTYPE_REBROADCAST);
			}
		}
	};
	
	/** gossips messages */
	private EventHandler gossipHandler = new EventHandler(){
		@Override
		public void eventOccurred(Object content, int type) {
			assert type == EVENTTYPE_GOSSIP;
			assert content instanceof HyperGossipDataNotification;
			
			if (getCurrentState() == DistrState.OFF) {
				return;
			}

			HyperGossipDataNotification n = (HyperGossipDataNotification)content;
			HyperGossipDataNotification send = new HyperGossipDataNotification(getLocalOverlayContact(), 
					n.getID(), 
					n.getNotification(), 
					n.getRemainingLifeTime());
			send(send);
		}
	};
	
	/** sends beacons (HELLO_PURE and HELLO_LBR) */
	private EventHandler beaconHandler = new EventHandler() {
		@Override
		public void eventOccurred(Object content, int type) {
			assert type == EVENTTYPE_BEACON;
			
			if (getCurrentState() == DistrState.OFF) {
				return;
			}

			if(sendLBR)
			{
				//a new neighbor was discovered! compare last received broadcasts
				send(new HyperGossipLBRNotification(getLocalOverlayContact(), myLBR));
			}
			else
			{
				//send regular beacon
				send(new HyperGossipBeacon(getLocalOverlayContact()));
			}
			
			sendLBR = false;
			Event.scheduleWithDelay(beaconTime, beaconHandler, content, EVENTTYPE_BEACON);
			
			//remove lost neighbors
			Set<Entry<OverlayContact, Long>> keyValuePairs = new HashSet<Entry<OverlayContact, Long>>(neighbors.entrySet());
			for(Entry<OverlayContact, Long> n : keyValuePairs)
			{
				if(Time.getCurrentTime() - n.getValue() > timeToForgetNeighbor)
				{
					neighbors.remove(n.getKey());
	//				_removedNeighbors++;
				}
			}
		}
	};
	
	/** events occur when a message's lifetime ends **/
	private EventHandler lifetimeHandler = new EventHandler(){
		@Override
		public void eventOccurred(Object content, int type){
			assert type == EVENTTYPE_LIFETIMEMSG || type == EVENTTYPE_LIFETIMECANCEL;
			assert content instanceof HyperGossipDataNotification;
			
			if (getCurrentState() == DistrState.OFF) {
				return;
			}

			HyperGossipDataNotification msg = (HyperGossipDataNotification)content;
		
			if(type == EVENTTYPE_LIFETIMEMSG)
			{
				if(buffer.containsKey(msg))
					buffer.remove(msg);
				if(myLBR.contains(msg.getID()))
					myLBR.remove(msg.getID());
				//delete message from broadcast table (in out case the buffer)
			}
			else
			{
				canceledIDs.remove(msg.getID());
			}
			
		}
	};
	
	/** removes old messages */
	private EventHandler oldMessageHandler = new EventHandler(){
		@Override
		public void eventOccurred(Object content, int type){
			assert type == EVENTTYPE_OLDMSG;
			assert content instanceof Integer;	// -> id might be sufficient
		
			if (getCurrentState() == DistrState.OFF) {
				return;
			}

			Integer msgID = (Integer)content;
			
			if(buffer.containsKey(msgID))
				buffer.remove(msgID);
			if(myLBR.contains(msgID))
				myLBR.remove(msgID);
			if(seenMsgs.contains(msgID))
				seenMsgs.remove(msgID);
			if(canceledIDs.contains(msgID))
				canceledIDs.remove(msgID);
		}
		 
	};
	
	@TransferState(value = { "Component", "Port" })
	public HyperGossipDistribution(BypassClientComponent comp, int port) {
		super(comp, port);
	}

	@Override
	public LocalDistributionType getLocalDistributionType() {
		return LocalDistributionType.HYPERGOSSIP;
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		super.notify(n, subscribers); // trigger analyzer
		HyperGossipDataNotification msg = new HyperGossipDataNotification(getLocalOverlayContact(),getMsgId(), n, msgLifeTime);
		send(msg);
		getComponent().handleLocalNotification(n);
	}

	@Override
	protected boolean handleMessage(BypassLocalMessage msg, int commID) {
		assert getCurrentState() != DistrState.OFF;
		if (!(msg instanceof HyperGossipNotification)) {
			return false;
		}
		assert !msg.getSender().equals(getLocalOverlayContact());
		
	/*	
		if((_recvedBR + _recvedDATA + _recvedLBR) %100000 == 0)
		{
			if(myLBR.size() > _myLBRPeak)
				_myLBRPeak = myLBR.size();
			if(seenMsgs.size() > _seenMsgsPeak)
				_seenMsgsPeak = seenMsgs.size();
			if(canceledIDs.size() > _canceledIDsPeak)
				_canceledIDsPeak = canceledIDs.size();
			if(buffer.size() > _bufferPeak)
				_bufferPeak = buffer.size();
			
			System.out.println("after X*100.000 msgs: "); 
			System.out.println("	Total received HELLO_BRs: " + _recvedBR);
			System.out.println("	Total received DATAs: "	+ _recvedDATA);
			System.out.println("	Total received LBRs: " +_recvedLBR);
			System.out.println("	Total removed Neighbors: "+_removedNeighbors); 
			System.out.println("	Total new Neighbors: "+_newNeighbors);
			System.out.println("	Entries in myLBR: " + myLBR.size());
			System.out.println("	Entries in seenMsgs: " + seenMsgs.size());
			System.out.println("	Entries in canceledIDs: " + canceledIDs.size());
			System.out.println("	Entries in buffer: " + buffer.size());
			System.out.println("	Peak in myLBR: " + _myLBRPeak);
			System.out.println("	Peak in seenMsgs: " + _seenMsgsPeak);
			System.out.println("	Peak in canceledIDs: " + _canceledIDsPeak);
			System.out.println("	Peak in buffer: " + _bufferPeak);
			
			
			System.out.println("In the last 100.000 msgs: "); 
			System.out.println("	Total received HELLO_BRs: " + (_recvedBR - _lastrecvedBR));
			System.out.println("	Total received DATAs: "	+ (_recvedDATA - _lastrecvedDATA));
			System.out.println("	Total received LBRs: " + (_recvedLBR - _lastrecvedLBR));
			System.out.println("	Total removed Neighbors: "+ (_removedNeighbors - _lastremovedNeighbors)); 
			System.out.println("	Total new Neighbors: "+ (_newNeighbors - _lastnewNeighbors));
			
			
			
			_lastrecvedBR = _recvedBR;
			_lastrecvedDATA = _recvedDATA;
			_lastrecvedLBR = _recvedLBR;
			_lastremovedNeighbors = _removedNeighbors;
			_lastnewNeighbors = _newNeighbors;
		}
		*/
		
		if(!neighbors.containsKey(msg.getSender()))	//new neighbor discovered?
		{
//			_newNeighbors++;
			sendLBR = true;
		}
		
		neighbors.put(msg.getSender(), Time.getCurrentTime());	//in any case, sender is neighbor
		
		HyperGossipNotification m = (HyperGossipNotification)msg;
		
		if(m instanceof HyperGossipDataNotification)	//M is DATA do gossiping
		{
			HyperGossipDataNotification data = (HyperGossipDataNotification)m;
			//part of rebroadcast - meaning: someone else already rebroadcasts
			if(buffer.keySet().contains(data.getID()))
			{
				canceledIDs.add(data.getID()); //cancel rebroadcast
				Event.scheduleWithDelay(lifeTimeCancel, lifetimeHandler, data, EVENTTYPE_LIFETIMECANCEL);
			}
//			_recvedDATA++;
			gossip(data);
		}
		else if(m instanceof HyperGossipLBRNotification)	//M is HELLO with LBR do partition detection
		{
			partitionDetection((HyperGossipLBRNotification)m);
	//		_recvedLBR++;
		}
		else if(m instanceof HyperGossipBRNotification)	// M is HELLO with BR do reBroadcasting
		{
			Event.scheduleWithDelay((long)(rnd.nextDouble()*rDelay), rebroadcastHandler, (HyperGossipBRNotification)m, EVENTTYPE_REBROADCAST);
//			_recvedBR++;
		}
		return true;
	}
	
	private void partitionDetection(HyperGossipLBRNotification m){
		HashSet<Integer> recvLBR = new HashSet<Integer>(m.getLBRIds());
		recvLBR.retainAll(myLBR);
		
		double is;
		
		if(myLBR.size() > 0)
			is = recvLBR.size() / myLBR.size();	// |myLBR n recvLBR| / |myLBR|
		else
			is = IS_threshold+1;
		
		if(is <= IS_threshold)					//partition detected? -> send BR
		{
			// a partition join was detected! send the received broadcast IDs
			send(new HyperGossipBRNotification(getLocalOverlayContact(), new LinkedHashSet<Integer>(buffer.keySet())));
		}

	}
	
	private void gossip(HyperGossipDataNotification m){
		if(!seenMsgs.contains(m.getID()))	//received for the first time
		{
			seenMsgs.add(m.getID());
			if(m.getRemainingLifeTime() > 0)
				Event.scheduleWithDelay(m.getRemainingLifeTime(), lifetimeHandler, m, EVENTTYPE_LIFETIMEMSG);
			else
				Event.scheduleImmediately(lifetimeHandler, m, EVENTTYPE_LIFETIMEMSG);
			buffer.put(m.getID(), m);
			
			if(myLBR.size() >= maxLBRlength)
				myLBR.remove(0);
			myLBR.add(m.getID());
			
			Event.scheduleWithDelay(msgOldTime, oldMessageHandler, m.getID(), EVENTTYPE_OLDMSG);	// forget about very old messages eventually
			
			// notify own node
			boolean relevant = getComponent().handleLocalNotification(
					m.getNotification());
			if (isSubjectToGeofencing(m.getNotification()) && !relevant) {
				return;
			}
			
			if(rnd.nextDouble() <= 1/Math.sqrt(neighbors.size()))	//
				Event.scheduleWithDelay((long)(rnd.nextDouble()* fDelay), gossipHandler, m, EVENTTYPE_GOSSIP);
		}
	}
	
	@Override
	public void startMechanism(Callback cb) {
		if (!beaconRunning) {
			Event.scheduleImmediately(beaconHandler, null, EVENTTYPE_BEACON);
			beaconRunning = true;
		}
		super.startMechanism(cb);
	}
}
