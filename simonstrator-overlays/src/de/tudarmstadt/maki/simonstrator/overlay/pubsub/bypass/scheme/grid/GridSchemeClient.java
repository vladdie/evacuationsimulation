package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationUpdateMsg;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;

/**
 * A location-based subscription scheme where clients are grouped in grid cells
 * based on their location. Cells are defined by squares using two points
 * (minimum bounding rectangle). Clients update their location to the server
 * when they move out of the bounds. Notifications are send to all clients
 * inside a cell.
 * 
 * @author Julian Zobel
 */
public class GridSchemeClient extends AbstractLocationBasedSubscriptionScheme.Client {

	/**
	 * The {@link BoundingRectangle} this client is currently subscribed to.
	 */
	private BoundingRectangle mbr;

	@TransferState({ "Component" })
	public GridSchemeClient(BypassPubSubComponent component) {
		super(component, SchemeName.GRID);
	}

	@Override
	public boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {

		if (msg instanceof GridCellUpdateMsg) {
			/*
			 * Invoked on client, if server sends a grid update message
			 */
			GridCellUpdateMsg gMsg = (GridCellUpdateMsg) msg;
			if (gMsg.getCellMBR() == null) {
				/*
				 * received GridCellUpdateMessage with empty mbr = request for
				 * current location: Send LocationUpdateMessage with current
				 * location.
				 */
				getComponent().sendViaMobileNetwork(new LocationUpdateMsg(getComponent().getLocalOverlayContact(),
						getComponent().getBrokerContact(), getCurrentLocation(), getName()));
			} else {
				/*
				 * Change MBR to the new received MBR in the
				 * GridCellUpdateMessage
				 */
				mbr = gMsg.getCellMBR();
			}
		} else {
			return false;
		}
		return true;
	}

	@Override
	public Notification createLocalNotification(Topic topic, List<Attribute<?>> attributes, double radiusOfInterest,
			byte[] payload) {
		if (mbr != null) {
			return new ChannelBasedNotification(topic, attributes, payload, getCurrentLocation(), radiusOfInterest,
					mbr.getChannelName());
		} else {
			return new LocationBasedNotification(topic, attributes, payload, getCurrentLocation(), radiusOfInterest,
					true);
		}
	}

	/**
	 * Send an update message to our broker, carrying the new location. This
	 * method is invoked periodically on a host. The host only sends his new
	 * position, if he has moved outside of the MBR. In this case, he has to
	 * send his current position to the broker in order for the broker to update
	 * its state.
	 * 
	 * @param location
	 * @param component
	 */
	@Override
	protected void onLocationChanged(Location newLocation, Location oldLocation, long timeSinceLastUpdate,
			double distanceSinceLastUpdate) {
		if (mbr == null || !mbr.contains(newLocation.getLatitude(), newLocation.getLongitude())) {
			sendLocationUpdateMessage();
		}
	}

	public BoundingRectangle getMbr() {
		return mbr;
	}

	public void setMbr(BoundingRectangle mbr) {
		this.mbr = mbr;
	}

}
