package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.LocalDissemination;

/**
 * Just relays messages to a {@link LocalDissemination}.
 * 
 * TODO switch between Unicast/Broadcast? Use a second proxy?
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class StatelessGatewayBehavior extends AbstractLocalGatewayBehavior {

	private LocalDissemination gatewayDissemination = null;

	@TransferState(value = { "Component" })
	public StatelessGatewayBehavior(BypassClientComponent comp) {
		super(comp, LocalGatewayBehavior.GatewayBehavior.STATELESS);
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		// getComponent().getLocalDistribution().notify(n, subscribers);
		gatewayDissemination.notify(n, subscribers);
	}

	@Override
	public void startMechanism(Callback cb) {
		/*
		 * Try to retrieve a proxy for the local gateway dissemination.
		 */
		TransitionEngine tEngine = getComponent().getTransitionEngine();
		if (!tEngine.proxyExists(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION)) {
			throw new AssertionError(
					"This GatewayBehavior requires a PROXY_LOCAL_GATEWAY_DISSEMINATION in the Transition Engine.");
		}
		gatewayDissemination = tEngine.getProxy(BypassPubSubComponent.PROXY_LOCAL_GATEWAY_DISSEMINATION,
					LocalDissemination.class);
		super.startMechanism(cb);
	}

	@Override
	public void stopMechanism(Callback cb) {
		gatewayDissemination = null;
		super.stopMechanism(cb);
	}

}
