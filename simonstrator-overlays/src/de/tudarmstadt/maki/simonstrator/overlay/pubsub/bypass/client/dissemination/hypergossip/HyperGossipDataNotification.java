package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;

public class HyperGossipDataNotification extends HyperGossipNotification{

	private static final long serialVersionUID = 1L;
	private final int id;
	private final Notification n;
	private long lifeTime;
	private long creationTime;
	
	public HyperGossipDataNotification(OverlayContact sender, int id, Notification n, long lifeTime) {
		super(sender);
		this.id = id;
		this.n = n;
		this.lifeTime = lifeTime;
		creationTime = Time.getCurrentTime();
	}
	
	@Override
	public long getSize() {
		return super.getSize() + 4 + n.getTransmissionSize() + 8 + 8;
	}
	
	public int getID(){
		return id;
	}
	
	public Notification getNotification(){
		return n;
	}

	public long getRemainingLifeTime(){
		return creationTime + lifeTime - Time.getCurrentTime();
	}
}
