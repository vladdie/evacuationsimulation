package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;

public class PlanBWrapMsg extends DefaultLocalMessage implements
	BypassLocalMessage {

	private static final long serialVersionUID = 1L;

	private PlanBNotificationMsg planBNotificationMsg;
	private INodeID parentID; // Integer since it may be null
	private Double proximity;								//Double since it my be null
	private Location senderLocation;
	
	public PlanBWrapMsg(OverlayContact sender, int msgId, PlanBNotificationMsg planBMsg, INodeID parentID,
			Double proximity, Location senderLocation) {
		super(sender, null);

		this.planBNotificationMsg = planBMsg;
		this.parentID = parentID;
		this.proximity = proximity;
		this.senderLocation = senderLocation;
	}
	
	
	
	public Location getSenderLocation(){
		return senderLocation;
	}
	
	@Override
	public long getSize() {
		return super.getSize()+ planBNotificationMsg.getSize()  + 4;
	}

	@Override
	public String toString() {
		return "PlanB-WrapNotify " +  " from " + getSender().toString();
	}

	public PlanBNotificationMsg getNotificationMsg(){
		return planBNotificationMsg;
	}
	
	public INodeID getParentID() {
		return parentID;
	}
	
	public Double getProximity(){
		return proximity;
	}
}
