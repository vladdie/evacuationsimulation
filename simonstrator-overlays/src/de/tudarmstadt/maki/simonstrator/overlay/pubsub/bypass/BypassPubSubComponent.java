package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.common.IDSpace;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.AnalyzerNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.DefaultIDSpace;
import de.tudarmstadt.maki.simonstrator.overlay.api.IOverlayMessageAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.AbstractPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.BypassMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.BypassMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.LocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.broadcast.BroadcastDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.flooding.FloodingDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.gossip.GossipDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip.HyperGossipDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.none.NoLocalDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb.PlanBDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.priogossip.PriogossipDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.unicast.UnicastDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme;

/**
 * The base class for Bypass components (running on cloud infrastructure and
 * mobile devices).
 * 
 * Bypass... BYörns PAblish Subscribe System :)
 * 
 * @author Bjoern Richerzhagen
 * @version May 2, 2014
 */
public abstract class BypassPubSubComponent extends AbstractPubSubComponent implements TransMessageListener {

	/**
	 * Update: no longer define ports per scheme. We just run one local
	 * dissemination on one port and transfer the port as a state variable. This
	 * enables us to create multiple proxies.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public enum LocalDistributionType {
		NONE(NoLocalDistribution.class), UNICAST(UnicastDissemination.class), BROADCAST(
				BroadcastDissemination.class), FLOODING(FloodingDistribution.class), GOSSIP(
						GossipDistribution.class), PLANB(PlanBDistribution.class), HYPERGOSSIP(
								HyperGossipDistribution.class), PRIOGOSSIP(PriogossipDistribution.class);

		public Class<? extends LocalDissemination> clazz;

		private LocalDistributionType(Class<? extends LocalDissemination> clazz) {
			this.clazz = clazz;
		}
	}
	
	/**
	 * We might want to add other types later.
	 *
	 * @author Bjoern Richerzhagen
	 *
	 */
	public enum NodeType {
		/**
		 * As implemented by {@link BypassClientComponent}
		 */
		CLIENT,
		/**
		 * TODO implement the BypassCloudletComponent!
		 */
		CLOUDLET,
		/**
		 * As implemented by {@link BypassCloudComponent}
		 */
		CLOUD;
	}

	private final NodeType nodeType;

	/**
	 * Allows other components in Bypass to register as message listeners.
	 */
	private List<BypassMessageListener> messageListeners = new LinkedList<>();

	/**
	 * Transport protocol to brokers via the mobile network
	 */
	private MessageBasedTransport mobileTransport;

	/**
	 * Mobile transport interface: cloud to clients, clients to cloudlets
	 */
	public static NetInterfaceName netTypeMobile = NetInterfaceName.MOBILE;

	// bit of cheating to augment bootstrapping

	/**
	 * Address of the broker (cloud or cloudlet) that is responsible for us
	 */
	private OverlayContact brokerContact = null;

	/**
	 * True, if no cloud was configured and Bypass is supposed to run in pure
	 * adhoc mode.
	 */
	private boolean adhocMode = false;

	private final String serverIP;

	public static IDSpace idSpace = new DefaultIDSpace(16);

	private final int cloudPort;

	protected final int cloudPortListen;

	private static IOverlayMessageAnalyzer _messageAnalyzer = null;

	private static boolean _messageAnalyzerInit = false;

	private final BasicOverlayContact ownContact;

	/**
	 * Convenience shortcut to the TransitionEngine
	 */
	private TransitionEngine tEngine = null;

	/*
	 * Proxy names for the TransitionEngine
	 */

	public static final String PROXY_LOCAL_DISSEMINATION = "BypassLocalDissemination";

	public static final String PROXY_LOCAL_PHY = "BypassLocalPhy";

	public static final String PROXY_LOCAL_GATEWAY = "BypassLocalGateway";

	public static final String PROXY_SUBSCRIPTION_SCHEME = "BypassSubScheme";

	public static final String PROXY_SUBSCRIPTION_SCHEME_BROKER = "BypassSubSchemeBroker";

	public static final String PROXY_UPLOAD_SCHEME = "BypassUpload";

	public static final String PROXY_LOCAL_GATEWAY_DISSEMINATION = "BypassLocalGatewayDissemination";

	/**
	 * For simulation deployments - the cloud contact is obtained from global
	 * knowledge
	 * 
	 * @param host
	 * @param peerId
	 * @param nodeType
	 */
	public BypassPubSubComponent(Host host, NodeType nodeType) {
		this(host, nodeType, null, 6400, 6400);
	}

	/**
	 * For real-world deployments
	 * 
	 * @param host
	 * @param peerId
	 * @param cloudIP
	 * @param cloudPort
	 *            port the cloud listens on
	 * @param cloudPortListen
	 *            port we use to receive messages from the cloud
	 */
	public BypassPubSubComponent(Host host, NodeType nodeType,
			String cloudIP, int cloudPort, int cloudPortListen) {
		super(host);
		this.nodeType = nodeType;
		this.cloudPort = cloudPort;
		this.cloudPortListen = cloudPortListen;
		this.serverIP = cloudIP;
		// Create OverlayContact
		ownContact = new BasicOverlayContact(getHost().getId());
	}

	@Override
	public void initialize() {
		super.initialize();

		try {
			tEngine = getHost().getComponent(TransitionEngine.class);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("A TransitionEngine is required for Bypass.");
		}

		NetInterface mobile = getHost().getNetworkComponent().getByName(netTypeMobile);
		try {
			if (mobile == null) {
				throw new ProtocolNotAvailableException();
			}
			mobileTransport = getAndBindUDP(mobile.getLocalInetAddress(),
					cloudPortListen, new BypassSerializer());
			mobileTransport.setTransportMessageListener(this);
			// Add cloud transport to our own trans info
			ownContact.addTransInfo(netTypeMobile, mobileTransport.getTransInfo());
		} catch (ProtocolNotAvailableException e) {
			adhocMode = true;
			Monitor.log(BypassPubSubComponent.class, Level.WARN,
					"No mobile data interface intiated. Ad Hoc-only mode of Bypass.");
		}
	}
	
	/**
	 * Add a message listener that is notified of incoming messages
	 * 
	 * @param listener
	 */
	public void registerMessageListener(BypassMessageListener listener) {
		if (!messageListeners.contains(listener)) {
			messageListeners.add(listener);
		}
	}

	/**
	 * Remove a message listener
	 * 
	 * @param toRemove
	 */
	public void removeMessageListener(BypassMessageListener toRemove) {
		messageListeners.remove(toRemove);
	}

	/**
	 * Type of this node
	 * 
	 * @return
	 */
	public NodeType getNodeType() {
		return nodeType;
	}

	/**
	 * Convenience pointer to the transition engine
	 * 
	 * @return
	 */
	public TransitionEngine getTransitionEngine() {
		return tEngine;
	}

	/**
	 * Getter for the transport protocol used to communicate via the mobile
	 * network (cellular) with a broker.
	 * 
	 * @return
	 */
	public MessageBasedTransport getMobileTransport() {
		return mobileTransport;
	}

	/**
	 * Easy access to currently active subscription scheme's name
	 * 
	 * @return
	 */
	public abstract SubscriptionScheme getSubscriptionScheme();

	/**
	 * Send the given message via the MOBILE interface. The recipient and sender
	 * are included in the {@link OverlayMessage}
	 * 
	 * @param m
	 *            message to be sent, including the receiver.
	 */
	public void sendViaMobileNetwork(OverlayMessage m) {
		assert m instanceof BypassMessage;
		// Only unicasts
		assert m.getReceiver() != null && m.getSender() != null;
		assert m.getReceiver().equals(getLocalOverlayContact()) || m.getSender().equals(getLocalOverlayContact());
		mobileTransport.send(m, m.getReceiver().getNetID(netTypeMobile), m.getReceiver().getPort(netTypeMobile));
		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onSentOverlayMessage(m, getHost(), netTypeMobile);
		}
	}

	@Override
	public final void messageArrived(Message msg, TransInfo sender, int commID) {
		assert msg instanceof BypassMessage;

		if (hasMessageAnalyzer()) {
			getMessageAnalyzer().onReceivedOverlayMessage((OverlayMessage) msg, getHost());
		}
		/*
		 * Check, if a message listener takes care of the message
		 */
		for (BypassMessageListener messageListener : messageListeners) {
			if (messageListener.messageArrived((BypassMessage) msg, sender, commID)) {
				// exclusive message
				return;
			}
		}
		/*
		 * Relay to component
		 */
		handleIncomingMessage((BypassMessage) msg, sender, commID);

	}

	/**
	 * Relay messages (incoming) to extending classes.
	 * 
	 * @param msg
	 * @param sender
	 */
	protected abstract void handleIncomingMessage(BypassMessage msg, TransInfo sender, int commID);

	@Override
	public BasicOverlayContact getLocalOverlayContact() {
		return ownContact;
	}

	/**
	 * Returns the server's overlay contact.
	 * 
	 * @return
	 */
	public OverlayContact getBrokerContact() {
		if (isInAdhocMode()) {
			Monitor.log(BypassPubSubComponent.class, Level.WARN,
					"Tried to access Broker Contact, but Bypass is running in adhoc-only mode.");
			return null;
		}
		if (brokerContact == null) {
			if (Oracle.isSimulation()) {
				for (Host host : Oracle.getAllHosts()) {
					try {
						BypassCloudComponent cloud = host
								.getComponent(BypassCloudComponent.class);
						brokerContact = cloud.getLocalOverlayContact();
					} catch (ComponentNotAvailableException e) {
						// don't care
					}
				}
				if (brokerContact == null) {
					throw new AssertionError("Cloud not found.");
				}
			} else {
				brokerContact = new BasicOverlayContact(INodeID.get(-1), mobileTransport.getNetInterface().getName(),
						mobileTransport.getNetInterface().getByName(serverIP), cloudPort);
			}
		}
		return brokerContact;
	}

	/**
	 * True, if the node operates in pure AdHoc mode (no cloud component was
	 * initialized)
	 * 
	 * @return
	 */
	public boolean isInAdhocMode() {
		return adhocMode;
	}

	/**
	 * Sets a new broker for the current node. The broker is the one reached by
	 * calls to "sendViaMobileNetwork".
	 * 
	 * @param brokerContact
	 *            The OverlayContact corresponding to the new overlay contact.
	 */
	public void setBrokerContact(OverlayContact brokerContact) {
		this.brokerContact = brokerContact;
	}

	@Override
	public void advertise(Filter filter) {
		throw new AssertionError("Not supported!");
	}

	@Override
	public void unadvertise(Filter filter) {
		throw new AssertionError("Not supported!");
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// don't care
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		// don't care
	}

	@Override
	public void startComponent() {
		// Called by a churn generator (app-level)
		this.setPeerStatus(PeerStatus.PRESENT);
	}

	@Override
	public void stopComponent() {
		// Called by a churn generator (app-level)
		this.setPeerStatus(PeerStatus.ABSENT);
	}

	/*
	 * Analyzers
	 */

	public IOverlayMessageAnalyzer getMessageAnalyzer() {
		return _messageAnalyzer;
	}

	public boolean hasMessageAnalyzer() {
		if (!_messageAnalyzerInit) {
			try {
				_messageAnalyzer = Monitor.get(IOverlayMessageAnalyzer.class);
			} catch (AnalyzerNotAvailableException e) {
				//
			}
			_messageAnalyzerInit = true;
		}
		return _messageAnalyzer != null;
	}

}
