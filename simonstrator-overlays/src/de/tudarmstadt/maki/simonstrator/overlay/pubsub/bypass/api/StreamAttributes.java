/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of Simonstrator.KOM.
 * 
 * Simonstrator.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;

/**
 * A simple registry of {@link Attribute} names that are used for streaming via a
 * pub/sub system.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public final class StreamAttributes {

	/**
	 * The root topic of streams
	 */
	public final static String TOPIC_ROOT = "/stream/";

	/**
	 * Numerical (integer) attribute used to denote the current chunk id within
	 * the stream.
	 */
	public final static String CHUNK_ID = "chk-id";

	/**
	 * Numerical (short) attribute for the block id within the current chunk
	 */
	public final static String BLOCK_ID = "blk-id";

	/**
	 * Boolean attribute: is this block required for playback (HAS to arrive at
	 * the viewer)?
	 */
	public final static String BLOCK_REQUIRED = "blk-req";

	/**
	 * Numerical (long) attribute containing the ID of the source
	 */
	public final static String SRC_ID = "src-id";
	
}
