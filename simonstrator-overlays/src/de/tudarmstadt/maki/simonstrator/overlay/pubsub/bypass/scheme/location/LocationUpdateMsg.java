package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.BypassCloudMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;

/**
 * A message carrying the new location of a mobile node. An update of the Radius
 * of Interest has to be modeled with a re-subscription (as this is correct
 * application-layer behavior)
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class LocationUpdateMsg extends AbstractOverlayMessage implements BypassCloudMessage, BypassSchemeMessage {

	private static final long serialVersionUID = 1L;

	private Location location;

	private SchemeName schemeName;

	public LocationUpdateMsg(OverlayContact sender, OverlayContact receiver, Location location,
			SchemeName schemeName) {
		super(sender, receiver);
		this.schemeName = schemeName;
		this.location = location.clone();
	}

	@Override
	public Message getPayload() {
		return null;
	}

	public Location getLocation() {
		return location;
	}

	@Override
	public SchemeName getSchemeName() {
		return schemeName;
	}

	@Override
	public long getSize() {
		return super.getSize() + location.getTransmissionSize() + Byte.BYTES;
	}

}
