package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb;

public class PolicyProvider {

	private final float Z = 1.0f;
	private final int K_s = 2;
	private final int K_p = 2;

	public float getZ(){
		return Z;
	}
	
	public int getK_s(){
		return K_s;
	}
	
	public int getK_p(){
		return K_p;
	}
}
