package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme;

import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction.AttractionSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction.AttractionSchemeClient;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.basic.BasicSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid.ExtGridSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid.ExtGridSchemeClient;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridSchemeClient;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSchemeClient;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.multi.MultiSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste.SpaceTimeEnvelopeSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste.SpaceTimeEnvelopeSchemeClient;

/**
 * Interface to the subscription scheme component. Performs matching and
 * annotations of notifications and subscriptions.
 * 
 * TODO: include switching pt / matrix as attribute in Notifications?
 * 
 * TODO: can we use TransitionEnabled and the BypassModule without adding
 * another layer of conflicts? Maybe there could lateron be a transition-enabled
 * {@link SubscriptionScheme}.
 * 
 * TODO: a real TransitionEnabled Scheme needs to update the subscriptions
 * stored at brokers as well (for bundling). Maybe we need to alter the
 * util-containers accordingly to enable such behavior. For now, the respective
 * implementation of a transition would have to delete and re-create the storage
 * scheme. Take care of identifiers, as we need to delete subscriptions even if
 * the client speaks a different semantic (based on subscription-ID?)
 * 
 * TODO for the aforementioned changes, it might make sense to switch the
 * subscription storage on brokers to a subscription-centric scheme rather than
 * using the current, client-centric scheme.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface SubscriptionScheme extends TransitionEnabled {

	/**
	 * Identifier of a specific {@link SubscriptionScheme}.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public enum SchemeName {

		/**
		 * A simple, attribute-based scheme (not location-based)
		 */
		BASIC(BasicSubscriptionScheme.class, BasicSubscriptionScheme.class),

		/**
		 * A parametric, location-based scheme
		 */
		LBS(LocationBasedSchemeBroker.class, LocationBasedSchemeClient.class),

		/**
		 * Single-cell grid-based scheme
		 */
		GRID(GridSchemeBroker.class, GridSchemeClient.class),

		/**
		 * Space-time-envelopes
		 */
		STE(SpaceTimeEnvelopeSchemeBroker.class, SpaceTimeEnvelopeSchemeClient.class),

		/**
		 * Multi-cell grid-based scheme
		 */
		EXT_GRID(ExtGridSchemeBroker.class, ExtGridSchemeClient.class),

		/**
		 * Multi-cell grid-based scheme w. rectangular ROI matching
		 */
		EXT_GRID_RECT(ExtGridSchemeBroker.class, ExtGridSchemeClient.class),

		/**
		 * Channel-based scheme using attraction points
		 */
		ATTRACTION(AttractionSchemeBroker.class, AttractionSchemeClient.class),

		/**
		 * Broker-scheme supporting multiple client schemes in one scenario.
		 * This scheme only defines a broker implementation, the client one is
		 * one of the other available {@link SubscriptionScheme}s
		 */
		MULTI(MultiSchemeBroker.class, LocationBasedSchemeClient.class);


		public final Class<? extends BrokerSubscriptionScheme> brokerClass;

		public final Class<? extends ClientSubscriptionScheme> clientClass;

		private SchemeName(Class<? extends BrokerSubscriptionScheme> brokerClass,
				Class<? extends ClientSubscriptionScheme> clientClass) {
			this.brokerClass = brokerClass;
			this.clientClass = clientClass;
		}

	}

	/**
	 * Name of the Scheme
	 * 
	 * @return
	 */
	public SchemeName getName();

}
