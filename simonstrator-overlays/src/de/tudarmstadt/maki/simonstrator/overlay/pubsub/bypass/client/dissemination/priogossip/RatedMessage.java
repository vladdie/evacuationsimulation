package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.priogossip;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.priogossip.PriogossipMessage.messageType;

/**
 * Placeholder class for a rated message inside a List
 * 
 * @author Julian Zobel
 *
 */
public class RatedMessage {
	private final PriogossipMessage message;
	private final int rating;
	private long timeDelay = 0;

	public RatedMessage(PriogossipMessage message, int rating) {
		this.message = message;
		this.rating = rating;
	}

	public RatedMessage(PriogossipMessage message, int rating, long timeDelay) {
		this.message = message;
		this.rating = rating;
		this.timeDelay = timeDelay;
	}

	public PriogossipMessage getMessage() {
		return message;
	}

	public int getRating() {
		return rating;
	}

	public void setTimeDelay(long timeDelay) {
		this.timeDelay = timeDelay;
	}

	public long getTimeDelay() {
		return timeDelay;
	}

	public messageType getType() {
		return message.getType();
	}

}
