package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.basic;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassFactory;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.NodeType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BrokerSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ClientSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.SubscriptionState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.SubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.SubscriptionStorage_Old;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.SubscriptionStorage_Old.StateCreationCallback;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicSubscription;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Default implementation of a subscription scheme, using the provided scheme in
 * pubsub-utils. No extra sauce here, just relying on the application-layer semantics.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class BasicSubscriptionScheme
		implements ClientSubscriptionScheme, BrokerSubscriptionScheme,
		StateCreationCallback<SubscriptionState> {

	@TransferState({ "Component" })
	private BypassPubSubComponent comp;

	private SubscriptionStorage_Old<SubscriptionState> storage;

	@TransferState({ "Component" })
	public BasicSubscriptionScheme(BypassPubSubComponent component) {
		this.comp = component;
	}

	public BypassPubSubComponent getComponent() {
		return comp;
	}

	@Override
	public SchemeName getName() {
		return SchemeName.BASIC;
	}

	@Override
	public SubscriptionStorage getStorage() {
		throw new UnsupportedOperationException();
	}

	@Override
	public BasicNotification createNotification(Topic topic,
			List<Attribute<?>> attributes, byte[] payload) {
		return new BasicNotification(topic, attributes, payload);
	}

	@Override
	public Notification createNotification(Topic topic, List<Attribute<?>> attributes, Location location,
			double radiusOfInterest, byte[] payload) {
		/*
		 * In the basic scheme, we just return a basic notification and do not
		 * provide means to filter based on the location.
		 */
		return createNotification(topic, attributes, payload);
	}

	@Override
	public Notification createLocalNotification(Topic topic, List<Attribute<?>> attributes, double radiusOfInterest,
			byte[] payload) {
		/*
		 * In the basic scheme, we just return a basic notification and do not
		 * provide means to filter based on the location.
		 */
		return createNotification(topic, attributes, payload);
	}

	@Override
	public BasicSubscription createSubscription(Topic topic, Filter filter) {
		return new BasicSubscription(topic, filter);
	}

	@Override
	public BasicSubscription createSubscription(Topic topic, Filter filter,
			LocationRequest locationRequest, double radiusOfInterest) {
		/*
		 * In the basic scheme, we do not filter locations (however, using the
		 * "magic filters", the application might still do that).
		 */
		return createSubscription(topic, filter);
	}

	@Override
	public void onSubscribeAtBroker(Subscription sub, OverlayContact subscriber) {
		/*
		 * Broker-Side storage of subscriptions
		 */
		assert comp.getNodeType() != NodeType.CLIENT : "Only allowed on brokers!";
		storage.addSubscription(sub, subscriber);
	}

	@Override
	public boolean onSubscribeAtClient(Subscription sub, PubSubListener listener) {
		/*
		 * Local storage of subscriptions and listeners. Usually invoked on
		 * clients, but brokers are also allowed to subscribe.
		 */
		storage.addSubscription(sub, listener);
		return true;
	}

	@Override
	public void onUnsubscribeAtBroker(Subscription sub, OverlayContact subscriber) {
		storage.unsubscribe(sub, subscriber);
	}

	@Override
	public boolean onUnsubscribeAtClient(Subscription sub, PubSubListener listener) {
		storage.unsubscribe(sub, null);
		// not supporting multi-listeners
		return true;
	}

	@Override
	public OverlayContacts getSubscribers() {
		OverlayContacts subscribers = new OverlayContacts();

		LinkedHashSet<SubscriptionState> subs = storage
				.getRemoteSubscriptions();
		for (SubscriptionState sub : subs) {
			if (sub.isExpired()) {
				// TODO cleanup!
				continue;
			}
			subscribers.addAll(sub.getClients());
		}
		return subscribers;
	}

	@Override
	public OverlayContacts getSubscribers(Notification notification) {
		/*
		 * Here, we perform the matching, relying on the default implementation
		 * of the overlay component.
		 */
		OverlayContacts subscribers = new OverlayContacts();
		
		LinkedHashSet<SubscriptionState> subs = storage
				.getRemoteSubscriptions();
		for (SubscriptionState sub : subs) {
			if (sub.isExpired()) {
				// TODO cleanup!
				continue;
			}
			if (comp.matches(notification, sub.subscription)) {
				subscribers.addAll(sub.getClients());
			}
		}
		return subscribers;
	}

	@Override
	public boolean notifySelf(Notification notification) {
		LinkedHashSet<SubscriptionState> subs = storage.getLocalSubscriptions();
		boolean matched = false;
		for (SubscriptionState sub : subs) {
			if (comp.matches(notification, sub.subscription)) {
				sub.notifyLocalListeners(notification);
				matched = true;
			}
		}
		return matched;
	}

	@Override
	public SubscriptionState createSubscriptionState(Subscription sub) {
		if (BypassFactory.REMOTE_SUBSCRIPTION_EXPIRES_AFTER != -1) {
			return new SubscriptionState(sub,
					BypassFactory.REMOTE_SUBSCRIPTION_EXPIRES_AFTER);
		} else {
			return new SubscriptionState(sub);
		}
	}

	@Override
	public Set<Subscription> getSubscriptionsFrom(OverlayContact subscriber) {
		LinkedHashSet<Subscription> subs = new LinkedHashSet<Subscription>();
		for (SubscriptionState sub : storage.getRemoteSubscriptions()) {
			if (sub.getClients().contains(subscriber)) {
				subs.add(sub.subscription);
			}
		}
		return subs;
	}

	@Override
	public Map<Subscription, Set<PubSubListener>> getLocalSubscriptions() {
		LinkedHashMap<Subscription, Set<PubSubListener>> subs = new LinkedHashMap<>();
		for (SubscriptionState sub : storage.getLocalSubscriptions()) {
			subs.put(sub.subscription, sub.getLocalListeners());
		}
		return subs;
	}

	/*
	 * Lifecycle methods for the TransitionEnabled Component
	 */

	@Override
	public void startMechanism(Callback cb) {
		storage = new SubscriptionStorage_Old<SubscriptionState>(this);
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		storage = null;
		cb.finished(true);
	}

}
