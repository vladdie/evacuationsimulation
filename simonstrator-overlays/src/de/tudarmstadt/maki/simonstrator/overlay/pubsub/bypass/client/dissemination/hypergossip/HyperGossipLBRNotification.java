package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip;

import java.util.LinkedHashSet;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;

public class HyperGossipLBRNotification extends HyperGossipSetNotification{

	private static final long serialVersionUID = 1L;

	public HyperGossipLBRNotification(OverlayContact sender, LinkedHashSet<Integer> ids) {
		super(sender, ids);
	}
	
	public LinkedHashSet<Integer> getLBRIds(){
		return ids;
	}

}
