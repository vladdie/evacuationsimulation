package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSType;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyGatewayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * For testing: just picks one node out of the list of subscribers and uses that
 * node as a gateway.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class SimpleGatewayLogic implements BypassGatewayLogic, EventHandler {

	private BypassPubSubComponent comp;

	private SiSComponent sis;

	private boolean triggeredRawObservations = false;

	private final static int EVENT_RECALCULATE_GATEWAY = 1;

	private final static long RECALCULATE_GATEWAY_INTERVAL = 20 * Time.SECOND;

	private final static int NUMBER_OF_GATEWAYS = 10;
	

	public SimpleGatewayLogic(BypassPubSubComponent comp) {
		this.comp = comp;
		try {
			sis = comp.getHost().getComponent(SiSComponent.class);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("SiS is required.");
		}
		Event.scheduleWithDelay(RECALCULATE_GATEWAY_INTERVAL, this, null,
				EVENT_RECALCULATE_GATEWAY);
	}

	@Override
	public BypassPubSubComponent getComponent() {
		return comp;
	}

	@Override
	public Collection<CloudNotifyMessage> packMessages(Notification n,
			OverlayContacts subscribers, int numberOfGateways) {
		Iterator<OverlayContact> it = subscribers.iterator();
		OverlayContact gateway = it.next();
		it.remove();

		SiSType<Double> type = SiSTypes.getType(
				"Delta_SizeBypassCloudMessageReceive", Double.class);

		if (!triggeredRawObservations) {
			sis.get().rawObservations(type, SiSRequest.NONE, null);
			triggeredRawObservations = true;
		}
		// SiS Test
		double observationGateway;
		try {
			observationGateway = sis.get().localObservationOf(gateway.getNodeID(),
					type, SiSRequest.NONE);
			System.out.println("Observation CloudMessageReceive: "
					+ observationGateway);

		} catch (InformationNotAvailableException e) {
			// Metric not (yet) available
		}

		List<CloudNotifyMessage> messages = new LinkedList<>();
		if (subscribers.isEmpty()) {
			// only one subscriber
			messages.add(new CloudNotifyMessage(comp.getLocalOverlayContact(),
					gateway, n));
		} else {
			messages.add(new CloudNotifyGatewayMessage(comp
					.getLocalOverlayContact(), gateway, n, subscribers, true));
		}
		return messages;
	}

	protected void recalculateGateway() {
		SiSType<Double> type = SiSTypes.getType(
				"Delta_SizeBypassCloudMessageReceive", Double.class);
		Map<INodeID, Double> trafficIn = sis.get().allLocalObservations(
				type, SiSRequest.NONE);
		List<Map.Entry<INodeID, Double>> toSort = new LinkedList<>(
				trafficIn.entrySet());
		Collections.sort(toSort, new Comparator<Map.Entry<INodeID, Double>>() {

			@Override
			public int compare(Entry<INodeID, Double> o1,
					Entry<INodeID, Double> o2) {
				return (int) (o1.getValue() - o2.getValue());
			}
		});
		toSort.subList(0, NUMBER_OF_GATEWAYS);
	}

	@Override
	public void eventOccurred(Object content, int type) {
		switch (type) {
		case EVENT_RECALCULATE_GATEWAY:
			recalculateGateway();
			Event.scheduleWithDelay(RECALCULATE_GATEWAY_INTERVAL, this, null,
					EVENT_RECALCULATE_GATEWAY);
			break;

		default:
			throw new AssertionError();
		}
	}

}
