package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.gossip;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;

public class GossipNotificationMsg extends DefaultLocalMessage implements
		BypassLocalMessage {

	private static final long serialVersionUID = 1L;

	private int msgId;

	private Notification n;

	private int hops;

	public GossipNotificationMsg(OverlayContact sender, Notification n,
			int msgId) {
		super(sender, null);
		this.n = n;
		this.msgId = msgId;
		this.hops = 0;
	}

	public GossipNotificationMsg(OverlayContact sender,
			GossipNotificationMsg toForward) {
		super(sender, null, toForward);
		this.n = toForward.n;
		this.msgId = toForward.msgId;
		this.hops = toForward.hops + 1;
	}

	public int getHops() {
		return hops;
	}

	@Override
	public long getSize() {
		return super.getSize() + n.getTransmissionSize() + 4;
	}

	public int getMsgId() {
		return msgId;
	}

	public Notification getNotification() {
		return n;
	}

	@Override
	public String toString() {
		return "Gossip [" + msgId + "] hops:" + hops + " from " + getSender() + " with " + n.toString();
	}

}
