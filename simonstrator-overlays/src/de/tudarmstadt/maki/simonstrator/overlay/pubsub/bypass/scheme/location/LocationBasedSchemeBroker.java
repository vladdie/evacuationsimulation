package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location;

import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;

/**
 * Broker-side implementation of the LBS-Scheme, where clients subscribe to a
 * circular area of interest around their current position. The last known
 * position of a client is used by the broker to filter for relevant
 * subscriptions.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class LocationBasedSchemeBroker extends AbstractLocationBasedSubscriptionScheme.Broker {

	/**
	 * Server/Broker: storage that contains all client subscriptions.
	 */
	private LocationBasedSubscriptionStorage storage;

	/**
	 * Constructor. Initializes the remote storage (if the node acts as a
	 * broker).
	 * 
	 * @param component
	 */
	@TransferState({ "Component" })
	public LocationBasedSchemeBroker(BypassCloudComponent component) {
		super(component, SchemeName.LBS);
		this.storage = new LocationBasedSubscriptionStorage(component);
	}

	@Override
	public LocationBasedSubscriptionStorage getStorage() {
		return storage;
	}

	@Override
	public boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {
		if (msg instanceof LocationUpdateMsg) {
			LocationUpdateMsg umsg = (LocationUpdateMsg) msg;
			storage.updateLocationOfClient(umsg.getSender(), umsg.getLocation());
		} else {
			return false;
		}
		return true;
	}

}
