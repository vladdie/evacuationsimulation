package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;

/**
 * Interface for a local distribution strategy. The {@link LocalDissemination}
 * communicates with the main component via the
 * {@link LocalDistributionCallback}.
 * 
 * @author Bjoern Richerzhagen
 * @version May 3, 2014
 */
public interface LocalDissemination extends TransitionEnabled {
	
	/**
	 * 
	 * @return
	 */
	public LocalDistributionType getLocalDistributionType();

	/**
	 * Called by the main component, which in turn was triggered by the
	 * application. The notification is to be distributed according to the rules
	 * of the respective strategy. This is only called on the originator-node.
	 * Message forwarding will result in calls to the {@link LocalMsgHandler}
	 * method onReceive.
	 * 
	 * @param n
	 *            the notification originating at this node
	 * @param subscribers
	 *            an (optional) hint containing contacts of subscribers.
	 *            Strategies might use it (or not). Can be null.
	 */
	public void notify(Notification n, Collection<OverlayContact> subscribers);

	/**
	 * A local dissemination strategy might also rely on the dissemination of
	 * subscriptions instead or besides the dissemination of notifications.
	 * Therefore, this method is invoked whenever a node subscribes (i.e., the
	 * application issued a subscribe() call).
	 * 
	 * @param sub
	 *            might need to be clone()'ed before being sent to other nodes.
	 */
	public void subscribe(Subscription sub);

	/**
	 * The main component calls the onReceive method as soon as local messages
	 * arrive. Beware: during transitions, messages of a different type than the
	 * locally active dissemination scheme might arrive.
	 * 
	 * @return
	 */
	public void onReceive(BypassLocalMessage msg, int commID);

}
