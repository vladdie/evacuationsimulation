package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid;

import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.BypassCloudMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;

/**
 * A message carrying the information of a grid cell
 * 
 * @author Julian Zobel
 *
 */
public class ExtGridCellUpdateMsg extends AbstractOverlayMessage
		implements BypassCloudMessage, BypassSchemeMessage {

	private static final long serialVersionUID = 1L;

	private Set<BoundingRectangle> otherMbrs;

	private BoundingRectangle mainMbr;

	private SchemeName schemeName;

	public ExtGridCellUpdateMsg(OverlayContact sender, OverlayContact receiver, BoundingRectangle mainMbr,
			Set<BoundingRectangle> otherMbrs, SchemeName schemeName) {
		super(sender, receiver);
		this.mainMbr = mainMbr;
		this.otherMbrs = otherMbrs;
		this.schemeName = schemeName;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public SchemeName getSchemeName() {
		return schemeName;
	}

	public Set<BoundingRectangle> getOtherMbrs() {
		return otherMbrs;
	}

	public BoundingRectangle getMainMbr() {
		return mainMbr;
	}

	@Override
	public long getSize() {
		return super.getSize() + (mainMbr != null ? mainMbr.getTransmissionSize() * (otherMbrs.size() + 1) : 0) + 1 + Byte.BYTES;
	}

	@Override
	public String toString() {
		return "ExtCellUpdMsg: " + getSender().getNodeID() + " -> " + getReceiver().getNodeID() + " : " + mainMbr + " "
				+ otherMbrs;
	}

}
