package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.AttributeFilter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.handover.HandoverSensor;
import de.tudarmstadt.maki.simonstrator.api.component.service.DiscoveryService;
import de.tudarmstadt.maki.simonstrator.api.component.service.DiscoveryService.DiscoveryListener;
import de.tudarmstadt.maki.simonstrator.api.component.service.DiscoveryService.DiscoveryRequest;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.StreamAttributes;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.AbstractUploadStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.GKRelayStrategy.ForwardNotificationMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring.MonitoredAttribute;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring.MonitoringAttributes;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring.MonitoringSource;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.schedule.FilterBasedSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.schedule.Schedule;
import de.tudarmstadt.maki.simonstrator.service.discovery.BroadcastDiscoveryService;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * TODO
 * TODO rename to DirectorAssistedUpload
 * @author Julian Wulfheide
 */
public class DirectorAssistedUploadStrategy extends AbstractUploadStrategy implements TransMessageListener, PubSubListener, HostComponent {

    public final static int PORT = 27643;
    private HandoverSensor handoverSensor;

    private PubSubComponent pubSub;
    private Subscription sub;

    private Topic ATTR_TOPIC_CONTROL;
    private Attribute ATTR_CLIENT_ID;

    /**
     * This nodes schedule, which has been sent over by the director.
     */
    private Schedule schedule;

    /**
     * The monitoring component. This gathers all needed information and sends it to the director.
     */
    private MonitoringSource monitor;

    // TODO get generics up in here and fill list
    private Set<OverlayContact> neighbors = new LinkedHashSet<>();

    private DiscoveryService discovery;
    private DiscoveryRequest request;

    private int numDirectUpload = 0;
    private int numForward = 0;

    @TransferState({"Component"})
    public DirectorAssistedUploadStrategy(BypassClientComponent comp) {
        super(comp);

        discovery = new BroadcastDiscoveryService(comp.getHost());
        request = discovery.createRequest("clustering", NetworkComponent.NetInterfaceName.WIFI, 27081, true);
        request.setDiscoveryInterval(1000 * Time.MILLISECOND);
    }

    @Override
    public void startMechanism(Callback cb) {
        getComponent().getLocalTransport().setTransMessageListener(this, PORT);
        getHost().registerComponent(this);

        /**
         * Start discovery
         */
        try {
            // TODO this is unstable (service gets lost and found again several times)
            discovery.startDiscovery(request, new DiscoveryListener() {
                @Override
                public void onServiceFound(String serviceName, Collection<OverlayContact> serviceEndpoints) {
                    assert neighbors.isEmpty();
                    neighbors.addAll(serviceEndpoints);
                }

                @Override
                public void onServiceUpdate(String serviceName, Collection<OverlayContact> serviceEndpoints) {
                    assert !neighbors.isEmpty();
                    neighbors.addAll(serviceEndpoints);
                    neighbors.removeIf(o -> !serviceEndpoints.contains(o));
                }

                @Override
                public void onServiceLost(String serviceName) {
                    assert !neighbors.isEmpty();
                    neighbors.clear();
                }
            });
        } catch (ProtocolNotAvailableException e) {
            throw new AssertionError("Need protocol for discovery.");
        }

        /**
         * Start monitoring
         */
        this.monitor = new MonitoringSource(getComponent(), this);
        monitor.startMonitoring();

        /**
         * Subscribe to "/control/<client_id>" (a filter is used instead of a subtopic)
         */
        try {
            long clientId = getComponent().getHost().getId().value();
            pubSub = getComponent().getHost().getComponent(PubSubComponent.class);
            ATTR_TOPIC_CONTROL = pubSub.createTopic(ControlAttributes.TOPIC_ROOT_CONTROL);
            ATTR_CLIENT_ID = pubSub.createAttribute(Long.class, ControlAttributes.CLIENT_ID, clientId);

            List<AttributeFilter<?>> filters = new LinkedList<>();
            filters.add(pubSub.createAttributeFilter(ATTR_CLIENT_ID, FilterOperator.EQ));

            sub = pubSub.createSubscription(ATTR_TOPIC_CONTROL, pubSub.createFilter(filters));
            pubSub.subscribe(sub, this);

            try {
                handoverSensor = pubSub.getHost().getComponent(HandoverSensor.class);
            } catch (ComponentNotAvailableException e) {
            }

        } catch (ComponentNotAvailableException e) {
            throw new AssertionError("Needs a PubSubComponent.");
        }

        super.startMechanism(cb);
    }

    @Override
    public void stopMechanism(Callback cb) {
        neighbors.clear();
        monitor.stopMonitoring();
        getComponent().getLocalTransport().removeTransMessageListener(PORT);
        getHost().removeComponent(this);
        super.stopMechanism(cb);
    }

    @Override
    public void uploadNotification(Notification n) {
        if (StreamAttributes.TOPIC_ROOT.equals(n.getTopic().getValue())) {
            // Chunk notification -> upload according to upload strategy

            List<Long> uploaders = null;
            boolean directUpload = false;
            boolean discard = false;

            if (schedule != null) {
                // Query the schedule for the current chunk
                uploaders = schedule.getUploaders(getComponent(), n, getNeighborIDs());
                if (uploaders != null) {
                    // Only keep those uploaders that this node currently knows
                    uploaders.retainAll(getNeighborIDs());

                    if (uploaders.isEmpty()) {
                        // No uploaders, either because none have been selected by the UploaderSelection or because
                        // all selected uploaders are currently unknown to the source.
                        directUpload = true;
                    }
                } else {
                    // No uploader list in schedule -> Don't upload this chunk
                    discard = true;
                }
            } else {
                // Not schedule has been set -> direct upload
                directUpload = true;
            }

            if (!discard) {
                if (isConnectedToAP() || directUpload) {
                    System.out.println("(" + getComponent().getHost().getId() + ") DIRECT upload of " + n);
                    numDirectUpload++;
                    doUpload(n);
                } else {
                    numForward++;
                    forwardLocally(n, uploaders);
                }
            }

        } else if (MonitoringAttributes.TOPIC_ROOT.equals(n.getTopic().getValue())) {
            // This is a monitor notification -> Direct upload to cloud
            doUpload(n);
        }
    }

    private void forwardLocally(Notification notification, List<Long> uploaders) {
        assert !uploaders.isEmpty();
        for (Long uploaderId : uploaders) {
            OverlayContact relay = getNeighbor(uploaderId);
            assert relay != null; // Should never be null because all unknown neighbors have already been removed from the uploader list
            System.out.println("(" + getComponent().getHost().getId() + ") FORWARDING " + notification + " locally to " + relay);

            ForwardNotificationMessage fw = new ForwardNotificationMessage(getComponent().getLocalOverlayContact(), relay, notification);
            sendLocally(fw, PORT);
        }
    }

    @Override
    public void messageArrived(Message msg, TransInfo sender, int commID) {
        assert msg instanceof ForwardNotificationMessage;

        if (getComponent().hasMessageAnalyzer()) {
            getComponent().getMessageAnalyzer().onReceivedOverlayMessage((ForwardNotificationMessage) msg, getComponent().getHost());
        }

        // Upload to cloud
        doUpload(((ForwardNotificationMessage) msg).getNotification());
    }

    @Override
    public void onNotificationArrived(Subscription matchedSubscription, Notification n) {
        // TODO extract "schedule" Attribute?
        schedule = n.getAttribute("schedule", Schedule.class).getValue();

        if (schedule != null) {
            System.out.println("DirectorAssistedRelay (" + getComponent().getHost().getId() + ") received new schedule:");
            System.out.println(((FilterBasedSchedule) schedule).prettyPrint());
        } else {
            System.out.println("DirectorAssistedRelay (" + getComponent().getHost().getId() + ") deleted last schedule.");
        }
    }

    public List<Long> getNeighborIDs() {
        return getNeighbors().stream()
                .map(neighbor -> neighbor.getNodeID().value())
                .collect(Collectors.toList());
    }

    private OverlayContact getNeighbor(long clientID) {
        for (OverlayContact o : getNeighbors()) {
            if (o.getNodeID().value() == clientID) {
                return o;
            }
        }
        return null;
    }

    private boolean isConnectedToAP() {
        return handoverSensor != null && handoverSensor.isConnectedToAccessPoint();
    }

    public Collection<OverlayContact> getNeighbors() {
        return neighbors;
    }

    public int getNumDirectUpload() {
        return numDirectUpload;
    }

    public int getNumForwarded() {
        return numForward;
    }

    public void addMonitoredAttribute(MonitoredAttribute attribute) {
        monitor.addMonitoredAttribute(attribute);
    }

    public void removeMonitoredAttribute(MonitoredAttribute attribute) {
        monitor.removeMonitoredAttribute(attribute);
    }

    @Override
    public void initialize() {
        // TODO?
    }

    @Override
    public void shutdown() {
        // TODO?
    }

    @Override
    public Host getHost() {
        return getComponent().getHost();
    }

}
