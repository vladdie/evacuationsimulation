package de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute.filter;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;

/**
 * Provides the implemenation of a single {@link FilterOperator}.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public interface IFilterOperator {

	/**
	 * 
	 * @param filter
	 * @param candidate
	 * @return
	 */
	public boolean matches(Attribute<?> filter, Attribute<?> candidate);

}
