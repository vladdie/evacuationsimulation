package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;

/**
 * TODO
 * @author Julian Wulfheide
 */
public abstract class PollingAttribute<T> extends MonitoredAttribute<T> {
    private final PeriodicOperation op;
    private long pollingInterval = Time.SECOND;

    public PollingAttribute(HostComponent comp, Attribute<T> attribute, AttributeListener listener) {
        super(attribute, listener);

        op = new PeriodicOperation(comp, null, pollingInterval) {
            @Override
            protected void executeOnce() {
                T newValue = getValue();
                assert newValue != null;
                // First condition is for the very first update (oldValue == null && newValue == something)
                if ((value == null && newValue != value) || !newValue.equals(value)) {
                    update(newValue);
                }
            }

            @Override
            public Void getResult() {
                return null;
            }
        };
        op.scheduleImmediately();
    }

    protected void setup() {
        // Default implementation because not every PollingAttribute needs setup
    }

    public abstract T getValue();

    public void stopMonitoring() {
        op.stop();
    }

}
