package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.core.MonitorComponent.Analyzer;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme;

/**
 * Analyzer to assess the complexity and performance of subscription schemes on
 * a per-notification basis. This is implemented in the simrunner-project to
 * track number and type of matching operations that are triggered for a given
 * notification.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface BypassSubscriptionSchemeAnalyzer extends Analyzer {

	/**
	 * This is called ONCE when a notification is matched within a
	 * {@link SubscriptionScheme} on a node acting as a broker.
	 * 
	 * @param host
	 * @param n
	 * @param scheme
	 *            currently active scheme (allows us to assess the complexity of
	 *            each match-call)
	 * @param numberOfSubscriptions
	 *            the number of subscriptions this notification was matched
	 *            against (with simple data structures: all)
	 */
	public void onMatchNotification(Host host, Notification n, SubscriptionScheme.SchemeName scheme,
			int numberOfSubscriptions);

}
