package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicSubscription;

/**
 * A subscription that is valid for an area around the location of a client. To
 * update the RoI, a new subscription is to be issued (we consider this a usual
 * attribute, not a contextual variable). Therefore, the RoI is part of hash()
 * and equals() to distinguish individual subscriptions.
 * 
 * Update: we do only include the ROI and NOT the current location, as we simply
 * assume this information will be updated by an out-of-band context protocol
 * (as is currently the case) and will be managed centrally for each client by
 * the broker, not on a per-subscription state. Note, that server-side behavior
 * might need to deal with clients without associated locations for some
 * (hopefully very short) time.
 * 
 * This class should serve as base for other (extended) types of
 * LBS-subscriptions.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class LocationBasedSubscription extends BasicSubscription {

	private static final long serialVersionUID = 1L;

	private double radiusOfInterest;

	private transient LocationRequest locationRequest;

	/**
	 * Creation of location-based subscriptions at the client. The subscription
	 * carries a {@link LocationRequest} that is used to determine the frequence
	 * of location updates needed by the application.
	 * 
	 * The subscription is valid around the current position of the client, with
	 * the given radius of interest (in meters).
	 * 
	 * @param topic
	 * @param filter
	 * @param locationRequest
	 * @param radiusOfInterest
	 *            radius of interest in meters
	 */
	public LocationBasedSubscription(Topic topic, Filter filter, LocationRequest locationRequest,
			double radiusOfInterest) {
		super(topic, filter);
		this.locationRequest = locationRequest;
		this.radiusOfInterest = radiusOfInterest;
	}

	public LocationBasedSubscription(LocationBasedSubscription toClone) {
		super(toClone);
		this.radiusOfInterest = toClone.radiusOfInterest;
	}

	/**
	 * "Special" attribute that described the radius of interest (circle) around
	 * the current location of the node that this subscription covers.
	 * 
	 * @return
	 */
	public double getRadiusOfInterest() {
		return radiusOfInterest;
	}

	/**
	 * Only available at the client that issued this subscription, used to
	 * request periodic updates. This is not transmitted to the broker.
	 * 
	 * @return
	 */
	public LocationRequest getLocationRequest() {
		return locationRequest;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(radiusOfInterest);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		LocationBasedSubscription other = (LocationBasedSubscription) obj;
		if (Double.doubleToLongBits(radiusOfInterest) != Double.doubleToLongBits(other.radiusOfInterest))
			return false;
		return true;
	}

	@Override
	public LocationBasedSubscription clone() {
		return new LocationBasedSubscription(this);
	}

	@Override
	public int getTransmissionSize() {
		int size = super.getTransmissionSize();
		size += Double.BYTES; // double value for RoI
		return size;
	}

}
