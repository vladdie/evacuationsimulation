package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;

public class PlanBNotificationMsg extends DefaultLocalMessage implements
		BypassLocalMessage {

	private static final long serialVersionUID = 1L;

	private final Notification n;

	private final int msgId;

	public PlanBNotificationMsg(OverlayContact sender, Notification n, int msgId) {
		super(sender, null);
		this.n = n;
		this.msgId = msgId;
	}
	
	@Override
	public long getSize() {
		return super.getSize() + n.getTransmissionSize() + 4;
	}

	public int getMsgId() {
		return msgId;
	}

	public Notification getNotification() {
		return n;
	}

	@Override
	public String toString() {
		return "PlanB-Notify " + n._getNotificationInfo(null).getSequenceNumber() + " from " + getSender().toString();
	}

}
