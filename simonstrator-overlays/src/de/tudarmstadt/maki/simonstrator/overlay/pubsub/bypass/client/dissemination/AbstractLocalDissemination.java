package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination;

import java.util.Collection;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.transition.MechanismState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.broadcast.BroadcastDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.flooding.FloodingDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.gossip.GossipDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip.HyperGossipDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.none.NoLocalDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb.PlanBDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.priogossip.PriogossipDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.unicast.UnicastDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.AbstractLocalTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.BypassCloudMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassDisseminationConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;

/**
 * Abstract base class for distribution strategies. Use getComp() to communicate
 * with the main overlay component.
 * 
 * @author Bjoern Richerzhagen
 * @version May 3, 2014
 */
public abstract class AbstractLocalDissemination
		implements LocalDissemination, TransitionEnabled, TransMessageListener {

	protected static Random rnd = Randoms.getRandom(AbstractLocalDissemination.class);

	protected enum DistrState {
		OFF, ON
	}

	/**
	 * Enables the usage of the {@link AbstractLocalTransport} instead of own
	 * protocol instances when sending and receiving messages. true: messages
	 * are sent via our own transport instance within this component. false: the
	 * transition-enabled component is used.
	 */
	public static boolean useOwnTransport = false;

	/**
	 * Direct transport-interaction is only used, if 'useOwnTransport' is set to
	 * true.
	 */
	private MessageBasedTransport transport;

	/**
	 * Direct transport-interaction is only used, if 'useOwnTransport' is set to
	 * true.
	 */
	private static final NetInterfaceName localTransportNetName = NetInterfaceName.WIFI;

	private DistrState currentState = DistrState.OFF;

	/**
	 * Port that is used for communication with this proxy. Transfered during
	 * transitions.
	 */
	@TransferState(value = { "Port" })
	private final int port;

	/**
	 * Counter of transitions of this proxy
	 */
	@MechanismState("NumTransitions")
	@TransferState(value = { "NumTransitions" })
	private int numTransitions;

	/**
	 * True, if this proxy is self-healing if transitions were missed, based on
	 * incoming messages
	 */
	@MechanismState("SelfHeal")
	@TransferState(value = { "SelfHeal" })
	private boolean selfHeal;

	/**
	 * The name of the proxy, needed for self-healing.
	 */
	@MechanismState("ProxyName")
	@TransferState(value = { "ProxyName" })
	private String proxyName;

	/**
	 * Pointer to the {@link BypassClientComponent}, transfered during
	 * transitions.
	 */
	@TransferState(value = { "Component" })
	private BypassClientComponent comp;

	@TransferState(value = { "Component", "Port" })
	public AbstractLocalDissemination(BypassClientComponent comp, int port) {
		this.comp = comp;
		this.port = port;
		this.numTransitions = 0;
	}

	/**
	 * Pointer to the Component
	 * 
	 * @return
	 */
	public BypassClientComponent getComponent() {
		return comp;
	}

	/**
	 * Simple transition counter for this proxy
	 * 
	 * @return
	 */
	public int getNumTransitions() {
		return numTransitions;
	}
	
	/**
	 * For state transfer
	 * 
	 * @return
	 */
	public String getProxyName() {
		return proxyName;
	}

	/**
	 * For state transfer
	 * 
	 * @param proxyName
	 */
	public void setProxyName(String proxyName) {
		this.proxyName = proxyName;
	}

	/**
	 * For state transfer.
	 * 
	 * @return
	 */
	public boolean getSelfHeal() {
		return selfHeal;
	}

	/**
	 * For state transfer
	 * @param selfHeal
	 */
	public void setSelfHeal(boolean selfHeal) {
		this.selfHeal = selfHeal;
	}

	/**
	 * Used during state transfer; increments by one.
	 * 
	 * @param numTransitions
	 */
	public void setNumTransitions(int numTransitions) {
		this.numTransitions = numTransitions + 1;
	}

	/**
	 * Port this scheme listens (and sends) on.
	 * 
	 * @return
	 */
	public int getPort() {
		return port;
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		// default: do nothing
	}

	@Override
	public void subscribe(Subscription sub) {
		// default implementation: do nothing.
	}

	@Override
	public final void messageArrived(Message rawMsg, TransInfo sender, int commID) {
		assert currentState != DistrState.OFF;
		assert rawMsg instanceof BypassLocalMessage;

		if (!comp.isActive()) {
			return;
		}

		BypassLocalMessage msg = (BypassLocalMessage) rawMsg;
		if (msg.getSender().equals(comp.getLocalOverlayContact())) {
			// Prevent self-broadcasts!
			return;
		}
		assert !msg.getSender().equals(comp.getLocalOverlayContact());

		// notify Distribution Protocol
		/*
		 * TODO as we now stay on one port, we might receive invalid messages
		 * during transitions. Deal with it -- e.g., by returning false in this
		 * method and triggering a corresponding analyzer.
		 */
		onReceive(msg, commID);

		// Analyzer
		if (comp.hasMessageAnalyzer()) {
			comp.getMessageAnalyzer().onReceivedOverlayMessage(msg, comp.getHost());
		}
	}

	/**
	 * Return true, if the message is subject to geofencing (has to be enabled
	 * and the notifica
	 * 
	 * @param n
	 * @return
	 */
	protected boolean isSubjectToGeofencing(Notification n) {
		if (BypassDisseminationConfiguration.enableGeofencing) {
			if (n instanceof LocationBasedNotification) {
				if (((LocationBasedNotification) n).isPublisherLocation()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * To be implemented by each Dissemination Strategy, has to handle an
	 * incoming message. If the message is of a type that is not compatible to
	 * the currently used strategy (which might happen during a transition!),
	 * return false!
	 * 
	 * @param msg
	 * @param commID
	 * @return true, if the message has been processed. False, if the message is
	 *         not understood by the current dissemination protocol.
	 */
	protected abstract boolean handleMessage(BypassLocalMessage msg, int commID);

	@Override
	public final void onReceive(BypassLocalMessage msg, int commID) {
		boolean processed = handleMessage(msg, commID);
		if (!processed) {
			/*
			 * TODO analyzer: this message has not been processed, as the
			 * strategies did not match.
			 */
			/*
			 * TODO we COULD trigger a transition as consequence... 
			 * Maybe use some kind of "time since last transition" thingy.
			 */
			assert msg.getDisseminationType() != getLocalDistributionType();
			if (selfHeal && msg.getSenderTransitionCount() > numTransitions) {
				// triggger an immediate local transition
				Event.scheduleImmediately(new EventHandler() {
					@Override
					public void eventOccurred(Object content, int type) {
						getComponent().getTransitionEngine().executeAtomicTransition(proxyName,
								msg.getDisseminationType().clazz);
					}
				}, null, 0);
			}
		}
	}

	/**
	 * Dispatches the message. Depending on the state of
	 * 'useLocalTransportComponent', we dispatch locally (own transport
	 * interface) or by using the transition-enabled
	 * {@link AbstractLocalTransport}.
	 * 
	 * @param m
	 */
	protected void send(OverlayMessage m) {
		assert !(m instanceof BypassCloudMessage);
		assert m instanceof BypassLocalMessage;
		assert m.getSender() != null;

		/*
		 * Bit hackish: implementation of the vector clock / repair mechanism.
		 */
		if (m instanceof DefaultLocalMessage) {
			DefaultLocalMessage dm = (DefaultLocalMessage) m;
			dm.setDisseminationType(getLocalDistributionType());
			dm.setSenderTransitionCount(numTransitions);
		}

		if (useOwnTransport) {
			// local dispatching
			if (m.getReceiver() == null) {
				// broadcast (assuming unified port)
				transport.send(m, transport.getNetInterface().getBroadcastAddress(), getPort());
			} else {
				// unicast
				transport.send(m, m.getReceiver().getNetID(localTransportNetName), getPort());
			}
			if (comp.hasMessageAnalyzer()) {
				comp.getMessageAnalyzer().onSentOverlayMessage(m, comp.getHost(), localTransportNetName);
			}
		} else {
			// dispatching using the transition-enabled trans-component
			assert transport == null;
			comp.getLocalTransport().send(m, getPort(), getPort());
			if (comp.hasMessageAnalyzer()) {
				comp.getMessageAnalyzer().onSentOverlayMessage(m, comp.getHost(),
						comp.getLocalTransport().getUtilizedNetInterfaceName());
			}
		}

	}

	/**
	 * Shortcut to the local overlay contact
	 * 
	 * @return
	 */
	protected OverlayContact getLocalOverlayContact() {
		return comp.getLocalOverlayContact();
	}

	/**
	 * Current state of this component
	 * 
	 * @return
	 */
	protected DistrState getCurrentState() {
		return currentState;
	}

	@Override
	public void startMechanism(Callback cb) {
		if (useOwnTransport) {
			// Bind transport and create overlay contact
			if (getLocalDistributionType() != LocalDistributionType.NONE) {
				NetInterface wifi = comp.getHost().getNetworkComponent().getByName(localTransportNetName);
				try {
					transport = comp.getAndBindUDP(wifi.getLocalInetAddress(), getPort(), null);
					transport.setTransportMessageListener(this);
					comp.getLocalOverlayContact().addTransInfo(localTransportNetName, transport.getTransInfo());
				} catch (ProtocolNotAvailableException e) {
					cb.finished(false);
					throw new AssertionError("Whaaaaa!");
				}
			}
		} else {
			// Remove old TransMessageListener (from previous strategy)
			comp.getLocalTransport().removeTransMessageListener(getPort());
			if (getLocalDistributionType() != LocalDistributionType.NONE) {
				// Add self as TransMessageListener
				comp.getLocalTransport().setTransMessageListener(this, getPort());
			}
		}

		currentState = DistrState.ON;
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		assert currentState != DistrState.OFF;

		if (useOwnTransport) {
			// Unbind protocol
			if (transport != null) {
				transport.removeTransportMessageListener();
				transport = null;
			}
		}
		comp = null;

		currentState = DistrState.OFF;
		cb.finished(true);
	}

	protected int getMsgId() {
		return rnd.nextInt();
	}

	/**
	 * Returns the implementing class of a distribution type.
	 * 
	 * @param distributionType
	 * @return
	 */
	public static AbstractLocalDissemination getInstanceFor(LocalDistributionType distributionType,
			BypassClientComponent comp, int port) {
		switch (distributionType) {
		case BROADCAST:
			return new BroadcastDissemination(comp, port);

		case UNICAST:
			return new UnicastDissemination(comp, port);

		case GOSSIP:
			return new GossipDistribution(comp, port);

		case HYPERGOSSIP:
			return new HyperGossipDistribution(comp, port);

		case PRIOGOSSIP:
			return new PriogossipDistribution(comp, port);

		case PLANB:
			return new PlanBDistribution(comp, port);

		case NONE:
			return new NoLocalDistribution(comp, port);

		case FLOODING:
			return new FloodingDistribution(comp, port);

		default:
			throw new AssertionError();
		}
	}

}
