package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.unicast;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;

/**
 * A message sent by the {@link UnicastDissemination} scheme.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class UnicastNotificationMsg extends DefaultLocalMessage implements BypassLocalMessage {

	private static final long serialVersionUID = 1L;

	private Notification n;

	public UnicastNotificationMsg(OverlayContact sender, OverlayContact receiver, Notification n) {
		super(sender, receiver);
		this.n = n;
	}

	public Notification getNotification() {
		return n;
	}

	@Override
	public long getSize() {
		return super.getSize() + n.getTransmissionSize();
	}
}
