package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid;

import java.util.LinkedHashSet;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.ChannelBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;

/**
 * A location-based subscription scheme where clients are grouped in grid cells
 * based on their location. Cells are defined by squares using two points
 * (minimum bounding rectangle). Clients update their location to the server
 * when they move out of the bounds. Notifications are send to all clients
 * inside a cell.
 * 
 * FIXME (as follow-on-work, not to be done by Julian): currently, the
 * subscriptions themselves are only stored by clients and not at the broker
 * (the broker just performs location matching). We might want to change that to
 * include at least topic/attribute filtering on the broker! This is also true
 * for the Grid-Scheme
 * 
 * @author Julian Zobel
 *
 */
public class ExtGridSchemeClient extends AbstractLocationBasedSubscriptionScheme.Client {

	/**
	 * All rectangles we are currently subscribed to.
	 */
	private LinkedHashSet<BoundingRectangle> subscribedMBRs;

	/**
	 * The rectangle containing our current position.
	 */
	private BoundingRectangle coreMBR;

	@TransferState({ "Component" })
	public ExtGridSchemeClient(BypassPubSubComponent component) {
		super(component,
				BypassSubscriptionSchemeConfiguration.rectangularAoi ? SchemeName.EXT_GRID_RECT : SchemeName.EXT_GRID);
		subscribedMBRs = new LinkedHashSet<>();
	}

	@Override
	public boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {
		/*
		 * Invoked on a broker, if a client sent a location update message.
		 */
		if (msg instanceof ExtGridCellUpdateMsg) {
			// Server sent updated grids
			ExtGridCellUpdateMsg gMsg = (ExtGridCellUpdateMsg) msg;
			/*
			 * received GridCellUpdateMessage with empty mbr => request for
			 * current location: Send LocationUpdateMessage with current
			 * location.
			 */
			if (gMsg.getMainMbr() == null) {
				System.out.println("Empty GUM: Send ExtLocUpdMsg");
				sendLocationUpdateMessage();
			} else {
				/*
				 * Store updated MBRs
				 */
				subscribedMBRs.clear();
				coreMBR = gMsg.getMainMbr();
				subscribedMBRs.add(gMsg.getMainMbr());
				subscribedMBRs.addAll(gMsg.getOtherMbrs());
			}
		} else {
			/*
			 * We also get all other messages (cloud publish), that are NOT
			 * intended for this listener. Return false.
			 */
			return false;
		}
		return true;
	}

	@Override
	public Notification createLocalNotification(Topic topic, List<Attribute<?>> attributes, double radiusOfInterest,
			byte[] payload) {
		if (coreMBR != null) {
			return new ChannelBasedNotification(topic, attributes, payload, getCurrentLocation(), radiusOfInterest,
					coreMBR.getChannelName());
		} else {
			return new LocationBasedNotification(topic, attributes, payload, getCurrentLocation(), radiusOfInterest,
					true);
		}
	}

	/**
	 * Returns current radius of interest
	 * 
	 * @return
	 */
	protected double getRadiusOfInterest() {
		double roi = 0;
		for (LocationBasedSubscription lbs : getLocalLBSSubscriptions()) {
			if (lbs.getRadiusOfInterest() > roi) {
				roi = lbs.getRadiusOfInterest();
			}
		}
		return roi;
	}

	@Override
	protected void onLocationChanged(Location location, Location oldLocation, long timeSinceLastUpdate,
			double distanceSinceLastUpdate) {
		/*
		 * FIXME quick and dirty hack to check rectangular AOIs
		 */
		if (BypassSubscriptionSchemeConfiguration.rectangularAoi) {
			if (coreMBR == null || !coreMBR.contains(location.getLatitude(), location.getLongitude())) {
				sendLocationUpdateMessage();
			} else {
				/*
				 * The current solution sends position updates every time a
				 * component moved. Suggestion for a later TODO: only send
				 * position updates if the current subscription is not fully
				 * covered by ALL MBRs
				 * 
				 * Within this, we assume that the client is always subscribed
				 * to a full rectangle w/o holes or rounded corners! This is not
				 * the case.
				 */
				BoundingRectangle outer = BoundingRectangle.getOuterBoundingRectangle(subscribedMBRs);
				if (outer.radiusoutside(location.getLatitude(), location.getLongitude(), getRadiusOfInterest())) {
					sendLocationUpdateMessage();
				} else {
					/*
					 * Also send an update, if one MBR is no longer intersected
					 */
					for (BoundingRectangle mbr : subscribedMBRs) {
						if (!mbr.circleIntersection(location.getLatitude(), location.getLongitude(),
								getRadiusOfInterest())) {
							sendLocationUpdateMessage();
							break;
						}
					}
				}
			}
		} else {
			/*
			 * Take Two: Only send updates if we are no longer in the CORE-mbr
			 * or our radius is outside of the core MBR.
			 */
			if (coreMBR == null || !coreMBR.contains(location.getLatitude(), location.getLongitude())) {
				sendLocationUpdateMessage();
			} else if (coreMBR.radiusoutside(location.getLatitude(), location.getLongitude(), getRadiusOfInterest())) {
				sendLocationUpdateMessage();
			}
			// Not outside and no overlap means only the coreMBR is interesting
			// for
			// subscription. Send message to inform server
			else if (subscribedMBRs.size() > 1) {
				sendLocationUpdateMessage();
			}
		}
	}

	public BoundingRectangle getCoreMBR() {
		return coreMBR;
	}

	public void setMbr(BoundingRectangle mbr) {
		subscribedMBRs.clear();
		subscribedMBRs.add(mbr);
		coreMBR = mbr;
	}

}
