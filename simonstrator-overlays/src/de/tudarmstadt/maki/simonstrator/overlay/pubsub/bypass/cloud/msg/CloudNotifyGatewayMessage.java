package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;

/**
 * A notification for a gateway, includes a list of clients that need to be
 * notified by that respective gateway.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class CloudNotifyGatewayMessage extends CloudNotifyMessage {

	private static final long serialVersionUID = 1L;

	private Collection<OverlayContact> subscribers;

	private boolean notifyGateway;

	@SuppressWarnings("unused")
	private CloudNotifyGatewayMessage() {
		// For Kryo
	}

	public CloudNotifyGatewayMessage(OverlayContact sender,
			OverlayContact receiver, Notification n,
			Collection<OverlayContact> subscribers, boolean notifyGateway) {
		super(sender, receiver, n);
		this.subscribers = subscribers;
		this.notifyGateway = notifyGateway;
	}

	/**
	 * The clients that are to be notified by the respective gateway node
	 * 
	 * @return
	 */
	public Collection<OverlayContact> getSubscribers() {
		return subscribers;
	}

	/**
	 * True, if the gateway is also subscribed to the notification
	 * 
	 * @return
	 */
	public boolean isNotifyGateway() {
		return notifyGateway;
	}

	@Override
	public long getSize() {
		int subsSize = 0;
		for (OverlayContact contact : subscribers) {
			subsSize += contact.getTransmissionSize();
		}
		// +1 for boolean
		return super.getSize() + 1 + subsSize;
	}

}
