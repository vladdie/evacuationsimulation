package de.tudarmstadt.maki.simonstrator.overlay.pubsub.util;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.AttributeFilter;

/**
 * A composition of {@link AttributeFilter}s into a single {@link Filter}.
 * Immutable.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class BasicFilter implements Filter {

	private static final long serialVersionUID = 1L;

	private List<AttributeFilter<?>> filters;

	private transient int size_cached = -1;

	private transient int hash_cached = -1;

	@SuppressWarnings("unused")
	private BasicFilter() {
		// for Kryo
	}

	/**
	 * A Filter that is composed of several attribute filters.
	 * 
	 * @param filters
	 */
	public BasicFilter(List<AttributeFilter<?>> filters) {
		assert filters != null;
		if (Oracle.isSimulation()) {
			// Just to aid in debugging...
			this.filters = Collections.unmodifiableList(filters);
		} else {
			this.filters = filters;
		}
	}

	@Override
	public List<AttributeFilter<?>> getAttributeFilters() {
		return Collections.unmodifiableList(filters);
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Filter: {");
		for (AttributeFilter<?> fltr : filters) {
			str.append(fltr);
			str.append('|');
		}
		str.append('}');
		return str.toString();
	}

	@Override
	public int getTransmissionSize() {
		if (size_cached == -1) {
			for (AttributeFilter<?> filter : filters) {
				size_cached += filter.getTransmissionSize();
			}
		}
		return size_cached;
	}

	@Override
	public int hashCode() {
		if (hash_cached == -1) {
			hash_cached = filters.hashCode();
		}
		return hash_cached;
	}

	@Override
	public boolean equals(Object obj) {
		/*
		 * Attention: this is a weak equals - we rely on the cached hash values!
		 */
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicFilter other = (BasicFilter) obj;
		return other.hashCode() == this.hashCode();
	}

	@Override
	public BasicFilter clone() {
		List<AttributeFilter<?>> filtersCloned = new LinkedList<AttributeFilter<?>>();
		for (AttributeFilter<?> filter : filters) {
			filtersCloned.add(filter.clone());
		}
		return new BasicFilter(filtersCloned);
	}

}
