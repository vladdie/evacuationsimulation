package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb;

import java.util.Collection;
import java.util.HashMap;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.PubSubNotificationForwardingAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;

/**
 * 
 * Plan-B protocol. Messages are broadcasted through the network,
 * encouraging nodes that are further apart to rebroadcast,
 * while lowering the chance of nodes close to each other to both broadcast the same message.
 * 
 * @author Lukas Fey
 * @version November 21, 2014
 */
public class PlanBDistribution extends AbstractLocalDissemination{
	public static final int EVENTTYPE_MSG 		= 0;							// sending message
	public static final int EVENTTYPE_FORGET_MSG= 1;							// forget about message [old message id that is no longer relevant]
	
	/** transmission range */
	private float r = 200;

	/** maximal waiting time */
	private long t_max = 25 * Time.MILLISECOND;

	/** contains all information relevant for a message */
	private HashMap<PlanBNotificationMsg, PlanBMessageState> messageStates = new HashMap<>();

	/** provides policies, setting thresholds */
	private PolicyProvider pp = new PolicyProvider();

	
	/** extension: how long to wait after receiving a message for the
	 *  first time and removing it*/
	private long timeUntilRemovingID = 30 * Time.SECOND;

	/** schedules events -> sends or discards messages after a delay*/
	private EventHandler scheduler = new EventHandler() {
		
		@Override
		public void eventOccurred(Object content, int type) {
			//asserts
			assert type == EVENTTYPE_MSG || type == EVENTTYPE_FORGET_MSG;
			
			if (getCurrentState() == DistrState.OFF) {
				return;
			}

			if(type == EVENTTYPE_MSG)
			{
				assert content instanceof PlanBWrapMsg;
				
				PlanBWrapMsg msg = (PlanBWrapMsg)content;
				
				if (!messageStates.get(msg.getNotificationMsg()).getCanceled()) {
					// Forwarding Analyzer: dropped
					if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
						Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onForwardNotification(
								getComponent().getHost(), msg.getNotificationMsg().getNotification(),
								msg.getSender().getNodeID(), 0);
					}
					send(msg);
				} else {
					// Forwarding Analyzer: dropped
					if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
						Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
								getComponent().getHost(), msg.getNotificationMsg().getNotification(),
								msg.getSender().getNodeID(), 0);
					}
				}
			}
			else if(type == EVENTTYPE_FORGET_MSG)
			{
				assert content instanceof PlanBNotificationMsg;
				
				messageStates.remove(content);
			}
		}
	};

	@TransferState(value = { "Component", "Port" })
	public PlanBDistribution(BypassClientComponent comp, int port) {
		super(comp, port);
	}
	
	@Override
	public LocalDistributionType getLocalDistributionType() {
		return LocalDistributionType.PLANB;
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		super.notify(n, subscribers); // trigger analyzers
		PlanBWrapMsg msg = new PlanBWrapMsg(
				getLocalOverlayContact(), 
				getMsgId(), 
				new PlanBNotificationMsg(getLocalOverlayContact(), n, getMsgId()),
				null, 
				null,
				getCurrentLocation());
		send(msg);

		getComponent().handleLocalNotification(n);

		//TODO: add messages that originate from this node to buffer + broadcast lists.. 
		//messageStates.put(msg.getNotificationMsg().getMsgId(), new PlanBMessageState());
	}

	@Override
	protected boolean handleMessage(BypassLocalMessage msg, int commID) {
		assert getCurrentState() != DistrState.OFF;
		if (!(msg instanceof PlanBWrapMsg)) {
			return false;
		}
		assert !msg.getSender().equals(getLocalOverlayContact());
		
		PlanBWrapMsg planBMsg = (PlanBWrapMsg)msg;

		// notify own node
		boolean relevant = getComponent().handleLocalNotification(
				planBMsg.getNotificationMsg().getNotification());
		
		if (isSubjectToGeofencing(planBMsg.getNotificationMsg().getNotification()) && !relevant) {
			// Forwarding Analyzer: dropped
			if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
				Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
						getComponent().getHost(), planBMsg.getNotificationMsg().getNotification(),
						planBMsg.getSender().getNodeID(), 0);
			}
			return true; // true, because the msg was processed by PlanB
		}

		if (!messageStates.containsKey(planBMsg.getNotificationMsg()))
			Event.scheduleWithDelay(timeUntilRemovingID, scheduler, planBMsg.getNotificationMsg(),
					EVENTTYPE_FORGET_MSG);
		//TODO: maybe check if message is in canceled IDs -> don't waste time looking at it
		deliver(planBMsg.getNotificationMsg(), planBMsg.getSender().getNodeID(), planBMsg.getParentID(),
				planBMsg.getProximity(), planBMsg.getSenderLocation());
		//			1: message, 2: commID (sender of message), 3: parent, 4: proximity
		
		return true;
	}
	
	/**
	 * delivers a new PlanBNotificationMsg to the distribution strategy.
	 * Decides weather the message was sent by a parent or a sibling.
	 * 
	 * @param m
	 * 			The PlanBNotificationMsg received
	 * @param srcID
	 * 			id of the sender of the message
	 * @param parentID
	 * 			id of the parent of the message
	 * @param a
	 * 			proximity
	 * @param senderLocation
	 * 			location of the sender of the message
	 */
	protected void deliver(PlanBNotificationMsg m, INodeID srcID, INodeID parentID, Double a, Location senderLocation) {
		
		assert m != null;
		assert m.getNotification() != null;
		
		double prox = getCurrentLocation().distanceTo(senderLocation);
		
		PlanBMessageState msgState = messageStates.get(m);
		if (msgState == null) {
			msgState = new PlanBMessageState();
			messageStates.put(m, msgState);
		}
		
		if (parentID != null && parentID.equals(msgState.getParentID())) {
			siblingReception(m, a, prox, msgState.getProximity());
		} else {
			parentReception(srcID, m, prox);
		}
		
	}
	
	/**
	 * Message received from parent
	 * 
	 * @param srcID
	 * 			id of the sender
	 * @param m
	 * 			received message
	 * @param d
	 * 			proximity
	 */
	protected void parentReception(INodeID srcID, PlanBNotificationMsg m, double d) {

		PlanBMessageState msgState = messageStates.get(m);
		
		if(msgState.getParentCount() == 0)
		{	
			
			//--deliver to application was here--
			msgState.setParentID(srcID);
			msgState.setProximity(d);
			
			if(r > d)
			{
				long delay = (long)(t_max * (r-d)/r);
				
				Event.scheduleWithDelay(delay, scheduler, 
						new PlanBWrapMsg(	getLocalOverlayContact(), 
											m.getMsgId(), 
											m, 
											srcID,		//[parentID]
											d,			//[Proximity]
											getCurrentLocation()),
						EVENTTYPE_MSG);	
				
			}
			else			/* if message was received, even though the sender is out of send range, send message immediately*/
			{
				Event.scheduleImmediately(scheduler, 
						new PlanBWrapMsg(	getLocalOverlayContact(),
								m.getMsgId(),
								m,
								srcID,			//[parentID]
								d,				//[Proximity]
								getCurrentLocation()), 
						EVENTTYPE_MSG);
			}
		}
		
		msgState.increaseParentCount();
		
		if(r - d > pp.getZ() * r || msgState.getParentCount() > pp.getK_p())
			msgState.cancel();
	}
	
	/**
	 * Message received from Sibling
	 * 
	 * @param m
	 * 			received message
	 * @param a
	 * 			distance sender to parent
	 * @param b
	 * 			distance to sender
	 * @param c
	 * 			distance to parent
	 */
	protected void siblingReception(PlanBNotificationMsg m, double a, double b, double c){
		float R = (float)Math.acos( (a*a) / (2*a*r) );						//angle R parent-target-sibling
		float C = (float)Math.acos( (a*a + b*b - c*c) / 2*a*b);				//angle C parent-sibling-receiver
		float D = R - C;													//angle D receiver-sibling-target
		float sigma = (float)Math.sqrt( b*b + r*r - 2*b*r * Math.cos(D));	//receiver - target distance
		
		PlanBMessageState msgState = messageStates.get(m);
		
		msgState.increaseSiblingCount();
		
		if(sigma > pp.getZ() * r || msgState.getSiblingCount() > pp.getK_s())
			msgState.cancel();
	}
	
	private LocationSensor locSensor = null;
	
	/**
	 * Returns the current location of this node.
	 * @return
	 */
	protected Location getCurrentLocation() {
		if (locSensor == null) {
			try {
				locSensor = getComponent().getHost().getComponent(
						LocationSensor.class);
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError(
						"PlanB requires a LocationSensor-Component to determine the node position!");
			}
		}
		/*
		 * FIXME BR: within simulations, this works flawlessly. In the real
		 * world, we might need to update the position more frequently for good
		 * results.
		 */
		return locSensor.getLastLocation();
	}

}
