package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.SubscriptionGridCell;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;

/**
 * Storage for subscription grid cells. Models the grid with extended
 * functionalities: Clients are subscribed to the grid they are in and also the
 * grids their radius of interest overlaps.
 * 
 * Update: now extending {@link GridBasedSubscriptionStorage}, to prevent code
 * duplication if we want to evaluate the impact of different grid construction
 * types.
 * 
 * @author Julian Zobel, Bjoern Richerzhagen
 *
 */
public class ExtGridBasedSubscriptionStorage extends GridBasedSubscriptionStorage {

	private Map<OverlayContact, BoundingRectangle> subscribersMainMbrs;

	public ExtGridBasedSubscriptionStorage(BypassPubSubComponent component, BoundingRectangle mbr) {
		super(component, mbr);

		// Initialize the hash set of grids
		subscribersMainMbrs = new LinkedHashMap<>();
	}

	/**
	 * Send a GridLocationMessage to a specific client/subscriber
	 * 
	 * @param subscriber
	 *            The subscriber that receives the message
	 * @param mbr
	 *            The MBR that defines the grid cell that the
	 *            GridLocationMessage announces
	 * @param otherMbrs
	 *            surrounding MBRs
	 */
	protected void sendGridLocationMessage(OverlayContact subscriber, BoundingRectangle mainMbr,
			Set<BoundingRectangle> otherMbrs) {
		ExtGridCellUpdateMsg msg = new ExtGridCellUpdateMsg(getComponent().getLocalOverlayContact(), subscriber,
				mainMbr, otherMbrs, getComponent().getSubscriptionScheme().getName());
		getComponent().sendViaMobileNetwork(msg);
	}

	@Override
	protected void sendGridLocationMessage(OverlayContact subscriber, BoundingRectangle mbr) {
		/*
		 * Intercept sendGridLocationMessage to ensure the correct message type
		 * is sent.
		 */
		this.sendGridLocationMessage(subscriber, mbr, Collections.emptySet());
	}

	/**
	 * Returns the radius of interest of the given subscriber
	 * 
	 * @param subscriber
	 * @return
	 */
	public double getRadiusOfInterestFor(OverlayContact subscriber) {
		double roi = 0;
		for (LocationBasedSubscription sub : getLocationBasedSubscriptionsBy(subscriber)) {
			if (sub.getRadiusOfInterest() > roi) {
				roi = sub.getRadiusOfInterest();
			}
		}
		return roi;
	}

	@Override
	protected void moveClient(OverlayContact client, Location previousLocation, Location updatedLocation) {
		/*
		 * Implement custom handling. DO NOT call super()!
		 */

		Set<BoundingRectangle> otherMbrs = new LinkedHashSet<>();
		BoundingRectangle mainMbr = subscribersMainMbrs.get(client);
		double radiusOfInterest = getRadiusOfInterestFor(client);
		boolean leftMainMbr = false;
		if (mainMbr == null || !mainMbr.contains(updatedLocation.getLatitude(), updatedLocation.getLongitude())) {
			leftMainMbr = true;
		}

		/*
		 * We first create a list of MBRs and mark one as being the main MBR.
		 * Then, we only send one message to the client.
		 * 
		 * FIXME we need to send an update also if the main MBR changes!
		 */

		boolean alteredSet = leftMainMbr;

		for (SubscriptionGridCell cell : getGridCells()) {
			if (cell.getMBR().circleIntersection(updatedLocation.getLatitude(), updatedLocation.getLongitude(),
					radiusOfInterest)) {
				if (!cell.getSubscribers().contains(client)) {
					cell.addSubscriber(client);
					alteredSet = true;
				}
				if (cell.getMBR().contains(updatedLocation.getLatitude(), updatedLocation.getLongitude())) {
					mainMbr = cell.getMBR();
				} else {
					otherMbrs.add(cell.getMBR());
				}
			} else {
				// Remove from other cells.
				if (cell.getSubscribers().contains(client)) {
					cell.removeSubscriber(client);
					alteredSet = true;
				}
			}
		}

		subscribersMainMbrs.put(client, mainMbr);

		assert mainMbr != null;
		if (alteredSet) {
			sendGridLocationMessage(client, mainMbr, otherMbrs);
		}
	}

	@Override
	protected void onNoLongerLBSSubscriber(OverlayContact subscriber) {
		super.onNoLongerLBSSubscriber(subscriber);
		subscribersMainMbrs.remove(subscriber);
	}

	/**
	 * The main MBR of a subscriber as known to the broker.
	 * 
	 * @param contact
	 * @return
	 */
	public BoundingRectangle getMainMbrForSubscriber(OverlayContact contact) {
		return subscribersMainMbrs.get(contact);
	}

}
