package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.Graph;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * A simple GlobalKnowledge-based strategy that uploads content by relaying
 * chunks to N nearby nodes (round robbin) which then upload the content to the
 * cloud.
 * 
 * The strategy relies on the {@link SiSComponent} to retrieve the current
 * underlay neighbors and their distance.
 * 
 * TODO more sophisticated strategies should rely on monitoring data to consider
 * the achievable bandwidth!
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class GKRelayStrategy extends AbstractUploadStrategy implements TransMessageListener {

	public final static int PORT = 27643;

	public static OverlayContacts otherNodes = new OverlayContacts();

	private List<OverlayContact> activeRelays = new LinkedList<>();

	private SiSComponent sis;

	private final long validityActiveRelays = Time.SECOND;

	private long lastActiveRelayDetermination = -1;

	private int i = 0;

	private int maxNumberOfRelays = 1;

	private boolean enableDirectUpload = false;

	private RelayFunction relayFunction;

	@TransferState({ "Component" })
	public GKRelayStrategy(BypassClientComponent comp) {
		super(comp);
	}

	@Override
	public void startMechanism(Callback cb) {
		getComponent().getLocalTransport().setTransMessageListener(this, PORT);
		otherNodes.add(getComponent().getLocalOverlayContact());

		try {
			sis = getComponent().getHost().getComponent(SiSComponent.class);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("Cannot operate without WiFi neighbors (SiS)");
		}
		super.startMechanism(cb);
	}

	@Override
	public void stopMechanism(Callback cb) {
		getComponent().getLocalTransport().removeTransMessageListener(PORT);
		otherNodes.remove(getComponent().getLocalOverlayContact());
		super.stopMechanism(cb);
	}

	/**
	 * Set the maximum number of relay nodes utilized (default: 1)
	 * 
	 * @param maxNumberOfRelays
	 */
	public void setMaxNumberOfRelays(int maxNumberOfRelays) {
		this.maxNumberOfRelays = maxNumberOfRelays;
	}

	/**
	 * Enable or disable direct upload (cellular), default: disabled.
	 * 
	 * @param enableDirectUpload
	 */
	public void setEnableDirectUpload(boolean enableDirectUpload) {
		this.enableDirectUpload = enableDirectUpload;
	}

	/**
	 * Allows to specify a (more complex) relay function
	 * 
	 * @param relayFunction
	 */
	public void setRelayFunction(RelayFunction relayFunction) {
		this.relayFunction = relayFunction;
	}

	/**
	 * Basic encapsulation of a relay function.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static interface RelayFunction {
		/**
		 * Return the relay OverlayContact out of the active relays, or the
		 * idForDirectUpload (or null, if the msg is to be dropped locally)
		 * 
		 * @param n
		 * @param activeRelays
		 * @param idForDirectUpload
		 * @return
		 */
		public OverlayContact getRelayForNotification(Notification n, List<OverlayContact> activeRelays,
				OverlayContact contact);
	}

	/**
	 * Fills the "activeRelays" list based on the max number of relays to be
	 * used and all available relays. This takes the distance between nodes into
	 * account (relying on the SiS).
	 */
	private void determineActiveRelays() {
		if (lastActiveRelayDetermination + validityActiveRelays > Time.getCurrentTime()) {
			return;
		}
		lastActiveRelayDetermination = Time.getCurrentTime();
		try {
			final INodeID localNodeID = getComponent().getLocalOverlayContact().getNodeID();
			final Graph neighbors = sis.get().localState(SiSTypes.NEIGHBORS_WIFI, SiSRequest.NONE);
			Set<INodeID> neighborIds = neighbors.getNeighbors(localNodeID);
			activeRelays = otherNodes.getListFor(neighborIds);
			Collections.sort(activeRelays, new Comparator<OverlayContact>() {
				@Override
				public int compare(OverlayContact o1, OverlayContact o2) {
					double d1 = neighbors.getEdge(localNodeID, o1.getNodeID()).getProperty(SiSTypes.PHY_DISTANCE);
					double d2 = neighbors.getEdge(localNodeID, o2.getNodeID()).getProperty(SiSTypes.PHY_DISTANCE);
					return Double.compare(d1, d2);
				}
			});
		} catch (InformationNotAvailableException e) {
			throw new AssertionError("Requires WIFI_Neighbors (SiS)!");
		}

	}

	@Override
	public void uploadNotification(Notification n) {
		determineActiveRelays();
		OverlayContact relay = relayFunction.getRelayForNotification(n, activeRelays,
				getComponent().getLocalOverlayContact());
		if (relay == null) {
			// Discard.
			System.out.println("GKRelay: DROPPING locally: " + n);
			return;
		}
		if (relay.equals(getComponent().getLocalOverlayContact())) {
			System.out.println("GKRelay: DIRECT upload of " + n);
			doUpload(n);
		} else {
			System.out.println("GKRelay: FORWARDING " + n + " locally to " + relay);
			ForwardNotificationMessage m = new ForwardNotificationMessage(getComponent().getLocalOverlayContact(), relay, n);
			sendLocally(m, PORT);
		}
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		assert msg instanceof ForwardNotificationMessage;
		// Upload to cloud
		doUpload(((ForwardNotificationMessage) msg).getNotification());
	}

	/**
	 * Simple message used to carry a notification locally.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class ForwardNotificationMessage extends DefaultLocalMessage {

		private static final long serialVersionUID = 1L;

		private Notification notification;

		public ForwardNotificationMessage(OverlayContact sender, OverlayContact receiver, Notification notification) {
			super(sender, receiver);
			this.notification = notification;
		}

		public Notification getNotification() {
			return notification;
		}

		@Override
		public Message getPayload() {
			return null;
		}

		@Override
		public long getSize() {
			return super.getSize() + notification.getTransmissionSize();
		}

	}

}
