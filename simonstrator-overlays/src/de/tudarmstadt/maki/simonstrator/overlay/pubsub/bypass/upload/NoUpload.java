package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;

/**
 * Completely disable upload of notifications to the cloud.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class NoUpload extends AbstractUploadStrategy {

	public NoUpload(BypassClientComponent comp) {
		super(comp);
	}

	@Override
	public void uploadNotification(Notification n) {
		// do nothing.
	}

}
