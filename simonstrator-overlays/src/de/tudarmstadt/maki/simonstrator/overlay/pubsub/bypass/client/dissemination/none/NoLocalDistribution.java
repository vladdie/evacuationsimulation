package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.none;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;

public class NoLocalDistribution extends AbstractLocalDissemination {

	@TransferState(value = { "Component", "Port" })
	public NoLocalDistribution(BypassClientComponent comp, int port) {
		super(comp, port);
	}

	@Override
	public LocalDistributionType getLocalDistributionType() {
		return LocalDistributionType.NONE;
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		// Don't care
		// DO NOT trigger super (analyzer)

		/*
		 * FIXME we do NOT trigger the current node in cases where no local
		 * distribution is used. Reasoning: when clients only act as clients and
		 * not also as brokers, they should not do any filtering on their own
		 * but instead rely on the cloud/cloudlet broker.
		 */
	}

	@Override
	protected boolean handleMessage(BypassLocalMessage msg, int commID) {
		// Don't care
		return true;
	}

}
