package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;

public class HyperGossipBeacon extends HyperGossipNotification implements HGBeacon{

	private static final long serialVersionUID = 1L;

	public HyperGossipBeacon(OverlayContact sender) {
		super(sender);
	}

}
