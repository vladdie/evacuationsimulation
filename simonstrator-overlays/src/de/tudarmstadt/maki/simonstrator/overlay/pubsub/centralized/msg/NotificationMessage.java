package de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;

/**
 * This message forwards a {@link Notification}
 * 
 * @author bjoern
 * 
 */
public class NotificationMessage implements Message {

	private static final long serialVersionUID = 1L;

	private Notification notification;

	@SuppressWarnings("unused")
	private NotificationMessage() {
		// For Kryo
	}

	public NotificationMessage(Notification notification) {
		this.notification = notification;
	}

	@Override
	public long getSize() {
		return notification.getTransmissionSize();
	}

	@Override
	public Message getPayload() {
		return null;
	}

	public Notification getNotification() {
		return notification;
	}

	@Override
	public String toString() {
		return "Notification: " + notification.toString();
	}

}
