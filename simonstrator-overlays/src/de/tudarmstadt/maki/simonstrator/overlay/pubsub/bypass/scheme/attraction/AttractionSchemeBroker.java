package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction;

import java.util.LinkedHashSet;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationActuator;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationUpdateMsg;

/**
 * Location based subscription scheme using {@link SubscriptionBubble}s around
 * attraction points for the publication matching.
 * 
 * @author Julian Zobel, Bjoern Richerzhagen
 *
 */
public class AttractionSchemeBroker extends AbstractLocationBasedSubscriptionScheme.Broker {

	/**
	 * Storage for {@link SubscriptionBubble}s, that are placed around a
	 * attraction points with given radius.
	 */
	private AttractionSubscriptionStorage storage;

	/**
	 * Default radius of subscriptions around an attraction point is 100. Can be
	 * configured via the {@link BypassSubscriptionSchemeConfiguration}.
	 */
	public static double attractionPointROI = -1;

	@TransferState({ "Component" })
	public AttractionSchemeBroker(BypassCloudComponent component) {
		super(component, SchemeName.ATTRACTION);
		storage = new AttractionSubscriptionStorage(super.getComponent());
	}

	@Override
	public void startMechanism(Callback cb) {
		super.startMechanism(cb);
		/*
		 * We use global knowledge to retrieve the list of AttractionPoints from
		 * one of the clients.
		 */
		Set<AttractionPoint> attractionPoints = new LinkedHashSet<>();
		for (Host h : Oracle.getAllHosts()) {
			try {
				// check, if this is a client
				h.getComponent(BypassClientComponent.class);
				// get the location actuator
				LocationActuator actuator = h.getComponent(LocationActuator.class);
				/*
				 * FIXME for now, we simply assign each AP a radius of 100. This should
				 * later be done by the application or workload. We leave this sysout in
				 * place as a reminder...
				 */
				for (AttractionPoint ap : actuator.getAllAttractionPoints()) {
					if (attractionPointROI != -1) {
						System.out.println(
								"BypassScheme: Setting AP-Radius of " + ap.getName() + " to " + attractionPointROI + ".");
						ap.setRadius(attractionPointROI);
						attractionPoints.add(ap);
					} else {
						if (ap.getRadius() > 0) {
							/*
							 * Only include APs as channels with a radius > 0
							 */
							attractionPoints.add(ap);
						}
					}
				}
				// we only need to do this once.
				break;
			} catch (ComponentNotAvailableException e) {
				// don't care
			} catch (UnsupportedOperationException e) {
				// mobility models that do not have any APS
			}
		}

		
		storage.addAttractionPoints(attractionPoints);

	}

	/**
	 * Distribute the set of current {@link AttractionPoint}s. Used if the set
	 * of attraction points changes or as part of a transition.
	 */
	public void distributeAttractionPoints(Set<OverlayContact> to) {
		for (OverlayContact subscriber : to) {
			getComponent().sendViaMobileNetwork(new AttractionAnnounceMsg(getComponent().getLocalOverlayContact(),
					subscriber, storage.getAttractionPoints()));
		}
	}

	/**
	 * Distribute the set of current {@link AttractionPoint}s. Used if the set
	 * of attraction points changes or as part of a transition.
	 */
	public void distributeAttractionPoints(OverlayContact to) {
		if (storage.getAttractionPoints().isEmpty()) {
			throw new AssertionError("Scheme used without any AttractionPoints.");
		}
		getComponent().sendViaMobileNetwork(
				new AttractionAnnounceMsg(getComponent().getLocalOverlayContact(), to, storage.getAttractionPoints()));
	}

	/**
	 * Adds an attraction point and creates the respective channel during
	 * operation (interesting for demos or self-learning approaches...).
	 * 
	 * @param ap
	 */
	public void addAttractionPoints(Set<AttractionPoint> ap) {
		storage.addAttractionPoints(ap);
		distributeAttractionPoints(storage.getLBSSubscribers());
	}

	/**
	 * Removes an attraction point and the associated channel. (interesting for
	 * demos or self-learning approaches...).
	 * 
	 * @param ap
	 */
	public void removeAttractionPoints(Set<AttractionPoint> ap) {
		storage.removeAttractionPoints(ap);
		distributeAttractionPoints(storage.getLBSSubscribers());
	}

	@Override
	public AttractionSubscriptionStorage getStorage() {
		return storage;
	}

	@Override
	public boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {

		if (msg instanceof LocationUpdateMsg) {
			/*
			 * Invoked on a broker, if a client sent a location update message.
			 * This should only be the case if hybrid mode is enabled.
			 */
			if (BypassSubscriptionSchemeConfiguration.attractionEnableLBSHybrid) {
				LocationUpdateMsg uMsg = (LocationUpdateMsg) msg;
				storage.updateLocationOfClient(uMsg.getSender(), uMsg.getLocation());
			}
		} else if (msg instanceof AttractionSubscriptionMsg) {
			/*
			 * Client sends its current set of subscriptions on AttractionPoints
			 */
			AttractionSubscriptionMsg asub = (AttractionSubscriptionMsg) msg;
			if (asub.isRequestUpdate()) {
				distributeAttractionPoints(asub.getSender());
				if (BypassSubscriptionSchemeConfiguration.attractionEnableLBSHybrid) {
					/*
					 * In the hybrid case, we still need to update associations
					 * to add the client as a "unassociated" subscriber.
					 * However, we do NOT want to inform listeners.
					 */
					storage.updateAttractionPointAssociation(asub.getSender(), asub.getAttractionPoints(), false);
				}
			} else {
				storage.updateAttractionPointAssociation(asub.getSender(), asub.getAttractionPoints(), true);
			}
		}
		return false;
	}

	/**
	 * Return the currently used list of attraction points.
	 */
	public Set<AttractionPoint> getAttractionPoints() {
		return storage.getAttractionPoints();
	}

}