package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip;

import java.util.LinkedHashSet;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;

public class HyperGossipSetNotification extends HyperGossipNotification{

	private static final long serialVersionUID = 1L;
	protected final LinkedHashSet<Integer> ids;
	
	public HyperGossipSetNotification(OverlayContact sender, LinkedHashSet<Integer> ids) {
		super(sender);
		this.ids = ids;
	}

	@Override
	public long getSize() {
		return super.getSize() + ids.size() * 4;
	}

}
