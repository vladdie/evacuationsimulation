package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.schedule;

/**
 * TODO
 * @autor Julian Wulfheide
 */
public abstract class AbstractSchedule implements Schedule {

    @Override
    public abstract boolean equals(Object o);

    @Override
    public abstract int hashCode();

}
