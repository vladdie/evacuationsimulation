package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.priogossip;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.PubSubNotificationForwardingAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.priogossip.PriogossipMessage.messageType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;

/**
 * Skeleton for a priority-based version of Gossiping.
 * 
 * @author Bjoern Richerzhagen, Julian Zobel, Sanja Huhle, Martin Wende
 *
 */
public class PriogossipDistribution extends AbstractLocalDissemination implements EventHandler {

	private final int EVENT_PERIODIC_CHECK = 2;

	public static long periodicCheckInterval = 100 * Time.MILLISECOND;

	public static String ATTRIBUTE_BATTERY_LOW = "BatteryLow";

	private int maxHops = 50;

	private LinkedHashMap<Integer, OverlayContact> seenMessages = new LinkedHashMap<>();

	private PriorityList priolist = new PriorityList(this);

	private boolean debugMode = false;

	private final Random rnd = Randoms.getRandom(PriogossipDistribution.class);

	@TransferState(value = { "Component", "Port" })
	public PriogossipDistribution(BypassClientComponent comp, int port) {
		super(comp, port);
	}

	@Override
	public LocalDistributionType getLocalDistributionType() {
		return LocalDistributionType.PRIOGOSSIP;
	}

	@Override
	public void startMechanism(Callback cb) {
		super.startMechanism(cb);
		/*
		 * Schedule periodic event.
		 * 
		 * (BR): added random offset, otherwise queues would send synchronously,
		 * leading to high message drop.
		 */
		Event.scheduleWithDelay((long) (periodicCheckInterval * rnd.nextDouble()), this, null, EVENT_PERIODIC_CHECK);
	}

	/**
	 * Notification is invoked by the node if it creates and sends a message
	 */
	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		// inform analyzer.
		super.notify(n, subscribers);

		String topic = n.getTopic().getValue().substring(1);

		messageType mtype = messageType.valueOf(topic);
		
		boolean lowBattery = n.getAttribute(ATTRIBUTE_BATTERY_LOW, Boolean.class).getValue();

		// create a new prioritized gossip message
		PriogossipMessage msg = new PriogossipMessage(getLocalOverlayContact(), mtype, getMsgId(), maxHops, n,
				lowBattery);

		if (debugMode) {
			System.out.println(getLocalOverlayContact().getNodeID() + ": Nachricht des Typs " + msg.getType()
					+ " wurde erstellt.");
		}

		// Put the message into the buffer list
		priolist.add(msg);
		seenMessages.put(msg.getMessageID(), msg.getSender());
	}

	/**
	 * Handles an incoming message, that was received by the node
	 */
	@Override
	protected boolean handleMessage(BypassLocalMessage msg, int commID) {

		if (!(msg instanceof PriogossipMessage)) {
			return false;
		}

		PriogossipMessage pmsg = (PriogossipMessage) msg;

		if (seenMessages.containsKey(pmsg.getMessageID())) {
			// Forwarding Analyzer: dropped
			if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
				Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
						getComponent().getHost(), pmsg.getNotification(), pmsg.getSender().getNodeID(), 0);
			}
			return true; // true = msg was processed by this scheme
		}

		// notify own node
		getComponent().handleLocalNotification(pmsg.getNotification());

		// put the message in the priority list
		seenMessages.put(pmsg.getMessageID(), pmsg.getSender());
		priolist.add(new PriogossipMessage(getLocalOverlayContact(), pmsg));

		return true;
	}

	@Override
	public void eventOccurred(Object content, int type) {

		if (getCurrentState() != DistrState.ON) {
			/*
			 * Stop periodic checks once the distribution is no longer active
			 */
			return;
		}

		/*
		 * This method is invoked by the scheduler, if a given event occurred.
		 */
		if (type == EVENT_PERIODIC_CHECK) {
			/*
			 * TODO do sth, and schedule the next periodic check
			 */

			PriogossipMessage msg = priolist.getNextMessage(rnd.nextDouble());

			if (msg != null) {
				if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
					Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onForwardNotification(
							getComponent().getHost(), msg.getNotification(), msg.getSender().getNodeID(), 0);
				}

				send(msg);
			}

			Event.scheduleWithDelay(periodicCheckInterval, this, null, EVENT_PERIODIC_CHECK);
		}
	}

}
