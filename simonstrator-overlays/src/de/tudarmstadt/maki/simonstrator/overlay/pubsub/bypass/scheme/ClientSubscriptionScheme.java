package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme;

import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicSubscription;

/**
 * Client-side implementation of a {@link SubscriptionScheme}.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface ClientSubscriptionScheme extends SubscriptionScheme {

	/**
	 * To prevent additional changes in the API, this is the threshold in meters
	 * until which a published location is still considered to be the client's
	 * own location (difference between publish to own location and publish to
	 * arbitrary locations as defined in the thesis)
	 */
	public static final int OWN_LOCATION_THRESHOLD = 25;

	/**
	 * Enables the creation of custom Notification Schemes. Should extend
	 * {@link BasicNotification} for higher compatibility with applications.
	 * Usually, this is only called on clients.
	 * 
	 * On the client side, this method takes care of wrapping the app-level call
	 * (using any kind of attribute or content-based semantic) into the
	 * currently used scheme.
	 * 
	 * @return
	 */
	public Notification createNotification(Topic topic, List<Attribute<?>> attributes, byte[] payload);

	/**
	 * An extension for location-based pub/sub
	 * 
	 * @param topic
	 * @param attributes
	 * @param location
	 * @param radiusOfInterest
	 * @param payload
	 * @return
	 */
	public Notification createNotification(Topic topic, List<Attribute<?>> attributes, Location location,
			double radiusOfInterest, byte[] payload);

	/**
	 * An extension for location-based pub/sub, publishing to own location
	 * 
	 * @param topic
	 * @param attributes
	 * @param radiusOfInterest
	 * @param payload
	 * @return
	 */
	public Notification createLocalNotification(Topic topic, List<Attribute<?>> attributes,
			double radiusOfInterest, byte[] payload);

	/**
	 * Enables the creation of custom subscription schemes. Should extend
	 * {@link BasicSubscription} to maintain high compatibility with other
	 * applications. Usually, this is only called on clients.
	 * 
	 * @param topic
	 * @param filter
	 * @return
	 */
	public Subscription createSubscription(Topic topic, Filter filter);

	/**
	 * Create a subscription which is limited to the given radius of interest
	 * (in meters) around our current location. The {@link LocationRequest}
	 * object enables the application to specify the granularity of context
	 * updates desired in the overlay.
	 * 
	 * If the radius of interest is to be changed, one needs to re-subscribe
	 * using the default methods provided in {@link PubSubComponent}.
	 * 
	 * @param topic
	 * @param filter
	 * @param locationRequest
	 * @param radiusOfInterest
	 */
	public Subscription createSubscription(Topic topic, Filter filter, LocationRequest locationRequest,
			double radiusOfInterest);

	/**
	 * Called on the client when an application subscribes.
	 * 
	 * @param sub
	 * @param listener
	 *            local listener that is notified on incoming (matching)
	 *            notifications.
	 * @return first listener for this subscription? If yes, return true to
	 *         inform the broker.
	 */
	public boolean onSubscribeAtClient(Subscription sub, PubSubListener listener);

	/**
	 * Remove the given local subscription.
	 * 
	 * @param sub
	 * @param listener
	 *            can be null, in this case, all listeners are removed. If not
	 *            null, we still receive updates for other listeners, if there
	 *            are any.
	 * 
	 * @param true,
	 *            if there are NO MORE listeners on the respective subscription.
	 */
	public boolean onUnsubscribeAtClient(Subscription sub, PubSubListener listener);

	/**
	 * Notifies local {@link PubSubListener}s, if the notification matches the
	 * respective subscription(s) according to the scheme!
	 * 
	 * @param notification
	 * @return true, if a match occurred
	 */
	public boolean notifySelf(Notification notification);

	/**
	 * Returns all local subscriptions and their corresponding
	 * {@link PubSubListener}s.
	 * 
	 * @return
	 */
	public Map<Subscription, Set<PubSubListener>> getLocalSubscriptions();

}
