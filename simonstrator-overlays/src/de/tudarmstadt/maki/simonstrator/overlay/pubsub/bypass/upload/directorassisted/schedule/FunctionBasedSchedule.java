package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.schedule;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;

import java.util.List;

/**
 * TODO
 * @author Julian Wulfheide
 */
public class FunctionBasedSchedule extends AbstractSchedule {

    private final RelayFunction function;

    public static abstract class RelayFunction {
        public abstract List<Long> getUploaders(Notification chunk, List<Long> neighbors);

        public abstract boolean equals(Object o);
    }

    public FunctionBasedSchedule(RelayFunction function) {
        this.function = function;
    }

    @Override
    public List<Long> getUploaders(BypassPubSubComponent comp, Notification chunk, List<Long> neighbors) {
        return function.getUploaders(chunk, neighbors);
    }

    public String toString() {
        return "Function Based Schedule";
    }

    @Override
    public int getTransmissionSize() {
        // TODO
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof FunctionBasedSchedule)) {
            return false;
        } else {
            return (((FunctionBasedSchedule) o).function).equals(function);
        }
    }

    @Override
    public int hashCode() {
        return function.hashCode();
    }

}
