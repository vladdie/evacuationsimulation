package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.unicast;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.PubSubNotificationForwardingAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;

/**
 * Rely on any kind of MANET routing by just sending unicasts. In a ONE-HOP
 * routing environment, this will only work if the nodes are currently within
 * range of each other.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class UnicastDissemination extends AbstractLocalDissemination {

	@TransferState(value = { "Component", "Port" })
	public UnicastDissemination(BypassClientComponent comp, int port) {
		super(comp, port);
	}

	@Override
	public LocalDistributionType getLocalDistributionType() {
		return LocalDistributionType.UNICAST;
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		if (subscribers == null) {
			return;
		}
		super.notify(n, subscribers);
		// Rely on Net-routing
		for (OverlayContact client : subscribers) {
			send(new UnicastNotificationMsg(getLocalOverlayContact(), client, n));
		}
		getComponent().handleLocalNotification(n);
	}

	@Override
	protected boolean handleMessage(BypassLocalMessage msg, int commID) {
		// Just inform the application
		if (!(msg instanceof UnicastNotificationMsg)) {
			return false;
		}
		UnicastNotificationMsg unm = (UnicastNotificationMsg) msg;
		// Forwarding Analyzer
		if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
			Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onForwardNotification(
					getComponent().getHost(), unm.getNotification(), unm.getSender().getNodeID(), 0);
		}
		getComponent().handleLocalNotification(unm.getNotification());
		return true;
	}

}
