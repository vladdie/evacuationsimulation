package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationListener;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationRequest;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSDataCallback;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInfoProperties;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSInformationProvider.SiSProviderHandle;
import de.tudarmstadt.maki.simonstrator.api.component.sis.exception.InformationNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.sis.type.SiSTypes;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer.BypassSubscriptionSchemeAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.BypassMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.BypassMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BrokerSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ClientSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicSubscription;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Abstract base class for location-based subscription schemes. Allows easier
 * analyzing and configuration of some location-uncertainty related parameters.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public abstract class AbstractLocationBasedSubscriptionScheme implements SubscriptionScheme, BypassMessageListener {

	private final SchemeName schemeName;

	@TransferState({ "Component" })
	private BypassPubSubComponent comp;

	/**
	 * Constructor. Initializes the remote storage (if the node acts as a
	 * broker).
	 * 
	 * @param component
	 */
	public AbstractLocationBasedSubscriptionScheme(BypassPubSubComponent component, SchemeName schemeName) {
		this.comp = component;
		this.schemeName = schemeName;
	}

	/**
	 * Handle scheme messages.
	 * 
	 * @param msg
	 * @param sender
	 * @param commID
	 * @return true, if the scheme took care of this message
	 */
	protected abstract boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID);

	@Override
	public SchemeName getName() {
		return schemeName;
	}

	@Override
	public final boolean messageArrived(BypassMessage msg, TransInfo sender, int commID) {
		if (msg instanceof BypassSchemeMessage) {
			/*
			 * Per default, we simply discard messages if they originate from
			 * the wrong scheme to simulate real-world behavior.
			 */
			if (BypassSubscriptionSchemeConfiguration.disableMessageFilteringBasedOnType) {
				return onMessageArrived((BypassSchemeMessage) msg, sender, commID);
			} else if (getName().equals(SchemeName.MULTI)
					|| ((BypassSchemeMessage) msg).getSchemeName().equals(getName())) {
				/*
				 * The MULTI-scheme also always accepts any type (only BROKER!)
				 */
				return onMessageArrived((BypassSchemeMessage) msg, sender, commID);
			}
		}
		return false;
	}

	/**
	 * Access to the parent {@link BypassPubSubComponent}
	 * 
	 * @return
	 */
	public BypassPubSubComponent getComponent() {
		return comp;
	}

	/**
	 * Abstract base class for a location-based {@link BrokerSubscriptionScheme}
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static abstract class Broker extends AbstractLocationBasedSubscriptionScheme
			implements BrokerSubscriptionScheme {

		private SiSProviderHandle sisHandle;

		/**
		 * A location-based broker scheme
		 * 
		 * @param comp
		 * @param schemeName
		 */
		public Broker(BypassCloudComponent comp, SchemeName schemeName) {
			super(comp, schemeName);
		}

		@Override
		public BypassCloudComponent getComponent() {
			return (BypassCloudComponent) super.getComponent();
		}

		/**
		 * Provide access to the {@link LocationBasedSubscriptionStorage} that
		 * maintains client's subscriptions on a broker. Implement your handler
		 * for custom storage types, if needed.
		 * 
		 * @return
		 */
		@Override
		public abstract LocationBasedSubscriptionStorage getStorage();

		@Override
		public OverlayContacts getSubscribers() {
			return getStorage().getSubscribers();
		}

		@Override
		public void onSubscribeAtBroker(Subscription sub, OverlayContact subscriber) {
			getStorage().addSubscription(subscriber, sub);
		}

		@Override
		public void onUnsubscribeAtBroker(Subscription sub, OverlayContact subscriber) {
			getStorage().removeSubscription(subscriber, sub);
		}

		@Override
		public OverlayContacts getSubscribers(Notification notification) {
			OverlayContacts subscribers = getStorage().getSubscribers(notification);
			if (Monitor.hasAnalyzer(BypassSubscriptionSchemeAnalyzer.class)) {
				/*
				 * Inform monitor of performed matches
				 */
				Monitor.getOrNull(BypassSubscriptionSchemeAnalyzer.class).onMatchNotification(getComponent().getHost(),
						notification, getName(), getStorage().getMatchComplexity(notification));
			}

			return subscribers;
		}

		@Override
		public Set<Subscription> getSubscriptionsFrom(OverlayContact subscriber) {
			return getStorage().getSubscriptionsFromContact(subscriber);
		}

		@Override
		public void startMechanism(Callback cb) {
			/*
			 * Register as a listener for incoming transport layer messages (to
			 * be able to deal with location-update messages)
			 */
			getComponent().registerMessageListener(this);

			/*
			 * Provide location data for the SiS (GATEWAY selection)
			 */
			try {
				SiSComponent sis = getComponent().getHost().getComponent(SiSComponent.class);
				sisHandle = sis.provide().nodeState(SiSTypes.PHY_LOCATION, new SiSDataCallback<Location>() {

							@Override
							public Location getValue(INodeID nodeID, SiSProviderHandle providerHandle)
									throws InformationNotAvailableException {
								Location loc = getStorage().getLastKnownLocation(nodeID);
								if (loc == null) {
									throw new InformationNotAvailableException();
								}
								return loc;
							}

							@Override
							public Set<INodeID> getObservedNodes() {
								return getStorage().getNodeIdsWithKnownLocations();
							}

							@Override
							public SiSInfoProperties getInfoProperties() {
								return SiSInfoProperties.NONE;
							}
						});
			} catch (ComponentNotAvailableException e) {
				System.out.println("NO SIS FOUND for BYPASS - this might impact GW selection.");
			}

			// Notify engine that the startup is complete
			cb.finished(true);
		}

		@Override
		public void stopMechanism(Callback cb) {
			getComponent().removeMessageListener(this);
			if (sisHandle != null) {
				try {
					SiSComponent sis = getComponent().getHost().getComponent(SiSComponent.class);
					sis.provide().revoke(sisHandle);
				} catch (ComponentNotAvailableException e) {
					System.out.println("UNABLE TO REVOKE INFO-HANDLER - this might impact GW selection.");
				}
			}
			cb.finished(true);
		}
	}

	/**
	 * Abstract base class for a location-based {@link ClientSubscriptionScheme}
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static abstract class Client extends AbstractLocationBasedSubscriptionScheme
			implements ClientSubscriptionScheme, LocationListener {

		/**
		 * Reference to the {@link LocationSensor} of the current host. Used to
		 * retrieve the current position periodically.
		 */
		@TransferState({ "LocationSensor" })
		private LocationSensor locationSensor;

		/**
		 * Client: currently acting as a location listener (is a subscriber,
		 * needs to update broker context by sending update messages)
		 */
		private boolean isActiveLocationListener = false;

		/**
		 * Client: all local subscriptions, including
		 * {@link LocationBasedSubscription}s
		 */
		private LinkedHashMap<Subscription, Set<PubSubListener>> localSubscriptions = new LinkedHashMap<>();

		/**
		 * Location-based subscriptions for easier reference to determine, if we
		 * still need to send periodic context updates to the broker.
		 */
		private LinkedHashSet<LocationBasedSubscription> localLBSSubscriptions = new LinkedHashSet<>();

		private Location lastLocation = null;

		private long timestampLastLocationUpdate = -1;

		/**
		 * A location-based client Scheme
		 * 
		 * @param comp
		 * @param name
		 */
		public Client(BypassPubSubComponent comp, SchemeName name) {
			super(comp, name);
		}

		/**
		 * Use this method to send location updates or related messages to the
		 * broker.
		 * 
		 * @param newLocation
		 * @param oldLocation
		 *            (can be null)
		 * @param timeSinceLastUpdate
		 *            -1 if invalid
		 * @param distanceSinceLastUpdate
		 *            -1 if invalid
		 */
		protected abstract void onLocationChanged(Location newLocation, Location oldLocation, long timeSinceLastUpdate,
				double distanceSinceLastUpdate);

		@Override
		public boolean onUnsubscribeAtClient(Subscription sub, PubSubListener listener) {
			if (!localSubscriptions.containsKey(sub)) {
				return true;
			}

			if (sub instanceof LocationBasedSubscription) {
				// check if ctx-updates are still needed
				localLBSSubscriptions.remove(sub);
				if (localLBSSubscriptions.isEmpty() && isActiveLocationListener) {
					locationSensor.removeLocationUpdates(this);
					isActiveLocationListener = false;
				}
			}

			if (listener == null) {
				localSubscriptions.get(sub).clear();
			} else {
				localSubscriptions.get(sub).remove(listener);
			}
			if (localSubscriptions.get(sub).isEmpty()) {
				localSubscriptions.remove(sub);
				return true;
			}
			return false;
		}

		@Override
		public boolean notifySelf(Notification notification) {
			/*
			 * Triggered locally on a client (e.g., directly after a new
			 * notification is issued) to check, whether the notification is
			 * also relevant to the creator. In this case, we directly trigger
			 * the subscription callback instead of waiting for a notification
			 * by the broker network.
			 */
			boolean matched = false;

			for (Entry<Subscription, Set<PubSubListener>> entry : localSubscriptions.entrySet()) {
				/*
				 * TODO for evaluation purposes, the scheme does NOT filter
				 * based on location on the client. Instead, it just notifies
				 * the application, leading to "unintended" deliveries that
				 * would then be filtered out.
				 */
				if (getComponent().matches(notification, entry.getKey())) {
					for (PubSubListener listener : entry.getValue()) {
						listener.onNotificationArrived(entry.getKey(), notification);
					}
					
					/*
					 * The boolean, however, has to reflect the actual
					 * location-based matching, as it is used in geofencing ad
					 * hoc dissemination.
					 */
					if (notification instanceof LocationBasedNotification) {
						if (entry.getKey() instanceof LocationBasedSubscription) {
							LocationBasedSubscription lsub = (LocationBasedSubscription) entry.getKey();
							double radiusOfInterest = lsub.getRadiusOfInterest();
							Location notificationLocation = ((LocationBasedNotification) notification).getLocation();
							double notificationRadius = ((LocationBasedNotification) notification)
									.getRadiusOfInterest();
							matched |= notificationLocation.distanceTo(getCurrentLocation()) <= radiusOfInterest
									+ notificationRadius;
						} else {
							matched |= true;
						}
					} else {
						matched |= true;
					}
				}
			}

			return matched;
		}

		@Override
		public final void onLocationChanged(Host host, Location location) {
			assert isActiveLocationListener;

			/*
			 * TODO here, we could filter to only trigger the onLocationChanged
			 * after a given distance or time has passed. Therefore, this method
			 * is final to allow unified handling of such configurations accross
			 * all schemes.
			 */

			long timeSinceUpdate = timestampLastLocationUpdate != -1
					? Time.getCurrentTime() - timestampLastLocationUpdate : -1;
			double distanceSinceUpdate = lastLocation != null ? location.distanceTo(lastLocation) : -1;
			Location oldLocation = lastLocation;
			lastLocation = location;
			onLocationChanged(location, oldLocation, timeSinceUpdate, distanceSinceUpdate);
			timestampLastLocationUpdate = Time.getCurrentTime();
		}

		@Override
		public Map<Subscription, Set<PubSubListener>> getLocalSubscriptions() {
			return localSubscriptions;
		}

		/**
		 * Returns all local LBS subscriptions
		 * 
		 * @return
		 */
		public LinkedHashSet<LocationBasedSubscription> getLocalLBSSubscriptions() {
			return localLBSSubscriptions;
		}

		@Override
		public boolean onSubscribeAtClient(Subscription sub, PubSubListener listener) {
			/*
			 * Called on clients as soon as the subscription is issued
			 */
			if (!localSubscriptions.containsKey(sub)) {
				localSubscriptions.put(sub, new LinkedHashSet<>());
			}
			localSubscriptions.get(sub).add(listener);

			if (sub instanceof LocationBasedSubscription) {
				assert locationSensor != null;
				LocationBasedSubscription xsub = (LocationBasedSubscription) sub;
				/*
				 * Save that this node is actively listening to location updates
				 * and has an active subscription to its position. This triggers
				 * the callback onLocationChanged periodically to receive
				 * location updates.
				 */
				if (!isActiveLocationListener) {
					/*
					 * TODO BR: currently, we just use the first LocationRequest
					 * and ignore others if we still have one active local LBS
					 * subscription.
					 */
					locationSensor.requestLocationUpdates(xsub.getLocationRequest(), this);
					isActiveLocationListener = true;
				}
				localLBSSubscriptions.add(xsub);
			}

			return localSubscriptions.get(sub).size() == 1;
		}

		@Override
		public Notification createNotification(Topic topic, List<Attribute<?>> attributes, byte[] payload) {
			/*
			 * If the notification does not have a position assigned to it
			 * ("normal" notification)
			 */
			return new BasicNotification(topic, attributes, payload);
		}

		@Override
		public Notification createNotification(Topic topic, List<Attribute<?>> attributes, Location location,
				double radiusOfInterest, byte[] payload) {
			return new LocationBasedNotification(topic, attributes, payload, location, radiusOfInterest, false);
		}

		/**
		 * This method explicitly creates a notification AT our current
		 * location, used, for example, by channel-based schemes.
		 * 
		 * @param topic
		 * @param attributes
		 * @param radiusOfInterest
		 * @param payload
		 * @return
		 */
		@Override
		public abstract Notification createLocalNotification(Topic topic, List<Attribute<?>> attributes,
				double radiusOfInterest, byte[] payload);

		@Override
		public Subscription createSubscription(Topic topic, Filter filter) {
			/*
			 * Just a regular subscription (not location based)
			 */
			return new BasicSubscription(topic, filter);
		}

		@Override
		public Subscription createSubscription(Topic topic, Filter filter, LocationRequest locationRequest,
				double radiusOfInterest) {
			return new LocationBasedSubscription(topic, filter, locationRequest, radiusOfInterest);
		}

		/**
		 * Current location
		 * 
		 * @return
		 */
		protected Location getCurrentLocation() {
			return lastLocation;
		}

		public LocationSensor getLocationSensor() {
			return locationSensor;
		}

		public void setLocationSensor(LocationSensor locationSensor) {
			this.locationSensor = locationSensor;
		}

		/**
		 * Send a location update to the broker, containing our current position
		 * (as updated by the location request)
		 */
		protected void sendLocationUpdateMessage() {
			if (getComponent().isInAdhocMode()) {
				// Dropping message in adhoc mode
				return;
			}
			getComponent().sendViaMobileNetwork(new LocationUpdateMsg(getComponent().getLocalOverlayContact(),
					getComponent().getBrokerContact(), getCurrentLocation(), getName()));
		}

		@Override
		public void startMechanism(Callback cb) {

			/*
			 * Initially called by the TransitionEngine to start this
			 * SubscriptionScheme. We retrieve a pointer to the location sensor
			 * to get access to our host's current location.
			 */
			try {
				locationSensor = getComponent().getHost().getComponent(LocationSensor.class);
				lastLocation = locationSensor.getLastLocation();
				timestampLastLocationUpdate = Time.getCurrentTime();
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError("A location sensor is required for operation!");
			}

			/*
			 * Register as a listener for incoming transport layer messages (to
			 * be able to deal with location-update messages)
			 */
			getComponent().registerMessageListener(this);

			// Notify engine that the startup is complete
			cb.finished(true);

		}

		@Override
		public void stopMechanism(Callback cb) {
			/*
			 * stop all location updates, unregister as a message listener.
			 */
			if (isActiveLocationListener) {
				isActiveLocationListener = false;
				locationSensor.removeLocationUpdates(this);
			}

			getComponent().removeMessageListener(this);

			/*
			 * DO NOT clear, if we just pass references in state transfer
			 */
			localSubscriptions = null;
			localLBSSubscriptions = null;
			locationSensor = null;

			cb.finished(true);
		}

	}

}
