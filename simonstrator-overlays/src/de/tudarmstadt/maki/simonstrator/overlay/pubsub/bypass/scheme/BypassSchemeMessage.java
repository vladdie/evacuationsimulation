package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;

/**
 * A marker interface for all {@link SubscriptionScheme}-related messages that
 * would be considered as "overhead" when evaluating the scheme's traffic
 * consumption.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface BypassSchemeMessage {

	// marker

	/**
	 * The name of the scheme used when sending the message.
	 * 
	 * @return
	 */
	public SchemeName getSchemeName();

}
