package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction;

import java.util.LinkedHashSet;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.BypassCloudMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;

/**
 * Carries the set of {@link AttractionPoint}s the client wants to subscribe to.
 * We COULD also only send their names, but this adds a bit of comfort to the
 * set operations.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class AttractionSubscriptionMsg extends AbstractOverlayMessage
		implements BypassCloudMessage, BypassSchemeMessage {

	private static final long serialVersionUID = 1L;

	private Set<AttractionPoint> attractionPoints;

	boolean requestUpdate;

	/**
	 * 
	 * @param sender
	 * @param receiver
	 * @param attractionPoints
	 *            attraction points this client subscribes to.
	 */
	public AttractionSubscriptionMsg(OverlayContact sender, OverlayContact receiver,
			Set<AttractionPoint> attractionPoints, boolean requestUpdate) {
		super(sender, receiver);
		this.attractionPoints = new LinkedHashSet<>(attractionPoints);
		this.requestUpdate = requestUpdate;
	}

	/**
	 * True, if the client requests an updated list of APs
	 * 
	 * @return
	 */
	public boolean isRequestUpdate() {
		return requestUpdate;
	}

	/**
	 * Set of all attraction points
	 * 
	 * @return
	 */
	public Set<AttractionPoint> getAttractionPoints() {
		return attractionPoints;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public SchemeName getSchemeName() {
		return SchemeName.ATTRACTION;
	}

}
