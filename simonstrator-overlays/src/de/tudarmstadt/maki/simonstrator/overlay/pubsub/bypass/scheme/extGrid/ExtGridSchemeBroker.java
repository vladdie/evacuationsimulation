package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid;

import de.tudarmstadt.maki.simonstrator.api.component.transition.MechanismState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.SubscriptionGridCell;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationUpdateMsg;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;

/**
 * Broker side of the location-based subscription scheme where clients are
 * grouped in grid cells based on their location. Cells are defined by squares
 * using two points (minimum bounding rectangle). Clients update their location
 * to the server when they move out of the bounds. Notifications are send to all
 * clients inside a cell.
 * 
 * FIXME (as follow-on-work, not to be done by Julian): currently, the
 * subscriptions themselves are only stored by clients and not at the broker
 * (the broker just performs location matching). We might want to change that to
 * include at least topic/attribute filtering on the broker! This is also true
 * for the Grid-Scheme
 * 
 * @author Julian Zobel
 *
 */
public class ExtGridSchemeBroker extends AbstractLocationBasedSubscriptionScheme.Broker {

	/**
	 * Storage for all {@link SubscriptionGridCell}. Each
	 * {@link SubscriptionGridCell} manages a single cell of the grid (defined
	 * by it's minimum bounding rectangle) and all subscribers within that
	 * cells.
	 */
	private ExtGridBasedSubscriptionStorage storage;

	@MechanismState("GridSize")
	private int gridSize;

	@TransferState({ "Component" })
	public ExtGridSchemeBroker(BypassCloudComponent component) {
		super(component,
				BypassSubscriptionSchemeConfiguration.rectangularAoi ? SchemeName.EXT_GRID_RECT : SchemeName.EXT_GRID);
		BoundingRectangle mbr = new BoundingRectangle(BypassSubscriptionSchemeConfiguration.gridWorldY,
				BypassSubscriptionSchemeConfiguration.gridWorldX, 0, 0);
		storage = new ExtGridBasedSubscriptionStorage(component, mbr);
	}

	public ExtGridBasedSubscriptionStorage getStorage() {
		return storage;
	}

	@Override
	public boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {
		/*
		 * Invoked on a broker, if a client sent a location update message.
		 */
		if (msg instanceof LocationUpdateMsg) {
			LocationUpdateMsg uMsg = (LocationUpdateMsg) msg;
			storage.updateLocationOfClient(uMsg.getSender(), uMsg.getLocation());
		} else {
			/*
			 * We also get all other messages (cloud publish), that are NOT
			 * intended for this listener. Return false.
			 */
			return false;
		}
		return true;
	}

	/**
	 * Supported on the broker: updates the cell-sizes.
	 * 
	 * @param x
	 * @param y
	 */
	public void updateGrid(int x, int y) {
		storage.setAndAdjustGridCells(x, y);
	}

	/**
	 * Automated transitions using the mechanism state to update the grid size.
	 * 
	 * @param gridSize
	 */
	public void setGridSize(int gridSize) {
		this.gridSize = gridSize;
		updateGrid(gridSize, gridSize);
	}

}
