package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;

/**
 * Concept of upload strategies in Bypass, responsible for sending events to the
 * cloud/cloudlet, usually in a collaborative fashion. This strategy is only
 * running on {@link BypassClientComponent}s - in the end, the respective event
 * arrives via the usual way (MOBILE) at the cloud/cloudlet node.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface UploadStrategy extends TransitionEnabled {

	/**
	 * The notification to upload to the cloud / cloudlet. Called by the
	 * producer of the respective notification.
	 * 
	 * @param n
	 */
	public void uploadNotification(Notification n);

	public static enum UploadStrategyType {
		NONE, DIRECT, GK_RELAY, DIRECTOR_ASSISTED
	}

}
