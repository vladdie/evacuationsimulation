package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme;

import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.SubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Broker-side implementation of a {@link SubscriptionScheme}
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface BrokerSubscriptionScheme extends SubscriptionScheme {

	/**
	 * Called on a broker if a subscription for the given overlay contact
	 * arrives.
	 * 
	 * @param sub
	 * @param subscriber
	 *            contact of the subscriber
	 */
	public void onSubscribeAtBroker(Subscription sub, OverlayContact subscriber);

	/**
	 * Remove the given subscription for the given client, called on brokers
	 * 
	 * @param sub
	 * @param subscriber
	 *            the subscriber
	 */
	public void onUnsubscribeAtBroker(Subscription sub, OverlayContact subscriber);

	/**
	 * Access to the storage for subscriptions and clients + state.
	 * 
	 * @return
	 */
	public SubscriptionStorage getStorage();

	/**
	 * Returns a list of all current subscribers independent of their
	 * notification subscriptions.
	 */
	public OverlayContacts getSubscribers();

	/**
	 * Realization of custom matches-methods, invoked on brokers on each call to
	 * the component's matches method (also for app-layer calls). Note, that
	 * this call does NOT return our own node as subscriber!
	 * 
	 * @param notification
	 */
	public OverlayContacts getSubscribers(Notification notification);

	/**
	 * Returns all subscriptions originating from the given subscriber
	 * 
	 * @param subscriber
	 * @return
	 */
	public Set<Subscription> getSubscriptionsFrom(OverlayContact subscriber);

}
