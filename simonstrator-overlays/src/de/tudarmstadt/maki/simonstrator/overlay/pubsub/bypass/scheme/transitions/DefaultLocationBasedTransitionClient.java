package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.transition.AtomicTransition;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;

/**
 * Client-side implementation of a default transition.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class DefaultLocationBasedTransitionClient implements
		AtomicTransition<AbstractLocationBasedSubscriptionScheme.Client, AbstractLocationBasedSubscriptionScheme.Client> {

	@Override
	public Class<AbstractLocationBasedSubscriptionScheme.Client> getTargetType() {
		return AbstractLocationBasedSubscriptionScheme.Client.class;
	}

	@Override
	public Class<AbstractLocationBasedSubscriptionScheme.Client> getSourceType() {
		return AbstractLocationBasedSubscriptionScheme.Client.class;
	}

	private Map<Subscription, Set<PubSubListener>> localSubscriptions = new LinkedHashMap<>();

	@Override
	public void transferState(AbstractLocationBasedSubscriptionScheme.Client sourceComponent,
			AbstractLocationBasedSubscriptionScheme.Client targetComponent) {
		/*
		 * Store a copy of local subscriptions that are later used to
		 * re-subscribe (and trigger location updates) at the client. Please
		 * note: calling onSubscribeAtClient does NOT trigger the transmission
		 * of subscriptions to the broker. It just takes care of starting the
		 * location-sensor updates and thereby sending position updates.
		 */
		localSubscriptions = sourceComponent.getLocalSubscriptions();
	}

	@Override
	public void transitionFinished(AbstractLocationBasedSubscriptionScheme.Client sourceComponent,
			AbstractLocationBasedSubscriptionScheme.Client targetComponent, boolean successful) {
		/*
		 * Re-subscribe to local subscriptions. If the subscription is
		 * LocationBased, this will take care of re-enabling periodic location
		 * updates (as the LocationRequest is stored within the subscription!)
		 */
		for (Entry<Subscription, Set<PubSubListener>> sub : localSubscriptions.entrySet()) {
			for (PubSubListener listener : sub.getValue()) {
				targetComponent.onSubscribeAtClient(sub.getKey(), listener);
			}
		}
	}

}
