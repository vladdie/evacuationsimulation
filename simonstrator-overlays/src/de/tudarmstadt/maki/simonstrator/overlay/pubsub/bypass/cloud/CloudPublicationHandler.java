package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudPublishMessage;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Allows us to extend and manipulate the strategy used by the cloud to
 * disseminate notifications.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface CloudPublicationHandler {

	/**
	 * Handle incoming publications.
	 * 
	 * @param cloud
	 * @param msg
	 */
	public void handlePublication(BypassCloudComponent cloud, CloudPublishMessage msg);

	/**
	 * The default handler.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class DefaultHandler implements CloudPublicationHandler {

		@Override
		public void handlePublication(BypassCloudComponent cloud, CloudPublishMessage msg) {
			// filter and notify clients
			OverlayContacts clients = cloud.getSubscriptionScheme().getSubscribers(msg.getNotification());

			/*
			 * TODO here we could add the gateway-strategy to bundle
			 * subscriptions according to gateways (only notify the gateway, but
			 * include a list of clients that should be notified by that
			 * respective gateway).
			 */
			if (!clients.isEmpty()) {
				Collection<CloudNotifyMessage> msgs = cloud.getGatewayLogic().packMessages(msg.getNotification(),
						clients, -1);
				for (CloudNotifyMessage notifyMessage : msgs) {
					cloud.sendViaMobileNetwork(notifyMessage);
				}
			}
		}
	}

}
