package de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;

/**
 * A subscription being sent to the central server
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class SubscriptionMessage implements Message {

	private static final long serialVersionUID = 1L;

	private Subscription subscription;

	private boolean unsubscribe;

	private OverlayContact subscriber;

	@SuppressWarnings("unused")
	private SubscriptionMessage() {
		// For Kryo
	}

	public SubscriptionMessage(Subscription subscription,
			OverlayContact subscriber) {
		this(subscription, subscriber, false);
	}

	public SubscriptionMessage(Subscription subscription,
			OverlayContact subscriber, boolean unsubscribe) {
		this.subscription = subscription;
		this.unsubscribe = unsubscribe;
		this.subscriber = subscriber;
	}

	@Override
	public long getSize() {
		return subscriber.getTransmissionSize()
				+ subscription.getTransmissionSize();
	}

	public Subscription getSubscription() {
		return subscription;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	public OverlayContact getSubscriber() {
		return subscriber;
	}

	@Override
	public String toString() {
		if (unsubscribe) {
			return "Unsubscribe: " + subscription.toString();
		} else {
			return "Subscription: " + subscription.toString();
		}
	}

	public boolean isUnsubscribe() {
		return unsubscribe;
	}

}
