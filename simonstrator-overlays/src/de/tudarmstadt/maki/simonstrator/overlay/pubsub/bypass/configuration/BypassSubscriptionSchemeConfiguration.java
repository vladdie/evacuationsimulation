package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration;

import java.util.Arrays;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction.AttractionSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction.AttractionSchemeClient;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.basic.BasicSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid.ExtGridBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid.ExtGridSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid.ExtGridSchemeClient;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridSchemeClient;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSchemeClient;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.multi.MultiSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste.SpaceTimeEnvelopeSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste.SpaceTimeEnvelopeSchemeClient;

/**
 * Configures the utilized subscription scheme
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class BypassSubscriptionSchemeConfiguration implements
		BypassConfiguration {

	public static SubscriptionScheme.SchemeName scheme = null;

	private int initialGridSize = 2;

	public static int gridWorldX = 1300;

	public static int gridWorldY = 1300;

	/**
	 * Alpha of the STE
	 * 
	 * For now, we rely on this static parameter. Otherwise, the respective
	 * value would need to be set during each transition.
	 */
	public static double STE_ALPHA = 0.0;

	/**
	 * For debugging purposes, the {@link AttractionSchemeClient} and
	 * {@link AttractionSchemeBroker} can be configured to behave just like a
	 * hybrid of the AttractionScheme and a LBS-Scheme (clients will report
	 * their location periodically).
	 */
	public static boolean attractionEnableLBSHybrid = false;

	/**
	 * If set to true, the client will only subscribe to one (the closest)
	 * attraction point.
	 */
	public static boolean attractionOnlyOneSubscription = false;

	/**
	 * If set to true, we do no longer filter messages based on the sending
	 * scheme type. This leads to behavior where location updates can also be
	 * processed by the broker during a transition, as if we had a translation
	 * matrix in place.
	 */
	public static boolean disableMessageFilteringBasedOnType = false;

	/**
	 * For testing purposes. TODO define a way to pass parameters directly to
	 * object instantiation in the TransitonEngine (w/o requiring a direct self
	 * transition) DEFAULT: behavior of the EXT_GRID
	 */
	public static boolean rectangularAoi = true;

	/**
	 * FIXME Dirty Hack for the simple transition control
	 */
	public static boolean testAutomatedTransitions = false;

	/**
	 * FIXME Dirty hack to evaluate the impact of a disabled state transfer
	 * mechanism. (clients need to resubscribe!)
	 */
	public static boolean disableStateTransfer = false;

	/**
	 * FIXME Dirty hack to delay the execution of transitions on clients for a
	 * random delay of 200 +/- 100ms.
	 */
	public static boolean delayTransitionTrigger = false;

	@Override
	public void configureClient(BypassClientComponent comp) {
		switch (scheme) {
		case BASIC:
			comp.setSubscriptionScheme(new BasicSubscriptionScheme(comp));
			break;

		case LBS:
			comp.setSubscriptionScheme(new LocationBasedSchemeClient(comp));
			break;

		case EXT_GRID:
			rectangularAoi = false;
			ExtGridBasedSubscriptionStorage.defaultXNum = initialGridSize;
			ExtGridBasedSubscriptionStorage.defaultYNum = initialGridSize;
			comp.setSubscriptionScheme(new ExtGridSchemeClient(comp));
			break;

		case EXT_GRID_RECT:
			rectangularAoi = true;
			ExtGridBasedSubscriptionStorage.defaultXNum = initialGridSize;
			ExtGridBasedSubscriptionStorage.defaultYNum = initialGridSize;
			comp.setSubscriptionScheme(new ExtGridSchemeClient(comp));
			break;

		case GRID:
			GridBasedSubscriptionStorage.defaultXNum = initialGridSize;
			GridBasedSubscriptionStorage.defaultYNum = initialGridSize;
			comp.setSubscriptionScheme(new GridSchemeClient(comp));
			break;

		case STE:
			comp.setSubscriptionScheme(new SpaceTimeEnvelopeSchemeClient(comp));
			break;

		case ATTRACTION:
			comp.setSubscriptionScheme(new AttractionSchemeClient(comp));
			break;

		case MULTI:
			comp.setSubscriptionScheme(new LocationBasedSchemeClient(comp));
			break;

		default:
			throw new AssertionError("Unknown subscription scheme.");
		}
	}

	@Override
	public void configureCloud(BypassCloudComponent comp) {
		switch (scheme) {
		case BASIC:
			comp.setSubscriptionScheme(new BasicSubscriptionScheme(comp));
			break;

		case LBS:
			comp.setSubscriptionScheme(new LocationBasedSchemeBroker(comp));
			break;

		case EXT_GRID:
			rectangularAoi = false;
			comp.setSubscriptionScheme(new ExtGridSchemeBroker(comp));
			break;

		case EXT_GRID_RECT:
			rectangularAoi = true;
			comp.setSubscriptionScheme(new ExtGridSchemeBroker(comp));
			break;

		case GRID:
			comp.setSubscriptionScheme(new GridSchemeBroker(comp));
			break;

		case STE:
			comp.setSubscriptionScheme(new SpaceTimeEnvelopeSchemeBroker(comp));
			break;

		case ATTRACTION:
			comp.setSubscriptionScheme(new AttractionSchemeBroker(comp));
			break;

		case MULTI:
			comp.setSubscriptionScheme(new MultiSchemeBroker(comp));
			break;

		default:
			throw new AssertionError("Unknown subscription scheme.");
		}
	}

	/**
	 * Set the subscription scheme to be used by Bypass.
	 * 
	 * @param scheme
	 */
	public void setScheme(String scheme) {
		if (this.scheme != null) {
			throw new AssertionError("Duplicate Scheme configuration!");
		}
		this.scheme = SubscriptionScheme.SchemeName.valueOf(scheme.toUpperCase());
		if (this.scheme == null) {
			throw new AssertionError("Invalid Scheme: " + scheme
					+ ". Must be one of "
					+ Arrays.toString(SubscriptionScheme.SchemeName.values()));
		}
	}
	
	/**
	 * Allows us to configure the initial size of the Grid
	 * 
	 * @param initialGridSize
	 */
	public void setInitialGridSize(int initialGridSize) {
		GridBasedSubscriptionStorage.defaultXNum = initialGridSize;
		GridBasedSubscriptionStorage.defaultYNum = initialGridSize;
		ExtGridBasedSubscriptionStorage.defaultXNum = initialGridSize;
		ExtGridBasedSubscriptionStorage.defaultYNum = initialGridSize;
	}

	/**
	 * Grid needs to know the world size
	 * 
	 * @param worldX
	 */
	public void setWorldX(int worldX) {
		BypassSubscriptionSchemeConfiguration.gridWorldX = worldX;
	}

	/**
	 * Grid needs to know the world size
	 * 
	 * @param worldY
	 */
	public void setWorldY(int worldY) {
		BypassSubscriptionSchemeConfiguration.gridWorldY = worldY;
	}

	/**
	 * Factor for the space-time envelope size
	 * 
	 * @param alpha
	 */
	public void setAlphaFactor(double alpha) {
		BypassSubscriptionSchemeConfiguration.STE_ALPHA = alpha;
	}

	/**
	 * Standard radius of influence for the attraction points, overwrites loaded
	 * Attraction Point settings.
	 * 
	 * @param radius
	 */
	public void setAttractionRadius(double radius) {
		AttractionSchemeBroker.attractionPointROI = radius;
	}

	/**
	 * For debugging purposes, the {@link AttractionSchemeClient} and
	 * {@link AttractionSchemeBroker} can be configured to behave just like a
	 * hybrid of the AttractionScheme and a LBS-Scheme (clients will report
	 * their location periodically).
	 * 
	 * @param attractionEnableLBSHybrid
	 */
	public void setAttractionEnableLBSHybrid(boolean attractionEnableLBSHybrid) {
		BypassSubscriptionSchemeConfiguration.attractionEnableLBSHybrid = attractionEnableLBSHybrid;
	}

	/**
	 * 
	 * @param testTransitions
	 */
	public void setTestTransitions(boolean testTransitions) {
		if (testTransitions) {
			// no longer supported
			throw new AssertionError("No longer supported. Make use of the lbs-demo instead.");
		}
	}


	/**
	 * Enable the automated transition control based on number of nodes in the
	 * BypassCloud
	 * 
	 * @param enable
	 */
	public void setTestAutomatedTransitions(boolean enable) {
		testAutomatedTransitions = enable;
	}

	/**
	 * Disable the state transfer during a transition
	 * 
	 * @param disableTransfer
	 */
	public void setDisableStateTransfer(boolean disableStateTransfer) {
		BypassSubscriptionSchemeConfiguration.disableStateTransfer = disableStateTransfer;
	}

	/**
	 * Delay the execution of transitions on clients by 200 +/- 100 ms to model
	 * message transmission times.
	 * 
	 * @param delayTransitionTrigger
	 */
	public void setDelayTransitionTrigger(boolean delayTransitionTrigger) {
		BypassSubscriptionSchemeConfiguration.delayTransitionTrigger = delayTransitionTrigger;
	}

	/**
	 * For direct access
	 * 
	 * @param scheme
	 */
	public void setSchemeDirect(SubscriptionScheme.SchemeName scheme) {
		this.scheme = scheme;
	}

}
