package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.flooding;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;

/**
 * A TTL-limited broadcast. The distribution strategy could also filter for
 * duplicate messages based on the contained notification.
 * 
 * @author Bjoern Richerzhagen
 * @version May 3, 2014
 */
public class FloodingNotificationMsg extends DefaultLocalMessage implements
		BypassLocalMessage {

	private static final long serialVersionUID = 1L;

	private Notification n;

	private int msgId;

	private int hops;

	public FloodingNotificationMsg(OverlayContact sender, Notification n,
			int msgId) {
		super(sender, null);
		this.n = n;
		this.msgId = msgId;
		this.hops = 0;
	}

	public FloodingNotificationMsg(OverlayContact sender, FloodingNotificationMsg toForward) {
		super(sender, null, toForward);
		this.n = toForward.n;
		this.msgId = toForward.msgId;
		this.hops = toForward.hops + 1;
	}

	public int getHops() {
		return hops;
	}

	@Override
	public long getSize() {
		return super.getSize() + n.getTransmissionSize() + 4;
	}

	public int getMsgId() {
		return msgId;
	}

	public Notification getNotification() {
		return n;
	}

	@Override
	public String toString() {
		return "BC-Notify "
				+ " " + hops + " from " + getSender().toString();
	}

}
