package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudPublishMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.DirectorAssistedUploadStrategy;

/**
 * Basic implementation of an {@link UploadStrategy}.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public abstract class AbstractUploadStrategy implements UploadStrategy {

	@TransferState({ "Component" })
	private BypassClientComponent comp;

	@TransferState({ "Component" })
	public AbstractUploadStrategy(BypassClientComponent comp) {
		this.comp = comp;
	}

	/**
	 * Actually send the notification to the cloud using our local cellular
	 * link.
	 * 
	 * @param n
	 *            the notification to send.
	 */
	protected void doUpload(Notification n) {
		CloudPublishMessage message = new CloudPublishMessage(comp.getLocalOverlayContact(), comp.getBrokerContact(), n,
				comp.getActiveLocalDistribution());
		comp.sendViaMobileNetwork(message);
	}

	/**
	 * Send a message locally.
	 * @param m the message to send.
	 * @param port the port to send the message on.
	 */
	protected void sendLocally(OverlayMessage m, int port) {
		if (comp.hasMessageAnalyzer()) {
			comp.getMessageAnalyzer().onSentOverlayMessage(m, comp.getHost(),
					comp.getLocalTransport().getUtilizedNetInterfaceName());
		}
		comp.getLocalTransport().send(m, port, port);
	}

	public BypassClientComponent getComponent() {
		return comp;
	}

	@Override
	public void startMechanism(Callback cb) {
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		comp = null;
		cb.finished(true);
	}

	/**
	 * Returns the implementing class of a distribution type.
	 * 
	 * @param uploadStrategyType
	 * @return
	 */
	public static UploadStrategy getInstanceFor(UploadStrategyType uploadStrategyType, BypassClientComponent comp) {
		switch (uploadStrategyType) {

		case NONE:
			return new NoUpload(comp);

		case DIRECT:
			return new DirectUploadStrategy(comp);

		case GK_RELAY:
			return new GKRelayStrategy(comp);

        case DIRECTOR_ASSISTED:
            return new DirectorAssistedUploadStrategy(comp);

		default:
			throw new AssertionError();
		}
	}

}
