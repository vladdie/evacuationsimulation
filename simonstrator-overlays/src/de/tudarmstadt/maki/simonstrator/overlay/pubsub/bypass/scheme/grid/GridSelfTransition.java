package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid;

import de.tudarmstadt.maki.simonstrator.api.component.transition.SelfTransition;

/**
 * A self-transition that updates the current grid sizes.
 * 
 * @author Bjoern Richerzhagen
 */
public class GridSelfTransition implements SelfTransition<GridSchemeBroker> {

	private int x = 0;
	private int y = 0;

	public GridSelfTransition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public void alterState(GridSchemeBroker scheme) {
		scheme.getStorage().setAndAdjustGridCells(x, y);
	}

}
