package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip;

import java.util.LinkedHashSet;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;

public class HyperGossipBRNotification extends HyperGossipSetNotification implements HGBeacon{

	private static final long serialVersionUID = 1L;

	public HyperGossipBRNotification(OverlayContact sender, LinkedHashSet<Integer> ids) {
		super(sender, ids);
	}

	public LinkedHashSet<Integer> getBRIds(){
		return ids;
	}
}
