package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.priogossip;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;

/**
 * A message for a prioritized gossiping dissemination. Contains a unique
 * message id, the number of allowed hops and a message type ID. All messages
 * that are send in the priogossip scenario should be of this class.
 * 
 * @author Julian Zobel
 *
 */
public class PriogossipMessage extends DefaultLocalMessage implements
		BypassLocalMessage {

	private static final long serialVersionUID = 1L;

	private final int messageID;
	private final Notification notification;

	/**
	 * The INITIAL sender of the message
	 */
	private final OverlayContact originator;
	private final messageType type;
	private final int ttl;
	private final long sentAt;
	private final boolean senderHasLowBattery;

	// TODO further ideas
	// Position/Location data?

	// TODO Extend the message type if necessary, but check for correlations and
	// overlap to minimize needed message types!
	public enum messageType {
		EMERGENCY, WARNING, INFO, ALIVE, SEARCHPERSON, NEEDRESOURCE, OFFERRESOURCE, MESSAGE, HELLO
	};

	/**
	 * 
	 * @param sender
	 *            The original sender of this message
	 * @param type
	 *            The message type, used for priorization
	 * @param messageID
	 *            Unique identifier for this message
	 * @param timeToLive
	 *            The number of maximum hops that message is allowed to make
	 */
	public PriogossipMessage(OverlayContact sender, messageType type,
			int messageID, int timeToLive, Notification notification, boolean lowBattery) {
		super(sender, null);
		this.originator = sender;
		this.type = type;
		this.messageID = messageID;
		this.ttl = timeToLive;
		this.sentAt = Time.getCurrentTime();
		this.senderHasLowBattery = lowBattery;
		this.notification = notification;
	}

	/**
	 * Clone-Constructor used when forwarding messages. Decrements maxHops by
	 * one.
	 */
	public PriogossipMessage(OverlayContact sender, PriogossipMessage toForward) {
		super(sender, null, toForward);
		this.originator = toForward.originator;
		this.type = toForward.type;
		this.messageID = toForward.messageID;
		this.ttl = toForward.ttl - 1;
		this.sentAt = toForward.sentAt;
		this.senderHasLowBattery = toForward.senderHasLowBattery;
		this.notification = toForward.notification;
	}
	
	public int getMessageID() {
		return messageID;
	}

	public OverlayContact getOriginator() {
		return originator;
	}

	public messageType getType() {
		return type;
	}

	/**
	 * Remaining hops (time to live)
	 * 
	 * @return
	 */
	public int getTimeToLive() {
		return ttl;
	}

	public long getSentAt() {
		return sentAt;
	}

	// FIXME correct size!
	@Override
	public long getSize() {
		// FIXME Sender size handled by super.getSize() ?
		// FIXME Enum size should be around the same size as an integer (?)
		// return super.getSize() + (Integer.BYTES * 4) + (Long.BYTES * 2) +
		// notification.getTransmissionSize();

		// FIXME This is the same size as gossip; for evaluation purposes only!
		return super.getSize() + notification.getTransmissionSize() + 4;
	}

	public boolean hasLowBattery() {
		return senderHasLowBattery;
	}

	public Notification getNotification() {
		return notification;
	}

}
