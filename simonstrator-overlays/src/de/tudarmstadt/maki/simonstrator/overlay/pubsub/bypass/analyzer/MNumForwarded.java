package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer.MNumForwarded.MVNumForwarded;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.DirectorAssistedUploadStrategy;

import java.util.List;

/**
 * TODO
 * @author Julian Wulfheide
 */
public class MNumForwarded extends AbstractMetric<MVNumForwarded> {

    public MNumForwarded() {
        super("Total count for chunks that have been forwarded to another client", MetricUnit.NONE);
    }

    @Override
    public void initialize(List<Host> hosts) {
        for (Host host : hosts) {
            addHost(host, new MVNumForwarded(host));
        }
    }

    public class MVNumForwarded implements de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue<Integer> {

        private final Host host;
        DirectorAssistedUploadStrategy uls = null;

        public MVNumForwarded(Host host) {
            this.host = host;
        }

        @Override
        public Integer getValue() {
            try {
                uls = host.getComponent(DirectorAssistedUploadStrategy.class);
                return uls.getNumForwarded();
            } catch (ComponentNotAvailableException e) {
                uls = null;
                return 0;
            }
        }

        @Override
        public boolean isValid() {
            return uls != null && (uls.getNumDirectUpload() + uls.getNumForwarded() > 0);
        }
    }
}
