package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.transition.AtomicTransition;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;

/**
 * Broker-side implementation of a default scheme transition.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class DefaultLocationBasedTransitionBroker implements
		AtomicTransition<AbstractLocationBasedSubscriptionScheme.Broker, AbstractLocationBasedSubscriptionScheme.Broker> {

	@Override
	public Class<AbstractLocationBasedSubscriptionScheme.Broker> getTargetType() {
		return AbstractLocationBasedSubscriptionScheme.Broker.class;
	}

	@Override
	public Class<AbstractLocationBasedSubscriptionScheme.Broker> getSourceType() {
		return AbstractLocationBasedSubscriptionScheme.Broker.class;
	}

	@Override
	public void transferState(AbstractLocationBasedSubscriptionScheme.Broker sourceComponent,
			AbstractLocationBasedSubscriptionScheme.Broker targetComponent) {
		/*
		 * Transfer broker-subscriptions (we can do this right now, as no
		 * periodic operations or events requiring a fully started mechanism are
		 * required for this).
		 */
		targetComponent.getStorage().importSubscriptions(sourceComponent.getStorage());

		/*
		 * Transfer last known locations. This also triggers assignments to
		 * grid-cells etc, depending on the utilized scheme.
		 */
		for (OverlayContact subscriber : sourceComponent.getStorage().getSubscribers()) {
			Location lastKnownLocation = sourceComponent.getStorage().getLastKnownLocation(subscriber);
			if (lastKnownLocation != null) {
				targetComponent.getStorage().updateLocationOfClient(subscriber, lastKnownLocation);
			}
		}
	}

	@Override
	public void transitionFinished(AbstractLocationBasedSubscriptionScheme.Broker sourceComponent,
			AbstractLocationBasedSubscriptionScheme.Broker targetComponent, boolean successful) {
		// not needed.
	}

}
