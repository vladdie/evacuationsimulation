package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;

/**
 * Gateway-functionality is disabled.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class NoGatewayBehavior extends AbstractLocalGatewayBehavior {

	@TransferState(value = { "Component" })
	public NoGatewayBehavior(BypassClientComponent comp) {
		super(comp, LocalGatewayBehavior.GatewayBehavior.NONE);
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		// ignore.
	}

}
