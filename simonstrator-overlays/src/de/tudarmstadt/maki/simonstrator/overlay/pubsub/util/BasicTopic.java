package de.tudarmstadt.maki.simonstrator.overlay.pubsub.util;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute.BasicAttribute;

/**
 * Implementation of a {@link Topic}, which is basically just a more convenient
 * {@link StringAttribute}. Serialization and De-Serialization is done within
 * the {@link BasicAttribute}
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class BasicTopic extends BasicAttribute<String> implements Topic {

	private static final long serialVersionUID = 1L;

	private BasicTopic() {
		// for Kryo
		super(String.class, Topic.NAME, null);
	}

	public BasicTopic(String topic) {
		super(String.class, Topic.NAME, topic);
	}

	@Override
	public Attribute<String> create(String value) {
		return new BasicTopic(value);
	}

	@Override
	public boolean isIncludedBy(Topic t) {
		/*
		 * Simple prefix-filtering. This topic is included by t, if t is more
		 * general, i.e., t is a prefix of this topic.
		 */
		return getValue().startsWith(t.getValue());
	}

	@Override
	public boolean includes(Topic t) {
		/*
		 * Simple prefix-filtering. Topic t is included, if the path is a
		 * specialization of this topic (i.e., this topic is a prefix of t)
		 */
		return t.getValue().startsWith(getValue());
	}

	@Override
	public FilterOperator getOperator() {
		return FilterOperator.PREFIX;
	}

	@Override
	public Attribute<String> getAttribute() {
		return this;
	}

	@Override
	public BasicTopic clone() {
		return new BasicTopic(getValue());
	}

}
