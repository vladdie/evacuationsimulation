package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.LocationSensor;
import de.tudarmstadt.maki.simonstrator.api.component.sis.SiSComponent;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyGatewayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.AAdvGatewaySelection;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.AAdvGatewaySelection.GatewayLogicBehavior;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.GatewaySelectionStrategy;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Bridge to the {@link TransitionEnabled} implementations of a
 * {@link GatewaySelectionStrategy} (part of the SiS).
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class TransitionEnabledGatewayLogic implements BypassGatewayLogic {

	private static final boolean ENABLE_DEBUG = false;

	private final boolean enableGlobalClustering;

	private final long GLOBAL_GW_SELECTION_RESULT_VALID_FOR = 5 * Time.SECOND;

	private Map<INodeID, List<INodeID>> globalResult = null;

	private long globalResultLastCalculationTimestamp = -1;

	private BypassCloudComponent comp;

	private GatewaySelectionStrategy selection;

	private boolean sendClientContacts;

	private final double desiredGatewayRatio;

	public final static String COMP_NAME = "BypassGatewayLogic";

	/**
	 * 
	 * @param comp
	 * @param defaultStrategy
	 * @param enableGlobalClustering
	 * @param logicBehavior
	 * @param desiredGatewayRatio
	 *            ratio between 0 and 1 -> 1: every subscriber should act as
	 *            gateway, 0: only one gateway for all.
	 * @param sendClientContacts
	 *            attach client contact data to outgoing messages.
	 */
	public TransitionEnabledGatewayLogic(BypassCloudComponent comp, GatewaySelectionStrategy defaultStrategy,
			boolean enableGlobalClustering, GatewayLogicBehavior logicBehavior, double desiredGatewayRatio,
			boolean sendClientContacts) {
		this.comp = comp;
		this.sendClientContacts = sendClientContacts;
		this.enableGlobalClustering = enableGlobalClustering;
		this.desiredGatewayRatio = desiredGatewayRatio;
		try {
			defaultStrategy.setSiS(comp.getHost().getComponent(SiSComponent.class));
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("SiS is required.");
		}

		try {
			TransitionEngine tEngine = comp.getHost().getComponent(TransitionEngine.class);
			selection = tEngine.createMechanismProxy(GatewaySelectionStrategy.class, defaultStrategy, COMP_NAME);
			tEngine.alterLocalState(COMP_NAME, AAdvGatewaySelection.class, "Behaviour", logicBehavior);
			// FIXME Controller-Logic
			// new GatewayLogicTransitionController(this);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("TransitionEngine is required.");
		}

	}

	/**
	 * Disable or enable sending client contacts to gateways. This is only
	 * required for local unicast delivery, for broadcast schemes it will save
	 * message size.
	 * 
	 * @param sendClientContacts
	 */
	public void setSendClientContacts(boolean sendClientContacts) {
		this.sendClientContacts = sendClientContacts;
	}

	public GatewaySelectionStrategy getStrategy() {
		return selection;
	}

	@Override
	public BypassPubSubComponent getComponent() {
		return comp;
	}

	/**
	 * Get the relevant gateways for the set of subscribers.
	 * 
	 * @param subscribers
	 * @return
	 */
	private Map<INodeID, List<INodeID>> getGateways(List<INodeID> subscribers, int numberOfGateways) {
		if (enableGlobalClustering) {
			/*
			 * Select GW out of **all** nodes, and later reduce the list to only
			 * contain subscribers and respective gateways.
			 */
			// cache the clustering results
			if (globalResult == null || globalResultLastCalculationTimestamp
					+ GLOBAL_GW_SELECTION_RESULT_VALID_FOR < Time.getCurrentTime()) {
				List<INodeID> allNodes = comp.getSubscriptionScheme().getSubscribers().getIdList();
				int desiredGateways = numberOfGateways;
				if (desiredGateways <= 0) {
					desiredGateways = (int) Math.max(1, allNodes.size() * desiredGatewayRatio);
				}
				globalResult = selection.getGateways(allNodes, null, desiredGateways);
				globalResultLastCalculationTimestamp = Time.getCurrentTime();
			}
			// for faster matching
			Set<INodeID> subscriberSet = new LinkedHashSet<>(subscribers);
			Map<INodeID, List<INodeID>> cleaned = new LinkedHashMap<>();
			// Check, if a gateway is needed
			for (Entry<INodeID, List<INodeID>> entry : globalResult.entrySet()) {
				// clone, as we cache the GW-selection result
				List<INodeID> nodes = new LinkedList<>(entry.getValue());
				nodes.retainAll(subscriberSet);
				// GW itself is subscriber
				if (subscriberSet.contains(entry.getKey()) || !nodes.isEmpty()) {
					cleaned.put(entry.getKey(), nodes);
				}
			}
			return cleaned;
		} else {
			/*
			 * Just feed the current subscribers into the GW-Selection Process
			 */
			int desiredGateways = numberOfGateways;
			if (desiredGateways <= 0) {
				desiredGateways = (int) Math.max(1, subscribers.size() * desiredGatewayRatio);
			}
			return selection.getGateways(subscribers, null, desiredGateways);
		}
	}

	@Override
	public Collection<CloudNotifyMessage> packMessages(Notification n, OverlayContacts subscribers,
			int numberOfGateways) {
		/*
		 * TODO currently limited to subscribers to the current notification -
		 * global selection out of all subscribers as configurable extra? Does
		 * that make sense at all in any of the Bypass-Scenarios?
		 */
		Map<INodeID, List<INodeID>> gateways = getGateways(subscribers.getIdList(), numberOfGateways);
		List<CloudNotifyMessage> messages = new LinkedList<>();
		StringBuilder info = null;
		if (ENABLE_DEBUG) {
			info = new StringBuilder();
			info.append("FW notification " + gateways.toString());
		}
		for (Map.Entry<INodeID, List<INodeID>> gateway : gateways.entrySet()) {
			OverlayContact gatewayContact = comp.getSubscriptionScheme().getSubscribers().get(gateway.getKey());
			Collection<OverlayContact> clients;
			if (gateway.getValue().isEmpty()) {
				// All Subscribers
				clients = Collections.emptyList();
			} else {
				clients = subscribers.getListFor(gateway.getValue());
			}
			clients.remove(gatewayContact);
			if (clients.isEmpty()) {
				// only one subscriber (gateway itself)
				messages.add(new CloudNotifyMessage(comp.getLocalOverlayContact(), gatewayContact, n));
			} else {
				if (ENABLE_DEBUG) {
					/*
					 * SANITY-Checks (TODO remove lateron) to check node
					 * distance based on global knowledge.
					 */
					try {
						Location gwLocation = Oracle.getHostByID(gatewayContact.getNodeID())
								.getComponent(LocationSensor.class).getLastLocation();
						info.append("\nForward notification via " + gatewayContact + " to " + clients.size()
								+ " clients:\n");
						for (OverlayContact client : clients) {
							Location clientLocation = Oracle.getHostByID(client.getNodeID())
									.getComponent(LocationSensor.class).getLastLocation();
							double dist = gwLocation.distanceTo(clientLocation);
							info.append("\tto " + client + " with distance " + (int) dist + " meters "
									+ (dist > 100 ? " !!!! " : "") + " \n");
						}
					} catch (ComponentNotAvailableException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				boolean notifyGW = subscribers.contains(gatewayContact);
				messages.add(new CloudNotifyGatewayMessage(comp.getLocalOverlayContact(), gatewayContact, n,
						sendClientContacts ? clients : Collections.emptyList(), notifyGW));
			}
		}
		if (ENABLE_DEBUG) {
			System.out.println(info);
		}
		return messages;
	}

}
