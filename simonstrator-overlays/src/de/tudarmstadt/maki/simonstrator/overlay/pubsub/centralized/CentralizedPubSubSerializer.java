package de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized;

import java.io.InputStream;
import java.io.OutputStream;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.Serializer;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized.msg.NotificationMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized.msg.SubscriptionMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.PubSubSerializerUtils;

/**
 * Serializer for the centralized Pub/Sub
 * 
 * @author Bjoern Richerzhagen
 * @version Jun 27, 2014
 */
public class CentralizedPubSubSerializer implements Serializer {

	private static Class<?>[] types = new Class<?>[] {
			NotificationMessage.class, SubscriptionMessage.class };

	@Override
	public Class<?>[] getSerializableTypes() {
		return PubSubSerializerUtils.concatenateWithUtilClasses(types);
	}

	@Override
	public void serialize(OutputStream out, Message msg) {
		throw new UnsupportedOperationException(
				"Old serializer interface is no longer supported.");
	}

	@Override
	public Message create(InputStream in) {
		throw new UnsupportedOperationException(
				"Old serializer interface is no longer supported.");
	}

}
