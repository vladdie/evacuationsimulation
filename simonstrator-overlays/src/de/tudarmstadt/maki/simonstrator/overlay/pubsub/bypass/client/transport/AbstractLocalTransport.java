package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.component.LifecycleComponent;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassSerializer;

/**
 * To enable two layers of coordinated transitions, this component realizes the
 * local communication with the transport-stack via a proxy instance. It can
 * then be realized either via WiFi Ad Hoc or via Bluetooth - switching between
 * those is done as part of a transition. Enabling both could also be a
 * transition in that sense (however, we do not yet know how to distinguish them
 * in that case)
 * 
 * Its sole purpose is to return a working protocol instance on the currently
 * active protocol. TODO: Lateron, we might really turn BT/WiFi on and off
 * within this component.
 * 
 * @author Bjoern Richerzhagen
 * @version Sep 18, 2014
 */
public abstract class AbstractLocalTransport implements
		LocalTransport {

	/**
	 * The OverlayComponent-pointer to be transferred
	 */
	@TransferState({ "Component" })
	protected final BypassClientComponent comp;

	protected final NetInterfaceName netToUse;

	protected NetInterface net;

	/**
	 * Those need to be replaced (new PHY), they are not transferred
	 */
	private final Map<Integer, MessageBasedTransport> transportByPort = new LinkedHashMap<Integer, MessageBasedTransport>();

	/**
	 * Listeners need to be transferred to new protocol
	 */
	private final Map<Integer, TransMessageListener> listenerByPort = new LinkedHashMap<Integer, TransMessageListener>();

	/**
	 * 
	 * @param netToUse
	 *            the netInterface that is used in this
	 *            LocalTransportComponent-Instance.
	 */
	public AbstractLocalTransport(BypassClientComponent component, NetInterfaceName netToUse) {
		this.comp = component;
		this.netToUse = netToUse;
	}

	public BypassClientComponent getComponent() {
		// for state transfer
		return comp;
	}

	public Map<Integer, TransMessageListener> getListenerByPort() {
		return listenerByPort;
	}

	/**
	 * Used by the local dissemination component to listen to incoming messages.
	 * 
	 * @param listener
	 * @param port
	 */
	@Override
	public void setTransMessageListener(TransMessageListener listener, int port) {
		if (netToUse != null) {
			MessageBasedTransport transport = getTransport(port);
			transport.setTransportMessageListener(listener);
		}
		listenerByPort.put(port, listener);
	}

	/**
	 * Removes the TransMessageListener listening on the given port.
	 * 
	 * @param port
	 */
	@Override
	public void removeTransMessageListener(int port) {
		if (netToUse != null && transportByPort.containsKey(port)) {
			MessageBasedTransport transport = getTransport(port);
			transport.removeTransportMessageListener();
		}
		listenerByPort.remove(port);
	}

	/**
	 * Used by the LocalDistribution to send overlay messages
	 * 
	 * @param msg
	 */
	@Override
	public void send(OverlayMessage msg, int senderPort, int receiverPort) {
		if (netToUse == null) {
			return;
		}
		if (!comp.isActive()) {
			return;
		}
		MessageBasedTransport transport = getTransport(senderPort);
		if (msg.getReceiver() == null) {
			// broadcast (assuming unified port)
			transport.send(msg, transport.getNetInterface()
					.getBroadcastAddress(), transport.getLocalPort());
		} else {
			// unicast
			if (msg.getReceiver().getNetID(netToUse) != null) {
				transport.send(msg, msg.getReceiver().getNetID(netToUse),
						receiverPort);
			} else {
				Monitor.log(
						AbstractLocalTransport.class,
						Level.WARN,
						"Dropping message %s, as contact %s has no matching IP for the NetType.",
						msg, msg.getReceiver());
			}
		}
	}

	@Override
	public void startMechanism(Callback cb) {
		if (netToUse == null) {
			cb.finished(true);
			return;
		}
		net = comp.getHost().getNetworkComponent().getByName(netToUse);
		if (net == null) {
			cb.finished(false);
			return;
		}
		if (net instanceof LifecycleComponent) {
			((LifecycleComponent) net).startComponent();
		}

		// Transfer listeners within the TransitionImplementation!
		for (Entry<Integer, TransMessageListener> transferredListener : listenerByPort.entrySet()) {
			MessageBasedTransport trans = getTransport(transferredListener.getKey());
			trans.setTransportMessageListener(transferredListener.getValue());
		}

		// TODO: add time for setup?
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		if (netToUse == null) {
			cb.finished(true);
			return;
		}
		if (net instanceof LifecycleComponent) {
			((LifecycleComponent) net).stopComponent();
		}
		/*
		 * Clear lists, remove listeners
		 */
		for (MessageBasedTransport transport : transportByPort.values()) {
			transport.removeTransportMessageListener();
		}
		transportByPort.clear();
		listenerByPort.clear();
		net = null;
		// Notify engine
		cb.finished(true);
	}

	/**
	 * Initializes a new Protocol Instance on the given port with the current
	 * PHY (determined by the state of this proxy), if the instance does not yet
	 * exist.
	 * 
	 * @param port
	 * @return
	 */
	private MessageBasedTransport getTransport(int port) {
		MessageBasedTransport transport = transportByPort.get(port);
		if (transport == null) {
			try {
				transport = comp.getAndBindUDP(
						net.getLocalInetAddress(), port, new BypassSerializer());
				comp.getLocalOverlayContact().addTransInfo(
						net.getName(), transport.getTransInfo());
			} catch (ProtocolNotAvailableException e) {
				throw new RuntimeException("Protocol not available.");
			}
			transportByPort.put(port, transport);
		}
		return transport;
	}

	@Override
	public NetInterfaceName getUtilizedNetInterfaceName() {
		return netToUse;
	}

	/**
	 * WiFi-Instance
	 * 
	 * @author Bjoern Richerzhagen
	 * @version Sep 18, 2014
	 */
	public static class WiFiAdHocTransport extends AbstractLocalTransport {
		@TransferState({ "Component" })
		public WiFiAdHocTransport(BypassClientComponent comp) {
			super(comp, NetInterfaceName.WIFI);
		}
	}

	/**
	 * Bluetooth-Instance
	 * 
	 * @author Bjoern Richerzhagen
	 * @version Sep 18, 2014
	 */
	public static class BluetoothAdHocTransport extends AbstractLocalTransport {
		@TransferState({ "Component" })
		public BluetoothAdHocTransport(BypassClientComponent comp) {
			super(comp, NetInterfaceName.BLUETOOTH);
		}
	}

	/**
	 * No local Transport interface
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public static class NoAdHocTransport extends AbstractLocalTransport {
		@TransferState({ "Component" })
		public NoAdHocTransport(BypassClientComponent comp) {
			super(comp, null);
		}
	}

}
