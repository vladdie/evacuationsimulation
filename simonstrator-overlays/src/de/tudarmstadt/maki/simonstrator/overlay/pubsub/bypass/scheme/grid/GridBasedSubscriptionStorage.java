package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid;

import java.util.LinkedHashSet;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Storage for subscription grid cells. Models the grid and it's
 * functionalities.
 * 
 * Update BR: extends {@link LocationBasedSubscriptionStorage} to store last
 * known locations, and overwrites the respective matching function.
 * 
 * The grid cells are used to determine the set of subscribers (i.e., the
 * {@link OverlayContact}s), which are then used to retrieve and match all
 * {@link LocationBasedSubscription}s.
 * 
 * @author Julian Zobel, Bjoern Richerzhagen
 *
 */
public class GridBasedSubscriptionStorage extends LocationBasedSubscriptionStorage {

	private LinkedHashSet<SubscriptionGridCell> subscriptionGridCells;
	// The maximum outline of the grid
	private final BoundingRectangle mbr;

	private int x_num;
	private int y_num;

	public static int defaultXNum = 2;
	public static int defaultYNum = 2;

	public GridBasedSubscriptionStorage(BypassPubSubComponent component, BoundingRectangle mbr) {
		super(component);
		this.mbr = mbr;

		// Initialize the hash set of grids
		subscriptionGridCells = new LinkedHashSet<SubscriptionGridCell>();

		x_num = defaultXNum;
		y_num = defaultYNum;

		setAndAdjustGridCells(x_num, y_num);
	}

	@Override
	public void updateLocationOfClient(OverlayContact client, Location updatedLocation) {
		// move client to a new cell
		moveClient(client, getLastKnownLocation(client), updatedLocation);
		// store last known location
		super.updateLocationOfClient(client, updatedLocation);
	}

	/**
	 * Move the client to a new cell. In this grid we assume that clients only
	 * update their position whenever they leave their current cell. Therefore,
	 * we always send an update containing the new cell.
	 * 
	 * @param client
	 * @param previousLocation
	 *            (can be null)
	 * @param updatedLocation
	 */
	protected void moveClient(OverlayContact client, Location previousLocation, Location updatedLocation) {
		for (SubscriptionGridCell cell : subscriptionGridCells) {
			// Just delete in all
			cell.removeSubscriber(client);
		}
		for (SubscriptionGridCell cell : subscriptionGridCells) {
			if (cell.getMBR().contains(updatedLocation.getLatitude(), updatedLocation.getLongitude())) {
				cell.addSubscriber(client);
				sendGridLocationMessage(client, cell.getMBR());
				break;
			}
		}
	}

	@Override
	protected OverlayContacts getLBSSubscribers(LocationBasedNotification notification) {
		/*
		 * Find clients in the affected grids. DO NOT call
		 * super.getLBSSubscribers()
		 */
		OverlayContacts affectedClients = new OverlayContacts();
		OverlayContacts subscribers = new OverlayContacts();

		if (notification instanceof ChannelBasedNotification) {
			// Based on channel
			for (SubscriptionGridCell cell : subscriptionGridCells) {
				if (((ChannelBasedNotification) notification).getChannels().contains(cell.getMBR().getChannelName())) {
					affectedClients.addAll(cell.getSubscribers());
				}
			}
		} else {
			// Based on location
			for (SubscriptionGridCell cell : subscriptionGridCells) {
				if (cell.getMBR().circleIntersection(notification.getLocation().getLatitude(),
						notification.getLocation().getLongitude(), notification.getRadiusOfInterest())) {
					affectedClients.addAll(cell.getSubscribers());
				}
			}
		}
		/*
		 * Check subscribers' subscriptions
		 */
		for (OverlayContact potentialSubscriber : affectedClients) {
			for (Subscription sub : getLocationBasedSubscriptionsBy(potentialSubscriber)) {
				if (getComponent().matches(notification, sub)) {
					subscribers.add(potentialSubscriber);
				}
			}
		}
		return subscribers;
	}

	@Override
	protected void onNoLongerLBSSubscriber(OverlayContact subscriber) {
		super.onNoLongerLBSSubscriber(subscriber);
		for (SubscriptionGridCell cell : subscriptionGridCells) {
			// Just delete in all cells
			cell.removeSubscriber(subscriber);
		}
	}

	@Override
	protected int getLBSMatchComplexity(LocationBasedNotification notification) {
		/*
		 * Here, match complexity is reduced to the number of cells
		 * 
		 * TODO differentiate between Channel-based notification and default
		 * location-based notification!
		 */
		if (notification instanceof ChannelBasedNotification) {
			return ((ChannelBasedNotification) notification).getChannels().size();
		}

		return subscriptionGridCells.size();
	}

	/**
	 * Returns the set of current grid cells.
	 * 
	 * @return
	 */
	public LinkedHashSet<SubscriptionGridCell> getGridCells() {
		return subscriptionGridCells;
	}

	/**
	 * Split the grid according to the given numbers of cells
	 * 
	 * @param cellsX
	 * @param cellsY
	 */
	public void setAndAdjustGridCells(int cellsX, int cellsY) {

		assert cellsX >= 0;
		assert cellsY >= 0;

		x_num = cellsX;
		y_num = cellsY;

		LinkedHashSet<OverlayContact> subscribers = new LinkedHashSet<OverlayContact>();

		// save all contacts
		for (SubscriptionGridCell cell : subscriptionGridCells) {
			subscribers.addAll(cell.getSubscribers());
		}

		subscriptionGridCells = new LinkedHashSet<SubscriptionGridCell>();

		double cellwidth = mbr.getWidth() / x_num;
		double cellheight = mbr.getHeight() / y_num;
		for (int x = 0; x < x_num; x++) {
			for (int y = 0; y < y_num; y++) {
				BoundingRectangle r = new BoundingRectangle(cellwidth * (x + 1), cellheight * (y + 1), cellwidth * x,
						cellheight * y);
				subscriptionGridCells.add(new SubscriptionGridCell(r));
			}
		}

		/*
		 * Assign client based on last known location. If we are wrong, the
		 * client will reply with an updated grid subscribe message.
		 */
		for (OverlayContact client : subscribers) {
			Location lastLocation = getLastKnownLocation(client);
			if (lastLocation != null) {
				// this triggers the re-assignment and a gridLocationMsg
				updateLocationOfClient(client, lastLocation);
			} else {
				sendGridLocationMessage(client, null);
			}
		}
	}

	/**
	 * Send a GridLocationMessage to a specific client/subscriber
	 * 
	 * @param subscriber
	 *            The subscriber that receives the message
	 * @param mbr
	 *            The MBR that defines the grid cell that the
	 *            GridLocationMessage announces
	 */
	protected void sendGridLocationMessage(OverlayContact subscriber, BoundingRectangle mbr) {
		getComponent().sendViaMobileNetwork(new GridCellUpdateMsg(getComponent().getLocalOverlayContact(), subscriber,
				mbr, getComponent().getSubscriptionScheme().getName()));
	}
}
