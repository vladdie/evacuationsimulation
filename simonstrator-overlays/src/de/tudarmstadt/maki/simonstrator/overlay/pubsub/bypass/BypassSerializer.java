package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass;

import java.io.InputStream;
import java.io.OutputStream;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.Serializer;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.flooding.FloodingNotificationMsg;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.gossip.GossipNotificationMsg;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip.HyperGossipBRNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip.HyperGossipBeacon;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip.HyperGossipDataNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip.HyperGossipLBRNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.hypergossip.HyperGossipSetNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb.PlanBNotificationMsg;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb.PlanBWrapMsg;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudPublishMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudSubscribeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudUnsubscribeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.PubSubSerializerUtils;
import de.tudarmstadt.maki.simonstrator.service.location.DummyLocation;
import de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.data.NodeMetaData;
import de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.msg.CurrentPositionMsg;
import de.tudarmstadt.maki.simonstrator.service.virtualtopologywithlocation.msg.TopologyUpdateMsg;

/**
 * For efficient serialization, ensure that all serializable types of this
 * overlay are included below. This is not only limited to messages, but also to
 * data structures contained within those messages.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class BypassSerializer implements Serializer {

	private static Class<?>[] types = new Class[] { /* Local msg types */
	FloodingNotificationMsg.class, GossipNotificationMsg.class,
			HyperGossipBRNotification.class, HyperGossipBeacon.class,
			HyperGossipDataNotification.class,
			HyperGossipLBRNotification.class, HyperGossipSetNotification.class,
			PlanBNotificationMsg.class, PlanBWrapMsg.class, /* global msg types */
			CloudNotifyMessage.class,
			CloudPublishMessage.class, CloudSubscribeMessage.class,
			CloudUnsubscribeMessage.class, /* Sub-Schemes */
			TopologyUpdateMsg.class, NodeMetaData.class,
			DummyLocation.class, CurrentPositionMsg.class };

	@Override
	public Class<?>[] getSerializableTypes() {
		return PubSubSerializerUtils.concatenateWithUtilClasses(types);
	}

	@Override
	public void serialize(OutputStream out, Message msg) {
		throw new UnsupportedOperationException(
				"Legacy analyzer is no longer supported");
	}

	@Override
	public Message create(InputStream in) {
		throw new UnsupportedOperationException(
				"Legacy analyzer is no longer supported");
	}

}
