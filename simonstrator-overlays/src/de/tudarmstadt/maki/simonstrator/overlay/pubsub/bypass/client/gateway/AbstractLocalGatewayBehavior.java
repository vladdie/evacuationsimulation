package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway;

import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;

/**
 * Abstract base class for {@link LocalGatewayBehavior}s, implementing the
 * lifecycle methods.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public abstract class AbstractLocalGatewayBehavior implements LocalGatewayBehavior {

	@TransferState(value = { "Component" })
	private BypassClientComponent comp;

	private final LocalGatewayBehavior.GatewayBehavior type;

	@TransferState(value = { "Component" })
	public AbstractLocalGatewayBehavior(BypassClientComponent comp, LocalGatewayBehavior.GatewayBehavior type) {
		this.comp = comp;
		this.type = type;
	}

	@Override
	public GatewayBehavior getGatewayBehavior() {
		return type;
	}

	public BypassClientComponent getComponent() {
		return comp;
	}

	@Override
	public void startMechanism(Callback cb) {
		cb.finished(true);
	}

	@Override
	public void stopMechanism(Callback cb) {
		cb.finished(true);
	}

}
