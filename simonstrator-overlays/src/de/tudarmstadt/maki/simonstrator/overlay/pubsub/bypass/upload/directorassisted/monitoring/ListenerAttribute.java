package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;

/**
 * TODO
 * @author Julian Wulfheide
 */
public abstract class ListenerAttribute<T> extends MonitoredAttribute<T> {

    public ListenerAttribute(Attribute<T> attribute, AttributeListener listener) {
        super(attribute, listener);
    }

    public T getValue() {
        return value;
    }

}
