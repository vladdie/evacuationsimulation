package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.flooding;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.PubSubNotificationForwardingAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;

/**
 * Just simple broadcasting of notifications (we could evaluate different
 * numbers of hops and ranges...). Prevents duplicates.
 * 
 * TODO update number of hops etc with a self-transition.
 * 
 * @author Bjoern Richerzhagen
 * @version May 3, 2014
 */
public class FloodingDistribution extends AbstractLocalDissemination {

	private final static int ID_CACHE_SIZE = 2000;

	private int TTL = 15;

	@SuppressWarnings("serial")
	LinkedHashMap<Integer, Object> seenMsgs = new LinkedHashMap<Integer, Object>() {
		@Override
		protected boolean removeEldestEntry(Map.Entry<Integer, Object> eldest) {
			return size() > ID_CACHE_SIZE;
		}
	};

	@TransferState(value = { "Component", "Port" })
	public FloodingDistribution(BypassClientComponent comp, int port) {
		super(comp, port);
	}

	@Override
	public LocalDistributionType getLocalDistributionType() {
		return LocalDistributionType.FLOODING;
	}

	public void setTTL(int ttl) {
		TTL = ttl;
	}

	public int getTTL() {
		return TTL;
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		super.notify(n, subscribers); // triggering analyzer
		FloodingNotificationMsg msg = new FloodingNotificationMsg(
				getLocalOverlayContact(), n, getMsgId());
		seenMsgs.put(msg.getMsgId(), null);
		send(msg);
		getComponent().handleLocalNotification(n);
	}

	@Override
	protected boolean handleMessage(BypassLocalMessage msg, int commID) {
		assert getCurrentState() != DistrState.OFF;
		if (!(msg instanceof FloodingNotificationMsg)) {
			return false;
		}
		assert !msg.getSender().equals(getLocalOverlayContact());

		FloodingNotificationMsg bnm = (FloodingNotificationMsg) msg;
		if (seenMsgs.containsKey(bnm.getMsgId())) {
			// Forwarding Analyzer: dropped
			if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
				Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
						getComponent().getHost(), bnm.getNotification(), bnm.getSender().getNodeID(), 0);
			}
			return true;
		}

		// notify own node
		boolean isSubscriber = getComponent().handleLocalNotification(bnm.getNotification());
		boolean forward = getCurrentState() == DistrState.ON;

		if (isSubjectToGeofencing(bnm.getNotification())) {
			forward = forward && isSubscriber;
		}

		// forward
		if (forward && bnm.getHops() < getTTL()) {
			/*
			 * FIXME limit the maximum number of hops
			 */
			assert getCurrentState() == DistrState.ON;
			seenMsgs.put(bnm.getMsgId(), null);
			FloodingNotificationMsg fw = new FloodingNotificationMsg(
					getLocalOverlayContact(), bnm);

			// Forwarding Analyzer
			if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
				Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class)
						.onForwardNotification(getComponent().getHost(), bnm.getNotification(),
								bnm.getSender().getNodeID(), 0);
			}

			send(fw);
		} else {
			// Forwarding Analyzer: dropped
			if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
				Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
						getComponent().getHost(), bnm.getNotification(), bnm.getSender().getNodeID(), 0);
			}
		}

		return true;
	}

}
