package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.gossip.GossipDistribution;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway.LocalGatewayBehavior.GatewayBehavior;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway.NoGatewayBehavior;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway.StatelessGatewayBehavior;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport.AbstractLocalTransport.WiFiAdHocTransport;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway.NoGatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway.SimpleGatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway.TransitionEnabledGatewayLogic;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.AAdvGatewaySelection.GatewayLogicBehavior;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.GatewaySelectionStrategy;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.misc.TempClusteringConfig;

/**
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class BypassGatewayConfiguration implements BypassConfiguration {

	private GatewaySelectionStrategy defaultStrategy = null;

	public enum GatewayStrategy {
		NONE, SIMPLE, TRANSITION
	}

	private GatewayStrategy selectedStrategy = GatewayStrategy.NONE;

	private LocalDistributionType selectedDelivery = LocalDistributionType.NONE;

	private GatewayBehavior clientGatewayBehavior = GatewayBehavior.NONE;

	private boolean enableGlobalClustering = false;

	private double gossipAlpha = -1;

	private double desiredGatewayRatio = 0;

	private boolean sendClientContacts = false;

	private GatewayLogicBehavior gwLogicBehavior = GatewayLogicBehavior.CUTTING;

	public static final int port = 7711;

	@Override
	public void configureClient(BypassClientComponent comp) {
		/*
		 * Ensure GW-local delivery
		 */
		if (selectedStrategy != GatewayStrategy.NONE) {
			AbstractLocalDissemination dis = AbstractLocalDissemination.getInstanceFor(selectedDelivery, comp, port);
			if (gossipAlpha != -1 && dis instanceof GossipDistribution) {
				((GossipDistribution) dis).setAlpha(gossipAlpha);
			}
			comp.setLocalGatewayDistribution(dis);
			comp.setLocalTransport(new WiFiAdHocTransport(comp));
		}
		if (selectedStrategy == GatewayStrategy.TRANSITION) {
			switch (clientGatewayBehavior) {
			case NONE:
				comp.setLocalGatewayBehavior(new NoGatewayBehavior(comp));
				break;

			case STATELESS:
				comp.setLocalGatewayBehavior(new StatelessGatewayBehavior(comp));
				break;

			default:
				break;
			}
		}
	}

	@Override
	public void configureCloud(BypassCloudComponent comp) {
		switch (selectedStrategy) {
		case SIMPLE:
			comp.setGatewayLogic(new SimpleGatewayLogic(comp));
			break;

		case TRANSITION:
			assert defaultStrategy != null;
			comp.setGatewayLogic(
					new TransitionEnabledGatewayLogic(comp, defaultStrategy, enableGlobalClustering, gwLogicBehavior,
							desiredGatewayRatio, sendClientContacts));
			break;

		default:
			comp.setGatewayLogic(new NoGatewayLogic(comp));
			break;
		}
	}
	
	/**
	 * Sets the default strategy to be used by the cloud when relying on
	 * transition enabled.
	 * 
	 * @param strategy
	 */
	public void setDefault(GatewaySelectionStrategy strategy) {
		defaultStrategy = strategy;
	}
	
	/**
	 * Which strategy scheme to use
	 * 
	 * @param strategy
	 */
	public void setStrategy(String strategy) {
		selectedStrategy = GatewayStrategy.valueOf(strategy);
		assert selectedStrategy != null;
	}

	/**
	 * How do clients act?
	 * 
	 * @param clientGatewayBehavior
	 */
	public void setClientGatewayBehavior(String clientGatewayBehavior) {
		this.clientGatewayBehavior = GatewayBehavior.valueOf(clientGatewayBehavior);
		assert this.clientGatewayBehavior != null;
	}

	/**
	 * Which delivery scheme to use (out of LocalDistributionType)
	 * 
	 * @param strategy
	 */
	public void setDelivery(String delivery) {
		selectedDelivery = LocalDistributionType.valueOf(delivery);
		// include subscribers in GW-msg for unicast delivery.
		sendClientContacts = selectedDelivery == LocalDistributionType.UNICAST;
		assert selectedDelivery != null;
	}

	/**
	 * If set to true, clusters / gateways are calculated based on the global
	 * state, not only based on the subscribers of a notification.
	 * 
	 * @param globalClustering
	 */
	public void setGlobalClustering(boolean globalClustering) {
		this.enableGlobalClustering = globalClustering;
	}

	/**
	 * Allows us to set the GOSSIP-alpha
	 * 
	 * @param alpha
	 */
	public void setGossipAlpha(double alpha) {
		this.gossipAlpha = alpha;
	}

	/**
	 * Set an assumed communication range used by the clustering algorithm and
	 * all other relevant parameters for Bypass.
	 * 
	 * @param assumedCommunicationRange
	 */
	public void setAssumedCommunicationRange(double assumedCommunicationRange) {
		TempClusteringConfig.maxTransmissionRange = assumedCommunicationRange;
		TempClusteringConfig.cushionFactor = 0.8;
		TempClusteringConfig.maxRange = assumedCommunicationRange;
		TempClusteringConfig.minPTS = 2;
	}

	/**
	 * Set, if the number of gateways is enforced (CUTTING) or simply a
	 * suggestion.
	 * 
	 * @param logic
	 */
	public void setLogicBehavior(String logicBehavior) {
		this.gwLogicBehavior = GatewayLogicBehavior.valueOf(logicBehavior);
		if (this.gwLogicBehavior == null) {
			throw new AssertionError();
		}
	}

	/**
	 * The maxGateways-parameter in Gateway Selection is not clearly defined -
	 * sometimes it is a maximum, often a recommendation, sometimes a minimum.
	 * Therefore, we enable configuration of a desired ratio of gateways out of
	 * the provided candidates.
	 * 
	 * @param desiredGatewayRatio
	 * @return
	 */
	public void setDesiredGatewayRatio(double desiredGatewayRatio) {
		this.desiredGatewayRatio = desiredGatewayRatio;
	}

}
