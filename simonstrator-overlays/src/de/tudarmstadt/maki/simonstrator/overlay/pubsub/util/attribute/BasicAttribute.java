package de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute;

import java.lang.reflect.InvocationTargetException;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.KryoSerializable;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;

/**
 * Basic implementation of a generic attribute
 * 
 * @author Bjoern Richerzhagen
 * 
 * @param <T>
 *            attribute type
 */
public class BasicAttribute<T> implements Attribute<T>, KryoSerializable {

	private static final long serialVersionUID = 1L;

	private Class<T> type;

	private String name;

	private T value;

	@SuppressWarnings("unused")
	private BasicAttribute() {
		// for Kryo
	}

	/**
	 * Creates an attribute of the given type, name, and content.
	 * 
	 * @param type
	 * @param name
	 * @param value
	 */
	public BasicAttribute(Class<T> type, String name, T value) {
		this.type = type;
		this.name = name;
		this.value = value;
	}

	@Override
	public Class<T> getType() {
		return type;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public Attribute<T> create(T value) {
		return new BasicAttribute<T>(type, name, value);
	}

	@Override
	public int getTransmissionSize() {
		int size = 4; // We assume a default value size of 4 bytes for
						// non-Transmitable types.
		if (value instanceof Transmitable) {
			size = ((Transmitable) value).getTransmissionSize();
		}
		return name.length() + 4 + size; // 4 for type
	}

	@Override
	public String toString() {
		return name + " - " + type.getSimpleName() + " - "
				+ value.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicAttribute<?> other = (BasicAttribute<?>) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public BasicAttribute<T> clone() {
		/*
		 * If the value implements cloneable, we clone.
		 */
		if (value instanceof Cloneable) {
			try {
				return new BasicAttribute<T>(type, name, (T) value.getClass()
						.getMethod("clone").invoke(value));
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			throw new AssertionError(
						"Something went wrong on an attempt to clone an attribute value!");
		} else {
			return new BasicAttribute<T>(type, name, value);
		}
	}

	@Override
	public void write(Kryo kryo, Output output) {
		kryo.writeObject(output, name);
		kryo.writeClass(output, type);
		kryo.writeClassAndObject(output, value);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void read(Kryo kryo, Input input) {
		name = kryo.readObject(input, String.class);
		type = (Class<T>) kryo.readClass(input).getType();
		value = (T) kryo.readClassAndObject(input);
	}

}
