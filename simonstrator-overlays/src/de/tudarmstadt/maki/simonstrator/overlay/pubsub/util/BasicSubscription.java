package de.tudarmstadt.maki.simonstrator.overlay.pubsub.util;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.SubscriptionInfo;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;

/**
 * Basic implementation of the {@link Subscription}-interface. Updated equals
 * and hashCode to comply with overlay contract: two subscriptions are equal, if
 * the contained filters are equal as well.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class BasicSubscription implements Subscription {

	private static final long serialVersionUID = 1L;

	private Topic topic;

	private Filter filter;

	// Analyzing, used by Applications
	private transient SubscriptionInfo _subscriptionInfo;

	@SuppressWarnings("unused")
	private BasicSubscription() {
		// for Kryo
	}

	/**
	 * A Subscription with additional filters
	 * 
	 * @param topic
	 * @param filter (can be null)
	 */
	public BasicSubscription(Topic topic, Filter filter) {
		this.topic = topic;
		this.filter = filter;
	}

	/**
	 * Clone-Constructor
	 * 
	 * @param toClone
	 */
	protected BasicSubscription(BasicSubscription toClone) {
		this.topic = toClone.topic.clone();
		if (toClone.filter != null) {
			this.filter = toClone.filter.clone();
		} else {
			this.filter = null;
		}
		this._subscriptionInfo = toClone._subscriptionInfo;
	}

	@Override
	public Filter getFilter() {
		return filter;
	}

	@Override
	public Topic getTopic() {
		return topic;
	}

	@Override
	public SubscriptionInfo _getSubscriptionInfo(SubscriptionInfo info) {
		if (info != null) {
			if (_subscriptionInfo != null) {
				throw new AssertionError("Already set!");
			}
			_subscriptionInfo = info;
		}
		return _subscriptionInfo;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append(topic);
		str.append("   filter: ");
		str.append(filter);
		return str.toString();
	}

	private int size_cached = -1;

	@Override
	public int getTransmissionSize() {
		if (size_cached == -1) {
			size_cached = topic.getTransmissionSize();
			if (filter != null) {
				size_cached += filter.getTransmissionSize();
			}
		}
		return size_cached;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((filter == null) ? 0 : filter.hashCode());
		result = prime * result + ((topic == null) ? 0 : topic.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicSubscription other = (BasicSubscription) obj;
		if (filter == null) {
			if (other.filter != null)
				return false;
		} else if (!filter.equals(other.filter))
			return false;
		if (topic == null) {
			if (other.topic != null)
				return false;
		} else if (!topic.equals(other.topic))
			return false;
		return true;
	}

	@Override
	public BasicSubscription clone() {
		return new BasicSubscription(this);
	}


}
