package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util;

import java.awt.geom.Line2D;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;

/**
 * Class that represents a rectangle based on two geographical points.
 * 
 * Partially found on http://stackoverflow.com/a/23174504/5723264
 * 
 * @author Julian Zobel
 *
 */
public class BoundingRectangle implements Transmitable {

	private static final long serialVersionUID = 1L;

	public double neLatitude = 0.0; // x
	public double neLongitude = 0.0; // y
	public double swLatitude = 0.0; // x
	public double swLongitude = 0.0; // y

	private double centerLat; // x
	private double centerLong; // y

	private final String channelName;

	/**
	 * 
	 * @param neLatitude
	 *            The latitude of the northeast point that defines this
	 *            rectangle
	 * @param neLongitude
	 *            The longitude of the northeast point that defines this
	 *            rectangle
	 * @param swLatitude
	 *            The latitude of the southwest point that defines this
	 *            rectangle
	 * @param swLongitude
	 *            The longitude of the southwest point that defines this
	 *            rectangle
	 */
	public BoundingRectangle(double neLatitude, double neLongitude, double swLatitude, double swLongitude)
    {  
		this.neLatitude = neLatitude;
		this.neLongitude = neLongitude;
		this.swLatitude = swLatitude;
		this.swLongitude = swLongitude;
		
		centerLat = neLatitude - (Math.abs(neLatitude - swLatitude) / 2);
		centerLong = neLongitude - (Math.abs(neLongitude - swLongitude) / 2);

		this.channelName = "MBR-" + toString();
    }

	public double getCenterLat() {
		return centerLat;
	}

	public double getCenterLong() {
		return centerLong;
	}

	public double getWidth() {
		return Math.abs(neLatitude - swLatitude);
	}

	public double getHeight() {
		return Math.abs(neLongitude - swLongitude);
	}

	/**
	 * An identifier for the channel defined by this MBR
	 * 
	 * @return
	 */
	public String getChannelName() {
		return channelName;
	}

    @Override
    public String toString()
    {
		return String.format("(%.2f|%.2f),(%.2f|%.2f)", swLatitude, swLongitude, neLatitude, neLongitude);
    }

	/**
	 * Check for intersection between a circle shaped notification and the MBR
	 * 
	 * http://stackoverflow.com/a/402010/5723264
	 * 
	 * @param circleCenterLat
	 *            Latitude (x) of circle's center
	 * @param circleCenterLong
	 *            Longitude (y) of circle's center
	 * @param radius
	 *            Radius of circle
	 * @return true if circle overlaps with the bounding rectangle, false if
	 *         otherwise
	 */
	public boolean circleIntersection(double circleCenterLat, double circleCenterLong, double radius) {
		/*
		 * FIXME Ultra-dirty hack to test rectangular AoIs. This results in the
		 * broker always returning a set of MBRs that can be described as a
		 * rectangle.
		 */
		if (BypassSubscriptionSchemeConfiguration.rectangularAoi) {
			double r1Left = swLatitude;
			double r1Right = neLatitude;
			double r1Top = neLongitude;
			double r1Bottom = swLongitude;
			double r2Left = circleCenterLat - radius;
			double r2Right = circleCenterLat + radius;
			double r2Top = circleCenterLong + radius;
			double r2Bottom = circleCenterLong - radius;
			// http://stackoverflow.com/questions/2752349/fast-rectangle-to-rectangle-intersection
			return !(r2Left > r1Right || r2Right < r1Left || r2Top < r1Bottom || r2Bottom > r1Top);
		}

		double circleDistanceX = Math.abs(circleCenterLat - centerLat);
		double circleDistanceY = Math.abs(circleCenterLong - centerLong);

		if (circleDistanceX > (getWidth() / 2 + radius))
			return false;

		if (circleDistanceY > (getHeight() / 2 + radius))
			return false;

		if (circleDistanceX <= (getWidth() / 2))
			return true;

		if (circleDistanceY <= (getHeight() / 2))
			return true;

		double cornerDistance_sq = Math.pow((circleDistanceX - getWidth() / 2), 2)
				+ Math.pow((circleDistanceY - getHeight() / 2), 2);

		return (cornerDistance_sq <= Math.pow(radius, 2));
	}

	/**
	 * Check if the bounding rectangle contains the latitude and longitude. The
	 * function will not work if the box contains a pole.
	 * 
	 * @param latitude
	 *            The latitude of the point
	 * @param longitude
	 *            The longitude of the same point
	 * @return true if longitude and latitude are both contained in the
	 *         rectangle, false if otherwise
	 */
    public boolean contains(double latitude, double longitude)
    {
        boolean longitudeContained = false;
        boolean latitudeContained = false;

		// Check if the bounding rectangle contains the prime meridian
		// (longitude 0.0).
        if (swLongitude < neLongitude)
        {
			if (swLongitude <= longitude && longitude <= neLongitude)
            {
                longitudeContained = true;
            }
        }
        else
        {
            // Contains prime meridian.
			if ((0 <= longitude && longitude <= neLongitude) || (swLongitude <= longitude && longitude <= 0))
            {
                longitudeContained = true;
            }
        }

        if (swLatitude < neLatitude)
        {
			if (swLatitude <= latitude && latitude <= neLatitude)
            {
                latitudeContained = true;
            }
		}

        return (longitudeContained && latitudeContained);
    }

	public boolean radiusoutside(double latitude, double longitude, double radius) {
		assert contains(latitude, longitude);

		Line2D left = new Line2D.Double(swLatitude, neLongitude, swLatitude, swLongitude);
		Line2D right = new Line2D.Double(neLatitude, neLongitude, neLatitude, swLongitude);
		Line2D top = new Line2D.Double(swLatitude, neLongitude, neLatitude, neLongitude);
		Line2D down = new Line2D.Double(swLatitude, swLongitude, neLatitude, swLongitude);

		if (left.ptLineDist(latitude, longitude) < radius)
			return true;
		if (right.ptLineDist(latitude, longitude) < radius)
			return true;
		if (top.ptLineDist(latitude, longitude) < radius)
			return true;
		if (down.ptLineDist(latitude, longitude) < radius)
			return true;

		return false;
	}

	/**
	 * Helper method that returns the outer rectangle for a set of
	 * {@link BoundingRectangle}s
	 * 
	 * @param rects
	 * @return
	 */
	public static BoundingRectangle getOuterBoundingRectangle(Set<BoundingRectangle> rects) {
		/*
		 * FIXME this most likely only works in Cartesian coordinates! OK for
		 * simulations, but keep in mind for potential prototypes.
		 */
		double neLongitude = 0;
		double neLatitude = 0;
		double swLongitude = Double.MAX_VALUE;
		double swLatitude = Double.MAX_VALUE;
		for (BoundingRectangle rect : rects) {
			assert rect.neLongitude > rect.swLongitude
					&& rect.neLatitude > rect.swLatitude : "Works only for Cartesian Coordinates...";
			neLongitude = Math.max(rect.neLongitude, neLongitude);
			neLatitude = Math.max(rect.neLatitude, neLatitude);
			swLongitude = Math.min(rect.swLongitude, swLongitude);
			swLatitude = Math.min(rect.swLatitude, swLatitude);
		}
		return new BoundingRectangle(neLatitude, neLongitude, swLatitude, swLongitude);

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(neLatitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(neLongitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(swLatitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(swLongitude);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoundingRectangle other = (BoundingRectangle) obj;
		if (Double.doubleToLongBits(neLatitude) != Double.doubleToLongBits(other.neLatitude))
			return false;
		if (Double.doubleToLongBits(neLongitude) != Double.doubleToLongBits(other.neLongitude))
			return false;
		if (Double.doubleToLongBits(swLatitude) != Double.doubleToLongBits(other.swLatitude))
			return false;
		if (Double.doubleToLongBits(swLongitude) != Double.doubleToLongBits(other.swLongitude))
			return false;
		return true;
	}

	@Override
	public int getTransmissionSize() {
		// 4 double values (center can be calculated).
		return 4 * 8;
	}
}