package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.monitoring;

/**
 * TODO
 * @author Julian Wulfheide
 */
public class MonitoringAttributes {

    public final static String TOPIC_ROOT = "/monitor/";
    public final static String SRC_ID = "src-id";
    public final static String LOCATION = "loc";
    public final static String NEIGHBORS = "nghbrs";
    public final static String AP_CONNECTED = "ap-cnctd";
    public final static String REMAINING_UPLOAD = "rem-up";
    // public final static String SSID = "ssid";
    public final static String BITRATES = "rates";
    public final static String LAYER_PRIORITIES = "lyr-weights";

    public final static String UP_SINCE = "up";

}
