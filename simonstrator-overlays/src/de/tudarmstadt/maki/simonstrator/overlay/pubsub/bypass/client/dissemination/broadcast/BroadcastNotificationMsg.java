package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.broadcast;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.DefaultLocalMessage;

/**
 * 
 * @author bjoern
 *
 */
public class BroadcastNotificationMsg extends DefaultLocalMessage implements BypassLocalMessage {

	private static final long serialVersionUID = 1L;

	private Notification n;

	public BroadcastNotificationMsg(OverlayContact sender, Notification n) {
		super(sender, null);
		this.n = n;
	}

	@Override
	public long getSize() {
		return super.getSize() + n.getTransmissionSize();
	}

	public Notification getNotification() {
		return n;
	}

}
