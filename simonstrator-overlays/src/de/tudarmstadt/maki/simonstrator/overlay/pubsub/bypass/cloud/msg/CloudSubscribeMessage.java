package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;

/**
 * Subscription message sent from clients to the Bypass cloudlet/server.
 * 
 * @author Bjoern Richerzhagen
 * @version May 2, 2014
 */
public class CloudSubscribeMessage extends AbstractOverlayMessage implements
		BypassCloudMessage {

	private static final long serialVersionUID = 1L;

	private Subscription[] subs;

	private transient long _size = -1;

	@SuppressWarnings("unused")
	private CloudSubscribeMessage() {
		// For Kryo
	}

	public CloudSubscribeMessage(OverlayContact sender, OverlayContact receiver, Subscription... subs) {
		super(sender, receiver);
		this.subs = subs;
		assert subs.length > 0;
	}

	public Subscription[] getSubscriptions() {
		return subs;
	}

	@Override
	public long getSize() {
		if (_size == -1) {
			_size = super.getSize();
			for (Subscription subscription : subs) {
				_size += subscription.getTransmissionSize();
			}
		}
		return _size;
	}

	public OverlayContact getSubscriber() {
		return getSender();
	}

	@Override
	public Message getPayload() {
		return null;
	}

}
