package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer;

import java.io.Writer;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.common.metric.AbstractMetric;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;
import de.tudarmstadt.maki.simonstrator.overlay.api.IOverlayMessageAnalyzer;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.analyzer.MNotifications.NotificationMetricValue;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudPublishMessage;


/**
 * Generic Metric-Implementation of the {@link IOverlayMessageAnalyzer},
 * providing size and count for given Notifications.
 * @author Julian Wulfheide
 */
public abstract class MNotifications extends
        AbstractMetric<NotificationMetricValue> implements IOverlayMessageAnalyzer {

    protected enum Reason {
        SEND, RECEIVE
    }

    private Reason reason;

    private final boolean perHost;

    protected final String topic;

    /**
     * @param name        name of the metric, global metrics will be prefixed with a "G"
     * @param description
     * @param unit
     * @param reason      SEND, RECEIVE
     * @param perHost     if false, this is a global metric
     */
    public MNotifications(String name, String description, MetricUnit unit,
                          Reason reason, String topic, boolean perHost) {
        super(perHost ? name : "G" + name, description, unit);
        Monitor.registerAnalyzer(this);
        this.reason = reason;
        this.perHost = perHost;
        this.topic = topic;
    }

    /**
     * A metric value with a simple counter that can be incremented
     * @author Julian Wulfheide
     */
    public class NotificationMetricValue
            implements
            de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue<Double> {

        private double value = 0;

        public NotificationMetricValue() {
            //
        }

        @Override
        public Double getValue() {
            return value;
        }

        @Override
        public boolean isValid() {
            return value >= 0;
        }

        public void incValue(double inc) {
            this.value += inc;
        }

    }

    @Override
    public void initialize(List<Host> hosts) {
        if (perHost) {
            for (Host host : hosts) {
                try {
                    if (host.getComponent(BypassPubSubComponent.class).getNodeType() == BypassPubSubComponent.NodeType.CLIENT) {
                        addHost(host, createMessageMetricValue(host));
                    }
                } catch (ComponentNotAvailableException e) {
                }
            }
        } else {
            setOverallMetric(createMessageMetricValue(null));
        }
    }

    /**
     * Allows extending metrics to add more complex metric values.
     * @param host or null, if global
     * @return
     */
    protected NotificationMetricValue createMessageMetricValue(Host host) {
        return new NotificationMetricValue();
    }

    /**
     * Called whenever a message is passed to the monitor that matches our
     * filters.
     * @param mv   the MetricValue to update
     * @param msg  the message
     * @param host the host or null, if overall metric
     */
    protected abstract void update(NotificationMetricValue mv, Message msg, Host host);

    /*
     * (non-Javadoc)
     *
     * @see
     * de.tudarmstadt.maki.simonstrator.overlay.api.IOverlayMessageAnalyzer#
     * onSentOverlayMessage
     * (de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage,
     * de.tudarmstadt.maki.simonstrator.api.Host)
     */
	@Override
	public void onSentOverlayMessage(OverlayMessage msg, Host host, NetInterfaceName netName) {
        if (reason == Reason.SEND) {
            if (perHost) {
                update(getPerHostMetric(host.getId()), msg, host);
            } else {
                update(getOverallMetric(), msg, null);
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * de.tudarmstadt.maki.simonstrator.overlay.api.IOverlayMessageAnalyzer#
     * onReceivedOverlayMessage
     * (de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage,
     * de.tudarmstadt.maki.simonstrator.api.Host)
     */
	@Override
    public void onReceivedOverlayMessage(OverlayMessage msg, Host host) {
        if (reason == Reason.RECEIVE) {
            if (perHost) {
                update(getPerHostMetric(host.getId()), msg, host);
            } else {
                update(getOverallMetric(), msg, null);
            }
        }
    }

    @Override
    public final void start() {
        // ignore, should use initialize instead.
    }

    @Override
    public final void stop(Writer output) {
        // ignore
    }


    /**
     * Counter for notifications with the given topic TOPIC.
     * @author Julian Wulfheide
     */
    public static class CountTopic extends MNotifications {

        @XMLConfigurableConstructor({"reason", "perHost", "topic"})
        public CountTopic(String reason, boolean perHost, String topic) {
            super("Count" + topic + reason,
                    "Total count for " + reason + " of " + topic,
                    MetricUnit.NONE, Reason.valueOf(reason.toUpperCase()),
                    topic, perHost);
        }

        @Override
        protected void update(NotificationMetricValue mv, Message msg, Host host) {
            if (msg.getClass().isAssignableFrom(CloudNotifyMessage.class)) {
                Notification n = ((CloudNotifyMessage) msg).getNotification();
                if (n.getTopic().getValue().equals(topic)) {
                    mv.incValue(1);
                }
            } else if (msg.getClass().isAssignableFrom(CloudPublishMessage.class)) {
                Notification n = ((CloudPublishMessage) msg).getNotification();
                if (n.getTopic().getValue().equals(topic)) {
                    mv.incValue(1);
                }
            }
        }
    }

    /**
     * Message size aggregator for notifications with the given topic TOPIC.
     * @author Julian Wulfheide
     */
    public static class SizeTopic extends MNotifications {

        @XMLConfigurableConstructor({"reason", "perHost", "topic"})
        public SizeTopic(String reason, boolean perHost, String topic) {
            super("Size" + topic + reason,
                    "Total size for " + reason + " of " + topic,
                    MetricUnit.DATA, Reason.valueOf(reason.toUpperCase()),
                    topic, perHost);
        }

        @Override
        protected void update(NotificationMetricValue mv, Message msg, Host host) {
            if (msg.getClass().isAssignableFrom(CloudNotifyMessage.class)) {
                Notification n = ((CloudNotifyMessage) msg).getNotification();
                if (n.getTopic().getValue().equals(topic)) {
                    mv.incValue(msg.getSize());
                }
            } else if (msg.getClass().isAssignableFrom(CloudPublishMessage.class)) {
                Notification n = ((CloudPublishMessage) msg).getNotification();
                if (n.getTopic().getValue().equals(topic)) {
                    mv.incValue(msg.getSize());
                }
            }
        }
    }

}
