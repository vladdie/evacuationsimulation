package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg;

import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.BypassMessage;

/**
 * This is a marker-interface for a message that is only sent or received
 * locally (WiFi Ad Hoc, BT, ...). Used for some sanity checks throughout the
 * code.
 * 
 * @author Bjoern Richerzhagen
 * @version Jun 25, 2014
 */
public interface BypassLocalMessage extends BypassMessage {
	// marker

	/**
	 * Return the {@link LocalDistributionType} that produced this message.
	 * 
	 * @return
	 */
	public LocalDistributionType getDisseminationType();

	/**
	 * Some sort of simplified vector clock, assuming that we have ONE
	 * originator per transition. This counts the number of transitions between
	 * dissemination mechansims to determine the "most up to date" dissemination
	 * type.
	 * 
	 * @return
	 */
	public int getSenderTransitionCount();

}
