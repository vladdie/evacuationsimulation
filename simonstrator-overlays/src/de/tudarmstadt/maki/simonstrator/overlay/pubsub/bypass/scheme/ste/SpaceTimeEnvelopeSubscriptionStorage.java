package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste;

import java.util.LinkedHashMap;
import java.util.Map;

import org.dyn4j.geometry.Vector2;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscriptionStorage;

/**
 * Storage for location-based subscriptions using space-time-envelopes. They
 * rely on the {@link LocationBasedSubscriptionStorage} and simply add the
 * current vector for the STE as additional state.
 * 
 * @author Julian Zobel, Bjoern Richerzhagen
 *
 */
public class SpaceTimeEnvelopeSubscriptionStorage extends LocationBasedSubscriptionStorage {

	private Map<OverlayContact, Vector2> movementVectors = new LinkedHashMap<>();

	private Map<OverlayContact, Map<LocationBasedSubscription, SpaceTimeEnvelope>> cachedSTEs = new LinkedHashMap<>();

	private double alpha;

	public SpaceTimeEnvelopeSubscriptionStorage(BypassPubSubComponent comp, double alpha) {
		super(comp);
		this.alpha = alpha;
	}

	@Override
	protected boolean matchesLBS(LocationBasedNotification notification, LocationBasedSubscription subscription,
			OverlayContact subscriber) {
		/*
		 * If we do not have a STE yet, match solely based on the last known
		 * location.
		 */
		SpaceTimeEnvelope ste = getCachedSTE(subscriber, subscription);
		if (ste == null || ste.getCenterCircle() == null) {
			return super.matchesLBS(notification, subscription, subscriber);
		}

		/*
		 * Otherwise, match against STE
		 */
		return ste.containsPoint(
				new Vector2(notification.getLocation().getLongitude(), notification.getLocation().getLatitude()));
	}

	@Override
	public void addSubscription(OverlayContact subscriber, Subscription subscription) {
		super.addSubscription(subscriber, subscription);
		if (subscription instanceof LocationBasedSubscription) {
			/*
			 * Create an STE for the cache
			 */
			LocationBasedSubscription lbSub = (LocationBasedSubscription) subscription;
			SpaceTimeEnvelope ste = new SpaceTimeEnvelope(lbSub.getRadiusOfInterest());
			Map<LocationBasedSubscription, SpaceTimeEnvelope> cache = cachedSTEs.get(subscriber);
			if (cache == null) {
				cache = new LinkedHashMap<>();
				cachedSTEs.put(subscriber, cache);
			}
			cache.put(lbSub, ste);
		}
	}

	@Override
	protected void onNoLongerLBSSubscriber(OverlayContact subscriber) {
		// Delete movement Vector
		movementVectors.remove(subscriber);
		cachedSTEs.remove(subscriber);
	}

	/**
	 * For visualization purposes, simply return the first cached STE.
	 * 
	 * @param contact
	 * @return
	 */
	public SpaceTimeEnvelope getCachedSTE(OverlayContact contact) {
		Map<LocationBasedSubscription, SpaceTimeEnvelope> cache = cachedSTEs.get(contact);
		for (SpaceTimeEnvelope ste : cache.values()) {
			return ste;
		}
		return null;
	}

	/**
	 * For production use: return the cached STE for the corresponding
	 * subscription (defining the radius of interest!)
	 * 
	 * @param contact
	 * @param subscription
	 * @return
	 */
	public SpaceTimeEnvelope getCachedSTE(OverlayContact contact, LocationBasedSubscription subscription) {
		Map<LocationBasedSubscription, SpaceTimeEnvelope> cache = cachedSTEs.get(contact);
		if (cache != null) {
			return cache.get(subscription);
		}
		return null;
	}

	/**
	 * Update subscriber-specific state with a movement vector.
	 * 
	 * @param subscriber
	 * @param vector
	 */
	public void updateMovementVectorOfClient(OverlayContact client, Location location, Vector2 vector) {
		movementVectors.put(client, vector);
		/*
		 * update STE HERE to prevent high overhead.
		 */
		SpaceTimeEnvelope ste = getCachedSTE(client);
		if (ste != null) {
			ste.updateEnvelope(location, vector, alpha);
		}
	}

	@Override
	public void updateLocationOfClient(OverlayContact client, Location updatedLocation) {
		super.updateLocationOfClient(client, updatedLocation);
	}

	/**
	 * Return last known movement vector of subscriber, null otherwise.
	 * 
	 * @param subscriber
	 * @return
	 */
	public Vector2 getMovementVector(OverlayContact subscriber) {
		return movementVectors.get(subscriber);
	}

	public void setAlpha(double alpha) {
		this.alpha = alpha;
	}

}
