package de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute.filter;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.AttributeFilter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;

/**
 * Default Implementation of an {@link AttributeFilter}.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class BasicAttributeFilter<T> implements AttributeFilter<T> {

	private static final long serialVersionUID = 1L;

	private FilterOperator operator;

	private Attribute<T> attribute;

	@SuppressWarnings("unused")
	private BasicAttributeFilter() {
		// for Kryo
	}

	public BasicAttributeFilter(FilterOperator operator, Attribute<T> attribute) {
		this.operator = operator;
		this.attribute = attribute;
	}

	@Override
	public FilterOperator getOperator() {
		return operator;
	}

	@Override
	public Attribute<T> getAttribute() {
		return attribute;
	}

	@Override
	public int getTransmissionSize() {
		return attribute.getTransmissionSize() + operator.getTransmissionSize();
	}

	@Override
	public String toString() {
		return operator.toString() + " " + attribute.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attribute == null) ? 0 : attribute.hashCode());
		result = prime * result
				+ ((operator == null) ? 0 : operator.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicAttributeFilter<?> other = (BasicAttributeFilter<?>) obj;
		if (attribute == null) {
			if (other.attribute != null)
				return false;
		} else if (!attribute.equals(other.attribute))
			return false;
		if (operator != other.operator)
			return false;
		return true;
	}

	@Override
	public BasicAttributeFilter<T> clone() {
		return new BasicAttributeFilter<T>(operator, attribute.clone());
	}

}
