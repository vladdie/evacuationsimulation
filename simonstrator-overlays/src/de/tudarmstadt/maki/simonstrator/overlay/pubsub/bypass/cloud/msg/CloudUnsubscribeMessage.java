package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;

/**
 * Unsubscribe at the cloud
 * 
 * @author Bjoern Richerzhagen
 * @version May 3, 2014
 */
public class CloudUnsubscribeMessage extends AbstractOverlayMessage implements
		BypassCloudMessage {

	private static final long serialVersionUID = 1L;

	private Subscription[] subs;

	private transient long _size = -1;

	@SuppressWarnings("unused")
	private CloudUnsubscribeMessage() {
		// For Kryo
	}

	public CloudUnsubscribeMessage(OverlayContact sender, OverlayContact receiver, Subscription... subs) {
		super(sender, receiver);
		this.subs = subs;
	}

	public Subscription[] getSubscriptions() {
		return subs;
	}

	public OverlayContact getSubscriber() {
		return getSender(); // one hop!
	}

	@Override
	public Message getPayload() {
		return null;
	}

	@Override
	public long getSize() {
		if (_size == -1) {
			_size = super.getSize();
			for (Subscription subscription : subs) {
				_size += subscription.getTransmissionSize();
			}
		}
		return _size;
	}

}
