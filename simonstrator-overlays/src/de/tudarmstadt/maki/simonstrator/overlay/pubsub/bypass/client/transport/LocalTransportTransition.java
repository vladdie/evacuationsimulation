package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.transport;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import de.tudarmstadt.maki.simonstrator.api.component.transition.AtomicTransition;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;

/**
 * A Transition inbetween two {@link AbstractLocalTransport}s
 * 
 * @author Bjoern Richerzhagen
 * @version Sep 19, 2014
 */
public class LocalTransportTransition implements AtomicTransition<AbstractLocalTransport, AbstractLocalTransport> {

	@Override
	public Class<AbstractLocalTransport> getSourceType() {
		return AbstractLocalTransport.class;
	}

	@Override
	public Class<AbstractLocalTransport> getTargetType() {
		return AbstractLocalTransport.class;
	}

	private Map<Integer, TransMessageListener> listenersCache = new LinkedHashMap<>();

	@Override
	public void transferState(AbstractLocalTransport sourceComponent, AbstractLocalTransport targetComponent) {
		// Transfer listeners
		listenersCache = new LinkedHashMap<>(sourceComponent.getListenerByPort());
	}

	@Override
	public void transitionFinished(AbstractLocalTransport sourceComponent, AbstractLocalTransport targetComponent,
			boolean successful) {
		for (Entry<Integer, TransMessageListener> entry : listenersCache.entrySet()) {
			targetComponent.setTransMessageListener(entry.getValue(), entry.getKey());
		}
	}

}
