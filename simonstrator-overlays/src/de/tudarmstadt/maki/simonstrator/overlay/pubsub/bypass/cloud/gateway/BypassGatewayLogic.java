package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Interface for the gateway logic at the cloud/cloudlet in Bypass
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface BypassGatewayLogic {

	public BypassPubSubComponent getComponent();

	/**
	 * Returns the messages that are to be sent. If not gateway is used, a
	 * notification for every subscriber is created.
	 * 
	 * @param n
	 * @param subscribers
	 * @param numberOfGateways
	 *            allows overwriting the expected or enforced/upper number of
	 *            gateways. Set to <= 0 to use the strategie's default.
	 * @return
	 */
	public Collection<CloudNotifyMessage> packMessages(Notification n,
			OverlayContacts subscribers, int numberOfGateways);

}
