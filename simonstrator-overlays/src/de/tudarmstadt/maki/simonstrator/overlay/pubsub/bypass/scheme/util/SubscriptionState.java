package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util;

import java.util.HashSet;
import java.util.LinkedHashSet;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;

/**
 * 
 * State for a subscription (either locally or remotely)
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class SubscriptionState {

	protected final HashSet<PubSubListener> localListeners;

	protected final HashSet<OverlayContact> clients;

	public final Subscription subscription;

	protected final long timestampCreated;

	protected long expiresAt = -1;

	public SubscriptionState(Subscription subscription, long validityDuration) {
		this.clients = new LinkedHashSet<OverlayContact>();
		this.localListeners = new LinkedHashSet<PubSubListener>();
		this.subscription = subscription;
		this.timestampCreated = Time.getCurrentTime();
		if (validityDuration != -1) {
			this.expiresAt = timestampCreated + validityDuration;
		} 
	}

	public HashSet<PubSubListener> getLocalListeners() {
		return localListeners;
	}

	public SubscriptionState(Subscription subscription) {
		this(subscription, -1);
	}

	public void addLocalListener(PubSubListener listener) {
		this.localListeners.add(listener);
	}

	public void removeLocalListeners() {
		this.localListeners.clear();
	}

	public void addSubscriber(OverlayContact contact) {
		clients.add(contact);
	}

	public void removeSubscriber(OverlayContact contact) {
		clients.remove(contact);
	}

	public boolean hasClients() {
		return !clients.isEmpty();
	}

	public boolean hasLocalListeners() {
		return !localListeners.isEmpty();
	}

	public boolean isExpired() {
		return (expiresAt != -1) && expiresAt < Time.getCurrentTime();
	}

	public void notifyLocalListeners(Notification n) {
		for (PubSubListener listener : localListeners) {
			listener.onNotificationArrived(subscription, n);
		}
	}

	public HashSet<OverlayContact> getClients() {
		return clients;
	}

	public long getTimestampCreated() {
		return timestampCreated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((subscription == null) ? 0 : subscription.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubscriptionState other = (SubscriptionState) obj;
		if (subscription == null) {
			if (other.subscription != null)
				return false;
		} else if (!subscription.equals(other.subscription))
			return false;
		return true;
	}

}
