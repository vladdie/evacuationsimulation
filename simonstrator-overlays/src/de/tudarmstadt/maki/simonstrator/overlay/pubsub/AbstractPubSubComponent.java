package de.tudarmstadt.maki.simonstrator.overlay.pubsub;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.Monitor.Level;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.AttributeFilter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.OperatorImplementation;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicFilter;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicSubscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicTopic;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute.BasicAttribute;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute.filter.BasicAttributeFilter;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute.filter.DefaultFilters;

/**
 * Abstract Base-Class for a pub/sub component. Provides the Basic
 * implementations of most components as provided in .util - think twice before
 * changing this behavior, as it might lead to broken analyzers or applications
 * that rely on some common datastructures.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public abstract class AbstractPubSubComponent extends AbstractOverlayNode
		implements PubSubComponent {

	/**
	 * Holds operation implementations for filters used in subscriptions.
	 */
	private final Map<FilterOperator, Map<Class<?>, OperatorImplementation>> operatorImplementations;

	/**
	 * 
	 * @param host
	 * @param peerId
	 */
	public AbstractPubSubComponent(Host host) {
		super(host);
		this.operatorImplementations = new LinkedHashMap<FilterOperator, Map<Class<?>, OperatorImplementation>>();
		for (FilterOperator op : FilterOperator.values()) {
			operatorImplementations.put(op,
					new LinkedHashMap<Class<?>, OperatorImplementation>());
		}
	}

	@Override
	public Topic createTopic(String uri) {
		return new BasicTopic(uri);
	}

	@Override
	public Notification createNotification(Topic topic,
			List<Attribute<?>> attributes, byte[] payload) {
		return new BasicNotification(topic, attributes, payload);
	}

	@Override
	public Notification createNotification(Topic topic, byte[] payload) {
		return createNotification(topic, null, payload);
	}

	@Override
	public <T> Attribute<T> createAttribute(Class<T> type, String name, T value) {
		return new BasicAttribute<T>(type, name, value);
	}

	@Override
	public <T> AttributeFilter<T> createAttributeFilter(Attribute<T> attribute,
			FilterOperator operator) {
		return new BasicAttributeFilter<T>(operator, attribute);
	}

	@Override
	public Filter createFilter(List<AttributeFilter<?>> attributeFilters) {
		return new BasicFilter(attributeFilters);
	}

	@Override
	public Subscription createSubscription(Topic topic, Filter filter) {
		return new BasicSubscription(topic, filter);
	}

	@Override
	public Subscription createSubscription(Topic topic) {
		return createSubscription(topic, null);
	}

	@Override
	public <T> void setOperatorImplementation(FilterOperator operator,
			Class<T> operandType, OperatorImplementation implementation) {
		assert operatorImplementations.containsKey(operator);
		assert !operatorImplementations.get(operator).containsKey(operandType);
		operatorImplementations.get(operator).put(operandType, implementation);
		Monitor.log(PubSubComponent.class, Level.INFO,
				"Installed new filter for operand %s: %s", operator,
				implementation);
	}

	/**
	 * Returns the implementation for a given Operator, as used in the
	 * matches-function.
	 * 
	 * @param op
	 * @param type
	 * @return
	 */
	public <T> OperatorImplementation getOperatorImplementation(
			FilterOperator op, Class<T> type) {
		// have been loaded in the constructor
		assert operatorImplementations.containsKey(op);
		// Already set (e.g., by app or by prior call with a found default)
		Map<Class<?>, OperatorImplementation> candidates = operatorImplementations
				.get(op);
		OperatorImplementation impl = candidates.get(type);
		if (impl == null) {
			// Fallback to a default filter and save its reference
			impl = DefaultFilters.get(op, type);
			if (impl != null) {
				candidates.put(type, impl);
			} else {
				throw new AssertionError("No OperatorImplementation for "
						+ op.toString() + " available!");
			}
		}
		return impl;
	}

	/**
	 * Basic implementation of a matcher between subscriptions and
	 * notifications. Checks topics and all other attributes.
	 * 
	 * The contract of this matcher: if a filter specifies an attribute that is
	 * not contained in a notification, the notification does NOT match the
	 * filter.
	 * 
	 * @param notification
	 * @param subscription
	 * @return
	 */
	@Override
	public boolean matches(Notification notification, Subscription subscription) {
		assert subscription != null && subscription.getTopic() != null
				&& notification != null && notification.getTopic() != null;
		if (subscription.getTopic().includes(notification.getTopic())) {
			if (subscription.getFilter() != null) {
				if (notification.getAttributes() == null) {
					// Filter, but no matching attribute: false
					return false;
				}
				/*
				 * Policy: all filters need to match. If an attribute is
				 * specified by the filter but not contained in the
				 * notification, the notification does not match the
				 * subscription.
				 */
				for (AttributeFilter<?> filter : subscription.getFilter()
						.getAttributeFilters()) {
					if (!matches(notification.getAttributes(), filter)) {
						return false;
					}
				}
				return true;
			} else {
				// No filters: match only by topic
				return true;
			}
		}
		return false;
	}

	/**
	 * Inner part of the matches-method, matching a single attributeFilter
	 * against a notification. If the attribute is not contained in the
	 * notification, matches returns false.
	 * 
	 * @param notification
	 * @param attributeFilter
	 * @return
	 */
	public boolean matches(List<Attribute<?>> candidates,
			AttributeFilter<?> attributeFilter) {
		Attribute<?> candidate = null;
		for (Attribute<?> attr : candidates) {
			if (attr.getName().equals(attributeFilter.getAttribute().getName())) {
				candidate = attr;
				break;
			}
		}
		if (candidate == null) {
			// The attribute is not set in the notification.
			return false;
		}
		OperatorImplementation op = getOperatorImplementation(
				attributeFilter.getOperator(), attributeFilter.getAttribute()
						.getType());
		if (!op.matches(attributeFilter.getAttribute(), candidate)) {
			return false;
		}
		return true;
	}

}
