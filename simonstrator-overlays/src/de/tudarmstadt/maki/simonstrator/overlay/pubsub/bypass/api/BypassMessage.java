package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api;

import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayMessage;

/**
 * Marker interface for all messages that are exchanged between cloudlet and
 * local node and not directly between local nodes to allow assertion checks
 * 
 * @author Bjoern Richerzhagen
 * @version May 3, 2014
 */
public interface BypassMessage extends OverlayMessage {
	// marker
}
