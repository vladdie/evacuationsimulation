package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api;

import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;

/**
 * Simply intercept received messages
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface BypassMessageListener {

	/**
	 * A message arrived. Return true, if the message is exclusive to this
	 * listener (it will not be processed further)
	 * 
	 * @param msg
	 * @param sender
	 * @param commID
	 * @return
	 */
	public boolean messageArrived(BypassMessage msg, TransInfo sender, int commID);

}
