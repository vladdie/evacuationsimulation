package de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.AbstractPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized.msg.NotificationMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized.msg.SubscriptionMessage;

/**
 * The centralized Broker of the pub/sub system
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class CentralizedPubSubServer extends AbstractPubSubComponent implements TransMessageListener {

	protected final int port;

	private UDP udp = null;

	private OverlayContact localContact = null;

	private final SubscriptionStorage storage = new SubscriptionStorage();

	/**
	 * Needed, if the server itself wants to participate by registering
	 * subscriptions and positing notifications.
	 */
	private final Map<Subscription, PubSubListener> listeners = new LinkedHashMap<Subscription, PubSubListener>();

	/**
	 * 
	 * @param host
	 * @param peerId
	 */
	public CentralizedPubSubServer(Host host, int port) {
		super(host);
		this.port = port;
	}

	@Override
	public void initialize() {
		super.initialize();
		NetInterface net = getHost().getNetworkComponent().getByName(CentralizedPubSubFactory.netInterface);
		NetID localNet = net.getLocalInetAddress();
		try {
			udp = getAndBindUDP(localNet, port, new CentralizedPubSubSerializer());
			udp.setTransportMessageListener(this);
			localContact = new BasicOverlayContact(getHost().getId(), CentralizedPubSubFactory.netInterface,
					localNet, port);
		} catch (ProtocolNotAvailableException e) {
			throw new AssertionError("Protocol Binding failed!");
		}
	}

	@Override
	public OverlayContact getLocalOverlayContact() {
		return localContact;
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		setPeerStatus(PeerStatus.PRESENT);
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		setPeerStatus(PeerStatus.ABSENT);
	}

	@Override
	public void messageArrived(Message msg, TransInfo sender, int commID) {
		System.out.println("A Message from " + sender.toString() + " arrived at the central broker.");
		System.out.println(msg.toString());

		if (msg instanceof SubscriptionMessage) {
			handleSubscription((SubscriptionMessage) msg);
		} else if (msg instanceof NotificationMessage) {
			handleNotification((NotificationMessage) msg);
		}
	}

	/**
	 * Subsciption handling
	 * 
	 * @param sMsg
	 */
	protected void handleSubscription(SubscriptionMessage sMsg) {
		if (sMsg.isUnsubscribe()) {
			storage.removeSubscription(sMsg.getSubscriber(), sMsg.getSubscription());
		} else {
			storage.addSubscription(sMsg.getSubscriber(), sMsg.getSubscription());
		}
	}

	/**
	 * Notification handling
	 * 
	 * @param nMsg
	 */
	protected void handleNotification(NotificationMessage nMsg) {
		List<OverlayContact> receivers = storage.getSubscribers(nMsg.getNotification());
		for (OverlayContact receiver : receivers) {
			/*
			 * Assuming, that Notification Messages do NOT contain any state, we
			 * do not need to clone anything.
			 */
			if (receiver.equals(localContact)) {
				// Local delivery
				Event.scheduleImmediately(new EventHandler() {
					@Override
					public void eventOccurred(Object content, int type) {
						Notification notification = (Notification) content;
						for (Subscription sub : listeners.keySet()) {
							if (matches(notification, sub)) {
								listeners.get(sub).onNotificationArrived(sub, notification);
							}
						}
					}
				}, nMsg.getNotification(), 0);
			} else {
				// Remote delivery
				udp.send(nMsg, receiver.getNetID(CentralizedPubSubFactory.netInterface),
						receiver.getPort(CentralizedPubSubFactory.netInterface));
			}
		}
	}

	@Override
	public void publish(Notification notification) {
		handleNotification(new NotificationMessage(notification));
	}

	@Override
	public void advertise(Filter filter) {
		throw new AssertionError("The central broker does not support this operation.");
	}

	@Override
	public void unadvertise(Filter filter) {
		throw new AssertionError("The central broker does not support this operation.");
	}

	@Override
	public void subscribe(Subscription sub, PubSubListener listener) {
		listeners.put(sub, listener);
		handleSubscription(new SubscriptionMessage(sub, localContact));
	}

	@Override
	public void unsubscribe(Subscription sub, PubSubListener listener) {
		listeners.remove(sub);
	}

	private class SubscriptionStorage {

		private final List<SubscriptionInformation> subs;

		public SubscriptionStorage() {
			subs = new LinkedList<SubscriptionInformation>();
		}

		public void addSubscription(OverlayContact from, Subscription subscription) {
			subs.add(new SubscriptionInformation(from, subscription));
		}

		public void removeSubscription(OverlayContact from, Subscription subscription) {
			Iterator<SubscriptionInformation> it = subs.iterator();
			while (it.hasNext()) {
				SubscriptionInformation subInfo = it.next();
				if (subInfo.subscriber.equals(from) && subInfo.subscription.equals(subscription)) {
					it.remove();
				}
			}
		}

		public List<OverlayContact> getSubscribers(Notification notification) {
			List<OverlayContact> targets = new LinkedList<OverlayContact>();
			for (SubscriptionInformation sub : subs) {
				if (matches(notification, sub.subscription)) {
					targets.add(sub.subscriber);
				}
			}
			return targets;
		}

	}

	private class SubscriptionInformation {

		public final OverlayContact subscriber;

		public final Subscription subscription;

		public SubscriptionInformation(OverlayContact subscriber, Subscription subscription) {
			this.subscriber = subscriber;
			this.subscription = subscription;
		}

	}

}
