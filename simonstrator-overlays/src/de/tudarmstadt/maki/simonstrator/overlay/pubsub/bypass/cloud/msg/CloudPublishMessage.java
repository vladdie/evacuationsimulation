package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;

/**
 * A notification message sent from clients to the cloudlet/server, including
 * their current local distribution state.
 * 
 * @author Bjoern Richerzhagen
 * @version May 2, 2014
 */
public class CloudPublishMessage extends AbstractOverlayMessage implements
		BypassCloudMessage {

	private static final long serialVersionUID = 1L;

	private Notification n;

	private LocalDistributionType localDistributionType;

	@SuppressWarnings("unused")
	private CloudPublishMessage() {
		// For Kryo
	}

	public CloudPublishMessage(OverlayContact sender, OverlayContact receiver,
			Notification n, LocalDistributionType localDistributionType) {
		super(sender, receiver);
		this.n = n;
		this.localDistributionType = localDistributionType;
	}

	public Notification getNotification() {
		return n;
	}

	@Override
	public Message getPayload() {
		return null;
	}

	public LocalDistributionType getLocalDistributionType() {
		return localDistributionType;
	}

	@Override
	public long getSize() {
		return super.getSize() + n.getTransmissionSize();
	}

}
