package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.BasicNotification;

/**
 * A Notification that carries a location and a radius in which it is valid.
 * Might be the location of the client, but this is optional.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class LocationBasedNotification extends BasicNotification {

	private static final long serialVersionUID = 1L;

	private Location location;

	private boolean isPublisherLocation = false;

	private double radiusOfInterest;

	public LocationBasedNotification(Topic topic, List<Attribute<?>> attributes, byte[] payload, Location location,
			double radiusOfInterest, boolean isPublisherLocation) {
		super(topic, attributes, payload);
		this.location = location.clone();
		this.radiusOfInterest = radiusOfInterest;
		this.isPublisherLocation = isPublisherLocation;
	}

	@Override
	public int getTransmissionSize() {
		return super.getTransmissionSize() + location.getTransmissionSize() + Double.BYTES;
	}

	public Location getLocation() {
		return location;
	}

	public double getRadiusOfInterest() {
		return radiusOfInterest;
	}

	/**
	 * True, if the notification is a local one (i.e., issued to the location of
	 * the publisher.
	 * 
	 * @return
	 */
	public boolean isPublisherLocation() {
		return isPublisherLocation;
	}

}
