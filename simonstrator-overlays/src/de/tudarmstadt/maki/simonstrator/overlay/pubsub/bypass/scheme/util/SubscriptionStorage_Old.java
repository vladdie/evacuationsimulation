package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;

/**
 * Storage for subscriptions
 * 
 * @author Bjoern Richerzhagen
 * 
 * @param <T>
 *            {@link SubscriptionState}
 *
 */
public class SubscriptionStorage_Old<T extends SubscriptionState> {

	/**
	 * Register as StateCreationCallback
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 * @param <T>
	 */
	public interface StateCreationCallback<T extends SubscriptionState> {
		public T createSubscriptionState(Subscription sub);
	}

	private final LinkedHashMap<Subscription, T> subInfos;

	private final LinkedHashSet<T> localSubscriptions;

	private final LinkedHashSet<T> remoteSubscriptions;
	
	private final StateCreationCallback<T> stateCreationCallback;

	public SubscriptionStorage_Old(StateCreationCallback<T> stateCreationCallback) {
		this.subInfos = new LinkedHashMap<Subscription, T>();
		this.localSubscriptions = new LinkedHashSet<T>();
		this.remoteSubscriptions = new LinkedHashSet<T>();
		this.stateCreationCallback = stateCreationCallback;
	}

	/**
	 * Subscriptions that are locally relevant (i.e., the application registered
	 * a listener)
	 * 
	 * @return
	 */
	public LinkedHashSet<T> getLocalSubscriptions() {
		return localSubscriptions;
	}

	/**
	 * Subscriptions that have one or more remote clients.
	 * 
	 * @return
	 */
	public LinkedHashSet<T> getRemoteSubscriptions() {
		return remoteSubscriptions;
	}

	public T addSubscription(Subscription sub, OverlayContact client) {
		T info = getOrCreateSubscriptionState(sub);
		info.addSubscriber(client);
		remoteSubscriptions.add(info);
		return info;
	}

	public T addSubscription(Subscription sub, PubSubListener listener) {
		T info = getOrCreateSubscriptionState(sub);
		info.addLocalListener(listener);
		localSubscriptions.add(info);
		return info;
	}

	public T unsubscribe(Subscription sub, OverlayContact client) {
		T info = getOrCreateSubscriptionState(sub);
		if (client != null) {
			info.removeSubscriber(client);
		} else {
			info.removeLocalListeners();
		}
		cleanup(info);
		return info;
	}

	private void cleanup(T info) {
		// Update local subscriptions
		if (!info.hasLocalListeners()) {
			localSubscriptions.remove(info);
		}
		// Update remote subscriptions
		if (!info.hasClients()) {
			remoteSubscriptions.remove(info);
		}
		// remove empty subscriptions
		if (!info.hasClients() && !info.hasLocalListeners()) {
			subInfos.remove(info.subscription);
		}
		// remove timed-out subscriptions (only if no local sub!)
		if (!info.hasLocalListeners() && info.isExpired()) {
			subInfos.remove(info.subscription);
		}
	}

	private T getOrCreateSubscriptionState(Subscription subscription) {
		T info = subInfos.get(subscription);
		if (info == null) {
			info = stateCreationCallback.createSubscriptionState(subscription);
			subInfos.put(subscription, info);
		}
		return info;
	}

}
