package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.broadcast;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.Monitor;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.analyzer.PubSubNotificationForwardingAnalyzer;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent.LocalDistributionType;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.AbstractLocalDissemination;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.msg.BypassLocalMessage;

/**
 * Simple ONE-hop broadcast (e.g., a border case of the Flooding-dissemination)
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class BroadcastDissemination extends AbstractLocalDissemination {

	@TransferState(value = { "Component", "Port" })
	public BroadcastDissemination(BypassClientComponent comp, int port) {
		super(comp, port);
	}

	@Override
	public LocalDistributionType getLocalDistributionType() {
		return LocalDistributionType.BROADCAST;
	}

	@Override
	public void notify(Notification n, Collection<OverlayContact> subscribers) {
		super.notify(n, subscribers); // triggering analyzer
		BroadcastNotificationMsg msg = new BroadcastNotificationMsg(getLocalOverlayContact(), n);
		send(msg);
		getComponent().handleLocalNotification(n);
	}

	@Override
	protected boolean handleMessage(BypassLocalMessage msg, int commID) {
		assert getCurrentState() != DistrState.OFF;
		if (!(msg instanceof BroadcastNotificationMsg)) {
			return false;
		}
		assert !msg.getSender().equals(getLocalOverlayContact());
		BroadcastNotificationMsg bnm = (BroadcastNotificationMsg) msg;
		if (Monitor.hasAnalyzer(PubSubNotificationForwardingAnalyzer.class)) {
			Monitor.getOrNull(PubSubNotificationForwardingAnalyzer.class).onDropNotification(
					getComponent().getHost(), bnm.getNotification(), bnm.getSender().getNodeID(), 0);
		}
		getComponent().handleLocalNotification(bnm.getNotification());
		return true;
	}

}
