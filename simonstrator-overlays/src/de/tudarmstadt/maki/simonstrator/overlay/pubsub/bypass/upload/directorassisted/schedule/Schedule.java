package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.schedule;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;

import java.util.List;

/**
 * TODO
 * @autor Julian Wulfheide
 */
public interface Schedule extends Transmitable {

    /**
     * Returns an (ordered) list of client IDs who are responsible for uploading the given chunk.
     *
     * TODO make up mind about what an empty list means
     *      a) that chunk is uploaded by it's source alone (this would be best I think)
     *      b) that chunk is not uploaded at all (this way we would include overhead that is part of the source selection = not great)
     * @param comp currently needed, because the Filter matching is defined in {@link BypassPubSubComponent}
     * @param chunk TODO
     * @param neighbors TODO
     * @return TODO
     */
    List<Long> getUploaders(BypassPubSubComponent comp, Notification chunk, List<Long> neighbors);

}
