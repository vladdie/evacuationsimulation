package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.BypassCloudMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;

/**
 * A message carrying the information of a grid cell
 * 
 * @author Julian Zobel
 *
 */
public class GridCellUpdateMsg extends AbstractOverlayMessage implements BypassCloudMessage, BypassSchemeMessage {

	private static final long serialVersionUID = 1L;

	private BoundingRectangle cell;

	private SchemeName schemeName;

	public GridCellUpdateMsg(OverlayContact sender, OverlayContact receiver, BoundingRectangle r,
			SchemeName schemeName) {
		super(sender, receiver);
		cell = r;
		this.schemeName = schemeName;
	}

	@Override
	public Message getPayload() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public SchemeName getSchemeName() {
		return schemeName;
	}

	public BoundingRectangle getCellMBR() {
		return cell;
	}

	@Override
	public long getSize() {
		return super.getSize() + (cell != null ? cell.getTransmissionSize() : 0) + Byte.BYTES;
	}

}
