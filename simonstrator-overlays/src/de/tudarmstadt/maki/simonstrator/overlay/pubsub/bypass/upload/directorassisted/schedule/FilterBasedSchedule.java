package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted.schedule;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Filter;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.AttributeFilter;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.misc.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * TODO
 * @autor Julian Wulfheide
 */
public class FilterBasedSchedule extends AbstractSchedule {

    /**
     * The internal representation of the schedule. It maps {@link Filter}s to a lists of clientID/uploaderID. For a
     * notification to match a mapping, all filters in the list have to match. The first mapping that matches
     * is the one that will be returned, that means insertion order matters ({@link LinkedHashMap} keeps insertion
     * order).
     */
    private Map<Filter, List<Long>> schedule = new LinkedHashMap<>();

    private int sizeCached = -1;

    /**
     * TODO filter = null means it matches everything
     * TODO uploaderIds = null means direct upload
     * @param filter
     * @param uploaderIds
     */
    public void addMapping(@Nullable Filter filter, @NotNull List<Long> uploaderIds) {
        // TODO deep clone list of filters?
        schedule.put(filter, uploaderIds);
    }

    @Override
    @Nullable
    public List<Long> getUploaders(BypassPubSubComponent comp, Notification chunk, List<Long> clients) {
        for (Map.Entry<Filter, List<Long>> entry : schedule.entrySet()) {
            Filter filter = entry.getKey();
            List<Long> uploaders = entry.getValue();

            if (matches(filter, chunk, comp)) {
                return new ArrayList<>(uploaders);
            }
        }
        return null;
    }

    private boolean matches(Filter filter, Notification chunk, BypassPubSubComponent comp) {
        if (filter == null) {
            return true;
        }
        List<Attribute<?>> attributes = chunk.getAttributes();

        for (AttributeFilter attributeFilter : filter.getAttributeFilters()) {
            if (!comp.matches(attributes, attributeFilter)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public int getTransmissionSize() {
        if (sizeCached == -1) {
            // Calculate size once
            sizeCached = 0;

            for (Map.Entry<Filter, List<Long>> entry : schedule.entrySet()) {
                Filter f = entry.getKey();
                List uploaders = entry.getValue();
                if (f != null) {
                    sizeCached += f.getTransmissionSize();
                }
                sizeCached += uploaders.size() * Long.BYTES;
            }
        }

        return sizeCached;
    }

    public String toString() {
        return schedule.size() + " Filters";
    }

    public String prettyPrint() {
        StringBuilder sb = new StringBuilder();
        sb.append("FilterBasedSchedule (" + schedule.size() + " Filters)\n");
        for (Map.Entry<Filter, List<Long>> entry : schedule.entrySet()) {
            Filter filter = entry.getKey();
            List<Long> uploaders = entry.getValue();
            String sFilter = filter == null ? "*" : filter.toString();

            String sUploader;
            if (uploaders == null) {
                sUploader = "drop";
            } else if (uploaders.isEmpty()) {
                sUploader = "direct";
            } else {
                sUploader = Arrays.toString(uploaders.toArray());
            }

            sb.append("\t" + sFilter + " -> " + sUploader);
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof FilterBasedSchedule)) {
            return false;
        } else {
            return ((FilterBasedSchedule) obj).schedule.equals(this.schedule);
        }
    }

    @Override
    public int hashCode() {
        return schedule.hashCode();
    }

}
