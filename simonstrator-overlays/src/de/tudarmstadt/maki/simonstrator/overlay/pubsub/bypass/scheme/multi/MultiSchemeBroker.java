package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.multi;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BrokerSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction.AttractionSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction.AttractionSubscriptionStorage.AssignmentListener;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSchemeBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationUpdateMsg;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions.DefaultLocationBasedTransitionClient;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlan;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.AtomicTransitionAction;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * A wrapper used at the broker if multiple subscription schemes are to be
 * supported in parallel. This scheme can only be used on brokers. It manages a
 * set of internal subscription schemes and takes care of moving client
 * subscriptions in-between schemes.
 * 
 * For the set of schemes currently supported, this only makes sense in
 * conjunction with the ATTRACTION-scheme. Therefore, we fix one instance to
 * always use ATTRACTION at the broker.
 * 
 * FIXME keep in mind, that internal schemes will also register as message
 * listener. This can lead to some unintended side effects.
 * 
 * TODO implementation
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class MultiSchemeBroker extends AbstractLocationBasedSubscriptionScheme.Broker implements AssignmentListener {

	private AttractionSchemeBroker attraction;

	private BrokerSubscriptionScheme secondScheme;

	private MultiSchemeStorage storage;

	private static final String proxyName = "BYPASS_MULTI_SCHEME_INTERNAL";

	private Map<OverlayContact, SchemeName> activeScheme;

	private TransitionCoordinator tCoord;

	private List<MultiSchemeListener> listeners = new LinkedList<>();

	@TransferState({ "Component" })
	public MultiSchemeBroker(BypassCloudComponent component) {
		super(component, SchemeName.MULTI);
		this.storage = new MultiSchemeStorage(component);
		this.activeScheme = new LinkedHashMap<>();
	}

	@Override
	public void startMechanism(Callback cb) {
		/*
		 * Workaround: if this scheme is statically configured, we need to add
		 * custom transitions to the clients. In this scenario, we assume that
		 * no transitions are executed from MULTI to other broker schemes. For
		 * the demo, custom transitions are registered within the visualization.
		 */
		if (BypassSubscriptionSchemeConfiguration.scheme == SchemeName.MULTI
				&& !BypassSubscriptionSchemeConfiguration.disableStateTransfer) {
			for (Host host : Oracle.getAllHosts()) {
				try {
					BypassClientComponent client = host.getComponent(BypassClientComponent.class);
					client.getTransitionEngine().registerTransition(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME,
							new DefaultLocationBasedTransitionClient());
				} catch (ComponentNotAvailableException e) {
					//
				}
			}
		}
		/*
		 * For the Attraction scheme we do not use the T-Engine, as it is static
		 */
		this.attraction = new AttractionSchemeBroker(getComponent());
		this.attraction.startMechanism(new Callback() {
			@Override
			public void finished(boolean successful) {
				/*
				 * Register own scheme as listener for assignment updates.
				 */
				attraction.getStorage().setAssignmentListener(MultiSchemeBroker.this);
			}
		});
		// previous instances should have been destroyed.
		assert getComponent().getTransitionEngine().getProxy(proxyName, LocationBasedSchemeBroker.class) == null;
		LocationBasedSchemeBroker lbs = new LocationBasedSchemeBroker(getComponent());
		lbs.getStorage().importSubscriptions(getStorage());
		this.secondScheme = getComponent().getTransitionEngine().createMechanismProxy(BrokerSubscriptionScheme.class,
				lbs, proxyName);
		try {
			this.tCoord = getComponent().getHost().getComponent(TransitionCoordinator.class);
		} catch (ComponentNotAvailableException e) {
			throw new AssertionError("TransitionCoordinator is required.");
		}
		super.startMechanism(cb);
	}

	@Override
	public void stopMechanism(Callback cb) {
		this.attraction.stopMechanism(new Callback() {
			@Override
			public void finished(boolean successful) {
				// don't care
			}
		});
		this.attraction = null;
		getComponent().getTransitionEngine().destroyMechanismProxy(proxyName);
		this.secondScheme = null;
		this.activeScheme.clear();
		super.stopMechanism(cb);
	}

	@Override
	public void updateAttractionPointAssociation(OverlayContact subscriber,
			Set<AttractionPoint> associatedAttractionPoints) {
		/*
		 * Listener which is called each time a node is associated to a new AP
		 * by the AP-Scheme. This is also called if a node is no longer in reach
		 * of any AP -> this would need to trigger the transition.
		 */
		if (associatedAttractionPoints.isEmpty()) {
			switchToSecondScheme(subscriber);
		}
	}

	@Override
	public void onSubscribeAtBroker(Subscription sub, OverlayContact subscriber) {
		/*
		 * We intercept these calls to forward them to the correct internal
		 * scheme. Ensure, that the subscription storage in this scheme is
		 * updated as well by calling super.
		 */
		super.onSubscribeAtBroker(sub, subscriber);

		if (activeScheme.containsKey(subscriber)) {
			SchemeName name = activeScheme.get(subscriber);
			if (name.equals(SchemeName.ATTRACTION)) {
				attraction.onSubscribeAtBroker(sub, subscriber);
			} else {
				secondScheme.onSubscribeAtBroker(sub, subscriber);
			}
		} else {
			/*
			 * We subscribe assuming that each client initially uses the
			 * NON-Attraction scheme.
			 */
			secondScheme.onSubscribeAtBroker(sub, subscriber);
		}

	}

	@Override
	public void onUnsubscribeAtBroker(Subscription sub, OverlayContact subscriber) {
		/*
		 * We intercept these calls to forward them to the correct internal
		 * scheme. Ensure, that the subscription storage in this scheme is
		 * updated as well by calling super.
		 */
		super.onUnsubscribeAtBroker(sub, subscriber);

		if (activeScheme.containsKey(subscriber)) {
			SchemeName name = activeScheme.get(subscriber);
			if (name.equals(SchemeName.ATTRACTION)) {
				attraction.onUnsubscribeAtBroker(sub, subscriber);
			} else {
				secondScheme.onUnsubscribeAtBroker(sub, subscriber);
			}
		} else {
			/*
			 * We subscribe assuming that each client initially uses the
			 * NON-Attraction scheme.
			 */
			secondScheme.onUnsubscribeAtBroker(sub, subscriber);
		}
	}

	@Override
	public OverlayContacts getSubscribers() {
		/*
		 * Here, we simply use our storage (only full set of subscribers needed)
		 */
		return storage.getSubscribers();
	}

	@Override
	public OverlayContacts getSubscribers(Notification notification) {
		/*
		 * Now this is the matching process. We need to ask both schemes and
		 * merge the results.
		 */
		OverlayContacts result = new OverlayContacts();
		result.addAll(attraction.getSubscribers(notification));
		result.addAll(secondScheme.getSubscribers(notification));
		return result;
	}

	/**
	 * Return all ATTRACT subscribers sorted by attraction point
	 * 
	 * @param notification
	 * @return
	 */
	public Map<AttractionPoint, Set<OverlayContact>> getAllAttractionSubscribers() {
		return attraction.getStorage().getAttractionPointSubscribers();
	}


	/**
	 * Return all SECOND_SCHEME (LBS) subscribers
	 * 
	 * @param notification
	 * @return
	 */
	public OverlayContacts getAllSecondSchemeSubscribers() {
		return secondScheme.getSubscribers();
	}

	/**
	 * Return all ATTRACT subscribers for the given notification
	 * 
	 * @param notification
	 * @return
	 */
	public OverlayContacts getAttractionSubscribers(Notification notification) {
		/*
		 * We implicitly assume that attraction radii do not overlap...
		 */
		OverlayContacts result = new OverlayContacts();
		result.addAll(attraction.getSubscribers(notification));
		return result;
	}

	/**
	 * Return all SECOND_SCHEME (LBS) subscribers for the given notification
	 * 
	 * @param notification
	 * @return
	 */
	public OverlayContacts getSecondSchemeSubscribers(Notification notification) {
		/*
		 * We implicitly assume that attraction radii do not overlap...
		 */
		OverlayContacts result = new OverlayContacts();
		result.addAll(secondScheme.getSubscribers(notification));
		return result;
	}

	@Override
	public MultiSchemeStorage getStorage() {
		return storage;
	}

	@Override
	protected boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {
		/*
		 * We simply do some "wire-tapping" to wait for incoming location update
		 * messages. If we detect that a client should switch schemes based on
		 * his location, we trigger the transition on the client and move the
		 * subscription between our broker schemes.
		 * 
		 * This is only done for clients who are NOT using ATTRACTION to detect
		 * cases where we need to transition TO ATTRACTION. The reverse case is
		 * realized via the assignmentListener registered within the ATTRACTION
		 * storage.
		 */

		if ((msg instanceof LocationUpdateMsg)) {
			Location loc = ((LocationUpdateMsg) msg).getLocation();
			/*
			 * Case 1: client should switch to ATTRACTION now
			 */
			if (!msg.getSchemeName().equals(SchemeName.ATTRACTION)) {
				// probe against set of attraction points.
				AttractionPoint ap = attraction.getStorage().isWithinAttractionPointReach(loc);
				if (ap != null) {
					// TODO YES -> Transition.
					OverlayContact subscriber = ((LocationUpdateMsg) msg).getSender();
					switchToAttraction(subscriber, ap);
				}
			}
			/*
			 * Case 2: client already uses ATTRACTION, and just reported an
			 * updated location, which we will store in our Storage for access
			 * by the SiS.
			 */
			if (msg.getSchemeName().equals(SchemeName.ATTRACTION)) {
				this.storage.updateLocationOfClient(((LocationUpdateMsg) msg).getSender(), loc);
			}
		}

		return false;
	}

	/**
	 * Switch to the Second Scheme for a given subscriber.
	 * 
	 * @param subscriber
	 */
	protected void switchToSecondScheme(OverlayContact subscriber) {
		if (activeScheme.containsKey(subscriber) && activeScheme.get(subscriber).equals(secondScheme.getName())) {
			/*
			 * Already triggered.
			 */
			// System.err.println(
			// "stopped an already triggered transition TO " +
			// secondScheme.getName() + " for client "
			// + subscriber.toString());
			return;
		}
		activeScheme.put(subscriber, secondScheme.getName());
		// System.out.println("starting transition TO " + secondScheme.getName()
		// + " for client " + subscriber.toString());

		/*
		 * Transfer subscriptions.
		 */
		Set<Subscription> subs = new LinkedHashSet<>(attraction.getSubscriptionsFrom(subscriber));
		for (Subscription sub : subs) {
			attraction.onUnsubscribeAtBroker(sub, subscriber);
			secondScheme.onSubscribeAtBroker(sub, subscriber);
		}

		/*
		 * Trigger client transition
		 */
		TransitionExecutionPlan plan = tCoord.createExecutionPlan();
		plan.addAction(new AtomicTransitionAction(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME,
				secondScheme.getName().clientClass));

		/*
		 * Give listeners a chance to add actions to the transitions plan.
		 */
		listeners.forEach(listener -> listener.onSwitchClient(subscriber, secondScheme.getName(), plan, null));

		tCoord.executeTransitionPlan(Collections.singletonList(subscriber), plan);
	}

	/**
	 * Starts a transition to the ATTRACTION scheme for a given subscriber.
	 * 
	 * @param subscriber
	 */
	protected void switchToAttraction(OverlayContact subscriber, AttractionPoint ap) {
		if (activeScheme.containsKey(subscriber) && activeScheme.get(subscriber).equals(attraction.getName())) {
			/*
			 * Already triggered.
			 */
			// System.err.println(
			// "stopped an already triggered transition TO ATTRACTION for client
			// " + subscriber.toString());
			return;
		}
		activeScheme.put(subscriber, attraction.getName());
		// System.out.println("starting transition TO ATTRACTION for client " +
		// subscriber.toString());

		/*
		 * Transfer subscriptions.
		 */
		Set<Subscription> subs = new LinkedHashSet<>(secondScheme.getSubscriptionsFrom(subscriber));
		for (Subscription sub : subs) {
			secondScheme.onUnsubscribeAtBroker(sub, subscriber);
			attraction.onSubscribeAtBroker(sub, subscriber);
		}

		/*
		 * Trigger client transition
		 */
		TransitionExecutionPlan plan = tCoord.createExecutionPlan();
		plan.addAction(new AtomicTransitionAction(
				BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME, attraction.getName().clientClass));

		/*
		 * Give listeners a chance to add actions to the transitions plan.
		 */
		listeners.forEach(listener -> listener.onSwitchClient(subscriber, attraction.getName(), plan, ap));

		tCoord.executeTransitionPlan(Collections.singletonList(subscriber), plan);
	}

	public void addMultiSchemeListener(MultiSchemeListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	public void removeMultiSchemeListener(MultiSchemeListener listener) {
		listeners.remove(listener);
	}

	/**
	 * We provide a basic scheme storage used during transitions from and to the
	 * MULTI-scheme at the broker.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public class MultiSchemeStorage extends LocationBasedSubscriptionStorage {

		public MultiSchemeStorage(BypassPubSubComponent comp) {
			super(comp);
		}

		@Override
		protected int getLBSMatchComplexity(LocationBasedNotification notification) {
			int complexity = attraction.getStorage().getMatchComplexity(notification);
			complexity += secondScheme.getStorage().getMatchComplexity(notification);
			return complexity;
		}

	}

	/**
	 * Enable other components to react to client scheme switches.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	public interface MultiSchemeListener {

		/**
		 * Notifies the listener whenever we are in the process of switching a
		 * client's scheme.
		 * 
		 * @param client
		 * @param newScheme
		 * @param plan
		 *            you may add new actions to the transition plan.
		 * @param ap
		 *            or null
		 */
		public void onSwitchClient(OverlayContact client, SchemeName newScheme, TransitionExecutionPlan plan,
				AttractionPoint ap);

	}

}
