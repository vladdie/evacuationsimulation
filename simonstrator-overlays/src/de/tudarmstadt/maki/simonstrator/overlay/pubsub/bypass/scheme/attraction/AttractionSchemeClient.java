package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.api.component.transition.MechanismState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.ChannelBasedNotification;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;

/**
 * This scheme basically behaves as a special form of the grid scheme with
 * channels assigned to mobility model attraction points. Clients are subscribed
 * to one or multiple channels corresponding to attraction points nearby. All
 * clients that are not within reach of any one of the attraction point channels
 * are simply grouped into an "other" channel, receiving all messages of
 * unassigned nodes. While this is not a very clever design, it is intentionally
 * done this way, as this scheme is to be used in conjunction with another
 * scheme (for example STE or LBS); and the broker should take care of executing
 * transitions from this scheme to LBS/STE for clients that are within the
 * "other" channel.
 * 
 * For debug purposes, this scheme can behave just like a hybrid with the LBS by
 * setting the respective configuration parameter.
 * 
 * Assignment to an attraction-point cell is done locally by the client (the
 * broker therefore distributes a set of attraction points). Clients in this
 * scheme DO NOT need to send their location to the broker, instead they only
 * send the set of attraction point names (channels) they want to subscribe to.
 * 
 * @author Julian Zobel, Bjoern Richerzhagen
 *
 */
public class AttractionSchemeClient extends AbstractLocationBasedSubscriptionScheme.Client {

	/**
	 * Set of ALL attraction points, sent by the broker.
	 */
	private Set<AttractionPoint> attractionPoints = new LinkedHashSet<>();

	/**
	 * Set of attraction points we are currently subscribed to (using their
	 * names).
	 */
	private Set<AttractionPoint> subscribedAttractionPoints = new LinkedHashSet<>();

	public static boolean onlySingleSubscription = false;

	/**
	 * If true, the client still periodically reports location updates to the
	 * broker. This may
	 */
	@MechanismState("ReportLocationUpdates")
	private boolean reportLocationUpdates = false;

	/**
	 * Distance between last reported location and current location before an
	 * update is sent when reporting is activated. This is only used by the
	 * MULTI-scheme during the COEXIST evaluation to gather location data for
	 * the gateway selection process.
	 */
	@MechanismState("LocationUpdateThreshold")
	private int locationUpdateThreshold = 0;

	private double distanceSinceLastUpdate = 0;

	@TransferState({ "Component" })
	public AttractionSchemeClient(BypassPubSubComponent component) {
		super(component, SchemeName.ATTRACTION);
	}

	/**
	 * For local state transition.
	 * 
	 * @param reportLocationUpdates
	 */
	public void setReportLocationUpdates(boolean reportLocationUpdates) {
		this.reportLocationUpdates = reportLocationUpdates;
	}

	/**
	 * For state transitions
	 * 
	 * @param locationUpdateThreshold
	 */
	public void setLocationUpdateThreshold(int locationUpdateThreshold) {
		this.locationUpdateThreshold = locationUpdateThreshold;
	}

	@Override
	protected void onLocationChanged(Location newLocation, Location oldLocation, long timeSinceLastUpdate,
			double distanceSinceLastUpdate) {
		/*
		 * Send an update message to our broker, carrying the new location. This
		 * method is invoked periodically on a host, if the host is a
		 * location-based subscriber. He has to send his current position to the
		 * broker in order for the broker to update its state.
		 * 
		 * In the case of this scheme, the location is only sent if hybrid
		 * operation is enabled for testing purposes. Otherwise, the client uses
		 * its local list of attraction points to subscribe to the correct
		 * channels.
		 */
		boolean sentUpdate = checkAttractionPointAssignment();
		boolean sentLocation = false;
		if (BypassSubscriptionSchemeConfiguration.attractionEnableLBSHybrid) {
			if (subscribedAttractionPoints.isEmpty()) {
				// send location, if we are outside AP range
				sendLocationUpdateMessage();
				sentLocation = true;
			}
		}

		/*
		 * ATTRACT can be configured to still report client locations
		 * periodically.
		 */
		if (reportLocationUpdates && !sentLocation && !sentUpdate
				&& this.distanceSinceLastUpdate > locationUpdateThreshold) {
			sendLocationUpdateMessage();
			this.distanceSinceLastUpdate = 0;
		}
		this.distanceSinceLastUpdate += distanceSinceLastUpdate;
	}

	/**
	 * 
	 * @param distanceSinceLastUpdate
	 * @return true, if we sent an update to the broker component.
	 */
	private boolean checkAttractionPointAssignment() {
		/*
		 * Any APs at all?
		 */
		if (attractionPoints.isEmpty()) {
			// Request APs from Broker, stop here.
			getComponent().sendViaMobileNetwork(new AttractionSubscriptionMsg(getComponent().getLocalOverlayContact(),
					getComponent().getBrokerContact(), attractionPoints, true));
			return false;
		}


		boolean sendUpdate = false;

		if (BypassSubscriptionSchemeConfiguration.attractionOnlyOneSubscription) {
			/*
			 * Check Attraction Point assignments (case where the client can
			 * subscribe to only a single AP)
			 */
			AttractionPoint currentAP = subscribedAttractionPoints.isEmpty() ? null : subscribedAttractionPoints.iterator().next();
			double currentDistance = currentAP != null ? currentAP.distanceTo(getCurrentLocation()) : Double.MAX_VALUE;
			for (AttractionPoint ap : attractionPoints) {
				if (ap.distanceTo(getCurrentLocation()) <= ap.getRadius()) {
					// within reach
					if (ap.distanceTo(getCurrentLocation()) < currentDistance) {
						subscribedAttractionPoints.clear();
						subscribedAttractionPoints.add(ap);
						assert !ap.equals(currentAP);
						currentAP = ap;
						currentDistance = ap.distanceTo(getCurrentLocation());
						// update subscription at broker
						sendUpdate = true;
					}
				} else {
					if (subscribedAttractionPoints.contains(ap)) {
						subscribedAttractionPoints.remove(ap);
						// update subscription at broker
						sendUpdate = true;
					}
				}
			}
		} else {
			/*
			 * Check Attraction Point assignments (case where the client can
			 * subscribe to multiple APs at once)
			 */
			for (AttractionPoint ap : attractionPoints) {
				if (ap.distanceTo(getCurrentLocation()) <= ap.getRadius()) {
					// within reach
					if (!subscribedAttractionPoints.contains(ap)) {
						subscribedAttractionPoints.add(ap);
						// update subscription at broker
						sendUpdate = true;
					}
				} else {
					if (subscribedAttractionPoints.contains(ap)) {
						subscribedAttractionPoints.remove(ap);
						// update subscription at broker
						sendUpdate = true;
					}
				}
			}
		}
		// Send updated list of subscribed APs
		if (sendUpdate) {
			getComponent().sendViaMobileNetwork(new AttractionSubscriptionMsg(getComponent().getLocalOverlayContact(),
					getComponent().getBrokerContact(), subscribedAttractionPoints, false));
		}

		return sendUpdate;
	}

	@Override
	protected boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {
		if (msg instanceof AttractionAnnounceMsg) {
			// Retrieved an updated list of AttractionPoints
			AttractionAnnounceMsg aMsg = (AttractionAnnounceMsg) msg;
			this.attractionPoints = aMsg.getAttractionPoints();
			checkAttractionPointAssignment();
			return true;
		}
		return false;
	}

	@Override
	public Notification createLocalNotification(Topic topic, List<Attribute<?>> attributes, double radiusOfInterest,
			byte[] payload) {
		if (!subscribedAttractionPoints.isEmpty()) {
			Set<String> channels = new LinkedHashSet<>();
			for (AttractionPoint attractionPoint : subscribedAttractionPoints) {
				channels.add(attractionPoint.getName());
			}
			return new ChannelBasedNotification(topic, attributes, payload, getCurrentLocation(), radiusOfInterest,
					channels);
		} else {
			return new LocationBasedNotification(topic, attributes, payload, getCurrentLocation(), radiusOfInterest,
					true);
		}
	}

}
