package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.attraction;

import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;

/**
 * Container for an attraction point used by the {@link AttractionSchemeBroker}
 * to determine channels.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class AttractionPoint {

	private final String name;

	private final Location location;

	public AttractionPoint(String name, Location location) {
		this.name = name;
		this.location = location;
	}

	public String getName() {
		return name;
	}

	public Location getLocation() {
		return location;
	}

}
