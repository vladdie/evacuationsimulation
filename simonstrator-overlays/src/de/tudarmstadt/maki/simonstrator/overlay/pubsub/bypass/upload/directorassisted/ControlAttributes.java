package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload.directorassisted;

/**
 * TODO
 * @author Julian Wulfheide
 */
public class ControlAttributes {
    public final static String TOPIC_ROOT_CONTROL = "/control/";

    public final static String CLIENT_ID = "client-id";
    public final static String SCHEDULE = "schedule";
}
