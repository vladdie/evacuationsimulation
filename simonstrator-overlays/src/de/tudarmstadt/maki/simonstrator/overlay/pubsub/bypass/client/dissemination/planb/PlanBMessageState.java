package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.dissemination.planb;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;

public class PlanBMessageState {

	private int parentCount;
	private int siblingCount;
	private INodeID parentID;
	private double proximity;
	private boolean canceled;
	
	public PlanBMessageState()
	{
		parentCount = 0;
		siblingCount = 0;
		parentID = null;
		proximity = -1;
		canceled = false;
	}
	
	public void increaseParentCount(){
		parentCount++;
	}
	
	public void increaseSiblingCount(){
		siblingCount++;
	}
	
	public void setParentID(INodeID id) {
		parentID = id;
	}
	
	public void setProximity(double proximity){
		this.proximity = proximity;
	}
	
	public void cancel(){
		canceled = true;
	}
	
	public int  getParentCount(){
		return parentCount;
	}
	
	public int getSiblingCount(){
		return siblingCount;
	}
	
	public INodeID getParentID() {
		return parentID;
	}
	
	public double getProximity(){
		return proximity;
	}
	
	public boolean getCanceled(){
		return canceled;
	}
}
