package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.upload;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassClientComponent;

/**
 * Simple strategy that just uploads the respective events directly.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class DirectUploadStrategy extends AbstractUploadStrategy {

	@TransferState({ "Component" })
	public DirectUploadStrategy(BypassClientComponent comp) {
		super(comp);
	}

	@Override
	public void uploadNotification(Notification n) {
		doUpload(n);
	}

}
