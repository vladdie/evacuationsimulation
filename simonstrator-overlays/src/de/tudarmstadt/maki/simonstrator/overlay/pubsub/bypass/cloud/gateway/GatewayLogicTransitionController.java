package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.cd.CD_BECLEACH_DBScan;
import de.tudarmstadt.maki.simonstrator.service.sis.clustering.strategy.d1c.D1C_Random;

/**
 * This class interacts with the {@link TransitionEnabledGatewayLogic} to start
 * transitions between the different schemes based on any kind of input metric.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class GatewayLogicTransitionController {

	protected final TransitionEnabledGatewayLogic logic;

	protected final TransitionEngine engine;

	public GatewayLogicTransitionController(TransitionEnabledGatewayLogic logic) throws ComponentNotAvailableException {
		this.logic = logic;
		this.engine = logic.getComponent().getHost().getComponent(TransitionEngine.class);
		Event.scheduleWithDelay(2 * Time.MINUTE, new EventHandler() {

			@Override
			public void eventOccurred(Object content, int type) {
				engine.executeAtomicTransition(TransitionEnabledGatewayLogic.COMP_NAME, D1C_Random.class);
			}
		}, null, 0);

		Event.scheduleWithDelay(5 * Time.MINUTE, new EventHandler() {

			@Override
			public void eventOccurred(Object content, int type) {
				engine.executeAtomicTransition(TransitionEnabledGatewayLogic.COMP_NAME, CD_BECLEACH_DBScan.class);
			}
		}, null, 0);
	}

}
