package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Basic implementation of a subscription storage used by a broker to maintain
 * remote subscriptions.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class SubscriptionStorage {

	private BypassPubSubComponent component;

	/*
	 * For more efficient filtering: in the default case, maintain ONE
	 * subscription and a list of clients subscribed to the subscription.
	 */
	// private LinkedHashMap<OverlayContact, Set<Subscription>> subsBySubscriber
	// = new LinkedHashMap<>();

	private LinkedHashMap<Subscription, Set<OverlayContact>> subscriptions = new LinkedHashMap<>();;

	public SubscriptionStorage(BypassPubSubComponent comp) {
		this.component = comp;
	}

	/**
	 * For convenience during state transfers. Just transfers all subscriptions
	 * to the data structures of the new storage.
	 * 
	 * @param storage
	 */
	public void importSubscriptions(SubscriptionStorage storage) {
		for (Entry<Subscription, Set<OverlayContact>> subscription : storage.subscriptions.entrySet()) {
			for (OverlayContact subscriber : subscription.getValue()) {
				addSubscription(subscriber, subscription.getKey());
			}
		}
	}

	/**
	 * Returns the number of match operations that need to be performed (a
	 * complexity measure).
	 * 
	 * @param the
	 *            notification to match
	 * @return
	 */
	public int getMatchComplexity(Notification notification) {
		return subscriptions.size();
	}

	/**
	 * Reference to the BypassComponent
	 * 
	 * @return
	 */
	public BypassPubSubComponent getComponent() {
		return component;
	}

	/**
	 * Remove a subscription
	 * 
	 * @param subscriber
	 * @param subscription
	 * @param returns
	 *            true, if there is NO other subscription for this client (can
	 *            be used to delete client-specific state etc.)
	 */
	public boolean removeSubscription(OverlayContact subscriber, Subscription subscription) {
		Set<OverlayContact> subscribers = subscriptions.get(subscription);
		if (subscribers != null) {
			subscribers.remove(subscriber);
			if (subscribers.isEmpty()) {
				// no more subscribers, remove subscription
				subscriptions.remove(subscription);
				return true;
			}
		}
		return false;
	}

	/**
	 * Add a subscription to the storage
	 * 
	 * @param contact
	 *            The overlay contact of that subscription
	 * @param xsub
	 *            The subscription to be added
	 */
	public void addSubscription(OverlayContact subscriber, Subscription subscription) {
		if (!subscriptions.containsKey(subscription)) {
			subscriptions.put(subscription, new LinkedHashSet<>());
		}
		subscriptions.get(subscription).add(subscriber);
	}

	/**
	 * Returns the set of subscriptions that were issued by an overlay contact
	 * 
	 * @param contact
	 *            The overlay contact
	 * @return Set of subscriptions from this overlay contact
	 */
	public Set<Subscription> getSubscriptionsFromContact(OverlayContact contact) {
		Set<Subscription> allSubsByClient = new LinkedHashSet<>();
		for (Entry<Subscription, Set<OverlayContact>> entry : subscriptions.entrySet()) {
			if (entry.getValue().contains(contact)) {
				allSubsByClient.add(entry.getKey());
			}
		}
		return allSubsByClient;
	}

	/**
	 * Returns the set of subscribers to the given {@link Notification}
	 * 
	 * @param notification
	 * @return
	 */
	public OverlayContacts getSubscribers(Notification notification) {
		OverlayContacts subscribers = new OverlayContacts();
		for (Entry<Subscription, Set<OverlayContact>> entry : subscriptions.entrySet()) {
			if (getComponent().matches(notification, entry.getKey())) {
				subscribers.addAll(entry.getValue());
			}
		}
		return subscribers;
	}

	/**
	 * Return all subscribers in this storage
	 * 
	 * @return OverlayContacts inside the storage
	 */
	public OverlayContacts getSubscribers() {
		OverlayContacts subscribers = new OverlayContacts();
		for (Entry<Subscription, Set<OverlayContact>> entry : subscriptions.entrySet()) {
			subscribers.addAll(entry.getValue());
		}
		return subscribers;
	}

}
