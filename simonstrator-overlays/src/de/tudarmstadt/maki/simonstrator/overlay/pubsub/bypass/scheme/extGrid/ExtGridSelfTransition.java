package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid;

import de.tudarmstadt.maki.simonstrator.api.component.transition.SelfTransition;

/**
 * A self-transition that updates the current grid sizes.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class ExtGridSelfTransition implements SelfTransition<ExtGridSchemeBroker> {

	private int x = 0;
	private int y = 0;

	public ExtGridSelfTransition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public void alterState(ExtGridSchemeBroker scheme) {
		scheme.getStorage().setAndAdjustGridCells(x, y);
	}

}
