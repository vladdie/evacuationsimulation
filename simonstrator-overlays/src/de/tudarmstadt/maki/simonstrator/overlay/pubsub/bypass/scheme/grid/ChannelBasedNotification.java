package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.Location;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationBasedNotification;

/**
 * Add a string-based channel to the notification
 * 
 * @author Bjoern Richerzhagen
 *
 */
public class ChannelBasedNotification extends LocationBasedNotification {

	private static final long serialVersionUID = 1L;

	private final Set<String> channels;

	public ChannelBasedNotification(Topic topic, List<Attribute<?>> attributes, byte[] payload, Location location,
			double radiusOfInterest, String singleChannel) {
		this(topic, attributes, payload, location, radiusOfInterest, Collections.singleton(singleChannel));
	}

	public ChannelBasedNotification(Topic topic, List<Attribute<?>> attributes, byte[] payload, Location location,
			double radiusOfInterest, Set<String> channels) {
		super(topic, attributes, payload, location, radiusOfInterest, true);
		this.channels = channels;
	}

	public Set<String> getChannels() {
		return channels;
	}

}
