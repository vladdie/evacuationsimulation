package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.client.gateway;

import java.util.Collection;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEnabled;

/**
 * 
 * Bypass clients can act as gateways if a cloud/cloudlet tells them to forward
 * notifications to other clients.
 * 
 * @author Bjoern Richerzhagen
 *
 */
public interface LocalGatewayBehavior extends TransitionEnabled {

	public enum GatewayBehavior {
		NONE(NoGatewayBehavior.class), STATELESS(StatelessGatewayBehavior.class);

		public final Class<? extends LocalGatewayBehavior> clazz;

		private GatewayBehavior(Class<? extends LocalGatewayBehavior> clazz) {
			this.clazz = clazz;
		}
	}

	/**
	 * When a notification from the cloud is received by a client with a local
	 * distribution strategy, that notification can optionally be distributed
	 * locally (gateway functionality).
	 * 
	 * @param n
	 * @param subscribers
	 */
	public void notify(Notification n, Collection<OverlayContact> subscribers);
	
	/**
	 * Currently active gateway behavior
	 * 
	 * @return
	 */
	public GatewayBehavior getGatewayBehavior();

}
