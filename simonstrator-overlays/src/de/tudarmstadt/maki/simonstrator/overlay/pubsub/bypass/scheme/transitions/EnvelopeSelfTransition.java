package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions;

import de.tudarmstadt.maki.simonstrator.api.component.transition.SelfTransition;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ste.SpaceTimeEnvelopeSchemeBroker;

/**
 * SelfTransition of the Space-Time Envelope scheme to alter the size of the
 * envelope by setting the factor alpha to a certain value
 * 
 * @author Julian Zobel
 *
 */
public class EnvelopeSelfTransition implements SelfTransition<SpaceTimeEnvelopeSchemeBroker>
{
	double alpha;

	public EnvelopeSelfTransition(double alpha) {
		this.alpha = alpha;
	}

	/**
	 * Invoked as soon as the self-transition is executed. U
	 * 
	 * @param mechanism
	 */
	public void alterState(SpaceTimeEnvelopeSchemeBroker scheme) {
		System.out.println("SelfTransition to alpha = " + alpha);
		scheme.setAlpha(alpha);
	}

}
