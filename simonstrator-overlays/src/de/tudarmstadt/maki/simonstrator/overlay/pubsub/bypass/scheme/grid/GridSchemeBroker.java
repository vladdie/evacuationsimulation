package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid;

import de.tudarmstadt.maki.simonstrator.api.component.transition.MechanismState;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransferState;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassCloudComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BypassSchemeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.AbstractLocationBasedSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.location.LocationUpdateMsg;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.util.BoundingRectangle;

/**
 * A location-based subscription scheme where clients are grouped in grid cells
 * based on their location. Cells are defined by squares using two points
 * (minimum bounding rectangle). Clients update their location to the server
 * when they move out of the bounds. Notifications are send to all clients
 * inside a cell.
 * 
 * @author Julian Zobel
 */
public class GridSchemeBroker extends AbstractLocationBasedSubscriptionScheme.Broker {

	/**
	 * Storage for all {@link SubscriptionGridCell}. Each
	 * {@link SubscriptionGridCell} manages a single cell of the grid (defined
	 * by it's minimum bounding rectangle) and all subscribers within that
	 * cells.
	 */
	private GridBasedSubscriptionStorage storage;

	@MechanismState("GridSize")
	private int gridSize;

	@TransferState({ "Component" })
	public GridSchemeBroker(BypassCloudComponent component) {
		super(component, SchemeName.GRID);
		storage = new GridBasedSubscriptionStorage(component,
				new BoundingRectangle(BypassSubscriptionSchemeConfiguration.gridWorldY,
						BypassSubscriptionSchemeConfiguration.gridWorldX, 0, 0));
	}

	@Override
	public GridBasedSubscriptionStorage getStorage() {
		return storage;
	}

	@Override
	public boolean onMessageArrived(BypassSchemeMessage msg, TransInfo sender, int commID) {

		if (msg instanceof LocationUpdateMsg) {
			/*
			 * Invoked on a broker, if a client sent a location update message.
			 */
			LocationUpdateMsg uMsg = (LocationUpdateMsg) msg;
			storage.updateLocationOfClient(uMsg.getSender(), uMsg.getLocation());
		} else {
			return false;
		}
		return true;
	}

	/**
	 * Supported on the broker: updates the cell-sizes.
	 * 
	 * @param x
	 * @param y
	 */
	public void updateGrid(int x, int y) {
		storage.setAndAdjustGridCells(x, y);
	}

	/**
	 * Automated transitions using the mechanism state to update the grid size.
	 * 
	 * @param gridSize
	 */
	public void setGridSize(int gridSize) {
		this.gridSize = gridSize;
		updateGrid(gridSize, gridSize);
	}

}
