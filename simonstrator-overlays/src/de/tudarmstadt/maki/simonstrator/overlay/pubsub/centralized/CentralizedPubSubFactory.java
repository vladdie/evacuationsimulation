package de.tudarmstadt.maki.simonstrator.overlay.pubsub.centralized;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent;

/**
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class CentralizedPubSubFactory implements HostComponentFactory {

	private boolean isServer = false;

	public static int PORT = 14665;

	public static CentralizedPubSubServer server = null;

	public static boolean USE_TCP = false;

	public static NetworkComponent.NetInterfaceName netInterface = NetworkComponent.NetInterfaceName.WIFI;

	@Override
	public HostComponent createComponent(Host host) {
		if (isServer) {
			server = new CentralizedPubSubServer(host, PORT);
			return server;
		} else {
			return new CentralizedPubSubClient(host, PORT);
		}
	}

	public void setIsServer(boolean isServer) {
		this.isServer = isServer;
	}

	public void setPort(int port){
		CentralizedPubSubFactory.PORT = port;
	}

	public void setNetInterface(String netInterface) {
		CentralizedPubSubFactory.netInterface = NetworkComponent.NetInterfaceName.valueOf(netInterface);
	}

	public void setUseTcp(boolean useTcp) {
		CentralizedPubSubFactory.USE_TCP = useTcp;
	}
}
