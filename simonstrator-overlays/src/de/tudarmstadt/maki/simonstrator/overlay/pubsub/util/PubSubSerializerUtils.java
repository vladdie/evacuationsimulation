package de.tudarmstadt.maki.simonstrator.overlay.pubsub.util;

import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.FilterOperator;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.BypassPubSubComponent;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute.BasicAttribute;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.util.attribute.filter.BasicAttributeFilter;

/**
 * Contains all types used in the pub/sub utils. This should be used and
 * extended in custom serializers, probably using the convenience method
 * concatenateWithUtilClasses.
 * 
 * @author Bjoern Richerzhagen
 * @version Nov 12, 2014
 */
public class PubSubSerializerUtils {
	
	public static final Class<?>[] UTIL_CLASSES = new Class<?>[]{
			BasicTopic.class, BasicSubscription.class, BasicNotification.class,
			BasicFilter.class, BasicAttribute.class,
			BypassPubSubComponent.LocalDistributionType.class,
			BasicAttributeFilter.class, BasicOverlayContact.class,
			INodeID.class, FilterOperator.class
	};
	
	public static Class<?>[] concatenateWithUtilClasses(Class<?>[] ownClasses) {
		if (ownClasses == null || ownClasses.length == 0) {
			return UTIL_CLASSES;
		}
		Class<?>[] concatencated = new Class<?>[ownClasses.length
				+ UTIL_CLASSES.length];
		System.arraycopy(UTIL_CLASSES, 0, concatencated, 0, UTIL_CLASSES.length);
		System.arraycopy(ownClasses, 0, concatencated, UTIL_CLASSES.length,
				ownClasses.length);
		return concatencated;
	}

}
