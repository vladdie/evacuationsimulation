package de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.transition.TransitionEngine;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.api.BypassMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.CloudPublicationHandler;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.gateway.BypassGatewayLogic;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.BypassCloudMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudNotifyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudPublishMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudSubscribeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.cloud.msg.CloudUnsubscribeMessage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.configuration.BypassSubscriptionSchemeConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.BrokerSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.ClientSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.SubscriptionScheme.SchemeName;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.basic.BasicSubscriptionScheme;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.extGrid.ExtGridBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.grid.GridBasedSubscriptionStorage;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions.DefaultLocationBasedTransitionBroker;
import de.tudarmstadt.maki.simonstrator.overlay.pubsub.bypass.scheme.transitions.DefaultLocationBasedTransitionClient;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionCoordinator;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.TransitionExecutionPlan;
import de.tudarmstadt.maki.simonstrator.service.transition.coordination.action.AtomicTransitionAction;
import de.tudarmstadt.maki.simonstrator.util.OverlayContacts;

/**
 * Component running on the cloud/cloudlet/antenna
 * 
 * @author Bjoern Richerzhagen
 * @version May 2, 2014
 */
public class BypassCloudComponent extends BypassPubSubComponent {

	private BypassGatewayLogic gatewayLogic;

	/**
	 * Transition-enabled {@link BrokerSubscriptionScheme} used by Bypass
	 */
	private BrokerSubscriptionScheme brokerSubscriptionScheme = new BasicSubscriptionScheme(this);

	/**
	 * A broker might also act as a client
	 */
	private ClientSubscriptionScheme clientSubscriptionScheme = new BasicSubscriptionScheme(this);

	/**
	 * Can be extended by the application
	 */
	private CloudPublicationHandler publicationHandler = new CloudPublicationHandler.DefaultHandler();

	/**
	 * FIXME dirty hack for evaluation. Remove lateron.
	 */
	private DirtyHackedTransitionControllerTime _schemeTransitionController;

	public BypassCloudComponent(Host host) {
		super(host, NodeType.CLOUD);
		// Nothing to do here.
	}

	@Override
	public void initialize() {
		super.initialize();
		TransitionEngine tEngine = getTransitionEngine();

		brokerSubscriptionScheme = tEngine.createMechanismProxy(BrokerSubscriptionScheme.class,
				brokerSubscriptionScheme, BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER);

		clientSubscriptionScheme = tEngine.createMechanismProxy(ClientSubscriptionScheme.class,
				clientSubscriptionScheme, PROXY_SUBSCRIPTION_SCHEME);

		setPeerStatus(PeerStatus.PRESENT);

		// FIXME dirty hack for transition control.
		if (BypassSubscriptionSchemeConfiguration.testAutomatedTransitions) {
			Event.scheduleImmediately(new EventHandler() {
				@Override
				public void eventOccurred(Object content, int type) {
					_schemeTransitionController = new DirtyHackedTransitionControllerTime();
				}
			}, null, 0);
		}
	}

	/**
	 * Configures the gateway logic for this cloud
	 * 
	 * @param gwLogic
	 */
	public void setGatewayLogic(BypassGatewayLogic gwLogic) {
		assert gatewayLogic == null;
		this.gatewayLogic = gwLogic;
	}

	/**
	 * Cloud gateway logic
	 * 
	 * @return
	 */
	public BypassGatewayLogic getGatewayLogic() {
		return gatewayLogic;
	}

	/**
	 * Overwrite the default publication Handler.
	 * 
	 * @param publicationHandler
	 */
	public void setPublicationHandler(CloudPublicationHandler publicationHandler) {
		this.publicationHandler = publicationHandler;
	}

	@Override
	public void handleIncomingMessage(BypassMessage msg, TransInfo sender, int commID) {

		// Only cloud messages
		assert msg instanceof BypassCloudMessage;

		if (msg instanceof CloudPublishMessage) {
			handleCloudPublishMessage((CloudPublishMessage) msg, commID);
		} else if (msg instanceof CloudSubscribeMessage) {
			handleCloudSubscribeMessage((CloudSubscribeMessage) msg, commID);
		} else if (msg instanceof CloudUnsubscribeMessage) {
			handleCloudUnsubscribeMessage((CloudUnsubscribeMessage) msg, commID);
		}

	}

	/**
	 * Act on a publication received by a client
	 * 
	 * @param msg
	 */
	protected void handleCloudPublishMessage(CloudPublishMessage msg, int commID) {
		// notify local (for the server-app)
		clientSubscriptionScheme.notifySelf(msg.getNotification());
		// let the publication-handler handle the rest.
		publicationHandler.handlePublication(this, msg);
	}

	/**
	 * Act on a subscription received by a client
	 * 
	 * @param msg
	 */
	protected void handleCloudSubscribeMessage(CloudSubscribeMessage msg,
			int commID) {
		// store locally
		for (Subscription sub : msg.getSubscriptions()) {
			getSubscriptionScheme().onSubscribeAtBroker(sub, msg.getSubscriber());
		}
		// FIXME dirty hack for transition control.
		if (BypassSubscriptionSchemeConfiguration.testAutomatedTransitions) {
			_schemeTransitionController.evaluate();
		}

		// TODO allow DELTA-notifications?
	}

	/**
	 * Act on a received Unsubscribe Message
	 * 
	 * @param msg
	 * @param commID
	 */
	protected void handleCloudUnsubscribeMessage(CloudUnsubscribeMessage msg,
			int commID) {
		for (Subscription sub : msg.getSubscriptions()) {
			getSubscriptionScheme().onUnsubscribeAtBroker(sub, msg.getSubscriber());
		}
		// FIXME dirty hack for transition control.
		if (BypassSubscriptionSchemeConfiguration.testAutomatedTransitions) {
			_schemeTransitionController.evaluate();
		}
	}


	@Override
	public void publish(Notification notification) {
		clientSubscriptionScheme.notifySelf(notification);
		OverlayContacts subscribers = getSubscriptionScheme().getSubscribers(notification);
		/*
		 * TODO we do NOT trigger the cloud control strategy here. It is
		 * deprecated and will be removed in future versions.
		 */
		if (!subscribers.isEmpty()) {
			Collection<CloudNotifyMessage> msgs = getGatewayLogic().packMessages(notification, subscribers, -1);
			for (CloudNotifyMessage notifyMessage : msgs) {
				sendViaMobileNetwork(notifyMessage);
			}
		}
	}

	@Override
	public void subscribe(Subscription sub, PubSubListener listener) {
		clientSubscriptionScheme.onSubscribeAtClient(sub, listener);
	}

	@Override
	public void unsubscribe(Subscription sub, PubSubListener listener) {
		clientSubscriptionScheme.onUnsubscribeAtClient(sub, listener);
	}

	public BrokerSubscriptionScheme getSubscriptionScheme() {
		return brokerSubscriptionScheme;
	}

	/**
	 * Overwrite the default subscription scheme implementation
	 * 
	 * @param subscriptionScheme
	 */
	public void setSubscriptionScheme(BrokerSubscriptionScheme subscriptionScheme) {
		this.brokerSubscriptionScheme = subscriptionScheme;
	}

	/**
	 * Allows the broker to also act as a client.
	 * 
	 * @param subscriptionScheme
	 */
	public void setClientSubscriptionScheme(ClientSubscriptionScheme subscriptionScheme) {
		this.clientSubscriptionScheme = subscriptionScheme;
	}

	/**
	 * Dirty controller to test the subscription schemes and switch them based
	 * on the number of subscribers - with some hysteresis.
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private class DirtyHackedTransitionController {

		int limitLocationBased = 100;

		int limitGrid = 180;

		int hyst = 10;

		long lastTransitionTimestamp = 0;

		protected Random rnd = Randoms.getRandom(DirtyHackedTransitionController.class);

		public DirtyHackedTransitionController() {
			/*
			 * DEFAULTS: Grid is 5*5, ExtGrid is 5*5 w. RECT
			 */
			BypassSubscriptionSchemeConfiguration.rectangularAoi = true;
			ExtGridBasedSubscriptionStorage.defaultXNum = 5;
			ExtGridBasedSubscriptionStorage.defaultYNum = 5;
			GridBasedSubscriptionStorage.defaultXNum = 5;
			GridBasedSubscriptionStorage.defaultYNum = 5;
			assert hyst > 0;
			/*
			 * custom transitions EXT_GRID <-> LOCATION
			 */
			if (!BypassSubscriptionSchemeConfiguration.disableStateTransfer) {
				for (Host host : Oracle.getAllHosts()) {
					try {
						TransitionEngine te = host.getComponent(TransitionEngine.class);
						te.registerTransition(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME,
								new DefaultLocationBasedTransitionClient());
					} catch (ComponentNotAvailableException e) {
						//
					}
				}

				try {
					TransitionEngine te = getHost().getComponent(TransitionEngine.class);
					te.registerTransition(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
							new DefaultLocationBasedTransitionBroker());
				} catch (ComponentNotAvailableException e) {
					//
				}
			}
		}

		private <T extends SubscriptionScheme> void execTransition(SchemeName to) {
			List<OverlayContact> contacts = new LinkedList<>();
			for (Host host : Oracle.getAllHosts()) {
				try {
					BypassClientComponent client = host.getComponent(BypassClientComponent.class);
					contacts.add(client.getLocalOverlayContact());
				} catch (ComponentNotAvailableException e) {
					//
				}
			}

			try {
				// Also execute the transition on the cloud!
				getHost().getComponent(TransitionEngine.class).executeAtomicTransition(PROXY_SUBSCRIPTION_SCHEME_BROKER,
						to.brokerClass);
				// Distribute to clients
				TransitionCoordinator coordinator = getHost().getComponent(TransitionCoordinator.class);
				TransitionExecutionPlan plan = coordinator.createExecutionPlan();
				plan.addAction(new AtomicTransitionAction(PROXY_SUBSCRIPTION_SCHEME, to.clientClass));
				coordinator.executeTransitionPlan(contacts, plan);
			} catch (ComponentNotAvailableException e1) {
				throw new AssertionError("Requires a TransitionCoordinator.");
			}
		}

		public void evaluate() {
			int subscribers = getSubscriptionScheme().getSubscribers().size();
			SchemeName currScheme = getSubscriptionScheme().getName();
			
			if (lastTransitionTimestamp + 5 * Time.SECOND > Time.getCurrentTime()) {
				return;
			}

			if (currScheme == SchemeName.EXT_GRID_RECT) {
				// FROM ExtGrid
				if (subscribers <= limitLocationBased - hyst) {
					// Transition to LOCATION_BASED
					execTransition(SchemeName.LBS);
					lastTransitionTimestamp = Time.getCurrentTime();
					System.out.println(Time.getFormattedTime() + " TRANSITION from EXT_GRID to LOCATION_BASED.");
				} else if (subscribers >= limitGrid + hyst) {
					// Transition to GRID
					execTransition(SchemeName.GRID);
					lastTransitionTimestamp = Time.getCurrentTime();
					System.out.println(Time.getFormattedTime() + " TRANSITION from EXT_GRID to GRID.");
				}
			} else if (currScheme == SchemeName.LBS) {
				// From LocationBased
				if (subscribers >= limitLocationBased + hyst) {
					// Transition to EXT_GRID_RECT
					execTransition(SchemeName.EXT_GRID_RECT);
					lastTransitionTimestamp = Time.getCurrentTime();
					System.out.println(Time.getFormattedTime() + " TRANSITION from LOCATION_BASED to EXT_GRID.");
				}
			} else if (currScheme == SchemeName.GRID) {
				// From Grid
				if (subscribers <= limitGrid - hyst) {
					// Transition to EXT_GRID_RECT
					execTransition(SchemeName.EXT_GRID_RECT);
					lastTransitionTimestamp = Time.getCurrentTime();
					System.out.println(Time.getFormattedTime() + " TRANSITION from GRID to EXT_GRID.");
				}
			} else {
				throw new AssertionError();
			}
		}

	}

	/**
	 * Dirty controller to test the subscription schemes and switch them based
	 * on fixed time points (to be synchronous on multiple runs)
	 * 
	 * @author Bjoern Richerzhagen
	 *
	 */
	private class DirtyHackedTransitionControllerTime implements EventHandler {

		protected Random rnd = Randoms.getRandom(DirtyHackedTransitionController.class);

		public DirtyHackedTransitionControllerTime() {
			/*
			 * DEFAULTS: Grid is 5*5, ExtGrid is 5*5 w. RECT
			 */
			BypassSubscriptionSchemeConfiguration.rectangularAoi = true;
			ExtGridBasedSubscriptionStorage.defaultXNum = 5;
			ExtGridBasedSubscriptionStorage.defaultYNum = 5;
			GridBasedSubscriptionStorage.defaultXNum = 5;
			GridBasedSubscriptionStorage.defaultYNum = 5;
			/*
			 * custom transitions EXT_GRID <-> LOCATION
			 */
			if (!BypassSubscriptionSchemeConfiguration.disableStateTransfer) {
				for (Host host : Oracle.getAllHosts()) {
					try {
						TransitionEngine te = host.getComponent(TransitionEngine.class);
						te.registerTransition(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME,
								new DefaultLocationBasedTransitionClient());
					} catch (ComponentNotAvailableException e) {
						//
					}
				}

				try {
					TransitionEngine te = getHost().getComponent(TransitionEngine.class);
					te.registerTransition(BypassPubSubComponent.PROXY_SUBSCRIPTION_SCHEME_BROKER,
							new DefaultLocationBasedTransitionBroker());
				} catch (ComponentNotAvailableException e) {
					//
				}
			}

			/*
			 * Schedule transition events.
			 */
			Event.scheduleWithDelay(11 * Time.MINUTE + 12 * Time.SECOND, this, null, 1);
			Event.scheduleWithDelay(16 * Time.MINUTE + 30 * Time.SECOND, this, null, 2);
			Event.scheduleWithDelay(21 * Time.MINUTE + 10 * Time.SECOND, this, null, 3);
			Event.scheduleWithDelay(26 * Time.MINUTE + 10 * Time.SECOND, this, null, 4);
		}

		@Override
		public void eventOccurred(Object content, int type) {
			switch (type) {
			case 1:
				execTransition(SchemeName.EXT_GRID);
				System.out.println(Time.getFormattedTime() + " TRANSITION from LOCATION_BASED to EXT_GRID.");
				break;

			case 2:
				execTransition(SchemeName.GRID);
				System.out.println(Time.getFormattedTime() + " TRANSITION from EXT_GRID to GRID.");
				break;

			case 3:
				execTransition(SchemeName.EXT_GRID);
				System.out.println(Time.getFormattedTime() + " TRANSITION from GRID to EXT_GRID.");
				break;

			case 4:
				execTransition(SchemeName.LBS);
				System.out.println(Time.getFormattedTime() + " TRANSITION from EXT_GRID to LOCATION_BASED.");
				break;

			default:
				break;
			}
		}

		private <T extends SubscriptionScheme> void execTransition(SchemeName to) {
			List<OverlayContact> contacts = new LinkedList<>();
			for (Host host : Oracle.getAllHosts()) {
				try {
					BypassClientComponent client = host.getComponent(BypassClientComponent.class);
					contacts.add(client.getLocalOverlayContact());
				} catch (ComponentNotAvailableException e) {
					//
				}
			}

			try {
				// Also execute the transition on the cloud!
				getHost().getComponent(TransitionEngine.class).executeAtomicTransition(PROXY_SUBSCRIPTION_SCHEME_BROKER,
						to.brokerClass);
				// Distribute to clients
				TransitionCoordinator coordinator = getHost().getComponent(TransitionCoordinator.class);
				TransitionExecutionPlan plan = coordinator.createExecutionPlan();
				plan.addAction(new AtomicTransitionAction(PROXY_SUBSCRIPTION_SCHEME, to.clientClass));
				coordinator.executeTransitionPlan(contacts, plan);
			} catch (ComponentNotAvailableException e1) {
				throw new AssertionError("Requires a TransitionCoordinator.");
			}
		}

		public void evaluate() {
			// don't care.
		}

	}

}
