/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.optimization;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;

/**
 * Possible Low Delay Jump Optimization as defined in Lidanski MA.
 * @author Eduardo Lidanski
 * @version 1.0, 14.04.2014
 */
public class PossibleLowDelayJumpOptimization implements
		PossibleOptimizationAction {

	private TransitConnection jumpTo;

	private int layer;

	private double gain;
	
	public PossibleLowDelayJumpOptimization(TransitConnection jumpTo,
			int layer, int actualDepth, int newDepth) {
		this.jumpTo = jumpTo;
		this.layer = layer;
		gain = Math.abs(actualDepth - newDepth);
	}

	@Override
	public double getGain() {
		return gain;
	}

	@Override
	public TransitConnection getTargetConnection() {
		return jumpTo;
	}

	@Override
	public int getLayer() {
		return layer;
	}

	@Override
	public String toString() {
		return jumpTo.getOwner().getNodeID() + " jumps to "
				+ jumpTo.getEndpoint().getNodeID() + "(Layer: " + layer
				+ " Gain: " + gain + ")";
	}
}
