/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.TransitServingAdvise.ServingAdvise;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact.ContactType;

public class TransitIncentiveTitForTat extends AbstractTransitIncentiveManager implements
        TransitIncentiveRequestAdvisor {

	private final long UNCHOKING_PERIOD;

	private final double UNCHOKING_GENEROSITY;

	protected final long SLIDING_WINDOW_SIZE;

	private final static boolean SHUFFLE_UNKNOWN_CONNECTIONS = true;

	private final boolean ONLY_SERVER_BASE_LAYER_TO_UNCOOPERATING_NODES;

	protected HashMap<TransitContact, Long> incentiveDatabase;

	/**
	 * Instantiates a new transit incentive tit for tat.
	 * 
	 * @param transitNode
	 *            the transit node
	 */
	public TransitIncentiveTitForTat(TransitNode transitNode) {
		super(transitNode);
		UNCHOKING_PERIOD = transitNode.getSettings().getTime(TransitTimes.INCENTIVE_UNCHOKING_PERIOD);
		SLIDING_WINDOW_SIZE = transitNode.getSettings().getTime(TransitTimes.INCENTIVE_SLIDING_WINDOW_SIZE);
		UNCHOKING_GENEROSITY = ((transitNode.getSettings().getParam(TransitParams.INCENTIVE_UNCHOKING_GENEROSITY) * 1.0) / 100.0);
		ONLY_SERVER_BASE_LAYER_TO_UNCOOPERATING_NODES = transitNode.getSettings().getParam(
		        TransitParams.INCENTIVE_ONLY_SERVER_BASE_LAYER_TO_UNCOOPERATING_NODES) > 0;
	}

	/**
	 * Checks if is in unchoking mode.
	 * 
	 * @param connection
	 *            the connection
	 * @return true, if is in unchoking mode
	 */
	private boolean isInUnchokingMode(TransitConnection connection) {

		if (connection == null) {
			return false;
		}

		if (connection.getCreationTime() > Time.getCurrentTime() - UNCHOKING_PERIOD) {
			return true;
		}

		return false;
	}

	/**
	 * Checks if is in sliding window.
	 * 
	 * @param transitConnection
	 *            the transit connection
	 * @return true, if is in sliding window
	 */
	private boolean isInSlidingWindow(TransitConnection transitConnection) {

		if (transitConnection == null) {
			return false;
		}

		if (Time.getCurrentTime() - transitConnection.getLastBlockTransferredTimestamp() < SLIDING_WINDOW_SIZE) {
			return true;
		}

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.
	 * TransitIncentiveManager#sortConnectionsByIncentive(java.util.List)
	 */
	@Override
	public List<TransitConnection> sortConnectionsByIncentiveForRequesting(List<TransitConnection> inConnections) {

		if (inConnections.size() == 0) {
			return inConnections;
		}

		/*
		 * (0) Get all connections we served data to.
		 */
		final HashMap<TransitContact, Long> nodeTransferBalance = new LinkedHashMap<TransitContact, Long>();
		final List<TransitConnection> outConnections = getNode().getNeighborhood().getConnectionManager()
		        .getConnections(ConnectionDirection.OUT, ConnectionState.OPEN, null, null);
		for (TransitConnection con : outConnections) {
			nodeTransferBalance.put(con.getEndpoint(), con.getConnectionStatistics().getRecentlyBytesTransferred());
		}

		/*
		 * (1) Divide by previously seen connection or unknown.
		 */
		final List<TransitConnection> knownConnections_01_owingUs = new LinkedList<TransitConnection>();
		final List<TransitConnection> knownConnections_02_outstandingDebt = new LinkedList<TransitConnection>();
		final List<TransitConnection> knownConnections_03_twoway = new LinkedList<TransitConnection>();
		final List<TransitConnection> unknownConnections_04 = new LinkedList<TransitConnection>();

		for (TransitConnection transitConnection : inConnections) {

			if (nodeTransferBalance.containsKey(transitConnection.getEndpoint())) {

				/* 1. We are known. Did we consume already? */
				final long currentNodeBalanceServed = nodeTransferBalance.remove(transitConnection.getEndpoint());
				final long currentNodeBalanceReceived = transitConnection.getConnectionStatistics()
				        .getRecentlyBytesTransferred();
				final long currentNodeBalanceTotal = currentNodeBalanceServed - currentNodeBalanceReceived;
				nodeTransferBalance.put(transitConnection.getEndpoint(), currentNodeBalanceTotal);

				/* 2. Add connection to respective set. */
				if (currentNodeBalanceTotal > 0) {
					knownConnections_01_owingUs.add(transitConnection);
				} else if (currentNodeBalanceReceived > 0) {
					if (isInSlidingWindow(transitConnection)) {
						knownConnections_02_outstandingDebt.add(transitConnection);
					} else {
						knownConnections_03_twoway.add(transitConnection);
					}
				} else {
					/* Possible free rider? */
					knownConnections_03_twoway.add(transitConnection);
				}

			} else {
				/* Oneway connection: Possible free rider? */
				unknownConnections_04.add(transitConnection);
			}

		}

		/*
		 * (2) Sort each set, known connections by participation, unknown by
		 * load.
		 */
		Collections.sort(knownConnections_01_owingUs, new Comparator<TransitConnection>() {
			@Override
			public int compare(TransitConnection o1, TransitConnection o2) {
				return nodeTransferBalance.get(o2.getEndpoint()).compareTo(nodeTransferBalance.get(o1.getEndpoint()));
			}
		});
		Collections.sort(knownConnections_02_outstandingDebt, COMP_DISTANCE_TO_SLIDING_WINDOW_ASC);
		Collections.shuffle(knownConnections_03_twoway);

		if (SHUFFLE_UNKNOWN_CONNECTIONS) {
			Collections.shuffle(unknownConnections_04);
		} else {
			Collections.sort(unknownConnections_04, COMP_LOAD_ASC);
		}

		/*
		 * (3) Add optimistic unchoking to sorting.
		 */
		Iterator<TransitConnection> it = knownConnections_03_twoway.iterator();
		LinkedList<TransitConnection> unchokedConnections = new LinkedList<TransitConnection>();
		while (it.hasNext()) {
			TransitConnection transitConnection = it.next();
			if (isInUnchokingMode(transitConnection)) {
				it.remove();
				unchokedConnections.add(transitConnection);
			}
		}
		knownConnections_03_twoway.addAll(0, unchokedConnections);

		/*
		 * (4) Merge lists.
		 */
		knownConnections_01_owingUs.addAll(knownConnections_02_outstandingDebt);
		knownConnections_01_owingUs.addAll(knownConnections_03_twoway);
		knownConnections_01_owingUs.addAll(unknownConnections_04);

		/*
		 * (5) And move sources to top of list: will get skipped under heavy
		 * load anyway.
		 */
		LinkedList<TransitConnection> sources = new LinkedList<TransitConnection>();
		for (TransitConnection transitConnection : knownConnections_01_owingUs) {
			if (transitConnection.getEndpoint().getType().equals(ContactType.SOURCE)) {
				sources.add(transitConnection);
			}
		}
		knownConnections_01_owingUs.removeAll(sources);
		knownConnections_01_owingUs.addAll(0, sources);

		/* Done. */
		return knownConnections_01_owingUs;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.
	 * TransitIncentiveManager
	 * #getServingAdvise(de.tudarmstadt.maki.simonstrator.
	 * overlay.streaming.transit.neighborhood.TransitConnection)
	 */
	@Override
	public TransitServingAdvise getServingAdvise(TransitConnection outConnection) {

		TransitConnection inConnection = getOppositeDirectionConnection(outConnection);

		/*
		 * Blocks have been received from the other side. This is an active
		 * participating host.
		 */
		if (isActiveConnection(inConnection)) {
			return new TransitServingAdvise(ServingAdvise.A_SERVE_FULL);
		}

		/*
		 * NO blocks have been transferred to us yet.
		 */
		if (inConnection != null) {
			/*
			 * No blocks have been transferred. What up here? Unchoking?
			 * Malicious?
			 */
			if (isInUnchokingMode(inConnection)) {
				if (ONLY_SERVER_BASE_LAYER_TO_UNCOOPERATING_NODES) {
					/*
					 * We have a two way connection, and our IN connection is
					 * still in unchoking mode and was not served yet by us.
					 */
					return new TransitServingAdvise(ServingAdvise.A_SERVE_BASELAYER, 1.0);
				} else {
					return new TransitServingAdvise(ServingAdvise.A_SERVE_WITH_PROBABILITY, UNCHOKING_GENEROSITY);
				}
			}
		}

		/*
		 * This is not even a two way connection. No interest. Thank you.
		 */
		return new TransitServingAdvise(ServingAdvise.A_DO_NOT_SERVE_AND_CLOSE);

	}

	/**
	 * Gets the serving probability.
	 * 
	 * @param outConnection
	 *            the connection
	 * @return the serving probability
	 */
	@SuppressWarnings("unused")
	private double getServingProbability(TransitConnection outConnection) {

		// Get all connections we are receiving data from.
		List<TransitConnection> inConnections = getNode().getNeighborhood().getConnectionManager()
		        .getConnections(ConnectionDirection.IN, ConnectionState.OPEN, null, null);

		if (inConnections.size() == 0)
			return 0;

		assert inConnections.get(0).getEndpoint() != getNode().getLocalOverlayContact();

		long bwSum = 0;
		for (TransitConnection transitConnection : inConnections) {
			bwSum += transitConnection.getConnectionStatistics().getRecentlyBytesTransferred();
		}

		if (bwSum == 0) {
			return 0;
		}

		TransitConnection correspondingInConnection = getOppositeDirectionConnection(outConnection);
		return ((double) correspondingInConnection.getConnectionStatistics().getRecentlyBytesTransferred())
		        / ((double) bwSum);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.
	 * TransitIncentiveManager#getIncentiveRequestAdvisor(java.util.List)
	 */
	@Override
	public TransitIncentiveRequestAdvisor getIncentiveRequestAdvisor(List<TransitConnection> connections) {
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.
	 * TransitIncentiveRequestAdvisor
	 * #getRecommendedRequestProbability(de.tudarmstadt
	 * .maki.simonstrator.overlay
	 * .streaming.transit.neighborhood.TransitConnection)
	 */
	@Override
	public double getRecommendedRequestProbability(TransitConnection connection) {
		return 1.0;
	}

	/** The Constant COMP_TRANSFER. */
	private final Comparator<TransitConnection> COMP_DISTANCE_TO_SLIDING_WINDOW_ASC = new Comparator<TransitConnection>() {

		/**
		 * Compare two Connections by transferred Bytes.
		 * 
		 * @param o1
		 *            the o1
		 * @param o2
		 *            the o2
		 * @return the int
		 */
		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			Long result1 = SLIDING_WINDOW_SIZE - (Time.getCurrentTime() - o1.getLastBlockTransferredTimestamp());
			Long result2 = SLIDING_WINDOW_SIZE - (Time.getCurrentTime() - o2.getLastBlockTransferredTimestamp());
			return result1.compareTo(result2);
		}
	};

}
