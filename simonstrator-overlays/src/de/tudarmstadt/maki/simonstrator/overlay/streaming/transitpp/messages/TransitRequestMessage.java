/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitRequestManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitRequestSchedule;

/**
 * This message is used by the {@link TransitRequestManager} to request blocks.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public interface TransitRequestMessage extends TransitMessage {

	/**
	 * Intention of this message
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 04.07.2012
	 */
	public static enum RequestMsgType {
		/**
		 * This is a pure request for the given schedule. It may be confirmed
		 * with an OK, or the receiver might just start sending packets.
		 * However, we will directly react to negative answers or to a timeout
		 * in the packet delivery process.
		 */
		REQUEST,
		/**
		 * The receiver has no spare capacity for the request
		 */
		DENY_CAPACITY,
		/**
		 * This message contains a buffer-map
		 */
		OFFER
	}

	/**
	 * This is the {@link TransitRequestSchedule} which is specifying which
	 * blocks we request. It may contain additional meta-data to help us answer
	 * the request.
	 * 
	 * @return
	 */
	public TransitRequestSchedule getRequestSchedule();

	/**
	 * Returns the meaning of this message from a protocol perspective.
	 * 
	 * @return
	 */
	public RequestMsgType getType();

}
