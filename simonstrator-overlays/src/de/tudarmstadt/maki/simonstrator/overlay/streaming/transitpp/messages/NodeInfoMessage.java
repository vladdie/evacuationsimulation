/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNodeInfo;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;

/**
 * Contains the node info sent periodically between nodes with important information, e.g. depth, number of children, etc.
 * @author Eduardo Lidanski
 * @version 1.0, 14.04.2014
 */
public class NodeInfoMessage extends AbstractTransitMessage {

	private final TransitNodeInfo nodeInfo;

	public NodeInfoMessage(TransitContact sender, int[] numberOfChildren, int[] depth, BitSet capacities, TransitContact[] parents) {
		super(sender);
		nodeInfo = new TransitNodeInfo(numberOfChildren, depth, capacities, parents);
	}


	@Override
	public long getSize() {
		return super.getSize() + nodeInfo.getTransmissionSize();
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Info: ");
		sb.append(nodeInfo.toString());
		return sb.toString();
	}


	public TransitNodeInfo getNodeInfo() {
		return nodeInfo;
	}

}
