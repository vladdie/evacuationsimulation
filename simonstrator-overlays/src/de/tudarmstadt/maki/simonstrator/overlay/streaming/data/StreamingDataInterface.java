package de.tudarmstadt.maki.simonstrator.overlay.streaming.data;

import java.io.InputStream;

/**
 * Buffer Interface
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public interface StreamingDataInterface {

	/**
	 * Communication interface for the player/source
	 * 
	 * @return
	 */
	public NorthboundInterface getNorthboundInterface();

	/**
	 * Communication interface for transit
	 * 
	 * @return
	 */
	public SouthboundInterface getSouthboundInterface();

	/**
	 * Interface to the video model (may be implemented by the player)
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	public interface ModelInterface {

		public boolean isBlockRequired(int blocknum);

		/**
		 * Get number of the first block that is required at the output of the
		 * sorted buffer.
		 * 
		 * @return
		 */
		public int getFirstRequiredBlock();

	}

	/**
	 * Player -- Interface working solely on streams
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	public interface NorthboundInterface {

		/**
		 * Make use of available()! ;)
		 * 
		 * @return
		 */
		public InputStream getInputStream();

		/**
		 * For the source - Ensure that this method is called with the same pace
		 * as the source would play the video!
		 * 
		 * For Multi-layer, the source HAS TO USE ALL LAYERS!
		 * 
		 * @return
		 */
		public void newBlock(byte[] data);

	}

	/**
	 * Transit -- Interface working on blocks
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	public interface SouthboundInterface {

		public void receivedBlock(int blocknum, byte[] data);

		public byte[] getDataForBlock(int blocknum);

		public boolean hasDataForBlock(int blocknum);

	}

}
