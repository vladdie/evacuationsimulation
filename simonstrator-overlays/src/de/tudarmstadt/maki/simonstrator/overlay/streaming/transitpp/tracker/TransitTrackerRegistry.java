/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.tracker;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContactManager;

/**
 * Inside this registry, a tracker saves all contacts it has gathered so far.
 * This is a special case of the {@link TransitContactManager}. In contrast to
 * the table on a participating node, it does differentiate between source nodes
 * and leechers to provided new nodes with a <i>useful</i> starting set.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 08.06.2012
 */
public class TransitTrackerRegistry extends TransitContactManager {

	private LinkedHashMap<UniqueID, TransitContact> sourceContacts = new LinkedHashMap<UniqueID, TransitContact>();

	private int numberOfInitialContacts = 30;

	public TransitTrackerRegistry() {

	}

	@Override
	public void addOrUpdateContact(TransitContact contact) {
		if (contact.getType() == ContactType.SOURCE) {
			TransitContact old = sourceContacts.get(contact.getNodeID());
			if (old == null) {
				old = contact;
				sourceContacts.put(contact.getNodeID(), contact);
			} else {
				old.update(contact);
			}
		} else {
			super.addOrUpdateContact(contact);
		}
	}

	private void removeOldContacts() {
		List<TransitContact> result = new LinkedList<TransitContact>(
				getSortedContacts(COMP_LASTUPDATE, null, -1));
		for (TransitContact contact : result) {
			if (contact.getLastUpdate() + Time.MINUTE < Time
					.getCurrentTime()) {
				removeContact(contact);
			}
		}
	}

	/**
	 * Returns a list of nodes that is beneficial for the requester based on his
	 * current set of neighbors as well as his current {@link TransitContact}
	 * -state.
	 * 
	 * @param requester
	 * @param numberOfContacts
	 * @param exclude
	 * @return
	 */
	public List<TransitContact> getAdditionalNeighborhood(
			TransitContact requester, int numberOfContacts,
			List<UniqueID> exclude) {
		/*
		 * TODO implement this mechanism in a more sophisticated way!
		 */
		removeOldContacts();
		exclude.add(requester.getNodeID());
		List<TransitContact> result = new LinkedList<TransitContact>(
				sourceContacts.values());
		result.addAll(getSortedContacts(COMP_LASTUPDATE, exclude,
				numberOfContacts));

		if (result.isEmpty()) {
			throw new AssertionError(
					"No source node was specified (or at least not registered with the tracker!)");
		}

		// System.out.println("TRACKER sending additional Neighborhood: "
		// + result.toString());

		return result;
	}

	/**
	 * Returns a list of nodes considered the initial Neighborhood of the
	 * requester. This should consist of high-power nodes that provide high
	 * bandwidth for the initial caching of content.
	 * 
	 * TODO add more sophisticated metrics on how to select this list
	 * 
	 * @param requester
	 * @return
	 */
	public List<TransitContact> getInitialNeighborhood(TransitContact requester) {

		removeOldContacts();
		List<UniqueID> exclude = new LinkedList<UniqueID>();
		exclude.add(requester.getNodeID());
		LinkedList<TransitContact> result = new LinkedList<TransitContact>(
				getSortedContacts(COMP_LASTUPDATE, exclude,
						Math.max(0, numberOfInitialContacts - 1)));

		/*
		 * TODO for now, just take a source and other, random contacts
		 */
		Vector<TransitContact> sourceList = new Vector<TransitContact>(
				sourceContacts.values());
		if (sourceList.isEmpty()) {
			throw new AssertionError(
					"No source node was specified (or at least not registered with the tracker!)");
		}
		int randIndex = Randoms.getRandom(this).nextInt(sourceList.size());
		result.addFirst(sourceList.get(randIndex));

//		System.out.println("TRACKER sending initial Neighborhood: "
//		+ result.toString());

		return result;
	}

}
