/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.plugins.pulltoken.PullToken;

public class PullTokenTransitMessage extends AbstractTransitMessage implements
		TransitPluginMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 593360842481963268L;

	/**
	 * The Enum MSG_TYPE.
	 */
	public enum TOKEN {
		REDEEM, ISSUING
	}

	/** The msg type. */
	private TOKEN msgType = TOKEN.ISSUING;

	/** The token. */
	private PullToken token;

	/**
	 * Instantiates a new pull token message.
	 * 
	 * @param sender
	 *            the sender
	 * @param pullToken
	 *            the pull token
	 */
	public PullTokenTransitMessage(TransitContact sender, PullToken token) {
		super(sender);
		this.token = token;
	}

	/**
	 * Instantiates a new pull token message.
	 * 
	 * @param sender
	 *            the sender
	 * @param pullToken
	 *            the pull token
	 * @param type
	 *            the type
	 */
	public PullTokenTransitMessage(TransitContact sender, PullToken token,TOKEN type) {
		this(sender, token);
		this.msgType = type;
	}

	/**
	 * Gets the token.
	 * 
	 * @return the token
	 */
	public PullToken getToken() {
		return token;
	}

	/**
	 * Gets the msg type.
	 * 
	 * @return the msg type
	 */
	public TOKEN getMsgType() {
		return msgType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PullTokenMessage {Token = " + token.getPullToken() + "}";
	}

}
