/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.network.Bandwidth;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageCallback;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.ConnectionMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.JoinOverlayCommand;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.NodeInfoMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SetConnectionDirectionMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitFlowMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitOfferNeighborhoodMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitRequestMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.util.BandwidthEstimator;

/**
 * This class is used to dispatch messages.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public class TransitMessageHandler implements TransMessageListener,
		IPeerStatusListener {

	private final TransitNode node;

	public final BandwidthEstimator currentBandwidth;

	protected boolean active = false;

	private final List<TransitPiggybackListener> piggybackListeners;

	private final MessageBasedTransport transport;

	private final MessageBasedTransport transportToTracker;

	public TransitMessageHandler(TransitNode node,
			MessageBasedTransport transport,
			MessageBasedTransport transportToTracker) {
		this.node = node;
		Bandwidth maxBw = transport.getNetInterface().getMaxBandwidth();
		this.currentBandwidth = new BandwidthEstimator(maxBw.clone(), maxBw, 3);
		piggybackListeners = new LinkedList<TransitMessageHandler.TransitPiggybackListener>();
		this.transport = transport;
		this.transportToTracker = transportToTracker;
	}

	protected TransitNode getNode() {
		return node;
	}

	public MessageBasedTransport getTransport() {
		return transport;
	}

	public MessageBasedTransport getTransportToTracker() {
		return transportToTracker;
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		switch (peerStatus) {
		case PRESENT:
		case TO_JOIN:
			active = true;
			break;

		case ABSENT:
			active = false;
			break;

		default:
			throw new AssertionError("Unknown PeerStatus.");
		}
	}

	/**
	 * Adds a listener that is notified if messages are sent. The listener might
	 * then add payload to be piggybacked.
	 * 
	 * @param listener
	 */
	public void addPiggybackListener(TransitPiggybackListener listener) {
		piggybackListeners.add(listener);
	}

	/**
	 * Dispatcher for the SendAndWait-Method. This allows us to have a central
	 * place where all messages pass through. The timeout for the message is
	 * based on the connection if we have established one.
	 * 
	 * @param message
	 * @param receiver
	 * @param callback
	 * @return commId
	 */
	public int sendAndWait(TransitMessage message, TransitContact receiver,
			TransMessageCallback callback) {

		if (!active) {
			/*
			 * Might happen when host was taken offline by churn modell but
			 * periodic events are still scheduling.
			 * 
			 * TODO: Proper shutdown for TransitNode.
			 */

			return -1;
		}

		assert active;

		for (TransitPiggybackListener listener : piggybackListeners) {
			TransitMessage msg = listener.whatToPiggyback(receiver);
			if (msg != null) {
				message.piggyback(msg);
			}
		}

		currentBandwidth.outgoingTransmission(message.getSize()
				+ transport.getHeaderSize());
		TransitMessageCallback tCallback = new TransitMessageCallback(receiver,
				callback, message);
		message.setWantsReply();

		/*
		 * Compute the timeout: 1.5s for new contacts, otherwise based on
		 * RTT-measurements, with 750ms min.
		 */
		TransitConnection connection = node.getNeighborhood()
				.getConnectionManager().getConnection(receiver);
		long timeout = Time.SECOND;
		if (connection != null) {
			timeout = Math.max(connection.getCurrentRTTEstimation() * 3,
					1 * Time.SECOND);
		} else if (receiver.getType() == ContactType.TRACKER) {
			timeout = 5 * Time.SECOND;
		}
		return transport.sendAndWait(message, receiver.getNetID(), receiver.getPort(), tCallback,
				timeout);
	}

	/**
	 * Dispatcher for the send-Method
	 * 
	 * @param message
	 * @param receiver
	 * @return commId
	 */
	public void send(TransitMessage message, TransitContact receiver) {
		assert active;

		for (TransitPiggybackListener listener : piggybackListeners) {
			TransitMessage msg = listener.whatToPiggyback(receiver);
			if (msg != null) {
				message.piggyback(msg);
			}
		}
		currentBandwidth.outgoingTransmission(message.getSize()
				+ transport.getHeaderSize());

		if (receiver.getType() == ContactType.TRACKER) {
			transportToTracker.send(message,
					receiver.getNetID(), receiver.getPort());
		} else {
			transport.send(message, receiver.getNetID(), receiver.getPort());
		}
	}

	/**
	 * Dispatcher for reply-messages - protected, because in all
	 * request-reply-APIs throughout Transit you will be provided with a
	 * {@link TransitReplyHandler} that you have to use to send a reply.
	 * 
	 */
	protected int sendReply(TransitMessage message, TransitMessage request,
			TransitContact receiver, int commId) {
		assert active;

		for (TransitPiggybackListener listener : piggybackListeners) {
			TransitMessage msg = listener.whatToPiggyback(receiver);
			if (msg != null) {
				message.piggyback(msg);
			}
		}
		currentBandwidth.outgoingTransmission(message.getSize()
				+ transport.getHeaderSize());

		if (receiver.getType() == ContactType.TRACKER) {
			return transportToTracker.sendReply(message, receiver.getNetID(), receiver.getPort(), commId);
		} else {
			return transport.sendReply(message, receiver.getNetID(), receiver.getPort(), commId);
		}
	}

	/**
	 * This method is called once for <b>every</b> message that arrives. It is
	 * not used to handle a message, instead it should be used to update routing
	 * tables and contact timestamps in a central fashion.
	 * 
	 * @param msg
	 * @param RTT
	 *            (only available in Request-Reply scenarios, 0 otherwise)
	 */
	protected void messageArrived(TransitMessage msg, long rtt) {
		currentBandwidth.incomingTransmission(msg.getSize()
				+ transport.getHeaderSize());
		node.getNeighborhood().messageArrived(msg.getSenderContact(), rtt,
				msg.getSize());
	}

	/**
	 * This method is called once for every message that is lost, if we are able
	 * to get this information. This may happen in a request-reply scenario when
	 * the timeout fires or in a scenario where we use the
	 * {@link ModularTransLayer} with {@link TransmissionControlProtocol} and
	 * callbacks.
	 * 
	 * @param contact
	 * @param isTcpFailure
	 *            true, if the contact was not reachable as indicated by the
	 *            Transport Protocol. This is in most cases a reliable
	 *            indication that the host is no longer online.
	 * @param transInfo
	 *            if this is a tcp-failure, we do not have a contact, instead we
	 *            only get the transInfo
	 */
	protected void messageDropped(TransitContact contact, boolean isTcpFailure,
			TransInfo transInfo) {
		if (contact != null) {
			node.getNeighborhood().timeout(contact);
		} else {
			assert isTcpFailure;
			assert transInfo != null;
			/*
			 * TODO TCP is currently not used.
			 */
		}
	}

	/**
	 * Processes all piggybacked information
	 * 
	 * @param piggybackedMessages
	 */
	protected void processPiggybackedMessages(
			List<TransitMessage> piggybackedMessages) {
		for (TransitMessage message : piggybackedMessages) {
			TransitReplyHandler replyHandler = getReplyHandler(message
					.getSenderContact());
			processMessage(message, replyHandler);
		}
	}

	/**
	 * Processing of a message
	 * 
	 * @param msg
	 */
	private void processMessage(TransitMessage msg,
			TransitReplyHandler replyHandler) {
		if (msg instanceof ConnectionMessage) {
			node.getNeighborhood()
					.getConnectionManager()
					.receivedConnectionMessage((ConnectionMessage) msg,
							replyHandler);
		} else if (msg instanceof TransitBlockMessage) {
			node.getScheduler().receivedBlock((TransitBlockMessage) msg,
					replyHandler);
			node.getNeighborhood().getConnectionManager()
					.receivedPayloadMessage(msg.getSenderContact());
		} else if (msg instanceof TransitOfferNeighborhoodMessage) {
			if (node.getSettings().getParam(TransitParams.BATCH) == 1 || node.getSettings().getParam(TransitParams.BATCH) == 2) {
				if (node.getNeighborhood().isInBatchJoin()) {
					/* We ignore the neighborhood messages if we are still in the batch join process. */
					return;
				}
			}
			TransitOfferNeighborhoodMessage nMsg = (TransitOfferNeighborhoodMessage) msg;
			node.getNeighborhood().receivedNeighborhood(
					nMsg.getSenderContact(), nMsg.getNeighbors(), replyHandler);
		} else if (msg instanceof TransitRequestMessage) {
			node.getScheduler()
					.getRequestManager()
					.receivedRequestMessage((TransitRequestMessage) msg,
							replyHandler);
			node.getNeighborhood().getConnectionManager()
					.receivedPayloadMessage(msg.getSenderContact());
		} else if (msg instanceof TransitFlowMessage) {
			node.getScheduler()
					.getFlowManager()
					.receivedFlowMessage((TransitFlowMessage) msg, replyHandler);
			node.getNeighborhood().getConnectionManager()
					.receivedPayloadMessage(msg.getSenderContact());
		} else if (msg instanceof NodeInfoMessage) {
			node.getNeighborhood().getConnectionManager().receivedNodeInfo((NodeInfoMessage) msg);
		} else if (msg instanceof SetConnectionDirectionMessage) {
			node.getNeighborhood().getConnectionManager().receivedSetConnectionDirectionMsg((SetConnectionDirectionMessage) msg, replyHandler);
		} else if (msg instanceof JoinOverlayCommand) {
			JoinOverlayCommand jMsg = (JoinOverlayCommand) msg;
			node.setVideo(jMsg.getStreamingDocument());
			node.getNeighborhood().receivedNeighborhood(jMsg.getSenderContact(), jMsg.getContacts(), null);
		} else {
			throw new AssertionError("Unknown message " + msg.toString());
		}
	}

	@Override
	public void messageArrived(Message message, TransInfo sender, int commId) {
		TransitMessage msg = (TransitMessage) message;
		TransitReplyHandler replyHandler = new TransitReplyHandler(msg,
				msg.getSenderContact(), msg.wantsReply(), commId);

		/*
		 * no way to estimate the RTT here, as this is not a request-reply
		 * scenario.
		 */
		messageArrived(msg, 0);

		processMessage(msg, replyHandler);

		if (msg.hasPiggybackedMessages()) {
			processPiggybackedMessages(msg.getPiggybackedMessages());
		}
	}

	// @Override
	// public final void connectionFailed(TransInfo receiverInfo) {
	// /*
	// * This is triggered by a TransProtocol such as TCP, if supported. Use
	// * the information to declare a node dead.
	// */
	// messageDropped(null, true, receiverInfo);
	// }

	/**
	 * This allows us to use the sendAndWait-method of the {@link TransLayer}
	 * but at the same time maintain a central point where all messages pass
	 * through. This comes in handy if additional information is sent with a
	 * message.
	 * 
	 * Therefore, this is a pass-through callback for the real
	 * {@link TransMessageCallback} that is provided by the respective
	 * Operation.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 29.05.2012
	 */
	public class TransitMessageCallback implements TransMessageCallback {

		private final TransMessageCallback callback;

		private final TransitContact receiver;

		private final TransitMessage requestMessage;

		private final long sentTime;

		public TransitMessageCallback(TransitContact receiver,
				TransMessageCallback callback, TransitMessage requestMessage) {
			this.callback = callback;
			this.receiver = receiver;
			this.sentTime = Time.getCurrentTime();
			this.requestMessage = requestMessage;
		}

		@Override
		public void receive(Message msg, TransInfo senderInfo, int commId) {
			if (!active) {
				return;
			}
			TransitMessage tMsg = (TransitMessage) msg;
			messageArrived(tMsg, Time.getCurrentTime() - sentTime);
			callback.receive(tMsg, senderInfo, commId);
			if (tMsg.hasPiggybackedMessages()) {
				processPiggybackedMessages(tMsg.getPiggybackedMessages());
			}
		}

		@Override
		public void messageTimeoutOccured(int commId) {
			if (!active) {
				return;
			}
			messageDropped(receiver, false, null);
			callback.messageTimeoutOccured(commId);
		}

	}

	/**
	 * Use the {@link TransitReplyHandler}-Objects to unify the request/reply
	 * functionality inside of transit.
	 * 
	 * @param replyTo
	 * @return
	 */
	public TransitReplyHandler getReplyHandler() {
		return new TransitReplyHandler(node.getLocalOverlayContact());
	}

	/**
	 * The replyHandler unifies send and sendAndWait-messages throughout
	 * transit. Use this object to reply to a message!
	 * 
	 * @param contact
	 * @return
	 */
	public TransitReplyHandler getReplyHandler(TransitContact contact) {
		return new TransitReplyHandler(contact);
	}

	/**
	 * This handler ensures correct (and more important: unified) dispatching of
	 * messages that are sent as a reply. Some of them might be an answer to a
	 * sendAndWait, while other might just be a reply without a callback waiting
	 * for them.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 15.06.2012
	 */
	public class TransitReplyHandler {

		private final TransitMessage request;

		private final boolean wantsReply;

		private final TransitContact replyTo;

		private final int commId;

		private int numOfReplies = 0;

		/**
		 * 
		 * @param receivingEvent
		 * @param wantsReply
		 */
		protected TransitReplyHandler(TransitMessage request,
				TransitContact replyTo, boolean wantsReply, int commId) {
			this.request = request;
			this.replyTo = replyTo;
			this.wantsReply = wantsReply;
			this.commId = commId;
		}

		/**
		 * Handler for messages that do not result from a request
		 * 
		 * @param replyTo
		 */
		protected TransitReplyHandler(TransitContact replyTo) {
			this(null, replyTo, false, 0);
		}

		public void reply(TransitMessage msg) {
			numOfReplies++;
			if (wantsReply) {
				sendReply(msg, request, replyTo, commId);
			} else {
				send(msg, replyTo);
			}
			if (numOfReplies > 1) {
				throw new UnsupportedOperationException(
						"This is not necessarily an error, but at the current stage of Transit there should be no occurance of this!");
			}
		}
	}

	/**
	 * Enables components to piggyback information on top of other messages.
	 * These Listeners have to be registered with the
	 * {@link TransitMessageHandler}. If information is to be sent periodically
	 * to each neighbor, consider extending the
	 * {@link PeriodicPiggybackListener}
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	public interface TransitPiggybackListener {

		/**
		 * If you want to piggyback a message to the given contact, just return
		 * it.
		 * 
		 * @param to
		 * @return
		 */
		public TransitMessage whatToPiggyback(TransitContact to);

	}

	/**
	 * Abstract base for periodic piggybacking
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	public static abstract class PeriodicPiggybackListener implements
			TransitPiggybackListener {

		private long interval = 10 * Time.SECOND;

		private final Map<TransitContact, Long> lastTimeSent;

		public PeriodicPiggybackListener(long interval) {
			this.interval = interval;
			this.lastTimeSent = new LinkedHashMap<TransitContact, Long>();
		}

		public void setInterval(long interval) {
			this.interval = interval;
		}

		public long getInterval() {
			return interval;
		}

		/**
		 * Return the message that you want to piggyback to the given contact.
		 * This method is called once for every contact for the first message in
		 * every interval.
		 * 
		 * @param to
		 * @return
		 */
		protected abstract TransitMessage whatToPiggybackPeriodically(
				TransitContact to);

		@Override
		public final TransitMessage whatToPiggyback(TransitContact to) {
			if (to.getType() != ContactType.CLIENT) {
				return null;
			}
			Long lastTime = lastTimeSent.get(to);
			if (lastTime == null
					|| lastTime.longValue() + interval < Time.getCurrentTime()) {
				lastTimeSent.put(to, Time.getCurrentTime());

				return whatToPiggybackPeriodically(to);
			}
			return null;
		}

	}

}
