/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.requesting;

import java.util.BitSet;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.RequestState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestManagerImpl;

/**
 * Basic version of a requestManager.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public class TransitRequestStrategyPullToken extends AbstractRequestingStrategy {

	/**
	 * Instantiates a new simple request strategy.
	 * 
	 * @param transitRequestManager
	 *            the transit request manager
	 */
	public TransitRequestStrategyPullToken(TransitRequestManagerImpl transitRequestManager) {
		super(transitRequestManager);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling
	 * .strategies.requesting.RequestingStrategy#requestBlocks(int,
	 * java.util.BitSet)
	 */
	@Override
	public void requestBlocks(int blockOffset, BitSet mask) {

		if (getNumberOfActiveRequests() > getNode().getSettings().getParam(TransitParams.REQUEST_MAX_PARALLEL)) {
			return;
		}

		List<TransitConnection> connections = getNode().getNeighborhood().getConnectionManager()
		        .getConnections(ConnectionDirection.IN, ConnectionState.OPEN, null, RequestState.INACTIVE);

		if (connections.size() == 0) {
			/* Failed. */
			return;
		}

		/* Ask nodes first, who owe us. */
		LinkedList<TransitConnection> connectionWithToken = new LinkedList<TransitConnection>();
		LinkedList<TransitConnection> connectionWithInvalidToken = new LinkedList<TransitConnection>();
		LinkedList<TransitConnection> connectionWithoutToken = new LinkedList<TransitConnection>();
		for (TransitConnection transitConnection : connections) {
			if (transitConnection.getPullToken() != null) {
				if (transitConnection.getPullToken().getValidUntil() <= Time.getCurrentTime()) {
					connectionWithToken.add(transitConnection);
				} else {
					connectionWithInvalidToken.add(transitConnection);
				}
			} else {
				connectionWithoutToken.add(transitConnection);
			}
		}
		Collections.shuffle(connectionWithToken);
		Collections.shuffle(connectionWithInvalidToken);
		Collections.shuffle(connectionWithoutToken);
		connectionWithToken.addAll(0, connectionWithInvalidToken);
		connectionWithToken.addAll(0, connectionWithoutToken);

		/* Now send request to hosts. */
		for (TransitConnection connection : connectionWithToken) {
			if (mask.isEmpty()) {
				break;
			}

			if (connection.getBufferMap() == null || connection.getBufferMap().getBlockMap().size() == 0) {
				continue;
			}

			if (connection.getCurrentRTTEstimation() > getNode().getSettings().getTime(TransitTimes.REQUEST_RTT_MAX)) {
				continue;
			}

			if (connection.getEndpoint().getLoad() > 6) {
				continue;
			}

			assert connection.getRequestState() == RequestState.INACTIVE;
			assert connection.getRequest() == null;
			doRequestAndMaskRequest(connection, blockOffset, mask);
		}
	}
}
