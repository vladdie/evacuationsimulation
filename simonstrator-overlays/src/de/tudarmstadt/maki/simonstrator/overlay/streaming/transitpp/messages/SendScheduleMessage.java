/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitFlowSchedule;

/**
 * Simple message that contains a schedule, this is used in both direction. The
 * exact meaning of the message is defined by the {@link FlowMsgType}
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 11.06.2012
 */
public class SendScheduleMessage extends AbstractTransitMessage implements
		TransitFlowMessage {

	private final TransitFlowSchedule schedule;

	private final FlowMsgType messageType;
	
	private TransitContact childToReplace;
	
	private long calculatedRTT;

	/**
	 * Shortcut for {@link FlowMsgType}s that do not include a schedule but
	 * instead just send a flag.
	 * 
	 * @param sender
	 * @param messageType
	 */
	public SendScheduleMessage(TransitContact sender, FlowMsgType messageType) {
		this(sender, messageType, null);
		assert messageType == FlowMsgType.CANCEL
				|| messageType == FlowMsgType.DENY;
	}

	/**
	 * A new {@link SendScheduleMessage} with a schedule included.
	 * 
	 * @param sender
	 * @param messageType
	 * @param schedule
	 */
	public SendScheduleMessage(TransitContact sender, FlowMsgType messageType,
			TransitFlowSchedule schedule) {
		super(sender);
		this.messageType = messageType;
		this.schedule = schedule;
	}
	
	/** For REQUEST_REPLACE_CHILD messages.
	 */
	public SendScheduleMessage(TransitContact sender,
			TransitFlowSchedule schedule, TransitContact childToReplace, long calculatedRTT) {
		super(sender);
		this.messageType = FlowMsgType.REQUEST_REPLACE_CHILD;
		this.schedule = schedule;
		this.childToReplace = childToReplace;
		this.calculatedRTT = calculatedRTT;
	}

	@Override
	public TransitFlowSchedule getFlowSchedule() {
		return schedule;
	}

	@Override
	public FlowMsgType getType() {
		return messageType;
	}

	@Override
	public String toString() {
		return messageType.toString()
				+ (schedule != null ? " " + schedule.toString() : "");
	}

	@Override
	public long getSize() {
		switch (messageType) {
		case CANCEL:
		case DENY:
			return super.getSize() + 1;
		case REQUEST_REPLACE_CHILD:
			return super.getSize() + 1 + schedule.getTransmissionSize() + childToReplace.getTransmissionSize() + 8; 
		default:
			return super.getSize() + 1 + schedule.getTransmissionSize();
		}

	}

	@Override
	public TransitContact getNodeToReplace() {
		return childToReplace;
	}

	@Override
	public long getCalculatedRTT() {
		return calculatedRTT;
	}

}
