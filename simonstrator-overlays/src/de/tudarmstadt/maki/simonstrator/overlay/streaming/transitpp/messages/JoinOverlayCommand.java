/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingDocument;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;

/**
 * Tracker sends this message to the root of the batch join in order to let him
 * join the overlay. Contains links to nodes with capacity in the overlay.
 * 
 * @author Eduardo Lidanski
 * @version 1.0, 03.03.2014
 */
public class JoinOverlayCommand extends AbstractTransitMessage {

	private List<TransitContact> contacts;
	
	private StreamingDocument streamingDocument;
	
	public JoinOverlayCommand(TransitContact sender, List<TransitContact> contacts, StreamingDocument streamingDocument) {
		super(sender);
		this.contacts = contacts;
		this.streamingDocument = streamingDocument;
	}
	
	public List<TransitContact> getContacts() {
		return contacts;
	}
	
	public StreamingDocument getStreamingDocument() {
		return streamingDocument;
	}
	@Override
	public long getSize() {
		long size = 0;
		size += super.getSize();
		size += 50;
		for (int i=0;i<= contacts.size()-1; i++) {
			size += contacts.get(i).getTransmissionSize();
		}
		return size;
	}
}
