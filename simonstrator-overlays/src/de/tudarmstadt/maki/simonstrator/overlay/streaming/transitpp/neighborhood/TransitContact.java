/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetID;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetworkComponent.NetInterfaceName;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.overlay.BasicOverlayContact;

/**
 * Overlay Contact in the transit-Overlay. As this may contain information that
 * is estimated on a per-node basis, for example the current layer, it has to be
 * cloned when sent inside a message.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public class TransitContact extends BasicOverlayContact implements Cloneable {


	public static enum ContactType {
		SOURCE, TRACKER, CLIENT
	}

	private final ContactType type;

	/**
	 * Layer that is currently being played by the node.
	 */
	private byte[] layer;

	/**
	 * Playback-Position of this node
	 */
	private int currentChunk;

	/**
	 * Overloaded-Flag (0 to 10)
	 */
	private short load = 0;

	/**
	 * Timestamp of the lastUpdate
	 */
	private long lastUpdate = Time.getCurrentTime();
	
	private long joiningTime = -1;

	/**
	 * A new TransitContact (not a tracker, not a source)
	 * 
	 * @param nodeID
	 * @param transInfo
	 */
	public TransitContact(INodeID nodeID, TransInfo transInfo) {
		this(nodeID, transInfo, ContactType.CLIENT);
	}

	/**
	 * A new TransitContact (used for Trackers and Sources). If a node acts as a
	 * tracker and an overlay node, it will get two {@link TransitContact}s, as
	 * the ports will differ.
	 * 
	 * @param nodeID
	 * @param transInfo
	 * @param nodeType
	 */
	public TransitContact(INodeID nodeID, TransInfo transInfo,
			ContactType nodeType) {
		super(nodeID, transInfo);
		this.type = nodeType;
	}

	/**
	 * A new TransitContact (used for Trackers and Sources). If a node acts as a
	 * tracker and an overlay node, it will get two {@link TransitContact}s, as
	 * the ports will differ.
	 * 
	 * @param nodeID
	 * @param nodeType
	 */
	public TransitContact(INodeID nodeID, NetID netId, int port, ContactType nodeType) {
		super(nodeID, NetInterfaceName.ETHERNET, netId, port);
		this.type = nodeType;
	}

	/**
	 * Call this method to update an already existing contact in the routing
	 * table rather than inserting the new contact. This allows better error
	 * handling and debugging.
	 * 
	 * @param newInformation
	 */
	public void update(TransitContact newInformation) {
		assert this.equals(newInformation);
		/*
		 * update needed fields
		 */
		layer = newInformation.getLayer();
		lastUpdate = Time.getCurrentTime();
		load = newInformation.getLoad();
		currentChunk = newInformation.getCurrentChunk();
		if (newInformation.getJoiningTime()!=-1) {
			joiningTime = newInformation.getJoiningTime();
		}
	}

	public NetID getNetID() {
		return super.getNetID(NetInterfaceName.ETHERNET);
	}

	public int getPort() {
		return super.getPort(NetInterfaceName.ETHERNET);
	}

	/**
	 * from 0 to 10 with 0 meaning no load, and 10 meaning the contact is
	 * overloaded
	 * 
	 * @return
	 */
	public short getLoad() {
		return load;
	}

	/**
	 * Timestamp of the last update of this contact (i.e. the last time we
	 * received a message by the contact)
	 * 
	 * @return
	 */
	public long getLastUpdate() {
		return lastUpdate;
	}

	/**
	 * last known SVC-layer this contact is playing
	 * 
	 * @return
	 */
	public byte[] getLayer() {
		return layer;
	}

	/**
	 * Last known playback position (chunk)
	 * 
	 * @return
	 */
	public int getCurrentChunk() {
		return currentChunk;
	}

	/**
	 * Called, whenever we receive information about a layer change of the given
	 * node
	 * 
	 * @param layer
	 */
	public void updateLayer(byte[] layer) {
		this.layer = layer.clone();
	}

	public void setLoad(short load) {
		this.load = load;
	}

	public void updateCurrentChunk(int chunk) {
		this.currentChunk = chunk;
	}
	
	public void setJoiningTime(long joiningTime) {
		this.joiningTime = joiningTime;
	}
	
	public long getJoiningTime(){
		return joiningTime;
	}

	/**
	 * Type of this contact
	 * 
	 * @return
	 */
	public ContactType getType() {
		return type;
	}

	@Override
	public TransitContact clone() {
		/*
		 * Return a clone of the current TransitContact
		 */
		TransitContact clone = new TransitContact(getNodeID(),
				getNetID(), getPort(), getType());
		clone.update(this);
		return clone;
	}

	@Override
	public int getTransmissionSize() {
		/*
		 * Currently, only the ID and TransInfo are considered. Later, we might
		 * add additional information which we then need to account for.
		 */
		return super.getTransmissionSize() + 6;
	}

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("[");
		str.append(String.format("%1$-" + 3 + "s",
 getNodeID().toString()));
		str.append("]");
//		str.append(String.format("%1$-" + 8 + "s", type.toString()));
//		str.append(Arrays.toString(layer));
//		str.append(" c");
//		str.append(currentChunk);
//		str.append(" l");
//		str.append(load);
		return str.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((getNodeID() == null) ? 0 : getNodeID().hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TransitContact other = (TransitContact) obj;
		if (getNodeID() == null) {
			if (other.getNodeID() != null) {
				return false;
			}
		} else if (!getNodeID().equals(other.getNodeID())) {
			return false;
		}
		return true;
	}

}
