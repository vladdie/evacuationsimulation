package de.tudarmstadt.maki.simonstrator.overlay.streaming;

import java.util.BitSet;

/**
 * Interface for a layered video stream
 * 
 * @author Osama Abboud
 */
public interface VideoModel {

	/**
	 * @return the size of the video (in bytes)
	 */
	public long getVideoFileSize();

	/**
	 * 
	 * @return total number of chunks(playback units) of the video
	 */
	public int getNumOfChunks();

	/**
	 * 
	 * @return number of blocks per chunk
	 */
	public int getNumOfBlocksPerChunk();

	/**
	 * @param blockNum
	 *            the position of the block in the video file
	 * @return return the size of a block(in bytes)
	 */
	public int getBlockSize(int blockNum);

	/**
	 * 
	 * @return number of levels in temporal-dimension
	 */
	public byte getTemporalLevels();

	/**
	 * 
	 * @return number of levels in spatial-dimension
	 */
	public byte getSpatialLevels();

	/**
	 * 
	 * @return number of levels in SNR-dimension
	 */
	public byte getSnrLevels();

	/**
	 * @param blockNum
	 *            The absolute block number in a video file
	 * @return the layer level the given block belongs to.
	 */
	public byte[] getBlockLayer(int blockNum);

	/**
	 * 
	 * @param layer
	 *            the given SVC layer level in the order: d,t,q
	 * @return the pattern of block distribution in the given layer
	 */
	public BitSet getLayerMask(byte[] layer);

	/**
	 * 
	 * @param layer
	 *            a given layer level
	 * @return the position of the layer in a linearized chunk
	 * 
	 *         [JR] Note: This seems to be also the index in the table showing
	 *         the quality level combinations as used in the papers!
	 */
	public Byte getLayerIndex(byte[] layer);

	/**
	 * @param d
	 *            spatial layer level
	 * @param t
	 *            temporal layer level
	 * @param q
	 *            SNR layer level
	 * @return byte-rate (KByte/s) of a certain layer
	 */
	public double getLayerByteRate(int d, int t, int q);

	/**
	 * 
	 * @param d
	 * @param t
	 * @param q
	 * @return the complexity of the given layer
	 */
	public int getLayerComplexity(int d, int t, int q);

}
