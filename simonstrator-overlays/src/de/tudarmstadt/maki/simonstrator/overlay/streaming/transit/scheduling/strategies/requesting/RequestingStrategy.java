/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.requesting;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestManagerImpl.RequestInfo;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestSchedule;

public interface RequestingStrategy {

	public void requestBlocks(int blockOffset, BitSet mask);

	public void cancelRequest(int blocknum);

	public int getNumberOfActiveRequests();

	public void receivedRequestedBlock(TransitConnection connection, TransitBlockMessage block);
	
	public void requestDenied(TransitConnection connection);
	
	public void requestAltered(TransitConnection connection, TransitRequestSchedule newRequestSchedule);

	public RequestInfo getRequestInfo(int i);
	
}
