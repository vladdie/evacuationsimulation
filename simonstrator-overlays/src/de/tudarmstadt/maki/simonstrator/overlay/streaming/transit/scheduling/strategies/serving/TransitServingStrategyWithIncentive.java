/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.serving;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.TransitServingAdvise;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.TransitServingAdvise.ServingAdvise;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestManagerImpl;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.PiecePickingHelper;

public class TransitServingStrategyWithIncentive extends AbstractServingStrategy {

	private final long SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_PERCENT_LOAD;

	private final static double SCHEDULER_NEVER_ABOVE_VIDEO_TIMES = 3.0;
	private final static double SCHEDULER_ALWAYS_BELOW_VIDEO_TIMES = 0.8;

	public TransitServingStrategyWithIncentive(TransitRequestManagerImpl transitRequestManager) {
		super(transitRequestManager);
		SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_PERCENT_LOAD = transitRequestManager.getNode().getSettings()
		        .getParam(TransitParams.SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_PERCENT_LOAD);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling
	 * .strategies .serving
	 * .ServingStrategy#handleRequest(de.tud.kom.p2psim.impl.overlay.streaming
	 * .transit.neighborhood.TransitConnection,
	 * de.tud.kom.p2psim.impl.overlay.streaming
	 * .transit.scheduling.TransitRequestSchedule)
	 */
	@Override
	public void handleRequest(TransitConnection connection, TransitRequestSchedule request) {

		if (request == null || connection == null || !transitRequestManager.getNode().isPresent())
			return;

		/* Check if bandwidth is available. */
		final double connectionLoad = transitRequestManager.getNode().getBandwidthManager()
		        .getLoad(ConnectionDirection.OUT);
		final double usedBandwidth = transitRequestManager.getNode().getBandwidthManager()
		        .getUsedBandwidth(ConnectionDirection.OUT) / 1024;
		final double videoRate = StreamingConfiguration.getVideoByteRate();

		double contributionRatio = usedBandwidth / videoRate;

		if (connectionLoad * 100 > SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_PERCENT_LOAD
		        || (contributionRatio > SCHEDULER_NEVER_ABOVE_VIDEO_TIMES)) {
			/* Okay, not too much bandwidth left. Deny this one. */
			doDenyRequest(connection);
			return;
		}

		/* Serve request with propability p. */
		TransitServingAdvise advisor = transitRequestManager.getNode().getIncentiveManager()
		        .getServingAdvise(connection);

		/* SERVE FULL; */
		if (advisor.getServingAdvise().equals(ServingAdvise.A_SERVE_FULL)) {
			new ServeRequestWorker(connection);
			return;

			/* Don't Serve at all. */
		} else if (advisor.getServingAdvise().equals(ServingAdvise.A_DO_NOT_SERVE_AND_CLOSE)) {
			doDenyRequest(connection);
			transitRequestManager.getNode().getNeighborhood().getConnectionManager().closeConnection(connection);
			return;
		}

		if (contributionRatio > SCHEDULER_ALWAYS_BELOW_VIDEO_TIMES) {

			/* SERVE DIFFERENT; only base layer; maybe according to PROB */
			if (advisor.getServingAdvise().equals(ServingAdvise.A_SERVE_BASELAYER)) {
				if (advisor.getServingProbability() > Randoms.getRandom(TransitServingStrategyWithIncentive.class)
				        .nextDouble()) {
					boolean successful = modifyRequestToBaselayer(connection);
					if (successful) {
						new ServeRequestWorker(connection);
					} else {
						doDenyRequest(connection);
					}
				} else {
					doDenyRequest(connection);
				}
				return;

				/* SERVE MAYBE; according to PROB */
			} else if (advisor.getServingAdvise().equals(ServingAdvise.A_SERVE_WITH_PROBABILITY)) {
				if (advisor.getServingProbability() > Randoms.getRandom(TransitServingStrategyWithIncentive.class)
				        .nextDouble()) {
					new ServeRequestWorker(connection);
				} else {
					doDenyRequest(connection);
				}
				return;
			}

		} else {

			/* Plenty bandwidth left anyway. Serve! */
			if (advisor.getServingAdvise().equals(ServingAdvise.A_SERVE_BASELAYER)) {
				boolean successful = modifyRequestToBaselayer(connection);
				if (!successful) {
					doDenyRequest(connection);
					return;
				}
			}
			new ServeRequestWorker(connection);
			return;

		}

		doDenyRequest(connection);

	}

	/**
	 * Modify request to baselayer.
	 * 
	 * @param connection
	 *            the connection
	 * @return
	 */
	private boolean modifyRequestToBaselayer(TransitConnection connection) {

		/* Alter request to only serve base layer. */
		final BitSet alteredMap = (BitSet) connection.getRequest().getBlockMap().clone();
		final BitSet BASELAYER_MASK = PiecePickingHelper.getBaseLayerMask(
				transitRequestManager.getNode().BLOCKS_PER_CHUNK,
				alteredMap.size());
		alteredMap.and(BASELAYER_MASK);

		/* Check if more than baselayer was requested. If so, alter request. */
		if (!alteredMap.equals(connection.getRequest().getBlockMap()) && !alteredMap.isEmpty()) {
			TransitRequestSchedule newSchedule = new TransitRequestSchedule(connection.getRequest().getBlockOffset(),
			        alteredMap);
			doAlterRequest(connection, newSchedule);
			return true;
		}

		return false;
	}
}