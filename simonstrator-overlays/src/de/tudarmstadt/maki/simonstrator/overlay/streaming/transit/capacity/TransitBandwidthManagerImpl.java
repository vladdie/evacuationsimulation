/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.capacity;

import java.util.BitSet;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.network.Bandwidth;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitFlowSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestSchedule;

/**
 * Basic implementation of the {@link TransitBandwidthManager}. TODO
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 17.08.2012
 */
public class TransitBandwidthManagerImpl implements TransitBandwidthManager {

	// private final Bandwidth freeBandwidth;

	private final Bandwidth freeBandwidthOwnMeasurement;

	// private final Bandwidth usedBandwidthOwnMeasurement;

	private final Bandwidth maxBandwidth;

	private final TransitNode node;

	private long[] reservedBandwidth = new long[ConnectionDirection.values().length];

	private long reservedUpdateTime = 0;

	private final double MAX_BW_SHARE;

	public TransitBandwidthManagerImpl(TransitNode node) {
		// this.freeBandwidth =
		// node.getHost().getNetLayer().getCurrentBandwidth();
		this.maxBandwidth = node.getMessageHandler().getTransport()
				.getNetInterface().getMaxBandwidth();
		this.freeBandwidthOwnMeasurement = node.getMessageHandler().currentBandwidth
				.getEstimatedBandwidth();
		// this.usedBandwidthOwnMeasurement =
		// node.getMessageHandler().currentUsedBandwidth
		// .getEstimatedBandwidth();
		this.node = node;
		MAX_BW_SHARE = ((double) node.getSettings().getParam(
				TransitParams.BANDWIDTH_MAX_SHARE)) / 100;
	}

	private double getMaxBw(ConnectionDirection direction) {
		if (direction == ConnectionDirection.IN) {
			return maxBandwidth.getDownBW();
		} else {
			return maxBandwidth.getUpBW();
		}
	}

	private double getFreeBw(ConnectionDirection direction) {
		if (direction == ConnectionDirection.IN) {
			return freeBandwidthOwnMeasurement.getDownBW();
		} else {
			return freeBandwidthOwnMeasurement.getUpBW();
		}
	}

	@Override
	public boolean hasSpareBandwidthFor(int blocknum,
			ConnectionDirection direction) {
		long blockSize = node.getVideo().getBlockSize(blocknum);
		if (getCurrentBandwidthNeedForFlows(direction) + blockSize
				+ getReservedBandwidth(direction) > getMaxBw(direction)
				* MAX_BW_SHARE) {
			return false;
		}

		if (node.isServer()) {
			return blockSize + getReservedBandwidth(direction) < getFreeBw(direction)
					* MAX_BW_SHARE; // 0.9
		}
		return blockSize + getReservedBandwidth(direction) < getFreeBw(direction)
				* MAX_BW_SHARE; // 0.8
	}

	@Override
	public void reserveBandwidthFor(int blocknum, ConnectionDirection direction) {
		reservedBandwidth[direction.ordinal()] = getReservedBandwidth(direction)
				+ node.getVideo().getBlockSize(blocknum);
	}

	private long getReservedBandwidth(ConnectionDirection direction) {
		if (reservedUpdateTime
				+ (1 / StreamingConfiguration.CHUNKS_PER_SECOND * Time.SECOND) < Time
					.getCurrentTime()) {
			reservedBandwidth[direction.ordinal()] = 0;
			reservedUpdateTime = Time.getCurrentTime();
		}
		return reservedBandwidth[direction.ordinal()];
	}

	@Override
	public boolean hasSpareBandwidthFor(TransitFlowSchedule flow,
			ConnectionDirection direction) {

		if (getCurrentBandwidthNeedForFlows(direction)
				+ getBandwidthNeededFor(flow) > getMaxBw(direction)
				* MAX_BW_SHARE) {
			return false;
		}

		if (node.isServer()) {
			return getBandwidthNeededFor(flow) < getFreeBw(direction)
					* MAX_BW_SHARE; // 0.6
		}
		return getBandwidthNeededFor(flow) < getFreeBw(direction)
				* MAX_BW_SHARE; // 0.9
	}

	@Override
	public double getLoad(ConnectionDirection direction) {
		return Math.max(0, 1 - (getFreeBw(direction) / getMaxBw(direction)));
	}

	@Override
	public double getUsedBandwidth(ConnectionDirection direction) {
		return getMaxBw(direction) - getFreeBw(direction);
	}

	protected double getCurrentBandwidthNeedForFlows(
			ConnectionDirection direction) {
		List<TransitConnection> activeFlows = node
				.getNeighborhood()
				.getConnectionManager()
				.getConnections(direction, ConnectionState.OPEN,
						ScheduleState.ACTIVE, null);

		double currentFlows = 0;
		for (TransitConnection activeFlow : activeFlows) {
			currentFlows += getBandwidthNeededFor(activeFlow.getSchedule());
		}
		return currentFlows;
	}

	protected double getBandwidthNeededFor(TransitFlowSchedule flow) {
		BitSet mask = flow.getBlockMask();
		long bw = 0;
		for (int i = mask.nextSetBit(0); i >= 0; i = mask.nextSetBit(i + 1)) {
			bw += node.getVideo().getBlockSize(i);
		}
		return bw * StreamingConfiguration.CHUNKS_PER_SECOND;
	}

	protected double getBandwidthNeededFor(TransitRequestSchedule request) {
		BitSet mask = request.getBlockMap();
		long bw = 0;
		for (int i = mask.nextSetBit(0); i >= 0; i = mask.nextSetBit(i + 1)) {
			bw += node.getVideo().getBlockSize(i + request.getBlockOffset());
		}
		return bw;
	}

	@Override
	public void onHasSpareBandwith(ConnectionDirection direction,
			TranssitBandwidthListener callback) {
		// TODO Auto-generated method stub
		
	}

}
