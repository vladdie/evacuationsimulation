/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.serving;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestManagerImpl;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestSchedule;

/**
 * The Class TransitServingStrategyMalicous.
 * 
 * No host is served at all.
 * 
 */
public class TransitServingStrategyMalicous extends AbstractServingStrategy {

	public TransitServingStrategyMalicous(TransitRequestManagerImpl transitRequestManager) {
		super(transitRequestManager);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling
	 * .strategies .serving
	 * .ServingStrategy#handleRequest(de.tud.kom.p2psim.impl.overlay.streaming
	 * .transit.neighborhood.TransitConnection,
	 * de.tud.kom.p2psim.impl.overlay.streaming
	 * .transit.scheduling.TransitRequestSchedule)
	 */
	@Override
	public void handleRequest(TransitConnection connection, TransitRequestSchedule request) {

		if (request == null || connection == null || !transitRequestManager.getNode().isPresent())
			return;

		doDenyRequest(connection);

	}

}
