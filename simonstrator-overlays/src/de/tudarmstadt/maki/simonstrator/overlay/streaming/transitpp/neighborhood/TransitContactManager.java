/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.LayerUtils;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact.ContactType;

/**
 * This is a storage for {@link TransitContact}s. It provides means to query for
 * contacts with specific features.
 * 
 * @deprecated this is only used by the tracker and will be merged into the
 *             TrackerRegistry soon.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
@Deprecated
public class TransitContactManager {

	private LinkedHashMap<UniqueID, TransitContact> contacts;

	private LinkedList<TransitContact> allContacts;

	public final ComparatorByLastUpdate COMP_LASTUPDATE = new ComparatorByLastUpdate(
			true);

	public TransitContactManager() {
		this.contacts = new LinkedHashMap<UniqueID, TransitContact>();
		this.allContacts = new LinkedList<TransitContact>();
	}

	/**
	 * Removes a contact
	 * 
	 * @param contact
	 */
	public void removeContact(TransitContact contact) {
		if (contact.getType() == ContactType.TRACKER) {
			return;
		}
		TransitContact removed = contacts.remove(contact.getNodeID());
		if (removed != null) {
			allContacts.remove(removed);
		}
	}

	/**
	 * This will add or update the given contact
	 * 
	 * @param contact
	 */
	public void addOrUpdateContact(TransitContact contact) {
		if (contact.getType() != ContactType.TRACKER) {
			TransitContact entry = contacts.get(contact.getNodeID());
			if (entry == null) {
				entry = contact.clone();
				contacts.put(contact.getNodeID(), entry);
				allContacts.add(entry);
			}
			entry.update(contact);
		}
	}

	/**
	 * Get all contacts
	 * 
	 * @return
	 */
	public List<TransitContact> allContacts() {
		return toList(allContacts, null, -1);
	}

	/**
	 * Number of contacts
	 * 
	 * @return
	 */
	public int getNumberOfContacts() {
		return allContacts.size();
	}

	/**
	 * Returns a list of contacts matching the given comparator and excluding
	 * specific contacts.
	 * 
	 * @param comparator
	 * @param exclude
	 *            Contacts to exclude
	 * @param maxSize
	 *            Max number of results
	 * @return
	 */
	public List<TransitContact> getSortedContacts(
			Comparator<TransitContact> comparator, List<UniqueID> exclude,
			int maxSize) {
		LinkedList<TransitContact> result = new LinkedList<TransitContact>(
				allContacts); 
		Collections.sort(result, comparator);
		return toList(result, exclude, maxSize);
	}

	/**
	 * Returning a list of contacts by removing the contacts that are to be
	 * excluded and limiting the size of the list to a maximum.
	 * 
	 * @param contactInfos
	 * @param exclude
	 *            or null
	 * @param maxSize
	 *            or -1
	 * @return
	 */
	private List<TransitContact> toList(List<TransitContact> contacts,
			List<UniqueID> exclude, int maxSize) {
		List<TransitContact> result = new LinkedList<TransitContact>();
		int i = 0;
		for (TransitContact contact : contacts) {
			if (exclude != null && exclude.contains(contact.getNodeID())) {
				continue;
			}
			result.add(contact);
			i++;
			if (i == maxSize) {
				break;
			}
		}
		return result;
	}

	/**
	 * This method returns a list of all contacts in our RoutingTable that are
	 * at least on the given SVC-layer.
	 * 
	 * @param layer
	 * @return
	 */
	public List<TransitContact> getContactsOnAndAboveLayer(byte[] layer) {
		List<TransitContact> results = new LinkedList<TransitContact>();
		for (TransitContact entry : allContacts) {
			if (entry.getType() == ContactType.TRACKER) {
				continue;
			}
			if (LayerUtils
					.compareLayer(layer.clone(), entry.getLayer().clone()) >= 0) {
				results.add(entry);
			}
		}
		return results;
	}

	/**
	 * Get a list of contacts that are on exactly the specified layer
	 * 
	 * @param layer
	 * @return
	 */
	public List<TransitContact> getContactsOnLayer(byte[] layer) {
		List<TransitContact> results = new LinkedList<TransitContact>();
		for (TransitContact entry : allContacts) {
			if (entry.getType() == ContactType.TRACKER) {
				continue;
			}
			if (LayerUtils
					.compareLayer(layer.clone(), entry.getLayer().clone()) == 0) {
				results.add(entry);
			}
		}
		return results;
	}

	/**
	 * Sorts a list of contacts by the time they have been updated
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 10.07.2012
	 */
	private static class ComparatorByLastUpdate implements
			Comparator<TransitContact> {

		private final boolean newestFirst;

		public ComparatorByLastUpdate(boolean newestFirst) {
			this.newestFirst = newestFirst;
		}

		@Override
		public int compare(TransitContact o1, TransitContact o2) {
			int ret = 0;
			if (o1.getLastUpdate() < o2.getLastUpdate()) {
				ret = 1;
			} else if (o1.getLastUpdate() > o2.getLastUpdate()) {
				ret = -1;
			}
			if (!newestFirst) {
				ret = ret * -1;
			}
			return ret;
		}
	}

	@Override
	public String toString() {
		return "TConM: " + allContacts.toString();
	}

}
