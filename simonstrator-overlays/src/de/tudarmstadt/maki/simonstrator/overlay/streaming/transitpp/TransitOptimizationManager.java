/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp;

import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.optimization.HighDegreePreemptionOptimization;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.optimization.LowDelayJumpOptimization;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.optimization.TopologyOptimization;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitFlowSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.simple.TransitSimpleFlowManager;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * Manages the optimizations and decides which potential optimization to
 * execute, based on their respective gain.
 * 
 * @author Eduardo Lidanski
 * @version 1.0, 14.04.2014
 */
public class TransitOptimizationManager {

	public static final boolean DEBUG = false;

	private TransitNode node;

	private long lastTry = 0;

	private long wait = 0;

	/* FlowId->Connection */
	private Map<Integer, TransitConnection> oldParents;

	private Map<Integer, TransitConnection> newParents;

	private Map<Integer, TransitConnection> awaitingReply;

	private Set<TopologyOptimization> optimizations = new HashSet<TopologyOptimization>();

	private Set<TransitConnection> reservedConnections = new HashSet<TransitConnection>();

	private TransitSimpleFlowManager flowManager;

	private TimeoutSet<TopologyOptimization> actualOptimization;

	public TransitOptimizationManager(TransitNode transitNode) {
		this.node = transitNode;
		oldParents = new HashMap<Integer, TransitConnection>();
		newParents = new HashMap<Integer, TransitConnection>();
		awaitingReply = new HashMap<Integer, TransitConnection>();
		actualOptimization = new TimeoutSet<TopologyOptimization>(Time.MINUTE);

		/*
		 * Initially: put the optimization we have. But this can come from
		 * tracker.
		 */
		optimizations.add(new LowDelayJumpOptimization(node));
		optimizations.add(new HighDegreePreemptionOptimization(node));
	}

	public void setActualOptimization(TopologyOptimization optimization) {
		if (optimization == null) {
			actualOptimization.removeOldest();
			resetDoubleParents();
			return;
		}

		this.actualOptimization.addNow(optimization);
	}

	public boolean isOptimizing() {
		return !actualOptimization.isEmpty();
	}

	/**
	 * Is the given connection reserved by an optimization?
	 * 
	 * @param connectionToSwitch
	 * @return
	 */
	public boolean isReserved(TransitConnection connection) {
		return reservedConnections.contains(connection);
	}

	public void reserve(TransitConnection connection) {
		reservedConnections.add(connection);
	}

	public void tick() {

		if (node.getSettings().getParam(TransitParams.DO_OPTIMIZE) == 0) {
			return;
		}

		if (lastTry + wait > Time.getCurrentTime()) {
			return;
		}

		this.flowManager = (TransitSimpleFlowManager) node.getScheduler()
				.getFlowManager();

		wait = node.getSettings().getTime(TransitTimes.OPTIMIZATION_INTERVAL);
		lastTry = Time.getCurrentTime();

		if (node.isServer()) {
			return;
		}

		if (!node.isPresent()) {
			return;
		}

		/*
		 * TODO[JR]: This way, the first registered optimization is always
		 * preferred to the other ones. Should we maybe add some more randomness
		 * here or allow to compare two optimization types with each other?
		 */
		for (TopologyOptimization optimization : optimizations) {
			boolean canOptimize = optimization.canOptimize();
			if (canOptimize && !isOptimizing()) {
				actualOptimization.addNow(optimization);
				wait = optimization.doOptimize();
				return;
			}
		}

	}

	public void setDoubleParents(TransitConnection oldParent,
			TransitConnection newParent, int doubleParentsLayer) {
		oldParents.put(doubleParentsLayer, oldParent);
		newParents.put(doubleParentsLayer, newParent);
		awaitingReply.put(doubleParentsLayer, newParent);
	}

	/*
	 * TODO: this can come from the tracker or somewhere. It actually comes from
	 * the REQUEST_FROM_ME_COMMAND, since only it uses the HighDegreePreemption.
	 * This can be put in another (better) place.
	 */
	public void setActualOptimizationHighDegreePreemption() {
		for (TopologyOptimization optimization : optimizations) {
			if (optimization instanceof HighDegreePreemptionOptimization) {
				setActualOptimization(optimization);
			}
		}
	}

	public TransitConnection getOldParent(int layer) {
		return oldParents.get(layer);
	}

	public TransitConnection getNewParent(int layer) {
		return newParents.get(layer);
	}

	public void resetDoubleParents() {
		for (int i = 0; i <= 3; i++) {
			oldParents.put(i, null);
			newParents.put(i, null);
			awaitingReply.put(i, null);
		}
	}

	public boolean acceptsDoubleParents(int layer) {
		return oldParents.get(layer) != null && newParents.get(layer) != null;
	}

	public int getLayer(TransitConnection conn) {
		int isFromLayer = -1;
		Iterator<Integer> it = awaitingReply.keySet().iterator();
		while (it.hasNext()) {
			Integer layer = it.next();
			if (conn == awaitingReply.get(layer)) {
				isFromLayer = layer;
			}
		}
		return isFromLayer;
	}

	public void onFlowReply(TransitConnection conn) {
		int isFromLayer = getLayer(conn);

		if (isFromLayer == -1) {
			return;
		}

		if (conn == newParents.get(isFromLayer)) {

			if (actualOptimization.size() == 0) {
				resetDoubleParents();
				return;
			}

			if (DEBUG) {
				System.out
.println("[" + node.getHost().getId()
								+ "] "
						+ Time.getFormattedTime(Time
										.getCurrentTime()) + " "
 + node.getHost().getId() + ": Parent "
						+ conn.getEndpoint().getNodeID()
								+ " accepted (" + isFromLayer + ")");
			}

			/* Cancel the old parent. */
			/*
			 * If we receive other flows from this parent: alter schedule. If
			 * not: cancel schedule.
			 */
			if (oldParents.get(isFromLayer) == null) {
				return;
			}

			BitSet oldParentSchedule = oldParents.get(isFromLayer)
					.getSchedule().getBlockMask();
			int counter = 0;
			BitSet alteredSchedule = new BitSet();
			for (int i = oldParentSchedule.nextSetBit(0); i >= 0; i = oldParentSchedule
					.nextSetBit(i + 1)) {
				if (i != isFromLayer) {
					alteredSchedule.set(i);
				}
				counter++;
			}
			if (counter > 1) {
				flowManager.doAlterSchedule(oldParents.get(isFromLayer),
						new TransitFlowSchedule(alteredSchedule));
			} else {
				flowManager.doCancel(oldParents.get(isFromLayer));
			}

			wait = actualOptimization.getUnmodifiableSet().toArray(
					new TopologyOptimization[] {})[0].onFlowReply(conn);
		}
	}

	public void onFlowDeny(TransitConnection conn) {
		int isFromLayer = getLayer(conn);

		if (isFromLayer == -1) {
			return;
		}
		if (conn == newParents.get(isFromLayer)) {
			if (DEBUG) {
				System.out
.println("[" + node.getHost().getId()
								+ "] "
								+ Time.getFormattedTime(Time.getCurrentTime()) 
								+ " " 
								+ node.getHost().getId() + ": Parent "
								+ conn.getEndpoint().getNodeID()
								+ " did NOT accept (" + isFromLayer + ")");
			}

			if (actualOptimization.size() == 0) {
				resetDoubleParents();
				return;
			}

			wait = actualOptimization.getUnmodifiableSet().toArray(
					new TopologyOptimization[] {})[0].onFlowDeny(conn);
		}
	}
}
