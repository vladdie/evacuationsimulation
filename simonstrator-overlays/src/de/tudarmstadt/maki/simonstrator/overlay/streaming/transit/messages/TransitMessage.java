/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;

/**
 * Basic Interface for all messages in the Transit Overlay to allow unified
 * message handling, regardless of send/sendAndWait.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public interface TransitMessage extends Message {

	/**
	 * Used in the {@link TransitMessageHandler} to mark messages that are to be
	 * answered via sendReply.
	 */
	public void setWantsReply();

	/**
	 * Reply to this message via sendReply(), used in the
	 * {@link TransitMessageHandler} to correctly dispatch a reply to this
	 * message.
	 * 
	 * @return
	 */
	public boolean wantsReply();

	/**
	 * The {@link TransitContact} of the sender of this message.
	 * 
	 * @return
	 */
	public TransitContact getSenderContact();

	/**
	 * True, if this message carries additional, piggybacked information
	 * 
	 * @return
	 */
	public boolean hasPiggybackedMessages();

	/**
	 * List of piggybacked messages
	 * 
	 * @return
	 */
	public List<TransitMessage> getPiggybackedMessages();

	/**
	 * Piggyback a message on top of this one, may be used to add buffer maps or
	 * other information on top of any block transmission or maintenance message
	 * 
	 * @param message
	 */
	public void piggyback(TransitMessage message);

}
