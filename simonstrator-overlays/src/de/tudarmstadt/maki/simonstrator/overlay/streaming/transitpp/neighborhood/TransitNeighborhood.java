/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitOfferNeighborhoodMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.operations.JoinTrackerOperation;

/**
 * This interface exposes the functionality of the base-layer (in terms of
 * signaling) in transit. It maintains a robust signaling mesh based on the
 * feedback of the {@link TransitMessageHandler}. This signaling mesh is formed
 * out of {@link TransitConnection}s between nodes. These connections can then
 * be used by the scheduler to assign schedules.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 08.06.2012
 */
public interface TransitNeighborhood extends IPeerStatusListener {

	/**
	 * The {@link TransitConnectionManager} is used to query for a set of
	 * connections that match given criteria.
	 * 
	 * @return
	 */
	public TransitConnectionManager getConnectionManager();

	/**
	 * This is triggered whenever a {@link TransitOfferNeighborhoodMessage}
	 * arrives and as soon as the tracker sends us the initial neighborhood for
	 * the signaling mesh.
	 * 
	 * @param from
	 * @param neighborhood
	 * @param replyTo
	 *            Handler to use when sending a message in reply to this event.
	 *            Might be null, in such a case you should not send a reply at
	 *            all.
	 */
	public void receivedNeighborhood(TransitContact from,
			List<TransitContact> neighborhood, TransitReplyHandler replyTo);

	/**
	 * Called by the {@link TransitConnectionManager} to request new contacts.
	 * This is an asynchronous request, that might ultimately lead to a call to
	 * onNewNeighbors on the ConnectionManager.
	 */
	public void requestNeighbors();

	/**
	 * Initiates the connection to a tracker.
	 * 
	 * @param callback
	 * @return operationID of the {@link JoinTrackerOperation}
	 */
	public int connectToTracker(OperationCallback<Object> callback);

	/**
	 * This method is called once for <b>every</b> message that arrives. It is
	 * not used to handle a message, instead it should be used to update routing
	 * tables and contact timestamps in a central fashion.
	 * 
	 * @param from
	 * @param rtt
	 *            (only available in Request-Reply scenarios, 0 otherwise)
	 * @param messageSize
	 *            to count traffic over a {@link TransitConnection}
	 */
	public void messageArrived(TransitContact from, long rtt,
			long messageSize);

	/**
	 * Called as soon as a timeout occurred when trying to reach the given
	 * contact.
	 * 
	 * @param contact
	 */
	public void timeout(TransitContact contact);

	/**
	 * The contact of the tracker this node is associated with.
	 * 
	 * @return
	 */
	public TransitContact getTracker();

	boolean isInBatchJoin();

	void setInBatchJoin(boolean inBatchJoin);

}
