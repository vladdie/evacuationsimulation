/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive;

import java.util.Comparator;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnectionListener;

public abstract class AbstractTransitIncentiveManager implements TransitIncentiveManager, TransitConnectionListener {

	protected final static boolean DEBUG = true;

	protected final TransitNode transitNode;

	protected boolean requestPullTokens;

	public AbstractTransitIncentiveManager(TransitNode transitNode) {
		this.transitNode = transitNode;
		this.transitNode.getNeighborhood().getConnectionManager().addConnectionListener(this);
	}

	/**
	 * Gets the node.
	 * 
	 * @return the node
	 */
	public TransitNode getNode() {
		return transitNode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitConnectionListener
	 * #onConnectionStateChanged(de.tudarmstadt.maki.simonstrator
	 * .overlay.streaming.transit.neighborhood.TransitConnection,
	 * de.tudarmstadt.
	 * maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection
	 * .ConnectionState,
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit
	 * .neighborhood.TransitConnection.ConnectionState)
	 */
	@Override
	public void onConnectionStateChanged(TransitConnection connection, ConnectionState oldState,
	        ConnectionState newState) {
		assert connection.getEndpoint() != transitNode.getLocalOverlayContact();
	}

	/**
	 * Checks if is new connection.
	 * 
	 * @param connection
	 *            the connection
	 * @return true, if is new connection
	 */
	protected boolean isActiveConnection(TransitConnection connection) {

		if (connection == null) {
			return false;
		}

		if (connection.getConnectionStatistics().getBlocksTransfered() > 0) {
			return true;
		}
		return false;

	}

	/**
	 * Checks if is new connection.
	 * 
	 * @param connection
	 *            the connection
	 * @return true, if is new connection
	 */
	protected TransitConnection getOppositeDirectionConnection(TransitConnection connection) {

		assert connection.getDirection().equals(ConnectionDirection.OUT);

		// Get all connections we are receiving data from.
		List<TransitConnection> inConnections = getNode().getNeighborhood().getConnectionManager()
		        .getConnections(ConnectionDirection.IN, ConnectionState.OPEN, null, null);

		/* No connections at all? -> new! */
		if (inConnections.size() == 0) {
			return null;
		}

		/*
		 * Iterate through all connections and check if anything was ever
		 * transferred.
		 */
		for (TransitConnection transitConnection : inConnections) {
			if (transitConnection.getEndpoint().equals(connection.getEndpoint())) {
				return transitConnection;
			}
		}

		return null;

	}

	/** The Constant COMP_LOAD. */
	protected static final Comparator<TransitConnection> COMP_LOAD_ASC = new Comparator<TransitConnection>() {

		/**
		 * Compare two Connections by Workload.
		 * 
		 * @param o1
		 *            the o1
		 * @param o2
		 *            the o2
		 * @return the int
		 */
		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			return Short.valueOf(o1.getEndpoint().getLoad()).compareTo(Short.valueOf(o2.getEndpoint().getLoad()));
		}
	};
}
