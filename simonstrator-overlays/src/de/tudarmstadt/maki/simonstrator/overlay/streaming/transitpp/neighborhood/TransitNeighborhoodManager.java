/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.operation.Operation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.operations.JoinTrackerOperation;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * Management of the neighborhood, i.e. the signaling layer in transit.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 08.06.2012
 */
public class TransitNeighborhoodManager implements TransitNeighborhood {

	private final TransitConnectionManager connectionManager;

	private final TransitNode node;

	private TransitContact tracker;
	
	private boolean inBatchJoin = false;

	private final TimeoutSet<TransitContact> notReachable = new TimeoutSet<TransitContact>(
			Time.MINUTE);

	private final TimeoutSet<TransitContact> recentContacts = new TimeoutSet<TransitContact>(
			Time.MINUTE);


	public TransitNeighborhoodManager(TransitNode node) {
		this.connectionManager = new TransitConnectionManager(node);
		this.node = node;
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		switch (peerStatus) {
		case PRESENT:
		case TO_JOIN:
			break;

		case ABSENT:
			break;

		default:
			throw new AssertionError("Unknown PeerStatus.");
		}
	}

	@Override
	public void receivedNeighborhood(TransitContact from,
			List<TransitContact> neighborhood, TransitReplyHandler replyTo) {

		if (node.isServer()) {
			return;
		}

		List<TransitContact> contacts = new LinkedList<TransitContact>();
		for (TransitContact transitContact : neighborhood) {
			if (transitContact.equals(node.getLocalOverlayContact())) {
				continue;
			}
			if (notReachable.contains(transitContact)) {
				continue;
			}
			if (connectionManager.getConnection(transitContact) != null) {
				continue;
			}
			contacts.add(transitContact);
			recentContacts.addNow(transitContact);
		}
		if (!contacts.isEmpty()) {
			connectionManager.onNewNeighbors(contacts);
		}
	}

	@Override
	public void requestNeighbors() {
		/*
		 * We do not support active requests, but we call onNewNeighbors with
		 * some of the previously unused contacts.
		 */
		List<TransitContact> contacts = new LinkedList<TransitContact>();
		for (TransitContact contact : recentContacts.getUnmodifiableSet()) {
			assert !contact.equals(node.getLocalOverlayContact());
			if (notReachable.contains(contact)) {
				continue;
			}
			if (connectionManager.getConnection(contact) != null) {
				continue;
			}
			contacts.add(contact);
		}
		if (!contacts.isEmpty()) {
			connectionManager.onNewNeighbors(contacts);
		}
	}

	/**
	 * For callback-access
	 * 
	 * @return
	 */
	protected TransitNode getNode() {
		return node;
	}

	@Override
	public void messageArrived(TransitContact from, long rtt, long messageSize) {
		connectionManager.updateRttEstimation(from, rtt);
	}

	@Override
	public void timeout(TransitContact contact) {
		notReachable.addNow(contact);
		recentContacts.remove(contact);
		connectionManager.timeout(contact);
	}

	@Override
	public TransitContact getTracker() {
		return tracker;
	}

	/**
	 * Set the tracker, this is done as a result of the
	 * {@link JoinTrackerOperation} inside the {@link JoinTrackerCallback}.
	 * 
	 * @param tracker
	 */
	protected void setTracker(TransitContact tracker) {
		assert tracker.getType() == ContactType.TRACKER;
		this.tracker = tracker;
	}

	@Override
	public TransitConnectionManager getConnectionManager() {
		return connectionManager;
	}

	@Override
	public int connectToTracker(OperationCallback<Object> callback) {
		JoinTrackerOperation op = new JoinTrackerOperation(node,
				new JoinTrackerCallback(callback));
		op.scheduleImmediately();
		return op.getOperationID();
	}
	
	@Override 
	public boolean isInBatchJoin() {
		return inBatchJoin;
	}
	
	@Override
	public void setInBatchJoin(boolean inBatchJoin) {
		this.inBatchJoin = inBatchJoin;
	}

	/**
	 * This callback is passed to the {@link JoinTrackerOperation} and might be
	 * extended lateron to support retries. For now, we just inform the
	 * application.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 29.05.2012
	 */
	public class JoinTrackerCallback implements
			OperationCallback<TransitContact> {

		private OperationCallback<Object> appCallback;

		public JoinTrackerCallback(OperationCallback<Object> appCallback) {
			this.appCallback = appCallback;
		}

		@Override
		public void calledOperationFailed(Operation<TransitContact> op) {
			Operations.createEmptyFailingOperation(getNode(), appCallback)
					.scheduleImmediately();
		}

		@Override
		public void calledOperationSucceeded(Operation<TransitContact> op) {
			JoinTrackerOperation joinOp = (JoinTrackerOperation) op;
			getNode().setPeerStatus(PeerStatus.PRESENT);
			setTracker(op.getResult());
			receivedNeighborhood(op.getResult(), joinOp.getInitialContacts(),
					null);
			Operations.createEmptyOperation(getNode(), appCallback)
					.scheduleImmediately();
		}
	}
}
