/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.tracker;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.TCPMessageBased;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingDocument;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingOverlayTrackerInterface;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitFactory;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.batchJoin.BatchJoinHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.HeadCapacityMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.JoinTrackerMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.JoinTrackerReplyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.OfferNeighborhoodMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.RequestNeighborhoodMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * This is a Tracker in the Transit Overlay. In its simplest form this is a
 * central server, but one might think about extending it towards a
 * DHT-approach. A peer might act as a tracker as well as a normal
 * {@link TransitNode} at the same time.
 * 
 * TODO for simplicity it is assumed that the tracker always knows the current
 * playback position of the source node. Lateron, the source will send updates
 * to the tracker announcing new chunks. These updates will then be used for the
 * latest chunk.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public class TransitTracker extends AbstractOverlayNode implements
		TransMessageListener, StreamingOverlayTrackerInterface {

	private TransitContact ownContact;

	private MessageBasedTransport transport;

	/**
	 * Global-Knowledge Source, remove this lateron as described above
	 */
	// private IPlayer sourcePlayer;

	/**
	 * Global knowledge source, remove this dependency lateron!
	 */
	private TransitNode sourceNode;

	private TransitSettings transitSettings;

	private BatchJoinHandler batchJoinHandler;

	private TimeoutSet<TransitContact> headsWithCapacity;

	private TransitTrackerRegistry trackerRegistry;

	private final int port;

	public TransitTracker(Host host, int port,
			TransitSettings transitSettings) {
		super(host);
		this.port = port;
		this.transitSettings = transitSettings;

		headsWithCapacity = new TimeoutSet<TransitContact>(10 * Time.SECOND);
	}

	public TransitSettings getSettings() {
		return transitSettings;
	}

	@Override
	public void initialize() {

		/*
		 * Bind the first Network Interface
		 */
		NetInterface net = getHost().getNetworkComponent()
				.getNetworkInterfaces().iterator().next();
		try {
			transport = getHost().getTransportComponent().getProtocol(
					TCPMessageBased.class, net.getLocalInetAddress(), port);
		} catch (ProtocolNotAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ownContact = new TransitContact(
				// Using the right tracker id (just to be sure!)
				INodeID.get(TransitFactory.DEFAULT_TRACKER_ID), 				// getHost().getId(),
				transport.getTransInfo(),
				ContactType.TRACKER);

		ownContact.updateLayer(new byte[] { 0, 0, 0 });
		transport.setTransportMessageListener(this);

		batchJoinHandler = new BatchJoinHandler(this);

		trackerRegistry = new TransitTrackerRegistry();

		/*
		 * TODO go online in churn or action!
		 */
		setPeerStatus(PeerStatus.PRESENT);

	}

	@Override
	public TransitContact getLocalOverlayContact() {
		return ownContact;
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		// TODO Auto-generated method stub

	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageArrived(Message omsg, TransInfo sender, int commID) {
		TransitMessage msg = (TransitMessage) omsg;
		TransitMessage reply = null;
		if (msg instanceof JoinTrackerMessage) {
			boolean inFlashCrowd = inFlashCrowd();
			reply = handleJoinTrackerMsg((JoinTrackerMessage) msg, inFlashCrowd);
		} else if (msg instanceof RequestNeighborhoodMessage) {
			reply = handleUpdateTrackerMsg((RequestNeighborhoodMessage) msg);
			/*
			 * TODO add a timeout-mechnism that removes contacts after a given
			 * time of inactivity
			 */
		} else if (msg instanceof HeadCapacityMessage) {
			handleHeadCapacityMessage((HeadCapacityMessage) msg);
		}

		else {
			throw new AssertionError("Unknown message type "
					+ msg.getClass().getSimpleName());
		}
		if (reply != null) {
			transport.sendReply(reply, sender.getNetId(), sender.getPort(),
					commID);
		}
	}

	private void handleHeadCapacityMessage(HeadCapacityMessage msg) {

		if (getSettings().getParam(TransitParams.HEADS_CAPACITY) != 0) {
			// System.out.println(Simulator.getFormattedTime(Simulator.getCurrentTime())
			// + ": Arrived head capacity message from " +
			// msg.getSenderContact() + " with capacity: " + msg.hasCapacity());

			TransitContact sender = msg.getSenderContact();
			boolean hasCapacity = msg.hasCapacity();
			if (hasCapacity) {
				headsWithCapacity.addNow(sender);
			} else {
				headsWithCapacity.remove(sender);
			}

		} else {
			return;
		}
	}

	/**
	 * Node sends updated information to the tracker
	 * 
	 * @param msg
	 * @return
	 */
	protected TransitMessage handleUpdateTrackerMsg(
			RequestNeighborhoodMessage msg) {
		/*
		 * TODO check for layer change, if so, definitely send some contacts!
		 */
		// this will trigger an update of the contact info
		trackerRegistry.addOrUpdateContact(msg.getSenderContact());
		/*
		 * TODO send neighborhood matching the layer?
		 */
		if (msg.getNumberOfRequestedContacts() > 0) {
			List<TransitContact> contacts = trackerRegistry
					.getAdditionalNeighborhood(msg.getSenderContact(),
							msg.getNumberOfRequestedContacts(),
							msg.getKnownContacts());
			OfferNeighborhoodMessage offerMsg = new OfferNeighborhoodMessage(
					getLocalOverlayContact(), contacts);
			return offerMsg;
		}
		return null;
	}

	/**
	 * Initial join of a node with the tracker
	 * 
	 * @param msg
	 * @return
	 */
	protected JoinTrackerReplyMessage handleJoinTrackerMsg(
			JoinTrackerMessage msg, boolean inFlashCrowd) {

		/*
		 * FIXME using global knowledge to determine the current playback
		 * position. This may be done as part of the source <-> tracker protocol
		 * lateron
		 */
		if (sourceNode == null) {
			/*
			 * 1. Try to find the source on the same host
			 */
			try {
				sourceNode = getHost().getComponent(TransitNode.class);
				if (!sourceNode.isServer()) {
					sourceNode = null;
				} else {
					trackerRegistry.addOrUpdateContact(sourceNode
							.getLocalOverlayContact());
				}
			} catch (ComponentNotAvailableException e1) {
				// no local source found
			}
			if (sourceNode == null) {
				/*
				 * 2. Global Knowledge
				 */
				for (Host host : Oracle.getAllHosts()) {
					try {
						sourceNode = host.getComponent(TransitNode.class);
						if (sourceNode.isServer()) {
							trackerRegistry.addOrUpdateContact(sourceNode
									.getLocalOverlayContact());
							break;
						}
					} catch (ComponentNotAvailableException e) {
						continue;
					}
				}
			}
		}

		if (sourceNode == null) {
			throw new AssertionError("You did not specify a source node!");
		}

		if (getSettings().getParam(TransitParams.BATCH) == 1
				|| getSettings().getParam(TransitParams.BATCH) == 2) {
			if (inFlashCrowd) {
				return batchJoinHandler.handleJoinTrackerMsg(msg);
			}
			batchJoinHandler.onFlashCrowdFinish();
		}

		/*
		 * This simple tracker just accepts everyone
		 */
		TransitContact senderContact = msg.getSenderContact();
		List<TransitContact> initialContacts = new LinkedList<TransitContact>();
		if (senderContact.getType() == ContactType.CLIENT) {
			/*
			 * TODO find a set of nodes for this client
			 */
			initialContacts.addAll(trackerRegistry
					.getInitialNeighborhood(senderContact));
		}

		if (getSettings().getParam(TransitParams.HEADS_CAPACITY) != 0) {
			if (getSettings().getParam(TransitParams.HEADS_CAPACITY) == 1) {
				// Adds the nodes we think they have capacity.
				ArrayList<TransitContact> toAdd = new ArrayList<TransitContact>();
				for (TransitContact tc : headsWithCapacity.getUnmodifiableSet()) {
					if (!initialContacts.contains(tc)) {
						toAdd.add(tc);
					}
				}
				initialContacts.addAll(0, toAdd);
			} else if (getSettings().getParam(TransitParams.HEADS_CAPACITY) == 2) {
				initialContacts = new ArrayList<TransitContact>(
						headsWithCapacity.getUnmodifiableSet());
			}
		}

		/*
		 * Add contact to tracker
		 */
		senderContact.setJoiningTime(Time.getCurrentTime());
		/* Contact cannot still send flow, since it just arrived. */
		trackerRegistry.addOrUpdateContact(senderContact);
		// System.out.println("Contact " + senderContact + " just joined");

		StreamingDocument newDoc = createStreamingDoc();

		byte[] moovAtom = sourceNode.getVideo().getMoovAtom();

		if (moovAtom != null) { // Required for simulation case
			newDoc.setMoovAtom(moovAtom);
		}

		TransitContact trackerContact = getLocalOverlayContact().clone();
		return new JoinTrackerReplyMessage(trackerContact, trackerContact,
				newDoc, initialContacts, senderContact.getJoiningTime(), false);
	}

	/**
	 * Returns true, if the tracker detected a flash crowd. For now, this is
	 * static.
	 * 
	 * @return
	 */
	public boolean inFlashCrowd() {
		return Time.getCurrentTime() >= transitSettings
				.getTime(TransitTimes.FLASH_CROWD_START)
				&& Time.getCurrentTime() <= transitSettings
						.getTime(TransitTimes.FLASH_CROWD_END);
	}

	public List<TransitContact> getInitialContacts(TransitContact contact) {
		return trackerRegistry.getInitialNeighborhood(contact);
	}

	public StreamingDocument createStreamingDoc() {
		/*
		 * TODO check, if we may need to use getBufferBegin - 1 instead!
		 */
		StreamingDocument newDoc = new StreamingDocument(sourceNode.getVideo(),
				sourceNode.getBuffer().getBufferBegin());
		return newDoc;
	}

	public MessageBasedTransport getTransport() {
		return transport;
	}

	public void addOrUpdateContact(TransitContact contact) {
		trackerRegistry.addOrUpdateContact(contact);
	}

}
