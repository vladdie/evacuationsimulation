/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.simple;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNodeInfo;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnectionListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.operations.CloseFlowOperation;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.AbstractTransitFlowManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitFlowSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitFlowScheduleListener;

/**
 * Mainly for testing purposes and to minimize cross-effects when evaluating new
 * components, this FlowManager is the most basic implementation.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 15.06.2012
 */
public class TransitSimpleFlowManager extends AbstractTransitFlowManager {

	private RepairPhase repairPhase;

	private int serverLastSentFlow = -1;

	/* See the FLOW_REQUEST_REPLACE_CHILD Request. */
	private TransitConnection temporalConnection;

	/**
	 * Very simple FlowManager
	 * 
	 * @param node
	 */
	public TransitSimpleFlowManager(TransitNode node) {
		super(node);
		repairPhase = new RepairPhase();
	}

	public TransitConnection getTemporalConnection() {
		return temporalConnection;
	}

	public void resetTemporalConnection() {
		temporalConnection = null;
	}

	@Override
	protected void handleMissingFlow(BitSet blockMask) {
		if (getNode().getSettings().getParam(TransitParams.FLOW_MAX_INCOMING) > getIncomingFlow()
				.getNumberOfFlows()) {
			repairPhase.handleMissingFlow(blockMask);
		}
	}

	@Override
	protected void processScheduleRequestReplace(TransitFlowSchedule request,
			TransitConnection connection, TransitReplyHandler replyTo,
			TransitContact childToReplace, long calculatedRTT) {
		/*
		 * Only grant request if the replacement is (still) our child. It may
		 * happen that the other node still thinks it is our child, but it is
		 * not!.
		 */
		TransitConnection connToChildToReplace = getNode().getNeighborhood()
				.getConnectionManager().getConnection(childToReplace);
		if (connToChildToReplace == null) {
			doDenyRequest(connection, replyTo);
			return;
		}
		if (connToChildToReplace.getState() != ConnectionState.OPEN) {
			doDenyRequest(connection, replyTo);
			return;
		}

		if (connToChildToReplace.getDirection() != ConnectionDirection.OUT) {
			doDenyRequest(connection, replyTo);
			return;
		}

		if (connToChildToReplace.getScheduleState() != ScheduleState.ACTIVE) {
			doDenyRequest(connection, replyTo);
			return;
		}

		if (getNode().getOptimizationManager().isOptimizing()) {
			doDenyRequest(connection, replyTo);
			return;
		}
		// ///////////

		/*
		 * Can we fulfill the schedule?
		 */
		BitSet blockMaskAvailable = (BitSet) request.getBlockMask().clone();
		BitSet ownMask = null;
		if (getNode().isServer()) {
			ownMask = new BitSet();

			if (getNode().getSettings().getParam(TransitParams.HEADS_AS_LEAVES) == 1) {

				/* Check if we are already sending it a flow. */
				if (getOutgoingFlow().hasOutgoingFlow(connection.getEndpoint())) {
					doDenyRequest(connection, replyTo);
					return;
				} else {
					while (ownMask.isEmpty()) {
						serverLastSentFlow = (serverLastSentFlow + 1) % 4;
						ownMask.set(serverLastSentFlow);
						ownMask.and(blockMaskAvailable);
					}
				}

			} else {
				ownMask.set(0, blockMaskAvailable.length());
			}

		} else {

			if (getNode().getSettings().getParam(TransitParams.HEADS_AS_LEAVES) == 1) {
				if (getIncomingFlow().isHead()) {
					ownMask = getIncomingFlow().getActiveFlow();
				} else {
					ownMask = getIncomingFlow().getHealthyFlow();
				}
			} else {
				ownMask = getIncomingFlow().getHealthyFlow();
			}

		}
		blockMaskAvailable.and(ownMask);
		if (!blockMaskAvailable.isEmpty()) {
			/*
			 * Do we have spare capacity?
			 */
			TransitFlowSchedule flowSchedule = new TransitFlowSchedule(
					blockMaskAvailable);
			if (getNode().getBandwidthManager().hasSpareBandwidthFor(
					flowSchedule, ConnectionDirection.OUT)) {
				doGrantRequest(flowSchedule, connection, replyTo);
				return;
			} else {

				//continue
				
			}
		} else {
			if (DEBUG) {
				System.out
						.println("TFM: Denied schedule, NO BLOCKS AVAILABLE. "
								+ connection.toString());
			}
			doDenyRequest(connection, replyTo);
			return;
		}

		// //////////

		/*
		 * Since we know that this request is just temporal: accept
		 * request even though we don't have capacity. We ensure that
		 * the other flow is being canceled after a short time and we
		 * ensure that this happens only once at a time.
		 */
		TransitFlowSchedule flowSchedule = new TransitFlowSchedule(
				blockMaskAvailable);
		if (temporalConnection == null) {
			temporalConnection = connToChildToReplace;
			doGrantRequest(flowSchedule, connection, replyTo);
		} else {
			doDenyRequest(connection, replyTo);
		}
		
		long cancelChildAfter = calculatedRTT
				+ (long) Math.ceil(((double) connToChildToReplace
						.getCurrentRTTEstimation()) / 2)
				+ (long) Math.ceil(((double) connection
						.getCurrentRTTEstimation()) / 2);

		CloseFlowOperation closeFlowOp = new CloseFlowOperation(getNode(),
				null, connToChildToReplace, request, this);
		closeFlowOp.scheduleWithDelay(cancelChildAfter);

	}

	@Override
	protected void processScheduleRequest(TransitFlowSchedule request,
			TransitConnection connection, TransitReplyHandler replyTo) {

		/*
		 * Is the connection OPEN and the direction OUT?
		 */
		if (connection.getState() != ConnectionState.OPEN
				|| connection.getDirection() != ConnectionDirection.OUT) {
			doDenyRequest(connection, replyTo);
			return;
		}

		/*
		 * Can we fulfill the schedule? At least partial?
		 */
		BitSet blockMaskAvailable = (BitSet) request.getBlockMask().clone();
		BitSet ownMask = null;
		if (getNode().isServer()) {
			ownMask = new BitSet();

			if (getNode().getSettings().getParam(TransitParams.HEADS_AS_LEAVES) == 1) {

				/* Check if we are already sending it a flow. */
				if (getOutgoingFlow().hasOutgoingFlow(connection.getEndpoint())) {
					doDenyRequest(connection, replyTo);
				} else {
					while (ownMask.isEmpty()) {
						serverLastSentFlow = (serverLastSentFlow + 1) % 4;
						ownMask.set(serverLastSentFlow);
						ownMask.and(blockMaskAvailable);
					}
				}

			} else {
				ownMask.set(0, blockMaskAvailable.length());
			}

		} else {

			if (getNode().getSettings().getParam(TransitParams.HEADS_AS_LEAVES) == 1) {
				if (getIncomingFlow().isHead()) {
					ownMask = getIncomingFlow().getActiveFlow();
				} else {
					ownMask = getIncomingFlow().getHealthyFlow();
				}
			} else {
				ownMask = getIncomingFlow().getHealthyFlow();
			}

		}
		blockMaskAvailable.and(ownMask);
		
		
		if (!blockMaskAvailable.isEmpty()) {
			/*
			 * Do we have spare capacity?
			 */
			TransitFlowSchedule flowSchedule = new TransitFlowSchedule(
					blockMaskAvailable);
			if (getNode().getBandwidthManager().hasSpareBandwidthFor(
					flowSchedule, ConnectionDirection.OUT)) {
				doGrantRequest(flowSchedule, connection, replyTo);
			} else {
				
				/*
				 * compute the blocks we are able to send with our spare
				 * capacity and send a REPLY with the altered Schedule.
				 */
				BitSet blockMaskPossible = new BitSet();
				for (int i = blockMaskAvailable.nextSetBit(0); i >= 0; i = blockMaskAvailable
						.nextSetBit(i + 1)) {

					blockMaskPossible.set(i);
					TransitFlowSchedule scheduleOffer = new TransitFlowSchedule(
							blockMaskPossible);
					if (!getNode().getBandwidthManager().hasSpareBandwidthFor(
							scheduleOffer, ConnectionDirection.OUT)) {
						blockMaskPossible.set(i, false);
					}
				}
				if (blockMaskPossible.isEmpty()) {
					doDenyRequest(connection, replyTo);
					if (DEBUG) {
						System.out
								.println("TFM: Denied schedule, NO SPARE CAPACITY. "
										+ connection.toString());
					}
				} else {
					TransitFlowSchedule possible = new TransitFlowSchedule(
							blockMaskPossible);
					doGrantRequest(possible, connection, replyTo);
					if (DEBUG) {
						System.out
								.println("TFM: Altered schedule, NO SPARE CAPACITY. "
										+ connection.toString());
					}
				}
			}
		} else {
			if (DEBUG) {
				System.out
						.println("TFM: Denied schedule, NO BLOCKS AVAILABLE. "
								+ connection.toString());
			}
			doDenyRequest(connection, replyTo);
		}
	}

	@Override
	protected void processRequestFromMeCommand(TransitFlowSchedule request,
			TransitConnection connection, TransitReplyHandler replyTo) {

		if (getNode().getOptimizationManager().isOptimizing()) {
			return;
		}
		
		int layer = -1;
		for (int i= 0; i<= 3; i++) {
			if (request.getBlockMask().get(i)) {
				layer = i;
				break;
			}
		}
		
		TransitConnection oldParent = getIncomingFlow().getConnectionForFlow(layer);
		
		getNode().getOptimizationManager().setDoubleParents(oldParent, connection, layer);
		getNode().getOptimizationManager().setActualOptimizationHighDegreePreemption();
		
		connection.setDirection(ConnectionDirection.IN);

		doRequest(request, connection);
	}

	@Override
	protected void processScheduleOffer(TransitFlowSchedule offer,
			TransitConnection connection) {
		/*
		 * TODO use OFFERs? Add a Piggyback-Listener for this purpose.
		 */
	}

	/**
	 * This phase is used to negotiate new schedules if some blocks are missing.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 01.07.2012
	 */
	protected class RepairPhase implements TransitFlowScheduleListener,
			TransitConnectionListener {

		private BitSet missing = new BitSet();

		private long lastTry = 0;

		private long wait = 0;

		public RepairPhase() {
			getNode().getNeighborhood().getConnectionManager()
					.addConnectionListener(this);
		}

		/**
		 * The original algorithm to choose peers to request from. It was
		 * "blind" because it does not take the offers into account.
		 * 
		 * @param missing
		 * @param chosenConnections
		 * @return
		 */
		private long getChosenConnectionsBlind(BitSet missing,
				Map<TransitConnection, BitSet> chosenConnections,
				Set<UniqueID> exclude) {
			List<TransitConnection> candidates = getNode()
					.getNeighborhood()
					.getConnectionManager()
					.getConnections(ConnectionDirection.IN,
							ConnectionState.OPEN, ScheduleState.INACTIVE, null);
			TransitConnection chosen = null;

			if (candidates.isEmpty()) {
				candidates = getNode()
						.getNeighborhood()
						.getConnectionManager()
						.getConnections(ConnectionDirection.IN,
								ConnectionState.OPEN, ScheduleState.CANCELED,
								null);

				long oldest = Time.getCurrentTime();
				if (getNode().getSettings().getParam(
						TransitParams.PRIORITIZE_REQUESTS) == 1) {
					candidates = getSortedConnections(candidates,
							new ComparatorByJoin(false), null,
							Integer.MAX_VALUE);
				}
				for (TransitConnection candidate : candidates) {
					if (candidate.getCurrentRTTEstimation() < getNode()
							.getSettings().getTime(TransitTimes.FLOW_RTT_MAX)
							&& candidate.getLastScheduleStateChangeTimestamp() < oldest
							&& !exclude.contains(candidate.getEndpoint()
									.getNodeID())) {
						oldest = candidate
								.getLastScheduleStateChangeTimestamp();
						chosen = candidate;
					}
				}
				if (chosen != null) {
					chosen.setScheduleState(ScheduleState.INACTIVE);
				}
			} else {
				if (getNode().getSettings().getParam(
						TransitParams.PRIORITIZE_REQUESTS) == 1) {
					candidates = getSortedConnections(candidates,
							new ComparatorByJoin(false), null,
							Integer.MAX_VALUE);
				}

				for (TransitConnection transitConnection : candidates) {
					if (transitConnection.getCurrentRTTEstimation() > getNode()
							.getSettings().getTime(TransitTimes.FLOW_RTT_MAX)
							|| exclude.contains(transitConnection.getEndpoint()
									.getNodeID())) {
						continue;
					}
					chosen = transitConnection;
					break;
				}
			}

			if (chosen == null) {
				if (DEBUG) {
					System.err.println("No Connection in the Repair-Phase! "
							+ getNode().getNeighborhood()
									.getConnectionManager().getAllConnections()
									.toString());
				}
				return 0;
			}

			chosenConnections.put(chosen, missing);
			return chosen.getCurrentRTTEstimation();
		}

		public long getChosenConnections(BitSet missing,
				Map<TransitConnection, BitSet> chosenConnections) {

			if (getNode().getSettings().getParam(
					TransitParams.REQUEST_IF_HAS_OFFER) == 1) {
				return getChosenConnectionsBasedOnOffers(missing,
						chosenConnections);
			} else {
				return getChosenConnectionsBlind(missing, chosenConnections,
						new HashSet<UniqueID>());
			}

		}

		/**
		 * Can the list of connections supply the needed schedules (based on
		 * their offers)?
		 * 
		 * @param missing
		 * @param conns
		 * @return
		 */
		private boolean canSupply(BitSet missing, List<TransitConnection> conns) {
			BitSet canSupply = new BitSet();

			for (TransitConnection connection : conns) {
				if (connection.getCurrentRTTEstimation() > getNode()
						.getSettings().getTime(TransitTimes.FLOW_RTT_MAX)) {
					continue;
				}
				TransitNodeInfo nodeInfo = connection.getNodeInfo();
				if (nodeInfo == null) {
					continue;
				}
				BitSet bs = nodeInfo.getCapacities();
				BitSet match = new BitSet();
				match.or(bs);
				match.and(missing);
				canSupply.or(match);
			}

			return canSupply.equals(missing);

		}

		private long getChosenConnectionsBasedOnOffers(BitSet missing,
				Map<TransitConnection, BitSet> chosenConnections) {
			List<TransitConnection> primaryCandidates = getNode()
					.getNeighborhood()
					.getConnectionManager()
					.getConnections(ConnectionDirection.IN,
							ConnectionState.OPEN, ScheduleState.INACTIVE, null);
			List<TransitConnection> secondaryCandidates = null;

			boolean canSupply = canSupply(missing, primaryCandidates);

			if (!canSupply) {
				secondaryCandidates = getNode()
						.getNeighborhood()
						.getConnectionManager()
						.getConnections(ConnectionDirection.IN,
								ConnectionState.OPEN, ScheduleState.CANCELED,
								null);
			}

			return chooseCandidates(missing, primaryCandidates,
					secondaryCandidates, chosenConnections);

		}

		/**
		 * Chooses some candidates from the given lists, which have the required
		 * offers. Returns the number of ms to wait (the longest waiting time of
		 * the result).
		 * 
		 * @param missing
		 * @param primaryCandidates
		 * @param secondaryCandidates
		 * @param chosenCandidates
		 * @return
		 */
		private long chooseCandidates(BitSet missing,
				List<TransitConnection> primaryCandidates,
				List<TransitConnection> secondaryCandidates,
				Map<TransitConnection, BitSet> chosenCandidates) {

			long waitingTime = 0;
			BitSet stillMissing = new BitSet();
			stillMissing.or(missing);

			for (TransitConnection transitConnection : primaryCandidates) {
				if (transitConnection.getCurrentRTTEstimation() > getNode()
						.getSettings().getTime(TransitTimes.FLOW_RTT_MAX)) {
					continue;
				}

				TransitNodeInfo candidateNodeInfo = transitConnection
						.getNodeInfo();
				if (candidateNodeInfo != null) {
					BitSet candidateCanSupply = candidateNodeInfo
							.getCapacities();
					BitSet requestFromCandidate = new BitSet();
					for (int i = candidateCanSupply.nextSetBit(0); i >= 0; i = candidateCanSupply
							.nextSetBit(i + 1)) {
						if (stillMissing.get(i) == true) {
							requestFromCandidate.set(i);
							long rtt = transitConnection
									.getCurrentRTTEstimation();
							if (rtt > waitingTime) {
								waitingTime = rtt;
							}
							stillMissing.set(i, false);
						}
					}
					if (!requestFromCandidate.isEmpty()) {
						chosenCandidates.put(transitConnection,
								requestFromCandidate);
					}
				}
			}

			if (secondaryCandidates != null) {
				for (TransitConnection transitConnection : secondaryCandidates) {
					if (transitConnection.getCurrentRTTEstimation() > getNode()
							.getSettings().getTime(TransitTimes.FLOW_RTT_MAX)) {
						continue;
					}

					TransitNodeInfo candidateNodeInfo = transitConnection
							.getNodeInfo();
					if (candidateNodeInfo != null) {
						BitSet candidateCanSupply = candidateNodeInfo
								.getCapacities();
						BitSet requestFromCandidate = new BitSet();
						for (int i = candidateCanSupply.nextSetBit(0); i >= 0; i = candidateCanSupply
								.nextSetBit(i + 1)) {
							if (stillMissing.get(i) == true) {
								requestFromCandidate.set(i);
								long rtt = transitConnection
										.getCurrentRTTEstimation();
								if (rtt > waitingTime) {
									waitingTime = rtt;
								}
								stillMissing.set(i, false);
							}
						}
						transitConnection
								.setScheduleState(ScheduleState.INACTIVE);
						if (!requestFromCandidate.isEmpty()) {
							chosenCandidates.put(transitConnection,
									requestFromCandidate);
						}
					}
				}
			}

			/*
			 * Still cannot completely supply what we need based on offers? we
			 * use one of the connections for rest ignoring its offers.
			 */
			if (stillMissing.nextSetBit(0) != -1) {
				/*
				 * We only do this if at least one of the connections has
				 * offers. If not, we just joined and we should wait a tick().
				 */
				boolean hasOffers = false;
				List<TransitConnection> all = new ArrayList<TransitConnection>();
				all.addAll(primaryCandidates);
				all.addAll(secondaryCandidates);
				for (TransitConnection conn : all) {
					if (conn.getNodeInfo() != null) {
						hasOffers = true;
						break;
					}
				}
				if (hasOffers) {
					Set<UniqueID> exclude = new HashSet<UniqueID>();
					Iterator<TransitConnection> it = chosenCandidates.keySet()
							.iterator();
					while (it.hasNext()) {
						TransitConnection conn = it.next();
						UniqueID id = conn.getEndpoint().getNodeID();
						exclude.add(id);
					}
					if (DEBUG) {
						System.out
								.println(Time.getFormattedTime(Time
								.getCurrentTime())
								+ " Actual offers cannot completely supply, trying with random for rest of needed flow. Originally Missing: "
								+ missing
								+ ", still missing: "
								+ stillMissing);
					}
					long time = getChosenConnectionsBlind(stillMissing,
							chosenCandidates, exclude);
					if (time > waitingTime) {
						waitingTime = time;
					}
				}

			}

			return waitingTime;
		}

		public void handleMissingFlow(BitSet missingBlocks) {

			missing.clear();
			for (int i = missingBlocks.nextSetBit(0); i >= 0; i = missingBlocks
					.nextSetBit(i + 1)) {
				missing.set(i);
				if (missing.cardinality() >= getNode().getSettings().getParam(
						TransitParams.FLOW_MAX_PER_CONNECTION)) {
					break;
				}
			}

			if (lastTry + wait > Time.getCurrentTime()) {
				return;
			}

			/*
			 * Are we already waiting for a flow we requested? This is specially
			 * needed for the REQUEST_FROM_ME_COMMAND
			 */
			List<TransitConnection> waiting = getNode()
					.getNeighborhood()
					.getConnectionManager()
					.getConnections(ConnectionDirection.IN,
							ConnectionState.OPEN, ScheduleState.NEGOTIATING,
							null);

			Map<TransitConnection, BitSet> chosenConnections = new HashMap<TransitConnection, BitSet>();

			wait = getChosenConnections(missing, chosenConnections);

			Iterator<TransitConnection> it = chosenConnections.keySet()
					.iterator();
			while (it.hasNext()) {
				TransitConnection chosen = it.next();
				BitSet requestFromCandidate = chosenConnections.get(chosen);

				TransitFlowSchedule request = new TransitFlowSchedule(
						requestFromCandidate);
				chosen.addScheduleListener(this);
				doRequest(request, chosen);
			}
			lastTry = Time.getCurrentTime();

		}

		@Override
		public void onConnectionStateChanged(TransitConnection connection,
				ConnectionState oldState, ConnectionState newState) {
			// if (connection.getDirection() == ConnectionDirection.IN) {
			// if (newState == ConnectionState.OPEN
			// && connection.getScheduleState() == ScheduleState.INACTIVE) {
			// if (lastTry + wait > Simulator.getCurrentTime()) {
			// return;
			// }
			// if (!missing.isEmpty()) {
			// if (connection.getCurrentRTTEstimation() < getNode()
			// .getSettings().getTime(
			// TransitTimes.FLOW_RTT_MAX)) {
			// TransitFlowSchedule request = new TransitFlowSchedule(
			// missing);
			// connection.addScheduleListener(this);
			// lastTry = Simulator.getCurrentTime();
			// wait = connection.getCurrentRTTEstimation();
			// doRequest(request, connection);
			// }
			// }
			// }
			// }
		}

		@Override
		public boolean onScheduleChanged(TransitConnection connection,
				TransitFlowSchedule oldSchedule, TransitFlowSchedule newSchedule) {
			return true;
		}

		@Override
		public boolean onScheduleStateChanged(TransitConnection connection,
				ScheduleState oldState, ScheduleState newState) {
			if (newState == ScheduleState.ACTIVE) {
				missing.andNot(connection.getSchedule().getBlockMask());
				lastTry = 0;
				wait = 0;
				return false;
			}
			if (newState == ScheduleState.CANCELED) {
				lastTry = 0;
				wait = 0;
				return false;
			}
			return true;
		}

		/**
		 * Returns a list of contacts matching the given comparator and
		 * excluding specific contacts.
		 * 
		 * @param comparator
		 * @param exclude
		 *            Contacts to exclude
		 * @param maxSize
		 *            Max number of results
		 * @return
		 */
		public List<TransitConnection> getSortedConnections(
				List<TransitConnection> connections,
				Comparator<TransitConnection> comparator,
				List<UniqueID> exclude, int maxSize) {
			LinkedList<TransitConnection> result = new LinkedList<TransitConnection>(
					connections);
			Collections.sort(result, comparator);
			return toList(result, exclude, maxSize);
		}

		/**
		 * Returning a list of connections by removing the contacts that are to
		 * be excluded and limiting the size of the list to a maximum.
		 * 
		 * @param contactInfos
		 * @param exclude
		 *            or null
		 * @param maxSize
		 *            or -1
		 * @return
		 */
		private List<TransitConnection> toList(
				List<TransitConnection> connections, List<UniqueID> exclude,
				int maxSize) {
			List<TransitConnection> result = new LinkedList<TransitConnection>();
			int i = 0;
			for (TransitConnection connection : connections) {
				if (exclude != null
						&& exclude.equals(connection.getEndpoint().getNodeID())) {
					continue;
				}
				result.add(connection);
				i++;
				if (i == maxSize) {
					break;
				}
			}
			return result;
		}

	}

	private static class ComparatorByJoin implements
			Comparator<TransitConnection> {

		private final boolean newestFirst;

		public ComparatorByJoin(boolean newestFirst) {
			this.newestFirst = newestFirst;
		}

		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			int ret = 0;
			if (o1.getEndpoint().getJoiningTime() < o2.getEndpoint()
					.getJoiningTime()) {
				ret = 1;
			} else if (o1.getEndpoint().getJoiningTime() > o2.getEndpoint()
					.getJoiningTime()) {
				ret = -1;
			}
			if (!newestFirst) {
				ret = ret * -1;
			}
			return ret;
		}
	}

}
