/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.optimization;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;

/**
 * Possible-High degree preemption optimization as defined in Lidanski MA.
 * @author Eduardo Lidanski
 * @version 1.0, 14.04.2014
 */
public class PossibleHighDegreePreemptionOptimization implements
		PossibleOptimizationAction {

	private TransitConnection preempt;

	private TransitConnection parentOfPreempted;

	private int layer;

	private double gain;

	private int myNumberOfChildren;

	private int preemptedNumberOfChildren;

	/* How important is the difference in depth. [0,1] */
	private final double alpha = 0.7;

	/* How important is the difference in number of children [0,1] */
	private final double beta = 1;

	public PossibleHighDegreePreemptionOptimization(TransitConnection preempt,
			TransitConnection parentOfPreempted, int layer, int actualDepth,
			int preemptedDepth, int myNumberOfChildren,
			int preemptedNumberOfChildren) {
		this.preempt = preempt;
		this.parentOfPreempted = parentOfPreempted;
		this.layer = layer;
		gain = alpha
				* Math.abs((double) (actualDepth - preemptedDepth))
				+ beta
				* Math.abs((double) (myNumberOfChildren - preemptedNumberOfChildren));
		this.myNumberOfChildren = myNumberOfChildren;
		this.preemptedNumberOfChildren = preemptedNumberOfChildren;
	}

	@Override
	public double getGain() {
		return gain;
	}

	@Override
	public TransitConnection getTargetConnection() {
		return preempt;
	}

	@Override
	public int getLayer() {
		return layer;
	}

	public TransitConnection getParentOfPreempted() {
		return parentOfPreempted;
	}
	
	public BitSet getSchedule() {
		BitSet bs = new BitSet();
		bs.set(layer);
		return bs;
	}

	@Override
	public String toString() {
		return preempt.getOwner().getNodeID() + " preempts "
				+ preempt.getEndpoint().getNodeID() + "(Layer: " + layer
				+ " Gain: " + gain + ")" + ", My number of children: "
				+ myNumberOfChildren + ", preempted number of children: "
				+ preemptedNumberOfChildren;
	}
}
