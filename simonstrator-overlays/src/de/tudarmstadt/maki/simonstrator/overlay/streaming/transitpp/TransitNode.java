/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.TCPMessageBased;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.UDP;
import de.tudarmstadt.maki.simonstrator.api.operation.Operation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.BufferInterface;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingDocument;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingOverlayNodeInterface;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.AvailabilityEstimator;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.LayerUtils;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.ThroughputEstimator;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.capacity.TransitBandwidthManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.capacity.TransitBandwidthManagerImpl;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitNeighborhood;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitNeighborhoodManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitScheduler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitSchedulerImpl;

/**
 * A node in the Transit-Overlay. The source is a normal node as well, but it
 * has a special buffer that is always filled with packets up to the current
 * playback position of the {@link StreamPlayer} to support live streaming.
 * Therefore, VoD and LiveStreaming only differ in how the Buffer of the source
 * is implemented and announcing packets. In both cases the source will get the
 * full document by the Application and the overlay will just provide the "live"
 * streaming behavior.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public class TransitNode extends AbstractOverlayNode implements
		StreamingOverlayNodeInterface {

	private int port = 0;

	private MessageBasedTransport transport;

	private MessageBasedTransport transportToTracker;

	private TransitContact ownOverlayContact;

	private TransitSettings transitSettings;

	private TransitMessageHandler messageHandler;
	
	private TransitOptimizationManager optimizationManager;

	private TransitBandwidthManager bandwidthManager;

	private TransitScheduler transitScheduler;

	private TransitNeighborhood transitNeighborhood;

	private byte[] layer;

	private byte[] maxLayer;

	private final boolean isServer;

	public int BLOCKS_PER_CHUNK;

	/**
	 * View on the video
	 */
	private StreamingDocument video;
	
	/**
	 * Create a new Transit Node.
	 * 
	 * @param peerId
	 * @param port
	 * @param isServer
	 *            a server is a node that provides new content as soon as it
	 *            reaches the playback position (live streaming) or has the full
	 *            file in advance (VoD)
	 */
	protected TransitNode(Host host, int port,
			boolean isServer, TransitSettings settings) {
		super(host);
		this.port = port;
		this.isServer = isServer;
		this.transitSettings = settings;
	}

	@Override
	public void initialize() {
		super.initialize();

		/*
		 * Bind the first Network Interface
		 */
		NetInterface net = getHost().getNetworkComponent()
				.getNetworkInterfaces().iterator().next();
		try {
			transport = getHost().getTransportComponent().getProtocol(
					UDP.class, net.getLocalInetAddress(), port);
			transportToTracker = getHost().getTransportComponent().getProtocol(
					TCPMessageBased.class, net.getLocalInetAddress(), port);
		} catch (ProtocolNotAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (isServer()) {
			ownOverlayContact = new TransitContact(getHost().getId(),
					transport.getTransInfo(), ContactType.SOURCE);
		} else {
			ownOverlayContact = new TransitContact(getHost().getId(),
					transport.getTransInfo());
		}

		ownOverlayContact.updateLayer(getLayer());
		messageHandler = new TransitMessageHandler(this, transport,
				transportToTracker);
		addPeerStatusListener(messageHandler);

		// Bind the message Handler
		transport.setTransportMessageListener(messageHandler);
		transportToTracker.setTransportMessageListener(messageHandler);

		/*
		 * TODO BR Protocol Listener not yet supported
		 */
		// transLayer.addProtocolListener(messageHandler, TransProtocol.UDP,
		// getPort());

		transitNeighborhood = new TransitNeighborhoodManager(this);
		addPeerStatusListener(transitNeighborhood);

		optimizationManager = new TransitOptimizationManager(this);
		
		transitScheduler = new TransitSchedulerImpl(this);
		addPeerStatusListener(transitScheduler);

		bandwidthManager = new TransitBandwidthManagerImpl(this);

		setPeerStatus(PeerStatus.ABSENT);
		/*
		 * FIXME BR should instead be handled in the actions-file or the churn
		 * generator.
		 */
		// if (!getHost().getProperties().isChurnAffected()) {
		// getHost().getNetLayer().goOnline();
		// }
	}

	@Override
	public int join(final OperationCallback<Object> appCallback) {
		/*
		 * Join with the tracker and inform the appCallback. The app can then
		 * start the streaming process. If we already have a tracker contact,
		 * nothing happens.
		 */
		if (getPeerStatus() != PeerStatus.ABSENT) {
			Operation<Object> op = Operations.createEmptyFailingOperation(this,
					appCallback);
			op.scheduleImmediately();
			return op.getOperationID();
		}

		OperationCallback<Object> opCallback = new OperationCallback<Object>() {
			@Override
			public void calledOperationSucceeded(Operation<Object> op) {
				setPeerStatus(PeerStatus.PRESENT);
				appCallback.calledOperationSucceeded(op);
			}

			@Override
			public void calledOperationFailed(Operation<Object> op) {
				setPeerStatus(PeerStatus.ABSENT);
				appCallback.calledOperationFailed(op);
			}
		};

		setPeerStatus(PeerStatus.TO_JOIN);

		Operation<Object> op = null;
		if (getNeighborhood().getTracker() == null) {
			return getNeighborhood().connectToTracker(opCallback);
		} else {
			op = Operations.createEmptyOperation(this, opCallback);
		}
		op.scheduleImmediately();
		return op.getOperationID();
	}

	@Override
	public int leave(OperationCallback<Object> callback) {
		if (getPeerStatus() != PeerStatus.PRESENT) {
			Operation<Object> op = Operations.createEmptyFailingOperation(this,
					callback);
			op.scheduleImmediately();
			return op.getOperationID();
		}
		setPeerStatus(PeerStatus.ABSENT);
		/*
		 * TODO how to leave the overlay. Peers will not come online again,
		 * which is why we delete as much internal state as possible - freeing
		 * up memory.
		 */
		bandwidthManager = null;
		messageHandler = null;
		ownOverlayContact = null;
		// throughputEstimator = null;
		transitNeighborhood = null;
		transitScheduler = null;
		video = null;

		return -1;
	}

	/**
	 * Join and leave are "peaceful", whereas this method does not send any
	 * messages but just stops communication on this node.
	 */
	private void leaveUngracefully() {
		if (getPeerStatus() != PeerStatus.PRESENT) {
			return;
		}
		setPeerStatus(PeerStatus.ABSENT);
		/*
		 * TODO
		 */
	}

	@Override
	public StreamingDocument getVideo() {
		return video;
	}

	@Override
	public void setVideo(StreamingDocument video) {
		this.video = video;

		if (video.isComplete()) {
			/*
			 * This is a source node, we alter the OverlayContact accordingly
			 */
			ownOverlayContact = new TransitContact(getHost().getId(),
					transport.getTransInfo(), ContactType.SOURCE);
			ownOverlayContact.updateLayer(new byte[] {
					(byte) (video.getSpatialLevels() - 1),
					(byte) (video.getTemporalLevels() - 1),
					(byte) (video.getSnrLevels() - 1) });
		}
		BLOCKS_PER_CHUNK = video.getVideoModel().getNumOfBlocksPerChunk();
	}

	@Override
	public byte[] getLayer() {
		if (layer == null) {
			/*
			 * Default to BL, if we are not yet connected to the tracker.
			 * However, do NOT set this as layer yet, as otherwise setlayer will
			 * not trigger the scheduler if IQA requests the Baselayer
			 */
			return new byte[] { 0, 0, 0 };
		}
		return layer.clone();
	}

	@Override
	public void setLayer(byte[] newLayer) {
		assert newLayer != null;
		
		Integer comp = LayerUtils.compareLayer(layer, newLayer);
		if (comp != null && comp == 0) {
			/*
			 * New Layer equals old layer. This should only happen immediately
			 * after the IQA, if it is set to baselayer, as transit starts with
			 * this assumption as well.
			 */
		} else {
			// change layer of the node
			layer = newLayer;

			// update own contact info
			ownOverlayContact.updateLayer(newLayer);

			/*
			 * Inform scheduler/buffer of new layer
			 */
			transitScheduler.switchedLayer();
		}
	}

	@Override
	public byte[] getMaxLayer() {
		return maxLayer.clone();
	}

	@Override
	public void setMaxLayer(byte[] maxLayer) {
		this.maxLayer = maxLayer;
	}

	@Override
	public boolean isServer() {
		return isServer;
	}

	@Override
	public boolean isSeeder() {
		/*
		 * with this setting, transit is a pure live-streaming overlay.
		 */
		return !isServer;
	}

	@Override
	public AvailabilityEstimator getAvailabilityEstimator() {
		return transitScheduler;
	}

	@Override
	public BufferInterface getBuffer() {
		return transitScheduler;
	}

	/**
	 * The Settings-Object
	 * 
	 * @return
	 */
	public TransitSettings getSettings() {
		return transitSettings;
	}

	@Override
	public ThroughputEstimator getThroughputEstimator() {
		return null;
	}

	/**
	 * The Bandwidth manager
	 * 
	 * @return
	 */
	public TransitBandwidthManager getBandwidthManager() {
		return bandwidthManager;
	}

	/**
	 * The Scheduler used in transit
	 * 
	 * @return
	 */
	public TransitScheduler getScheduler() {
		return transitScheduler;
	}

	/**
	 * Return the local instance of our own overlay contact. Ensure, that the
	 * returned contact is cloned if it leaves this node. Otherwise, information
	 * might propagate due to Java but not due to messages being sent.
	 * 
	 * @see de.tud.kom.p2psim.api.overlay.OverlayNode#getLocalOverlayContact()
	 */
	@Override
	public TransitContact getLocalOverlayContact() {
		return ownOverlayContact;
	}

	/**
	 * Get the message handler that should be used to dispatch every message in
	 * Transit
	 * 
	 * @return
	 */
	public TransitMessageHandler getMessageHandler() {
		return messageHandler;
	}
	
	public TransitOptimizationManager getOptimizationManager() {
		return optimizationManager;
	}

	/**
	 * The neighborhood-manager of transit.
	 * 
	 * @return
	 */
	public TransitNeighborhood getNeighborhood() {
		return transitNeighborhood;
	}


	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// TODO Auto-generated method stub

	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		leaveUngracefully();
//		leave(Operations.getEmptyCallback());
	}

}
