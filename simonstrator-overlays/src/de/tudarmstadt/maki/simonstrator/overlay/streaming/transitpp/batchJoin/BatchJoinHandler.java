/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.batchJoin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.JoinOverlayCommand;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.JoinTrackerMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.JoinTrackerReplyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.tracker.TransitTracker;

/**
 * Handles joining peers during a flash crowd depending on the parameters
 * passed. So it is time-based, or peer-based, or chain-based.
 * 
 * @author Eduardo Lidanski
 * @version 1.0, 14.04.2014
 */
public class BatchJoinHandler {

	private TransitTracker tracker;

	/**
	 * Temporary root until its capacity is exhausted.
	 */
	private BatchJoinContact temporaryRoot;

	private BatchJoinContact root;

	private final static boolean DEBUG = false;

	/**
	 * From here we can choose the next temporary root. It should be the one
	 * with the largest capacity.
	 */
	private List<BatchJoinContact> choseNextRootFrom = new ArrayList<BatchJoinContact>();

	/** All members of the next batch */
	private List<BatchJoinContact> nextBatch = new ArrayList<BatchJoinContact>();

	private long lastJoined;

	public BatchJoinHandler(TransitTracker transitTracker) {
		this.tracker = transitTracker;
		lastJoined = tracker.getSettings().getTime(TransitTimes.FLASH_CROWD_START);
	}

	public JoinTrackerReplyMessage handleJoinTrackerMsg(JoinTrackerMessage msg) {
		if (temporaryRoot == null) {
			/* The first node to arrive. */

			JoinTrackerReplyMessage reply = getReplyToTemporaryNode(
					msg.getSenderContact(), msg.getCapacity(), false);

			if (DEBUG) {
				System.out.println(Time.getFormattedTime(Time
						.getCurrentTime())
						+ " adding to batch: "
						+ msg.getSenderContact().getNodeID());
			}

			return reply;
		}

		if (DEBUG) {
			System.out
					.println(Time.getFormattedTime(Time
					.getCurrentTime())
					+ " adding to batch: "
							+ msg.getSenderContact().getNodeID());
		}

		/*
		 * If temporary root has capacity: connect joining node to temporary
		 * root.
		 */
		if (temporaryRoot.getAvailableCapacity() > 0) {
			JoinTrackerReplyMessage reply = getReplyToTemporaryNode(
					msg.getSenderContact(), msg.getCapacity(), true);

			/* The next join message is from the next batch. */
			if (tracker.getSettings().getParam(TransitParams.BATCH_MODE) == 0) {
				if (nextBatch.size() == tracker.getSettings().getParam(
						TransitParams.BATCH_JOIN_NUMBER_OF_NODES)) {
					joinActualBatch();
				}
			} else if (tracker.getSettings().getParam(TransitParams.BATCH_MODE) == 1) {
				long now = Time.getCurrentTime();
				if (now >= lastJoined
						+ tracker.getSettings()
								.getTime(TransitTimes.BATCH_TIME)) {
					lastJoined = now;
					joinActualBatch();
				}
			}

			return reply;
		}

		removeNoCapacityNodes();

		/* Choose next temporal root. */
		Collections.sort(choseNextRootFrom, new ComparatorByCapacity(true));

		temporaryRoot = choseNextRootFrom.get(0);

		JoinTrackerReplyMessage reply = getReplyToTemporaryNode(
				msg.getSenderContact(), msg.getCapacity(), true);

		/* The next join message is from the next batch. */
		if (tracker.getSettings().getParam(TransitParams.BATCH_MODE) == 0) {
			if (nextBatch.size() == tracker.getSettings().getParam(
					TransitParams.BATCH_JOIN_NUMBER_OF_NODES)) {
				joinActualBatch();
			}
		} else if (tracker.getSettings().getParam(TransitParams.BATCH_MODE) == 1) {
			long now = Time.getCurrentTime();
			if (now >= lastJoined
					+ tracker.getSettings().getTime(TransitTimes.BATCH_TIME)) {
				lastJoined = now;
				joinActualBatch();
			}
		}

		return reply;

	}

	public void onFlashCrowdFinish() {
		if (nextBatch.size() != 0) {
			joinActualBatch();
		}
	}

	private void joinActualBatch() {
		if (DEBUG) {
			System.out.println(Time.getFormattedTime(Time
					.getCurrentTime())
					+ " batch joining overlay: send message to "
					+ root
					+ " to join the overlay");
		}

		System.out.println(Time.getFormattedTime(Time.getCurrentTime())
						+ " batch joining: size=" + nextBatch.size());

		/* Now the root can join the overlay. */
		List<TransitContact> initial = tracker.getInitialContacts(root
				.getContact());
		JoinOverlayCommand joinOverlayCommand = new JoinOverlayCommand(tracker
				.getLocalOverlayContact().clone(), initial,
				tracker.createStreamingDoc());
		tracker.getTransport().send(joinOverlayCommand,
				root.getContact().getNetID(), root.getContact().getPort());

		/* Add batch nodes to the registry. */
		for (BatchJoinContact bjc : nextBatch) {
			tracker.addOrUpdateContact(bjc.getContact());
		}

		nextBatch.clear();
		temporaryRoot = null;
		root = null;
		choseNextRootFrom.clear();
	}

	private void removeNoCapacityNodes() {
		/* Remove nodes with capacity = 0 */
		List<Integer> indexesToRemove = new ArrayList<Integer>();
		for (int i = 0; i <= choseNextRootFrom.size() - 1; i++) {
			if (choseNextRootFrom.get(i).getAvailableCapacity() == 0) {
				indexesToRemove.add(i);
			}
		}
		for (Integer toRemove : indexesToRemove) {
			choseNextRootFrom.remove(toRemove.intValue());
		}
	}

	private JoinTrackerReplyMessage getReplyToTemporaryNode(
			TransitContact contact, int capacity, boolean withInitial) {
		TransitContact trackerContact = tracker.getLocalOverlayContact()
				.clone();
		/*
		 * We cannot put more connections than the maximal number of
		 * connections.
		 */
		int capacityToPut = capacity;
		if (capacityToPut > tracker.getSettings().getParam(
				TransitParams.CONNECTION_MAX_OUTGOING)) {
			capacityToPut = tracker.getSettings().getParam(
					TransitParams.CONNECTION_MAX_OUTGOING);
		}
		BatchJoinContact jc = new BatchJoinContact(contact, capacityToPut);

		List<TransitContact> initial = new ArrayList<TransitContact>();

		if (!withInitial) {
			temporaryRoot = jc;
			root = temporaryRoot;
		} else {
			if (tracker.getSettings().getParam(TransitParams.BATCH) == 1) {
				initial.add(temporaryRoot.getContact().clone());
				temporaryRoot.decreaseCapacity();
				choseNextRootFrom.add(jc);

			} else if (tracker.getSettings().getParam(TransitParams.BATCH) == 2) {
				initial.add(temporaryRoot.getContact().clone());
				/* We want to create chains. */
				int chainLevel = tracker.getSettings().getParam(
						TransitParams.BATCH_CHAIN_LEVEL);
				if (temporaryRoot.getAvailableCapacity() > chainLevel) {
					temporaryRoot.setCapacity(chainLevel);
				} else {
					temporaryRoot.decreaseCapacity();
				}
				choseNextRootFrom.add(jc);
			}

		}

		nextBatch.add(jc);

		if (DEBUG) {
			System.out.println(Time.getFormattedTime(Time.getCurrentTime())
					+ " sending " + initial + " to " + contact.getNodeID());
		}

		JoinTrackerReplyMessage reply = new JoinTrackerReplyMessage(
				trackerContact, trackerContact, tracker.createStreamingDoc(),
				initial, Time.getCurrentTime(), true);

		return reply;
	}

	/**
	 * Sorts a list by the remaining capacity of the contacts inside the list.
	 * 
	 * @author Eduardo Lidanski
	 * @version 1.0, 03.03.2014
	 */
	private static class ComparatorByCapacity implements
			Comparator<BatchJoinContact> {

		private final boolean largeCapacityFirst;

		public ComparatorByCapacity(boolean largeCapacityFirst) {
			this.largeCapacityFirst = largeCapacityFirst;
		}

		@Override
		public int compare(BatchJoinContact o1, BatchJoinContact o2) {
			int ret = 0;
			if (o1.getAvailableCapacity() < o2.getAvailableCapacity()) {
				ret = 1;
			} else if (o1.getAvailableCapacity() > o2.getAvailableCapacity()) {
				ret = -1;
			}

			if (!largeCapacityFirst) {
				ret = ret * -1;
			}

			return ret;
		}
	}
}
