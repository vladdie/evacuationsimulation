/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.operations;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageCallback;
import de.tudarmstadt.maki.simonstrator.api.operation.AbstractOperation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SetConnectionDirectionMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SetConnectionDirectionMessageReply;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SetConnectionDirectionMessageReply.Response;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnectionManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;

/**
 * Operation which tries to switch the direction of a connection to the opposite
 * direction. If the switch is successful: returns true, if not: false. An
 * unsuccessful operation may happen if the maximum number of connections
 * reaches the maximum and no connection could be cut because all of them take
 * part of a flow.
 * 
 * @author Eduardo Lidanski
 * @version 1.0, 18.02.2014
 */
public class SwitchConnectionDirectionOperation extends
		AbstractOperation<TransitNode, Boolean> implements TransMessageCallback {

	private TransitConnection connectionToSwitch;

	private ConnectionDirection switchToDirection;

	private ConnectionDirection actualDirection;

	private TransitConnectionManager connectionManager;

	private TransitNode node;

	/**
	 * Result of the operation. It is true if the switch could take place,
	 * otherwise false.
	 */
	private Boolean couldSwitch = false;

	public SwitchConnectionDirectionOperation(TransitNode component,
			TransitConnection connectionToSwitch,
			OperationCallback<Boolean> callback) {
		super(component, callback);
		this.node = component;
		this.connectionManager = node.getNeighborhood().getConnectionManager();
		this.connectionToSwitch = connectionToSwitch;

		actualDirection = connectionToSwitch.getDirection();

		switch (actualDirection) {
		case IN:
			switchToDirection = ConnectionDirection.OUT;
			break;
		case OUT:
			switchToDirection = ConnectionDirection.IN;
			break;
		}
	}

	@Override
	protected void execute() {

		/* Check if the connection is reserved by the optimization manager. */
		if (node.getOptimizationManager().isReserved(connectionToSwitch)) {
			couldSwitch = false;
			operationFinished(false);
			return;
		}

		/* Check if we can open a connection in the new direction. */
		if (!connectionManager.mayOpenNewConnection(switchToDirection)) {
			/*
			 * Since no capacity left: check if we can cut an unimportant
			 * connection.
			 */
			boolean cut = connectionManager
					.cutUnimportantConnection(switchToDirection);
			if (!cut) {
				/* We give up. Cannot switch direction. */
				couldSwitch = false;
				operationFinished(false);
				return;
			}
		}

		/* We can switch direction! */
		boolean canReserve = connectionManager.reserve(switchToDirection);
		if (!canReserve) {
			/* Should not happen currently. Someone reserved it previously. */
			couldSwitch = false;
			operationFinished(false);
			return;
		}

		TransitContact endPoint = connectionToSwitch.getEndpoint();

		SetConnectionDirectionMessage switchConnectionMessage = new SetConnectionDirectionMessage(
				connectionToSwitch, actualDirection); // for the endpoint, this
														// actualDirection is
														// its opposite
														// direction.
		node.getMessageHandler().sendAndWait(switchConnectionMessage, endPoint,
				this);

	}

	@Override
	public void messageTimeoutOccured(int commId) {
		couldSwitch = false;
		connectionManager.unreserve(switchToDirection);
		operationFinished(false);
	}

	@Override
	public void receive(Message msg, TransInfo senderInfo, int commId) {
		if (isFinished()) {
			return;
		}

		SetConnectionDirectionMessageReply reply = (SetConnectionDirectionMessageReply) msg;

		Response response = reply.getResponse();

		switch (response) {
		case CANNOT_CHANGE_DIRECTION:
			couldSwitch = false;
			connectionManager.unreserve(switchToDirection);
			operationFinished(false);
			break;
		case OK:
			connectionToSwitch.setDirection(switchToDirection);
			connectionManager.unreserve(switchToDirection);
			couldSwitch = true;
			operationFinished(true);
			break;
		}

		operationFinished(true);
	}

	@Override
	public Boolean getResult() {
		return couldSwitch;
	}

}
