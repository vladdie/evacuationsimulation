package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.monitor;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubListener;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Subscription;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;

/**
 * Helper class for the receiver-side of the TransitMonitor - all received
 * per-node updates are processed and higher-level information is given to upper
 * layers.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class TransitMonitorReceiver extends AbstractOverlayNode {

	protected List<TransitMonitorListener> listeners = new LinkedList<TransitMonitorListener>();

	private ServerEvalOperation evalOp;

	public TransitMonitorReceiver(Host host) {
		super(host);
	}

	@Override
	public void initialize() {
		super.initialize();
		evalOp = new ServerEvalOperation(this);
		evalOp.scheduleWithDelay(5 * Time.SECOND);
	}

	/**
	 * Add a listener that is informed on node updates
	 * 
	 * @param listener
	 */
	public void addMonitorListener(TransitMonitorListener listener) {
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	/**
	 * Removes the given listener.
	 * 
	 * @param listener
	 */
	public void removeMonitorListener(TransitMonitorListener listener) {
		listeners.remove(listener);
	}

	@Override
	public OverlayContact getLocalOverlayContact() {
		return null;
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// not needed
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		// not needed
	}
	
	/**
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private class ServerEvalOperation extends
			PeriodicOperation<TransitMonitorReceiver, Void> implements
			PubSubListener {

		private final PubSubComponent pubsub;

		private final Topic topic;

		private List<TransitMonitorInfo> nodeInfos = new LinkedList<TransitMonitorInfo>();

		public ServerEvalOperation(TransitMonitorReceiver component) {
			super(component, null, TransitMonitor.REPORTING_INTERVAL);
			// Find and bind the pub/sub
			try {
				pubsub = getHost().getComponent(PubSubComponent.class);
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError("Pub/Sub not found.");
			}
			// Create relevant topic
			this.topic = pubsub.createTopic(TransitMonitor.TOPIC);

			// Subscribe
			Subscription s = pubsub.createSubscription(topic);
			pubsub.subscribe(s, this);
		}

		@Override
		protected void executeOnce() {
			/*
			 * Delete outdated information
			 */
			Iterator<TransitMonitorInfo> it = nodeInfos.iterator();
			while (it.hasNext()) {
				TransitMonitorInfo nodeInfo = it.next();
				if (nodeInfo.getLastUpdateReceived()
						+ TransitMonitor.REPORTING_INTERVAL * 3 < Time
							.getCurrentTime()) {
					it.remove();
					for (TransitMonitorListener listener : listeners) {
						// listener.onRemoveNode(nodeInfo);
					}
				}
			}
			for (TransitMonitorListener listener : listeners) {
				listener.onInterval(nodeInfos);
			}
		}

		@Override
		public Void getResult() {
			return null;
		}

		@Override
		public void onNotificationArrived(Subscription matchedSubscription,
				Notification notification) {
			String nodeName = null;
			String nodeId = null;
			List<String> connectionsIn = null;
			List<String> connectionsOut = null;
			for (Attribute<?> attr : notification.getAttributes()) {
				assert attr.getType().equals(String.class);
				if (attr.getName().equals(TransitMonitor.ATTR_NODENAME)) {
					nodeName = (String) attr.getValue();
				} else if (attr.getName()
						.equals(TransitMonitor.ATTR_TRANSIT_ID)) {
					nodeId = (String) attr.getValue();
				} else if (attr.getName().equals(
						TransitMonitor.ATTR_CONNECTIONS_IN)) {
					connectionsIn = Arrays.asList(((String) attr.getValue()).split(";"));
				} else if (attr.getName().equals(
						TransitMonitor.ATTR_CONNECTIONS_OUT)) {
					connectionsOut = Arrays.asList(((String) attr.getValue()).split(";"));
				}
			}

			boolean updated = false;
			for (TransitMonitorInfo nodeInfo : nodeInfos) {
				if (nodeInfo.getNodeId().equals(nodeId)) {
					nodeInfo.update(connectionsIn, connectionsOut);
					updated = true;
					for (TransitMonitorListener listener : listeners) {
						listener.onUpdateNode(nodeInfo);
					}
					break;
				}
			}
			if (!updated) {
				TransitMonitorInfo nodeInfo = new TransitMonitorInfo(nodeName, nodeId);
				nodeInfo.update(connectionsIn, connectionsOut);
				nodeInfos.add(nodeInfo);
				for (TransitMonitorListener listener : listeners) {
					listener.onAddNode(nodeInfo);
				}
			}
		}

	}

	/**
	 * Container for all information related to one node
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	public static class TransitMonitorInfo {

		private final String nodeName;

		private final String nodeId;

		private List<String> connectionsIn;

		private List<String> connectionsOut;
		
		private long lastUpdateReceived;

		public TransitMonitorInfo(String nodeName, String nodeId) {
			this.nodeName = nodeName;
			this.nodeId = nodeId;
		}

		public void update(List<String> connectionsIn,
				List<String> connectionsOut) {
			this.connectionsIn = connectionsIn;
			this.connectionsOut = connectionsOut;
			this.lastUpdateReceived = Time.getCurrentTime();
		}

		public String getNodeId() {
			return nodeId;
		}

		public String getNodeName() {
			return nodeName;
		}

		public List<String> getConnectionsIn() {
			return connectionsIn;
		}

		public List<String> getConnectionsOut() {
			return connectionsOut;
		}

		public long getLastUpdateReceived() {
			return lastUpdateReceived;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((nodeId == null) ? 0 : nodeId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TransitMonitorInfo other = (TransitMonitorInfo) obj;
			if (nodeId == null) {
				if (other.nodeId != null)
					return false;
			} else if (!nodeId.equals(other.nodeId))
				return false;
			return true;
		}

	}

	/**
	 * The factory
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	public static class TransitMonitorReceiverFactory implements
			HostComponentFactory {

		@Override
		public HostComponent createComponent(Host host) {
			return new TransitMonitorReceiver(host);
		}

	}

	/**
	 * This listener is called on specific actions (onUpdate, onRemove, onAdd)
	 * as well as periodically with the current state (onInterval).
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	public static interface TransitMonitorListener {
		/**
		 * Called once per monitoring interval
		 * 
		 * @param currentState
		 */
		public void onInterval(List<TransitMonitorInfo> currentNodeInfos);

		/**
		 * Called, when a node sends information
		 * 
		 * @param updatedNodeInfo
		 */
		public void onUpdateNode(TransitMonitorInfo updatedNodeInfo);

		/**
		 * Called, when a new node is detected
		 * 
		 * @param newNodeInfo
		 */
		public void onAddNode(TransitMonitorInfo newNodeInfo);

		/**
		 * Called, when a node is removed due to inactivity or a leave-request
		 * 
		 * @param removedNodeInfo
		 */
		public void onRemoveNode(TransitMonitorInfo removedNodeInfo);

	}

}
