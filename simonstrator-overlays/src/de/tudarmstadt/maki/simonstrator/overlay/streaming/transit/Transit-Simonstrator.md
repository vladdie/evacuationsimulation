# Porting TRANSIT to Simonstrator Framework
In this document, major changes to the original TRANSIT are described that were done to port the overlay to the new Simonstrator Framework.

* Removed hard closing of connection on timeouts in ConnectionControlOperation:
class: de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.operations.ConnectionControlOperation

In method _handleTimeout()_: call to doCloseConnection was removed to not close connections on timeouts. This was necessary as UDP packets seemed to be lost a lot causing these timeouts. 

* Changed scheduling of flow manager for specific transition setting:
class: de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.AbstractTransitFlowManager

In method _tick(int chunk)_: For the demonstrator, the scheduling of the flow manager is only enabled if TransitParams.FLOW_MAX_INCOMING > 0

* Added sending of actual video block data:
class: de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.serving.AbstractServingStrategy

In method _doSendBlock(TransitConnection connection, int blocknum)_: added sending of actual video block data

* New methods at tracker and changes to its implementation
class: de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.tracker.TransitTracker

In this class, several changes where done: e.g. method to inform all peers about new neighbors (used for transitions where peers need to learn about potential neighbors to, e.g. build a P2P connection).

* Changed default timeout values
class: de.tud.kom.p2psim.impl.overlay.streaming.transit.TransitSettings.TransitTimes

In this class, the default timeout values were adapted to the demonstrator needs.