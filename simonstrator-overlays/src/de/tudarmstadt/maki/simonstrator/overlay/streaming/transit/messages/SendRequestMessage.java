/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestSchedule;

/**
 * Implementation of a {@link TransitRequestMessage}
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public class SendRequestMessage extends AbstractTransitMessage implements
		TransitRequestMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5004880336810809405L;

	private final RequestMsgType type;

	private final TransitRequestSchedule requestSchedule;

	/**
	 * RequestMessage with a schedule (PARTLY or REQUEST)
	 * 
	 * @param sender
	 * @param type
	 * @param schedule
	 */
	public SendRequestMessage(TransitContact sender, RequestMsgType type,
			TransitRequestSchedule schedule) {
		super(sender);
		this.type = type;
		/*
		 * Ensure that the schedule is copied
		 */
		this.requestSchedule = new TransitRequestSchedule(schedule);
	}

	@Override
	public TransitRequestSchedule getRequestSchedule() {
		assert requestSchedule != null;
		return requestSchedule;
	}

	@Override
	public RequestMsgType getType() {
		return type;
	}

	@Override
	public long getSize() {
		if (requestSchedule == null) {
			return super.getSize() + 1;
		}
		return super.getSize() + 1 + requestSchedule.getTransmissionSize();
	}

	@Override
	public String toString() {
		return (requestSchedule != null ? type.toString() + " from "
				+ getSenderContact().toString() + " "
				+ requestSchedule.toString() : type.toString());
	}

}
