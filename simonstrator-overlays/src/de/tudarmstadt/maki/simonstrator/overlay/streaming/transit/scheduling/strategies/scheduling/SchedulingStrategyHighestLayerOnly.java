/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;

public class SchedulingStrategyHighestLayerOnly implements SchedulingStrategy  {

	/** The node. */
	private TransitNode node;
	
	/**
	 * Instantiates a new transit piece picking strategy linear.
	 * 
	 * @param node
	 *            the node
	 */
	public SchedulingStrategyHighestLayerOnly(TransitNode node) {
		this.node = node; 
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling.SchedulingStrategy#selectNextBlocks(de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode, int, int)
	 */
	@Override
	public BitSet selectNextBlocks(int bufferBegin, int offsetGlobal, int blocksToSchedule) {
		
		assert bufferBegin >= 0;
		
		final int chunkSize = node.BLOCKS_PER_CHUNK;
		int newestBlockNum = node.getScheduler().getRequestManager().getNewestSeenBlockNumber();
		if( newestBlockNum < offsetGlobal ) {
			newestBlockNum = blocksToSchedule * chunkSize - 1;
		}
		
		/* SERVING MODE 2: HIGHEST LAYER ONLY! */
		final BitSet missing = new BitSet();		
		int offset = newestBlockNum;
		
		/* Start with first base layer block. */
		if( offset % chunkSize != chunkSize - 1 ) {
			offset -= (offset % chunkSize) + 1;
		}
		
		/* Schedule amount of blocks. */
		int blocksScheduled = 0;
		while(offset - offsetGlobal >= 0 && blocksScheduled < blocksToSchedule) {
			if (!node.getVideo().haveBlock(offset)) {
				missing.set(offset - offsetGlobal);
				blocksScheduled++;
			}
			offset -= chunkSize;
		}

		return missing;
	}

}
