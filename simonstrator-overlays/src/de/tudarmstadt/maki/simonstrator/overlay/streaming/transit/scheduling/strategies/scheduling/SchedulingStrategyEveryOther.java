/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;

public class SchedulingStrategyEveryOther implements SchedulingStrategy {

	/** The node. */
	private final TransitNode node;

	/** The pick even chunks. */
	private final boolean pickEvenChunks;

	/**
	 * Instantiates a new transit piece picking strategy linear.
	 * 
	 * @param node
	 *            the node
	 */
	public SchedulingStrategyEveryOther(TransitNode node) {
		this.node = node;
		this.pickEvenChunks = Randoms.getRandom(SchedulingStrategyEveryOther.class).nextBoolean();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling
	 * .strategies.scheduling.SchedulingStrategy#selectNextBlocks(int, int, int)
	 */
	@Override
	public BitSet selectNextBlocks(int bufferBegin, int offsetGlobal, int blocksToSchedule) {

		assert bufferBegin >= 0;

		final int chunkSize = node.BLOCKS_PER_CHUNK;
		int newestBlockNum = node.getScheduler().getRequestManager().getNewestSeenBlockNumber();
		if (newestBlockNum < offsetGlobal) {
			newestBlockNum = blocksToSchedule * chunkSize + offsetGlobal;
		}

		/* SERVING MODE 2: HIGHEST LAYER ONLY! */
		final BitSet missing = new BitSet();
		int pickingOffset = newestBlockNum;

		/* Subtract a picking offset eventually skip last frame. */
		if ((pickEvenChunks && bufferBegin % 2 == 1) || (!pickEvenChunks && bufferBegin % 2 == 0)) {
			pickingOffset -= chunkSize;
		}

		/* Select last baselayer. */
		if (pickingOffset % chunkSize != 0) {
			pickingOffset -= pickingOffset % chunkSize;
		}

		for (int blockOffset = 0; blockOffset < blocksToSchedule * 2; blockOffset += 2) {
			for (int i = 0; i < chunkSize; i++) {
				int blocknum = pickingOffset - blockOffset * node.BLOCKS_PER_CHUNK - i - 1;
				if (blocknum - offsetGlobal >= 0 && !node.getVideo().haveBlock(blocknum)) {
					missing.set(blocknum - offsetGlobal);
				}
			}
		}

		/* Now schedule full block with all chunks. */
		return missing;
	}

}
