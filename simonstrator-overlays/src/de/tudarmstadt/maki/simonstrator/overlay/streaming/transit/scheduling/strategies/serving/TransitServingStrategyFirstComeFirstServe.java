/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.serving;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestManagerImpl;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestSchedule;

public class TransitServingStrategyFirstComeFirstServe extends AbstractServingStrategy {

	private final double SCHEDULER_NEVER_ABOVE_VIDEO_TIMES;
	private final long SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_PERCENT_LOAD;

	public TransitServingStrategyFirstComeFirstServe(TransitRequestManagerImpl transitRequestManager) {
		super(transitRequestManager);
		SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_PERCENT_LOAD = transitRequestManager.getNode().getSettings()
		        .getParam(TransitParams.SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_PERCENT_LOAD);
		SCHEDULER_NEVER_ABOVE_VIDEO_TIMES = transitRequestManager.getNode().getSettings()
		        .getParam(TransitParams.SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_VIDEO_TIMES) / 100d;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling
	 * .strategies .serving
	 * .ServingStrategy#handleRequest(de.tud.kom.p2psim.impl.overlay.streaming
	 * .transit.neighborhood.TransitConnection,
	 * de.tud.kom.p2psim.impl.overlay.streaming
	 * .transit.scheduling.TransitRequestSchedule)
	 */
	@Override
	public void handleRequest(TransitConnection connection, TransitRequestSchedule request) {

		if (request == null || connection == null || !transitRequestManager.getNode().isPresent())
			return;

		/* Check if bandwidth is available. */
		final double connectionLoad = transitRequestManager.getNode().getBandwidthManager()
		        .getLoad(ConnectionDirection.OUT);
		final double usedBandwidth = transitRequestManager.getNode().getBandwidthManager()
		        .getUsedBandwidth(ConnectionDirection.OUT) / 1024;
		byte[] layer = transitRequestManager.getNode().getLayer();
		final double videoRate = transitRequestManager.getNode().getVideo()
				.getVideoModel().getLayerByteRate(layer[0], layer[1], layer[2]); // StreamingConfiguration.getVideoByteRate();

		double contributionRatio = usedBandwidth / videoRate;
		if (connectionLoad * 100 > SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_PERCENT_LOAD
		        || contributionRatio > SCHEDULER_NEVER_ABOVE_VIDEO_TIMES) {
			/* Okay, not too much bandwidth left. Deny this one. */
			doDenyRequest(connection);
		} else {
			/* What the heck, we got plenty of bandwidth. So serve the poor guy. */
			new ServeRequestWorker(connection);
		}

	}

}
