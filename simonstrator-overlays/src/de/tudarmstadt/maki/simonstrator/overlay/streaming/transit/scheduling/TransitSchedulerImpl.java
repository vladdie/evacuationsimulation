/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling;

import java.util.BitSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitFlowManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitRequestManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.TransitSimpleFlowManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling.SchedulingStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling.SchedulingStrategyBaseLayerOnly;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling.SchedulingStrategyEveryOther;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling.SchedulingStrategyHighestLayerOnly;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling.SchedulingStrategyLinearPlayback;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling.SchedulingStrategyRandom;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling.SchedulingStrategyReverseZigZag;

/**
 * Implementation of a {@link TransitScheduler}
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public class TransitSchedulerImpl implements TransitScheduler {

	protected static final boolean DEBUG = false;

	private final TransitFlowManager flowManager;

	private final TransitRequestManager requestManager;

	private final List<BufferListener> bufferListeners;

	/**
	 * The node
	 */
	private final TransitNode node;

	/**
	 * This is the chunk that is to be played next (or, if the player stalled:
	 * the chunk that is not yet complete)
	 */
	private int currentBufferBegin = -1;

	/**
	 * This chunk is the first upcoming, missing chunk within our buffer. If the
	 * player is stalling, this will usually be filled up again before playback
	 * is continued.
	 */
	private int mostUrgentChunk = -1;

	private long timestampStart = 0;

	private long timestampFirstPlay = 0;

	private final long initialBufferDelayTime;

	private boolean active = false;

	private BitSet mask;

	/**
	 * Internal bookkeeping of the player state
	 */
	private PlaybackState playbackState = PlaybackState.NONE;

	/** The scheduling mode. */
	private final int schedulingMode;

	/** The scheduling strategy. */
	private SchedulingStrategy schedulingStrategy;

	/** The urgent size. */
	private int urgentSize;

	public TransitSchedulerImpl(TransitNode node) {
		this.node = node;
		flowManager = new TransitSimpleFlowManager(node);
		node.addPeerStatusListener(flowManager);
		requestManager = new TransitRequestManagerImpl(node);
		node.addPeerStatusListener(requestManager);
		bufferListeners = new LinkedList<BufferListener>();
		initialBufferDelayTime = node.getSettings().getTime(TransitTimes.INITIAL_BUFFER_DELAY);

		schedulingMode = node.getSettings().getParam(TransitParams.SCHEDULING_MODE);
		switch (schedulingMode) {
		case 0:
			schedulingStrategy = new SchedulingStrategyLinearPlayback(node);
			break;
		case 1:
			schedulingStrategy = new SchedulingStrategyBaseLayerOnly(node);
			break;
		case 2:
			schedulingStrategy = new SchedulingStrategyHighestLayerOnly(node);
			break;
		case 3:
			schedulingStrategy = new SchedulingStrategyEveryOther(node);
			break;
		case 4:
			schedulingStrategy = new SchedulingStrategyRandom(node);
			break;
		case 5:
			schedulingStrategy = new SchedulingStrategyReverseZigZag(node);
			break;
		default:
			throw new AssertionError("Unknown SCHEDULING_MODE selected in config.");
		}
		this.urgentSize = node.getSettings().getParam(TransitParams.REQUEST_BUFFER_URGENT_SIZE);

		if (node.isHomeSwarmDevice()) {
			System.out.println("SCHEDULING_MODE = " + schedulingMode);
		}

	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		switch (peerStatus) {
		case PRESENT:
		case TO_JOIN:
			active = true;
			break;

		case ABSENT:
			active = false;
			break;

		default:
			throw new AssertionError("Unknown PeerStatus.");
		}
	}

	@Override
	public TransitFlowManager getFlowManager() {
		return flowManager;
	}

	@Override
	public TransitRequestManager getRequestManager() {
		return requestManager;
	}

	/**
	 * Sets the urgent size.
	 * 
	 * @param urgentSize
	 *            the new urgent size
	 */
	@Override
	public void setUrgentSize(int urgentSize) {
		this.urgentSize = urgentSize;
	}

	/**
	 * Gets the urgent size.
	 * 
	 * @return the urgent size
	 */
	@Override
	public int getUrgentSize() {
		return urgentSize;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling
	 * .TransitScheduler#getBufferBegin()
	 */
	@Override
	public int getBufferBegin() {
		return currentBufferBegin;
	}

	@Override
	public void receivedBlock(TransitBlockMessage blockMessage, TransitReplyHandler replyTo) {

		boolean duplicate = false;

		if (node.getVideo().haveBlock(blockMessage.getBlockNumber())) {
			duplicate = true;
		} else {
			// mark block as received
			if (node.getVideo().getStreamingData() != null) {
				node.getVideo()
						.getStreamingData()
						.getSouthboundInterface()
						.receivedBlock(blockMessage.getBlockNumber(),
								blockMessage.getRealData());
			}
			node.getVideo().markBlockAsReceived(blockMessage.getBlockNumber());
		}
		if (blockMessage.isLiveBlock()) {
			flowManager.receivedLiveBlock(blockMessage, replyTo, duplicate);
			requestManager.cancelRequest(blockMessage.getBlockNumber());
		} else {
			requestManager.receivedRequestedBlock(blockMessage, replyTo, duplicate);
		}
	}

	@Override
	public void doSwitchLayer(byte[] targetLayer, LayerSwitchCallback callback) {
		/*
		 * TODO The layer switching mechanism is currently not implemented in
		 * transit.
		 */
	}

	@Override
	public int getCurrentBufferLength() {
		// Calculate the buffering advance
		int i = getBufferBegin();
		if (i < 0) {
			return 0;
		}
		int length = 0;
		for (; i < node.getVideo().getLength(); i++) {
			if (!node.getVideo().layerIsCoveredByChunkBlocks(i, node.getLayer())) {
				break;
			}
			length++;
		}
		return length;
	}

	@Override
	public long getStartupDelay() {
		return Math.max(0, timestampFirstPlay - timestampStart);
	}

	@Override
	public byte[] getLayerForChunk(int chunk) {
		/*
		 * TODO the layer switching mechanism is currently not implemented.
		 */
		return node.getLayer();
	}

	@Override
	public boolean hasChunk(int chunk) {
		return node.getVideo().layerIsCoveredByChunkBlocks(chunk, getLayerForChunk(chunk));
	}

	@Override
	public boolean tryToPlay(int chunk) {

		node.getLocalOverlayContact().updateCurrentChunk(chunk);
		node.getLocalOverlayContact().setLoad(
		        (short) (10 * node.getBandwidthManager().getLoad(ConnectionDirection.OUT)));

		if (!active) {
			// intercept ticks here.
			System.err.println("TS: Node is not active!");
			return false;
		}

		if (schedulingMode == 0) {
			/* Mode 0: Normal playback mode. */

			if (playbackState == PlaybackState.NONE) {
				timestampStart = Time.getCurrentTime();
				playbackState = PlaybackState.BUFFERING;
				currentBufferBegin = chunk;
				mostUrgentChunk = chunk;
				if (node.isServer()) {
					playbackState = PlaybackState.PLAYING;
				}
			}
			while (hasChunk(mostUrgentChunk)) {
				mostUrgentChunk++;
			}

			boolean hasCurrentChunk = mostUrgentChunk > chunk;
			if (DEBUG && hasCurrentChunk) {
				System.out.println("TransitScheduler: current chunk " + chunk
						+ " is available.");
			}

			PlaybackState newState = playbackState;
			if (playbackState == PlaybackState.BUFFERING) {
				if (DEBUG && hasCurrentChunk) {
					System.out
							.println("TransitScheduler: current buffer length: "
									+ getCurrentBufferLength());
				}
				if (hasCurrentChunk
						&& getCurrentBufferLength() >= urgentSize * 0.75
				        && Time.getCurrentTime() >= timestampStart + initialBufferDelayTime) {
					if (timestampFirstPlay == 0) {
						timestampFirstPlay = Time.getCurrentTime();
					}
					newState = PlaybackState.PLAYING;
				}
			} else if (playbackState == PlaybackState.PLAYING) {
				if (!hasCurrentChunk) {
					newState = PlaybackState.BUFFERING;
				}
			}
			boolean canPlay = updateState(playbackState, newState, chunk);
			if (canPlay) {
				currentBufferBegin++;
				assert currentBufferBegin == chunk + 1;
			}
			tick(chunk);
			return canPlay;

		} else {

			/*
			 * Mode > 0: Some other scheduling mode. Scheduling Strategy will
			 * take care.
			 */
			if (playbackState == PlaybackState.NONE) {
				timestampStart = Time.getCurrentTime();
				playbackState = PlaybackState.PLAYING;
			}
			currentBufferBegin = chunk;
			tick(chunk);

			if (requestManager.getNewestSeenBlockNumber() < currentBufferBegin
					* node.getVideo().getVideoModel().getNumOfBlocksPerChunk()) {
				playbackState = PlaybackState.BUFFERING;
				return false;
			}

			playbackState = PlaybackState.PLAYING;
			return true;
		}
	}

	/**
	 * Updates the state and returns true, if the playback of the current chunk
	 * is possible.
	 * 
	 * @param oldState
	 * @param newState
	 * @param chunk
	 * @return
	 */
	private boolean updateState(PlaybackState oldState, PlaybackState newState, int chunk) {
		playbackState = newState;
		if (newState != oldState) {
			if (DEBUG) {
				System.out.println("TransitScheudler: switching from "
						+ oldState.toString() + " to " + newState.toString());
			}
			for (BufferListener bufferListener : bufferListeners) {
				bufferListener.onPlaybackStateChanged(oldState, newState, chunk);
			}
		}
		return newState == PlaybackState.PLAYING;
	}

	@Override
	public void tick(int chunkNumber) {

		if (DEBUG) {
			System.out
					.println("TransitScheduler: executing tick on chunk number "
							+ chunkNumber);
		}

		/*
		 * Execute flows
		 */
		flowManager.tick();

		/*
		 * Request missing blocks
		 */
		if (node.isServer()) {
			return;
		}

		int bufferBegin = getBufferBegin();

		/* Calculate offset. */
		int offset = -1;
		for (int chunk = bufferBegin; chunk < bufferBegin + urgentSize; chunk++) {
			if (!node.getVideo().layerIsCoveredByChunkBlocks(chunk, node.getLayer())) {
				offset = chunk * node.BLOCKS_PER_CHUNK;
				break;
			}
		}
		BitSet missing = schedulingStrategy.selectNextBlocks(bufferBegin, offset, urgentSize);

		if (!missing.isEmpty()) {
			if (DEBUG) {
				System.out
						.println("TransitScheduler: requesting missing blocks "
								+ missing.toString() + " with offset: "
								+ offset);
			}
			requestManager.requestBlocks(offset, missing);
		}
	}

	@Override
	public PlaybackState getPlaybackState() {
		return playbackState;
	}

	@Override
	public void switchedLayer() {
		// generate new BitSet
		mask = node.getVideo().getLayerMask(node.getLayer());
		flowManager.requestFlow(mask);
	}

	@Override
	public void addBufferListener(BufferListener listener) {
		bufferListeners.add(listener);
	}

	@Override
	@Deprecated
	public Map<UniqueID, BitSet> getAvailableBlocksByAllNeighbors() {
		/*
		 * TODO currently, this is only used for analyzing and PQA with
		 * NetStatusAdaption. In live-Streaming, we just take the contact-info
		 * layermap and inflate it to a BitSet spanning the current buffer size,
		 * so that PQA is satisfied...
		 */
		Map<UniqueID, BitSet> result = new LinkedHashMap<UniqueID, BitSet>();
		List<TransitConnection> incoming = node.getNeighborhood().getConnectionManager()
		        .getConnections(ConnectionDirection.IN, ConnectionState.OPEN, null, null);
		int offset = node.BLOCKS_PER_CHUNK * currentBufferBegin;
		for (TransitConnection connection : incoming) {
			BitSet mask = (BitSet) node.getVideo().getLayerMask(connection.getEndpoint().getLayer()).clone();
			BitSet chunks = new BitSet();
			for (int i = mask.nextSetBit(0); i >= 0; i = mask.nextSetBit(i + 1)) {
				for (int j = 0; j < StreamingConfiguration.BUFFER_SIZE; j++) {
					chunks.set(offset + (j * node.BLOCKS_PER_CHUNK) + i, true);
				}
			}

			result.put(connection.getEndpoint().getNodeID(), chunks);
		}

		return result;
	}

}
