/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnectionListener;

/**
 * Conceptionally, this is the extension to the
 * {@link TransitConnectionListener} on a scheduler level. It is triggered upon
 * changes or actions related to a {@link TransitFlowSchedule} running on a given
 * {@link TransitConnection}. It can be used inside a Scheduler to react to
 * missing blocks or to measure the performance of the Connection in terms of
 * fulfillment of the schedule.
 * 
 * The Listener registers itself with an individual {@link TransitConnection}.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 15.06.2012
 */
public interface TransitFlowScheduleListener {

	/**
	 * Notifies the listener that the schedule assigned to the connection
	 * changed
	 * 
	 * @param connection
	 *            the connection on which the action happened
	 * @param oldSchedule
	 * @param newSchedule
	 * @return true, if we want to receive further calls. False, if the listener
	 *         is to be removed.
	 */
	public boolean onScheduleChanged(TransitConnection connection,
			TransitFlowSchedule oldSchedule,
			TransitFlowSchedule newSchedule);

	/**
	 * The state connection with regard to the schedule changed. We can return
	 * false to notify the schedule, that we are no longer interested in the
	 * connection (we will be removed as a Listener)
	 * 
	 * @param connection
	 * @param oldState
	 * @param newState
	 * @return true, if we want to receive further calls. False, if the listener
	 *         is to be removed.
	 */
	public boolean onScheduleStateChanged(TransitConnection connection,
			ScheduleState oldState, ScheduleState newState);

}
