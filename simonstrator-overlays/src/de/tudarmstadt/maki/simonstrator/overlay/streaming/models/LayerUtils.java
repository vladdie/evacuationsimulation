package de.tudarmstadt.maki.simonstrator.overlay.streaming.models;

import java.util.ArrayList;
import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingDocument;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingOverlayNodeInterface;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.VideoModel;

/**
 * This class includes the Initial Quality Adaption (IQA) for SVC streaming and
 * some general helping functions to work with layers.
 * 
 * @author Yue Sheng
 */
public class LayerUtils {

	/**
	 * Initial Quality Adaptation Algorithm
	 * 
	 * @param video
	 * @param scrSize
	 *            Screen size of local node
	 * @param bandWidth
	 *            Bandwidth of local node in kBps
	 * @param compPower
	 *            Computing power of local node
	 * @return The highest possible SVC layer
	 */
	public static ArrayList<byte[]> getFittingIQALayers(StreamingDocument video, int scrSize, int bandWidth,
	        int compPower) {
		byte d = video.getSpatialLevels();
		byte t = video.getTemporalLevels();
		byte q = video.getSnrLevels();

		VideoModel videoModel = video.getVideoModel();

		/* filter layers according to local resources */
		ArrayList<byte[]> result = new ArrayList<byte[]>();

		for (byte i = 0; i < d; i++) {
			for (byte j = 0; j < t; j++) {
				for (byte k = 0; k < q; k++) {
					int layerComplexity = videoModel.getLayerComplexity(i, j, k);
					double layerBandwidth = videoModel.getLayerByteRate(i, j, k);
					if (i <= scrSize && layerComplexity <= compPower && layerBandwidth <= bandWidth) {
						byte[] match = { i, j, k };
						result.add(match);
					}
				}
			}
		}

		return result;
	}

	/**
	 * Net status adaption using block availability of neighbors
	 * 
	 * @param availability
	 *            the block availability of all the neighbor's buffers
	 * @return the new layer level after net-status adaptation
	 */
	public static byte[] getHighestPossibleNetStatLayer(StreamingOverlayNodeInterface node, BitSet availability,
	        int bufferSize) {
		StreamingDocument video = node.getVideo();
		byte[] initLayer = node.getMaxLayer();
		for (int i = 0; i < bufferSize; i++) {
			BitSet chunkAvailability = availability.get(i
					* video.getVideoModel().getNumOfBlocksPerChunk(), (i + 1)
					* video.getVideoModel().getNumOfBlocksPerChunk());
			if (!coversLayer(chunkAvailability, video.getLayerMask(initLayer))) {
				return getHighestCoveredLayer(chunkAvailability, video);
			}
		}
		return initLayer;
	}

	/**
	 * 
	 * @param chunkMap
	 *            the block map with the size of a video chunk
	 * @param video
	 * @return the highest SVC layer level of the given chunk
	 */
	public static byte[] getHighestCoveredLayer(BitSet chunkMap, StreamingDocument video) {
		byte d = 0, t = 0, q = 0;

		boolean possible = true;
		while (possible) {
			if (d + 1 < video.getSpatialLevels() && coversLayer(chunkMap, video.getLayerMask((byte) (d + 1), t, q))) {
				d++;
				continue;
			} else if (t + 1 < video.getTemporalLevels()
			        && coversLayer(chunkMap, video.getLayerMask(d, (byte) (t + 1), q))) {
				t++;
				continue;
			} else if (q + 1 < video.getSnrLevels() && coversLayer(chunkMap, video.getLayerMask(d, t, (byte) (q + 1)))) {
				q++;
				continue;
			} else {
				possible = false;
			}
		}
		byte[] result = { d, t, q };
		return result;
	}

	/**
	 * 
	 * @param chunkMap
	 *            the block map of a video chunk
	 * @param layerMask
	 *            the block map of a SVC layer for some video
	 * @return Whether the given chunk has all the blocks needed by the given
	 *         layer mask.
	 */
	public static boolean coversLayer(BitSet chunkMap, BitSet layerMask) {
		chunkMap.and(layerMask);
		return chunkMap.equals(layerMask);
	}

	/**
	 * 
	 * @param a
	 * @param b
	 * @return Null if layer a and b are not comparable, 1 if a is above b, -1
	 *         if a is below b, 0 if a is equal b.
	 */
	public static Integer compareLayer(byte[] a, byte[] b) {
		if (a == null | b == null) {
			return null;
		}
		Integer result = null;
		if (b != null) {
			if (a[0] == b[0] && a[1] == b[1] && a[2] == b[2]) {
				result = 0;
			} else if (a[0] <= b[0] && a[1] <= b[1] && a[2] <= b[2]) {
				result = -1;
			} else if (a[0] >= b[0] && a[1] >= b[1] && a[2] >= b[2]) {
				result = 1;
			}
		}
		return result;
	}
}
