package de.tudarmstadt.maki.simonstrator.overlay.streaming.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

/**
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class StreamingDataBuffer implements StreamingDataInterface {

	protected final static boolean DEBUG = false;

	protected int BUFFER_SIZE = 1024 * 1024 * 20;

	private final NorthboundInterface north = new Northbound();

	private final SouthboundInterface south = new Southbound();

	protected final Buffer buffer = new Buffer();

	protected final ModelInterface model;

	public StreamingDataBuffer(ModelInterface model) {
		this.model = model;
	}

	/**
	 * 
	 */
	public StreamingDataBuffer(ModelInterface model, int bufferSize) {
		this.model = model;
		this.BUFFER_SIZE = bufferSize;
	}

	@Override
	public NorthboundInterface getNorthboundInterface() {
		return north;
	}

	@Override
	public SouthboundInterface getSouthboundInterface() {
		return south;
	}

	/**
	 * 
	 * @author bjoern
	 * 
	 */
	private class Southbound implements SouthboundInterface {

		@Override
		public void receivedBlock(int blocknum, byte[] data) {
			buffer.addBlock(blocknum, data);
		}

		@Override
		public byte[] getDataForBlock(int blocknum) {
			return buffer.getBlock(blocknum);
		}

		@Override
		public boolean hasDataForBlock(int blocknum) {
			return buffer.isInBuffer(blocknum);
		}

	}

	/**
	 * 
	 * @author bjoern
	 * 
	 */
	private class Northbound implements NorthboundInterface {

		private int lastBlock = 0;

		@Override
		public InputStream getInputStream() {
			return buffer;
		}

		@Override
		public void newBlock(byte[] data) {
			buffer.addBlock(lastBlock, data);
			lastBlock++;
		}

	}

	/**
	 * Internal buffer. FIXME: check for concurrency-issues.
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private class Buffer extends InputStream {

		private final byte[] BUF_RANDOM = new byte[BUFFER_SIZE];

		private final byte[] BUF_SORTED = new byte[BUFFER_SIZE];

		/*
		 * Variables for the random buffer
		 */

		private final Map<Integer, Integer> blockLengths = new HashMap<Integer, Integer>();

		private final Map<Integer, Integer> blockPointers = new HashMap<Integer, Integer>();

		private int firstBlockInBuffer = -1;

		private int lastBlockInBuffer = -1;

		/**
		 * Pointer to the first 0-byte in the buffer
		 */
		private int bufPointer;

		/*
		 * Variables for the sorted buffer
		 */

		private int lastBlockInSortedBuffer = -1;

		private int bufSortedPointer = 0;

		/**
		 * Pointing to the first unread bit
		 */
		private int readMarker = 0;

		/**
		 * Pointing to the last bit that can be read from the buffer, where all
		 * intermediate bits are set.
		 */
		private int readEndMarker = 0;

		public void addBlock(int blocknum, byte[] data) {

			if (DEBUG) {
				System.out.println("StreamingDataBuffer: Writing block " + blocknum + " with " + data[0] + ","
				        + data[1] + " and length " + data.length);
			}

			// synchronized (lock) {
			if (lastBlockInBuffer == -1 && firstBlockInBuffer == -1) {
				lastBlockInBuffer = blocknum;
				firstBlockInBuffer = blocknum;
			}
			if (lastBlockInSortedBuffer == -1) {
				lastBlockInSortedBuffer = model.getFirstRequiredBlock() - 1;
			}

			if (blocknum > lastBlockInBuffer) {
				lastBlockInBuffer = blocknum;
			}
			if (blocknum < firstBlockInBuffer) {
				firstBlockInBuffer = blocknum;
			}

			/*
			 * Add data, check, if another block(s) is overwritten
			 */
			int oldBufPointer = bufPointer;
			boolean wrapped = false;
			// Special case: wrap-around
			if (bufPointer + data.length > BUF_RANDOM.length) {
				int wrapAround = bufPointer + data.length - BUF_RANDOM.length;
				System.arraycopy(data, 0, BUF_RANDOM, bufPointer, data.length - wrapAround);
				System.arraycopy(data, 0, BUF_RANDOM, 0, wrapAround);
				bufPointer = wrapAround;
				wrapped = true;
			} else {
				System.arraycopy(data, 0, BUF_RANDOM, bufPointer, data.length);
				bufPointer += data.length;
			}

			/*
			 * Delete references for blocks that are now overwritten
			 */
			Iterator<Entry<Integer, Integer>> it = blockPointers.entrySet().iterator();
			while (it.hasNext()) {
				Entry<Integer, Integer> entry = it.next();
				if (wrapped) {
					if (entry.getValue() >= oldBufPointer || entry.getValue() <= bufPointer) {
						blockLengths.remove(entry.getKey());
						it.remove();
					}
				} else {
					if (entry.getValue() >= oldBufPointer && entry.getValue() <= bufPointer) {
						blockLengths.remove(entry.getKey());
						it.remove();
					}
				}
			}
			blockLengths.put(blocknum, data.length);
			blockPointers.put(blocknum, oldBufPointer);

			/*
			 * Check from the beginning of the buffer, if and how many chunks
			 * are available now and sort them into the sorted buffer
			 */
			int nextBlock = lastBlockInSortedBuffer + 1;
			for (int i = nextBlock;; i++) {
				if (model.isBlockRequired(i)) {
					if (blockPointers.containsKey(i)) {
						/*
						 * Move block to other buffer
						 */
						if (DEBUG) {
							System.out.println("StreamingDataBuffer: Moving Block " + i + " to the Northbound Buffer");
						}
						byte[] toMove = getBlock(i);
						// Special case: wrap-around
						if (bufSortedPointer + toMove.length > BUF_SORTED.length) {
							int wrapAround = bufSortedPointer + toMove.length - BUF_SORTED.length;
							System.arraycopy(toMove, 0, BUF_SORTED, bufSortedPointer, toMove.length - wrapAround);
							System.arraycopy(toMove, 0, BUF_SORTED, 0, wrapAround);
							bufSortedPointer = wrapAround;
						} else {
							System.arraycopy(toMove, 0, BUF_SORTED, bufSortedPointer, toMove.length);
							bufSortedPointer += toMove.length;
						}
						lastBlockInSortedBuffer = i;
						readEndMarker = bufSortedPointer;
					} else {
						break;
					}
				}
			}
			// }
		}

		public boolean isInBuffer(int blocknum) {
			// synchronized (lock) {
			if (blocknum < firstBlockInBuffer || blocknum > lastBlockInBuffer || !blockPointers.containsKey(blocknum)) {
				System.err.println("Block " + blocknum + " is no longer available in the buffer.");
				return false;
			}
			return true;
			// }
		}

		public byte[] getBlock(int blocknum) {
			// synchronized (lock) {
			if (!isInBuffer(blocknum)) {
				System.err.println("Block is no longer in the buffer!");
				return null;
			}
			int blocklength = blockLengths.get(blocknum);
			int blockpointer = blockPointers.get(blocknum);
			byte[] block = new byte[blocklength];
			// Special case: wrap-around
			if (blockpointer + blocklength > BUF_RANDOM.length) {
				int wrapAround = blockpointer + blocklength - BUF_RANDOM.length;
				System.arraycopy(BUF_RANDOM, blockpointer, block, 0, blocklength - wrapAround);
				System.arraycopy(BUF_RANDOM, 0, block, 0, wrapAround);
			} else {
				System.arraycopy(BUF_RANDOM, blockpointer, block, 0, blocklength);
			}
			// System.out.println("Data for block " + blocknum + ": "
			// + block.length + " bytes.");
			return block;
			// }
		}

		@Override
		public int available() {
			// synchronized (lock) {
			if (readEndMarker >= readMarker) {
				return readEndMarker - readMarker;
			} else {
				// wrap-around
				return BUF_SORTED.length - readMarker + readEndMarker;
			}
			// }
		}

		@Override
		public int read() {
			throw new AssertionError("DO NOT USE!");
		}

		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			throw new AssertionError("DO NOT USE!");
		}

		@Override
		public int read(byte[] data) {
			if (DEBUG) {
				System.out.println("StreamingDataBuffer: Reading data[" + data.length + "] from the buffer, content: "
				        + data[0] + "," + data[1]);
			}
			// synchronized (lock) {
			if (available() < data.length) {
				throw new AssertionError("Check available bytes first!");
			}
			// Wrap-around
			if (readEndMarker >= readMarker) {
				System.arraycopy(BUF_SORTED, readMarker, data, 0, data.length);
				readMarker += data.length;
			} else {
				int wrapAround = readMarker + data.length - BUF_SORTED.length;
				System.arraycopy(BUF_SORTED, readMarker, data, 0, data.length - wrapAround);
				System.arraycopy(BUF_SORTED, 0, data, 0, wrapAround);
				readMarker = wrapAround;
			}
			return data.length;
		}
		// }

	}

	/**
	 * Testing
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		StreamingDataBuffer sdb = new StreamingDataBuffer(new ModelInterface() {
			@Override
			public boolean isBlockRequired(int blocknum) {
				return true;
			}

			@Override
			public int getFirstRequiredBlock() {
				return 0;
			}
		}, 80);

		// Packets
		byte[] p0 = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
		byte[] p1 = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 };
		byte[] p2 = { 2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
		byte[] p3 = { 3, 3, 3, 3, 3, 3, 3, 3, 3, 3 };
		byte[] p4 = { 4, 4, 4, 4, 4, 4, 4, 4, 4, 4 };

		sdb.getSouthboundInterface().receivedBlock(0, p0);
		sdb.getSouthboundInterface().receivedBlock(2, p2);
		try {
			int avail = sdb.getNorthboundInterface().getInputStream().available();
			if (avail > 0) {
				byte[] read = new byte[avail];
				sdb.getNorthboundInterface().getInputStream().read(read);
				System.out.println("I read: " + Arrays.toString(read));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sdb.getSouthboundInterface().receivedBlock(3, p3);
		sdb.getSouthboundInterface().receivedBlock(1, p1);
		sdb.getSouthboundInterface().receivedBlock(4, p4);

		try {
			int avail = sdb.getNorthboundInterface().getInputStream().available();
			if (avail > 0) {
				byte[] read = new byte[avail];
				sdb.getNorthboundInterface().getInputStream().read(read);
				System.out.println("I read: " + Arrays.toString(read));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
