/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;

public class SchedulingStrategyReverseZigZag implements SchedulingStrategy {

	/** The node. */
	private TransitNode node;

	/**
	 * Instantiates a new transit piece picking strategy linear.
	 * 
	 * @param node
	 *            the node
	 */
	public SchedulingStrategyReverseZigZag(TransitNode node) {
		this.node = node;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies
	 * .piecepicking
	 * .requesting.TransitPiecePickingRequestStrategy#selectPiecesForRequest
	 * (java.util.BitSet, int)
	 */
	public BitSet selectPiecesForRequest(BitSet match, int maxBlocks, boolean reverseOrderForTradingGain) {

		assert reverseOrderForTradingGain == true : "This module is called ReverseZigZag, only works with reverseOrderForTradingGain = true";
		
		int lastSetBit = match.length();
		final int CHUNK_SIZE = node.BLOCKS_PER_CHUNK;
		final int HIGHEST_LAYER = CHUNK_SIZE - 1;
		
		/* Find the latest highest piece. */
		int lastBifOffset = 0;
		while( (lastSetBit - lastBifOffset) % CHUNK_SIZE !=  HIGHEST_LAYER) {
			lastBifOffset++;
		}
		
		final int lastConsideredBit = lastSetBit - lastBifOffset;

		assert lastConsideredBit % CHUNK_SIZE == HIGHEST_LAYER;
		
		int set = 0;
		BitSet result = new BitSet();
		
		/* Phase 1: static */
		assert CHUNK_SIZE == 4;
		result.set(lastConsideredBit, match.get(lastConsideredBit));
		
		result.set(lastConsideredBit-1, match.get(lastConsideredBit-1));
		result.set(lastConsideredBit-4, match.get(lastConsideredBit-4));
		
		result.set(lastConsideredBit-2, match.get(lastConsideredBit-2));
		result.set(lastConsideredBit-5, match.get(lastConsideredBit-5));
		result.set(lastConsideredBit-8, match.get(lastConsideredBit-8));
		set = result.cardinality();
		
		/* Phase 2: dynamic */
		int lastBLBit = lastConsideredBit - CHUNK_SIZE + 1; // BL.
		while( set < maxBlocks && lastBLBit > 0 ) {
			for (int i = 0; i < CHUNK_SIZE && set < maxBlocks; i++) {
				int bitIndex = lastBLBit - i * (CHUNK_SIZE - 1);
				if( bitIndex > 0 && match.get(bitIndex) ) {
					set++;
					result.set(bitIndex);
				}				
			}
			lastBLBit -= CHUNK_SIZE;
		}

		return result;
	}

	@Override
	public BitSet selectNextBlocks(int bufferBegin, int offsetGlobal, int blocksToSchedule) {
		
		assert bufferBegin >= 0;
		
		final int CHUNK_SIZE = node.BLOCKS_PER_CHUNK;
		int newestBlockNum = node.getScheduler().getRequestManager().getNewestSeenBlockNumber();
		if( newestBlockNum < offsetGlobal ) {
			newestBlockNum = blocksToSchedule * CHUNK_SIZE - 1 + offsetGlobal;
		}
		
		/* SERVING MODE 2: HIGHEST LAYER ONLY! */
		final BitSet missing = new BitSet();		
		int lastConsideredBit = newestBlockNum;
		
		/* Start with first highest layer block. */
		if( lastConsideredBit % CHUNK_SIZE != CHUNK_SIZE - 1 ) {
			lastConsideredBit -= (lastConsideredBit % CHUNK_SIZE) + 1;
		}
		
		
		/* Phase 1: static */
		assert CHUNK_SIZE == 4;
		if( lastConsideredBit - offsetGlobal > 8 ) {
			missing.set(lastConsideredBit - offsetGlobal, !node.getVideo().haveBlock(lastConsideredBit));
			
			missing.set(lastConsideredBit - 1 - offsetGlobal, !node.getVideo().haveBlock(lastConsideredBit-1));
			missing.set(lastConsideredBit - 4 - offsetGlobal, !node.getVideo().haveBlock(lastConsideredBit-4));
			
			missing.set(lastConsideredBit - 2 - offsetGlobal, !node.getVideo().haveBlock(lastConsideredBit-2));
			missing.set(lastConsideredBit - 5 - offsetGlobal, !node.getVideo().haveBlock(lastConsideredBit-5));
			missing.set(lastConsideredBit - 8 - offsetGlobal, !node.getVideo().haveBlock(lastConsideredBit-8));
		}
		int set = missing.cardinality();
		
		/* Phase 2: dynamic */
		int lastBLBit = lastConsideredBit - CHUNK_SIZE + 1; // BL.
		while( set < blocksToSchedule && lastBLBit > 0 ) {
			for (int i = 0; i < CHUNK_SIZE && set < blocksToSchedule; i++) {
				int bitIndex = lastBLBit - i * (CHUNK_SIZE - 1);
				if( bitIndex - offsetGlobal > 0 && !node.getVideo().haveBlock(bitIndex) ) {
					set++;
					missing.set(bitIndex - offsetGlobal);
				}				
			}
			lastBLBit -= CHUNK_SIZE;
		}
		return missing;
		
	}

}
