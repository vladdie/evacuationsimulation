/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies;

import java.util.BitSet;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnectionListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.AbstractTransitFlowManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitFlowSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitFlowScheduleListener;

/**
 * Mainly for testing purposes and to minimize cross-effects when evaluating new
 * components, this FlowManager is the most basic implementation.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 15.06.2012
 */
public class TransitSimpleFlowManager extends AbstractTransitFlowManager {

	private final RepairPhase repairPhase;

	/**
	 * Very simple FlowManager
	 * 
	 * @param node
	 */
	public TransitSimpleFlowManager(TransitNode node) {
		super(node);
		repairPhase = new RepairPhase();
	}

	@Override
	protected void handleMissingFlow(BitSet blockMask) {
		if (getNode().getSettings().getParam(TransitParams.FLOW_MAX_INCOMING) > getIncomingFlow().getNumberOfFlows()) {
			repairPhase.handleMissingFlow(blockMask);
		}
	}

	@Override
	protected void processScheduleRequest(TransitFlowSchedule request, TransitConnection connection,
	        TransitReplyHandler replyTo) {
		/*
		 * Is the connection OPEN and the direction OUT?
		 */
		if (connection.getState() != ConnectionState.OPEN || connection.getDirection() != ConnectionDirection.OUT) {
			doDenyRequest(connection, replyTo);
			return;
		}

		/*
		 * Can we fulfill the schedule? At least partial?
		 */
		BitSet blockMaskAvailable = (BitSet) request.getBlockMask().clone();
		BitSet ownMask = null;
		if (getNode().isServer()) {
			ownMask = new BitSet();
			ownMask.set(0, blockMaskAvailable.length());
		} else {
			ownMask = getIncomingFlow().getHealthyFlow();
		}
		blockMaskAvailable.and(ownMask);
		if (!blockMaskAvailable.isEmpty()) {
			/*
			 * Do we have spare capacity?
			 */
			TransitFlowSchedule flowSchedule = new TransitFlowSchedule(blockMaskAvailable);
			if (getNode().getBandwidthManager().hasSpareBandwidthFor(flowSchedule, ConnectionDirection.OUT)) {
				doGrantRequest(flowSchedule, connection, replyTo);
			} else {
				/*
				 * compute the blocks we are able to send with our spare
				 * capacity and send a REPLY with the altered Schedule.
				 */
				BitSet blockMaskPossible = new BitSet();
				for (int i = blockMaskAvailable.nextSetBit(0); i >= 0; i = blockMaskAvailable.nextSetBit(i + 1)) {

					blockMaskPossible.set(i);
					TransitFlowSchedule scheduleOffer = new TransitFlowSchedule(blockMaskPossible);
					if (!getNode().getBandwidthManager().hasSpareBandwidthFor(scheduleOffer, ConnectionDirection.OUT)) {
						blockMaskPossible.set(i, false);
					}
				}
				if (blockMaskPossible.isEmpty()) {
					doDenyRequest(connection, replyTo);
					if (DEBUG) {
						System.out.println("TFM: Denied schedule, NO SPARE CAPACITY. " + connection.toString());
					}
				} else {
					TransitFlowSchedule possible = new TransitFlowSchedule(blockMaskPossible);
					doGrantRequest(possible, connection, replyTo);
					if (DEBUG) {
						System.out.println("TFM: Altered schedule, NO SPARE CAPACITY. " + connection.toString());
					}
				}
			}
		} else {
			if (DEBUG) {
				System.out.println("TFM: Denied schedule, NO BLOCKS AVAILABLE. " + connection.toString());
			}
			doDenyRequest(connection, replyTo);
		}
	}

	@Override
	protected void processScheduleOffer(TransitFlowSchedule offer, TransitConnection connection) {
		/*
		 * TODO use OFFERs? Add a Piggyback-Listener for this purpose.
		 */
	}

	/**
	 * This phase is used to negotiate new schedules if some blocks are missing.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 01.07.2012
	 */
	protected class RepairPhase implements TransitFlowScheduleListener, TransitConnectionListener {

		private final BitSet missing = new BitSet();

		private long lastTry = 0;

		private long wait = 0;

		public RepairPhase() {
			getNode().getNeighborhood().getConnectionManager().addConnectionListener(this);
		}

		public void handleMissingFlow(BitSet missingBlocks) {

			missing.clear();
			for (int i = missingBlocks.nextSetBit(0); i >= 0; i = missingBlocks.nextSetBit(i + 1)) {
				missing.set(i);
				if (missing.cardinality() >= getNode().getSettings().getParam(TransitParams.FLOW_MAX_PER_CONNECTION)) {
					break;
				}
			}
			// missing = missingBlocks;

			if (lastTry + wait > Time.getCurrentTime()) {
				return;
			}

			List<TransitConnection> candidates = getNode().getNeighborhood().getConnectionManager()
			        .getConnections(ConnectionDirection.IN, ConnectionState.OPEN, ScheduleState.INACTIVE, null);
			TransitConnection chosen = null;

			if (candidates.isEmpty()) {
				candidates = getNode().getNeighborhood().getConnectionManager()
				        .getConnections(ConnectionDirection.IN, ConnectionState.OPEN, ScheduleState.CANCELED, null);

				long oldest = Time.getCurrentTime();
				for (TransitConnection candidate : candidates) {
					if (candidate.getCurrentRTTEstimation() < getNode().getSettings()
					        .getTime(TransitTimes.FLOW_RTT_MAX)
					        && candidate.getLastScheduleStateChangeTimestamp() < oldest) {
						oldest = candidate.getLastScheduleStateChangeTimestamp();
						chosen = candidate;
					}
				}
				if (chosen != null) {
					chosen.setScheduleState(ScheduleState.INACTIVE);
				}
			} else {
				for (TransitConnection transitConnection : candidates) {
					if (transitConnection.getCurrentRTTEstimation() > getNode().getSettings().getTime(
					        TransitTimes.FLOW_RTT_MAX)) {
						continue;
					}
					chosen = transitConnection;
					break;
				}
			}

			if (chosen == null) {
				if (DEBUG) {
					System.err
							.println("TFM: No Connection in the Repair-Phase! "
					                + getNode().getNeighborhood().getConnectionManager().getAllInAndOutConnections()
					                        .toString());
				}
				return;
			}

			/*
			 * FIXME this is just a very dumb way to do this, we should add some
			 * more logic here. Decision based on offers would be a good idea...
			 */
			TransitFlowSchedule request = new TransitFlowSchedule(missing);
			chosen.addScheduleListener(this);
			lastTry = Time.getCurrentTime();
			wait = chosen.getCurrentRTTEstimation();
			doRequest(request, chosen);
		}

		@Override
		public void onConnectionStateChanged(TransitConnection connection, ConnectionState oldState,
		        ConnectionState newState) {
			// if (connection.getDirection() == ConnectionDirection.IN) {
			// if (newState == ConnectionState.OPEN
			// && connection.getScheduleState() == ScheduleState.INACTIVE) {
			// if (lastTry + wait > Simulator.getCurrentTime()) {
			// return;
			// }
			// if (!missing.isEmpty()) {
			// if (connection.getCurrentRTTEstimation() < getNode()
			// .getSettings().getTime(
			// TransitTimes.FLOW_RTT_MAX)) {
			// TransitFlowSchedule request = new TransitFlowSchedule(
			// missing);
			// connection.addScheduleListener(this);
			// lastTry = Simulator.getCurrentTime();
			// wait = connection.getCurrentRTTEstimation();
			// doRequest(request, connection);
			// }
			// }
			// }
			// }
		}

		@Override
		public boolean onScheduleChanged(TransitConnection connection, TransitFlowSchedule oldSchedule,
		        TransitFlowSchedule newSchedule) {
			return true;
		}

		@Override
		public boolean onScheduleStateChanged(TransitConnection connection, ScheduleState oldState,
		        ScheduleState newState) {
			if (newState == ScheduleState.ACTIVE) {
				missing.andNot(connection.getSchedule().getBlockMask());
				lastTry = 0;
				wait = 0;
				return false;
			}
			if (newState == ScheduleState.CANCELED) {
				lastTry = 0;
				wait = 0;
				return false;
			}
			return true;
		}
	}
}
