/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;

public class SchedulingStrategyLinearPlayback implements SchedulingStrategy {

	private TransitNode node;

	/**
	 * Instantiates a new scheduling strategy linear playback.
	 *
	 * @param node the node
	 */
	public SchedulingStrategyLinearPlayback(TransitNode node) {
		this.node = node;
	}
	
	/* (non-Javadoc)
	 * @see de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling.SchedulingStrategy#selectNextBlocks(int, int)
	 */
	@Override
	public BitSet selectNextBlocks(int bufferBegin, int offset, int blocksToSchedule) {
		
		/* SERVING MODE 0: PLAYBACK! */
		BitSet missing = new BitSet();
		
		int newestBlockNum = node.getScheduler().getRequestManager().getNewestSeenBlockNumber();
		if( newestBlockNum < bufferBegin ) {
			newestBlockNum = blocksToSchedule * node.BLOCKS_PER_CHUNK - 1;
		}
	
		int count = 0;
		boolean started = false;
		for (int chunk = bufferBegin; chunk < bufferBegin + blocksToSchedule; chunk++) {
			if (!node.getVideo().layerIsCoveredByChunkBlocks(chunk,
					node.getLayer())) {
				if (!started) {
					started = true;
				}
				BitSet blocks = node.getVideo().getLayerMask(node.getLayer());
				for (int i = blocks.nextSetBit(0); i >= 0; i = blocks
						.nextSetBit(i + 1)) {
					int blocknum = chunk * node.BLOCKS_PER_CHUNK + i;
					if (!node.getVideo().haveBlock(blocknum)) {
						missing.set(count * node.BLOCKS_PER_CHUNK + i);
					}
				}
			}
			if (started) {
				count++;
			}
		}
		
		return missing;
	}

}
