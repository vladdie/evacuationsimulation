/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking;

import java.util.BitSet;

public class PiecePickingHelper {
	
	/**
	 * Gets the base layer mask.
	 *
	 * @param chunkSize the chunk size
	 * @param bitSetSize the bit set size
	 * @return the base layer mask
	 */
	public static BitSet getBaseLayerMask(int chunkSize, int bitSetSize) {
		
		BitSet result = new BitSet(bitSetSize);
		
		for (int i = 0; i < bitSetSize; i += chunkSize) {
			result.set(i);
		}
		
		return result;		
	}
	
}
