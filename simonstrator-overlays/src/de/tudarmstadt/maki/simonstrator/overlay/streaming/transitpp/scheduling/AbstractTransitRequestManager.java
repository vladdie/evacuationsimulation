/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling;

import java.util.BitSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageCallback;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.BufferInterface.PlaybackState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.PeriodicPiggybackListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.AckBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SendBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SendRequestMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitRequestMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitRequestMessage.RequestMsgType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.RequestState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnectionListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact.ContactType;

/**
 * Base class for a {@link TransitRequestManager}.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public abstract class AbstractTransitRequestManager implements
		TransitRequestManager, TransitConnectionListener {

	protected final static boolean DEBUG = false;

	private final TransitNode node;

	/**
	 * Requests issued by this node where we wait for packets
	 */
	private final OutgoingRequests outgoingRequests;

	/**
	 * Requests issued by other nodes where we have to send packets
	 */
	private final IncomingRequests incomingRequests;

	/**
	 * 
	 * @param node
	 */
	public AbstractTransitRequestManager(TransitNode node) {
		this.node = node;
		this.outgoingRequests = new OutgoingRequests();
		this.incomingRequests = new IncomingRequests();
		node.getMessageHandler().addPiggybackListener(
				new BuffermapSender(node.getSettings().getTime(
						TransitTimes.BUFFERMAP_EXCHANGE_INTERVAL)));
		node.getNeighborhood().getConnectionManager()
				.addConnectionListener(this);
	}

	public TransitNode getNode() {
		return node;
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		switch (peerStatus) {
		case PRESENT:
		case TO_JOIN:
			break;

		case ABSENT:
			break;

		default:
			throw new AssertionError("Unknown PeerStatus.");
		}
	}

	/**
	 * 
	 * @param connection
	 * @param blockOffset
	 * @param mask
	 */
	protected void doRequest(TransitConnection connection,
			TransitRequestSchedule request) {
		SendRequestMessage requestMsg = new SendRequestMessage(
				node.getLocalOverlayContact(), RequestMsgType.REQUEST, request);
		node.getMessageHandler().send(requestMsg, connection.getEndpoint());
		outgoingRequests.addRequest(connection, request);
	}

	@Override
	public void cancelRequest(int blocknum) {
		outgoingRequests.cancelRequest(blocknum);
	}

	/**
	 * 
	 * @param connection
	 * @param blocknum
	 */
	protected void doSendBlock(TransitConnection connection, int blocknum) {
		assert node.getVideo().haveBlock(blocknum);
		SendBlockMessage blockMsg = new SendBlockMessage(
				node.getLocalOverlayContact(), blocknum, node.getVideo()
						.getBlockSize(blocknum), false);
		connection.blockTransferred(blocknum, blockMsg.getSize(), false);
		node.getMessageHandler().sendAndWait(blockMsg,
				connection.getEndpoint(), incomingRequests);
	}

	@Override
	public void receivedRequestedBlock(TransitBlockMessage message,
			TransitReplyHandler replyTo, boolean isDuplicate) {
		TransitConnection connection = node.getNeighborhood()
				.getConnectionManager()
				.getConnection(message.getSenderContact());
		outgoingRequests.receivedRequestedBlock(connection, message);
		replyTo.reply(new AckBlockMessage(node.getLocalOverlayContact(),
				message.getBlockNumber()));
	}

	@Override
	public void receivedRequestMessage(TransitRequestMessage message,
			TransitReplyHandler replyTo) {

		switch (message.getType()) {
		case OFFER:
			receivedBufferMap(message);
			break;

		case REQUEST:
			TransitRequestSchedule schedule = canFulfill(message);
			incomingRequests.addToQueue(message.getSenderContact(), schedule);
			break;

		case DENY_CAPACITY:
			if (DEBUG) {
				System.err.println(Time.getFormattedTime(Time
						.getCurrentTime())
						+ " "
						+ node.getLocalOverlayContact()
						+ " REQUEST DENIED by "
						+ message.getSenderContact()
						+ " because of capacity: "
						+ message.getRequestSchedule());
			}
			outgoingRequests.receivedDeny(message);
			break;

		default:
			throw new AssertionError("Unknown type " + message.toString());
		}
	}

	/**
	 * Returns the request-Info for the given block (outgoing requests, i.e.
	 * blocks that were requested by this node)
	 * 
	 * @param blocknum
	 * @return
	 */
	protected RequestInfo getRequestInfo(int blocknum) {
		return outgoingRequests.openRequests.get(Integer.valueOf(blocknum));
	}

	protected int getNumberOfOpenRequests() {
		return outgoingRequests.openRequests.size();
	}

	/**
	 * Called when a REQUEST arrived. Has to return the appropriate Schedule.
	 * 
	 * @param request
	 * @return
	 */
	protected TransitRequestSchedule canFulfill(TransitRequestMessage request) {
		assert hasAllBlocks(request.getRequestSchedule());
		return request.getRequestSchedule();
	}

	/**
	 * Returns all connections that have the requested block
	 * 
	 * @param block
	 * @return
	 */
	protected List<TransitConnection> whoHas(int block) {
		List<TransitConnection> cons = node
				.getNeighborhood()
				.getConnectionManager()
				.getConnections(ConnectionDirection.IN, ConnectionState.OPEN,
						null, RequestState.INACTIVE);
		List<TransitConnection> available = new LinkedList<TransitConnection>();
		for (TransitConnection connection : cons) {
			int blkIdx = block - connection.getBufferMap().getBlockOffset();
			if (blkIdx >= 0
					&& connection.getBufferMap().getBlockMap().get(blkIdx)) {
				available.add(connection);
			}
		}
		return available;
	}

	/**
	 * Returns a matching request schedule or null.
	 * 
	 * @param bufferMap
	 *            known buffer map
	 * @return request that we will send
	 */
	protected BitSet getMatch(TransitConnection connection,
			TransitRequestSchedule bufferMap, int requestedBlocksOffset,
			BitSet requestedBlocks) {

		/*
		 * remove already retrieved blocks and blocks that were just recently
		 * requested
		 */
		BitSet requested = (BitSet) (requestedBlocks.clone());
		for (int i = requestedBlocks.nextSetBit(0); i >= 0; i = requestedBlocks
				.nextSetBit(i + 1)) {
			assert !node.getVideo().haveBlock(requestedBlocksOffset + i);
			RequestInfo info = getRequestInfo(requestedBlocksOffset + i);
			if (info != null && !info.allowRequest(connection)) {
				requested.set(i, false);
			}
		}

		BitSet match = new BitSet();
		if (requested.isEmpty()) {
			return match;
		}

		if (bufferMap.getBlockOffset() <= requestedBlocksOffset) {
			int diff = requestedBlocksOffset - bufferMap.getBlockOffset();
			if (diff < bufferMap.getBlockMap().length()) {
				match = (BitSet) (bufferMap.getBlockMap().get(diff,
						bufferMap.getBlockMap().size()).clone());
				match.and(requested);
			}
		} else {
			int diff = bufferMap.getBlockOffset() - requestedBlocksOffset;
			if (diff >= requestedBlocks.length()) {
				// assuming that all past blocks are available ONLY on sources
				// if (connection.getEndpoint().getType() == ContactType.SOURCE)
				// {
				// match = requested;
				// }
				/*
				 * For this to work, buffer maps must be centered around the
				 * playback position of the requesting node, which is the case
				 * in transit.
				 */
			} else {
				for (int i = requested.nextSetBit(0); i >= 0; i = requested
						.nextSetBit(i + 1)) {
					match.set(i, bufferMap.getBlockMap().get(diff + i));
				}
			}
		}

		int maxBlocks = 0;
		if (connection.getEndpoint().getType() == ContactType.SOURCE) {
			maxBlocks = node.getSettings().getParam(
					TransitParams.REQUEST_MAX_BLOCKS_PER_CONNECTION_SOURCE);
		} else {
			maxBlocks = node.getSettings().getParam(
					TransitParams.REQUEST_MAX_BLOCKS_PER_CONNECTION);
		}
		int set = 0;
		for (int i = match.nextSetBit(0); i >= 0; i = match.nextSetBit(i + 1)) {
			set++;
			if (set >= maxBlocks) {
				return match.get(0, i + 1);
			}
		}
		return match;
	}

	/**
	 * true, if we have all blocks that are requested
	 * 
	 * @param request
	 * @return
	 */
	protected boolean hasAllBlocks(TransitRequestSchedule request) {
		int offset = request.getBlockOffset();
		BitSet requestMap = request.getBlockMap();
		// BitSet available = (BitSet) (node.getVideo().getBlockMap()
		// .get(offset, offset + requestMap.size()).clone());
		BitSet available = (BitSet) (node.getVideo().getBlockMap(offset,
				offset + requestMap.size()).clone());
		available.and(requestMap);
		return available.equals(requestMap);
	}

	/**
	 * Updates the BufferMap of a connection
	 * 
	 * @param message
	 */
	protected void receivedBufferMap(TransitRequestMessage message) {
		TransitConnection connection = node.getNeighborhood()
				.getConnectionManager()
				.getConnection(message.getSenderContact());
		if (connection != null) {
			connection.setBufferMap(message.getRequestSchedule());
		}
	}

	/**
	 * Has to return a BufferMap of our node
	 * 
	 * @return
	 */
	protected TransitRequestSchedule createBuffermap(TransitContact to) {

		/*
		 * Create the buffermap from the current position of the node that it is
		 * sent to - past is not needed!
		 */
		// int nodePos = getNode().getScheduler().getBufferBegin();
		int nodePos = to.getCurrentChunk();
		int size = Math.max(StreamingConfiguration.BUFFER_SIZE, node
				.getSettings().getParam(TransitParams.REQUEST_BUFFERMAP_SIZE));

		int blockOffset = nodePos * node.BLOCKS_PER_CHUNK;
		if (blockOffset < 0) {
			blockOffset = 0;
		}
		// BitSet buffermap = (BitSet) (node.getVideo().getBlockMap()
		// .get(blockOffset, blockOffset + node.BLOCKS_PER_CHUNK * size)
		// .clone());
		BitSet buffermap = node.getVideo().getBlockMap(blockOffset,
				size * node.BLOCKS_PER_CHUNK);
		return new TransitRequestSchedule(blockOffset, buffermap);
	}

	@Override
	public void onConnectionStateChanged(TransitConnection connection,
			ConnectionState oldState, ConnectionState newState) {
		if (newState == ConnectionState.CLOSED
				&& connection.getDirection() == ConnectionDirection.IN) {
			TransitRequestSchedule request = connection.getRequest();
			if (request != null) {
				BitSet requestMap = request.getBlockMap();
				int offset = request.getBlockOffset();
				for (int i = requestMap.nextSetBit(0); i >= 0; i = requestMap
						.nextSetBit(i + 1)) {
					RequestInfo reqInfo = outgoingRequests.openRequests
							.get(Integer.valueOf(offset + i));
					if (reqInfo != null) {
						reqInfo.deniedBy(connection.getEndpoint());
					}
				}
			}

		}
	}

	/**
	 * Manages requests that originate from our node
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	private class OutgoingRequests {

		public final Map<Integer, RequestInfo> openRequests;

		protected OutgoingRequests() {
			this.openRequests = new LinkedHashMap<Integer, AbstractTransitRequestManager.RequestInfo>();
		}

		public void addRequest(TransitConnection connection,
				TransitRequestSchedule request) {
			BitSet requestMap = request.getBlockMap();
			int offset = request.getBlockOffset();
			for (int i = requestMap.nextSetBit(0); i >= 0; i = requestMap
					.nextSetBit(i + 1)) {
				assert !getNode().getVideo().haveBlock(offset + i);
				RequestInfo reqInfo = openRequests.get(Integer.valueOf(offset
						+ i));
				if (reqInfo == null) {
					reqInfo = new RequestInfo(offset + i);
					openRequests.put(Integer.valueOf(offset + i), reqInfo);
				} else {
					// if (DEBUG) {
					// boolean wasOnline = GlobalOracle
					// .getHostForNetID(
					// reqInfo.lastRequestConnection
					// .getEndpoint().getTransInfo()
					// .getNetId()).getNetLayer()
					// .isOnline();
					// System.err.println(Time.getFormattedTime(Time
					// .getCurrentTime())
					// + "RE-REQUESTING from "
					// + connection.getEndpoint().toString()
					// + " "
					// + reqInfo.toString()
					// + " --> "
					// + (wasOnline ? "WAS ONLINE" : "WAS OFFLINE"));
					// }
				}
				assert !reqInfo.isFinished();
				reqInfo.requested(connection, request);
			}

			connection.setRequestState(RequestState.ACTIVE);
			connection.setRequest(request);
		}

		public void cancelRequest(int blocknum) {
			RequestInfo info = openRequests.remove(Integer.valueOf(blocknum));
			if (info != null && !info.isFinished()) {
				info.finished();
				if (DEBUG) {
					System.err.println("TRM: "
							+ getNode().getLocalOverlayContact().toString()
							+ " CANCELING REQUEST due to incoming FLOW block: "
							+ blocknum);
				}
			}
		}

		public void receivedDeny(TransitRequestMessage message) {
			BitSet requestMap = message.getRequestSchedule().getBlockMap();
			int offset = message.getRequestSchedule().getBlockOffset();

			TransitConnection connection = getNode().getNeighborhood()
					.getConnectionManager()
					.getConnection(message.getSenderContact());

			connection.setRequestState(RequestState.CANCELED);
			connection.setRequest(null);

			for (int i = requestMap.nextSetBit(0); i >= 0; i = requestMap
					.nextSetBit(i + 1)) {
				RequestInfo reqInfo = openRequests.get(Integer.valueOf(offset
						+ i));
				if (reqInfo != null) {
					reqInfo.deniedBy(message.getSenderContact());
				}
			}
		}

		public void receivedRequestedBlock(TransitConnection connection,
				TransitBlockMessage block) {
			if (connection != null) {
				connection.blockTransferred(block.getBlockNumber(),
						block.getSize(), false);
			}
			RequestInfo info = openRequests.remove(Integer.valueOf(block
					.getBlockNumber()));
			if (info == null) {
				if (DEBUG) {
					System.err.println(getNode().getLocalOverlayContact()
							+ " received not-requested or duplicate block "
							+ block.getBlockNumber());
				}
			} else {
				info.finished();
			}

			/*
			 * Enlarge Timeouts for other blocks that are requested via the same
			 * connection.
			 */
			if (connection != null) {
				TransitRequestSchedule request = connection.getRequest();
				if (request != null) {
					BitSet stillMissing = request.getBlockMap();
					assert !stillMissing.isEmpty();
					for (int i = stillMissing.nextSetBit(0); i >= 0; i = stillMissing
							.nextSetBit(i + 1)) {
						RequestInfo reqInfo = openRequests.get(Integer
								.valueOf(request.getBlockOffset() + i));
						if (reqInfo != null) {
							reqInfo.updateLastRequest();
						}
					}
				}
			}

		}

	}

	/**
	 * Manages requests that originate from other nodes and are to be fulfilled
	 * by our node.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	private class IncomingRequests implements TransMessageCallback {

		protected IncomingRequests() {
			//
		}

		public void addToQueue(TransitContact contact,
				TransitRequestSchedule request) {
			TransitConnection connection = getNode().getNeighborhood()
					.getConnectionManager().getConnection(contact);
			connection.setRequest(request);
			connection.setRequestState(RequestState.ACTIVE);
			new WaitToSend(connection);
		}

		@Override
		public void messageTimeoutOccured(int commId) {
			// not used
		}

		@Override
		public void receive(Message msg, TransInfo senderInfo, int commId) {
			// not interested in ACKs :)
		}

	}

	/**
	 * Just a queue-entry waiting to be sent
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 13.09.2012
	 */
	private class WaitToSend implements EventHandler {

		private final TransitConnection connection;

		private final BitSet requestMap;

		private final int offset;

		private int sent = 0;

		public WaitToSend(TransitConnection connection) {
			this.connection = connection;
			this.requestMap = connection.getRequest().getBlockMap();
			this.offset = connection.getRequest().getBlockOffset();
			sendBlock();
		}

		private void sendBlock() {
			if (!getNode().isPresent()) {
				return;
			}

			if (connection.getRequest() == null) {
				return;
			}

			int i = requestMap.nextSetBit(0);
			if (i >= 0 && connection.getState() == ConnectionState.OPEN) {
				/*
				 * RTTs and out
				 */
				if (sent == 0
						&& getNode().getBandwidthManager().getLoad(
						ConnectionDirection.OUT) > 0.8) {
					// if
					// (!getNode().getBandwidthManager().hasSpareBandwidthFor(
					// offset + i, ConnectionDirection.OUT)) {
					TransitRequestMessage deny = new SendRequestMessage(
							getNode().getLocalOverlayContact(),
							RequestMsgType.DENY_CAPACITY,
							connection.getRequest());
					getNode().getMessageHandler().send(deny,
							connection.getEndpoint());
					connection.setRequest(null);
					connection.setRequestState(RequestState.CANCELED);
				} else {
					doSendBlock(connection, offset + i);
					sent++;
					requestMap.set(i, false);
					long delay = connection.getCurrentRTTEstimation() / 4;
					if (!requestMap.isEmpty()) {
						Event.scheduleWithDelay(delay, this, null, 0);
					} else {
						connection.setRequest(null);
						connection.setRequestState(RequestState.INACTIVE);
					}
				}
			} else {
				connection.setRequest(null);
				connection.setRequestState(RequestState.INACTIVE);
			}
		}

		@Override
		public void eventOccurred(Object content, int type) {
			sendBlock();
		}

	}

	/**
	 * Information regarding a single requested block, be it outgoing or
	 * incoming.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	public class RequestInfo {

		public final int block;

		private final List<TransitContact> denied;

		private final List<TransitContact> currentlyRequesting;

		private final Map<TransitContact, TransitConnection> requestedViaConnections;

		private long lastRequest = Time.getCurrentTime();

		private TransitConnection lastRequestConnection;

		private boolean finished = false;

		private StringBuilder info;

		public RequestInfo(int block) {
			this.block = block;
			this.denied = new LinkedList<TransitContact>();
			this.currentlyRequesting = new LinkedList<TransitContact>();
			this.requestedViaConnections = new LinkedHashMap<TransitContact, TransitConnection>();
			if (DEBUG) {
				info = new StringBuilder();
			}
		}

		public void requested(TransitConnection connection,
				TransitRequestSchedule request) {
			if (DEBUG) {
				info.append("\t" + Time.getFormattedTime(Time.getCurrentTime())
						+ "Requesting via " + connection.toString() + "\n");
			}
			lastRequestConnection = connection;
			lastRequest = Time.getCurrentTime();
			currentlyRequesting.add(connection.getEndpoint());
			requestedViaConnections.put(connection.getEndpoint(), connection);
		}

		/**
		 * If a block arrived via a connection and this connection is
		 * responsible for other blocks as well, it will update the lastRequest
		 * for those other blocks to prevent duplicates.
		 */
		public void updateLastRequest() {
			lastRequest = Time.getCurrentTime();
			if (DEBUG) {
				info.append("\t"
 + Time.getFormattedTime(Time.getCurrentTime())
						+ " Updated last Request to " + lastRequest + "\n");
			}
		}

		/**
		 * Request has been denied
		 * 
		 * @param connection
		 */
		public void deniedBy(TransitContact contact) {
			this.denied.add(contact);
			this.currentlyRequesting.remove(contact);
			TransitConnection con = this.requestedViaConnections
					.remove(contact);
			if (DEBUG) {
				info.append("\t"
 + Time.getFormattedTime(Time.getCurrentTime())
						+ " Denied by " + contact.toString() + " on "
						+ con.toString() + "\n");
			}
		}

		public boolean allowRequest(TransitConnection connection) {
			/*
			 * After one second, allow a new request in every case!
			 */
			if (currentlyRequesting.isEmpty()) {
				// we currently do not query any contacts
				return true;
			} else if (denied.contains(connection.getEndpoint())) {
				return false;
			} else if (lastRequest + Time.SECOND < Time
					.getCurrentTime()
					|| lastRequestConnection.getState() != ConnectionState.OPEN) {
				// after one second without result, allow a request
				if (DEBUG) {
					info.append("\t"
							+ Time.getFormattedTime(Time
									.getCurrentTime()) + "Allowing on "
							+ connection.toString()
							+ ", because one second without answer passed.\n");
				}
				return true;
			} else {
				return false;
			}
		}

		public void finished() {
			if (!finished) {
				if (DEBUG) {
					info.append("\t"
							+ Time.getFormattedTime(Time
									.getCurrentTime()) + "Finished \n");
				}
				for (TransitConnection connection : requestedViaConnections
						.values()) {
					if (connection.getRequestState() == RequestState.ACTIVE) {
						TransitRequestSchedule request = connection
								.getRequest();
						int blockmapIdx = block - request.getBlockOffset();
						assert request.getBlockMap().get(blockmapIdx);
						request.getBlockMap().set(blockmapIdx, false);
						if (request.getBlockMap().cardinality() == 0) {
							/*
							 * Request is fully answered
							 */
							connection.setRequest(null);
							connection.setRequestState(RequestState.INACTIVE);
						}
					}
				}
				requestedViaConnections.clear();
				denied.clear();
				currentlyRequesting.clear();
				finished = true;
			}
		}

		@Override
		public String toString() {
			return block + " " + requestedViaConnections.toString() + " last: "
					+ Time.getFormattedTime(Time.getCurrentTime()
							- lastRequest) + " denied by " + denied.toString()
					+ (DEBUG ? "\n" + info.toString() : "");
		}

		public boolean isFinished() {
			return finished;
		}

	}

	/**
	 * Piggybacks our buffermap in specified intervals
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	public class BuffermapSender extends PeriodicPiggybackListener {

		public BuffermapSender(long interval) {
			super(interval);
		}

		@Override
		protected TransitMessage whatToPiggybackPeriodically(TransitContact to) {
			if (to.getType() != ContactType.CLIENT
					|| getNode().getScheduler().getPlaybackState() == PlaybackState.NONE) {
				return null;
			}
			TransitConnection co = getNode().getNeighborhood()
					.getConnectionManager().getConnection(to);
			if (getNode().getScheduler().getPlaybackState() != PlaybackState.NONE
					&& co != null && co.getState() == ConnectionState.OPEN) {
				return new SendRequestMessage(getNode()
						.getLocalOverlayContact(), RequestMsgType.OFFER,
						createBuffermap(to));
			}
			return null;
		}

	}

}
