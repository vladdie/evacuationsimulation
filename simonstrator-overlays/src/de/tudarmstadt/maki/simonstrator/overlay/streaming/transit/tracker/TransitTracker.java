/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.tracker;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Oracle;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.api.component.transport.ProtocolNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageListener;
import de.tudarmstadt.maki.simonstrator.api.component.transport.protocol.TCPMessageBased;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingDocument;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingOverlayTrackerInterface;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.JoinTrackerMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.JoinTrackerReplyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.OfferNeighborhoodMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.RequestNeighborhoodMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact.ContactType;

/**
 * This is a Tracker in the Transit Overlay. In its simplest form this is a
 * central server, but one might think about extending it towards a
 * DHT-approach. A peer might act as a tracker as well as a normal
 * {@link TransitNode} at the same time.
 * 
 * TODO for simplicity it is assumed that the tracker always knows the current
 * playback position of the source node. Lateron, the source will send updates
 * to the tracker announcing new chunks. These updates will then be used for the
 * latest chunk.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public class TransitTracker extends AbstractOverlayNode implements TransMessageListener,
        StreamingOverlayTrackerInterface {

	private TransitContact ownContact;

	private MessageBasedTransport transport;

	/**
	 * Global-Knowledge Source, remove this lateron as described above
	 */
	// private IPlayer sourcePlayer;

	/**
	 * Global knowledge source, remove this dependency lateron!
	 */
	private TransitNode sourceNode;

	private TransitTrackerRegistry trackerRegistry;

	private final int port;

	public TransitTracker(Host host, int port) {
		super(host);
		this.port = port;
	}

	@Override
	public void initialize() {

		/*
		 * Bind the first Network Interface
		 */
		NetInterface net = getHost().getNetworkComponent().getNetworkInterfaces().iterator().next();
		try {
			transport = getHost().getTransportComponent().getProtocol(TCPMessageBased.class, net.getLocalInetAddress(),
			        port);
		} catch (ProtocolNotAvailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ownContact = new TransitContact(getHost().getId(),
				transport.getTransInfo(),
				ContactType.TRACKER);

		ownContact.updateLayer(new byte[] { 0, 0, 0 });
		transport.setTransportMessageListener(this);

		trackerRegistry = new TransitTrackerRegistry();

		/*
		 * TODO go online in churn or action!
		 */
		setPeerStatus(PeerStatus.PRESENT);
	}

	@Override
	public TransitContact getLocalOverlayContact() {
		return ownContact;
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		// TODO Auto-generated method stub

	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// TODO Auto-generated method stub

	}

	@Override
	public void messageArrived(Message omsg, TransInfo sender, int commID) {
		TransitMessage msg = (TransitMessage) omsg;
		TransitMessage reply = null;
		if (msg instanceof JoinTrackerMessage) {
			reply = handleJoinTrackerMsg((JoinTrackerMessage) msg);
		} else if (msg instanceof RequestNeighborhoodMessage) {
			reply = handleUpdateTrackerMsg((RequestNeighborhoodMessage) msg);
			/*
			 * TODO add a timeout-mechnism that removes contacts after a given
			 * time of inactivity
			 */
		} else {
			throw new AssertionError("Unknown message type " + msg.getClass().getSimpleName());
		}
		if (reply != null) {
			transport.sendReply(reply, sender.getNetId(), sender.getPort(), commID);
		}
	}

	/**
	 * Node sends updated information to the tracker
	 * 
	 * @param msg
	 * @return
	 */
	protected TransitMessage handleUpdateTrackerMsg(RequestNeighborhoodMessage msg) {
		/*
		 * TODO check for layer change, if so, definitely send some contacts!
		 */
		// this will trigger an update of the contact info
		trackerRegistry.addOrUpdateContact(msg.getSenderContact());
		/*
		 * TODO send neighborhood matching the layer?
		 */
		if (msg.getNumberOfRequestedContacts() > 0) {
			List<TransitContact> contacts = trackerRegistry.getAdditionalNeighborhood(msg.getSenderContact(),
			        msg.getNumberOfRequestedContacts(), msg.getKnownContacts());
			OfferNeighborhoodMessage offerMsg = new OfferNeighborhoodMessage(getLocalOverlayContact(), contacts);
			return offerMsg;
		}
		return null;
	}

	/**
	 * Initial join of a node with the tracker
	 * 
	 * @param msg
	 * @return
	 */
	protected JoinTrackerReplyMessage handleJoinTrackerMsg(JoinTrackerMessage msg) {

		/*
		 * FIXME using global knowledge to determine the current playback
		 * position. This may be done as part of the source <-> tracker protocol
		 * lateron
		 */
		if (sourceNode == null) {
			/*
			 * 1. Try to find the source on the same host
			 */
			try {
				sourceNode = getHost().getComponent(TransitNode.class);
				if (!sourceNode.isServer()) {
					sourceNode = null;
				} else {
					trackerRegistry.addOrUpdateContact(sourceNode.getLocalOverlayContact());
				}
			} catch (ComponentNotAvailableException e1) {
				// no local source found
			}
			if (sourceNode == null) {
				/*
				 * 2. Global Knowledge
				 */
				for (Host host : Oracle.getAllHosts()) {
					try {
						sourceNode = host.getComponent(TransitNode.class);
						if (sourceNode.isServer()) {
							trackerRegistry.addOrUpdateContact(sourceNode.getLocalOverlayContact());
							break;
						}
					} catch (ComponentNotAvailableException e) {
						continue;
					}
				}
			}
		}

		if (sourceNode == null) {
			throw new AssertionError("You did not specify a source node!");
		}

		/*
		 * This simple tracker just accepts everyone
		 */
		TransitContact senderContact = msg.getSenderContact();
		List<TransitContact> initialContacts = new LinkedList<TransitContact>();
		if (senderContact.getType() == ContactType.CLIENT) {
			/*
			 * TODO find a set of nodes for this client
			 */
			initialContacts.addAll(trackerRegistry.getInitialNeighborhood(senderContact));
		}

		/*
		 * Add contact to tracker
		 */
		trackerRegistry.addOrUpdateContact(senderContact);

		// /*
		// * TODO check, if we may need to use getBufferBegin - 1 instead!
		// */
		StreamingDocument newDoc = new StreamingDocument(sourceNode.getVideo(), sourceNode.getBuffer().getBufferBegin());

		// /*
		// * FIXME FOR TESTING, we start at chunk 0
		// */
		// System.err
		// .println("FOR TESTING: Transit starts streaming at chunk 1 as set by TransitTracker!");
		// StreamingDocument newDoc = new
		// StreamingDocument(sourceNode.getVideo(),
		// 1);

		byte[] moovAtom = sourceNode.getVideo().getMoovAtom();
		
		if(moovAtom != null) { // Required for simulation case
			newDoc.setMoovAtom(moovAtom);
		}

		// if (sourcePlayer == null) {
		// for (Host host : Oracle.getAllHosts()) {
		// try {
		// sourcePlayer = host.getComponent(IPlayer.class);
		// if (sourcePlayer.getOverlayNode().isServer()) {
		// break;
		// }
		// } catch (ComponentNotAvailableException e) {
		// continue;
		// }
		// }
		// if (sourcePlayer == null) {
		// throw new AssertionError(
		// "You did not specify a source node!");
		// }
		// }
		// StreamingDocument newDoc = new StreamingDocument(sourcePlayer
		// .getOverlayNode().getVideo(),
		// sourcePlayer.getPlaybackPosition());

		TransitContact trackerContact = getLocalOverlayContact().clone();
		return new JoinTrackerReplyMessage(trackerContact, trackerContact, newDoc, initialContacts);
	}

	/**
	 * Inform all peers on potential neighbors
	 */
	public void offerAllPeersNewNeighbors() {

		int numOfContacts = 20;
		
		LinkedList<TransitContact> cs = new LinkedList<TransitContact>(trackerRegistry.allContacts());
		
		Iterator<TransitContact> rIt = cs.descendingIterator();
		int i = 1;
		while (rIt.hasNext()) {
			i++;
			final TransitContact c = rIt.next();
			List<TransitContact> contacts = trackerRegistry.getAdditionalNeighborhood(c, numOfContacts,
			        new LinkedList<UniqueID>());
			final OfferNeighborhoodMessage offerMsg = new OfferNeighborhoodMessage(getLocalOverlayContact(), contacts);
			Event.scheduleWithDelay(i*100*Time.MILLISECOND, new EventHandler() {
				
				@Override
				public void eventOccurred(Object content, int type) {
					transport.send(offerMsg, c.getNetID(), c.getPort());
				}
			}, null, 0);
			
		}
	}

}
