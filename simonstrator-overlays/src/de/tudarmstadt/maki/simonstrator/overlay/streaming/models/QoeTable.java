package de.tudarmstadt.maki.simonstrator.overlay.streaming.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;

public class QoeTable {

	private static QoeTable instance;

	protected final ArrayList<LayerEntry> entriesForQuality;

	protected final HashMap<Integer, LayerEntry> entries = new HashMap<Integer, QoeTable.LayerEntry>();

	protected byte[] maxLevels = new byte[] { -1, -1, -1 };

	/**
	 * The maximum video rate in KByte/s
	 */
	protected float maxByteRate = 0;

	protected float[][][] contributions;

	private QoeTable() {
		entriesForQuality = QoeTableFileParser.getEntries(StreamingConfiguration.getQoEAwareAdaptionTableFile());

		for (LayerEntry e : entriesForQuality) {
			int key = e.getD() * 10000 + e.getT() * 100 + e.getQ();
			entries.put(key, e);

			if (e.getD() > maxLevels[0] || e.getT() > maxLevels[1] || e.getQ() > maxLevels[2]) {
				if (e.getD() > maxLevels[0])
					maxLevels[0] = e.getD();
				if (e.getT() > maxLevels[1])
					maxLevels[1] = e.getT();
				if (e.getQ() > maxLevels[2])
					maxLevels[2] = e.getQ();
			}
		}

		int key = maxLevels[0] * 10000 + maxLevels[1] * 100 + maxLevels[2];
		maxByteRate = entries.get(key).getByteRate();

		Collections.sort(entriesForQuality);

		if (StreamingConfiguration.useLayerRatesOfQoeFile()) {
			/*
			 * Check integrity of data
			 */
			float[][][] layerBw = new float[maxLevels[0] + 1][maxLevels[1] + 1][maxLevels[2] + 1];
			boolean error = false;
			for (int d = 0; d <= maxLevels[0]; d++) {
				for (int t = 0; t <= maxLevels[1]; t++) {
					for (int q = 0; q <= maxLevels[2]; q++) {
						key = d * 10000 + t * 100 + q;
						LayerEntry e = entries.get(key);
						if (e == null) {
							error = true;
						} else {
							layerBw[d][t][q] = e.rate;
						}
					}
				}
			}

			/*
			 * Calculate layer weights
			 */
			if (!error) {
				contributions = new float[maxLevels[0] + 1][maxLevels[1] + 1][maxLevels[2] + 1];

				contributions[0][0][0] = layerBw[0][0][0];

				for (int d = 0; d <= maxLevels[0]; d++) {
					for (int t = 0; t <= maxLevels[1]; t++) {
						for (int q = 0; q <= maxLevels[2]; q++) {
							if (!(d == 0 && t == 0 && q == 0)) {
								float lBw = layerBw[d][t][q];
								float lowerSum = lowerContributionSum(contributions, d, t, q);
								contributions[d][t][q] = lBw - lowerSum;
							}
						}
					}
				}

				// Set contribution for each layer entry
				for (int d = 0; d <= maxLevels[0]; d++) {
					for (int t = 0; t <= maxLevels[1]; t++) {
						for (int q = 0; q <= maxLevels[2]; q++) {
							key = d * 10000 + t * 100 + q;
							LayerEntry e = entries.get(key);
							e.setPureRate(contributions[d][t][q]);
						}
					}
				}

				// [JR]: Can be removed when done
				// // Debugging
				// String out = "";
				// float sum = 0;
				// for (int d = 0; d <= maxLevels[0]; d++) {
				// for (int t = 0; t <= maxLevels[1]; t++) {
				// out += "\t" + contributions[d][t][0];
				// sum += contributions[d][t][0];
				// }
				// out += "\n";
				// }
				// System.out.println("2D Contribution matrix:");
				// System.out.print(out);
				// System.out.println("Sum: " + sum);

			}
		}
	}

	private static float lowerContributionSum(float[][][] contributions, int dUupper, int tUpper, int qUpper) {
		float sum = 0;
		for (int d = 0; d <= dUupper; d++) {
			for (int t = 0; t <= tUpper; t++) {
				for (int q = 0; q <= qUpper; q++) {
					if (!(d == dUupper && t == tUpper && q == qUpper)) {
						sum += contributions[d][t][q];
					}
				}
			}
		}
		return sum;
	}

	/**
	 * @param targetRate
	 *            the target video data rate to be met
	 * @param maxLayer
	 * @param oldLayer
	 * @param inc
	 * @return the layer entry with the best quality beyond the layers that meet
	 *         the target video data rate. If there is no such layer entry, the
	 *         one with the smallest data rate (the base layer) is returned.
	 */
	public static LayerEntry getBestFittingLayerToMaximizeQoE(float targetRate, byte[] maxLayer, byte[] oldLayer,
	        boolean inc) {
		QoeTable table = getInstance();
		ArrayList<LayerEntry> entries = table.entriesForQuality;
		LayerEntry oldLayerEntry = QoeTable.getLayer(oldLayer);

		for (int i = 0; i < entries.size(); i++) {
			LayerEntry e = entries.get(i);
			float layerBitRate = e.getByteRate() * 8;
			if (layerBitRate <= targetRate && e.getD() <= maxLayer[0] && e.getT() <= maxLayer[1]
			        && e.getQ() <= maxLayer[2]) {
				if (inc) {
					/*
					 * Make sure we only change the layer in this case if this
					 * also increases the quality.
					 */
					if (e.getQuality() < oldLayerEntry.getQuality())
						return e;
					return oldLayerEntry;
				} else {
					/*
					 * Make sure we chose a layer with lower bit rate on low
					 * buffer, to not make it worse by just meeting the
					 * estimated throughput.
					 */
					if (e.getByteRate() < oldLayerEntry.getByteRate())
						return e;
				}
			}
		}

		/*
		 * If there was no matching layer, return the one with the smallest data
		 * rate. This should be the base layer if the QoE table data is valid.
		 */
		return entries.get(0);
	}

	/**
	 * @param targetRate
	 *            the target video data rate to be met [kbps]
	 * @param maxLayer
	 * @return the layer entry that maximizes the used bandwidth, regardless of
	 *         the quality score of this layer.
	 */
	public static LayerEntry getBestFittingLayerToMaximizeBandwidth(float targetRate, byte[] maxLayer, byte[] oldLayer,
	        boolean inc) {
		QoeTable table = getInstance();
		ArrayList<LayerEntry> entries = new ArrayList<QoeTable.LayerEntry>(table.entriesForQuality);
		LayerEntry oldLayerEntry = QoeTable.getLayer(oldLayer);

		// Sort the entries descending, according to their bandwidth
		Collections.sort(entries, new Comparator<LayerEntry>() {

			@Override
			public int compare(LayerEntry o1, LayerEntry o2) {
				return ((Float) o2.getByteRate()).compareTo(o1.getByteRate());
			}
		});

		for (int i = 0; i < entries.size(); i++) {
			LayerEntry e = entries.get(i);
			float layerBitRate = e.getByteRate() * 8;
			if (layerBitRate <= targetRate && e.getD() <= maxLayer[0] && e.getT() <= maxLayer[1]
			        && e.getQ() <= maxLayer[2]) {
				if (inc) {
					/*
					 * Make sure we only change the layer in this case if this
					 * also increases the quality.
					 */
					if (e.getByteRate() > oldLayerEntry.getByteRate())
						return e;
					return oldLayerEntry;
				} else {
					/*
					 * Make sure we chose a layer with lower bit rate on low
					 * buffer, to not make it worse by just meeting the
					 * estimated throughput.
					 */
					if (e.getByteRate() < oldLayerEntry.getByteRate())
						return e;
				}
			}
		}

		/*
		 * If there was no matching layer, return the one with the smallest data
		 * rate. This should be the base layer if the QoE table data is valid.
		 */
		/*
		 * BUGFIX BR: if the table is sorted descending as described above, we
		 * have to return the LAST entry!
		 */
		return entries.get(entries.size() - 1);
		// return entries.get(0);
	}

	/**
	 * @param d
	 * @param t
	 * @param q
	 * @return the layer entry that matches the levels or null if there is no
	 *         such layer.
	 */
	public static LayerEntry getLayer(int d, int t, int q) {
		QoeTable table = getInstance();
		int key = d * 10000 + t * 100 + q;
		return table.entries.get(key);
	}

	public static LayerEntry getLayer(byte[] layer) {
		return getLayer(layer[0], layer[1], layer[2]);
	}

	public static LayerEntry getMaxLayer() {
		return getLayer(getMaxD(), getMaxT(), getMaxQ());
	}

	public static byte getMaxD() {
		QoeTable table = getInstance();
		return table.maxLevels[0];
	}

	public static byte getMaxT() {
		QoeTable table = getInstance();
		return table.maxLevels[1];
	}

	public static byte getMaxQ() {
		QoeTable table = getInstance();
		return table.maxLevels[2];
	}

	/**
	 * @return the byte max byte rate of this video in KByte/s
	 */
	public static float getMaxByteRate() {
		QoeTable table = getInstance();
		return table.maxByteRate;
	}

	public static float[][][] getLayerWeights() {
		QoeTable table = getInstance();
		return table.contributions;
	}

	private static QoeTable getInstance() {
		if (instance == null) {
			instance = new QoeTable();
		}
		return instance;
	}

	public static class LayerEntry implements Comparable<LayerEntry> {

		private final byte d, t, q;

		protected final float rate;

		private final float quality;

		private float pureRate = 0;

		public LayerEntry(byte d, byte t, byte q, float rate, float quality) {
			this.d = d;
			this.t = t;
			this.q = q;
			this.rate = rate;
			this.quality = quality;
		}

		@Override
		public int compareTo(LayerEntry otherEntry) {
			return ((Float) quality).compareTo(otherEntry.getQuality());
		}

		public byte getD() {
			return d;
		}

		public byte getT() {
			return t;
		}

		public byte getQ() {
			return q;
		}

		/**
		 * @return the rate for this entry in kBps
		 */
		public float getByteRate() {
			return rate * 1000;
		}

		public float getQuality() {
			return quality;
		}

		public float getPureRate() {
			return pureRate;
		}

		public void setPureRate(float pureRate) {
			this.pureRate = pureRate;
		}

		public byte getLayerIndex() {
			return (byte) (d * StreamingConfiguration.getTemporalLevels() * StreamingConfiguration.getQualityLevels()
			        + t * StreamingConfiguration.getQualityLevels() + q);
		}

		@Override
		public String toString() {
			return "[d=" + d + ",t=" + t + ",q=" + q + ",rate=" + rate + ",quality=" + quality + "]";
		}
	}
}
