package de.tudarmstadt.maki.simonstrator.overlay.streaming;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Properties;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.IDSpace;
import de.tudarmstadt.maki.simonstrator.overlay.DefaultIDSpace;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.CubeVideoModel;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.QoeTable;

/**
 * This class represents the configuration of the streaming system. It defines
 * constant values and extracts configuration values from the property file
 * referred to in the main configuration file.
 * 
 * @author Yue Sheng, complete rework by Julius Rueckert
 */
public class StreamingConfiguration {

	/**
	 * The path of the default property file. This file is used as fall-back, if
	 * a property is not present in the simulation's property file. Therefore it
	 * should contain a default value for all properties.
	 */
	private final static String DEFAULT_PROPERTY_FILE = "config" + File.separator + "transit" + File.separator
	        + "Streaming_default.properties";

	/**
	 * Instance of the simulation's property file
	 */
	private static Properties properties;

	/**
	 * Instance of the default property file
	 */
	private static Properties defaultProperties;

	public static final IDSpace IDSPACE = new DefaultIDSpace(32);

	public static VideoModel getVideoModel() {
		return CubeVideoModel.getInstance();
	}

	/**
	 * The number of chunks played in one second
	 */
	public static final float CHUNKS_PER_SECOND = 3.75f;

	/**
	 * The length of the transition phase after switching the quality layer in
	 * number of chunks.
	 */
	public final static short SWITCHING_WINDOW = 8;

	/**
	 * The global buffer size in number of chunks
	 */
	public final static int BUFFER_SIZE = Math.round(CHUNKS_PER_SECOND * 6);

	/**
	 * The length of video material in simulation time contained in one chunk
	 */
	public static long PLAYBACK_INTERVAL = Math.round(Time.SECOND / CHUNKS_PER_SECOND);

	/**
	 * Layer size (weight) of the cube model 1. dimension: spatial 2. dimension:
	 * temporal 3. dimension: quality
	 */
	// public final static short[][][] layerWeight = {{{1},{1},{1},{1}}};
	// public final static short[][][] layerWeight =
	// {{{4},{1},{1}},{{8},{2},{2}}};
	private final static float[][][] layerWeight = { { { 4 }, { 1 }, { 1 }, { 1 } }, { { 8 }, { 2 }, { 2 }, { 2 } },
	        { { 12 }, { 4 }, { 4 }, { 4 } } };

	// levels of SVC layers
	// public static final byte spatialLevels = (byte) layerWeight.length;
	// public static final byte temporalLevels = (byte) layerWeight[0].length;
	// public static final byte snrLevels = (byte) layerWeight[0][0].length;

	// computing complexity coefficient
	public final static int Cd = 30;
	public final static int Ct = 30;
	public final static int Cq = 40;

	// for priority calculation
	public final static float spatialWeight = 1 / 3f;
	public final static float temporalWeight = 1 / 3f;
	public final static float snrWeight = 1 / 3f;

	public static float[][][] getLayerWeights() {
		if (useLayerRatesOfQoeFile())
			return QoeTable.getLayerWeights();
		return layerWeight;
	}

	public static byte getSpatialLevels() {
		if (useLayerRatesOfQoeFile())
			return (byte) (QoeTable.getMaxD() + 1);
		return (byte) layerWeight.length;
	}

	public static byte getTemporalLevels() {
		if (useLayerRatesOfQoeFile())
			return (byte) (QoeTable.getMaxT() + 1);
		return (byte) layerWeight[0].length;
	}

	public static byte getQualityLevels() {
		if (useLayerRatesOfQoeFile())
			return (byte) (QoeTable.getMaxQ() + 1);
		return (byte) layerWeight[0][0].length;
	}

	/**
	 * @return the parameter A used in the block priority calculation
	 */
	public static float getA() {
		String defaultValue = getDefaultProperties().getProperty("A");
		return Float.parseFloat(getProperties().getProperty("A", defaultValue));
	}

	/**
	 * @return the parameter B used in the block priority calculation
	 */
	public static float getB() {
		String defaultValue = getDefaultProperties().getProperty("B");
		return Float.parseFloat(getProperties().getProperty("B", defaultValue));
	}

	// time out
	public static final long MSG_TIMEOUT = 2 * Time.SECOND;
	public static final long BLOCK_TIMEOUT = 4 * Time.SECOND;

	public static final long OPERATION_TIMEOUT = 8 * Time.SECOND;

	// for the peer-manager
	public static final long PEER_SELECT_INTERVAL = 4 * Time.SECOND;
	public static final long PEER_CONNECT_INTERVAL = Time.SECOND;

	/**
	 * @return the highest possible peer rank
	 */
	public static byte getMaxPeerRank() {
		String defaultValue = getDefaultProperties().getProperty("MAX_PEER_RANK");
		return Byte.parseByte(getProperties().getProperty("MAX_PEER_RANK", defaultValue));
	}

	/**
	 * Peers with rank lower than this get dropped
	 */
	public static final byte DROP_PEER_RANK = 0;

	/**
	 * Maximal number of upload connections
	 */
	public static short getMaxUpConnections() {
		String defaultValue = getDefaultProperties().getProperty("MAX_UP_CONNECTIONS");
		return Short.parseShort(getProperties().getProperty("MAX_UP_CONNECTIONS", defaultValue));
	}

	/**
	 * Minimal number of download connections
	 */
	public static short getMinDownConnections() {
		String defaultValue = getDefaultProperties().getProperty("MIN_DOWN_CONNECTIONS");
		return Short.parseShort(getProperties().getProperty("MIN_DOWN_CONNECTIONS", defaultValue));
	}

	/**
	 * Separator used in the configuration of scenarios
	 */
	public static final String SEPARATOR = ",";

	/**
	 * @return the video bit rate in KByte/s
	 */
	public static float getVideoByteRate() {
		if (useLayerRatesOfQoeFile())
			return QoeTable.getMaxByteRate();
		String defaultValue = getDefaultProperties().getProperty("VIDEO_BITRATE");
		return Float.parseFloat(getProperties().getProperty("VIDEO_BITRATE", defaultValue)) / 8;
	}

	/**
	 * @return the video length in seconds
	 */
	public static int getVideoLength() {
		String defaultValue = getDefaultProperties().getProperty("VIDEO_LENGTH");
		return Integer.parseInt(getProperties().getProperty("VIDEO_LENGTH", defaultValue));
	}

	/**
	 * @return the video length in chunks
	 */
	public static int getTotalNumberOfChunks() {
		return Math.round(getVideoLength() * CHUNKS_PER_SECOND);
	}

	/**
	 * @return the video name
	 */
	public static String getVideoName() {
		String defaultValue = getDefaultProperties().getProperty("VIDEO_NAME");
		return getProperties().getProperty("VIDEO_NAME", defaultValue);
	}

	/**
	 * @return the interval between PQA (in chunks)
	 */
	public static int getPQAChunkInterval() {
		int pqa = getPQATimeInterval();
		if (pqa < 0)
			return Integer.MAX_VALUE;
		return Math.round(pqa * StreamingConfiguration.CHUNKS_PER_SECOND);
	}

	/**
	 * @return the interval between PQA (in seconds)
	 */
	public static int getPQATimeInterval() {
		String defaultValue = getDefaultProperties().getProperty("PQA_INTERVAL");
		int pqaIntervall = Integer.parseInt(getProperties().getProperty("PQA_INTERVAL", defaultValue));
		if (pqaIntervall < 0)
			/*
			 * Set the PQA interval to two times the simulation time. This
			 * practically disables PQA.
			 */
			return (int) (48 * Time.HOUR);
		return pqaIntervall;
	}

	/**
	 * @return tells whether the IQA is enabled or not
	 */
	public static boolean getIQAEnabled() {
		String defaultValue = getDefaultProperties().getProperty("IQA_ENABLED");
		return Boolean.parseBoolean(getProperties().getProperty("IQA_ENABLED", defaultValue));
	}

	/**
	 * @return the maximum delay in simulation time to wait before leaving the
	 *         overlay after finishing to watch the video.
	 */
	public static long getMaxDelayOnLeave() {
		String defaultValue = getDefaultProperties().getProperty("LEAVE_MAX_DELAY");
		long v = Long.parseLong(getProperties().getProperty("LEAVE_MAX_DELAY", defaultValue));
		return v * Time.MINUTE;
	}

	/**
	 * @return the probability to immediately leave the overlay after finishing
	 *         to watch the video.
	 */
	public static float getProbabilityToImmediatelyLeave() {
		String defaultValue = getDefaultProperties().getProperty("LEAVE_IMMEDIATELY_PROBABILITY");
		return Float.parseFloat(getProperties().getProperty("LEAVE_IMMEDIATELY_PROBABILITY", defaultValue));
	}

	/**
	 * @return tells which layer should be used as initial layer
	 */
	public static int getInitialLayerChoice() {
		String defaultValue = getDefaultProperties().getProperty("INITIAL_LAYER_CHOICE");
		return Integer.parseInt(getProperties().getProperty("INITIAL_LAYER_CHOICE", defaultValue));
	}

	/**
	 * @return tells which layer decision algorithm is to be used (0 = max QoE,
	 *         1 = max bandwidth, 2 = Simple Layer Decision, 3 = Prioritized
	 *         Dimensions)
	 */
	public static int getLayerDecisionAlgorithm() {
		String defaultValue = getDefaultProperties().getProperty("LAYER_DECISION_ALGORITHM");
		return Integer.parseInt(getProperties().getProperty("LAYER_DECISION_ALGORITHM", defaultValue));
	}

	/**
	 * @return the priority to be used for prioritized dimensions layer decision
	 *         and switching (0 = d > t > q; 1 = t > d > q; 2 = t > q > d)
	 */
	public static int getPrioritizedDimensionsVariant() {
		String defaultValue = getDefaultProperties().getProperty("PRIORITIZED_DIMENSIONS_PRIORITY");
		return Integer.parseInt(getProperties().getProperty("PRIORITIZED_DIMENSIONS_PRIORITY", defaultValue));
	}

	/**
	 * @return tells whether the simulation should use the layer switching
	 *         mechanism. This is a feature that applies adaptation paths
	 *         instead of direct jumps to a target quality.
	 */
	public static boolean useLayerSwitching() {
		String defaultValue = getDefaultProperties().getProperty("LAYER_SWITCHING_ENABLED");
		Boolean b = Boolean.parseBoolean(getProperties().getProperty("LAYER_SWITCHING_ENABLED", defaultValue));

		// disabled or switching algorithm = 3
		return (b && getLayerSwitchingAlgorithm() != 3);
	}

	/**
	 * @return tells whether to use the throughput aware jumping on stalls. If
	 *         this is disabled, the adaptation always chooses the base layer on
	 *         stalls.
	 */
	public static boolean switchToBaseLayerOnStallDisabled() {
		String defaultValue = getDefaultProperties().getProperty("SWITCH_TO_BL_ON_STALL_DISABLED");
		return Boolean.parseBoolean(getProperties().getProperty("SWITCH_TO_BL_ON_STALL_DISABLED", defaultValue));
	}

	/**
	 * @return tells whether to use the layer switching also in the case of
	 *         stalling. In t his case the target layer always is the base layer
	 *         and one might argue if it makes sense to do a stepwise switching
	 *         because the peer is already in a stalling state.
	 */
	public static boolean useLayerSwitchingIfStalled() {
		String defaultValue = getDefaultProperties().getProperty("LAYER_SWITCHING_USE_FOR_STALLING_SWITCHES");
		return Boolean.parseBoolean(getProperties().getProperty("LAYER_SWITCHING_USE_FOR_STALLING_SWITCHES",
		        defaultValue));
	}

	public static boolean useNetStatAdaptationStep() {
		String defaultValue = getDefaultProperties().getProperty("USE_NET_STAT_ADAPT");
		return Boolean.parseBoolean(getProperties().getProperty("USE_NET_STAT_ADAPT", defaultValue));
	}

	/**
	 * @return tells which layer switching algorithm is to be used (0 = City
	 *         Block, 1 = Dijkstra (minimize QoE variation), 2 = Dijkstra
	 *         (minimize bandwidth variation), 3 = Simple Layer Switching (=
	 *         Disable Layer Switching)
	 */
	public static int getLayerSwitchingAlgorithm() {
		String defaultValue = getDefaultProperties().getProperty("LAYER_SWITCHING_ALGORITHM");
		return Integer.parseInt(getProperties().getProperty("LAYER_SWITCHING_ALGORITHM", defaultValue));
	}

	/**
	 * @return defined the minimal time (in seconds) for a switching step.
	 */
	public static int getQoEAwareLayerSwitchingMinimalTimePerStep() {
		String defaultValue = getDefaultProperties().getProperty("LAYER_SWITCHING_MINIMAL_TIME_PER_STEP");
		return Integer.parseInt(getProperties().getProperty("LAYER_SWITCHING_MINIMAL_TIME_PER_STEP", defaultValue));
	}

	/**
	 * @return the path of the file defining the qualities and video bit rates
	 *         for the layers of the SVC video.
	 */
	public static String getQoEAwareAdaptionTableFile() {
		String defaultValue = getDefaultProperties().getProperty("QOE_AWARE_ADAPTION_TABLE_FILE");
		return getProperties().getProperty("QOE_AWARE_ADAPTION_TABLE_FILE", defaultValue);
	}

	public static boolean useLayerRatesOfQoeFile() {
		String defaultValue = getDefaultProperties().getProperty("USE_LAYER_RATES_OF_QOE_FILE");
		return Boolean.parseBoolean(getProperties().getProperty("USE_LAYER_RATES_OF_QOE_FILE", defaultValue));
	}

	/**
	 * @return the interval (in seconds) that is used to calculate the received
	 *         throughput over.
	 */
	public static int getThroughputEstimationInterval() {
		String defaultValue = getDefaultProperties().getProperty("THROUGHPUT_ESTIMATION_INTERVAL");
		return Integer.parseInt(getProperties().getProperty("THROUGHPUT_ESTIMATION_INTERVAL", defaultValue));
	}

	/**
	 * @return the factor used when choosing a target bitrate to leave some room
	 */
	public static float getBitrateAdaptionFactor() {
		String defaultValue = getDefaultProperties().getProperty("BITRATE_ADAPTATION_FACTOR");
		return Float.parseFloat(getProperties().getProperty("BITRATE_ADAPTATION_FACTOR", defaultValue));
	}

	/*
	 * Configuration parameters for voting mechanism by Jan Wiese
	 */

	/**
	 * Tells whether to use an exponential weighted voting for the low priority
	 * set or not.
	 */
	public static boolean useExponentialVoting() {
		String defaultValue = getDefaultProperties().getProperty("USE_EXPONENTIAL_VOTING");
		return Boolean.parseBoolean(getProperties().getProperty("USE_EXPONENTIAL_VOTING", defaultValue));
	}

	/**
	 * If the upload bit rate of a peer is lower than this value, the peer is
	 * considered as slow peer. This parameter only takes effect with enabled
	 * advanced prefetching and upload strategies.
	 */
	public static double getSlowPeerBitRate() {
		String defaultValue = getDefaultProperties().getProperty("SLOW_PEER_BITRATE");
		return Double.parseDouble(getProperties().getProperty("SLOW_PEER_BITRATE", defaultValue));
	}

	/**
	 * The size of the low priority zone voting table
	 */
	public static int getVotingTableSize() {
		String defaultValue = getDefaultProperties().getProperty("VOTING_TABLE_SIZE");
		return Integer.parseInt(getProperties().getProperty("VOTING_TABLE_SIZE", defaultValue));
	}

	/**
	 * The start point of the low priority zone voting (in chunks after the
	 * buffer end).
	 */
	public static int getVotingTableStart() {
		String defaultValue = getDefaultProperties().getProperty("VOTING_TABLE_START");
		return Integer.parseInt(getProperties().getProperty("VOTING_TABLE_START", defaultValue));
	}

	/**
	 * The period (in seconds) the low priority set voting is repeated after.
	 */
	public static int getVotingPeriod() {
		String defaultValue = getDefaultProperties().getProperty("VOTING_PERIOD");
		return Integer.parseInt(getProperties().getProperty("VOTING_PERIOD", defaultValue));
	}

	/**
	 * The low priority zone voting algorithm to be used.
	 * 
	 * 0 = rarestFirst, 1 = SMN combined with rarestFirst, 2 = SMN combined with
	 * discrete Distribution, 3 = SMN combined with rarestFirst and discrete
	 * Distribution, 4 = SMN
	 */
	public static int getVotingAlgorithm() {
		String defaultValue = getDefaultProperties().getProperty("VOTING_ALGORITHM");
		return Integer.parseInt(getProperties().getProperty("VOTING_ALGORITHM", defaultValue));
	}

	/**
	 * Tells whether PQA is enabled or not. A disabled PQA is equivalent to the
	 * streaming of non SVC videos. It leads to a configuration where every peer
	 * streams the full video (with all layers).
	 */
	public static boolean getPQAEnabled() {
		String defaultValue = getDefaultProperties().getProperty("PQA_ENABLED");
		return Boolean.parseBoolean(getProperties().getProperty("PQA_ENABLED", defaultValue));
	}

	/**
	 * Tells whether to use the alternative connection importance by JW or not.
	 * This only takes effect if the advanced pretecthing and upload strategies
	 * are enabled.
	 */
	public static boolean useAlternativeConnectionImportance() {
		String defaultValue = getDefaultProperties().getProperty("USE_ALTERNATIVE_CONNECTION_IMPORTANCE");
		return Boolean.parseBoolean(getProperties().getProperty("USE_ALTERNATIVE_CONNECTION_IMPORTANCE", defaultValue));
	}

	/**
	 * @return tells whether to use the advanced prefetching and upload
	 *         strategies by JW.
	 */
	public static boolean useAdvancedPrefetchingAndUploadStrategies() {
		String defaultValue = getDefaultProperties().getProperty("ENABLE_ADVANCED_PREFETCHING_AND_UPLOAD_STRATEGIES");
		return Boolean.parseBoolean(getProperties().getProperty("ENABLE_ADVANCED_PREFETCHING_AND_UPLOAD_STRATEGIES",
		        defaultValue));
	}

	/**
	 * @return the upload bias probability as used for the upload strategies.
	 * 
	 *         NOTE: A value of 0 means that no low priority requests are
	 *         rejected, 1 means all of them are rejected (only high priority
	 *         request are served).
	 */
	public static float getUploadBiasProbability() {
		String defaultValue = getDefaultProperties().getProperty("UPLOAD_BIAS_PROBABILITY");
		return Float.parseFloat(getProperties().getProperty("UPLOAD_BIAS_PROBABILITY", defaultValue));
	}

	/**
	 * NOTE: The dynamic adaptation is not yet implemented!
	 * 
	 * @return tells whether to use a dynamic upload bias probability.
	 */
	public static boolean useDynamicUploadBiasProbability() {
		String defaultValue = getDefaultProperties().getProperty("ENABLE_DYNAMIC_UPLOAD_BIAS_PROBABILITY");
		return Boolean
		        .parseBoolean(getProperties().getProperty("ENABLE_DYNAMIC_UPLOAD_BIAS_PROBABILITY", defaultValue));
	}

	/**
	 * @return the total time a switching has to be finished in simulator time
	 *         units.
	 */
	public static long getLayerSwitchingSpeed() {
		String defaultValue = getDefaultProperties().getProperty("LAYER_SWITCHING_SPEED");
		return Time.SECOND * Integer.parseInt(getProperties().getProperty("LAYER_SWITCHING_SPEED", defaultValue));
	}

	/*
	 * Scenario configuration
	 */

	/**
	 * Tells which scenario is to be used. 0 = Simple random arrival with the
	 * configured mean arrival time; 1 = Double flash crowd scenario with the
	 * configured parameters.
	 * 
	 * @return the identifier of the scenario to be used.
	 */
	public static int getScenario() {
		String defaultValue = getDefaultProperties().getProperty("SCENARIO");
		return Integer.parseInt(getProperties().getProperty("SCENARIO", defaultValue));
	}

	public static String getMeanArrivalTime() {
		String defaultValue = getDefaultProperties().getProperty("MEAN_ARRIVAL_TIME");
		return getProperties().getProperty("MEAN_ARRIVAL_TIME", defaultValue);
	}

	public static String getFirstJoinPeriod() {
		String defaultValue = getDefaultProperties().getProperty("FIRST_JOIN_PERIOD");
		return getProperties().getProperty("FIRST_JOIN_PERIOD", defaultValue);
	}

	public static String getSecondJoinPeriod() {
		String defaultValue = getDefaultProperties().getProperty("SECOND_JOIN_PERIOD");
		return getProperties().getProperty("SECOND_JOIN_PERIOD", defaultValue);
	}

	public static float getPeerJoinDistributionBalance() {
		String defaultValue = getDefaultProperties().getProperty("PEER_DISTRIBUTION_BALANCE");
		return Float.parseFloat(getProperties().getProperty("PEER_DISTRIBUTION_BALANCE", defaultValue));
	}

	public static boolean getLeaveIfStallingTooLongEnabled() {
		String defaultValue = getDefaultProperties().getProperty("LEAVE_IF_STALLING_TOO_LONG");
		return Boolean.parseBoolean(getProperties().getProperty("LEAVE_IF_STALLING_TOO_LONG", defaultValue));
	}

	public static float getLeaveOnTooLongStallingRelativeToMovieLength() {
		String defaultValue = getDefaultProperties().getProperty("LEAVE_TRESHOLD_RELATIVE_TO_MOVIE_LENGTH");
		return Float.parseFloat(getProperties().getProperty("LEAVE_TRESHOLD_RELATIVE_TO_MOVIE_LENGTH", defaultValue));
	}

	public static float getLeaveOnTooLongInitialStallingRelativeToMovieLength() {
		String defaultValue = getDefaultProperties().getProperty("LEAVE_INITIAL_TRESHOLD_RELATIVE_TO_MOVIE_LENGTH");
		return Float.parseFloat(getProperties().getProperty("LEAVE_INITIAL_TRESHOLD_RELATIVE_TO_MOVIE_LENGTH",
		        defaultValue));
	}

	public static boolean isScaledScenario() {
		String defaultValue = getDefaultProperties().getProperty("IS_SCALED_SCENARIO");
		return Boolean.parseBoolean(getProperties().getProperty("IS_SCALED_SCENARIO", defaultValue));
	}

	public static long getBottlenackStart() {
		String defaultValue = getDefaultProperties().getProperty("BOTTLENECK_START");
		return Long.parseLong(getProperties().getProperty("BOTTLENECK_START", defaultValue)) * Time.SECOND;
	}

	public static long getBottlenackEnd() {
		String defaultValue = getDefaultProperties().getProperty("BOTTLENECK_END");
		return Long.parseLong(getProperties().getProperty("BOTTLENECK_END", defaultValue)) * Time.SECOND;
	}

	/**
	 * @return a map
	 */
	public static LinkedHashMap<String, String> getFieldsToBeUsedInFolderName() {
		LinkedHashMap<String, String> fields = new LinkedHashMap<String, String>();
		/*
		 * Specify all fields that should appear in output folder name
		 */
		fields.put("Scenario", getScenario() + "");

		if (getPQAEnabled() && getPQATimeInterval() < (48 * Time.HOUR))
			fields.put("PQA", getPQATimeInterval() + "");
		else
			fields.put("noPQA", "");

		if (getIQAEnabled()) {

			switch (getInitialLayerChoice()) {
			case 0:
				fields.put("IQA", "-def");
				break;
			case 1:
				fields.put("IQA", "-maxBw");
				break;
			case 2:
				fields.put("IQA", "-maxQoE");
				break;
			case 3:
				fields.put("IQA", "-BL");
				break;
			default:
				break;
			}
		} else
			fields.put("noIQA", "");

		switch (getLayerDecisionAlgorithm()) {
		case 0:
			fields.put("Dqoe", "");
			break;
		case 1:
			fields.put("Dbw", "");
			break;
		case 2:
			fields.put("Dsim", "");
			break;
		case 3:

			switch (getPrioritizedDimensionsVariant()) {
			case 0:
				fields.put("Dprio", "-dtq");
				break;
			case 1:
				fields.put("Dprio", "-tdq");
				break;
			case 2:
				fields.put("Dprio", "-tqd");
				break;
			default:
				fields.put("Dprio", "-_");
				break;
			}

			break;
		default:
			fields.put("D", "_");
			break;
		}

		if (!useNetStatAdaptationStep())
			fields.put("noNetStat", "");

		switch (getLayerSwitchingAlgorithm()) {
		case 0:
			switch (getPrioritizedDimensionsVariant()) {
			case 0:
				fields.put("Sprio", "-dtq");
				break;
			case 1:
				fields.put("Sprio", "-tdq");
				break;
			case 2:
				fields.put("Sprio", "-tqd");
				break;
			default:
				fields.put("Sprio", "-_");
				break;
			}
			break;
		case 1:
			fields.put("Sqoe", "");
			break;
		case 2:
			fields.put("Sbw", "");
			break;
		case 3:
			fields.put("Ssim", "");
			break;
		default:
			fields.put("S", "_");
			break;
		}

		if (useLayerSwitching()) {
			fields.put("MinInt", getQoEAwareLayerSwitchingMinimalTimePerStep() + "");
			fields.put("Sp", (getLayerSwitchingSpeed() / Time.SECOND) + "");
		}

		if (isScaledScenario()) {
			fields.put("Scaled", "");
		}

		if (useAdvancedPrefetchingAndUploadStrategies()) {
			String algorithm = "";

			switch (getVotingAlgorithm()) {
			case 0:
				algorithm = "-RF";
				break;

			case 1:
				algorithm = "-SMNRF";
				break;

			case 2:
				algorithm = "-SMNDD";
				break;

			case 3:
				algorithm = "-SMNRFDD";
				break;

			case 4:
				algorithm = "-SMN";
				break;

			default:
				break;
			}
			fields.put("Prefetch", algorithm + "");

			fields.put("p", getUploadBiasProbability() + (useDynamicUploadBiasProbability() ? "d" : ""));

		}

		return fields;
	}

	/**
	 * This is called by the overlay factory to specify the property file to be
	 * used. This usually is configured via the configuration file.
	 * 
	 * @param propertyFile
	 *            the property file to be used in a simulation
	 */
	public static void setProperties(String propertyFile) {
		StreamingConfiguration.defaultProperties = readDefaultProperties();
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(propertyFile));
			StreamingConfiguration.properties = properties;
		} catch (IOException e) {
			System.err.println("Could nor read property file: " + propertyFile);
			e.printStackTrace();
		}
	}

	private static Properties readDefaultProperties() {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(DEFAULT_PROPERTY_FILE));
		} catch (IOException e) {
			System.err.println("Could nor read property file: " + DEFAULT_PROPERTY_FILE);
			e.printStackTrace();
		}
		return properties;
	}

	private static Properties getProperties() {
		if (properties == null) {
			properties = readDefaultProperties();
		}
		if (defaultProperties == null) {
			defaultProperties = readDefaultProperties();
		}
		return properties;
	}

	private static Properties getDefaultProperties() {
		if (properties == null) {
			properties = readDefaultProperties();
		}
		if (defaultProperties == null) {
			defaultProperties = readDefaultProperties();
		}
		return defaultProperties;
	}

}
