/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;

/**
 * This is the on-demand version of a {@link TransitFlowSchedule} - it allows a
 * node to fetch one or many blocks missing from the live phase by issuing this
 * to a parent. Here, we do not negotiate a schedule, instead we send this
 * request and wait for a reply.
 * 
 * Each {@link TransitConnection} is allowed to have exactly one RequestSchedule
 * per chunk interval.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public class TransitRequestSchedule implements Transmitable {

	private final int blockOffset;

	private final BitSet blockMap;

	private final int transmissionSize;

	/**
	 * 
	 * @param blockOffset
	 * @param blockMap
	 */
	public TransitRequestSchedule(int blockOffset, BitSet blockMap) {
		this.blockMap = (BitSet) blockMap.clone();
		this.blockOffset = blockOffset;
		this.transmissionSize = (int) (Math.ceil(blockMap.size() / 8)) + 4;
	}

	/**
	 * Copy-Constructor
	 * 
	 * @param toCopy
	 */
	public TransitRequestSchedule(TransitRequestSchedule toCopy) {
		this(toCopy.getBlockOffset(), toCopy.getBlockMap());
	}

	/**
	 * Map of blocks requested or offered starting with blockOffset. This can
	 * safely be modified to check received blocks, as the RequestSchedule is
	 * copied as soon as it leaves the node.
	 * 
	 * @return
	 */
	public BitSet getBlockMap() {
		return blockMap;
	}

	/**
	 * First block in the bitmap
	 * 
	 * @return
	 */
	public int getBlockOffset() {
		return blockOffset;
	}

	@Override
	public int getTransmissionSize() {
		return transmissionSize;
	}

	@Override
	public String toString() {
		return blockOffset + " " + blockMap.toString();
	}

}
