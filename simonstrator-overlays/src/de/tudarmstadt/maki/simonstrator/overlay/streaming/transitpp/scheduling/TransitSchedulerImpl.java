/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling;

import java.util.BitSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.simple.TransitSimpleFlowManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.simple.TransitSimpleRequestManager;

/**
 * Implementation of a {@link TransitScheduler}
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public class TransitSchedulerImpl implements TransitScheduler {

	private final TransitFlowManager flowManager;

	private final TransitRequestManager requestManager;

	private final List<BufferListener> bufferListeners;

	/**
	 * The node
	 */
	private final TransitNode node;

	/**
	 * This is the chunk that is to be played next (or, if the player stalled:
	 * the chunk that is not yet complete)
	 */
	private int mostUrgendChunk = -1;

	private int lastFlowChunk = -1;

	private long timestampStart = 0;

	private long timestampFirstPlay = 0;

	private long initialBufferDelayTime;

	private boolean active = false;

	private BitSet mask;

	/**
	 * Internal bookkeeping of the player state
	 */
	private PlaybackState playbackState = PlaybackState.NONE;

	public TransitSchedulerImpl(TransitNode node) {
		this.node = node;
		flowManager = new TransitSimpleFlowManager(node);
		node.addPeerStatusListener(flowManager);
		requestManager = new TransitSimpleRequestManager(node);
		node.addPeerStatusListener(requestManager);
		bufferListeners = new LinkedList<BufferListener>();
		initialBufferDelayTime = node.getSettings().getTime(
				TransitTimes.INITIAL_BUFFER_DELAY);
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		switch (peerStatus) {
		case PRESENT:
		case TO_JOIN:
			active = true;
			break;

		case ABSENT:
			active = false;
			break;

		default:
			throw new AssertionError("Unknown PeerStatus.");
		}
	}

	@Override
	public TransitFlowManager getFlowManager() {
		return flowManager;
	}

	@Override
	public TransitRequestManager getRequestManager() {
		return requestManager;
	}

	@Override
	public int getBufferBegin() {
		return mostUrgendChunk;
	}

	@Override
	public void receivedBlock(TransitBlockMessage blockMessage,
			TransitReplyHandler replyTo) {

		boolean duplicate = false; 
		
		if (node.getVideo().haveBlock(blockMessage.getBlockNumber())) {
			duplicate = true;
		} else {
			// mark block as received
			node.getVideo().markBlockAsReceived(blockMessage.getBlockNumber());
		}
		if (blockMessage.isLiveBlock()) {
			flowManager.receivedLiveBlock(blockMessage, replyTo, duplicate);
			requestManager.cancelRequest(blockMessage.getBlockNumber());
		} else {
			requestManager.receivedRequestedBlock(blockMessage, replyTo,
					duplicate);
		}
	}

	@Override
	public void doSwitchLayer(byte[] targetLayer, LayerSwitchCallback callback) {
		/*
		 * TODO The layer switching mechanism is currently not implemented in
		 * transit.
		 */
	}

	@Override
	public int getCurrentBufferLength() {
		// Calculate the buffering advance
		int i = getBufferBegin();
		if (i < 0) {
			return 0;
		}
		int length = 0;
		for (; i < node.getVideo().getLength(); i++) {
			if (!node.getVideo()
					.layerIsCoveredByChunkBlocks(i, node.getLayer())) {
				break;
			}
			length++;
		}
		return length;
	}

	@Override
	public long getStartupDelay() {
		return Math.max(0, timestampFirstPlay - timestampStart);
	}

	@Override
	public byte[] getLayerForChunk(int chunk) {
		/*
		 * TODO the layer switching mechanism is currently not implemented.
		 */
		return node.getLayer();
	}

	@Override
	public boolean hasChunk(int chunk) {
		return node.getVideo().layerIsCoveredByChunkBlocks(chunk,
				getLayerForChunk(chunk));
	}

	@Override
	public boolean tryToPlay(int chunk) {

		node.getLocalOverlayContact().updateCurrentChunk(chunk);
		node.getLocalOverlayContact().setLoad(
				(short) (10 * node.getBandwidthManager().getLoad(
						ConnectionDirection.OUT)));

		if (!active) {
			// intercept ticks here.
			System.err.println("TS: Node is not active!");
			return false;
		}
		
		if (playbackState == PlaybackState.NONE) {
			timestampStart = Time.getCurrentTime();
			playbackState = PlaybackState.BUFFERING;
			if (node.isServer()) {
				playbackState = PlaybackState.PLAYING;
			}
		}

		boolean hasChunk = hasChunk(chunk);
		if (hasChunk) {
			mostUrgendChunk = chunk + 1;
		} else {
			mostUrgendChunk = chunk;
		}

		PlaybackState newState = playbackState;

		if (playbackState == PlaybackState.BUFFERING) {
			if (hasChunk
					&& getCurrentBufferLength() >= StreamingConfiguration.BUFFER_SIZE
					&& Time.getCurrentTime() >= timestampStart
							+ initialBufferDelayTime) {
				if (timestampFirstPlay == 0) {
					timestampFirstPlay = Time.getCurrentTime();
				}
				newState = PlaybackState.PLAYING;
			}
		} else if (playbackState == PlaybackState.PLAYING) {
			if (!hasChunk) {
				newState = PlaybackState.BUFFERING;
			}
		}
		boolean canPlay = updateState(playbackState, newState, chunk);
		tick(chunk);
		return canPlay;
	}

	/**
	 * Updates the state and returns true, if the playback of the current chunk
	 * is possible.
	 * 
	 * @param oldState
	 * @param newState
	 * @param chunk
	 * @return
	 */
	private boolean updateState(PlaybackState oldState, PlaybackState newState,
			int chunk) {
		playbackState = newState;
		if (newState != oldState) {
			for (BufferListener bufferListener : bufferListeners) {
				bufferListener
						.onPlaybackStateChanged(oldState, newState, chunk);
			}
		}
		return newState == PlaybackState.PLAYING;
	}

	@Override
	public void tick(int chunkNumber) {

		/*
		 * Execute flows
		 */
		flowManager.tick();

		/*
		 * Request missing blocks
		 */
		if (node.isServer()) {
			return;
		}

		int bufferBegin = getBufferBegin();

		int urgentSize = StreamingConfiguration.BUFFER_SIZE; // FIXME
		if (playbackState == PlaybackState.PLAYING) {
			urgentSize = node.getSettings().getParam(
					TransitParams.REQUEST_BUFFER_URGENT_SIZE);
		}

		BitSet missing = new BitSet();

		int count = 0;
		int offset = 0;
		boolean started = false;
		for (int chunk = bufferBegin; chunk < bufferBegin + urgentSize; chunk++) {
			if (!node.getVideo().layerIsCoveredByChunkBlocks(chunk,
					node.getLayer())) {
				if (!started) {
					started = true;
					offset = chunk * node.BLOCKS_PER_CHUNK;
				}
				BitSet blocks = node.getVideo().getLayerMask(node.getLayer());
				for (int i = blocks.nextSetBit(0); i >= 0; i = blocks
						.nextSetBit(i + 1)) {
					int blocknum = chunk * node.BLOCKS_PER_CHUNK + i;
					if (!node.getVideo().haveBlock(blocknum)) {
						missing.set(count * node.BLOCKS_PER_CHUNK + i);
					}
				}
			}
			if (started) {
				count++;
			}
		}
		
		if (!missing.isEmpty()) {
			requestManager.requestBlocks(offset, missing);
		} else {
			assert playbackState == PlaybackState.PLAYING;
		}
		
	}

	@Override
	public PlaybackState getPlaybackState() {
		return playbackState;
	}

	@Override
	public void switchedLayer() {
		// generate new BitSet
		mask = node.getVideo().getLayerMask(node.getLayer());
		flowManager.requestFlow(mask);
	}

	@Override
	public void addBufferListener(BufferListener listener) {
		bufferListeners.add(listener);
	}

	@Override
	@Deprecated
	public Map<UniqueID, BitSet> getAvailableBlocksByAllNeighbors() {
		/*
		 * TODO currently, this is only used for analyzing and PQA with
		 * NetStatusAdaption. In live-Streaming, we just take the contact-info
		 * layermap and inflate it to a BitSet spanning the current buffer size,
		 * so that PQA is satisfied...
		 */
		Map<UniqueID, BitSet> result = new LinkedHashMap<UniqueID, BitSet>();
		List<TransitConnection> incoming = node
				.getNeighborhood()
				.getConnectionManager()
				.getConnections(ConnectionDirection.IN, ConnectionState.OPEN,
						null, null);
		int offset = node.BLOCKS_PER_CHUNK * mostUrgendChunk;
		for (TransitConnection connection : incoming) {
			BitSet mask = (BitSet) node.getVideo()
					.getLayerMask(connection.getEndpoint().getLayer()).clone();
			BitSet chunks = new BitSet();
			for (int i = mask.nextSetBit(0); i >= 0; i = mask.nextSetBit(i + 1)) {
				for (int j = 0; j < StreamingConfiguration.BUFFER_SIZE; j++) {
					chunks.set(offset + (j * node.BLOCKS_PER_CHUNK) + i, true);
				}
			}

			result.put(connection.getEndpoint().getNodeID(), chunks);
		}

		return result;
	}

}
