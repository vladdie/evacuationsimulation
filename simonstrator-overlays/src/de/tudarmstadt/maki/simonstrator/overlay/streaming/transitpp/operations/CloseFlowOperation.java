/*
 * Copyright (c) 2005-2010 KOM � Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * 
 */
package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.operations;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.operation.AbstractOperation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitFlowSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.simple.TransitSimpleFlowManager;

/**
 * Operation for closing a flow. It can be used in such cases:
 * If we know that a request is just temporal: accept request even
 * though we don't have capacity. We ensure that the other flow is being
 * canceled after a short time and we ensure that this happens only once at a
 * time.
 * 
 * @author Eduardo Lidanski
 * @version 1.0, 29.05.2013
 */
public class CloseFlowOperation extends AbstractOperation<TransitNode, Object> {

	private final TransitNode node;

	private final TransitConnection toClose;

	private final TransitFlowSchedule scheduleToCancel;

	private final TransitSimpleFlowManager flowManager;

	public CloseFlowOperation(TransitNode component,
			OperationCallback<Object> callback, TransitConnection toClose,
			TransitFlowSchedule scheduleToCancel,
			TransitSimpleFlowManager transitSimpleFlowManager) {
		super(component, callback);
		this.node = component;
		this.toClose = toClose;
		this.scheduleToCancel = scheduleToCancel;
		this.flowManager = transitSimpleFlowManager;
	}

	@Override
	protected void execute() {
		if (!node.isPresent()) {
			return;
		}
		TransitConnection actualConn = node.getNeighborhood()
				.getConnectionManager().getConnection(toClose.getEndpoint());
		if (actualConn == null) {
			operationFinished(true);
			return;
		}
		if (actualConn.getScheduleState() != ScheduleState.ACTIVE) {
			operationFinished(true);
			return;
		}

		if (flowManager.getTemporalConnection() != toClose) {
			operationFinished(true);
			return;
		}

		TransitFlowSchedule actualSchedule = actualConn.getSchedule();

		if (actualSchedule.equals(scheduleToCancel)) {
			((TransitSimpleFlowManager) node.getScheduler().getFlowManager())
					.doDenySchedule(actualConn);
			flowManager.resetTemporalConnection();
			operationFinished(true);
			return;
		}

		BitSet alteredSchedule = new BitSet();
		alteredSchedule.or(actualSchedule.getBlockMask());

		for (int i = scheduleToCancel.getBlockMask().nextSetBit(0); i >= 0; i = scheduleToCancel
				.getBlockMask().nextSetBit(i + 1)) {
			alteredSchedule.set(i, false);
		}

		TransitFlowSchedule alter = new TransitFlowSchedule(alteredSchedule);
		((TransitSimpleFlowManager) node.getScheduler().getFlowManager())
				.doAlterSchedule(actualConn, alter);
		flowManager.resetTemporalConnection();
		operationFinished(true);
	}

	@Override
	public Object getResult() {
		return null;
	}

}
