/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming;


/**
 * Buffer-Interface for transit, used by the Player as well as by the scheduler
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 20.07.2012
 */
public interface BufferInterface {

	/**
	 * The Enum PlaybackState.
	 */
	public static enum PlaybackState {

		/** The none. */
		NONE,
		/** The playing. */
		PLAYING,
		/** The buffering. */
		BUFFERING
	}

	/**
	 * This is called by the player once in each chunk-interval. If the buffer
	 * is sufficiently filled, the method returns true and the playback moves
	 * one chunk ahead. Otherwise, it returns false and causes a stall.
	 * 
	 * This is <b>ONLY</b> to be called by the player.
	 * 
	 * @return
	 */
	public boolean tryToPlay(int chunk);

	/**
	 * Returns true, if the given chunk is available in the buffer at the
	 * quality that it was requested at.
	 * 
	 * @param chunk
	 * @return
	 */
	public boolean hasChunk(int chunk);

	/**
	 * The layer that the given chunk was requested at (or is available at)
	 * 
	 * @param chunk
	 * @return
	 */
	public byte[] getLayerForChunk(int chunk);

	/**
	 * Chunk that is to be played next (i.e. the most critical chunk in the
	 * buffer)
	 * 
	 * @return
	 */
	public int getBufferBegin();

	/**
	 * Returns the number of chunks in a row that are completely available
	 * starting with the chunk that is to be played next.
	 * 
	 * @return
	 */
	public int getCurrentBufferLength();

	/**
	 * Triggers a layer-switch, the given callback will be notified.
	 * 
	 * @param targetLayer
	 * @param callback
	 */
	public void doSwitchLayer(byte[] targetLayer, LayerSwitchCallback callback);

	/**
	 * Add a Listener to the Buffer
	 * 
	 * @param listener
	 */
	public void addBufferListener(BufferListener listener);

	/**
	 * Callback that is triggered as soon as the layer of the next chunk to be
	 * played is the requested layer. If the layer transition takes place in a
	 * stepwise manner, this might be called multiple times.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 20.07.2012
	 */
	public interface LayerSwitchCallback {

		/**
		 * Successfully switched to the newLayer starting with the next chunk.
		 * This might be called multiple times for one call to doSwitch.
		 * 
		 * @param newLayer
		 * @param firstChunkOnNewLayer
		 */
		public void onLayerSwitch(byte[] newLayer, int firstChunkOnNewLayer);

		/**
		 * The currently running layer-switching operation has ended (either
		 * successful or unsuccessful, depending on the calls of onLayerSwitch
		 * for this callback. If Layer switching was/is not possible, this
		 * method will be called directly without any prior call of
		 * onLayerSwitched.
		 */
		public void onLayerSwitchEnded();

	}

	/**
	 * Listener for Buffer-Related actions
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 07.08.2012
	 */
	public interface BufferListener {

		/**
		 * If the playback state changed, this listener is informed.
		 * 
		 * @param chunk
		 */
		public void onPlaybackStateChanged(PlaybackState oldState, PlaybackState newState, int chunk);

	}

}
