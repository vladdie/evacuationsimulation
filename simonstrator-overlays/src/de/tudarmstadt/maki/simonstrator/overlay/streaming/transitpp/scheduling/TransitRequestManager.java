/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitRequestMessage;

/**
 * Interface for the public part of the RequestManager in Transit. This manager
 * is used to coordinate the retrieval of missing blocks. Blocks might be
 * missing due to a switch in the parent for the Live Phase, but they migh also
 * occur due to message losses. In either case, depending on the strategy used
 * in this manager, missing blocks will be requested from other nodes.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public interface TransitRequestManager extends IPeerStatusListener {

	/**
	 * Adds a missing block to the request manager, which will then determine
	 * when and how to retrieve this block. This can safely be called multiple
	 * times for the same block.
	 * 
	 * @param blockOffset
	 * @param missingBlocks
	 */
	public void requestBlocks(int blockOffset, BitSet missingBlocks);

	/**
	 * Cancel the request for a given blocknumber, for example if a block
	 * meanwhile arrived via a flow. It is save to call this even for blocks
	 * that have not been requested at all.
	 * 
	 * @param blocknum
	 */
	public void cancelRequest(int blocknum);

	/**
	 * Called when the received block finally arrived. We should check if there
	 * are any open wait-requests for this block, as we need to forward the
	 * block to those peers.
	 * 
	 * @param message
	 * @param replyTo
	 *            optional reply handler
	 * @param isDuplicate
	 */
	public void receivedRequestedBlock(TransitBlockMessage message,
			TransitReplyHandler replyTo, boolean isDuplicate);

	/**
	 * Called when a {@link TransitRequestMessage} arrives at this node.
	 * 
	 * @param message
	 * @param replyTo
	 */
	public void receivedRequestMessage(TransitRequestMessage message,
			TransitReplyHandler replyTo);

}
