/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;

/**
 * Message to signalize the receiver that is has to change the connection
 * direction to the given direction.
 * 
 * @author Eduardo Lidanski
 * @version 1.0, 18.02.2014
 */
public class SetConnectionDirectionMessage extends AbstractTransitMessage {

	private ConnectionDirection switchToDirection;

	public SetConnectionDirectionMessage(TransitConnection connection,
			ConnectionDirection switchToDirection) {
		super(connection.getOwner());
		this.switchToDirection = switchToDirection;
	}

	public ConnectionDirection getSwitchToDirection() {
		return switchToDirection;
	}

	@Override
	public long getSize() {
		return super.getSize() + 1; // direction
	}

}
