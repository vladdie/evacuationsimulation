/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.requesting;

import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.RequestState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestManagerImpl;

/**
 * Basic version of a requestManager.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public class TransitRequestStrategyLoadBased extends AbstractRequestingStrategy {

	public boolean enableAllRequests = true;

	/**
	 * Instantiates a new simple request strategy.
	 *
	 * @param transitRequestManager the transit request manager
	 */
	public TransitRequestStrategyLoadBased(TransitRequestManagerImpl transitRequestManager) {
		super(transitRequestManager);
		enableAllRequests = getNode().getSettings().getParam(TransitParams.REQUEST_STRATEGY) == 1;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.requesting.RequestingStrategy#requestBlocks(int, java.util.BitSet)
	 */
	public void requestBlocks(int blockOffset, BitSet mask) {

		if (getNumberOfActiveRequests() > getNode().getSettings().getParam(TransitParams.REQUEST_MAX_PARALLEL)) {
			if (DEBUG) {
				System.out.println("currently max parallel requests ("
						+ getNumberOfActiveRequests() + ") open!");
			}
			return;
		}

		List<TransitConnection> connections = getNode()
				.getNeighborhood()
				.getConnectionManager()
				.getConnections(ConnectionDirection.IN, ConnectionState.OPEN, (enableAllRequests ? null : ScheduleState.ACTIVE),
						RequestState.INACTIVE);

		if( connections.size() == 0 ) {
			/* Failed. */
			if (DEBUG) {
				System.out
						.println("TransitRequestHandler: No Connections to place request on: "
								+ connections.toString());
			}
			return;
		}
		
		Collections.sort(connections, COMP_LOAD);
		

		for (TransitConnection connection : connections) {
			if (mask.isEmpty()) {
				break;
			}

			if (connection.getCurrentRTTEstimation() > getNode().getSettings().getTime(TransitTimes.REQUEST_RTT_MAX)) {
				if (DEBUG) {
					System.out
							.println("TransitRequestHandler: RTT too high on "
									+ connection.toString());
				}
				continue;
			}

			if (connection.getEndpoint().getLoad() > 6) {
				if (DEBUG) {
					System.out
							.println("TransitRequestHandler: Load on endpoint too high "
									+ connection.getEndpoint().toString());
				}
				continue;
			}

			assert connection.getRequestState() == RequestState.INACTIVE;
			assert connection.getRequest() == null;

			doRequestAndMaskRequest(connection, blockOffset, mask);
		
		}
		
	}
	
	/** The Constant COMP_LOAD:
	 * Compare hosts by load. */
	private static final Comparator<TransitConnection> COMP_LOAD = new Comparator<TransitConnection>() {
		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			if( o1.getEndpoint().getLoad() > o2.getEndpoint().getLoad() )
				return 1;
			
			if ( o1.getEndpoint().getLoad() < o2.getEndpoint().getLoad() )
				return -1;
			
			return 0;			
		}
	};
}
