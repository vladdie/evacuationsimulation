/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitFlowSchedule;

/**
 * A message implementing this Interface may contain a new
 * {@link TransitFlowSchedule} or participate in the overall flow negotiation
 * protocol.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 08.06.2012
 */
public interface TransitFlowMessage extends TransitMessage {

	/**
	 * Enumeration defining the meaning of the message.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 08.06.2012
	 */
	public static enum FlowMsgType {
		/**
		 * This schedule is a request for the given blocks. If the schedule is
		 * not fulfilled by the receiver within a given time, a CANCEL or an
		 * altered REQUEST might be issued. Upstream Operation.
		 */
		REQUEST,
		/**
		 * This schedule is an (optional) reply to a REQUEST-schedule. It
		 * signals the receiver that the sender is not able to fulfill the
		 * original REQUEST but is able to fulfill parts of it. It may contain
		 * additional, not requested blocks (just as an OFFER would). Downstream
		 * Operation.
		 */
		REPLY,
		/**
		 * This is issued by a sender if he is no longer able to provide the
		 * full schedule but maybe a subset and knows this in advance. This is
		 * an optional message, i.e. the sender might rely on the receiver to
		 * detect missing blocks and issue a new REQUEST accordingly. If an
		 * ALTER would contain no blocks, a DENY is issued instead. If a
		 * receiver wants to update the schedule, he would issue a new REQUEST!
		 * Downstream Operation.
		 */
		ALTER,
		/**
		 * This is an offer announcing spare capacity to neighbors in terms of a
		 * possible schedule. Nodes receiving this schedule decide if they are
		 * interested and may issue a REQUEST (maybe only for parts of the
		 * OFFER). Downstream Operation.
		 */
		OFFER,
		/**
		 * This is a notification that the schedule has been canceled and the
		 * node no longer wants to receive any blocks. If the schedule is only
		 * to be changed, a new REQUEST would be issued instead of this CANCEL.
		 * Upstream Operation.
		 */
		CANCEL,
		/**
		 * This is a hard deny of a REQUEST, in case of a node not having any
		 * spare capacities. It is a special case of the REPLY, where there is
		 * no blockMask issued. It can also be used as a special case of the
		 * ALTER-type, where the source no longer provides ANY packets.
		 * Downstream Operation.
		 */
		DENY,
		/**
		 * Request to parent node, which will replace the given node with the
		 * requesting node.
		 */
		REQUEST_REPLACE_CHILD,
		/**
		 * Instructs the receiver to issue a request to the sender.
		 */
		REQUEST_FROM_ME_COMMAND;
	}

	/**
	 * The schedule
	 * 
	 * @return
	 */
	public TransitFlowSchedule getFlowSchedule();

	/**
	 * The intention of this message, as defined in {@link FlowMsgType}
	 * 
	 * @return
	 */
	public FlowMsgType getType();

	/**
	 * If message of type REQUEST_REPLACE_CHILD: child to replace.
	 * 
	 * @return
	 */
	public TransitContact getNodeToReplace();

	/**
	 * Two times the RTT between the sender and the child to replace (if message
	 * of type REQUEST_REPLACE_CHILD).
	 * 
	 * @return
	 */
	public long getCalculatedRTT();

}
