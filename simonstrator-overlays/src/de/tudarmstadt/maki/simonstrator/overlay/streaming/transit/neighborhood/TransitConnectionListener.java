/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;

/**
 * Listener that gets notified upon changes with {@link TransitConnection}s.
 * This is dispatched via the {@link TransitConnectionManager} and interested
 * parties have to register via the {@link TransitNeighborhood}-API.
 * 
 * Each method in this listener contains the Connection as a parameter which
 * allows dedicated handling of only a subset of all connections.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 15.06.2012
 */
public interface TransitConnectionListener {

	/**
	 * Called whenever a {@link ConnectionState} changes. This includes state
	 * changes that arise from newly formed connections as well.
	 * 
	 * @param connection
	 * @param oldState
	 * @param newState
	 */
	public void onConnectionStateChanged(TransitConnection connection,
			ConnectionState oldState, ConnectionState newState);

}
