/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive;

/**
 * The Class TransitServingAdvise.
 */
public class TransitServingAdvise {

	/**
	 * The Enum ServingAdvise.
	 */
	public enum ServingAdvise { 
		A_SERVE_FULL, /** Serve full request. */ 
		A_SERVE_BASELAYER, /** Serve  SVC base layer only. */
		A_SERVE_WITH_PROBABILITY, /** Serve full request with prob. */
		A_DO_NOT_SERVE, /** The a do not serve. */
		A_DO_NOT_SERVE_AND_CLOSE /** We won't serve anymore. Might as well just kill it. */
	}
	
	/** The advise. */
	private final ServingAdvise advise;
	
	/** The probability. */
	private double probability;

	/**
	 * Instantiates a new transit serving advise.
	 *
	 * @param advise the advise
	 */
	public TransitServingAdvise(ServingAdvise advise) {
		this.advise = advise;
	}

	/**
	 * Instantiates a new transit serving advise.
	 *
	 * @param advise the advise
	 * @param probability the probability
	 */
	public TransitServingAdvise(ServingAdvise advise, double probability) {
		this.advise = advise;
		this.probability = probability;
	}

	/**
	 * Gets the serving advise.
	 *
	 * @return the serving advise
	 */
	public ServingAdvise getServingAdvise() {
		return advise;
	}

	/**
	 * Gets the serving probability.
	 *
	 * @return the serving probability
	 */
	public double getServingProbability() {
		return probability;
	}
	
	@Override
	public String toString() {
		return "Advise = " + this.advise.name();
	}
	
	
}
