/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.serving;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageCallback;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.SendBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.SendRequestMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitRequestMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitRequestMessage.RequestMsgType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.RequestState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestManagerImpl;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.serving.TransitPPServStrategyAscendingID;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.serving.TransitPPServStrategyRandom;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.serving.TransitPiecePickingServingStrategy;

public abstract class AbstractServingStrategy implements ServingStrategy, TransMessageCallback {

	protected final TransitRequestManagerImpl transitRequestManager;
	protected TransitPiecePickingServingStrategy piecePickingServingStrategy;

	public AbstractServingStrategy(TransitRequestManagerImpl transitRequestManager) {
		this.transitRequestManager = transitRequestManager;

		final int servingStrategy = transitRequestManager.getNode().getSettings()
		        .getParam(TransitParams.REQUESTS_SERVING_STRATEGY);
		if (servingStrategy == 0) {
			this.piecePickingServingStrategy = new TransitPPServStrategyAscendingID();
		} else if (servingStrategy == 1) {
			this.piecePickingServingStrategy = new TransitPPServStrategyRandom();
		} else {
			throw new AssertionError("Unknown REQUESTS_SERVING_STRATEGY selected.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tud.kom.p2psim.api.transport.TransMessageCallback#receive(de.tud.kom
	 * .p2psim.api.common.Message, de.tud.kom.p2psim.api.transport.TransInfo,
	 * int)
	 */
	@Override
	public void receive(Message msg, TransInfo senderInfo, int commId) {
		// Not in use.
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tud.kom.p2psim.api.transport.TransMessageCallback#messageTimeoutOccured
	 * (int)
	 */
	@Override
	public void messageTimeoutOccured(int commId) {
		// Not in use.
	}

	/**
	 * Do send block.
	 * 
	 * @param connection
	 *            the connection
	 * @param blocknum
	 *            the blocknum
	 */
	protected void doSendBlock(TransitConnection connection, int blocknum) {

		final TransitNode node = transitRequestManager.getNode();

		assert transitRequestManager.getNode().getVideo().haveBlock(blocknum);

		byte[] data = null;
		int blockSize = node.getVideo().getBlockSize(blocknum);
		if (node.getVideo().getStreamingData() != null
				&& node.getVideo().getStreamingData().getSouthboundInterface()
						.hasDataForBlock(blocknum)) {
			data = node.getVideo().getStreamingData().getSouthboundInterface()
					.getDataForBlock(blocknum);
		}

		SendBlockMessage blockMsg = new SendBlockMessage(
				node.getLocalOverlayContact(), blocknum, blockSize, false, data);
		connection.blockTransferred(blocknum, blockMsg.getSize(), false);
		node.getMessageHandler().sendAndWait(blockMsg, connection.getEndpoint(), this);
	}

	/**
	 * Do deny request.
	 * 
	 * @param connection
	 *            the connection
	 */
	protected void doDenyRequest(TransitConnection connection) {
		TransitRequestMessage deny = new SendRequestMessage(transitRequestManager.getNode().getLocalOverlayContact(),
		        RequestMsgType.DENY_CAPACITY, connection.getRequest());
		transitRequestManager.getNode().getMessageHandler().send(deny, connection.getEndpoint());
		connection.setRequest(null);
		connection.setRequestState(RequestState.CANCELED);
	}

	/**
	 * Do send altered request.
	 * 
	 * @param connection
	 *            the connection
	 */
	protected void doAlterRequest(TransitConnection connection, TransitRequestSchedule newSchedule) {
		TransitRequestMessage alteredReq = new SendRequestMessage(transitRequestManager.getNode()
		        .getLocalOverlayContact(), RequestMsgType.ALTERED_REQUEST, newSchedule);
		transitRequestManager.getNode().getMessageHandler().send(alteredReq, connection.getEndpoint());
		connection.setRequest(newSchedule);
	}

	/**
	 * Just a queue-entry waiting to be sent
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 13.09.2012
	 */
	class ServeRequestWorker implements EventHandler {

		private final TransitConnection connection;

		private final BitSet requestMap;

		private final int offset;

		/**
		 * Instantiates a new serve request worker.
		 * 
		 * @param connection
		 *            the connection
		 */
		public ServeRequestWorker(TransitConnection connection) {
			this.connection = connection;
			this.requestMap = connection.getRequest().getBlockMap();
			this.offset = connection.getRequest().getBlockOffset();
			sendBlock();
		}

		/**
		 * Send block.
		 */
		private void sendBlock() {
			if (!transitRequestManager.getNode().isPresent()) {
				return;
			}

			if (connection.getRequest() == null) {
				return;
			}

			int i = piecePickingServingStrategy.getNextBit(requestMap);
			if (i >= 0 && connection.getState() == ConnectionState.OPEN) {

				/*
				 * RTTs and out
				 */
				doSendBlock(connection, offset + i);
				requestMap.set(i, false);
				long delay = connection.getCurrentRTTEstimation() / 2;
				if (!requestMap.isEmpty()) {
					Event.scheduleWithDelay(delay, this, null, 0);
				} else {
					connection.setRequest(null);
					connection.setRequestState(RequestState.INACTIVE);
				}

			} else {
				connection.setRequest(null);
				connection.setRequestState(RequestState.INACTIVE);
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * de.tud.kom.p2psim.api.simengine.SimulationEventHandler#eventOccurred
		 * (de.tud.kom.p2psim.impl.simengine.SimulationEvent)
		 */
		@Override
		public void eventOccurred(Object content, int type) {
			sendBlock();
		}
	}

}
