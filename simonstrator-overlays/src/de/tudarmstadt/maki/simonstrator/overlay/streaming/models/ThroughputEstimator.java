package de.tudarmstadt.maki.simonstrator.overlay.streaming.models;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.VideoModel;

/**
 * 
 * @author unknown
 * @version 1.0, 08.08.2012
 * 
 * @deprecated Please use the more generic {@link BandwidthEstimator} instead!
 */

@Deprecated
public class ThroughputEstimator {

	private final long[] inTransRing = new long[StreamingConfiguration.getThroughputEstimationInterval()];

	private final long[] outTransRing = new long[StreamingConfiguration.getThroughputEstimationInterval()];

	private long inLastEditInSec = -1;
	private long outLastEditInSec = -1;

	private long inTransBytes = 0;
	private long outTransBytes = 0;

	private final VideoModel videomodel;

	public ThroughputEstimator(VideoModel videomodel) {
		this.videomodel = videomodel;
	}

	public void addIncomingBlockTransmission(int blocknum) {
		long size = videomodel.getBlockSize(blocknum);
		long currentTimeInSec = Time.getCurrentTime() / Time.SECOND;
		int index = (int) (currentTimeInSec % inTransRing.length);
		if (currentTimeInSec > inLastEditInSec) {
			if (currentTimeInSec - inLastEditInSec > inTransRing.length) {
				/*
				 * There was no transmission for a long time --> Reset of ring
				 * is the best.
				 */
				for (int i = 0; i < inTransRing.length; i++) {
					inTransRing[i] = 0;
				}
				inTransBytes = 0;
			} else {
				for (long i = inLastEditInSec + 1; i <= currentTimeInSec; i++) {
					int eraseIndex = (int) (i % inTransRing.length);
					inTransBytes -= inTransRing[eraseIndex];
					inTransRing[eraseIndex] = 0;
				}
			}
		}
		inTransRing[index] += size;
		inTransBytes += size;
		inLastEditInSec = currentTimeInSec;
	}

	public void addOutgoingBlockTransmission(int blocknum) {
		long size = videomodel.getBlockSize(blocknum);
		long currentTimeInSec = Time.getCurrentTime() / Time.SECOND;
		int index = (int) (currentTimeInSec % outTransRing.length);
		if (currentTimeInSec > outLastEditInSec) {
			if (currentTimeInSec - outLastEditInSec > outTransRing.length) {
				/*
				 * There was no transmission for a long time --> Reset of ring
				 * is the best.
				 */
				for (int i = 0; i < outTransRing.length; i++) {
					outTransRing[i] = 0;
				}
				outTransBytes = 0;
			} else {
				for (long i = outLastEditInSec + 1; i <= currentTimeInSec; i++) {
					int eraseIndex = (int) (i % outTransRing.length);
					outTransBytes -= outTransRing[eraseIndex];
					outTransRing[eraseIndex] = 0;
				}
			}
		}
		outTransRing[index] += size;
		outTransBytes += size;
		outLastEditInSec = currentTimeInSec;
	}

	/**
	 * @return the average download throughput [kbps] over the last
	 *         THROUGHPUT_ESTIMATION_INTERVAL seconds. This interval is
	 *         configurable via the properties file.
	 */
	public float getAverageDownThroughput() {
		long currentTimeInSec = Time.getCurrentTime() / Time.SECOND;
		if (inLastEditInSec == -1 || currentTimeInSec - inLastEditInSec > inTransRing.length)
			return 0;
		for (long i = inLastEditInSec + 1; i <= currentTimeInSec; i++) {
			int eraseIndex = (int) (i % inTransRing.length);
			inTransBytes -= inTransRing[eraseIndex];
			inTransRing[eraseIndex] = 0;
		}
		return ((float) inTransBytes / inTransRing.length) * 8 / 1000;
	}

	/**
	 * @return the average upload throughput [kbps] over the last
	 *         THROUGHPUT_ESTIMATION_INTERVAL seconds. This interval is
	 *         configurable via the properties file.
	 */
	public float getAverageUpThroughput() {
		long currentTimeInSec = Time.getCurrentTime() / Time.SECOND;
		if (outLastEditInSec == -1 || currentTimeInSec - outLastEditInSec > outTransRing.length)
			return 0;
		for (long i = outLastEditInSec + 1; i <= currentTimeInSec; i++) {
			int eraseIndex = (int) (i % outTransRing.length);
			outTransBytes -= outTransRing[eraseIndex];
			outTransRing[eraseIndex] = 0;
		}
		return ((float) outTransBytes / outTransRing.length) * 8 / 1000;
	}
}
