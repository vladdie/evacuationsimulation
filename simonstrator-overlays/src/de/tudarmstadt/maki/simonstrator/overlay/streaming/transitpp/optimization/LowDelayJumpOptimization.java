/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.optimization;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNodeInfo;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitOptimizationManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnectionManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitFlowSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.simple.TransitSimpleFlowManager;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * Low delay jump optimization as defined in Lidanski MA.
 * @author Eduardo Lidanski
 * @version 1.0, 14.04.2014
 */
public class LowDelayJumpOptimization implements TopologyOptimization {

	private TransitNode node;

	private TransitConnectionManager connectionManager;

	private TransitSimpleFlowManager flowManager;

	/* The chosen optimization, i.e. the best possible optimization. */
	private PossibleOptimizationAction chosen;

	private TransitOptimizationManager optimizationManager;

	/* Layer -> Last tried connections */
	private Map<Integer, TimeoutSet<TransitConnection>> lastTried = new HashMap<Integer, TimeoutSet<TransitConnection>>();

	private int gainThreshold = 1;

	/* Which trees shall we optimize */
	private Set<Integer> optimizeTrees = new HashSet<Integer>();

	/*
	 * Which was the last tree optimized? We need this in order to not only
	 * optimize one tree, but all of them, in a Round-Robin way.
	 */
	private int lastOptimizedTree = -1;

	public LowDelayJumpOptimization(TransitNode node) {
		this.node = node;

		for (int i = 0; i <= 3; i++) {
			lastTried.put(i, new TimeoutSet<TransitConnection>(2 * Time.SECOND));
		}
		/*
		 * Define trees to be optimized here
		 * 
		 * FIXME[JR]: Should be changed to dynamic detection of available trees
		 * and use all of them by default.
		 */
		optimizeTrees.add(0);
		optimizeTrees.add(1);
		optimizeTrees.add(2);
		optimizeTrees.add(3);
	}

	@Override
	public boolean canOptimize() {

		this.optimizationManager = node.getOptimizationManager();

		this.connectionManager = node.getNeighborhood().getConnectionManager();
		this.flowManager = (TransitSimpleFlowManager) node.getScheduler()
				.getFlowManager();

		List<TransitConnection> neighbors = connectionManager.getConnections(
				null, ConnectionState.OPEN, null, null);
		List<PossibleOptimizationAction> possibleOptimizations = new ArrayList<PossibleOptimizationAction>();

		int[] myDepths = flowManager.getDepth();
		BitSet incomingFlow = flowManager.getIncomingFlow().getHealthyFlow();
		for (int i = incomingFlow.nextSetBit(0); i >= 0; i = incomingFlow
				.nextSetBit(i + 1)) {

			for (TransitConnection neighbor : neighbors) {
				TransitNodeInfo nodeInfo = neighbor.getNodeInfo();
				if (nodeInfo == null) {
					continue;
				}
				BitSet nodeCapacity = nodeInfo.getCapacities();
				if (nodeCapacity.get(i) == true) {

					if (!optimizeTrees.contains(i)) {
						continue;
					}

					/* Only internal nodes are allowed to jump */
					if (node.getScheduler().getFlowManager()
							.getNumberOfChildren()[i] == 0) {
						return false;
					}

					/* The node can send us this flow. */
					if (myDepths[i] - 1 > nodeInfo.getDepth()[i]
							&& nodeInfo.getDepth()[i] != -1) {

						/*
						 * Is the connection in the right direction? If not:
						 * send a correct-direction message and keep trying with
						 * other nodes. But only if we are not sending a flow to
						 * this neighbor.
						 */
						if (neighbor.getDirection() == ConnectionDirection.OUT) {
							if (!flowManager.getOutgoingFlow().isChild(
									neighbor.getEndpoint(), i)) {
								connectionManager.changeDirection(neighbor);
							}
							continue;
						}

						/*
						 * Are we receiving (another) flow from this neighbor?
						 * If yes, we cannot jump to this neighbor.
						 * 
						 * FIXME[JR]: This is due to the fact that in transit
						 * you cannot ask for more layers if you are already
						 * receiving some layers from a peer. This may be
						 * changed in future. For now: we cannot jump to this
						 * node.
						 */
						if (neighbor.getDirection() == ConnectionDirection.IN
								&& (neighbor.getScheduleState() == ScheduleState.ACTIVE)
								|| neighbor.getScheduleState() == ScheduleState.NEGOTIATING) {
							continue;
						}

						/* No change of parent if we jump */
						TransitConnection actualParent = flowManager
								.getIncomingFlow().getConnectionForFlow(i);
						if (neighbor
								.getEndpoint().getNodeID()
								.equals(actualParent.getEndpoint().getNodeID())) {
							continue;
						}

						/* We just tried with this one. */
						if (lastTried.get(i).contains(neighbor)) {
							continue;
						}

						/* We can jump to this node */
						PossibleLowDelayJumpOptimization possible = new PossibleLowDelayJumpOptimization(
								neighbor, i, myDepths[i],
								nodeInfo.getDepth()[i]);
						if (possible.getGain() >= gainThreshold) {
							possibleOptimizations.add(possible);
						}

					}
				}
			}
		}

		if (possibleOptimizations.isEmpty()) {
			return false;
		}

		/*
		 * Now we have a list of possible optimizations. Choose the best one.
		 * LayerID->Possible OptimizationsAction
		 */
		Map<Integer, PossibleOptimizationAction> best = new HashMap<Integer, PossibleOptimizationAction>();
		for (PossibleOptimizationAction possible : possibleOptimizations) {
			Integer layer = possible.getLayer();
			double gain = possible.getGain();
			if (!best.containsKey(layer)) {
				best.put(layer, possible);
				
				if (node.getSettings().getParam(TransitParams.DO_OPTIMIZE) == 1) {
					/* We continue looking for the best optimization. */
					continue;
				} else if (node.getSettings().getParam(TransitParams.DO_OPTIMIZE) == 2) {
					/* We return the first optimization found. */
					chosen = possible;
					return true;
				}
			}

			if (gain > best.get(layer).getGain()) {
				best.put(layer, possible);
			}
		}

		chooseBest(best);
		if (chosen != null) {
			return true;

		} else {
			return false;
		}
	}

	private void chooseBest(Map<Integer, PossibleOptimizationAction> actions) {

		double bestGain = -1;

		Iterator<Integer> it = actions.keySet().iterator();
		while (it.hasNext()) {
			Integer next = it.next();
			PossibleOptimizationAction action = actions.get(next);
			if (action.getGain() > bestGain) {
				bestGain = action.getGain();
			}
		}

		chosen = null;

		boolean didChose = false;
		int counter = (lastOptimizedTree + 1) % 4;
		while (!didChose) {
			if (actions.containsKey(counter)
					&& actions.get(counter).getGain() == bestGain) {
				chosen = actions.get(counter);
				lastOptimizedTree = counter;
				didChose = true;
			}
			counter = (counter + 1) % 4;
		}

	}

	@Override
	public long doOptimize() {

		TransitConnection actualParent = flowManager.getIncomingFlow()
				.getConnectionForFlow(chosen.getLayer());

		if (actualParent == null) {
			return 0;
		}

		if (actualParent != null && actualParent.getEndpoint() == null) {
			return 0;
		}

		if (chosen.getTargetConnection() == null) {
			return 0;
		}
		if (chosen.getTargetConnection().getNodeInfo() == null) {
			return 0;
		}

		if (TransitOptimizationManager.DEBUG) {
			System.out
					.println("["
 + node.getHost().getId()
							+ "] "
							+ Time.getFormattedTime(Time
									.getCurrentTime())
							+ " Doing optimization: node ["
 + node.getHost().getId()
							+ "] tries to jump from "
							+ actualParent.getEndpoint().getNodeID()
							+ "  to "
							+ chosen.getTargetConnection().getEndpoint()
									.getNodeID()
							+ "("
							+ chosen.getLayer()
							+ "), Gain: "
							+ chosen.getGain()
							+ "--> depth: "
							+ node.getScheduler().getFlowManager().getDepth()[chosen
									.getLayer()]
							+ ", new parent depth: "
							+ chosen.getTargetConnection().getNodeInfo()
									.getDepth()[chosen.getLayer()]);

		}

		optimizationManager.setDoubleParents(actualParent,
				chosen.getTargetConnection(), chosen.getLayer());

		BitSet requestBitSet = new BitSet();
		requestBitSet.set(chosen.getLayer());
		lastTried.get(chosen.getLayer()).addNow(chosen.getTargetConnection());
		TransitFlowSchedule request = new TransitFlowSchedule(requestBitSet);
		flowManager.doRequest(request, chosen.getTargetConnection());

		long wait = chosen.getTargetConnection().getCurrentRTTEstimation()
				+ (long) Math.ceil(((double) actualParent
						.getCurrentRTTEstimation()) / 2);
		// long wait = 15 * Time.SECOND;

		if (TransitOptimizationManager.DEBUG) {
			System.out.println("[" + node.getHost().getId() + "] waiting "
					+ Time.getFormattedTime(wait) + " for "
					+ chosen.getTargetConnection().getEndpoint().getNodeID()
					+ "(" + chosen.getLayer() + ")");
		}

		chosen = null;
		return wait;
	}

	@Override
	public long onFlowReply(TransitConnection conn) {
		/* We finished optimizing. */
		optimizationManager.setActualOptimization(null);
		
		/* We just had an optimization! Wait at least one minute for the next one! */
		return Time.MINUTE;
	}

	@Override
	public long onFlowDeny(TransitConnection conn) {
		/* We cannot optimize. */
		optimizationManager.setActualOptimization(null);
		
		return 0;
	}

}
