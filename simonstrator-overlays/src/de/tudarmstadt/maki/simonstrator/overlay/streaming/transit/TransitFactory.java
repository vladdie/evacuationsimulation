/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.common.graph.INodeID;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.transport.MessageBasedTransport;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.tracker.TransitTracker;

/**
 * This factory creates all components that are configurable through the
 * simulator.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public class TransitFactory implements HostComponentFactory {

	private static final int DEFAULT_NODE_PORT = 55555;

	private static final int SERVER_PORT = 60999;

	public static final int DEFAULT_TRACKER_PORT = 60998;

	public static final int DEFAULT_TRACKER_ID = 0;

	public static final long TICK_INTERVAL = 120 * Time.MILLISECOND;

	/**
	 * TransportInfo of Tracker to be used (for non-simulations)
	 */
	private static String trackerAddress = null;

	/**
	 * TransportInfo of Tracker to be used (for non-simulations)
	 */
	private static int trackerPort = -1;

	/**
	 * Keep reference to tracker contact (to not generate it multiple times)
	 */
	private static TransitContact trackerContact = null;

	/**
	 * Tracker reference (only interesting for simulation)
	 */
	public static TransitTracker tracker = null;

	private static enum FactoryType {
		SERVER, NODE, TRACKER, DEBUGNODE, NONODE
	}

	private TransitSettings settings;

	private FactoryType type = null;

	/**
	 * Create a factory that produces components of the given type, which must
	 * be one out of {NODE, TRACKER, SERVER, DEBUGNODE}
	 * 
	 * @param factoryType
	 */
	public TransitFactory() {
		//
	}

	public void setFactoryType(String factoryType) {
		type = FactoryType.valueOf(factoryType);
		if (type == null) {
			throw new AssertionError("FactoryType must be one of {NODE, TRACKER, DEBUGNODE, SERVER}");
		}
	}

	/**
	 * Allows the configuration of settings on a per-host basis
	 * 
	 * @param settings
	 */
	public void setSettings(TransitSettings settings) {
		this.settings = settings;
	}

	public void setDefaultTracker(String address, int port) {
		trackerAddress = address;
		trackerPort = port;
	}

	@Override
	public OverlayComponent createComponent(Host host) {

		if (settings == null) {
			System.err.println("You did not configure any settings for Transit. Default Settings will be used!");
			settings = new TransitSettings();
		}

		if (type == null) {
			throw new AssertionError(
			        "You have to specify a type for the factory, by setting factoryType to one of {SERVER, NODE, TRACKER, DEBUGNODE}");
		}

		UniqueID id;

		switch (type) {

		case SERVER:
			TransitNode server = new TransitNode(host, SERVER_PORT, true,
					settings.clone());
			return server;

		case NODE:
			TransitNode node = new TransitNode(host, DEFAULT_NODE_PORT,
			// (DEFAULT_NODE_PORT + (int) Math.abs(host.getHostId() % 1000)),
					false, settings.clone());
			return node;

		case TRACKER:
			if (tracker != null)
				throw new AssertionError("There can be only one tracker!");

			tracker = new TransitTracker(host, DEFAULT_TRACKER_PORT);
			return tracker;
		default:
			break;
		}
		throw new AssertionError("Unable to create component...");
	}

	/**
	 * @param transport
	 * @return the contact information of the tracker
	 */
	public static TransitContact getTrackerContact(TransitNode node) {

		MessageBasedTransport transport = node.getMessageHandler().getTransport();

		if (trackerContact != null)
			return trackerContact;

		if (trackerAddress != null && trackerPort > -1) {
			// Use trackerAddress to contact tracker
			trackerContact = new TransitContact(
					INodeID.get(DEFAULT_TRACKER_ID),
					transport.getTransInfo(transport.getNetInterface()
							.getByName(TransitFactory.trackerAddress),
							trackerPort), ContactType.TRACKER);

		} else if (TransitFactory.tracker != null) {
			// Use tracker instance (most likely a simulation)
			trackerContact = TransitFactory.tracker.getLocalOverlayContact();

		} else {
			throw new AssertionError("No Tracker found!");

			// For simulations we could also use the GlobalOracle

			// TransitTracker tracker = null;
			// for (Host host : Oracle.getAllHosts()) {
			// try {
			// tracker = host.getComponent(TransitTracker.class);
			// break;
			// } catch (ComponentNotAvailableException e) {
			// continue;
			// }
			// }
		}
		return trackerContact;
	}
}
