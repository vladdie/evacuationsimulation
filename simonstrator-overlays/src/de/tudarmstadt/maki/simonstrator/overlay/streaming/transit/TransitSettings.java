/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.operations.ConnectionControlOperation;


/**
 * All parameters, be they adaptive or fixed, are accessed via this component of
 * a {@link TransitNode}. This enables a better overview on all possible tweaks
 * and configurations.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 23.09.2012
 */
public class TransitSettings implements Cloneable {

	private final long[] times = new long[TransitTimes.values().length];

	private final int[] params = new int[TransitParams.values().length];

	/**
	 * Generated for each node, as some settings might be adaptive and change
	 * over time
	 */
	public TransitSettings() {
		// Default values
		for (TransitTimes time : TransitTimes.values()) {
			times[time.ordinal()] = time.getDefault();
		}
		for (TransitParams param : TransitParams.values()) {
			params[param.ordinal()] = param.getDefault();
		}
	}

	/**
	 * Copy-constructor
	 * 
	 * @param toClone
	 */
	private TransitSettings(TransitSettings toClone) {
		int i = 0;
		for (long value : toClone.times) {
			times[i] = value;
			i++;
		}
		i = 0;
		for (int value : toClone.params) {
			params[i] = value;
			i++;
		}
	}

	/**
	 * Read the current value of the parameter with the given name
	 * 
	 * @param name
	 * @return
	 */
	public int getParam(TransitParams name) {
		return params[name.ordinal()];
	}

	/**
	 * Sets the given parameter
	 * 
	 * @param name
	 * @param value
	 */
	public void setParam(TransitParams name, int value) {
		params[name.ordinal()] = value;
	}

	/**
	 * Get the current time that is associated to the setting specified by the
	 * enum TransitTimes
	 * 
	 * @param name
	 * @return
	 */
	public long getTime(TransitTimes name) {
		return times[name.ordinal()];
	}

	/**
	 * Update the value of a setting describing a time in Transit
	 * 
	 * @param name
	 * @param value
	 */
	public void setTime(TransitTimes name, long value) {
		times[name.ordinal()] = value;
	}

	/**
	 * Manually overwrite some config parameters by specifying an attribute in
	 * the form overwrite="propname:value;propname:value", might be usefull for
	 * batch processing of minor config variations
	 * 
	 * @param toOverwrite
	 */
	public void setOverwrite(String toOverwrite) {
		String[] props = toOverwrite.split(";");
		for (String prop : props) {
			String[] val = prop.split(":");
			parseProperty(val[0], val[1]);
		}
	}

	private Properties defaultProperties = null;

	private boolean loadedProps = false;

	/**
	 * Allows to specify a settings file for default values other than the ones
	 * specified in the code below. This default file can then be overwritten
	 * with another properties file via setFile.
	 * 
	 * @param filename
	 */
	public void setDefaults(String filename) {
		if (loadedProps) {
			throw new AssertionError(
					"Configure setDefaults before calling setFile!");
		}
		defaultProperties = new Properties();
		BufferedInputStream bin = null;
		try {
			bin = new BufferedInputStream(new FileInputStream(filename));
			defaultProperties.load(bin);
			bin.close();
		} catch (Exception e) {
			throw new AssertionError(e.getMessage());
		}
		parseProperties(defaultProperties);
	}

	/**
	 * Can be used to pass a .properties-file that contains values to overwrite
	 * the default values of this settings-class.
	 * 
	 * @param filename
	 */
	public void setFile(String filename) {
		loadedProps = true;
		Properties props = null;
		if (defaultProperties != null) {
			props = new Properties(defaultProperties);
		} else {
			props = new Properties();
		}
		BufferedInputStream bin = null;
		try {
			bin = new BufferedInputStream(
					new FileInputStream(filename));
			props.load(bin);
			bin.close();
		} catch (Exception e) {
			throw new AssertionError(e.getMessage());
		}
		parseProperties(props);
	}

	/**
	 * Writes the values set in the properties into the settings for transit
	 * 
	 * @param props
	 */
	private void parseProperties(Properties props) {
		Set<String> names = props.stringPropertyNames();
		for (String name : names) {
			parseProperty(name, props.getProperty(name));
		}
	}

	/**
	 * Set the given property
	 * 
	 * @param name
	 * @param value
	 */
	private void parseProperty(String name, String value) {
		try {
			params[TransitParams.valueOf(name).ordinal()] = Integer
					.parseInt(value);
		} catch (IllegalArgumentException e) {
			//
		}
		try {
			times[TransitTimes.valueOf(name).ordinal()] = Time.parseTime(value);
		} catch (IllegalArgumentException e) {
			//
		}
	}

	/**
	 * All parameters used (and adapted) in Transit
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 23.09.2012
	 */
	public enum TransitParams {

		/**
		 * Percentage of the total bandwidth that transit is allowed to use
		 * (both up and down)
		 */
		BANDWIDTH_MAX_SHARE(90),

		/**
		 * Number of chunks that are considered urgent starting from the current
		 * playback position. A too high value will result in duplicates due to
		 * the live-flows, whereas a too low value will lead to stalls if some
		 * blocks are missing and can not be delivered in time.
		 */
		REQUEST_BUFFER_URGENT_SIZE(15),

		/**
		 * Max parallel open requests
		 */
		REQUEST_MAX_PARALLEL(16),

		/**
		 * Number of blocks that are requested at once over a connection
		 * (requests may directly contain more than one block)
		 */
		REQUEST_MAX_BLOCKS_PER_CONNECTION(2),

		/**
		 * Number of blocks that are requested in parallel from a source
		 */
		REQUEST_MAX_BLOCKS_PER_CONNECTION_SOURCE(4),

		/**
		 * Size of the buffermaps that are sent in CHUNKS, not blocks!
		 */
		REQUEST_BUFFERMAP_SIZE(50),

		/**
		 * When requesting a block, the request manager might additionally
		 * request this number of newest blocks from the buffermap. Only useful
		 * in a mesh, helps distributing new packets faster.
		 */
		REQUEST_NEWEST_BLOCKS(0),

		/**
		 * Maximum ticks (chunk intervals) after which a flow is considered
		 * dead, if no new block arrives.
		 */
		FLOW_MAX_LIVE_CHUNK_DELAY(3),

		/**
		 * Max variance between the most recent chunk on different flows (nodes
		 * try to maintain roughly the same positions in all flow trees)
		 * 
		 * TODO evaluate the impact of this parameter
		 */
		FLOW_MAX_LIVE_CHUNK_VARIANCE(16),

		/**
		 * Max number of concurrently active incoming flows, set to zero to
		 * disable flows completely.
		 */
		FLOW_MAX_INCOMING(10000),

		/**
		 * Limits the width of a flow on one connection. A flow can include this
		 * amount of layers.
		 */
		FLOW_MAX_PER_CONNECTION(10000),

		/**
		 * Max outgoing connections
		 */
		CONNECTION_MAX_OUTGOING(32),
		
		/**
		 * Max incoming connections (active + inactive)
		 */
		CONNECTION_MAX_INCOMING(16),

		/**
		 * Max idle incoming connections (not used for at least
		 * CONNECTION_INACTIVITY_TIMEOUT)
		 */
		CONNECTION_MAX_INCOMING_INACTIVE(8),

		/**
		 * The Connection manager tries to aggressively maintain at least this
		 * number of incoming connections.
		 */
		CONNECTION_MIN_INCOMING(2),

		/**
		 * Probability, that an inactive connection is killed (it has at least
		 * been inactive for CONNECTION_INACTIVITY_TIMEOUT). Enter a percentage
		 * (int between 0 and 100)
		 */
		CONNECTION_INACTIVITY_KILL_PROBABILITY(0),
		
		/**
		 * Allow two way data transfer? Do not enable for
		 * flow configurations.
		 */
		CONNECTION_ALLOW_TWO_WAY_DATA_TRANSFER(0),
		
		/**
		 * Open a reverse connection everytime a new
		 * connection is opened.
		 * 
		 * CONNECTION_ALLOW_TWO_WAY_DATA_TRANSFER has
		 * to be enabled in order for this to take effect.
		 */
		CONNECTION_ENFORCE_TWO_WAY_DATA_TRANSFER(1),
		
		/**
		 * Select a incentive strategy for handling requests:
		 * 
		 *  0 = do not deploy incentive mechanisms,
		 *  1 = deploy TIT-for-TAT style with Homeswarm
		 *  2 = deploy TIT-for-TAT style -> only served if known 
		 */
		REQUEST_INCENTIVE_STRATEGY(0),
		
		/**
		 * Which order to serve pieces within a request:
		 * 
		 * <ul>
		 * <li>0: default, ascending by ID.
		 * <li>1: random order. 
		 * </ul>
		 */
		REQUESTS_SERVING_STRATEGY(0),
		
		/**
		 * Which order to select pieces which are requested:
		 * 
		 * <ul>
		 * <li>0: default, linear, ascending by ID.
		 * <li>1: pick random pieces.
		 * <li>2: pick reverse zigzag style.
		 * </ul>
		 */
		REQUESTS_PIECEPICKING_STRATEGY(0),
		
		/**
		 * The chosen request-strategy.
		 * 
		 * <ul>
		 * <li>0: disable requests on connections that are not also used for
		 * flows
		 * <li>1: default, enable requests on all connections, prefer low load
		 * <li>2: enable requests on all connections, with incentive enabled
		 * </ul>
		 */
		REQUEST_STRATEGY(1),
			
		/**
		 * The chosen serving-strategy.
		 * 
		 * <ul>
		 * <li>0: default, load based.
		 * <li>1: Tit-for-Tat style, make sure to set request strategy 
		 * to TfT as well
		 * <li>9: malicious, do not serve anyone.
		 * </ul>
		 */
		SERVING_STRATEGY(0),
		
		/**
		 * The chosen scheduling-strategy.
		 * 
		 * <ul>
		 * <li>0: default, playback. Schedule all blocks needed for playback.
		 * <li>Bigger 0: NO Playback, SOMETHING ELSE: 
		 * 			Schedule only parts to be requested. tryPlay() will return false. 
		 * <li>1: Random
		 * </ul>
		 */
		SCHEDULING_MODE(0),
		
		
		/**
		 * Strategy to use when forcefully shuffling neighborhood.
		 * 	0: default, none. Disabled.
		 *  SHUFFLE_STRATEGY_BY_LOAD = 1:			load based,
		 *  SHUFFLE_STRATEGY_BY_RECENT_BYTES = 2:	recent transfered bytes,
		 *  SHUFFLE_STRATEGY_BY_INCENTIVE = 3:		ask incentive mgr
		 *  
		 *  Connections will get closed down to CONNECTION_MIN_INCOMING.
		 */
		NEIGHBORHOOD_SHUFFLING_STRATEGY(0),
		
		/**
		 * Incentive strategy: Percentage of overall bandwidth to add to
		 * 	unchoked host when calculating serving probability.
		 */
		INCENTIVE_UNCHOKING_GENEROSITY(30),
		
		SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_PERCENT_LOAD(90),
		
		/**
		 * Only serve 3 times video bandwidth max
		 */
		SCHEDULER_NEVER_SERVE_REQUEST_ABOVE_VIDEO_TIMES(300),
		
		SCHEDULER_ALWAYS_SERVE_REQUEST_BELOW_PERCENT_LOAD(40),
		
		/**
		 * Enable dynamic scaling or urgentsize window.
		 * 
		 *  0 = disable.
		 *  1 = enable, default. 
		 */
		SCHEDULER_DYNAMIC_URGENTSIZE_SCALING_VIA_HOMESWARM(0),

		/**
		 * Enable support for plugins and load.
		 * 
		 *  0 = disable.
		 *  1 = enable, default. 
		 */
		TRANSIT_LOAD_PLUGINS(0),
		
		/**
		 * Indicate weather host should behave maliciously or not
		 * 
		 *  0 = disable, default.
		 *  1 = enable
		 */
		NODE_IS_MALICIOUS(0),
		
		/** Node shall act as home swarm router and offer support to
		 *  home swarm phones? 
		 * 
		 *  0 = disable, default.
		 *  1 = enable
		 */
		NODE_IS_HOMESWARM_ROUTER(0),
		
		/** The node is home swarm phone and seeking help from a HS router.
		 * 
		 *  0 = disable, default.
		 *  1 = enable
		 */
		NODE_IS_HOMESWARM_PHONE(0), 
		
		/** Enable pull token support. 
		 * 
		 *  0 = disable, default.
		 *  1 = enable
		 */
		INCENTIVE_ENABLE_PULLTOKENS(0),
		
		/** Actively request pull tokens. 
		 * 
		 *  0 = disable, default.
		 *  1 = enable
		 */
		INCENTIVE_REQUEST_PULLTOKENS(0),

		/** If a node is not responding to us, only serve base layer. 
		 * 
		 *  0 = disable, default.
		 *  1 = enable
		 */
		INCENTIVE_ONLY_SERVER_BASE_LAYER_TO_UNCOOPERATING_NODES(0);
		
		private final int defaultValue;

		private TransitParams(int defaultValue) {
			this.defaultValue = defaultValue;
		}

		protected int getDefault() {
			return defaultValue;
		}

	}

	/**
	 * All times used in Transit
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 23.09.2012
	 */
	public enum TransitTimes {

		/**
		 * Timeout for requests
		 */
		REQUEST_TIMEOUT(1 * Time.SECOND),

		/**
		 * Max RTT for a request
		 */
		// REQUEST_RTT_MAX(600 * Time.MILLISECOND),
		REQUEST_RTT_MAX(1000 * Time.MILLISECOND),

		/**
		 * Buffermaps will be piggybacked every n seconds
		 */
		BUFFERMAP_EXCHANGE_INTERVAL(1500 * Time.MILLISECOND),

		/**
		 * Maximum RTT for a Flow
		 */
		FLOW_RTT_MAX(650 * Time.MILLISECOND),

		/**
		 * After the RTT grows beyond this value, a connection will be closed.
		 */
		// CONNECTION_RTT_MAX(750 * Time.MILLISECOND),
		CONNECTION_RTT_MAX(1100 * Time.MILLISECOND),

		/**
		 * A connection is kept open for this time, even if not a single video
		 * block has been transmitted. After this time, it may be closed again
		 */
		CONNECTION_INACTIVITY_TIMEOUT(30 * Time.SECOND),

		/**
		 * Time between two executions of the {@link ConnectionControlOperation}
		 * for a connection.
		 */
		// CONNECTION_CONTROL_OPERATION_INTERVAL(20 * Time.SECOND),
		CONNECTION_CONTROL_OPERATION_INTERVAL(5 * Time.SECOND),

		/**
		 * Interval in which neighborhoods are piggybacked
		 */
		NEIGHBORHOOD_EXCHANGE_INTERVAL(2 * Time.MINUTE),

		/**
		 * Interval in which neighborhoods get forcefully shuttled
		 * 	if 0, shuffling is set to off.
		 */
		NEIGHBORHOOD_SHUFFLING_INTERVAL(0),
		
		/**
		 * Minimal delay until playback is started
		 */
		INITIAL_BUFFER_DELAY(0 * Time.SECOND),
		
		/**
		 * Incentive mechanism: Size of sliding window:
		 * 	Bytes to be considered for Tit-For-Tat in X last
		 *  seconds.
		 */
		INCENTIVE_ROUND_TIME_SECONDS(60 * Time.SECOND),
		
		/**
		 * Incentive mechanism: Time of optimistic unchoking period.
		 */
		INCENTIVE_UNCHOKING_PERIOD(60 * Time.SECOND),
		
		/**
		 * Incentive mechanism: Time of window which should be considered for
		 * bandwidth statistics.
		 */
		INCENTIVE_SLIDING_WINDOW_SIZE(60 * Time.SECOND),
		
		/** The pulltoken transmit interval. */
		PULLTOKEN_TRANSMIT_INTERVAL(1 * Time.SECOND),
		
		/** The pulltoken valid time. */
		PULLTOKEN_VALID_TIME(10 * Time.SECOND);


		private final long defaultValue;

		private TransitTimes(long defaultValue) {
			this.defaultValue = defaultValue;
		}

		protected long getDefault() {
			return defaultValue;
		}
	}

	@Override
	public TransitSettings clone() {
		return new TransitSettings(this);
	}

}
