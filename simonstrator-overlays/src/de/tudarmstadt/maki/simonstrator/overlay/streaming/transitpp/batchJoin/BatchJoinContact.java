/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.batchJoin;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;

/**
 * Just includes a TransitContact and its available slots.
 * @author Eduardo Lidanski
 * @version 1.0, 03.03.2014
 */
public class BatchJoinContact {
	
	private TransitContact contact;
	private int availableCapacity;
	
	public BatchJoinContact(TransitContact contact, int availableCapacity) {
		this.contact = contact;
		this.availableCapacity= availableCapacity;
	}
	
	public void decreaseCapacity() {
		availableCapacity--;
	}
	
	public int getAvailableCapacity() {
		return availableCapacity;
	}
	
	public TransitContact getContact() {
		return contact;
	}
	
	@Override
	public String toString() {
		return contact.getNodeID() + " cap" + availableCapacity;
	}

	public void resetCapacity() {
		availableCapacity = 0;
	}

	public void setCapacity(int capacity) {
		availableCapacity = capacity;
	}

}
