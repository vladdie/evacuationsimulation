/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;

/**
 * Message that is sent to the tracker to keep him up-to-date with our contact
 * information.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 26.06.2012
 */
public class RequestNeighborhoodMessage extends AbstractTransitMessage {

	private final byte numberOfRequestedContacts;

	private final List<UniqueID> knownContacts;

	/**
	 * Updates the tracker with our contact information, but does not explicitly
	 * request any new contacts. The tracker might nevertheless decide to return
	 * a set of contacts, maybe based on the layer of the node, if a switch is
	 * detected.
	 * 
	 * @param sender
	 */
	public RequestNeighborhoodMessage(TransitContact sender) {
		this(sender, 0, new LinkedList<UniqueID>());
	}

	/**
	 * Update the tracker with our contact info and request some new contacts
	 * 
	 * @param sender
	 * @param numberOfRequestedContacts
	 *            amount of contacts we would like to retrieve by the tracker.
	 * @param knownContacts
	 *            Contacts we already know (only their IDs)
	 */
	public RequestNeighborhoodMessage(TransitContact sender,
			int numberOfRequestedContacts, List<UniqueID> knownContacts) {
		super(sender);
		this.numberOfRequestedContacts = (byte) numberOfRequestedContacts;
		this.knownContacts = knownContacts;
	}

	/**
	 * The sender might specify if and how many contacts it needs from the
	 * tracker.
	 * 
	 * @return
	 */
	public int getNumberOfRequestedContacts() {
		return numberOfRequestedContacts;
	}

	/**
	 * The sender might specify a list of already known contacts. To save
	 * message space, we only send the ID.
	 * 
	 * @return
	 */
	public List<UniqueID> getKnownContacts() {
		return knownContacts;
	}

	@Override
	public long getSize() {
		int knownContactsSize = 0;
		for (UniqueID id : knownContacts) {
			knownContactsSize += id.getTransmissionSize();
		}
		// 1 byte for numOfContacts
		return super.getSize() + 1 + knownContactsSize;
	}

	@Override
	public String toString() {
		return "REQUESTNEIGHBORS " + numberOfRequestedContacts + " KNOWN: "
				+ knownContacts.toString() ;
	}

}
