/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.BufferInterface;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.AvailabilityEstimator;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitFlowManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitRequestManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitBlockMessage;

/**
 * This is the interface used by transit to talk to its scheduler and its
 * components, namely the {@link TransitRequestManager} and the
 * {@link TransitFlowManager}.
 * 
 * In transit, layer changes (PQA) are initiated by the Scheduler.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 08.06.2012
 */
public interface TransitScheduler extends AvailabilityEstimator, IPeerStatusListener, BufferInterface {

	/**
	 * The FlowManager.
	 * 
	 * @return the flow manager
	 */
	public TransitFlowManager getFlowManager();

	/**
	 * The RequestManager.
	 * 
	 * @return the request manager
	 */
	public TransitRequestManager getRequestManager();

	/**
	 * Notifies the scheduler of a switch from one SVC-layer to another (at the
	 * currently playing chunk).
	 */
	public void switchedLayer();

	/**
	 * Next chunk to play or stalling chunk.
	 * 
	 * @return the buffer begin
	 */
	@Override
	public int getBufferBegin();

	/**
	 * Current state of the playback.
	 * 
	 * @return the playback state
	 */
	public PlaybackState getPlaybackState();

	/**
	 * Time between a node joining and the first successfully played chunk.
	 * 
	 * @return the startup delay
	 */
	public long getStartupDelay();

	/**
	 * Called, as soon as a block is received. This method should also call
	 * connection.receivedBlock to trigger the listeners.
	 * 
	 * @param blockMessage
	 *            the block message
	 * @param replyTo
	 *            reply-handler (optional)
	 */
	public void receivedBlock(TransitBlockMessage blockMessage, TransitReplyHandler replyTo);

	/**
	 * This is called once for every playback-Operation that is executed. If the
	 * chunkNumber increased from the last call, we have a successful step.
	 * Otherwise, the player stalled.
	 * 
	 * @param chunkNumber
	 *            the chunk number
	 */
	public void tick(int chunkNumber);

	/**
	 * Sets the urgent size, as blocks to be requested.
	 * 
	 * @param urgentSize
	 *            the new urgent size
	 */
	public void setUrgentSize(int urgentSize);

	/**
	 * Gets the urgent size, as blocks to be requested.
	 * 
	 * @return the urgent size
	 */
	public int getUrgentSize();

}
