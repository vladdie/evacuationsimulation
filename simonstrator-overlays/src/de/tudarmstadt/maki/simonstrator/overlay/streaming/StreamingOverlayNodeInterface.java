/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming;

import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.overlay.api.OverlayNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.AvailabilityEstimator;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.ThroughputEstimator;

/**
 * Trying to derive a common interface from the work Osama has done. This will
 * enable easier comparisons with Transit as well as further development of new
 * overlays. Furthermore, all models created by Osama, such as the VideoCube,
 * should be usable across all streaming overlays
 * 
 * This interface is to be implemented by a Node that participates in a
 * streaming overlay.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public interface StreamingOverlayNodeInterface extends OverlayNode {

	/*
	 * TODO define other useful methods?
	 */

	/**
	 * ThroughputEstimator used by the application to decide on layer switching.
	 * 
	 * @return
	 */
	public ThroughputEstimator getThroughputEstimator();

	/**
	 * Via this interface the overlay exposes information about available
	 * packets/chunks to the application. This information is then used to
	 * switch between SVC layers, for example.
	 * 
	 * @return
	 */
	public AvailabilityEstimator getAvailabilityEstimator();

	/**
	 * Allows applications to access the current state of the buffer in order to
	 * fetch required packets for playback. This is also used to notify the
	 * overlay of stalls and operations such as play/pause.
	 * 
	 * @return
	 */
	public BufferInterface getBuffer();

	/**
	 * Initial view on the video, for seeders, this is the full file. Leechers
	 * get an empty document. In a live-streaming case, the server will get an
	 * empty document as well, which will then be filled by the application as
	 * soon as new blocks become available. Note, that the total file size
	 * (number of chunks) has to be known in advance for this model to work.
	 */
	public void setVideo(StreamingDocument video);

	/**
	 * The current view on the streaming document on this node.
	 * 
	 * @return
	 */
	public StreamingDocument getVideo();

	/**
	 * The current SVC-Layer of the peer
	 * 
	 * @return
	 */
	public byte[] getLayer();

	/**
	 * Update the current SVC-Layer of the peer. This will trigger updates of
	 * the scheduler as well as the buffer, whenever needed.
	 * 
	 * @param newLayer
	 *            a layer different from the current layer.
	 */
	public void setLayer(byte[] newLayer);

	/**
	 * The maximum layer this node is requiring (determined by IQA based on
	 * screen size, processing power...)
	 * 
	 * @return
	 */
	public byte[] getMaxLayer();

	/**
	 * Set the maximum layer this node wants to play based on IQA
	 * 
	 * @param maxLayer
	 */
	public void setMaxLayer(byte[] maxLayer);

	/**
	 * Returns true, if the node has been configured as a server. In a live
	 * streaming scenario this will force a special buffer/scheduler
	 * implementation to announce packets as soon as their playback position is
	 * reached. In a VoD setting this is just a node that has the full file at
	 * the very beginning.
	 * 
	 * @return
	 */
	public boolean isServer();

	/**
	 * In live streaming, this is always false.
	 * 
	 * @return
	 */
	public boolean isSeeder();
	
	/**
	 * @param appCallback
	 * @return the operation id of the join operation
	 */
	public int join(final OperationCallback<Object> appCallback);
	
	/**
	 * @param appCallback
	 * @return the operation id of the leave operation
	 */
	public int leave(final OperationCallback<Object> appCallback);

}
