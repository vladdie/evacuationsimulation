/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;

/**
 * The base message for all TransitMessages. This will deal with sender-contact
 * cloning!
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public abstract class AbstractTransitMessage implements TransitMessage {

	private boolean isPiggybacked = false;

	private boolean hasPiggybackedInformation;

	private List<TransitMessage> piggybackedMessages;

	private final TransitContact sender;

	/**
	 * A marker, if this message is to be replied to via sendReply()
	 */
	private boolean wantsReply = false;

	/**
	 * 
	 * @param sender
	 *            this will be cloned
	 */
	public AbstractTransitMessage(TransitContact sender) {
		this.sender = sender.clone();
	}

	@Override
	public void setWantsReply() {
		wantsReply = true;
	}

	@Override
	public boolean wantsReply() {
		return wantsReply;
	}

	@Override
	public TransitContact getSenderContact() {
		return sender;
	}

	@Override
	public long getSize() {
		if (isPiggybacked) {
			return 0;
		}
		long piggybackedSize = 0;
		if (hasPiggybackedInformation) {
			for (TransitMessage piggybacked : piggybackedMessages) {
				piggybackedSize += piggybacked.getSize();
			}
		}
		return piggybackedSize + sender.getTransmissionSize();
	}

	@Override
	public Message getPayload() {
		/*
		 * We just assume that there is no additional payload for most
		 * TransitMessages.
		 */
		return null;
	}

	@Override
	public final List<TransitMessage> getPiggybackedMessages() {
		return piggybackedMessages;
	}

	@Override
	public final boolean hasPiggybackedMessages() {
		return hasPiggybackedInformation;
	}

	@Override
	public final void piggyback(TransitMessage message) {
		assert !this.isPiggybacked;
		((AbstractTransitMessage) message).isPiggybacked = true;
		if (!hasPiggybackedInformation) {
			hasPiggybackedInformation = true;
			piggybackedMessages = new LinkedList<TransitMessage>();
		}
		piggybackedMessages.add(message);
	}

}
