/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.operations;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageCallback;
import de.tudarmstadt.maki.simonstrator.api.operation.AbstractOperation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitFactory;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.JoinTrackerMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.JoinTrackerReplyMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;

/**
 * This operation finds a tracker and registers the node with the tracker. It
 * masks the bootstrapping process which allows for arbitrary complex ways to
 * find the tracker, if that becomes interesting in the future. For now, we just
 * rely on global knowledge.
 * 
 * At the end, a {@link TransitContact} is returned that refers to the tracker
 * (or one of the tracker nodes, if a distributed approach is used). The tracker
 * includes an initial neighborhood that consists of powerful nodes (probably
 * the source as well), to aid in the startup phase.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public class JoinTrackerOperation extends AbstractOperation<TransitNode, TransitContact> implements
        TransMessageCallback {

	private TransitContact trackerContact;

	private List<TransitContact> initialContacts;

	private boolean doNotSetVideo = false;

	/**
	 * Instantiates a new join tracker operation.
	 * 
	 * @param component
	 *            the component
	 * @param callback
	 *            the callback
	 */
	public JoinTrackerOperation(TransitNode component, OperationCallback<TransitContact> callback) {
		super(component, callback);
	}

	/**
	 * Instantiates a new join tracker operation.
	 * 
	 * @param component
	 *            the component
	 * @param doNotSetVideo
	 *            the do not set video
	 * @param callback
	 *            the callback
	 */
	public JoinTrackerOperation(TransitNode component, boolean doNotSetVideo, OperationCallback<TransitContact> callback) {
		super(component, callback);
		this.doNotSetVideo = doNotSetVideo;
	}

	@Override
	protected void execute() {

		TransitContact trackerContact = TransitFactory.getTrackerContact(getComponent());

		JoinTrackerMessage joinMsg = new JoinTrackerMessage(getComponent().getLocalOverlayContact());
		getComponent().getMessageHandler().sendAndWait(joinMsg, trackerContact, this);
	}

	@Override
	public void messageTimeoutOccured(int commId) {
		/*
		 * TODO: what about retries? We should not trigger this inside the Op
		 * but instead in the callback of the Operation
		 */
		System.err.println("JoinTracker failed due to a message timeout!");
		operationFinished(false);
	}

	@Override
	public void receive(Message msg, TransInfo senderInfo, int commId) {
		if (isFinished()) {
			return;
		}

		JoinTrackerReplyMessage reply = (JoinTrackerReplyMessage) msg;
		trackerContact = reply.getTrackerContact();
		// this will contain the current playback position
		if (!getComponent().isServer() && doNotSetVideo == false) {
			getComponent().setVideo(reply.getStreamingDocument());
		}

		/*
		 * The answer also contains a set of nodes that we will use as a
		 * starting point for the signaling mesh
		 */
		initialContacts = reply.getNeighbors();

		operationFinished(true);
	}

	@Override
	public TransitContact getResult() {
		return trackerContact;
	}

	/**
	 * List of contacts returned by the tracker as a starting point for the
	 * signaling mesh
	 * 
	 * @return
	 */
	public List<TransitContact> getInitialContacts() {
		return initialContacts;
	}

}
