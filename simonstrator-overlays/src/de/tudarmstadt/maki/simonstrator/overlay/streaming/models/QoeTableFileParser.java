package de.tudarmstadt.maki.simonstrator.overlay.streaming.models;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.models.QoeTable.LayerEntry;

public class QoeTableFileParser {

	public static ArrayList<QoeTable.LayerEntry> getEntries(String qoeTableFile) {

		ArrayList<QoeTable.LayerEntry> entries = new ArrayList<QoeTable.LayerEntry>();

		try {
			InputStream in = new FileInputStream(qoeTableFile);

			// Use UTF-8 as encoding
			BufferedReader br = new BufferedReader(new InputStreamReader(in,
					"UTF-8"));

			String fileContent = "";
			String line = "";
			while ((line = br.readLine()) != null) {
				if (line.startsWith("#")) {
					continue;
				}
				fileContent += line + "\n";
			}

			/*
			 * Match entry
			 * 
			 * Example: 1\t2\t3\t150.9\t0.8\n
			 */
			Pattern p = Pattern
					.compile("(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+(\\.\\d*){0,1}|\\?)\\s+(\\d+(\\.\\d*){0,1})\\s*\n");
			Matcher m = p.matcher(fileContent);

			while (m.find()) {
				byte d = Byte.parseByte(m.group(1));
				byte t = Byte.parseByte(m.group(2));
				byte q = Byte.parseByte(m.group(3));
				float rate = (m.group(4).compareTo("?") == 0) ? 0 : Float
						.parseFloat(m.group(4));
				float quality = Float.parseFloat(m.group(6));

				entries.add(new LayerEntry(d, t, q, rate, quality));
			}

		} catch (Exception e) {
			System.err.println("Could not read QoE table file (" + qoeTableFile
					+ ")!");
		}

		return entries;
	}

	// TODO [JR]: Remove after debugging
	// public static void main(String[] args) {
	// getEnrties("config/Streaming/QoE_Aware_Adaptation/QoE_table_Crowd-Run.dat");
	// }

}
