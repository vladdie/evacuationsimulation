/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;

/**
 * The reply to a {@link ConnectToNeighborMessage}.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 09.06.2012
 */
public class ConnectionMessage extends AbstractTransitMessage {

	/**
	 * Meaning of a {@link ConnectionMessage}
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 10.06.2012
	 */
	public static enum ConnectionMessageType {
		/**
		 * Request a connection to the target (REQUESTS will always be issued by
		 * the sink of the connection in terms of scheduling payload)
		 */
		OPEN,
		/**
		 * Request (OPEN) has been accepted
		 */
		ACCEPT,
		/**
		 * Request (OPEN) has been denied
		 */
		DENY,
		/**
		 * Request to CLOSE the connection
		 */
		CLOSE,
		/**
		 * ACK for CLOSE
		 */
		CLOSE_CONFIRM,
		/**
		 * Ping and Pong for RTT
		 */
		PING, PONG;
	}

	private ConnectionMessageType messageType;


	/**
	 * A message used as part of the connection maintenance protocol.
	 * 
	 * @param connection
	 * @param messageType
	 */
	public ConnectionMessage(TransitConnection connection,
			ConnectionMessageType messageType) {
		super(connection.getOwner());
		this.messageType = messageType;
	}

	/**
	 * Meaning of this message
	 * 
	 * @return
	 */
	public ConnectionMessageType getMessageType() {
		return messageType;
	}

	@Override
	public long getSize() {
		return super.getSize() + 1; // 1 for enum
	}

	@Override
	public String toString() {
		return messageType.toString();
	}

}
