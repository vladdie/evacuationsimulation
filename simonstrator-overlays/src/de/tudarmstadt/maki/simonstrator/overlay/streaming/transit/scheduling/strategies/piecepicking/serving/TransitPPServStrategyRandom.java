/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.serving;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.Randoms;

public class TransitPPServStrategyRandom implements TransitPiecePickingServingStrategy {

	@Override
	public int getNextBit(BitSet requestMap) {
		final int bitSum = requestMap.cardinality();
		final int randomChunk = Randoms.getRandom(TransitPPServStrategyRandom.class).nextInt(bitSum);

		int hit = 0;
		for (int i = requestMap.nextSetBit(0); i >= 0; i = requestMap.nextSetBit(i + 1)) {
			if (hit == randomChunk)
				return i;
			hit++;
		}

		assert false : "Hit should have occured.";
		return requestMap.nextSetBit(0);
	}

}
