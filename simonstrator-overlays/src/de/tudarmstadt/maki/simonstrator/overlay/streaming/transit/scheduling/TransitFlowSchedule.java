/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.VideoModel;

/**
 * This is a schedule that can be sent to other nodes. It specifies which blocks
 * a node wants to receive in the Live Phase. In this phase, incoming blocks are
 * directly passed to interested children which ensures timely delivery of the
 * most recent video content. In a stable environment this will lead to
 * distribution trees for the most recent blocks. Blocks that could not be
 * retrieved in the live phase (maybe because of a parent switch or simply
 * because of a packet drop) will result in a request of the specific packet.
 * 
 * Furthermore, a schedule should be completely state-less.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 08.06.2012
 */
public class TransitFlowSchedule implements Transmitable {

	/**
	 * As in the {@link VideoModel} each chunk has the same order of blocks when
	 * the layers are considered, we just need to pass a mask referring to one
	 * chunk. The node will then use the validFromChunk to determine the correct
	 * blocks to send.
	 */
	private final BitSet blockMask;

	/**
	 * This Schedule is a LIVE-Schedule, i.e. it requests the most recent blocks
	 * that are available.
	 * 
	 * @param blockMask
	 * @param requiredBandwidth
	 *            in byte/s
	 */
	public TransitFlowSchedule(BitSet blockMask) {
		this.blockMask = (BitSet) blockMask.clone();
	}

	@Override
	public int getTransmissionSize() {
		return blockMask.length();
	}

	/**
	 * The required/requested blocks inside a chunk
	 * 
	 * @return
	 */
	public BitSet getBlockMask() {
		return blockMask;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((blockMask == null) ? 0 : blockMask.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TransitFlowSchedule other = (TransitFlowSchedule) obj;
		if (blockMask == null) {
			if (other.blockMask != null) {
				return false;
			}
		} else if (!blockMask.equals(other.blockMask)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return blockMask.toString();
	}

}
