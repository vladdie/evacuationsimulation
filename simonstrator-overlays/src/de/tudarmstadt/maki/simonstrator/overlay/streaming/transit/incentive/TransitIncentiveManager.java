/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnectionListener;

public interface TransitIncentiveManager extends TransitConnectionListener {

	/**
	 * Sort connections by incentive.
	 *
	 * @param connections the connections
	 * @return the list
	 */
	List<TransitConnection> sortConnectionsByIncentiveForRequesting(List<TransitConnection> connections);
	
	/**
	 * Gets the serving probability.
	 *
	 * @param connection the connection
	 * @return the serving probability
	 */
	TransitServingAdvise getServingAdvise(TransitConnection connection);

	/**
	 * Gets the incentive request advisor.
	 *
	 * @param connections the connections
	 * @return the incentive request advisor
	 */
	TransitIncentiveRequestAdvisor getIncentiveRequestAdvisor(List<TransitConnection> connections);

}
