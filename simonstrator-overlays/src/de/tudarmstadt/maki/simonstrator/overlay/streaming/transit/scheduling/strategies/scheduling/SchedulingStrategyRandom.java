/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.scheduling;

import java.util.BitSet;
import java.util.Random;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;

public class SchedulingStrategyRandom implements SchedulingStrategy {

	/** The node. */
	private final TransitNode node;

	/**
	 * Instantiates a new transit piece picking strategy linear.
	 * 
	 * @param node
	 *            the node
	 */
	public SchedulingStrategyRandom(TransitNode node) {
		this.node = node;
	}

	@Override
	public BitSet selectNextBlocks(int bufferBegin, int offsetGlobal, int blocksToSchedule) {

		assert bufferBegin >= 0;

		final int chunkSize = node.BLOCKS_PER_CHUNK;
		int newestBlockNum = node.getScheduler().getRequestManager().getNewestSeenBlockNumber();
		if (newestBlockNum < offsetGlobal) {
			newestBlockNum = blocksToSchedule * chunkSize + offsetGlobal;
		}

		/* SERVING MODE 4: RANDOM SCHEDULING! */
		final BitSet missing = new BitSet();
		final int upperLimit = newestBlockNum;
		final int lowerLimit = offsetGlobal;
		final int window = upperLimit - lowerLimit;

		if (window <= 0) {
			/* Empty set. Up to date. We got everything already. */
			return missing;
		}

		final Random rand = Randoms.getRandom(SchedulingStrategyRandom.class);
		int chunksLeft = blocksToSchedule * chunkSize / 2;
		int triesLeft = chunksLeft * 10;
		while (chunksLeft > 0 && triesLeft > 0) {
			int nextBlock = rand.nextInt(window) + lowerLimit;
			if (!node.getVideo().haveBlock(nextBlock)) {
				missing.set(nextBlock - offsetGlobal);
				chunksLeft--;
			}
			triesLeft--;
		}

		/* Now schedule full block with all chunks. */
		return missing;

	}

}
