/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp;

import java.util.Arrays;
import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.common.Transmitable;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;

/**
 * Information periodically exchanged between peers for topology optimizations and offer-based flow requests.
 * @author Eduardo Lidanski
 * @version 1.0, 14.04.2014
 */
public class TransitNodeInfo implements Transmitable {

	/**
	 * The number of children, one value for each tree.
	 */
	private final int[] numberOfChildren;

	/**
	 * The depth of the respective trees.
	 */
	private final int[] depth;

	/**
	 * Does the node have capacity to send another flow in the respective tree?
	 */
	private final BitSet capacities;
	
	/**
	 * The parents of the node in the respective tree.
	 */
	private final TransitContact[] parents;

	public TransitNodeInfo(int[] numberOfChildren, int[] depth,
			BitSet capacities, TransitContact[] parents) {
		this.numberOfChildren = numberOfChildren;
		this.depth = depth;
		this.capacities = capacities;
		this.parents = parents;
	}

	@Override
	public int getTransmissionSize() {
		return numberOfChildren.length + depth.length + capacities.length() + parents.length;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("C: " + Arrays.toString(numberOfChildren) + " ");
		sb.append("D: " + Arrays.toString(depth) + " ");
		sb.append("Cap: " + capacities);
		return sb.toString();

	}
	
	public BitSet getCapacities() {
		return capacities;
	}
	
	public int[] getDepth() {
		return depth;
	}
	
	public TransitContact[] getParents() {
		return parents;
	}

	public int[] getChildren() {
		return numberOfChildren;
	}

}
