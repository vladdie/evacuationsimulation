/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.capacity;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitFlowSchedule;

/**
 * Provides bandwidth information to the scheduler and the request manager. This
 * information is then used to decide whether to accept or to deny flows and
 * requests.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 17.08.2012
 */
public interface TransitBandwidthManager {

	/**
	 * Returns true if the node can handle the flow
	 * 
	 * @param flow
	 * @param connection
	 * @return
	 */
	public boolean hasSpareBandwidthFor(TransitFlowSchedule flow,
			ConnectionDirection direction);

	/**
	 * Returns true if the node can handle the given block
	 * 
	 * @param blocknum
	 * @param direction
	 * @return
	 */
	public boolean hasSpareBandwidthFor(int blocknum,
			ConnectionDirection direction);

	/**
	 * Reserves BW for the given block -> this will trigger correct results of
	 * the hasBW-Methods if they are called multiple times in one tick.
	 * 
	 * @param blocknum
	 * @param direction
	 */
	public void reserveBandwidthFor(int blocknum, ConnectionDirection direction);

	/**
	 * Load-indicator of this node. 1 is fully loaded, 0 is not loaded at all.
	 * 
	 * @param direction
	 * @return
	 */
	public double getLoad(ConnectionDirection direction);

	/**
	 * Get the number of times the node can forward the given flow.
	 * 
	 * @param flow
	 * @param direction
	 * @return
	 */
	public int getNumberOfAvailableSlots(TransitFlowSchedule flow,
			ConnectionDirection direction);

}
