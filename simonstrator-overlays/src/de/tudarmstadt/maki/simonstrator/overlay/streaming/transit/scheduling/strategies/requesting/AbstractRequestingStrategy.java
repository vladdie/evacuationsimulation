/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.requesting;

import java.util.BitSet;
import java.util.LinkedHashMap;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.SendRequestMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitRequestMessage.RequestMsgType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.RequestState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestManagerImpl;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestManagerImpl.RequestInfo;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.requesting.TransitPPRequestStrategyLinear;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.requesting.TransitPPRequestStrategyRandom;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.requesting.TransitPPRequestStrategyReverseZigZag;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.requesting.TransitPiecePickingRequestStrategy;

/**
 * The Class AbstractRequestingStrategy.
 */
public abstract class AbstractRequestingStrategy implements RequestingStrategy {

	protected final static boolean DEBUG = false;

	private final TransitNode node;

	public final Map<Integer, RequestInfo> openRequests;

	private final TransitRequestManagerImpl transitRequestManager;

	private final TransitPiecePickingRequestStrategy piecePickingRequestStrategy;

	/**
	 * Instantiates a new abstract requesting strategy.
	 * 
	 * @param transitRequestManager
	 *            the transit request manager
	 */
	public AbstractRequestingStrategy(TransitRequestManagerImpl transitRequestManager) {
		this.transitRequestManager = transitRequestManager;
		this.node = transitRequestManager.getNode();
		this.openRequests = new LinkedHashMap<Integer, TransitRequestManagerImpl.RequestInfo>();

		final int piecePickingStrategy = transitRequestManager.getNode().getSettings()
		        .getParam(TransitParams.REQUESTS_PIECEPICKING_STRATEGY);
		if (piecePickingStrategy == 0) {
			this.piecePickingRequestStrategy = new TransitPPRequestStrategyLinear(this.node);
		} else if (piecePickingStrategy == 1) {
			this.piecePickingRequestStrategy = new TransitPPRequestStrategyRandom(this.node);
		} else if (piecePickingStrategy == 2) {
			this.piecePickingRequestStrategy = new TransitPPRequestStrategyReverseZigZag(this.node);
		} else {
			throw new AssertionError("Unknown REQUESTS_PIECEPICKING_STRATEGY selected.");
		}
	}

	/**
	 * Gets the node.
	 * 
	 * @return the node
	 */
	protected TransitNode getNode() {
		return node;
	}

	/**
	 * Do request and mask request.
	 * 
	 * @param connection
	 *            the connection
	 * @param blockOffset
	 *            the block offset
	 * @param mask
	 *            the mask
	 */
	protected void doRequestAndMaskRequest(TransitConnection connection, int blockOffset, BitSet mask) {
		TransitRequestSchedule bufferMap = connection.getBufferMap();
		if (bufferMap != null) {
			BitSet possible = getMatch(connection, bufferMap, blockOffset, mask);
			if (possible == null || possible.isEmpty()) {
				if (DEBUG) {
					System.out
							.println("TransitRequestHandler: Possible is null/empty!");
				}
				return;
			}
			TransitRequestSchedule request = new TransitRequestSchedule(blockOffset, possible);
			if (!getNode().getBandwidthManager().hasSpareBandwidthFor(blockOffset + possible.nextSetBit(0),
			        ConnectionDirection.IN)) {
				if (DEBUG) {
					System.out
							.println("TransitRequestHandler: No spare incoming bandwidth for the request!");
				}
				return;
			}
			getNode().getBandwidthManager().reserveBandwidthFor(blockOffset + possible.nextSetBit(0),
			        ConnectionDirection.IN);

			doRequest(connection, request);
			mask.andNot(possible);
		}
	}

	/**
	 * 
	 * @param connection
	 * @param blockOffset
	 * @param mask
	 */
	private void doRequest(TransitConnection connection, TransitRequestSchedule request) {
		SendRequestMessage requestMsg = new SendRequestMessage(node.getLocalOverlayContact(), RequestMsgType.REQUEST,
		        request);
		node.getMessageHandler().send(requestMsg, connection.getEndpoint());
		addRequest(connection, request);
	}

	/**
	 * Returns a matching request schedule or null.
	 * 
	 * @param bufferMap
	 *            known buffer map
	 * @return request that we will send
	 */
	protected BitSet getMatch(TransitConnection connection, TransitRequestSchedule bufferMap,
	        int requestedBlocksOffset, BitSet requestedBlocks) {

		/*
		 * remove already retrieved blocks and blocks that were just recently
		 * requested
		 */
		BitSet requested = (BitSet) (requestedBlocks.clone());
		for (int i = requestedBlocks.nextSetBit(0); i >= 0; i = requestedBlocks.nextSetBit(i + 1)) {
			assert !node.getVideo().haveBlock(requestedBlocksOffset + i);
			RequestInfo info = getRequestInfo(requestedBlocksOffset + i);
			if (info != null && !info.allowRequest(connection)) {
				requested.set(i, false);
			}
		}

		BitSet match = new BitSet();
		if (requested.isEmpty()) {
			return match;
		}

		if (bufferMap.getBlockOffset() <= requestedBlocksOffset) {
			int diff = requestedBlocksOffset - bufferMap.getBlockOffset();
			if (diff < bufferMap.getBlockMap().length()) {
				match = (BitSet) (bufferMap.getBlockMap().get(diff, bufferMap.getBlockMap().size()).clone());
				match.and(requested);
			}
		} else {
			int diff = bufferMap.getBlockOffset() - requestedBlocksOffset;
			if (diff >= requestedBlocks.length()) {
				// assuming that all past blocks are available ONLY on sources
				if (connection.getEndpoint().getType() == ContactType.SOURCE) {
					match = requested;
				}
				/*
				 * For this to work, buffer maps must be centered around the
				 * playback position of the requesting node, which is the case
				 * in transit.
				 */
				// throw new AssertionError();
				// Source or empty set.
				return match;
			} else {
				for (int i = requested.nextSetBit(0); i >= 0; i = requested.nextSetBit(i + 1)) {
					match.set(i, bufferMap.getBlockMap().get(diff + i));
				}
			}
		}

		int maxBlocks = 0;
		if (connection.getEndpoint().getType() == ContactType.SOURCE) {
			maxBlocks = node.getSettings().getParam(TransitParams.REQUEST_MAX_BLOCKS_PER_CONNECTION_SOURCE);
		} else {
			maxBlocks = node.getSettings().getParam(TransitParams.REQUEST_MAX_BLOCKS_PER_CONNECTION);
		}
		return piecePickingRequestStrategy.selectPiecesForRequest(match, maxBlocks, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling
	 * .strategies .requesting.RequestingStrategy#getRequestInfo(int)
	 */
	@Override
	public RequestInfo getRequestInfo(int blocknum) {
		return openRequests.get(Integer.valueOf(blocknum));
	}

	@Override
	public int getNumberOfActiveRequests() {
		long time = Time.getCurrentTime();
		int active = 0;
		for (RequestInfo info : openRequests.values()) {
			assert !info.isFinished();
			if (info.getLastRequest() + Time.SECOND >= time) {
				active++;
			}
		}
		return active;
	}

	protected void addRequest(TransitConnection connection, TransitRequestSchedule request) {
		BitSet requestMap = request.getBlockMap();
		int offset = request.getBlockOffset();
		for (int i = requestMap.nextSetBit(0); i >= 0; i = requestMap.nextSetBit(i + 1)) {
			assert !node.getVideo().haveBlock(offset + i);
			RequestInfo reqInfo = openRequests.get(Integer.valueOf(offset + i));
			if (reqInfo == null) {
				reqInfo = transitRequestManager.new RequestInfo(offset + i);
				openRequests.put(Integer.valueOf(offset + i), reqInfo);
			} else {
				// if (DEBUG) {
				// boolean wasOnline = Or
				// .getHostForNetID(reqInfo.getLastRequestConnection().getEndpoint().getTransInfo().getNetId())
				// .getNetLayer().isOnline();
				// System.err.println(Simulator.getFormattedTime(Simulator.getCurrentTime())
				// + "RE-REQUESTING from "
				// + connection.getEndpoint().toString() + " " +
				// reqInfo.toString() + " --> "
				// + (wasOnline ? "WAS ONLINE" : "WAS OFFLINE"));
				// }
			}
			assert !reqInfo.isFinished();
			reqInfo.requested(connection, request);
		}

		connection.setRequestState(RequestState.ACTIVE);
		connection.setRequest(request);
	}

	@Override
	public void cancelRequest(int blocknum) {
		RequestInfo info = openRequests.remove(Integer.valueOf(blocknum));
		if (info != null && !info.isFinished()) {
			info.finished();
			if (DEBUG) {
				System.err.println("TRM: " + node.getLocalOverlayContact().toString()
				        + " CANCELING REQUEST due to incoming FLOW block: " + blocknum);
			}
		}
	}

	@Override
	public void requestDenied(TransitConnection connection) {
		TransitRequestSchedule request = connection.getRequest();

		if (request != null) {
			BitSet requestMap = request.getBlockMap();
			int offset = request.getBlockOffset();
			for (int i = requestMap.nextSetBit(0); i >= 0; i = requestMap.nextSetBit(i + 1)) {
				RequestInfo reqInfo = openRequests.get(Integer.valueOf(offset + i));
				if (reqInfo != null) {
					reqInfo.deniedBy(connection.getEndpoint());
				}
			}
		}
	}

	@Override
	public void requestAltered(TransitConnection connection, TransitRequestSchedule newRequestSchedule) {

		/* Might be CLOSING right now. */
		if (!connection.getState().equals(ConnectionState.OPEN)) {
			return;
		}

		assert !connection.getRequest().equals(newRequestSchedule) : "Requests do not differ";
		assert connection.getRequest().getBlockOffset() == newRequestSchedule.getBlockOffset();

		final BitSet deniedBlocks = connection.getRequest().getBlockMap();
		deniedBlocks.andNot(newRequestSchedule.getBlockMap());

		int offset = connection.getRequest().getBlockOffset();
		for (int i = deniedBlocks.nextSetBit(0); i >= 0; i = deniedBlocks.nextSetBit(i + 1)) {
			RequestInfo reqInfo = openRequests.get(Integer.valueOf(offset + i));
			if (reqInfo != null) {
				reqInfo.deniedBy(connection.getEndpoint());
			}
		}
	}

	@Override
	public void receivedRequestedBlock(TransitConnection connection, TransitBlockMessage block) {
		if (connection != null) {
			connection.blockTransferred(block.getBlockNumber(), block.getSize(), false);
		}
		RequestInfo info = openRequests.remove(Integer.valueOf(block.getBlockNumber()));
		if (info == null) {
			if (DEBUG) {
				System.err.println(node.getLocalOverlayContact() + " received not-requested or duplicate block "
				        + block.getBlockNumber());
			}
		} else {
			info.finished();
		}

		/*
		 * Enlarge Timeouts for other blocks that are requested via the same
		 * connection.
		 */
		if (connection != null) {
			TransitRequestSchedule request = connection.getRequest();
			if (request != null) {
				BitSet stillMissing = request.getBlockMap();
				assert !stillMissing.isEmpty();
				for (int i = stillMissing.nextSetBit(0); i >= 0; i = stillMissing.nextSetBit(i + 1)) {
					RequestInfo reqInfo = openRequests.get(Integer.valueOf(request.getBlockOffset() + i));
					if (reqInfo != null) {
						reqInfo.updateLastRequest();
					}
				}
			}
		}
	}
}
