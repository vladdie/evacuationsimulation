package de.tudarmstadt.maki.simonstrator.overlay.streaming.data;

public enum Messages {
	ACK_BLK,
	CONNECTION_MSG,
	JOIN_TRACKER,
	JOIN_TRACKER_REPLY,
	OFFER_NEIGHBORHOOD,
	PULL_TOKEN_TRANSIT,
	REQUEST_NEIGHBORHOOD,
	SEND_BLOCK,
	SEND_REQUEST,
	SEND_SCHEDULE
}
