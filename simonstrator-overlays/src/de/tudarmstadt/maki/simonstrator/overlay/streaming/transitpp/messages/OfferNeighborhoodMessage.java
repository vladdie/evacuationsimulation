/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;

/**
 * Stand-alone implementation of the {@link TransitOfferNeighborhoodMessage}, to
 * actively push a set of neighborhood suggestions to another node.
 * 
 * TODO check, if it is sufficient to let the tracker send neighborhood contacts
 * upon request. It may not be necessary to make this part of the P2P system
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 08.06.2012
 */
public class OfferNeighborhoodMessage extends AbstractTransitMessage implements
		TransitOfferNeighborhoodMessage {

	private List<TransitContact> neighbors;

	/**
	 * Offers the list of {@link TransitContact}s as possible neighbors.
	 * 
	 * @param sender
	 * @param neighbors
	 */
	public OfferNeighborhoodMessage(TransitContact sender,
			List<TransitContact> neighbors) {
		super(sender);
		this.neighbors = neighbors;
	}

	/**
	 * A list of possible neighbors
	 * 
	 * @return
	 */
	public List<TransitContact> getNeighbors() {
		return neighbors;
	}

	@Override
	public long getSize() {
		return super.getSize() + neighbors.size()
				* getSenderContact().getTransmissionSize();
	}

}
