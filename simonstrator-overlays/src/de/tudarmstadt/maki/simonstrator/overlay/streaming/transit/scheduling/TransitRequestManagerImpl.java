/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling;

import java.util.BitSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.BufferInterface.PlaybackState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler.PeriodicPiggybackListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitRequestManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.AckBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.SendRequestMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitRequestMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitRequestMessage.RequestMsgType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.RequestState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnectionListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.requesting.RequestingStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.requesting.TransitRequestStrategyLoadBased;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.requesting.TransitRequestStrategyPullToken;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.requesting.TransitRequestStrategyWithIncentive;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.serving.ServingStrategy;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.serving.TransitServingStrategyFirstComeFirstServe;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.serving.TransitServingStrategyMalicous;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.serving.TransitServingStrategyWithIncentive;

/**
 * Base class for a {@link TransitRequestManager}.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public class TransitRequestManagerImpl implements TransitRequestManager, TransitConnectionListener {

	protected final static boolean DEBUG = false;

	private final TransitNode node;

	/**
	 * Requests issued by this node where we wait for packets
	 */
	private final RequestingStrategy outgoingRequestsHandler;

	/**
	 * Requests issued by other nodes where we have to send packets
	 */
	private final ServingStrategy incomingRequestsHandler;

	/** The newest seen block number. */
	private int newestSeenBlockNumber = 0;

	/**
	 * 
	 * @param node
	 */
	public TransitRequestManagerImpl(TransitNode node) {
		this.node = node;

		/* Set the node's request strategy. */
		switch (node.getSettings().getParam(TransitParams.REQUEST_STRATEGY)) {
		case 0:
		case 1:
			this.outgoingRequestsHandler = new TransitRequestStrategyLoadBased(this);
			break;
		case 2:
			this.outgoingRequestsHandler = new TransitRequestStrategyWithIncentive(this);
			break;
		case 3:
			this.outgoingRequestsHandler = new TransitRequestStrategyPullToken(this);
			break;
		default:
			throw new AssertionError("Unknown REQUEST_STRATEGY selected.");
		}

		/* Set the node's serving strategy. */
		switch (node.getSettings().getParam(TransitParams.SERVING_STRATEGY)) {
		case 0:
			this.incomingRequestsHandler = new TransitServingStrategyFirstComeFirstServe(this);
			break;
		case 1:
			this.incomingRequestsHandler = new TransitServingStrategyWithIncentive(this);
			break;
		case 9:
			this.incomingRequestsHandler = new TransitServingStrategyMalicous(this);
			break;
		default:
			throw new AssertionError("Unknown SERVING_STRATEGY selected.");
		}

		final long BUFFERMAP_EXCHANGE_INTERVAL = node.getSettings().getTime(TransitTimes.BUFFERMAP_EXCHANGE_INTERVAL);
		if (BUFFERMAP_EXCHANGE_INTERVAL > 0) {
			node.getMessageHandler().addPiggybackListener(new BuffermapSender(BUFFERMAP_EXCHANGE_INTERVAL));
		}
		node.getNeighborhood().getConnectionManager().addConnectionListener(this);
	}

	/**
	 * Gets the node.
	 * 
	 * @return the node
	 */
	public TransitNode getNode() {
		return node;
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		switch (peerStatus) {
		case PRESENT:
		case TO_JOIN:
			break;

		case ABSENT:
			break;

		default:
			throw new AssertionError("Unknown PeerStatus.");
		}
	}

	@Override
	public void cancelRequest(int blocknum) {
		outgoingRequestsHandler.cancelRequest(blocknum);
	}

	@Override
	public void receivedRequestedBlock(TransitBlockMessage message, TransitReplyHandler replyTo, boolean isDuplicate) {
		TransitConnection connection = node.getNeighborhood().getConnectionManager()
		        .getConnection(message.getSenderContact(), ConnectionDirection.IN);
		outgoingRequestsHandler.receivedRequestedBlock(connection, message);
		replyTo.reply(new AckBlockMessage(node.getLocalOverlayContact(), message.getBlockNumber()));
	}

	@Override
	public void receivedRequestMessage(TransitRequestMessage message, TransitReplyHandler replyTo) {

		switch (message.getType()) {
		case OFFER: {
			receivedBufferMap(message);
		}
			break;

		case REQUEST: {
			assert hasAllBlocks(message.getRequestSchedule());
			final TransitConnection outConnection = getNode().getNeighborhood().getConnectionManager()
			        .getConnection(message.getSenderContact(), ConnectionDirection.OUT);
			if (outConnection == null) {
				/* Might happen due to timeout in 2wy scenario. */
				return;
			}
			outConnection.setRequest(canFulfill(message));
			outConnection.setRequestState(RequestState.ACTIVE);
			incomingRequestsHandler.handleRequest(outConnection, outConnection.getRequest());
		}
			break;

		case ALTERED_REQUEST: {
			TransitConnection inConnection = getNode().getNeighborhood().getConnectionManager()
			        .getConnection(message.getSenderContact(), ConnectionDirection.IN);
			outgoingRequestsHandler.requestAltered(inConnection, message.getRequestSchedule());
			inConnection.setRequest(message.getRequestSchedule());
		}
			break;

		case DENY_CAPACITY: {
			TransitConnection inConnection = getNode().getNeighborhood().getConnectionManager()
			        .getConnection(message.getSenderContact(), ConnectionDirection.IN);
			if (DEBUG) {
				System.err.println(Time.getFormattedTime(Time.getCurrentTime()) + " " + node.getLocalOverlayContact()
				        + " REQUEST DENIED by " + message.getSenderContact() + " because of capacity: "
				        + message.getRequestSchedule());
			}

			inConnection.setRequestState(RequestState.CANCELED);
			inConnection.setRequest(null);
			outgoingRequestsHandler.requestDenied(inConnection);
		}
			break;

		default:
			throw new AssertionError("Unknown type " + message.toString());
		}
	}

	/**
	 * Gets the newest seen block number.
	 * 
	 * @return the newest seen block number
	 */
	@Override
	public int getNewestSeenBlockNumber() {
		return newestSeenBlockNumber;
	}

	/**
	 * Called when a REQUEST arrived. Has to return the appropriate Schedule.
	 * 
	 * @param request
	 * @return
	 */
	protected TransitRequestSchedule canFulfill(TransitRequestMessage request) {
		assert hasAllBlocks(request.getRequestSchedule());
		return request.getRequestSchedule();
	}

	/**
	 * Returns all connections that have the requested block
	 * 
	 * @param block
	 * @return
	 */
	protected List<TransitConnection> whoHas(int block) {
		List<TransitConnection> cons = node.getNeighborhood().getConnectionManager()
		        .getConnections(ConnectionDirection.IN, ConnectionState.OPEN, null, RequestState.INACTIVE);
		List<TransitConnection> available = new LinkedList<TransitConnection>();
		for (TransitConnection connection : cons) {
			int blkIdx = block - connection.getBufferMap().getBlockOffset();
			if (blkIdx >= 0 && connection.getBufferMap().getBlockMap().get(blkIdx)) {
				available.add(connection);
			}
		}
		return available;
	}

	/**
	 * true, if we have all blocks that are requested
	 * 
	 * @param request
	 * @return
	 */
	protected boolean hasAllBlocks(TransitRequestSchedule request) {
		int offset = request.getBlockOffset();
		BitSet requestMap = request.getBlockMap();
		// BitSet available = (BitSet) (node.getVideo().getBlockMap()
		// .get(offset, offset + requestMap.size()).clone());
		BitSet available = (BitSet) (node.getVideo().getBlockMap(offset, offset + requestMap.size()).clone());
		available.and(requestMap);
		return available.equals(requestMap);
	}

	/**
	 * Updates the BufferMap of a connection
	 * 
	 * @param message
	 */
	protected void receivedBufferMap(TransitRequestMessage message) {
		TransitConnection connection = node.getNeighborhood().getConnectionManager()
		        .getConnection(message.getSenderContact(), ConnectionDirection.IN);
		if (connection != null) {
			connection.setBufferMap(message.getRequestSchedule());

			/* Update latest seen block. */
			int blockOffset = message.getRequestSchedule().getBlockOffset();
			blockOffset += message.getRequestSchedule().getBlockMap().length() - 1;
			if (blockOffset > newestSeenBlockNumber) {
				newestSeenBlockNumber = blockOffset;
			}
		}
	}

	/**
	 * Has to return a BufferMap of our node
	 * 
	 * @return
	 */
	protected TransitRequestSchedule createBuffermap(TransitContact to) {

		/*
		 * Create the buffermap from the current position of the node that it is
		 * sent to - past is not needed!
		 */
		// int nodePos = getNode().getScheduler().getBufferBegin();
		int nodePos = to.getCurrentChunk();
		int size = Math.max(StreamingConfiguration.BUFFER_SIZE,
		        node.getSettings().getParam(TransitParams.REQUEST_BUFFERMAP_SIZE));

		int blockOffset = nodePos * node.BLOCKS_PER_CHUNK;
		if (blockOffset < 0) {
			blockOffset = 0;
		}
		// BitSet buffermap = (BitSet) (node.getVideo().getBlockMap()
		// .get(blockOffset, blockOffset + node.BLOCKS_PER_CHUNK * size)
		// .clone());
		BitSet buffermap = node.getVideo().getBlockMap(blockOffset, size * node.BLOCKS_PER_CHUNK);
		return new TransitRequestSchedule(blockOffset, buffermap);
	}

	@Override
	public void onConnectionStateChanged(TransitConnection connection, ConnectionState oldState,
	        ConnectionState newState) {
		if (newState == ConnectionState.CLOSED && connection.getDirection() == ConnectionDirection.IN) {
			outgoingRequestsHandler.requestDenied(connection);
		}
	}

	/**
	 * Information regarding a single requested block, be it outgoing or
	 * incoming.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	public class RequestInfo {

		public final int block;

		private final List<TransitContact> denied;

		private final List<TransitContact> currentlyRequesting;

		private final Map<TransitContact, TransitConnection> requestedViaConnections;

		private long lastRequest = Time.getCurrentTime();

		private TransitConnection lastRequestConnection;

		private boolean finished = false;

		private StringBuilder info;

		public RequestInfo(int block) {
			this.block = block;
			this.denied = new LinkedList<TransitContact>();
			this.currentlyRequesting = new LinkedList<TransitContact>();
			this.requestedViaConnections = new LinkedHashMap<TransitContact, TransitConnection>();
			if (DEBUG) {
				info = new StringBuilder();
			}
		}

		public void requested(TransitConnection connection, TransitRequestSchedule request) {
			if (DEBUG) {
				info.append("\t" + Time.getFormattedTime(Time.getCurrentTime()) + "Requesting via "
				        + connection.toString() + "\n");
			}
			lastRequestConnection = connection;
			lastRequest = Time.getCurrentTime();
			currentlyRequesting.add(connection.getEndpoint());
			requestedViaConnections.put(connection.getEndpoint(), connection);
		}

		/**
		 * If a block arrived via a connection and this connection is
		 * responsible for other blocks as well, it will update the lastRequest
		 * for those other blocks to prevent duplicates.
		 */
		public void updateLastRequest() {
			lastRequest = Time.getCurrentTime();
			if (DEBUG) {
				info.append("\t" + Time.getFormattedTime(Time.getCurrentTime()) + " Updated last Request to "
				        + lastRequest + "\n");
			}
		}

		/**
		 * Timestamp of last request
		 * 
		 * @return
		 */
		public long getLastRequest() {
			return lastRequest;
		}

		/**
		 * Request has been denied
		 * 
		 * @param connection
		 */
		public void deniedBy(TransitContact contact) {
			this.denied.add(contact);
			this.currentlyRequesting.remove(contact);
			TransitConnection con = this.requestedViaConnections.remove(contact);
			if (DEBUG) {
				info.append("\t" + Time.getFormattedTime(Time.getCurrentTime()) + " Denied by " + contact.toString()
				        + " on " + con.toString() + "\n");
			}
		}

		public boolean allowRequest(TransitConnection connection) {
			/*
			 * After one second, allow a new request in every case!
			 */
			if (currentlyRequesting.isEmpty()) {
				// we currently do not query any contacts
				return true;
			} else if (denied.contains(connection.getEndpoint())) {
				return false;
			} else if (lastRequest + Time.SECOND < Time.getCurrentTime()
			        || getLastRequestConnection().getState() != ConnectionState.OPEN) {
				// after one second without result, allow a request
				if (DEBUG) {
					info.append("\t" + Time.getFormattedTime(Time.getCurrentTime()) + "Allowing on "
					        + connection.toString() + ", because one second without answer passed.\n");
				}
				return true;
			} else {
				return false;
			}
		}

		public void finished() {
			if (!finished) {
				if (DEBUG) {
					info.append("\t" + Time.getFormattedTime(Time.getCurrentTime()) + "Finished \n");
				}
				for (TransitConnection connection : requestedViaConnections.values()) {
					if (connection.getRequestState() == RequestState.ACTIVE) {
						TransitRequestSchedule request = connection.getRequest();
						int blockmapIdx = block - request.getBlockOffset();
						request.getBlockMap().set(blockmapIdx, false);
						if (request.getBlockMap().cardinality() == 0) {
							/*
							 * Request is fully answered
							 */
							connection.setRequest(null);
							connection.setRequestState(RequestState.INACTIVE);
						}
					}
				}
				requestedViaConnections.clear();
				denied.clear();
				currentlyRequesting.clear();
				finished = true;
			}
		}

		@Override
		public String toString() {
			return block + " " + requestedViaConnections.toString() + " last: "
			        + Time.getFormattedTime(Time.getCurrentTime() - lastRequest) + " denied by " + denied.toString()
			        + (DEBUG ? "\n" + info.toString() : "");
		}

		/**
		 * Checks if is finished.
		 * 
		 * @return true, if is finished
		 */
		public boolean isFinished() {
			return finished;
		}

		/**
		 * Gets the last request connection.
		 * 
		 * @return the last request connection
		 */
		public TransitConnection getLastRequestConnection() {
			return lastRequestConnection;
		}

	}

	/**
	 * Piggybacks our buffermap in specified intervals
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	public class BuffermapSender extends PeriodicPiggybackListener {

		public BuffermapSender(long interval) {
			super(interval);
		}

		@Override
		protected TransitMessage whatToPiggybackPeriodically(TransitContact to) {
			if (to.getType() != ContactType.CLIENT || getNode().getScheduler().getPlaybackState() == PlaybackState.NONE) {
				return null;
			}
			TransitConnection co = getNode().getNeighborhood().getConnectionManager()
			        .getConnection(to, ConnectionDirection.OUT);
			if (getNode().getScheduler().getPlaybackState() != PlaybackState.NONE && co != null
			        && co.getState() == ConnectionState.OPEN) {
				return new SendRequestMessage(getNode().getLocalOverlayContact(), RequestMsgType.OFFER,
				        createBuffermap(to));
			}
			return null;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tud.kom.p2psim.api.overlay.streaming.transit.TransitRequestManager
	 * #requestBlocks(int, java.util.BitSet)
	 */
	@Override
	public void requestBlocks(int blockOffset, BitSet missingBlocks) {
		/*
		 * Here, we check for connections with outdated requests, which might
		 * occur due to the fact that the request itself is not ACKed. Might be
		 * a good idea to just sendAndWait requests instead...
		 */
		List<TransitConnection> activeReqs = node
				.getNeighborhood()
				.getConnectionManager()
				.getConnections(ConnectionDirection.IN, ConnectionState.OPEN,
						null, RequestState.ACTIVE);
		for (TransitConnection con : activeReqs) {
			if (Time.getCurrentTime() - con.getLastRequestStateChangeTimestamp() > 
				node.getSettings().getTime(TransitTimes.REQUEST_TIMEOUT)) {
				con.setRequest(null);
				con.setRequestState(RequestState.INACTIVE);
			}
		}

		outgoingRequestsHandler.requestBlocks(blockOffset, missingBlocks);
	}

}
