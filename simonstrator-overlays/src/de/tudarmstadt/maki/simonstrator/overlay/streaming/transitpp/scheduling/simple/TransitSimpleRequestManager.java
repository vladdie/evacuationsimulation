/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.simple;

import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.RequestState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.AbstractTransitRequestManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitRequestSchedule;

/**
 * Basic version of a requestManager.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public class TransitSimpleRequestManager extends AbstractTransitRequestManager {

	/**
	 * FIXME currently only used in the vis for interaction and demos
	 */
	public boolean enableAllRequests = true;

	private static final Comparator<TransitConnection> COMP_LOAD = new Comparator<TransitConnection>() {
		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			if (o1.getEndpoint().getType() == ContactType.SOURCE
					&& o2.getEndpoint().getType() != ContactType.SOURCE) {
				return 1;
			} else if (o2.getEndpoint().getType() == ContactType.SOURCE
					&& o1.getEndpoint().getType() != ContactType.SOURCE) {
				return -1;
			}
			return o1.getEndpoint().getLoad() - o2.getEndpoint().getLoad();
		}
	};

	public TransitSimpleRequestManager(TransitNode node) {
		super(node); 
		enableAllRequests = getNode().getSettings().getParam(
				TransitParams.REQUEST_STRATEGY) == 1;
	}

	@Override
	public void requestBlocks(int blockOffset, BitSet mask) {
		List<TransitConnection> connections = getNode()
				.getNeighborhood()
				.getConnectionManager()
				.getConnections(ConnectionDirection.IN, ConnectionState.OPEN,
						(enableAllRequests ? null : ScheduleState.ACTIVE),
						RequestState.INACTIVE);

		Collections.sort(connections, COMP_LOAD);
		// Collections.shuffle(connections, new Random(Simulator.getRandom()
		// .nextLong()));

		if (getNumberOfOpenRequests() > getNode().getSettings().getParam(
				TransitParams.REQUEST_MAX_PARALLEL)) {
			return;
		}

		for (TransitConnection connection : connections) {
			if (mask.isEmpty()) {
				break;
			}

			if (connection.getCurrentRTTEstimation() > getNode().getSettings()
					.getTime(TransitTimes.REQUEST_RTT_MAX)) {
				continue;
			}

			// FIXME[JR]: Why 6??
			if (connection.getEndpoint().getLoad() > 6) {
				continue;
			}

			assert connection.getRequestState() == RequestState.INACTIVE;
			assert connection.getRequest() == null;

			TransitRequestSchedule bufferMap = connection.getBufferMap();
			if (bufferMap != null) {
				BitSet possible = getMatch(connection, bufferMap, blockOffset,
						mask);
				if (possible == null || possible.isEmpty()) {
					continue;
				}
				TransitRequestSchedule request = new TransitRequestSchedule(
						blockOffset, possible);
				if (!getNode().getBandwidthManager().hasSpareBandwidthFor(
						blockOffset + possible.nextSetBit(0),
						ConnectionDirection.IN)) {
					break;
				}
				getNode().getBandwidthManager().reserveBandwidthFor(
						blockOffset + possible.nextSetBit(0),
						ConnectionDirection.IN);

				doRequest(connection, request);
				mask.andNot(possible);
			}
		}

	}

}
