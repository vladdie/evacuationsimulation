/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.operation.Operation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler.PeriodicPiggybackListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.ConnectionMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.ConnectionMessage.ConnectionMessageType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.OfferNeighborhoodMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.RequestState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.operations.ConnectionControlOperation;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * This component manages connections to a subset of all contacts that are
 * available in our routing table. The number of concurrent connections depends
 * on the state of the node and the demands of the scheduler.
 * 
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 10.06.2012
 */
public class TransitConnectionManager implements TransitConnectionListener, TransitPayloadListener,
        OperationCallback<TransitConnection> {

	/** The sliding window size. */
	private final long SLIDING_WINDOW_SIZE;

	/**
	 * Container for all connections
	 */
	private final Map<UniqueID, TransitConnection> allInConnections = new LinkedHashMap<UniqueID, TransitConnection>();
	private final Map<UniqueID, TransitConnection> allOutConnections = new LinkedHashMap<UniqueID, TransitConnection>();

	/*
	 * FIXME[JR]: Introduce a flag that allows to disable the maintenance of
	 * this data structures if they are not requires (i.e. in case no
	 * visualization is used)
	 */
	private final CopyOnWriteArrayList<TransitConnection> allInConnectionsThreadsafe = new CopyOnWriteArrayList<TransitConnection>();
	private final CopyOnWriteArrayList<TransitConnection> allOutConnectionsThreadsafe = new CopyOnWriteArrayList<TransitConnection>();

	/**
	 * Endpoint ID to ConnectionControlOperation
	 */
	private final Map<UniqueID, ConnectionControlOperation> ccopsIn = new LinkedHashMap<UniqueID, ConnectionControlOperation>();
	private final Map<UniqueID, ConnectionControlOperation> ccopsOut = new LinkedHashMap<UniqueID, ConnectionControlOperation>();

	private final TransitNode node;

	private final List<TransitConnectionListener> connectionListeners = new LinkedList<TransitConnectionListener>();

	private final List<TransitPayloadListener> payloadListeners = new LinkedList<TransitPayloadListener>();

	private final TimeoutSet<TransitContact> justClosed = new TimeoutSet<TransitContact>(10 * Time.SECOND);

	private final Comparator<TransitConnection> COMP_LASTBLOCK_NEWEST_FIRST = new Comparator<TransitConnection>() {
		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			return (int) Math.signum(o2.getLastBlockTransferredTimestamp() - o1.getLastBlockTransferredTimestamp());
		}
	};

	private final Comparator<TransitConnection> COMP_LOAD = new Comparator<TransitConnection>() {
		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			return o1.getEndpoint().getLoad() - o2.getEndpoint().getLoad();
		}
	};

	private final boolean ENFORCE_TWO_WAY_DATA_TRANSFER;

	/**
	 * Create the connection manager as part of the
	 * {@link TransitNeighborhoodManager}
	 * 
	 * @param node
	 */
	public TransitConnectionManager(TransitNode node) {
		this.node = node;
		node.getMessageHandler().addPiggybackListener(
		        new ExchangeNeighborhood(node.getSettings().getTime(TransitTimes.NEIGHBORHOOD_EXCHANGE_INTERVAL)));

		SLIDING_WINDOW_SIZE = node.getSettings().getTime(TransitTimes.INCENTIVE_SLIDING_WINDOW_SIZE);
		ENFORCE_TWO_WAY_DATA_TRANSFER = node.getSettings().getParam(
		        TransitParams.CONNECTION_ALLOW_TWO_WAY_DATA_TRANSFER) > 0
		        && node.getSettings().getParam(TransitParams.CONNECTION_ENFORCE_TWO_WAY_DATA_TRANSFER) > 0;

	}

	/**
	 * The node
	 * 
	 * @return
	 */
	protected TransitNode getNode() {
		return node;
	}

	/**
	 * Do open a connection - external interface.
	 * 
	 * @param to
	 *            the to
	 * @return true, if successful
	 */
	public TransitConnection getOrOpenConnection(TransitContact to) {
		if (to == null) {
			return null;
		}
		final ConnectionControlOperation cop = ccopsIn.get(to.getNodeID());
		if (cop == null || cop.isStopped()) {
			return openIncomingConnection(to);
		}
		return cop.getConnection();
	}

	/**
	 * Negotiates a new connection with the given contact.
	 * 
	 * @param to
	 */
	private TransitConnection openIncomingConnection(TransitContact to) {

		/*
		 * Incoming connections, called by onNewNeighbors. We want to get
		 * served, so ConnectionDirection.IN.
		 */
		ConnectionControlOperation cop = ccopsIn.get(to.getNodeID());

		if (cop == null || cop.isStopped()) {
			TransitConnection con = new TransitConnection(node.getLocalOverlayContact(), to, ConnectionDirection.IN,
			        this, SLIDING_WINDOW_SIZE);
			assert !allInConnections.containsKey(to.getNodeID());
			allInConnections.put(to.getNodeID(), con);
			allInConnectionsThreadsafe.add(con);
			cop = new ConnectionControlOperation(node, this, con);
			ccopsIn.put(to.getNodeID(), cop);
			// cop.startWithDelay(allConnections.size() * 50
			// * Simulator.MILLISECOND_UNIT);
			cop.start();
			cop.doOpenConnection();
			return con;
		} else {
			throw new AssertionError();
		}
	}

	/**
	 * Dispatcher for incoming {@link ConnectionMessage}s. These messages are
	 * forwarded to the correct {@link ConnectionControlOperation}.
	 * 
	 * @param msg
	 */
	public void receivedConnectionMessage(ConnectionMessage msg, TransitReplyHandler replyTo) {

		/*
		 * ConnectionMessage received, OPEN, CLOSE or PING. Other side want's to
		 * get served.
		 */
		final TransitContact sender = msg.getSenderContact();
		final boolean isTransferwiseOutgoing = (msg.getDirection() == ConnectionDirection.IN) ? true : false;
		if (isTransferwiseOutgoing) {
			ConnectionControlOperation cop = ccopsOut.get(sender.getNodeID());
			if (cop == null || cop.isStopped()) {
				if (msg.getMessageType() == ConnectionMessageType.OPEN) {
					assert !allOutConnections.containsKey(sender.getNodeID());
					TransitConnection con = new TransitConnection(node.getLocalOverlayContact(), sender,
					        ConnectionDirection.OUT, this, SLIDING_WINDOW_SIZE);
					allOutConnections.put(sender.getNodeID(), con);
					allOutConnectionsThreadsafe.add(con);
					cop = new ConnectionControlOperation(node, this, con);
					ccopsOut.put(sender.getNodeID(), cop);
					cop.start();
					cop.receive(msg, replyTo);
					if (ENFORCE_TWO_WAY_DATA_TRANSFER) {
						final ConnectionControlOperation copIn = ccopsIn
								.get(sender.getNodeID());
						if (copIn == null || copIn.isStopped()) {
							openIncomingConnection(sender);
						}
					}
					return;
				} else {
					return;
				}
			}

			cop.receive(msg, replyTo);

			if (ENFORCE_TWO_WAY_DATA_TRANSFER && msg.getMessageType() == ConnectionMessageType.CLOSE) {
				final ConnectionControlOperation copIn = ccopsIn.get(sender
						.getNodeID());
				if (copIn != null && copIn.getConnection().getState() == ConnectionState.OPEN) {
					copIn.closeConnection();
				}
			}

		} else {
			ConnectionControlOperation cop = ccopsIn.get(sender.getNodeID());
			if (cop != null && !cop.isStopped()) {
				cop.receive(msg, replyTo);

				if (ENFORCE_TWO_WAY_DATA_TRANSFER && msg.getMessageType() == ConnectionMessageType.CLOSE) {
					final ConnectionControlOperation copOut = ccopsOut
							.get(sender.getNodeID());
					if (copOut != null && copOut.getConnection().getState() == ConnectionState.OPEN) {
						copOut.closeConnection();
					}
				}

			}
		}
	}

	/**
	 * Called for every payload message (video block), checks if the connection
	 * is open at all or if the source might have missed a close-message
	 * 
	 * @param from
	 */
	public void receivedPayloadMessage(TransitContact from) {
		TransitConnection coIn = getConnection(from, ConnectionDirection.IN);
		if (coIn == null) {
			return;
		}
		if (coIn.getState() != ConnectionState.OPEN) {
			ccopsIn.get(from.getNodeID()).closeConnection();
		}
		TransitConnection coOut = getConnection(from, ConnectionDirection.OUT);
		if (coOut == null) {
			return;
		}
		if (coOut.getState() != ConnectionState.OPEN) {
			ccopsOut.get(from.getNodeID()).closeConnection();
		}
		this.onPayloadReceived(from);
	}

	/**
	 * Updates the RTT-Estimation of the given connection
	 * 
	 * @param newRtt
	 */
	public void updateRttEstimation(TransitContact from, long rtt) {
		if (rtt > 0) {

			/* Update both directions. Same RTT anyway. */
			ConnectionControlOperation copIn = ccopsIn.get(from.getNodeID());
			if (copIn != null) {
				copIn.updateRttEstimation(rtt);
				copIn.getConnection().getEndpoint().update(from);
			}

			ConnectionControlOperation copOut = ccopsOut.get(from.getNodeID());
			if (copOut != null) {
				copOut.updateRttEstimation(rtt);
				copOut.getConnection().getEndpoint().update(from);
			}
		}
	}

	@Override
	public void calledOperationFailed(Operation<TransitConnection> op) {
		throw new AssertionError("This operation is not allowed to fail!");
	}

	@Override
	public void calledOperationSucceeded(Operation<TransitConnection> op) {
		if (op instanceof ConnectionControlOperation) {
			ConnectionControlOperation cop = (ConnectionControlOperation) op;
			if (cop.isStopped()) {
				/*
				 * Removes the connection from the list of all connections.
				 */
				ConnectionDirection cDirection = cop.getConnection().getDirection();
				if (cDirection == ConnectionDirection.IN) {
					TransitConnection connection = allInConnections.remove(((ConnectionControlOperation) op)
									.getConnection().getEndpoint().getNodeID());
					assert connection != null;
					allInConnectionsThreadsafe.remove(connection);
					ccopsIn.remove(connection.getEndpoint().getNodeID());
					justClosed.addNow(connection.getEndpoint());
				} else {
					TransitConnection connection = allOutConnections.remove(((ConnectionControlOperation) op)
									.getConnection().getEndpoint().getNodeID());
					assert connection != null;
					allOutConnectionsThreadsafe.remove(connection);
					ccopsOut.remove(connection.getEndpoint().getNodeID());
					justClosed.addNow(connection.getEndpoint());
				}
			}
		}
	}

	/**
	 * Returns a connection object for the given contact, can be used to
	 * aggregate bandwidth statistics etc. inside the {@link TransitConnection}
	 * 
	 * @param to
	 * @return a {@link TransitConnection} or null, if the contact is not
	 *         connected to this node
	 */
	public TransitConnection getConnection(TransitContact to, ConnectionDirection direction) {
		return (direction == ConnectionDirection.IN) ? allInConnections.get(to
				.getNodeID()) : allOutConnections.get(to.getNodeID());
	}

	/**
	 * Generic way to query for a type of connections. Any combination of
	 * arguments can be null to not filter for this state.
	 * 
	 * @param direction
	 * @param connectionState
	 * @param scheduleState
	 * @param requestState
	 * @return
	 */
	public List<TransitConnection> getConnections(ConnectionDirection direction, ConnectionState connectionState,
	        ScheduleState scheduleState, RequestState requestState) {
		LinkedList<TransitConnection> result = new LinkedList<TransitConnection>();

		/*
		 * FIXME BR: why exactly was this code duplicated? Makes no sense to
		 * me...
		 */

		/* In connections. */
		if (direction == ConnectionDirection.IN || direction == null) {
			for (TransitConnection connection : allInConnections.values()) {
				if ((connectionState == null || connection.getState() == connectionState)
				        && (direction == null || connection.getDirection() == direction)
				        && (scheduleState == null || connection.getScheduleState() == scheduleState)
				        && (requestState == null || connection.getRequestState() == requestState)) {
					result.add(connection);
				}
			}
		}

		/* Out connections. */
		if (direction == ConnectionDirection.OUT || direction == null) {
			for (TransitConnection connection : allOutConnections.values()) {
				if ((connectionState == null || connection.getState() == connectionState)
				        && (direction == null || connection.getDirection() == direction)
				        && (scheduleState == null || connection.getScheduleState() == scheduleState)
				        && (requestState == null || connection.getRequestState() == requestState)) {
					result.add(connection);
				}
			}
		}

		return result;
	}

	/**
	 * Gets the connections thread safe.
	 * 
	 * @param direction
	 *            the direction
	 * @param connectionState
	 *            the connection state
	 * @param scheduleState
	 *            the schedule state
	 * @param requestState
	 *            the request state
	 * @return the connections thread safe
	 */
	public List<TransitConnection> getConnectionsThreadSafe(ConnectionDirection direction,
	        ConnectionState connectionState, ScheduleState scheduleState, RequestState requestState) {
		LinkedList<TransitConnection> result = new LinkedList<TransitConnection>();

		/* In connections. */
		if (direction == ConnectionDirection.IN || direction == null) {
			for (TransitConnection connection : allInConnectionsThreadsafe) {
				if ((connectionState == null || connection.getState() == connectionState)
				        && (direction == null || connection.getDirection() == direction)
				        && (scheduleState == null || connection.getScheduleState() == scheduleState)
				        && (requestState == null || connection.getRequestState() == requestState)) {
					result.add(connection);
				}
			}
		}

		/* Out connections. */
		if (direction == ConnectionDirection.OUT || direction == null) {
			for (TransitConnection connection : allOutConnectionsThreadsafe) {
				if ((connectionState == null || connection.getState() == connectionState)
				        && (direction == null || connection.getDirection() == direction)
				        && (scheduleState == null || connection.getScheduleState() == scheduleState)
				        && (requestState == null || connection.getRequestState() == requestState)) {
					result.add(connection);
				}
			}
		}

		return result;
	}

	/**
	 * Shortcut if you need to access all connections
	 * 
	 * @return
	 */
	public List<TransitConnection> getAllInAndOutConnections() {
		LinkedList<TransitConnection> resultList = new LinkedList<TransitConnection>(allInConnections.values());
		resultList.addAll(allOutConnections.values());
		return resultList;
	}

	/**
	 * Gets the all connections in one direction.
	 * 
	 * @param direction
	 *            the direction
	 * @return the all connections
	 */
	public List<TransitConnection> getAllConnections(ConnectionDirection direction) {
		return new LinkedList<TransitConnection>((direction == ConnectionDirection.IN) ? allInConnections.values()
		        : allOutConnections.values());
	}

	/**
	 * Close a connection.
	 * 
	 * @param connection
	 *            the connection
	 */
	public void closeConnection(TransitConnection connection) {

		if (connection == null || connection.getEndpoint() == null)
			return;

		ConnectionDirection direction = connection.getDirection();
		if (direction == ConnectionDirection.IN) {
			ccopsIn.get(connection.getEndpoint().getNodeID()).closeConnection();
		} else {
			ccopsOut.get(connection.getEndpoint().getNodeID())
					.closeConnection();
		}

		if (ENFORCE_TWO_WAY_DATA_TRANSFER) {
			ConnectionControlOperation ccop = null;
			if (direction == ConnectionDirection.IN) {
				ccop = ccopsOut.get(connection.getEndpoint().getNodeID());
			} else {
				ccop = ccopsIn.get(connection.getEndpoint().getNodeID());
			}
			if (ccop != null) {
				ccop.closeConnection();
			}
		}
	}

	/**
	 * Called by the {@link ConnectionControlOperation} on an incoming
	 * open-request.
	 * 
	 * @param cop
	 * @return
	 */
	public boolean mayOpen(ConnectionControlOperation cop) {
		assert cop.getResult().getDirection() == ConnectionDirection.OUT;
		int currentConnections = getConnections(ConnectionDirection.OUT, ConnectionState.OPEN, null, null).size();
		if (currentConnections >= node.getSettings().getParam(TransitParams.CONNECTION_MAX_OUTGOING)) {
			return false;
		}
		return true;
	}

	/**
	 * Called by the MessageHandler / NeighborhoodManager as soon as a timeout
	 * is detected
	 * 
	 * @param contact
	 */
	public void timeout(TransitContact contact) {
		ConnectionControlOperation copIn = ccopsIn.get(contact.getNodeID());
		if (copIn != null) {
			copIn.handleTimeout();
		}

		ConnectionControlOperation copOut = ccopsOut.get(contact.getNodeID());
		if (copOut != null) {
			copOut.handleTimeout();
		}
	}

	/**
	 * This method is called by the neighborhood manager and opens connections
	 * to some or all of the new contacts provided by the neighborhood manager.
	 * 
	 * @param newContacts
	 */
	public void onNewNeighbors(List<TransitContact> newContacts) {
		assert !getNode().isServer();

		for (TransitContact contact : newContacts) {
			assert !contact.equals(node.getLocalOverlayContact());
			assert getConnection(contact, ConnectionDirection.IN) == null;
		}
		newContacts.removeAll(justClosed.getUnmodifiableSet());

		List<TransitConnection> openConnections = getConnections(ConnectionDirection.IN, ConnectionState.OPEN, null,
		        null);
		int opening = getConnections(ConnectionDirection.IN, ConnectionState.NEGOTIATING, null, null).size();

		Collections.sort(openConnections, COMP_LASTBLOCK_NEWEST_FIRST);

		long time = Time.getCurrentTime();
		int passive = 0;
		int active = 0;
		for (TransitConnection connection : openConnections) {
			if (Math.max(connection.getLastBlockTransferredTimestamp(), connection.getLastStateChangeTimestamp())
			        + node.getSettings().getTime(TransitTimes.CONNECTION_INACTIVITY_TIMEOUT) < time) {
				passive++;
			} else {
				active++;
			}
		}

		/*
		 * Close passive connection to make room for new contacts, if there are
		 * too many of them
		 */
		int i = 0;
		while (passive > node.getSettings().getParam(TransitParams.CONNECTION_MAX_INCOMING_INACTIVE)) {
			TransitConnection toClose = openConnections.get(openConnections.size() - 1 - i);
			ccopsIn.get(toClose.getEndpoint().getNodeID()).closeConnection();
			i++;
			passive--;
		}

		/*
		 * Close the oldest passive connection with a probability of
		 * CONNECTION_INACTIVITY_KILL_PROBABILITY if no connection has already
		 * been closed
		 */
		if (i == 0 && passive > 0
		        && passive <= node.getSettings().getParam(TransitParams.CONNECTION_MAX_INCOMING_INACTIVE)) {
			int rnd = Randoms.getRandom(this).nextInt(100);
			if (rnd < node.getSettings().getParam(TransitParams.CONNECTION_INACTIVITY_KILL_PROBABILITY)) {
				TransitConnection toKill = openConnections.get(openConnections.size() - 1);
				ccopsIn.get(toKill.getEndpoint().getNodeID()).closeConnection();
				passive--;
			}
		}

		/*
		 * Open new connections
		 */
		int extraBuffer = (active == 0 ? Math.max(0, 5 - passive) : 0);
		while (opening + passive + active < node.getSettings().getParam(TransitParams.CONNECTION_MAX_INCOMING)
		        + extraBuffer) {
			if (newContacts.isEmpty()) {
				break;
			}
			if (opening + passive + active >= node.getSettings().getParam(TransitParams.CONNECTION_MAX_INCOMING)) {
				break;
			}
			TransitContact to = newContacts.remove(0);
			openIncomingConnection(to);
			opening++;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitConnectionListener
	 * #onConnectionStateChanged(de.tudarmstadt.maki.simonstrator
	 * .overlay.streaming.transit.neighborhood.TransitConnection,
	 * de.tudarmstadt.
	 * maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection
	 * .ConnectionState,
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit
	 * .neighborhood.TransitConnection.ConnectionState)
	 */
	@Override
	public void onConnectionStateChanged(TransitConnection connection, ConnectionState oldState,
	        ConnectionState newState) {

		assert (oldState == ConnectionState.CLOSING && newState == ConnectionState.CLOSED)
		        || oldState != ConnectionState.CLOSING;

		/*
		 * Dispatch to all listeners
		 */
		for (TransitConnectionListener listener : connectionListeners) {
			listener.onConnectionStateChanged(connection, oldState, newState);
		}

		if (connection.getDirection() == ConnectionDirection.IN && oldState == ConnectionState.NEGOTIATING) {
			/*
			 * If we run out of open connections, use the spare-list
			 */
			int open = getConnections(ConnectionDirection.IN, ConnectionState.OPEN, null, null).size();
			if (open < node.getSettings().getParam(TransitParams.CONNECTION_MIN_INCOMING)) {
				node.getNeighborhood().requestNeighbors();
			}
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitPayloadListener
	 * #onPayloadReceived(de.tudarmstadt.maki.simonstrator
	 * .overlay.streaming.transit.neighborhood.TransitContact)
	 */
	@Override
	public void onPayloadReceived(TransitContact from) {
		for (TransitPayloadListener listener : payloadListeners) {
			listener.onPayloadReceived(from);
		}
	}

	/**
	 * Adds a listener that is informed if connections change their state
	 * 
	 * @param listener
	 */
	public void addConnectionListener(TransitConnectionListener listener) {
		connectionListeners.add(listener);
	}

	/**
	 * Adds the payload listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void addPayloadListener(TransitPayloadListener listener) {
		payloadListeners.add(listener);
	}

	/**
	 * Removes the specified listener from the ConnectionManager
	 * 
	 * @param listener
	 */
	public void removeConnectionListener(TransitConnectionListener listener) {
		connectionListeners.remove(listener);
	}

	/**
	 * Removes the payload listener.
	 * 
	 * @param listener
	 *            the listener
	 */
	public void removePayloadListener(TransitPayloadListener listener) {
		payloadListeners.remove(listener);
	}

	/**
	 * A list of neighbors that can be sent to other nodes.
	 * 
	 * @return
	 */
	public List<TransitContact> getRandomNeighborsFor(TransitContact to) {
		List<TransitContact> neighbors = new LinkedList<TransitContact>();
		List<TransitConnection> candidates = new LinkedList<TransitConnection>(getConnections(null,
		        ConnectionState.OPEN, null, null));

		if (!candidates.isEmpty()) {
			// TODO
			// Add some randomness
			// Collections.rotate(candidates,
			// Simulator.getRandom().nextInt(candidates.size()));
			Collections.sort(candidates, COMP_LOAD);

			int in = 0;
			for (TransitConnection candidate : candidates) {
				if (candidate.getEndpoint().equals(to)) {
					continue;
				}
				if (in > node.getSettings().getParam(TransitParams.CONNECTION_MAX_INCOMING_INACTIVE) * 2) {
					break;
				}
				neighbors.add(candidate.getEndpoint());
				in++;
			}
		}
		return neighbors;
	}

	/**
	 * Periodically piggybacks the current neighborhood
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	public class ExchangeNeighborhood extends PeriodicPiggybackListener {

		public ExchangeNeighborhood(long interval) {
			super(interval);
		}

		@Override
		protected TransitMessage whatToPiggybackPeriodically(TransitContact to) {
			List<TransitContact> neighbors = getRandomNeighborsFor(to);
			if (!neighbors.isEmpty()) {
				return new OfferNeighborhoodMessage(getNode().getLocalOverlayContact(), neighbors);
			}
			return null;
		}

	}

}
