/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.operations;

import java.util.LinkedHashMap;

import de.tudarmstadt.maki.simonstrator.api.Message;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransInfo;
import de.tudarmstadt.maki.simonstrator.api.component.transport.TransMessageCallback;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.ConnectionMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.ConnectionMessage.ConnectionMessageType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.OfferNeighborhoodMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;

/**
 * This operation is created once for every {@link TransitConnection} and
 * manages all maintenance and control-traffic for the connection.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 22.07.2012
 */
public class ConnectionControlOperation extends
		PeriodicOperation<TransitNode, TransitConnection> implements
		TransMessageCallback {

	private static enum OpAction {
		INACTIVE, CONNECT, ACTIVE, TEARDOWN
	}

	private final static boolean DEBUG = false;

	private final TransitConnection connection;

	private final TransitMessageHandler msgHandler;

	private final TransitContact endpoint;

	private final TransitContact ownContact;

	private final LinkedHashMap<Integer, TransitMessage> waitingForReply = new LinkedHashMap<Integer, TransitMessage>();

	private OpAction currentAction;

	public ConnectionControlOperation(TransitNode component,
			OperationCallback<TransitConnection> callback,
			TransitConnection connection) {
		super(component, callback, component.getSettings().getTime(
				TransitTimes.CONNECTION_CONTROL_OPERATION_INTERVAL));
		this.connection = connection;
		this.msgHandler = component.getMessageHandler();
		this.endpoint = connection.getEndpoint();
		this.currentAction = OpAction.INACTIVE;
		this.ownContact = component.getLocalOverlayContact();
	}

	@Override
	protected void executeOnce() {

		if (!getComponent().isPresent()) {
			stop();
			operationFinished(true);
			return;
		}

		switch (currentAction) {
		case ACTIVE:
			doSendPing();
			break;

		case TEARDOWN:
			assert connection.getState() == ConnectionState.CLOSING
					|| connection.getState() == ConnectionState.CLOSED;
			if (connection.getState() == ConnectionState.CLOSED) {
				switchAction(OpAction.INACTIVE);
			} else {
				// Re-Send CLOSE?
				ConnectionMessage close = new ConnectionMessage(connection,
						ConnectionMessageType.CLOSE);
				send(close);
			}
			break;

		case INACTIVE:
			assert connection.getScheduleState() == ScheduleState.INACTIVE
					|| connection.getScheduleState() == ScheduleState.CANCELED;
			assert connection.getState() == ConnectionState.CLOSED;
			stop();
			break;

		default:
			break;
		}
		operationFinished(true);
	}


	private void switchAction(OpAction newState) {
		if (DEBUG) {
			System.out.println("CCO: Switching state from "
					+ currentAction.toString() + " to " + newState.toString()
					+ " on " + connection.toString());
		}
		if (newState != currentAction) {
			currentAction = newState;
		}
	}

	/**
	 * Updates the RTT
	 * 
	 * @param rtt
	 */
	public void updateRttEstimation(long rtt) {
		if (isStopped()) {
			return;
		}
		assert rtt > 0;
		connection.updateRTTEstimation(rtt);
		if (connection.getCurrentRTTEstimation() > getComponent().getSettings()
				.getTime(TransitTimes.CONNECTION_RTT_MAX)) {
			if (currentAction == OpAction.ACTIVE) {
				doCloseConnection();
				if (DEBUG) {
					System.out
							.println("CCO: Closing connection because of high RTT");
				}
			}
		}

	}

	private void doSendPing() {
		if (connection.getLastUpdateOfRTT() + Time.SECOND < Time
				.getCurrentTime()) {
			ConnectionMessage ping = new ConnectionMessage(connection,
					ConnectionMessageType.PING);
			send(ping);
		}
	}

	/**
	 * Opens this connection
	 */
	public void doOpenConnection() {
		assert !isStopped();
		assert connection.getDirection() == ConnectionDirection.IN;
		assert connection.getState() == ConnectionState.NEGOTIATING;
		assert currentAction == OpAction.INACTIVE;
		switchAction(OpAction.CONNECT);
		ConnectionMessage open = new ConnectionMessage(connection,
				ConnectionMessageType.OPEN);
		send(open);
	}

	/**
	 * External handle to close a connection
	 */
	public void closeConnection() {
		if (currentAction == OpAction.ACTIVE
				&& connection.getState() == ConnectionState.OPEN) {
			doCloseConnection();
		} else {
			// Just resend the close, as it might have been lost
			ConnectionMessage close = new ConnectionMessage(connection,
					ConnectionMessageType.CLOSE);
			switchAction(OpAction.TEARDOWN);
			connection.setState(ConnectionState.CLOSING);
			send(close);
		}
	}

	/**
	 * Closes this connection
	 */
	private void doCloseConnection() {
		assert !isStopped();
		assert connection.getState() == ConnectionState.OPEN;
		assert currentAction == OpAction.ACTIVE;
		switchAction(OpAction.TEARDOWN);
		connection.setState(ConnectionState.CLOSING);
		ConnectionMessage close = new ConnectionMessage(connection,
				ConnectionMessageType.CLOSE);
		send(close);
	}

	/**
	 * Cancel the negotiation of this connection.
	 */
	public void doCancelOpenConnection() {
		assert !isStopped();
		assert connection.getState() == ConnectionState.NEGOTIATING;
		assert currentAction == OpAction.CONNECT;
		assert connection.getScheduleState() == ScheduleState.INACTIVE
				|| connection.getScheduleState() == ScheduleState.CANCELED;
		switchAction(OpAction.TEARDOWN);
		connection.setState(ConnectionState.CLOSING);
		ConnectionMessage close = new ConnectionMessage(connection,
				ConnectionMessageType.CLOSE);
		send(close);
	}

	/**
	 * Called for (control-)messages that are received via the connection.
	 * 
	 * @param message
	 * @param replyTo
	 */
	public void receive(TransitMessage message, TransitReplyHandler replyTo) {

		if (isStopped()) {
			return;
		}

		if (DEBUG) {
			System.out.println(Time.getFormattedTime(Time
					.getCurrentTime())
					+ "CCO: RECEIVING  "
					+ message.toString() + " via " + connection.toString());
		}

		if (message instanceof ConnectionMessage) {
			ConnectionMessage cMsg = (ConnectionMessage) message;
			switch (cMsg.getMessageType()) {
			case PING:
				ConnectionMessage pong = new ConnectionMessage(connection,
						ConnectionMessageType.PONG);
				reply(replyTo, pong);
				break;

			case PONG:
				// nothing to do
				break;

			case OPEN:
				if (currentAction == OpAction.TEARDOWN) {
					ConnectionMessage deny = new ConnectionMessage(connection,
							ConnectionMessageType.DENY);
					reply(replyTo, deny);
					break;
				}

				// Incoming request to open a connection
				if (connection.getDirection() == ConnectionDirection.IN) {
					// Concurrent open-requests, bigger ID wins
					if (endpoint.getNodeID().compareTo(ownContact.getNodeID()) < 0) {
						// own ID > endpoint ID -> ignore!
						pong = new ConnectionMessage(connection,
								ConnectionMessageType.PONG);
						reply(replyTo, pong);
						break;
					} else {
						connection.setDirection(ConnectionDirection.OUT);
						switchAction(OpAction.INACTIVE);
					}
				}

				if (!getComponent().getNeighborhood().getConnectionManager()
						.mayOpen(this)) {
					ConnectionMessage deny = new ConnectionMessage(
							connection, ConnectionMessageType.DENY);
					OfferNeighborhoodMessage otherNeighbors = new OfferNeighborhoodMessage(
							ownContact, getComponent().getNeighborhood()
									.getConnectionManager()
									.getRandomNeighborsFor(endpoint));
					deny.piggyback(otherNeighbors);
					reply(replyTo, deny);
					switchAction(OpAction.TEARDOWN);
					connection.setState(ConnectionState.CLOSED);
					break;
				}

				assert currentAction == OpAction.INACTIVE;
				assert connection.getDirection() == ConnectionDirection.OUT;
				assert connection.getState() == ConnectionState.NEGOTIATING
						|| connection.getState() == ConnectionState.CLOSED;
				ConnectionMessage accept = new ConnectionMessage(connection,
						ConnectionMessageType.ACCEPT);
				reply(replyTo, accept);
				switchAction(OpAction.ACTIVE);
				connection.setState(ConnectionState.OPEN);
				break;

			case DENY:
				switchAction(OpAction.TEARDOWN);
				connection.setState(ConnectionState.CLOSING);
				connection.setState(ConnectionState.CLOSED);
				break;

			case ACCEPT:
				// Request to open was accepted
				assert currentAction == OpAction.CONNECT;
				assert connection.getDirection() == ConnectionDirection.IN;
				assert connection.getState() == ConnectionState.NEGOTIATING;
				switchAction(OpAction.ACTIVE);
				connection.setState(ConnectionState.OPEN);
				break;

			case CLOSE:
				if (currentAction == OpAction.TEARDOWN) {
					assert connection.getState() == ConnectionState.CLOSING
							|| connection.getState() == ConnectionState.CLOSED;
				} else if (currentAction == OpAction.ACTIVE) {
					assert connection.getState() == ConnectionState.OPEN;
					connection.setState(ConnectionState.CLOSING);
				} else if (currentAction == OpAction.CONNECT) {
					assert connection.getState() == ConnectionState.NEGOTIATING;
					connection.setState(ConnectionState.CLOSING);
				}
				switchAction(OpAction.TEARDOWN);
				connection.setNodeInfo(null);
				ConnectionMessage closeConfirm = new ConnectionMessage(
						connection, ConnectionMessageType.CLOSE_CONFIRM);
				reply(replyTo, closeConfirm);
				connection.setState(ConnectionState.CLOSED);
				break;

			case CLOSE_CONFIRM:
				assert currentAction == OpAction.TEARDOWN
						|| currentAction == OpAction.INACTIVE;
				assert connection.getState() == ConnectionState.CLOSING
						|| connection.getState() == ConnectionState.CLOSED;
				connection.setState(ConnectionState.CLOSED);
				break;

			default:
				throw new AssertionError("Unhandled Protocol Type: "
						+ cMsg.getMessageType().toString());
			}
		} else {
			throw new AssertionError("Unsupported Message Type!");
		}
	}

	/**
	 * Called by the ConnectionManager if a timeout occured.
	 */
	public void handleTimeout() {
		if (!getComponent().isPresent()) {
			stop();
		}

		connection.setNodeInfo(null);
		
		if (isStopped()) {
			return;
		}

		if (currentAction == OpAction.ACTIVE) {
			doCloseConnection();
		} else if (currentAction == OpAction.CONNECT) {
			doCancelOpenConnection();
		}
	}

	/**
	 * Called for messages that may not have arrived at the receiver, as we did
	 * not get a reply in time.
	 * 
	 * @param request
	 */
	public void handleTimeout(TransitMessage request) {

		if (!getComponent().isPresent()) {
			stop();
		}

		if (isStopped()) {
			return;
		}

		connection.setNodeInfo(null);
		
		if (request instanceof ConnectionMessage) {
			ConnectionMessage cMsg = (ConnectionMessage) request;
			switch (cMsg.getMessageType()) {
			case CLOSE_CONFIRM:
				assert connection.getState() == ConnectionState.CLOSING;
				connection.setState(ConnectionState.CLOSED);
				break;

			case CLOSE:
				assert connection.getState() == ConnectionState.CLOSING
						|| connection.getState() == ConnectionState.CLOSED;
				connection.setState(ConnectionState.CLOSED);
				break;
			}
			
		} else {
			throw new AssertionError("Unhandled Timeout!");
		}
	}

	/**
	 * Sends the given message to the endpoint of this connection.
	 * 
	 * @param message
	 */
	private void send(TransitMessage message) {
		assert !isStopped();
		if (DEBUG) {
			System.out.println(Time.getFormattedTime(Time
					.getCurrentTime())
					+ "CCO: SENDING    "
					+ message.toString() + " via " + connection.toString());
		}

		int commID = msgHandler.sendAndWait(message, endpoint, this);
		waitingForReply.put(commID, message);
	}

	/**
	 * Sends a reply
	 * 
	 * @param replyTo
	 * @param message
	 */
	private void reply(TransitReplyHandler replyTo, TransitMessage message) {
		assert !isStopped();
		if (DEBUG) {
			System.out.println(Time.getFormattedTime(Time
					.getCurrentTime())
					+ "CCO: REPLYING   "
					+ message.toString() + " via " + connection.toString());
		}
		replyTo.reply(message);
	}

	@Override
	public TransitConnection getResult() {
		return connection;
	}

	public TransitConnection getConnection() {
		return connection;
	}

	@Override
	public boolean isStopped() {
		return super.isStopped();
	}

	@Override
	public final void messageTimeoutOccured(int commId) {
		TransitMessage request = waitingForReply.remove(commId);
		assert request != null;
		handleTimeout(request);
	}

	@Override
	public final void receive(Message msg, TransInfo senderInfo, int commId) {
		assert msg instanceof TransitMessage;
		TransitMessage request = waitingForReply.remove(commId);
		if (request == null) {
			/*
			 * Timeout before the message could arrive - maybe handle this more
			 * elegantly?
			 */
			return;
		}
		receive((TransitMessage) msg, msgHandler.getReplyHandler(endpoint));
	}

}
