/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.plugins;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitPluginMessage;

// TODO: Auto-generated Javadoc
/**
 * The Class AbstractTransitPlugin.
 */
public abstract class AbstractTransitPlugin {

	/** The node. */
	private final TransitNode node;

	/** The plugin enabled. */
	private boolean pluginEnabled = false;

	/**
	 * Initialize.
	 */
	protected abstract void initialize();

	/**
	 * Received message.
	 * 
	 * @param msg
	 *            the msg
	 */
	public abstract void receivedMessage(TransitPluginMessage msg);

	/**
	 * Instantiates a new abstract transit plugin.
	 * 
	 * @param node
	 *            the node
	 */
	public AbstractTransitPlugin(TransitNode node) {
		this.node = node;
	}

	/**
	 * Checks if is plugin enabled.
	 * 
	 * @return true, if is plugin enabled
	 */
	public boolean isPluginEnabled() {
		return pluginEnabled;
	}

	/**
	 * Sets the plugin enabled.
	 * 
	 * @param pluginEnabled
	 *            the new plugin enabled
	 */
	public void setPluginEnabled(boolean pluginEnabled) {
		this.pluginEnabled = pluginEnabled;
	}

	/**
	 * Gets the node.
	 * 
	 * @return the node
	 */
	public TransitNode getNode() {
		return node;
	}

	public static List<AbstractTransitPlugin> loadAllPlugins(TransitNode node) {

		final LinkedList<AbstractTransitPlugin> result = new LinkedList<AbstractTransitPlugin>();

		/*
		 * ADD YOUR PLUGIN HERE.
		 */
		// result.add(new HomeSwarmMobilePlugin(node));
		// result.add(new PullTokenSupportPlugin(node));
		// result.add(new PullTokenToHomeSwarmPlugin(node));
		/*
		 * ADD YOUR PLUGIN HERE.
		 */

		Iterator<AbstractTransitPlugin> iterator = result.iterator();
		while (iterator.hasNext()) {
			AbstractTransitPlugin next = iterator.next();
			next.initialize();
			if (!next.isPluginEnabled()) {
				iterator.remove();
			}
		}

		return result;

	}

}
