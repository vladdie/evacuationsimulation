package de.tudarmstadt.maki.simonstrator.overlay.streaming.models;

import java.util.BitSet;
import java.util.HashMap;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.VideoModel;

public class CubeVideoModel implements VideoModel {

	private static CubeVideoModel instance;

	// layer levels
	protected final byte spatialLevels = StreamingConfiguration.getSpatialLevels();
	protected final byte temporalLevels = StreamingConfiguration.getTemporalLevels();
	protected final byte snrLevels = StreamingConfiguration.getQualityLevels();

	private final HashMap<Integer, Integer> blockImportance;

	/**
	 * number of video chunks(GOPs)
	 */
	private final int chunks;

	/**
	 * number of blocks per chunk
	 */
	private final int BLOCKS_PER_CHUNKS = spatialLevels * temporalLevels * snrLevels;

	/**
	 * chunk size in bytes
	 */
	private long chunkSize;

	/**
	 * size of a block in bytes
	 */
	private final int[] blockSize = new int[BLOCKS_PER_CHUNKS];

	/**
	 * byte-rate of the video (KByte/s)
	 */
	protected double videoByteRate;

	/**
	 * mapping layers into a chunk
	 */
	protected final BitSet[][][] layerMask = new BitSet[spatialLevels][temporalLevels][snrLevels];

	/**
	 * Byte rate of layers
	 */
	protected double[][][] layerByteRate = new double[spatialLevels][temporalLevels][snrLevels];// KBps

	/**
	 * Complexity of layers
	 */
	protected int[][][] layerComplexity = new int[spatialLevels][temporalLevels][snrLevels];

	/**
	 * Mapping of each block in the chunk to the belonging layer
	 */
	private final HashMap<Integer, byte[]> blockToLayerMapping = new HashMap<Integer, byte[]>();

	public CubeVideoModel() {
		chunks = StreamingConfiguration.getTotalNumberOfChunks();

		blockImportance = new HashMap<Integer, Integer>(); // Added by IP

		for (int i = 0; i < spatialLevels; i++) {
			for (int j = 0; j < temporalLevels; j++) {
				for (int k = 0; k < snrLevels; k++) {
					int layerIndex = i * temporalLevels * snrLevels + j * snrLevels + k;
					/* generate layer mask */
					assert layerMask[i][j][k] == null;
					layerMask[i][j][k] = new BitSet(BLOCKS_PER_CHUNKS);
					BitSet mask = layerMask[i][j][k];
					// set incremental part of this layer
					mask.set(layerIndex);
					// accumulate lower layers
					if (i > 0 && layerMask[i - 1][j][k] != null) {
						mask.or(layerMask[i - 1][j][k]);
					}
					if (j > 0 && layerMask[i][j - 1][k] != null) {
						mask.or(layerMask[i][j - 1][k]);
					}
					if (k > 0 && layerMask[i][j][k - 1] != null) {
						mask.or(layerMask[i][j][k - 1]);
					}

					/*
					 * Calculation of block importance, added by IP
					 */
					int index = 0;
					BitSet blocks = (BitSet) layerMask[i][j][k].clone();
					int value;
					while ((value = blocks.nextSetBit(index)) != -1) {
						if (blockImportance.get(value) == null) {
							blockImportance.put(value, 1);
						} else
							blockImportance.put(value, blockImportance.get(value) + 1);
						index = value + 1;
					}

					/*
					 * generate map between block offset and layer level
					 */
					byte[] layer = { (byte) i, (byte) j, (byte) k };
					blockToLayerMapping.put(layerIndex, layer);

					/*
					 * generate requirements for each layer
					 */
					layerComplexity[i][j][k] = StreamingConfiguration.Cd * (i + 1) + StreamingConfiguration.Ct
					        * (j + 1) + StreamingConfiguration.Cq * (k + 1);
				}
			}
		}

		initSizesAndRates();
	}

	/* getters */

	@Override
	public int getNumOfBlocksPerChunk() {
		return BLOCKS_PER_CHUNKS;
	}

	@Override
	public int getNumOfChunks() {
		return chunks;
	}

	@Override
	public int getBlockSize(int blockNum) {
		int blockOffset = blockNum % BLOCKS_PER_CHUNKS;
		return blockSize[blockOffset]; // return the block size of a given layer
	}

	@Override
	public byte[] getBlockLayer(int blockNum) {
		return blockToLayerMapping.get(blockNum % BLOCKS_PER_CHUNKS);
	}

	@Override
	public BitSet getLayerMask(byte[] layer) {
		int d = layer[0], t = layer[1], q = layer[2];
		if (layerMask.length <= d || layerMask[d].length <= t || layerMask[d][t].length <= q) {
			System.err.println("CUBEVIDEOMODEL: LayerMask problem!");
		}
		return layerMask[d][t][q];
	}

	@Override
	public Byte getLayerIndex(byte[] layer) {
		return (byte) (layer[0] * temporalLevels * snrLevels + layer[1] * snrLevels + layer[2]);
	}

	@Override
	public byte getSnrLevels() {
		return snrLevels;
	}

	@Override
	public byte getSpatialLevels() {
		return spatialLevels;
	}

	@Override
	public byte getTemporalLevels() {
		return temporalLevels;
	}

	@Override
	public double getLayerByteRate(int d, int t, int q) {
		return layerByteRate[d][t][q];
	}

	@Override
	public int getLayerComplexity(int d, int t, int q) {
		return layerComplexity[d][t][q];
	}

	@Override
	public long getVideoFileSize() {
		return chunks * chunkSize;
	}

	private void initSizesAndRates() {
		videoByteRate = StreamingConfiguration.getVideoByteRate();
		// the size of each chunk (byte)
		chunkSize = Math.round(videoByteRate / StreamingConfiguration.CHUNKS_PER_SECOND * 1000);

		// calculate the size of each block
		float sumWeight = 0;
		for (int i = 0; i < spatialLevels; i++) {
			for (int j = 0; j < temporalLevels; j++) {
				for (int k = 0; k < snrLevels; k++) {
					sumWeight += StreamingConfiguration.getLayerWeights()[i][j][k];
				}
			}
		}

		// TODO [JR]: Debug output
		System.out.println("\nCube Video Model - Information on used SVC Video File\nLength: "
		        + StreamingConfiguration.getVideoLength() + "seconds " + "\nNumber of chunks: "
		        + StreamingConfiguration.getTotalNumberOfChunks() + "\nBlocks per chunk: " + BLOCKS_PER_CHUNKS);

		for (int i = 0; i < spatialLevels; i++) {
			for (int j = 0; j < temporalLevels; j++) {
				for (int k = 0; k < snrLevels; k++) {
					int layerIndex = i * temporalLevels * snrLevels + j * snrLevels + k;
					blockSize[layerIndex] = Math.round(StreamingConfiguration.getLayerWeights()[i][j][k] / sumWeight
					        * chunkSize);

					/*
					 * Generate byte-rate requirements for each layer(KBytes/s)
					 */
					long layerSize = 0;
					for (int n = layerMask[i][j][k].nextSetBit(0); n >= 0; n = layerMask[i][j][k].nextSetBit(n + 1)) {
						layerSize += blockSize[n];
					}
					layerByteRate[i][j][k] = layerSize / 1000 * StreamingConfiguration.CHUNKS_PER_SECOND;

					// TODO [JR]: Debug output
					System.out.println("[" + layerIndex + "]: (" + i + "," + j + "," + k + ") - Bit rate = "
					        + layerByteRate[i][j][k] + " kBps (" + layerByteRate[i][j][k] * 8 + " kbps), Block size = "
					        + blockSize[layerIndex] + ", complexity: " + layerComplexity[i][j][k]);
				}
			}
		}
		// TODO [JR]: Debug output
		System.out.println();
	}

	public HashMap<Integer, Integer> getBlockImportance() {
		return blockImportance;
	}

	public static CubeVideoModel getInstance() {
		if (instance == null)
			instance = new CubeVideoModel();
		return instance;
	}

}
