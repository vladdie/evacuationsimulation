/*
 * Copyright (c) 2005-2013 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;

public class TransitIncentiveDummy extends AbstractTransitIncentiveManager implements TransitIncentiveRequestAdvisor {

	/**
	 * Instantiates a new transit incentive dummy.
	 * 
	 * @param transitNode
	 *            the transit node
	 */
	public TransitIncentiveDummy(TransitNode transitNode) {
		super(transitNode);
	}
	
	/* (non-Javadoc)
	 * @see de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.TransitIncentiveManager#sortConnectionsByIncentive(java.util.List)
	 */
	@Override
	public List<TransitConnection> sortConnectionsByIncentiveForRequesting(List<TransitConnection> connections) {
		Collections.sort(connections, COMP_LOAD);
		return connections;
	}
	
	/* (non-Javadoc)
	 * @see de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.TransitIncentiveManager#getIncentiveRequestAdvisor(java.util.List)
	 */
	@Override
	public TransitIncentiveRequestAdvisor getIncentiveRequestAdvisor(List<TransitConnection> connections) {
		return this;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.TransitIncentiveRequestAdvisor#getRecommendedRequestProbability(de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection)
	 */
	@Override
	public double getRecommendedRequestProbability(TransitConnection connection) {
		return 1;
	}

	/* (non-Javadoc)
	 * @see de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.incentive.TransitIncentiveManager#getServingAdvise(de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection)
	 */
	@Override
	public TransitServingAdvise getServingAdvise(TransitConnection connection) {
		return new TransitServingAdvise(TransitServingAdvise.ServingAdvise.A_SERVE_FULL);
	}

	/** The Constant COMP_LOAD. */
	private static final Comparator<TransitConnection> COMP_LOAD = new Comparator<TransitConnection>() {
		
		/**
		 * Compare two Connections by Bandwidth load.
		 *
		 * @param o1 the o1
		 * @param o2 the o2
		 * @return the int
		 */
		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			if (o1.getEndpoint().getLoad() > o2.getEndpoint().getLoad())
				return 1;

			if (o1.getEndpoint().getLoad() < o2.getEndpoint().getLoad())
				return -1;

			return 0;
		}
	};

}
