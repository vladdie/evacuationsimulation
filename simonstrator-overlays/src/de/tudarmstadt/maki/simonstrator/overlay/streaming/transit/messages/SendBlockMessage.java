/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact.ContactType;

/**
 * Sends a Block (part of a video chunk), either as part of a Flow or as a
 * result of a Request.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 12.06.2012
 */
public class SendBlockMessage extends AbstractTransitMessage implements
		TransitBlockMessage {

	private static final long serialVersionUID = 1L;

	private final int blockNumber;

	private final int blockSize;

	private final boolean isLiveBlock;

	private final int hopsFromSource;
	
	private final byte[] rawVideoData;
	
	/**
	 * This is the constructor for a new Block Message, either for a requested
	 * block or for a new block at the source.
	 * 
	 * @param sender
	 * @param blockNumber
	 * @param blockSize
	 * @param isLive
	 * @param rawVideoData
	 *            byte-array of real data or just null (in simulations)
	 */
	public SendBlockMessage(TransitContact sender, int blockNumber,
			int blockSize, boolean isLive, byte[] rawVideoData) {
		super(sender);
		assert (isLive && sender.getType() == ContactType.SOURCE) || !isLive;
		this.blockNumber = blockNumber;
		this.blockSize = blockSize;
		this.isLiveBlock = isLive;
		if (isLive) {
			this.hopsFromSource = 0;
		} else {
			this.hopsFromSource = -1;
		}
		this.rawVideoData = rawVideoData;
	}

	/**
	 * This is the constructor for Live-Blocks, i.e. Blocks that should be
	 * forwarded.
	 * 
	 * @param sender
	 * @param blockNumber
	 * @param blockSize
	 * @param hopsFromSource
	 */
	public SendBlockMessage(TransitContact sender, SendBlockMessage toForward) {
		super(sender);
		assert toForward.isLiveBlock;
		this.blockNumber = toForward.blockNumber;
		this.blockSize = toForward.blockSize;
		this.isLiveBlock = true;
		this.hopsFromSource = toForward.hopsFromSource + 1;
		this.rawVideoData = toForward.rawVideoData;
	}

	/**
	 * Constructor for the de-serialization of a message, allowing access to
	 * otherwise hidden fields. <b>DO NOT USE within the overlay!</b>
	 */
	public SendBlockMessage(TransitContact sender, int blockNumber,
			int blockSize, boolean isLive, byte[] rawVideoData,
			int hopsFromSource) {
		super(sender);
		assert (isLive && sender.getType() == ContactType.SOURCE) || !isLive;
		this.blockNumber = blockNumber;
		this.blockSize = blockSize;
		this.isLiveBlock = isLive;
		this.rawVideoData = rawVideoData;
		if (isLive) {
			this.hopsFromSource = hopsFromSource;
		} else {
			this.hopsFromSource = -1;
		}
	}


	@Override
	public int getBlockNumber() {
		return blockNumber;
	}

	@Override
	public boolean isLiveBlock() {
		assert (isLiveBlock && hopsFromSource >= 0) || !isLiveBlock;
		return isLiveBlock;
	}

	@Override
	public int getHopsFromSource() {
		assert isLiveBlock : "Only use this for live-blocks!";
		return hopsFromSource;
	}

	@Override
	public long getSize() {
		return super.getSize() + blockSize + 5; // 5 bytes blockNum and flags
	}

	@Override
	public int getBlockSize() {
		return blockSize;
	}

	@Override
	public String toString() {
		return "BlockMsg from " + getSenderContact().toString()
				+ " containing " + blockNumber
				+ (isLiveBlock ? " LIVE" : " REQ")
				+ (rawVideoData != null ? rawVideoData[0] : "");
	}

	@Override
	public byte[] getRealData() {
		return rawVideoData;
	}

}
