package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.monitor;

import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponent;
import de.tudarmstadt.maki.simonstrator.api.component.HostComponentFactory;
import de.tudarmstadt.maki.simonstrator.api.component.network.NetInterface;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayContact;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.Notification;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.PubSubComponent;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Attribute;
import de.tudarmstadt.maki.simonstrator.api.component.pubsub.attribute.Topic;
import de.tudarmstadt.maki.simonstrator.api.operation.PeriodicOperation;
import de.tudarmstadt.maki.simonstrator.overlay.AbstractOverlayNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;

/**
 * A Monitoring-Component for Transit, that uses the pub/sub service to
 * disseminate information on the node's current state. To use this information,
 * just subscribe to the relevant channel.
 * 
 * @author Bjoern Richerzhagen
 * 
 */
public class TransitMonitor extends AbstractOverlayNode {

	public static final String TOPIC = "transit/monitor";

	public static final String ATTR_NODENAME = "name";

	public static final String ATTR_TRANSIT_ID = "transitId";

	public static final String ATTR_CONNECTIONS_IN = "connectionsIn";

	public static final String ATTR_CONNECTIONS_OUT = "connectionsOut";

	public static String NODE_NAME = "DEFAULT_NAME";

	public static long REPORTING_INTERVAL = 2 * Time.SECOND;

	private ReportingOperation reportingOp;

	public TransitMonitor(Host host) {
		super(host);
	}

	@Override
	public void initialize() {
		super.initialize();
		// Start the periodic reporting operation after 5s
		reportingOp = new ReportingOperation(this);
		reportingOp.startWithDelay(5 * Time.SECOND);
	}

	@Override
	public OverlayContact getLocalOverlayContact() {
		return null;
	}

	@Override
	public void wentOnline(Host host, NetInterface netInterface) {
		// nothing
	}

	@Override
	public void wentOffline(Host host, NetInterface netInterface) {
		// nothing
	}

	/**
	 * This operation is executed periodically and publishes the current state
	 * on various channels.
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	private class ReportingOperation extends
			PeriodicOperation<TransitMonitor, Void> {

		private final PubSubComponent pubsub;

		private final TransitNode transit;

		private final Topic topic;

		private final Attribute<String> attrNodeName;

		private final Attribute<String> attrTransitId;

		private final Attribute<String> attrConnectionsIn;

		private final Attribute<String> attrConnectionsOut;

		private final long CON_ACTIVE_TIMEOUT = 5 * Time.SECOND;

		public ReportingOperation(TransitMonitor component) {
			super(component, null, REPORTING_INTERVAL);
			// Find and bind the pub/sub
			try {
				pubsub = getHost().getComponent(PubSubComponent.class);
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError("Pub/Sub not found.");
			}
			// Find and bind transit
			try {
				transit = getHost().getComponent(TransitNode.class);
			} catch (ComponentNotAvailableException e) {
				throw new AssertionError("Transit not found.");
			}
			// Create relevant topic
			this.topic = pubsub.createTopic(TOPIC);
			// Create attributes
			this.attrNodeName = pubsub.createAttribute(String.class,
					ATTR_NODENAME, NODE_NAME);
			this.attrTransitId = pubsub.createAttribute(String.class,
					ATTR_TRANSIT_ID, transit.getHost().getId().toString());
			this.attrConnectionsIn = pubsub.createAttribute(String.class,
					ATTR_CONNECTIONS_IN, "");
			this.attrConnectionsOut = pubsub.createAttribute(String.class,
					ATTR_CONNECTIONS_OUT, "");
		}

		@Override
		protected void executeOnce() {
			/*
			 * Get the currently used connections
			 */
			if (!transit.isPresent() || transit.getNeighborhood() == null
					|| transit.getNeighborhood().getConnectionManager() == null) {
				operationFinished(false);
				return;
			}
			List<TransitConnection> incoming = transit
					.getNeighborhood()
					.getConnectionManager()
					.getConnections(ConnectionDirection.IN,
							ConnectionState.OPEN, null, null);
			StringBuilder incomingNodeIDs = new StringBuilder();
			boolean first = true;
			for (TransitConnection connection : incoming) {
				if (connection.getLastBlockTransferredTimestamp()
						+ CON_ACTIVE_TIMEOUT > Time.getCurrentTime()) {
					if (first) {
						first = false;
					} else {
						incomingNodeIDs.append(';');
					}
					incomingNodeIDs.append(connection.getEndpoint()
.getNodeID()
							.toString());
				}
			}

			List<TransitConnection> outgoing = transit
					.getNeighborhood()
					.getConnectionManager()
					.getConnections(ConnectionDirection.OUT,
							ConnectionState.OPEN, null, null);
			StringBuilder outgoingNodeIDs = new StringBuilder();
			first = true;
			for (TransitConnection connection : outgoing) {
				if (connection.getLastBlockTransferredTimestamp()
						+ CON_ACTIVE_TIMEOUT > Time.getCurrentTime()) {
					if (first) {
						first = false;
					} else {
						outgoingNodeIDs.append(';');
					}
					outgoingNodeIDs.append(connection.getEndpoint().getNodeID()
							.toString());
				}
			}

			/*
			 * Update the attributes
			 */
			Attribute<String> attrConIn = attrConnectionsIn
					.create(incomingNodeIDs.toString());
			Attribute<String> attrConOut = attrConnectionsOut
					.create(outgoingNodeIDs.toString());
			/*
			 * Build the notification
			 */
			List<Attribute<?>> attributes = new LinkedList<Attribute<?>>();
			attributes.add(attrNodeName);
			attributes.add(attrTransitId);
			attributes.add(attrConIn);
			attributes.add(attrConOut);
			Notification n = pubsub.createNotification(topic, attributes, null);
			/*
			 * And send it.
			 */
			pubsub.publish(n);
		}

		@Override
		public Void getResult() {
			return null;
		}

	}

	/**
	 * The (very complex) factory for this node
	 * 
	 * @author Bjoern Richerzhagen
	 * 
	 */
	public static class TransitMonitorFactory implements HostComponentFactory {

		@Override
		public HostComponent createComponent(Host host) {
			return new TransitMonitor(host);
		}

	}

}
