/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.operations.ConnectionControlOperation;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.plugins.pulltoken.PullToken;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitFlowSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitFlowScheduleListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitRequestSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitScheduler;
import de.tudarmstadt.maki.simonstrator.util.BandwidthEstimator;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSum;

/**
 * Out of the set of possible neighbors in the {@link TransitContactManager} we
 * select a few nodes and connect to them. This process is managed by the
 * {@link ConnectionControlOperation}. A connection is unidirectional in terms
 * of video blocks but bidirectional for signaling.
 * 
 * Both endpoints maintain such an object for the given connection - of course,
 * target and direction will be switched. Connections are always formed by the
 * node that is to receive the video payload.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 09.06.2012
 */
public class TransitConnection {

	/**
	 * Direction of the connection in terms of video payload transfer
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 10.06.2012
	 */
	public static enum ConnectionDirection {
		/**
		 * Incoming data
		 */
		IN,
		/**
		 * Outgoing data
		 */
		OUT
	}

	/**
	 * Current state of the connection
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 10.06.2012
	 */
	public static enum ConnectionState {
		/**
		 * Connection is in the process of being opened
		 */
		NEGOTIATING,
		/**
		 * Connection is open
		 */
		OPEN,
		/**
		 * Connection is closing
		 */
		CLOSING,
		/**
		 * Connection is closed
		 */
		CLOSED
	}

	/**
	 * State of the Live-Schedule that is set for this connection.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.06.2012
	 */
	public static enum ScheduleState {
		/**
		 * Initial state, no schedule has been assigned to this connection yet.
		 * If a schedule is no longer valid, i.e. its validity time expired, the
		 * connection will return to this state. If the schedule could not be
		 * fulfilled, it will instead go into CANCELED
		 */
		INACTIVE,
		/**
		 * The connection is currently used to negotiate a schedule.
		 */
		NEGOTIATING,
		/**
		 * The connection is currently used for a Live-Schedule
		 */
		ACTIVE,
		/**
		 * The schedule was denied. This basically renders this connection
		 * useless, which might be a trigger to disconnect it using the
		 * NeighborhoodManager.
		 */
		CANCELED
	}

	/**
	 * State of the connection when used for requests.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 30.07.2012
	 */
	public static enum RequestState {
		/**
		 * The connection is not used for requests
		 */
		INACTIVE,
		/**
		 * The connection is currently used for a request
		 */
		ACTIVE,
		/**
		 * The connection was not able to fulfill one (or many) request and is
		 * no longer used for them at all. This signals the ConnectionManager
		 * that this connection can be closed.
		 */
		CANCELED,
	}

	private final long createdTimestamp = Time.getCurrentTime();

	public final TransitConnectionStatistics connectionStatistics;

	private long lastStateChangeTimestamp = 0;

	private long lastScheduleStateChangeTimestamp = 0;

	private long lastRequestStateChangeTimestamp = 0;

	private long lastDirectionChangeTimestamp = 0;

	private long lastBlockTransferredTimestamp = 0;

	private final TransitContact endpoint;

	private final TransitContact owner;

	private ConnectionDirection direction;

	private ConnectionState state;

	private ScheduleState scheduleState;

	private RequestState requestState;

	private TransitFlowSchedule currentSchedule;

	private TransitRequestSchedule currentRequest;

	private TransitRequestSchedule currentBufferMap;

	private final TransitConnectionListener connectionListener;

	private final List<TransitFlowScheduleListener> scheduleListeners = new CopyOnWriteArrayList<TransitFlowScheduleListener>();

	private int mostRecentBlock = 0;

	private long currentRTTEstimation = 400 * Time.MILLISECOND;

	private long lastUpdateOfRTT = Time.getCurrentTime();

	/**
	 * How long we consider an estimation to be valid. TODO: make this adaptive?
	 */
	private final long validityTimeOfRTT = Time.MINUTE;

	/** The request pull tokens. */
	private boolean requestPullTokens = false;

	private PullToken pullToken = null;

	/**
	 * Create a new connection
	 * 
	 * @param owner
	 * @param endpoint
	 * @param direction
	 * @param listener
	 *            this will be the dispatcher for the
	 *            {@link TransitConnectionListener}s, in most cases the
	 *            Neighborhood manager
	 */
	public TransitConnection(TransitContact owner, TransitContact endpoint, ConnectionDirection direction,
	        TransitConnectionListener connectionListener, long SLIDING_WINDOW_SIZE) {
		assert !owner.equals(endpoint);
		this.owner = owner;
		this.endpoint = endpoint;
		this.direction = direction;
		this.state = ConnectionState.NEGOTIATING;
		this.scheduleState = ScheduleState.INACTIVE;
		this.requestState = RequestState.INACTIVE;
		this.connectionListener = connectionListener;
		this.lastStateChangeTimestamp = Time.getCurrentTime();
		this.lastDirectionChangeTimestamp = Time.getCurrentTime();
		this.lastRequestStateChangeTimestamp = Time.getCurrentTime();
		this.lastBlockTransferredTimestamp = Time.getCurrentTime();
		this.connectionStatistics = new TransitConnectionStatistics(SLIDING_WINDOW_SIZE);
		this.addScheduleListener(connectionStatistics);
	}

	/**
	 * Adds a {@link TransitFlowScheduleListener} to this connection. It can be
	 * removed by returning false in the scheduleStateChanged callback.
	 * 
	 * @param listener
	 *            the listener to add
	 */
	public void addScheduleListener(TransitFlowScheduleListener listener) {
		scheduleListeners.add(listener);
	}

	/**
	 * Gets the creation time.
	 * 
	 * @return the creation time
	 */
	public long getCreationTime() {
		return createdTimestamp;
	}

	/**
	 * Sets the request pull tokens.
	 * 
	 * @param requestPullTokens
	 *            the new request pull tokens
	 */
	public void setRequestPullTokens(boolean requestPullTokens) {
		this.requestPullTokens = requestPullTokens;
	}

	/**
	 * Wants request pull tokens.
	 * 
	 * @return true, if successful
	 */
	public boolean wantsRequestPullTokens() {
		return requestPullTokens;
	}

	/**
	 * Sets the pull token.
	 * 
	 * @param pullToken
	 *            the new pull token
	 */
	public void setPullToken(PullToken pullToken) {
		this.pullToken = pullToken;
	}

	/**
	 * Gets the pull token.
	 * 
	 * @return the pull token
	 */
	public PullToken getPullToken() {
		return this.pullToken;
	}

	/**
	 * Current {@link ConnectionState} of the connection
	 * 
	 * @return
	 */
	public ConnectionState getState() {
		return state;
	}

	/**
	 * Set the state of the connection
	 * 
	 * @param state
	 */
	public void setState(ConnectionState state) {
		ConnectionState oldState = this.state;
		this.state = state;
		if (oldState != this.state) {
			connectionStatistics.onStateChanged(oldState, state);
			lastStateChangeTimestamp = Time.getCurrentTime();
			connectionListener.onConnectionStateChanged(this, oldState, this.state);
		}
	}

	/**
	 * State of the Schedule associated with this connection
	 * 
	 * @return
	 */
	public ScheduleState getScheduleState() {
		return scheduleState;
	}

	/**
	 * State of the request currently being executed on this connection
	 * 
	 * @return
	 */
	public RequestState getRequestState() {
		return requestState;
	}

	/**
	 * Set the state of the schedule associated with the connection
	 * 
	 * @param scheduleState
	 */
	public void setScheduleState(ScheduleState scheduleState) {
		ScheduleState oldState = this.scheduleState;
		if (scheduleState != this.scheduleState) {
			lastScheduleStateChangeTimestamp = Time.getCurrentTime();
			this.scheduleState = scheduleState;
			for (TransitFlowScheduleListener listener : scheduleListeners) {
				if (!listener.onScheduleStateChanged(this, oldState, scheduleState)) {
					scheduleListeners.remove(listener);
				}
			}
		}
	}

	/**
	 * Set the state of requests currently being associated with the connection
	 * 
	 * @param requestState
	 */
	public void setRequestState(RequestState requestState) {
		if (requestState != this.requestState) {
			connectionStatistics.onRequestStateChanged(requestState, this.requestState);
			lastRequestStateChangeTimestamp = Time.getCurrentTime();
		}
		this.requestState = requestState;
	}

	/**
	 * This is the timestamp of the last time the state of this connection
	 * changes.
	 * 
	 * @return
	 */
	public long getLastStateChangeTimestamp() {
		return lastStateChangeTimestamp;
	}

	/**
	 * Timestamp of the last schedule state change
	 * 
	 * @return
	 */
	public long getLastScheduleStateChangeTimestamp() {
		return lastScheduleStateChangeTimestamp;
	}

	/**
	 * Timestamp of the last request state change
	 * 
	 * @return
	 */
	public long getLastRequestStateChangeTimestamp() {
		return lastRequestStateChangeTimestamp;
	}

	/**
	 * Received or sent a block of the video file via this connection (depends
	 * on the connection direction)
	 * 
	 * @param blocknum
	 * @param size
	 * @param isLive
	 */
	public void blockTransferred(int blocknum, long size, boolean isLive) {
		connectionStatistics.onBlockTransferred(blocknum, size, isLive);
		mostRecentBlock = Math.max(blocknum, mostRecentBlock);
		lastBlockTransferredTimestamp = Time.getCurrentTime();
	}

	/**
	 * Timestamp of the last block transfer via this connection.
	 * 
	 * @return
	 */
	public long getLastBlockTransferredTimestamp() {
		return lastBlockTransferredTimestamp;
	}

	/**
	 * The most recent block that has been transferred via this connection
	 * (numerically highest). This is used to evaluate the block-availability of
	 * the previous chunk in the Scheduler.
	 * 
	 * @return
	 */
	public int getMostRecentBlock() {
		return mostRecentBlock;
	}

	/**
	 * Set the {@link TransitFlowSchedule} currently used on this connection
	 * 
	 * @param schedule
	 */
	public void setSchedule(TransitFlowSchedule schedule) {
		TransitFlowSchedule oldSchedule = this.currentSchedule;
		this.currentSchedule = schedule;
		if (!schedule.equals(oldSchedule)) {
			for (TransitFlowScheduleListener listener : scheduleListeners) {
				if (!listener.onScheduleChanged(this, oldSchedule, schedule)) {
					scheduleListeners.remove(listener);
				}
			}
		}
	}

	/**
	 * Set the request that is then requested or fulfilled via this connection
	 * 
	 * @param request
	 */
	public void setRequest(TransitRequestSchedule request) {
		this.currentRequest = request;
	}

	/**
	 * 
	 * @param bufferMap
	 */
	public void setBufferMap(TransitRequestSchedule bufferMap) {
		this.currentBufferMap = bufferMap;
	}

	/**
	 * The other endpoint of the connection (i.e. the contact that we are
	 * connected to)
	 * 
	 * @return
	 */
	public TransitContact getEndpoint() {
		return endpoint;
	}

	/**
	 * Current node
	 * 
	 * @return
	 */
	public TransitContact getOwner() {
		return owner;
	}

	/**
	 * Get the schedule that is currently assigned to this connection.
	 * Assignment is done by the {@link TransitScheduler}.
	 * 
	 * @return
	 */
	public TransitFlowSchedule getSchedule() {
		return this.currentSchedule;
	}

	/**
	 * Get the request that is currently assigned to this connection.
	 * 
	 * @return
	 */
	public TransitRequestSchedule getRequest() {
		return this.currentRequest;
	}

	/**
	 * 
	 * @return
	 */
	public TransitRequestSchedule getBufferMap() {
		return this.currentBufferMap;
	}

	/**
	 * Incoming or outgoing connection.
	 * 
	 * @return
	 */
	public ConnectionDirection getDirection() {
		return direction;
	}

	/**
	 * Gets the connection statistics.
	 * 
	 * @return the connection statistics
	 */
	public TransitConnectionStatistics getConnectionStatistics() {
		return connectionStatistics;
	}

	/**
	 * Incoming or outgoing direction
	 * 
	 * @param direction
	 */
	public void setDirection(ConnectionDirection direction) {

		// UGLY; But needs to be checked.
		// Iterator<OverlayNode> foo =
		// GlobalOracle.getHostForNetID(this.getOwner().getTransInfo().getNetId())
		// .getOverlays();
		// TransitNode first = (TransitNode) foo.next();
		// assert
		// first.getSettings().getParam(TransitParams.CONNECTION_ALLOW_TWO_WAY_DATA_TRANSFER)
		// < 1;

		if (this.direction != direction) {
			connectionStatistics.onDirectionChanged(direction);
			this.direction = direction;
			this.lastDirectionChangeTimestamp = Time.getCurrentTime();
		}
	}

	/**
	 * Timestamp of last direction change
	 * 
	 * @return
	 */
	public long getLastDirectionChangeTimestamp() {
		return lastDirectionChangeTimestamp;
	}

	/**
	 * Called on each new measurement of the RTT for this connection
	 * 
	 * @param newRtt
	 */
	public void updateRTTEstimation(long newRtt) {
		assert newRtt > 0;
		if (Time.getCurrentTime() - lastUpdateOfRTT > validityTimeOfRTT) {
			// begin with new RTT, at least with 500ms
			currentRTTEstimation = Math.max(newRtt, 500 * Time.MILLISECOND);
			lastUpdateOfRTT = Time.getCurrentTime();
		} else {
			// smoothing with alpha = 0.7
			currentRTTEstimation = (long) (currentRTTEstimation * 0.4 + newRtt * 0.6);
			lastUpdateOfRTT = Time.getCurrentTime();
		}
	}

	/**
	 * Returns the RTT that is currently estimated on this connection
	 * 
	 * @return
	 */
	public long getCurrentRTTEstimation() {
		return currentRTTEstimation;
	}

	/**
	 * Timestamp of last RTT update
	 * 
	 * @return
	 */
	public long getLastUpdateOfRTT() {
		return lastUpdateOfRTT;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endpoint == null) ? 0 : endpoint.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TransitConnection other = (TransitConnection) obj;
		if (endpoint == null) {
			if (other.endpoint != null) {
				return false;
			}
		} else if (!endpoint.equals(other.endpoint)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return owner.toString() + (direction == ConnectionDirection.IN ? "   <<--   " : "   -->>   ")
		        + endpoint.toString() + " " + state.toString() + " " + " FLOW: " + scheduleState.toString() + " "
		        + (currentSchedule != null ? currentSchedule.toString() : "") + " REQ: " + requestState.toString()
		        + " " + (currentRequest != null ? currentRequest.toString() : "") + " -- BW current: "
		        + (connectionStatistics.getCurrentBWEstimation(TransitConnectionStatistics.TOTAL) / 1000 * 8)
		        + " kbit/s (F "
		        + (connectionStatistics.getCurrentBWEstimation(TransitConnectionStatistics.FLOW) / 1000 * 8) + " R "
		        + (connectionStatistics.getCurrentBWEstimation(TransitConnectionStatistics.REQUEST) / 1000 * 8)
		        + "), RTT: " + Time.getFormattedTime(getCurrentRTTEstimation());
	}

	/**
	 * This is meant for performance evaluation of a connection, use as
	 * READ-Only!
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 30.07.2012
	 */
	public class TransitConnectionStatistics implements TransitFlowScheduleListener {

		public static final byte TOTAL = 0;

		public static final byte REQUEST = 1;

		public static final byte FLOW = 2;

		public static final byte TRANSFERED_NUM_OF_BLOCK_CHANGED = 0;

		private final static byte BANDWIDTH_ESTIMATOR_WINDOW = 4;

		public int numberOfScheduleChanges = 0;

		public int numberOfSuccessfulScheduleNegotiations = 0;

		public int numberOfFailedScheduleNegotiations = 0;

		public int numberOfSuccessfulRequestNegotiations = 0;

		public int numberOfFailedRequestNegotiations = 0;

		public int numberOfLiveBlocksTransferred = 0;

		public int numberOfRequestBlocksTransferred = 0;

		private long sizeOfLiveBlocksTransferred = 0;

		private long sizeOfRequestBlocksTransferred = 0;

		public int numberOfDirectionChanges = 0;

		public int numberOfSuccessfulConnectionAttempts = 0;

		private final TimeoutSum timeoutSizeOfAllBlocksTransferred;

		private final BandwidthEstimator bwEstimateTotal = new BandwidthEstimator(BANDWIDTH_ESTIMATOR_WINDOW);

		private final BandwidthEstimator bwEstimateRequest = new BandwidthEstimator(BANDWIDTH_ESTIMATOR_WINDOW);

		private final BandwidthEstimator bwEstimateLive = new BandwidthEstimator(BANDWIDTH_ESTIMATOR_WINDOW);

		public TransitConnectionStatistics(long SLIDING_WINDOW_SIZE) {
			timeoutSizeOfAllBlocksTransferred = new TimeoutSum(SLIDING_WINDOW_SIZE);
		}

		@Override
		public boolean onScheduleChanged(TransitConnection connection, TransitFlowSchedule oldSchedule,
		        TransitFlowSchedule newSchedule) {
			numberOfScheduleChanges++;
			return true;
		}

		@Override
		public boolean onScheduleStateChanged(TransitConnection connection, ScheduleState oldState,
		        ScheduleState newState) {
			if (newState == ScheduleState.ACTIVE) {
				numberOfSuccessfulScheduleNegotiations++;
			}
			if (newState == ScheduleState.CANCELED) {
				numberOfFailedScheduleNegotiations++;
			}
			return true;
		}

		protected void onBlockTransferred(int blocknum, long size, boolean isLive) {
			if (isLive) {
				numberOfLiveBlocksTransferred++;
				sizeOfLiveBlocksTransferred += size;
				bwEstimateLive.incomingTransmission(size);
			} else {
				numberOfRequestBlocksTransferred++;
				sizeOfRequestBlocksTransferred += size;
				bwEstimateRequest.incomingTransmission(size);
			}
			timeoutSizeOfAllBlocksTransferred.increase(size);
			bwEstimateTotal.incomingTransmission(size);
		}

		/**
		 * An estimation of the bandwidth sent or received via this connection
		 * over the last couple of seconds.
		 * 
		 * @param category
		 *            one of TOTAL, FLOW, REQUEST
		 * @return
		 */
		public double getCurrentBWEstimation(byte category) {
			switch (category) {
			case TOTAL:
				return bwEstimateTotal.getEstimatedBandwidth().getDownBW();

			case REQUEST:
				return bwEstimateRequest.getEstimatedBandwidth().getDownBW();

			case FLOW:
				return bwEstimateLive.getEstimatedBandwidth().getDownBW();

			default:
				throw new AssertionError();
			}
		}

		/**
		 * @return
		 */
		public long getBytesTransfered() {
			return sizeOfRequestBlocksTransferred + sizeOfLiveBlocksTransferred;
		}

		/**
		 * @return
		 */
		public long getBlocksTransfered() {
			return numberOfRequestBlocksTransferred + numberOfLiveBlocksTransferred;
		}

		/**
		 * Gets the timeout bytes transferred sum.
		 * 
		 * @return the timeout bytes transferred sum
		 */
		public long getRecentlyBytesTransferred() {
			return timeoutSizeOfAllBlocksTransferred.get();
		}

		protected void onStateChanged(ConnectionState oldState, ConnectionState newState) {
			if (newState == ConnectionState.OPEN) {
				numberOfSuccessfulConnectionAttempts++;
			}
		}

		protected void onRequestStateChanged(RequestState oldState, RequestState newState) {
			if (newState == RequestState.ACTIVE) {
				numberOfSuccessfulRequestNegotiations++;
			}
			if (newState == RequestState.CANCELED) {
				numberOfFailedRequestNegotiations++;
			}
		}

		protected void onDirectionChanged(ConnectionDirection newDirection) {
			numberOfDirectionChanges++;
		}

	}

}
