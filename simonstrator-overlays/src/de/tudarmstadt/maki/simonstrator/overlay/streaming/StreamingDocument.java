package de.tudarmstadt.maki.simonstrator.overlay.streaming;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.data.StreamingDataInterface;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.data.StreamingDataInterface.ModelInterface;

/**
 * A stream file, like a video, with SVC structure.
 * 
 * @author Yue Sheng, modified for live streaming by Bjoern Richerzhagen
 */
public class StreamingDocument implements ModelInterface {

	/**
	 * The popularity
	 */
	protected int popularity;

	/**
	 * The concrete structure of this video
	 */
	protected VideoModel model;

	/**
	 * The map of completed blocks of the video
	 */
	protected BitSet completedBlocks;

	/**
	 * The key of the document
	 */
	private UniqueID key;

	/**
	 * This is the chunk that is currently played. In a live-stream, this value
	 * might be set by the tracker to > 0.
	 */
	private int currentPlaybackChunk = 0;

	private int initialPlaybackChunk = 0;

	private int bufferStartChunk = 0;

	private StreamingDataInterface streamingData;

	private byte[] moovAtom = null;

	private boolean isComplete = false;

	/**
	 * This constructor is used for Video on Demand scenarios
	 * 
	 * @param model
	 * @param thePopularity
	 */
	public StreamingDocument(VideoModel model, int thePopularity) {
		this(model, thePopularity, 0);
	}

	/**
	 * This constructor is used for live streaming.
	 * 
	 * @param model
	 * @param thePopularity
	 * @param currentPlaybackChunk
	 *            This is the chunk that is considered the most recent one by
	 *            the tracker
	 */
	public StreamingDocument(VideoModel model, int thePopularity, int currentPlaybackChunk) {

		this.model = model;
		this.popularity = thePopularity;
		this.currentPlaybackChunk = currentPlaybackChunk;
		this.initialPlaybackChunk = currentPlaybackChunk;
		this.bufferStartChunk = Math.max(currentPlaybackChunk - 50, 0);
		// completedBlocks = new BitSet(
		// (model.getNumOfChunks() - currentPlaybackChunk)
		// * model.getNumOfBlocksPerChunk());
		completedBlocks = new BitSet();
		key = StreamingConfiguration.IDSPACE.createRandomID();
	}

	/**
	 * Copy-Constructor, returns a new Document with the updated playback
	 * position.
	 * 
	 * @param toCopy
	 * @param currentPlaybackChunk
	 */
	public StreamingDocument(StreamingDocument toCopy, int currentPlaybackChunk) {
		this(toCopy.getVideoModel(), toCopy.popularity, currentPlaybackChunk);
	}

	/**
	 * return the chunk number that is currently playing (or requested for
	 * playback if we are buffering)
	 * 
	 * @return
	 */
	public int getCurrentPlaybackChunk() {
		return currentPlaybackChunk;
	}

	public void setComplete(boolean isComplete) {
		this.isComplete = isComplete;
	}

	public boolean isComplete() {
		return isComplete;
	}

	/**
	 * Set the playback position. If you need to increment due to normal
	 * playback, just use the increment method instead.
	 * 
	 * @param newPlaybackChunk
	 */
	public void setInitialPlaybackChunk(int newPlaybackChunk) {
		currentPlaybackChunk = newPlaybackChunk;
		initialPlaybackChunk = newPlaybackChunk;
		bufferStartChunk = Math.max(newPlaybackChunk - 50, 0);
	}

	/**
	 * Move the current playback position by one chunk
	 */
	public void incrementCurrentPlaybackChunk() {
		currentPlaybackChunk++;
	}

	public BitSet getLayerMask(byte[] layer) {
		return model.getLayerMask(layer);
	}

	public BitSet getLayerMask(byte d, byte t, byte q) {
		byte[] layer = { d, t, q };
		return getLayerMask(layer);
	}

	/**
	 * @param layer
	 *            a given layer level
	 * @return the position of the layer in a linearized chunk
	 * 
	 *         [JR] Note: This seems to be also the index in the table showing
	 *         the quality level combinations as used in the papers!
	 */
	public Byte getLayerIndex(byte[] layer) {
		return model.getLayerIndex(layer);
	}

	/**
	 * @param chunk
	 *            a chunk of the video
	 * @param layer
	 *            a given SVC layer level
	 * @return true, if the given layer is completely covered by the present
	 *         blocks of the given chunk, false otherwise.
	 */
	public boolean layerIsCoveredByChunkBlocks(int chunk, byte[] layer) {
		if (layer == null)
			return false;
		// return haveLayer(chunk, d, t, q);
		int chunkSize = model.getNumOfBlocksPerChunk();
		if (chunk - bufferStartChunk < 0) {
			return false;
		}
		BitSet chunkMap = completedBlocks.get((chunk - bufferStartChunk) * chunkSize, (chunk - bufferStartChunk + 1)
		        * chunkSize);
		BitSet layerMask = model.getLayerMask(layer);

		chunkMap.and(layerMask);
		boolean complete = chunkMap.equals(layerMask);
		return complete;
	}

	public boolean haveBlock(int blockNum) {
		blockNum -= bufferStartChunk * model.getNumOfBlocksPerChunk();
		if (blockNum < 0) {
			return false;
		}
		return completedBlocks.get(blockNum);
	}

	public void markBlockAsReceived(int blockNum) {
		blockNum -= bufferStartChunk * model.getNumOfBlocksPerChunk();
		if (blockNum < 0) {
			return;
		}
		completedBlocks.set(blockNum);
	}

	/* -------- getters ---------- */

	public VideoModel getVideoModel() {
		return model;
	}

	public int getBlockSize(int blockNum) {
		return model.getBlockSize(blockNum);
	}

	public byte getTemporalLevels() {
		return model.getTemporalLevels();
	}

	public byte getSpatialLevels() {
		return model.getSpatialLevels();
	}

	public byte getSnrLevels() {
		return model.getSnrLevels();
	}

	public BitSet getBlockMap(int fromBlock, int lengthInBlocks) {
		int numblocks = model.getNumOfBlocksPerChunk();
		fromBlock -= bufferStartChunk * numblocks;
		if (fromBlock + lengthInBlocks <= 0) {
			return new BitSet();
		} else if (fromBlock < 0) {
			BitSet map = new BitSet(lengthInBlocks);
			for (int i = completedBlocks.nextSetBit(0); i >= 0; i = completedBlocks.nextSetBit(i + 1)) {
				map.set(i - fromBlock);
				if (i - fromBlock > lengthInBlocks) {
					break;
				}
			}
			return map;
		}
		return completedBlocks.get(fromBlock, fromBlock + lengthInBlocks);
	}

	public BitSet getBlockMap() {
		return this.completedBlocks;
	}

	@Override
	public boolean isBlockRequired(int blocknum) {
		byte[] mask = model.getBlockLayer(blocknum);
		return model.getLayerMask(mask).get(
				blocknum % model.getNumOfBlocksPerChunk());
	}

	@Override
	public int getFirstRequiredBlock() {
		return initialPlaybackChunk * model.getNumOfBlocksPerChunk();
	}

	/**
	 * @return the size of the video (in bytes)
	 */
	public long getSize() {
		return model.getVideoFileSize();
	}

	public void setKey(UniqueID key) {
		this.key = key;
	}

	/**
	 * @return the number of chunks of the stream : one chunk represents a
	 *         playback unit of the stream
	 */
	public int getLength() {
		return model.getNumOfChunks();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof StreamingDocument) {
			UniqueID k = ((StreamingDocument) obj).key;
			if (this.key.equals(k)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return key.hashCode();
	}

	public void setStreamingData(StreamingDataInterface data) {
		this.streamingData = data;
	}

	public StreamingDataInterface getStreamingData() {
		return streamingData;
	}

	public void setMoovAtom(byte[] moovAtom) {
		this.moovAtom = new byte[moovAtom.length];
		System.arraycopy(moovAtom, 0, this.moovAtom, 0, moovAtom.length);
	}

	public byte[] getMoovAtom() {
		return moovAtom;
	}

}
