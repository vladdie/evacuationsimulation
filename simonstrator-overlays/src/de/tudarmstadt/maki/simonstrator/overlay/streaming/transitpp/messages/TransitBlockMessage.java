/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages;

/**
 * This message carries payload in terms of video data.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public interface TransitBlockMessage extends TransitMessage {

	/**
	 * Number of the current block
	 * 
	 * @return
	 */
	public int getBlockNumber();

	/**
	 * Mainly used for analyzing, return the pure-payload block size
	 * 
	 * @return
	 */
	public long getBlockSize();

	/**
	 * Hops this block took until it reached us (only allowed for live-blocks)
	 * 
	 * @return
	 */
	public int getHopsFromSource();

	/**
	 * This is a Block that should be forwarded directly (i.e. a life-block). If
	 * this returns falls, the block has been requested, maybe because the flow
	 * was not able to deliver it.
	 * 
	 * @return
	 */
	public boolean isLiveBlock();

}
