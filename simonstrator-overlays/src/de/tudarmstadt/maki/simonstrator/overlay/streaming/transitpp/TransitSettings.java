/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.operations.ConnectionControlOperation;

/**
 * All parameters, be they adaptive or fixed, are accessed via this component of
 * a {@link TransitNode}. This enables a better overview on all possible tweaks
 * and configurations.
 * 
 * @author Bjoern Richerzhagen, Eduardo Lidanski
 * @version 1.0, 23.09.2012
 */
public class TransitSettings implements Cloneable {

	private final long[] times = new long[TransitTimes.values().length];

	private final int[] params = new int[TransitParams.values().length];

	/**
	 * Generated for each node, as some settings might be adaptive and change
	 * over time
	 */
	public TransitSettings() {
		// Default values
		for (TransitTimes time : TransitTimes.values()) {
			times[time.ordinal()] = time.getDefault();
		}
		for (TransitParams param : TransitParams.values()) {
			params[param.ordinal()] = param.getDefault();
		}
	}

	/**
	 * Copy-constructor
	 * 
	 * @param toClone
	 */
	private TransitSettings(TransitSettings toClone) {
		int i = 0;
		for (long value : toClone.times) {
			times[i] = value;
			i++;
		}
		i = 0;
		for (int value : toClone.params) {
			params[i] = value;
			i++;
		}
	}

	/**
	 * Read the current value of the parameter with the given name
	 * 
	 * @param name
	 * @return
	 */
	public int getParam(TransitParams name) {
		return params[name.ordinal()];
	}

	/**
	 * Sets the given parameter
	 * 
	 * @param name
	 * @param value
	 */
	public void setParam(TransitParams name, int value) {
		params[name.ordinal()] = value;
	}

	/**
	 * Get the current time that is associated to the setting specified by the
	 * enum TransitTimes
	 * 
	 * @param name
	 * @return
	 */
	public long getTime(TransitTimes name) {
		return times[name.ordinal()];
	}

	/**
	 * Update the value of a setting describing a time in Transit
	 * 
	 * @param name
	 * @param value
	 */
	public void setTime(TransitTimes name, long value) {
		times[name.ordinal()] = value;
	}

	/**
	 * Manually overwrite some config parameters by specifying an attribute in
	 * the form overwrite="propname:value;propname:value", might be usefull for
	 * batch processing of minor config variations
	 * 
	 * @param toOverwrite
	 */
	public void setOverwrite(String toOverwrite) {
		String[] props = toOverwrite.split(";");
		for (String prop : props) {
			String[] val = prop.split(":");
			parseProperty(val[0], val[1]);
		}
	}

	private Properties defaultProperties = null;

	private boolean loadedProps = false;

	/**
	 * Allows to specify a settings file for default values other than the ones
	 * specified in the code below. This default file can then be overwritten
	 * with another properties file via setFile.
	 * 
	 * @param filename
	 */
	public void setDefaults(String filename) {
		if (loadedProps) {
			throw new AssertionError(
					"Configure setDefaults before calling setFile!");
		}
		defaultProperties = new Properties();
		BufferedInputStream bin = null;
		try {
			bin = new BufferedInputStream(new FileInputStream(filename));
			defaultProperties.load(bin);
			bin.close();
		} catch (Exception e) {
			throw new AssertionError(e.getMessage());
		}
		parseProperties(defaultProperties);
	}

	/**
	 * Can be used to pass a .properties-file that contains values to overwrite
	 * the default values of this settings-class.
	 * 
	 * @param filename
	 */
	public void setFile(String filename) {
		loadedProps = true;
		Properties props = null;
		if (defaultProperties != null) {
			props = new Properties(defaultProperties);
		} else {
			props = new Properties();
		}
		BufferedInputStream bin = null;
		try {
			bin = new BufferedInputStream(new FileInputStream(filename));
			props.load(bin);
			bin.close();
		} catch (Exception e) {
			throw new AssertionError(e.getMessage());
		}
		parseProperties(props);
	}

	/**
	 * Writes the values set in the properties into the settings for transit
	 * 
	 * @param props
	 */
	private void parseProperties(Properties props) {
		Set<String> names = props.stringPropertyNames();
		for (String name : names) {
			parseProperty(name, props.getProperty(name));
		}
	}

	/**
	 * Set the given property
	 * 
	 * @param name
	 * @param value
	 */
	private void parseProperty(String name, String value) {
		try {
			params[TransitParams.valueOf(name).ordinal()] = Integer
					.parseInt(value);
		} catch (IllegalArgumentException e) {
			//
		}
		try {
			times[TransitTimes.valueOf(name).ordinal()] = Time.parseTime(value);
		} catch (IllegalArgumentException e) {
			//
		}
	}

	/**
	 * All parameters used (and adapted) in Transit
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 23.09.2012
	 */
	public enum TransitParams {

		/**
		 * Percentage of the total bandwidth that transit is allowed to use
		 * (both up and down)
		 */
		BANDWIDTH_MAX_SHARE(90),

		/**
		 * Number of chunks that are considered urgent starting from the current
		 * playback position. A too high value will result in duplicates due to
		 * the live-flows, whereas a too low value will lead to stalls if some
		 * blocks are missing and can not be delivered in time.
		 */
		REQUEST_BUFFER_URGENT_SIZE(15),

		/**
		 * Max parallel open requests
		 */
		REQUEST_MAX_PARALLEL(16),

		/**
		 * Number of blocks that are requested at once over a connection
		 * (requests may directly contain more than one block)
		 */
		REQUEST_MAX_BLOCKS_PER_CONNECTION(1),

		/**
		 * Number of blocks that are requested in parallel from a source
		 */
		REQUEST_MAX_BLOCKS_PER_CONNECTION_SOURCE(4),

		/**
		 * The chosen request-strategy.
		 * 
		 * 0: disable requests on connections that are not also used for flows
		 * 1: enable requests on all connections, prefer low load
		 * 
		 */
		REQUEST_STRATEGY(1),

		/**
		 * Size of the buffermaps that are sent in CHUNKS, not blocks!
		 */
		REQUEST_BUFFERMAP_SIZE(50),

		/**
		 * Maximum ticks (chunk intervals) after which a flow is considered
		 * dead, if no new block arrives.
		 */
		FLOW_MAX_LIVE_CHUNK_DELAY(3),

		/**
		 * Max variance between the most recent chunk on different flows (nodes
		 * try to maintain roughly the same positions in all flow trees)
		 * 
		 * TODO evaluate the impact of this parameter
		 */
		FLOW_MAX_LIVE_CHUNK_VARIANCE(16),

		/**
		 * Max number of concurrently active incoming flows, set to zero to
		 * disable flows completely.
		 */
		FLOW_MAX_INCOMING(10000),

		/**
		 * Limits the width of a flow on one connection. A flow can include this
		 * amount of layers.
		 */
		FLOW_MAX_PER_CONNECTION(10000),

		/**
		 * Max outgoing connections
		 */
		CONNECTION_MAX_OUTGOING(32),

		/**
		 * Max incoming connections (active + inactive)
		 */
		CONNECTION_MAX_INCOMING(16),

		/**
		 * Max idle incoming connections (not used for at least
		 * CONNECTION_INACTIVITY_TIMEOUT)
		 */
		CONNECTION_MAX_INCOMING_INACTIVE(8),

		/**
		 * The Connection manager tries to aggressively maintain at least this
		 * number of incoming connections.
		 */
		CONNECTION_MIN_INCOMING(2),

		/**
		 * Probability, that an inactive connection is killed (it has at least
		 * been inactive for CONNECTION_INACTIVITY_TIMEOUT). Enter a percentage
		 * (int between 0 and 100)
		 */
		CONNECTION_INACTIVITY_KILL_PROBABILITY(0),
		
		/**
		 * If the heads must be leaves on all other stripes they are not active.
		 * The heads can request only one stripe from the server.
		 */
		HEADS_AS_LEAVES(0),
		
		/**
		 * Shall the capacity of the heads be exhausted? 1=include them in the initial list, 2=only send them in the initial list.
		 */
		HEADS_CAPACITY(0),

		/**
		 * If the flow requests are sortered in order of join-time so older
		 * peers get more requests.
		 */
		PRIORITIZE_REQUESTS(0),

		/**
		 * If the peer request the flow from other peers only if they know they
		 * have it, i.e. they received the flow previously.
		 */
		REQUEST_IF_HAS_OFFER(0),

		/**
		 * Do topology optimizations. 0=no optimization, 1=our optimization, 2=mtreebone optimization.
		 */
		DO_OPTIMIZE(0),
		
		/**
		 * 0=size-based, 1=time-based
		 */
		BATCH_MODE(0),
		/**
		 * Batch-join. 0=no batch join, 1= batch join: without creating chains, 2= batch join: creating chains. 
		 */
		BATCH(0),
		/**
		 * For batch join (2): level1 is the deepest chain,, etc.
		 */
		BATCH_CHAIN_LEVEL(0),
		
		/**
		* Number of nodes to join in the batch-join modus.
		*/
		BATCH_JOIN_NUMBER_OF_NODES(32),
		
		GRAPHSTREAM_MODE(0);

		private final int defaultValue;

		private TransitParams(int defaultValue) {
			this.defaultValue = defaultValue;
		}

		protected int getDefault() {
			return defaultValue;
		}

	}

	/**
	 * All times used in Transit
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 23.09.2012
	 */
	public enum TransitTimes {

		/**
		 * Max RTT for a request
		 */
		REQUEST_RTT_MAX(600 * Time.MILLISECOND),

		/**
		 * Buffermaps will be piggybacked every n seconds
		 */
		BUFFERMAP_EXCHANGE_INTERVAL(1500 * Time.MILLISECOND),

		/**
		 * Maximum RTT for a Flow
		 */
		FLOW_RTT_MAX(650 * Time.MILLISECOND),

		/**
		 * After the RTT grows beyond this value, a connection will be closed.
		 */
		CONNECTION_RTT_MAX(750 * Time.MILLISECOND),

		/**
		 * A connection is kept open for this time, even if not a single video
		 * block has been transmitted. After this time, it may be closed again
		 */
		CONNECTION_INACTIVITY_TIMEOUT(30 * Time.SECOND),

		/**
		 * Time between two executions of the {@link ConnectionControlOperation}
		 * for a connection.
		 */
		CONNECTION_CONTROL_OPERATION_INTERVAL(20 * Time.SECOND),

		/**
		 * Interval in which neighborhoods are piggybacked
		 */
		NEIGHBORHOOD_EXCHANGE_INTERVAL(2 * Time.MINUTE),

		/**
		 * Minimal delay until playback is started
		 */
		INITIAL_BUFFER_DELAY(0 * Time.SECOND),

		/**
		 * Interval in which node info is piggybacked.
		 */
		INFO_EXCHANGE_INTERVAL(1500 * Time.MILLISECOND),
		
		/**
		 * How often to check for optimizations.
		 */
		OPTIMIZATION_INTERVAL(200 * Time.MILLISECOND),
		
		/**
		 * How often is the time-based batch inserted to the overlay.
		 */
		BATCH_TIME(5 * Time.SECOND),
		
		/**
		 * Static value of flash crowd start.
		 */
		FLASH_CROWD_START(2 * Time.HOUR),
		
		/**
		 * Static value of flash crowd end.
		 */
		FLASH_CROWD_END(2 * Time.HOUR + 3 * Time.MINUTE),
		
		HEADS_MESSAGE_INTERVAL(1500 * Time.MILLISECOND)
		;

		private final long defaultValue;

		private TransitTimes(long defaultValue) {
			this.defaultValue = defaultValue;
		}

		protected long getDefault() {
			return defaultValue;
		}
	}

	@Override
	public TransitSettings clone() {
		return new TransitSettings(this);
	}

}
