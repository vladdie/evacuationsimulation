/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.requesting;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;

public class TransitPPRequestStrategyLinear implements TransitPiecePickingRequestStrategy {

	/** The node. */
	private TransitNode node;

	/**
	 * Instantiates a new transit piece picking strategy linear.
	 *
	 * @param node the node
	 */
	public TransitPPRequestStrategyLinear(TransitNode node) {
		this.node = node;
	}
	
	/* (non-Javadoc)
	 * @see de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.strategies.piecepicking.requesting.TransitPiecePickingRequestStrategy#selectPiecesForRequest(java.util.BitSet, int)
	 */
	public BitSet selectPiecesForRequest(BitSet match, int maxBlocks, boolean reverseOrderForTradingGain) {
		
		int set = 0;
		BitSet result = new BitSet();
		/* Select bits in reverse order! */
		// if (reverseOrderForTradingGain == true) {
		// for (int i = match.length(); (i = match.previousSetBit(i - 1)) >= 0
		// && set < maxBlocks;) {
		// result.set(i);
		// set++;
		// }
		// /* Select bits in normal order! */
		// } else {
			for (int i = match.nextSetBit(0); i >= 0 && set < maxBlocks; i = match.nextSetBit(i + 1)) {
				result.set(i);
				set++;
			}
		// }
		
		
		/* Add new chunks from end as per config. */
		if (!match.isEmpty()) {
			int newBlocks = node.getSettings().getParam(TransitParams.REQUEST_NEWEST_BLOCKS);
			int prevCard = result.cardinality();
			int matchLength = match.length();

			if (newBlocks > 0) {
				for (int i = 0; i < prevCard; i++) {
					result.set(matchLength - 1 - i, match.get(matchLength - 1 - i));
					if (result.cardinality() >= prevCard + newBlocks) {
						break;
					}
				}
			}
		}
		
		return result;
	}
	
}
