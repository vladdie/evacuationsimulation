/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling;

import java.util.BitSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingConfiguration;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingDocument;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitFactory;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.PeriodicPiggybackListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.HeadCapacityMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.NodeInfoMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SendBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SendScheduleMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitFlowMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitFlowMessage.FlowMsgType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnectionListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact.ContactType;

/**
 * This scheduler assigns blocks of a chunk (a Blockmask) via a schedule to
 * another node and monitors the received blocks. If a neighbor performs badly
 * in terms of arrival rate, we request part of his schedule from another
 * neighbor. This way, flows are formed for each block of the chunk mask.
 * 
 * This class implements the basic protocol consisting of REQUEST -> REPLY,
 * CANCEL and DENY messages. Handling for enhanced messages such as ALTER and
 * OFFER have to be implemented by the extending class. The flow over each
 * connection is measured and as soon as it is below the expectations, the
 * corresponding methods are triggered. Again, HOW to react is then up to the
 * extending class. Layer changes do not trigger any proactive messages on
 * source nodes, they just trigger updated REQUESTS from sink nodes.
 * 
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 15.06.2012
 */
public abstract class AbstractTransitFlowManager implements TransitFlowManager,
		TransitConnectionListener {

	protected static final boolean DEBUG = false;

	/**
	 * The node
	 */
	private final TransitNode node;

	/**
	 * Incoming Flow (all connections that are used for incoming blocks)
	 */
	private IncomingFlow incomingFlow;

	/**
	 * Outgoing Flow (all connections that are used to send blocks)
	 */
	private final OutgoingFlow outgoingFlow;

	/**
	 * If the present node is a head node: when did it lastly execute the
	 * headsTick() method?
	 */
	private long headsMessageLastTry = 0;

	/**
	 * If the present node is a head node: how long to wait before executing the
	 * headsTick() method another time.
	 */
	private long headsMessageWait = 0;

	/**
	 * If the present node is a head node: what did it lastly report to the
	 * server? Did it report it has capacity or did it report it does not have
	 * capacity?
	 */
	private boolean headsLastCapacitySent = false;

	/**
	 * Create a new FlowManager
	 * 
	 * @param node
	 */
	public AbstractTransitFlowManager(TransitNode node) {
		this.node = node;
		if (getNode().isServer()) {
			incomingFlow = new SourceIncomingFlow();
		} else {
			incomingFlow = new IncomingFlow();
		}
		outgoingFlow = new OutgoingFlow();

		node.getNeighborhood().getConnectionManager()
				.addConnectionListener(this);

		node.getMessageHandler().addPiggybackListener(
				new ExchangeNodeInfo(node.getSettings().getTime(
						TransitTimes.INFO_EXCHANGE_INTERVAL)));
	}

	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		switch (peerStatus) {
		case PRESENT:
		case TO_JOIN:
			break;

		case ABSENT:
			// TODO
			break;

		default:
			throw new AssertionError("Unknown PeerStatus.");
		}
	}

	/**
	 * The node
	 * 
	 * @return
	 */
	public TransitNode getNode() {
		return node;
	}

	@Override
	public int getCapacities() {
		return incomingFlow.getCapacities();
	}

	@Override
	public void receivedFlowMessage(TransitFlowMessage message,
			TransitReplyHandler replyTo) {

		TransitFlowSchedule schedule = message.getFlowSchedule();
		TransitConnection connection = node.getNeighborhood()
				.getConnectionManager()
				.getConnection(message.getSenderContact());
		if (connection == null) {
			return;
		}
		if (connection.getState() == ConnectionState.CLOSED
				|| connection.getState() == ConnectionState.CLOSING) {
			if (DEBUG) {
				System.err.println("TFM: dumping message " + message.toString()
						+ " as connection is already closed/closing: "
						+ connection.toString());
			}
			return;
		}
		assert connection != null;

		switch (message.getType()) {
		case REQUEST:
			processScheduleRequest(schedule, connection, replyTo);
			break;

		case REPLY:
			processScheduleReply(schedule, connection, replyTo);
			break;

		case OFFER:
			processScheduleOffer(schedule, connection);
			break;

		case ALTER:
			processScheduleAlter(schedule, connection, replyTo);
			break;

		case DENY:
			processScheduleDeny(connection);
			break;

		case CANCEL:
			processScheduleCancel(connection);
			break;

		case REQUEST_REPLACE_CHILD:
			processScheduleRequestReplace(schedule, connection, replyTo,
					message.getNodeToReplace(), message.getCalculatedRTT());
			break;
		case REQUEST_FROM_ME_COMMAND:
			processRequestFromMeCommand(schedule, connection, replyTo);
			break;
		default:
			throw new UnsupportedOperationException("Unknown schedule type!");
		}

	}

	public IncomingFlow getIncomingFlow() {
		return incomingFlow;
	}

	public OutgoingFlow getOutgoingFlow() {
		return outgoingFlow;
	}

	@Override
	public boolean isHealthy() {
		return incomingFlow.isHealthy(null);
	}

	/**
	 * Call this if you want to DENY a request
	 * 
	 * @param connection
	 * @param replyTo
	 */
	protected void doDenyRequest(TransitConnection connection,
			TransitReplyHandler replyTo) {
		assert connection.getScheduleState() == ScheduleState.INACTIVE
				|| connection.getScheduleState() == ScheduleState.CANCELED;
		assert connection.getDirection() == ConnectionDirection.OUT;
		connection.setScheduleState(ScheduleState.CANCELED);
		SendScheduleMessage reply = new SendScheduleMessage(
				node.getLocalOverlayContact(), FlowMsgType.DENY);
		replyTo.reply(reply);
	}

	/**
	 * Call this if you want to ALTER a schedule (i.e. send less blocks than
	 * before) from the source-side or request less blocks than before from the
	 * sink side.
	 * 
	 * @param connection
	 * @param newSchedule
	 */
	public void doAlterSchedule(TransitConnection connection,
			TransitFlowSchedule newSchedule) {
		assert connection.getScheduleState() == ScheduleState.ACTIVE;
		/*
		 * This will trigger the listener in the OutgoingFlow/IngoingFlow which
		 * will in turn send the new subset of blocks per chunk or request
		 * missing blocks from other connections.
		 */
		connection.setSchedule(newSchedule);
		SendScheduleMessage msg = new SendScheduleMessage(
				node.getLocalOverlayContact(), FlowMsgType.ALTER, newSchedule);
		node.getMessageHandler().send(msg, connection.getEndpoint());
	}

	/**
	 * Call this if you want to send a CANCEL to one of your sources to stop
	 * them sending packets to you.
	 * 
	 * @param connection
	 */
	public void doCancel(TransitConnection connection) {
		assert connection.getDirection() == ConnectionDirection.IN;
		// assert connection.getState() == ConnectionState.OPEN;
		connection.setScheduleState(ScheduleState.CANCELED);
		SendScheduleMessage msg = new SendScheduleMessage(
				node.getLocalOverlayContact(), FlowMsgType.CANCEL);
		node.getMessageHandler().send(msg, connection.getEndpoint());
	}

	@Override
	public void onConnectionStateChanged(TransitConnection connection,
			ConnectionState oldState, ConnectionState newState) {
		if (newState == ConnectionState.CLOSING) {
			if (connection.getScheduleState() == ScheduleState.ACTIVE
					|| connection.getScheduleState() == ScheduleState.NEGOTIATING) {
				connection.setScheduleState(ScheduleState.CANCELED);
			}
		}
		if (newState == ConnectionState.CLOSED) {
			assert connection.getScheduleState() == ScheduleState.CANCELED
					|| connection.getScheduleState() == ScheduleState.INACTIVE;
		}
	}

	/**
	 * Call this, if you no longer can or want to send outgoing packets via this
	 * connection. We do not use a callback here, as this message does not get
	 * any reply and we do not add any retransmission. Instead, the receiver
	 * will either get this message or he will detect that no more blocks
	 * arrive. This message is just intended to shorten this waiting period to
	 * allow the receiver to connect to a new neighbor.
	 * 
	 * @param connection
	 */
	public void doDenySchedule(TransitConnection connection) {
		assert connection.getDirection() == ConnectionDirection.OUT;
		assert connection.getState() == ConnectionState.OPEN;
		assert connection.getScheduleState() == ScheduleState.ACTIVE;
		connection.setScheduleState(ScheduleState.CANCELED);
		SendScheduleMessage msg = new SendScheduleMessage(getNode()
				.getLocalOverlayContact(), FlowMsgType.DENY);
		node.getMessageHandler().send(msg, connection.getEndpoint());
	}

	@Override
	public BitSet getCapacity() {
		return incomingFlow.calculateCapacities();
	}

	/**
	 * Call this if you want to grant a request. The grant-schedule does not
	 * need to be the full REQUEST, it can also just contain a subset of the
	 * REQUESTed blocks.
	 * 
	 * @param grant
	 * @param connection
	 * @param replyTo
	 */
	public void doGrantRequest(TransitFlowSchedule grant,
			TransitConnection connection, TransitReplyHandler replyTo) {
		assert connection.getDirection() == ConnectionDirection.OUT;
		assert !grant.getBlockMask().isEmpty();
		addConnection(connection, grant);
		connection.setScheduleState(ScheduleState.ACTIVE);
		SendScheduleMessage reply = new SendScheduleMessage(
				node.getLocalOverlayContact(), FlowMsgType.REPLY, grant);
		replyTo.reply(reply);
		if (DEBUG) {
			System.out.println(Time.getFormattedTime(Time
					.getCurrentTime())
					+ " TFM: REQUEST GRANTED "
					+ connection.toString()
					+ " all active flows: "
					+ getOutgoingFlow().connections.toString());
		}
	}

	/**
	 * Request a schedule over the given connection. This is no longer done as
	 * part of an operation, as it allows easier handling in this class and does
	 * not scatter message handling. However, actions leading to a call of this
	 * method are still most likely the result of an operation.
	 * 
	 * @param schedule
	 * @param connection
	 */
	public void doRequest(TransitFlowSchedule schedule,
			TransitConnection connection) {
		assert !schedule.getBlockMask().isEmpty();
		assert connection.getDirection() == ConnectionDirection.IN;
		assert connection.getScheduleState() == ScheduleState.INACTIVE;
		assert connection.getState() == ConnectionState.OPEN;
		// addConnection(connection, schedule);
		connection.setScheduleState(ScheduleState.NEGOTIATING);
		SendScheduleMessage request = new SendScheduleMessage(
				node.getLocalOverlayContact(), FlowMsgType.REQUEST, schedule);

		node.getMessageHandler().send(request, connection.getEndpoint());
		if (DEBUG) {
			System.out.println(Time.getFormattedTime(Time
					.getCurrentTime())
					+ " TFM: REQUEST "
					+ schedule.toString()
					+ " on " + connection.toString());
		}
	}

	public void doRequestReplaceChild(TransitFlowSchedule schedule,
			TransitConnection connection, TransitContact replaceChild,
			long calculatedRTT) {
		connection.setScheduleState(ScheduleState.NEGOTIATING);
		SendScheduleMessage request = new SendScheduleMessage(
				node.getLocalOverlayContact(), schedule, replaceChild,
				calculatedRTT);
		node.getMessageHandler().send(request, connection.getEndpoint());
		if (DEBUG) {
			System.out.println(Time.getFormattedTime(Time
					.getCurrentTime())
					+ " TFM: REQUEST_REPLACE_CHILD "
					+ schedule.toString() + " on " + connection.toString());
		}
	}

	public void doSendRequestFromMeCommand(TransitFlowSchedule schedule,
			TransitConnection connection) {
		SendScheduleMessage command = new SendScheduleMessage(
				node.getLocalOverlayContact(),
				FlowMsgType.REQUEST_FROM_ME_COMMAND, schedule);
		node.getMessageHandler().send(command, connection.getEndpoint());
	}

	/**
	 * This is called whenever a live-Schedule (or Flow) is no longer fulfilled
	 * by our current selection of active schedules and should be replaced with
	 * a new one. This is only called if the set of schedules is not sufficient
	 * for our demands, not, if blocks are not received.
	 * 
	 * @param blockMask
	 * @param fromChunk
	 */
	protected abstract void handleMissingFlow(BitSet blockMask);

	/**
	 * Process a schedule-REQUEST. Finally, either call grantRequest or
	 * denyRequest with the schedule that you are able to fulfill.
	 * 
	 * @param request
	 * @param connection
	 * @param replyTo
	 */
	protected abstract void processScheduleRequest(TransitFlowSchedule request,
			TransitConnection connection, TransitReplyHandler replyTo);

	protected abstract void processScheduleRequestReplace(
			TransitFlowSchedule request, TransitConnection connection,
			TransitReplyHandler replyTo, TransitContact childToReplace,
			long calculatedRTT);

	protected abstract void processRequestFromMeCommand(
			TransitFlowSchedule request, TransitConnection connection,
			TransitReplyHandler replyTo);

	/**
	 * Process a schedule-REPLY: (positive) answer to a REQUEST
	 * 
	 * @param reply
	 * @param connection
	 * @param replyTo
	 */
	protected void processScheduleReply(TransitFlowSchedule reply,
			TransitConnection connection, TransitReplyHandler replyTo) {
		addConnection(connection, reply);
		assert connection.getState() == ConnectionState.OPEN;
		assert connection.getDirection() == ConnectionDirection.IN;
		connection.setScheduleState(ScheduleState.ACTIVE);
		node.getOptimizationManager().onFlowReply(connection);
	}

	/**
	 * Process a schedule-OFFER: source is offering a schedule. This is an
	 * optional operation.
	 * 
	 * @param offer
	 * @param connection
	 */
	protected abstract void processScheduleOffer(TransitFlowSchedule offer,
			TransitConnection connection);

	/**
	 * Process a schedule-ALTER: source is notifying us of a changed schedule.
	 * This is an optional operation. If we neglect this message, the scheduling
	 * mechanism should instead detect missing blocks after some time and alter
	 * its request accordingly as this is the basic contract.
	 * 
	 * @param alter
	 * @param connection
	 * @param replyTo
	 */
	protected void processScheduleAlter(TransitFlowSchedule alter,
			TransitConnection connection, TransitReplyHandler replyTo) {
		assert connection.getState() == ConnectionState.OPEN;
		// this will trigger the flows
		connection.setSchedule(alter);
	}

	/**
	 * Process a schedule-DENY: source does not allow a connection for
	 * scheduling or source canceled the schedule actively during the
	 * transmission.
	 * 
	 * @param connection
	 */
	protected void processScheduleDeny(TransitConnection connection) {
		if (connection.getScheduleState() == ScheduleState.INACTIVE) {
			return;
		}
		assert connection.getDirection() == ConnectionDirection.IN;
		/*
		 * we do not reply, we just set the incoming connection to canceled
		 */
		connection.setScheduleState(ScheduleState.CANCELED);
		node.getOptimizationManager().onFlowDeny(connection);
	}

	@Override
	public int[] calculateFreeSlots() {
		return incomingFlow.calculateFreeSlots();
	}

	/**
	 * Process a schedule-CANCEL: sink does not want to receive any packets
	 * anymore. This is sent instead of an empty REQUEST to stop a connection
	 * from receiver side.
	 * 
	 * @param connection
	 */
	protected void processScheduleCancel(TransitConnection connection) {
		assert connection.getDirection() == ConnectionDirection.OUT;
		/*
		 * We just close the connection (we are the source)
		 */
		connection.setScheduleState(ScheduleState.CANCELED);
	}

	/**
	 * A new connection is used for scheduling
	 * 
	 * @param connection
	 */
	private void addConnection(TransitConnection connection,
			TransitFlowSchedule schedule) {
		switch (connection.getDirection()) {
		case IN:
			incomingFlow.addConnection(connection);
			break;

		case OUT:
			outgoingFlow.addConnection(connection);
			break;

		default:
			throw new UnsupportedOperationException(
					"Unknown connection direction.");
		}

		connection.setSchedule(schedule);
	}

	@Override
	public void tick() {
		incomingFlow.tick(node.getVideo().getCurrentPlaybackChunk());
		outgoingFlow.tick(node.getVideo().getCurrentPlaybackChunk());
		node.getOptimizationManager().tick();

		headsTick();

	}

	/**
	 * Procedure executed by the heads on each tick.
	 */
	public void headsTick() {
		if (getNode().getSettings().getParam(TransitParams.HEADS_CAPACITY) != 0) {

			if (incomingFlow.isHead()) {
				if (headsMessageLastTry + headsMessageWait > Time
						.getCurrentTime()) {
					return;
				}

				BitSet active = incomingFlow.getActiveFlow();

				BitSet hasCapacity = getCapacity(active);

				boolean hasCapacityAsBoolean = false;
				if (hasCapacity.nextSetBit(0) != -1
						&& active.nextSetBit(0) != -1
						&& hasCapacity.nextSetBit(0) == active.nextSetBit(0)) {
					hasCapacityAsBoolean = true;
				} else {
					hasCapacityAsBoolean = false;
				}

				if (hasCapacityAsBoolean == headsLastCapacitySent) {
					/* We don't send it again. */
					return;
				}

				headsLastCapacitySent = hasCapacityAsBoolean;

				// /*
				// * Using global knowledge to find the first tracker
				// */
				// TransitTracker tracker = null;
				// for (Host host : GlobalOracle.getOnlineHosts()) {
				// tracker = host.getComponent(TransitTracker.class);
				// if (tracker != null) {
				// break;
				// }
				// }
				//
				// if (tracker == null) {
				// /*
				// * For now, we just throw an exception.
				// */
				// throw new ConfigurationException("No Tracker found!");
				// }

				TransitContact trackerContact = TransitFactory
						.getTrackerContact(node);

				HeadCapacityMessage capMsg = new HeadCapacityMessage(
						node.getLocalOverlayContact(), hasCapacityAsBoolean);
				node.getMessageHandler().send(capMsg, trackerContact);

				headsMessageWait = node.getSettings().getTime(
						TransitTimes.HEADS_MESSAGE_INTERVAL);
				headsMessageLastTry = Time.getCurrentTime();

			}
		}
	}

	/**
	 * Gets a bitset containing which of the requested flows we can accept
	 * because of spare capacity.
	 * 
	 * @param request
	 */
	public BitSet getCapacity(BitSet request) {
		/*
		 * Compute the blocks we are able to send with our spare capacity.
		 */
		BitSet blockMaskPossible = new BitSet();
		for (int i = request.nextSetBit(0); i >= 0; i = request
				.nextSetBit(i + 1)) {

			blockMaskPossible.set(i);
			TransitFlowSchedule scheduleOffer = new TransitFlowSchedule(
					blockMaskPossible);
			if (!getNode().getBandwidthManager().hasSpareBandwidthFor(
					scheduleOffer, ConnectionDirection.OUT)) {
				blockMaskPossible.set(i, false);
			}
		}

		return blockMaskPossible;
	}

	@Override
	public void receivedLiveBlock(TransitBlockMessage message,
			TransitReplyHandler replyTo, boolean duplicate) {
		assert message.isLiveBlock();
		TransitConnection connection = node.getNeighborhood()
				.getConnectionManager()
				.getConnection(message.getSenderContact());
		assert connection != null;
		connection.blockTransferred(message.getBlockNumber(),
				message.getSize(), true);

		if (connection.getScheduleState() != ScheduleState.ACTIVE) {
			/*
			 * This might indicate that the source did not get the information
			 * that we no longer want to receive packets via this connection. We
			 * send a CANCEL again.
			 */
			doCancel(connection);

			if (connection.getLastScheduleStateChangeTimestamp() + 2
					* Time.SECOND < Time.getCurrentTime()) {

				// Bandwidth bw = GlobalOracle
				// .getHostForNetID(
				// connection.getEndpoint().getTransInfo()
				// .getNetId()).getNetLayer()
				// .getCurrentBandwidth();
				// Bandwidth ownBw = node.getHost().getNetLayer()
				// .getCurrentBandwidth();
				// System.out.println("NET BANDWIDTH: ENDPOINT: " + bw +
				// " OWN: "
				// + ownBw);
				// System.out.println("BANDWIDTH MSG: "
				// + node.getMessageHandler().currentBandwidth
				// .getEstimatedBandwidth() + " of "
				// + node.getHost().getNetLayer().getMaxBandwidth());
				// throw new AssertionError("TFM: still receiving blocks via "
				// + connection.toString()
				// + " after schedule is no longer active!");
			}
		}
		if (!duplicate) {
			outgoingFlow.newBlockAvailable(message);
			incomingFlow.calculatetreeDepth(connection, message);
		}
		if (node.getSettings().getParam(TransitParams.BATCH) == 1) {
			if (node.getNeighborhood().isInBatchJoin()) {
				if (DEBUG) {
					System.out.println(node.getHost().getId()
							+ ": "
							+ Time.getFormattedTime(Time
									.getCurrentTime())
							+ " got live block from: "
							+ message.getSenderContact().getNodeID());
				}

				/* We have to adjust the video playback position. */
				StreamingDocument video = node.getVideo();
				long blocksPerChunk = video.getVideoModel()
						.getNumOfBlocksPerChunk();
				int correspondingChunk = (int) (message.getBlockNumber() / blocksPerChunk);
				StreamingDocument newDoc = new StreamingDocument(
						node.getVideo(), correspondingChunk + 1);
				node.setVideo(newDoc);

				node.getNeighborhood().setInBatchJoin(false);
			}
		}
		incomingFlow.newBlockArrived(connection, message.getBlockNumber(),
				replyTo);

	}

	@Override
	public void requestFlow(BitSet mask) {
		/*
		 * TODO get rid of "switch layer"
		 */
		incomingFlow.switchedLayer();
		outgoingFlow.switchedLayer();
	}

	/**
	 * This method is used to forward blocks.
	 * 
	 * This is basically just a shortcut to the message handler and ensures that
	 * the correct message is created.
	 * 
	 * @param blockMessage
	 *            the message to forward
	 * @param connection
	 */
	protected void forwardLiveBlock(TransitBlockMessage blockMessage,
			TransitConnection connection) {
		assert connection.getDirection() == ConnectionDirection.OUT;
		assert getNode().getVideo().haveBlock(blockMessage.getBlockNumber());
		assert blockMessage.isLiveBlock();

		SendBlockMessage msg = new SendBlockMessage(
				node.getLocalOverlayContact(), (SendBlockMessage) blockMessage);
		connection.blockTransferred(msg.getBlockNumber(), msg.getSize(), true);
		node.getMessageHandler().send(msg, connection.getEndpoint());
	}

	/**
	 * Returns true, if the given block is really requested by the schedule.
	 * 
	 * @param blocknum
	 * @param schedule
	 * @return
	 */
	protected boolean matchesSchedule(int blocknum, TransitFlowSchedule schedule) {
		assert schedule != null;
		int blockOffset = blocknum % node.BLOCKS_PER_CHUNK;
		return schedule.getBlockMask().get(blockOffset);
	}

	@Override
	public String toString() {
		return "FlowManager - in: " + incomingFlow.toString() + " out: "
				+ outgoingFlow.toString();
	}

	@Override
	public int[] getDepth() {
		return getIncomingFlow().getDepth();
	}

	@Override
	public int[] getNumberOfChildren() {
		return getOutgoingFlow().getNumberOfChildren();
	}

	public NodeInfoMessage getNodeInfo() {

		int[] numberOfChildren = getNumberOfChildren();
		int[] depth = getDepth();
		BitSet capacities = getIncomingFlow().calculateCapacities();
		TransitContact[] parents = getIncomingFlow().getParents();

		NodeInfoMessage info = new NodeInfoMessage(getNode()
				.getLocalOverlayContact(), numberOfChildren, depth, capacities,
				parents);

		return info;
	}

	/**
	 * Stub for the incoming flow of a source
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 21.08.2012
	 */
	protected class SourceIncomingFlow extends IncomingFlow implements
			EventHandler {

		private BitSet blockMask;

		private int currentChunk;

		private long interval;

		public SourceIncomingFlow() {
			super();
		}

		@Override
		public void addConnection(TransitConnection connection) {
			throw new AssertionError();
		}

		@Override
		public boolean isHealthy(BitSet request) {
			return true;
		}

		@Override
		public void switchedLayer() {
			blockMask = getNode().getVideo().getLayerMask(getNode().getLayer());
			interval = (long) (1
					/ (StreamingConfiguration.CHUNKS_PER_SECOND + 1)
					* Time.SECOND / getNode().BLOCKS_PER_CHUNK);
			interval = interval / Time.MILLISECOND;
		}

		@Override
		public BitSet getHealthyFlow() {
			return (BitSet) blockMask.clone();
		}

		@Override
		public BitSet calculateCapacities() {
			BitSet capacities = new BitSet();

			capacities.set(0, 4, true);

			return getCapacity(capacities);
		}

		@Override
		public void newBlockArrived(TransitConnection connection, int blocknum,
				TransitReplyHandler replyTo) {
			throw new AssertionError();
		}

		@Override
		public boolean onScheduleChanged(TransitConnection connection,
				TransitFlowSchedule oldSchedule, TransitFlowSchedule newSchedule) {
			throw new AssertionError();
		}

		@Override
		public boolean onScheduleStateChanged(TransitConnection connection,
				ScheduleState oldState, ScheduleState newState) {
			throw new AssertionError();
		}

		@Override
		public void tick(int chunk) {
			currentChunk = chunk;
			int firstblock = chunk * getNode().BLOCKS_PER_CHUNK;
			Event.scheduleImmediately(this, Integer.valueOf(firstblock), -1);
		}

		@Override
		public void eventOccurred(Object content, int type) {
			// FIXME[JR]: Should not happen - remove after testing!
			if (!(content instanceof Integer)) {
				throw new AssertionError(
						"Wrong content type in event handling!");
			}

			int block = Integer.valueOf((Integer) content);
			getOutgoingFlow().newBlockAvailable(
					new SendBlockMessage(getNode().getLocalOverlayContact(),
							block, getNode().getVideo().getBlockSize(block),
							true));
			if (block < (currentChunk + 1) * getNode().BLOCKS_PER_CHUNK - 1) {
				// one more
				block++;
				Event.scheduleWithDelay(interval, this, Integer.valueOf(block),
						-1);
			}
		}
	}

	/**
	 * Container for all incoming connections and their associated
	 * Live-Schedules.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 28.06.2012
	 */
	public class IncomingFlow implements TransitFlowScheduleListener {

		private final List<TransitConnection> connections;

		private BitSet blockMask;

		private int highestLiveChunk;

		private final LinkedHashMap<TransitConnection, Integer> roundsWithoutChunkChanges;

		private final LinkedHashMap<TransitConnection, Integer> lastChunkTimestampPerFlowLastRound;

		private final LinkedHashMap<TransitConnection, Integer> lastChunkPerFlow;

		private final LinkedHashMap<TransitConnection, Long> lastChunkTimestampPerFlow;

		// FIXME[JR]: Make this more flexible and not dependent on exactly 4
		// layers!
		private final int[] depth = new int[4];

		/**
		 * Seeders and Servers are healthy even if nothing is incoming.
		 */
		private boolean isHealthy = false;

		public IncomingFlow() {
			this.connections = new LinkedList<TransitConnection>();
			this.lastChunkPerFlow = new LinkedHashMap<TransitConnection, Integer>();
			this.lastChunkTimestampPerFlow = new LinkedHashMap<TransitConnection, Long>();
			this.lastChunkTimestampPerFlowLastRound = new LinkedHashMap<TransitConnection, Integer>();
			this.roundsWithoutChunkChanges = new LinkedHashMap<TransitConnection, Integer>();
			for (int i = 0; i <= 3; i++) {
				depth[i] = -1;
			}
		}

		/** How many times can we send all flows. */
		public int getCapacities() {
			BitSet bs = new BitSet();
			bs.set(0, 4, true);
			TransitFlowSchedule schedule = new TransitFlowSchedule(bs);

			int available = getNode().getBandwidthManager()
					.getNumberOfAvailableSlots(schedule,
							ConnectionDirection.OUT);

			return available;
		}

		public TransitContact[] getParents() {
			TransitContact[] parents = new TransitContact[4];

			for (int i = 0; i <= 3; i++) {
				for (TransitConnection connection : connections) {
					BitSet schedule = connection.getSchedule().getBlockMask();
					if (schedule.get(i)) {
						parents[i] = connection.getEndpoint();
					}
				}
			}

			return parents;
		}

		public void calculatetreeDepth(TransitConnection connection,
				TransitBlockMessage message) {

			if (connection.getSchedule() == null) {
				return;
			}

			int blockOffset = message.getBlockNumber()
					% getNode().BLOCKS_PER_CHUNK;
			depth[blockOffset] = message.getHopsFromSource();
		}

		public int[] getDepth() {
			return depth;
		}

		public BitSet calculateCapacities() {
			BitSet capacities = new BitSet();

			for (TransitConnection connection : connections) {
				BitSet schedule = connection.getSchedule().getBlockMask();
				for (int i = schedule.nextSetBit(0); i != -1; i = schedule
						.nextSetBit(i + 1)) {
					capacities.set(i, true);
				}
			}
			/* Do we have capacity for sending it? */
			return getCapacity(capacities);

		}

		public int[] calculateFreeSlots() {
			BitSet incomingFlows = new BitSet();

			for (TransitConnection connection : connections) {
				BitSet schedule = connection.getSchedule().getBlockMask();
				for (int i = schedule.nextSetBit(0); i != -1; i = schedule
						.nextSetBit(i + 1)) {
					incomingFlows.set(i, true);
				}
			}

			return calculateFreeSlots(incomingFlows);

		}

		private int[] calculateFreeSlots(BitSet incomingFlows) {
			int[] freeSlots = new int[4];
			for (int i = 0; i <= 3; i++) {
				freeSlots[i] = 0;
			}

			// ///////////

			BitSet blockMaskPossible = new BitSet();
			for (int i = incomingFlows.nextSetBit(0); i >= 0; i = incomingFlows
					.nextSetBit(i + 1)) {

				blockMaskPossible.set(i);
				TransitFlowSchedule scheduleOffer = new TransitFlowSchedule(
						blockMaskPossible);
				int available = getNode().getBandwidthManager()
						.getNumberOfAvailableSlots(scheduleOffer,
								ConnectionDirection.OUT);
				freeSlots[i] = available;
			}

			// ///////////

			return freeSlots;
		}

		public TransitConnection getConnectionForFlow(int flowIndex) {
			for (TransitConnection connection : connections) {
				if (connection.getSchedule().getBlockMask().get(flowIndex)) {
					return connection;
				}
			}

			return null;
		}

		public List<TransitConnection> getConnections() {
			return connections;
		}

		/**
		 * Returns the active flow of the node. The node must be a head node in
		 * order for it to have an active flow.
		 * 
		 * @return
		 */
		public BitSet getActiveFlow() {
			if (!isHead()) {
				return null;
			}

			for (TransitConnection connection : connections) {
				TransitContact endpoint = connection.getEndpoint();
				if (endpoint.getType() == ContactType.SOURCE) {
					return connection.getSchedule().getBlockMask();
				}
			}
			return null;
		}

		/**
		 * Returns true, if the node is a head node. i.e. the node receives a
		 * flow directly from the server.
		 * 
		 * @return
		 */
		public boolean isHead() {
			for (TransitConnection connection : connections) {
				TransitContact endpoint = connection.getEndpoint();
				if (endpoint.getType() == ContactType.SOURCE) {
					return true;
				}
			}
			return false;
		}

		public void addConnection(TransitConnection connection) {
			assert !connections.contains(connection);
			connections.add(connection);
			connection.addScheduleListener(this);
			assert blockMask != null;
		}

		/**
		 * Returns true, if this flow is currently healthy, i.e. it receives all
		 * needed live-packets for the given request and can therefore be used
		 * to help other players.
		 * 
		 * @return
		 */
		public boolean isHealthy(BitSet request) {
			if (!isHealthy && request != null) {
				/*
				 * Check mask
				 */
				BitSet check = (BitSet) request.clone();
				check.and(getMissingFlow());
				return check.isEmpty();
			}
			return isHealthy;
		}

		/**
		 * Returns a BitSet with the flows that are currently healthy.
		 * 
		 * @return
		 */
		public BitSet getHealthyFlow() {
			BitSet healthy = (BitSet) blockMask.clone();
			healthy.andNot(getMissingFlow());
			return healthy;
		}

		/**
		 * Number of active incoming flows
		 * 
		 * @return
		 */
		public int getNumberOfFlows() {
			return connections.size();
		}

		public void switchedLayer() {
			blockMask = getNode().getVideo().getLayerMask(getNode().getLayer());
		}

		public void newBlockArrived(TransitConnection connection, int blocknum,
				TransitReplyHandler replyTo) {
			if (!connections.contains(connection)) {
				return;
			}
			int chunk = blocknum / getNode().BLOCKS_PER_CHUNK;
			lastChunkPerFlow.put(connection, chunk);
			lastChunkTimestampPerFlow.put(connection,
 Time.getCurrentTime());
			highestLiveChunk = Math.max(chunk, highestLiveChunk);
		}

		public void tick(int chunk) {
			if (blockMask == null) {
				return;
			}

			if (!isHealthy) {

				/*
				 * This will request missing connections to create enough flows
				 * for the requested quality.
				 */
				BitSet missingFlow = getMissingFlow();
				handleMissingFlow(missingFlow);
			}

			/*
			 * Check chunk variance from live schedules
			 */
			List<TransitConnection> toCancel = new LinkedList<TransitConnection>();
			for (TransitConnection flow : connections) {
				Integer lastChunkInThisInterval = lastChunkPerFlow.get(flow);
				if (lastChunkInThisInterval != null) {
					if (lastChunkInThisInterval
							+ getNode().getSettings().getParam(
									TransitParams.FLOW_MAX_LIVE_CHUNK_VARIANCE) < highestLiveChunk) {
						/*
						 * Flow is to slow when compared to other flows, switch
						 * whole flow? Notify children? According to Bram Cohen:
						 * do NOT notify children
						 */
						if (DEBUG) {
							System.out
									.println("TFM: CANCEL because of liveChunkVariance "
											+ flow.toString());
						}
						toCancel.add(flow);
						continue;
					} else {
						/*
						 * Teardown of connections with high RTT (may go
						 * unnoticed in the below block if there is only one
						 * connection)
						 */
						if (flow.getCurrentRTTEstimation() > 1 * Time.SECOND) {
							/*
							 * RTT for this connection is dangerously high.
							 * Teardown!
							 */
							if (DEBUG) {
								System.out
										.println("TFM: CANCEL because of RTT "
												+ flow.toString());
							}
							toCancel.add(flow);
							continue;
						}

						Integer lastChunkInPreviousInterval = lastChunkTimestampPerFlowLastRound
								.get(flow);
						if (lastChunkInPreviousInterval != null
								&& lastChunkInPreviousInterval == lastChunkInThisInterval) {
							/*
							 * Evaluate connection performance (has another
							 * block arrived within the last chunk?)
							 */
							int roundsWithoutChanges = roundsWithoutChunkChanges
									.get(flow);
							roundsWithoutChanges++;
							roundsWithoutChunkChanges.put(flow,
									roundsWithoutChanges);

							if (roundsWithoutChanges > getNode()
									.getSettings()
									.getParam(
											TransitParams.FLOW_MAX_LIVE_CHUNK_DELAY)) {
								if (DEBUG) {
									System.out
											.println("TFM: CANCEL because of liveChunkDelay "
													+ flow.toString());
								}
								toCancel.add(flow);
								continue;
							} else {
								continue;
							}

						} else if (lastChunkInPreviousInterval == null
								|| lastChunkInPreviousInterval != lastChunkInThisInterval) {
							lastChunkTimestampPerFlowLastRound.put(flow,
									lastChunkInThisInterval);
							roundsWithoutChunkChanges.put(flow, 0);
							continue;
						} else {
							throw new AssertionError();
						}
					}
				} else {
					/*
					 * Maybe no block arrived at all?
					 */
					if (!roundsWithoutChunkChanges.containsKey(flow)) {
						roundsWithoutChunkChanges.put(flow, 0);
					}
					int roundsWithoutChanges = roundsWithoutChunkChanges
							.get(flow);
					roundsWithoutChanges++;
					roundsWithoutChunkChanges.put(flow, roundsWithoutChanges);
					if (roundsWithoutChanges > getNode().getSettings()
							.getParam(TransitParams.FLOW_MAX_LIVE_CHUNK_DELAY)) {
						if (DEBUG) {
							System.out
									.println("TFM: CANCEL because no block arrived at all "
											+ flow.toString());
						}
						toCancel.add(flow);
					}
				}
			}

			/*
			 * Connections to cancel
			 */
			BitSet missingMask = new BitSet();
			for (TransitConnection conToCancel : toCancel) {
				missingMask.or(conToCancel.getSchedule().getBlockMask());
				doCancel(conToCancel);
			}
		}

		private BitSet getMissingFlow() {
			BitSet missingBlocks = (BitSet) blockMask.clone();
			for (TransitConnection connection : connections) {
				TransitFlowSchedule schedule = connection.getSchedule();
				if (connection.getScheduleState() == ScheduleState.ACTIVE) {
					for (int i = missingBlocks.nextSetBit(0); i >= 0; i = missingBlocks
							.nextSetBit(i + 1)) {
						missingBlocks.set(i, !schedule.getBlockMask().get(i));
					}
				}
			}
			return missingBlocks;
		}

		@Override
		public boolean onScheduleChanged(TransitConnection connection,
				TransitFlowSchedule oldSchedule, TransitFlowSchedule newSchedule) {
			isHealthy = getMissingFlow().cardinality() == 0;
			return true;
		}

		@Override
		public boolean onScheduleStateChanged(TransitConnection connection,
				ScheduleState oldState, ScheduleState newState) {
			isHealthy = getMissingFlow().cardinality() == 0;
			if (newState == ScheduleState.CANCELED
					|| newState == ScheduleState.INACTIVE) {
				connections.remove(connection);
				lastChunkPerFlow.remove(connection);
				lastChunkTimestampPerFlow.remove(connection);
				lastChunkTimestampPerFlowLastRound.remove(connection);

				BitSet bitSet = connection.getSchedule().getBlockMask();
				for (int i = bitSet.nextSetBit(0); i != -1; i = bitSet
						.nextSetBit(i + 1)) {
					depth[i] = -1;
				}

				return false;
			}
			if (newState == ScheduleState.ACTIVE) {
				/*
				 * Check for duplicates that may arise from the selected set of
				 * flows.
				 */
				BitSet duplicates = new BitSet();
				for (TransitConnection flow : connections) {
					if (flow.equals(connection)) {
						continue;
					}
					assert flow.getState() == ConnectionState.OPEN;
					assert flow.getScheduleState() == ScheduleState.ACTIVE;
					int layer = getNode().getOptimizationManager().getLayer(
							connection);
					if (layer == -1) {
						duplicates.or(flow.getSchedule().getBlockMask());
					} else {
						if (getNode().getOptimizationManager()
								.acceptsDoubleParents(layer)) {
							if (getNode().getOptimizationManager()
									.getNewParent(layer) != connection) {
								duplicates
										.or(flow.getSchedule().getBlockMask());
							}
						} else {
							duplicates.or(flow.getSchedule().getBlockMask());
						}

					}
				}

				duplicates.and(connection.getSchedule().getBlockMask());
				if (!duplicates.isEmpty()) {
					BitSet request = (BitSet) connection.getSchedule()
							.getBlockMask().clone();
					request.andNot(duplicates);
					if (request.isEmpty()) {
						doCancel(connection);
					} else {
						doAlterSchedule(connection, new TransitFlowSchedule(
								request));
					}
				}
			}
			return true;
		}

		@Override
		public String toString() {
			return connections.toString();
		}

	}

	/**
	 * Container for outgoing connections and their Live-schedules.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 28.06.2012
	 */
	public class OutgoingFlow implements TransitFlowScheduleListener {

		private final List<TransitConnection> connections;

		public OutgoingFlow() {
			this.connections = new LinkedList<TransitConnection>();
		}

		public void addConnection(TransitConnection connection) {
			if (!connections.contains(connection)) {
				connections.add(connection);
				connection.addScheduleListener(this);
			}
		}

		public boolean hasOutgoingFlow(TransitContact contact) {
			for (TransitConnection connection : connections) {
				if (connection.getEndpoint().getNodeID()
						.equals(contact.getNodeID())) {
					return true;
				}
			}
			return false;
		}

		public int[] getNumberOfChildren() {
			int[] numberOfChildren = new int[4];
			for (TransitConnection connection : connections) {
				BitSet schedule = connection.getSchedule().getBlockMask();
				for (int i = schedule.nextSetBit(0); i != -1; i = schedule
						.nextSetBit(i + 1)) {
					numberOfChildren[i]++;
				}
			}
			return numberOfChildren;
		}

		public boolean isChild(TransitContact node, int layerIndex) {
			for (TransitConnection connection : connections) {
				BitSet schedule = connection.getSchedule().getBlockMask();
				if (schedule.get(layerIndex)) {
					return true;
				}
			}
			return false;
		}

		public void newBlockAvailable(TransitBlockMessage blockMessage) {
			assert blockMessage.isLiveBlock();
			for (TransitConnection connection : connections) {
				if (matchesSchedule(blockMessage.getBlockNumber(),
						connection.getSchedule())) {
					assert connection.getState() == ConnectionState.OPEN;
					assert connection.getScheduleState() == ScheduleState.ACTIVE;
					forwardLiveBlock(blockMessage, connection);
				}
			}
		}

		/**
		 * 
		 * @param chunk
		 */
		public void tick(int chunk) {
			List<TransitConnection> toCancel = new LinkedList<TransitConnection>();
			for (TransitConnection flow : connections) {
				if (flow.getCurrentRTTEstimation() > 1 * Time.SECOND) {
					/*
					 * RTT for this connection is dangerously high. Teardown!
					 */
					throw new AssertionError(
							"Should never happen, as the CCOP should take care of that!");
					// toCancel.add(flow);
					// continue;
				}
			}

			/*
			 * Connections to cancel
			 */
			for (TransitConnection conToCancel : toCancel) {
				doDenySchedule(conToCancel);
			}
		}

		public void switchedLayer() {
			/*
			 * TODO send ALTER if child-schedules can no longer be fulfilled
			 */
		}

		@Override
		public boolean onScheduleChanged(TransitConnection connection,
				TransitFlowSchedule oldSchedule, TransitFlowSchedule newSchedule) {
			return true;
		}

		@Override
		public boolean onScheduleStateChanged(TransitConnection connection,
				ScheduleState oldState, ScheduleState newState) {
			if (newState == ScheduleState.CANCELED
					|| newState == ScheduleState.INACTIVE) {
				connections.remove(connection);
				return false;
			}
			return true;
		}

		@Override
		public String toString() {
			return connections.toString();
		}

	}

	public class ExchangeNodeInfo extends PeriodicPiggybackListener {
		public ExchangeNodeInfo(long interval) {
			super(interval);
		}

		@Override
		protected TransitMessage whatToPiggybackPeriodically(TransitContact to) {

			NodeInfoMessage nodeInfo = getNodeInfo();
			return nodeInfo;
		}
	}

}
