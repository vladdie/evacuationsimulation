/*
 * Copyright (c) 2005-2010 KOM ‚Äì Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.StreamingDocument;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact;

/**
 * This is the reply a tracker sends when receiving a {@link JoinTrackerMessage}
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 29.05.2012
 */
public class JoinTrackerReplyMessage extends AbstractTransitMessage implements TransitOfferNeighborhoodMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3804130614398484964L;

	private final TransitContact tracker;

	private final StreamingDocument streamingDocument;

	private final List<TransitContact> initialNeighbors;

	public JoinTrackerReplyMessage(TransitContact sender, TransitContact tracker, StreamingDocument streamingDocument,
	        List<TransitContact> initialNeighbors) {
		super(sender);
		this.tracker = tracker;
		this.streamingDocument = streamingDocument;
		this.initialNeighbors = initialNeighbors;
	}

	/*
	 * TODO what to add to this reply?
	 */

	@Override
	public List<TransitContact> getNeighbors() {
		return initialNeighbors;
	}

	/**
	 * Lateron, one might want to support multiple trackers, where each tracker
	 * is able to specify another tracker as the tracker that has to be joined.
	 * Therefore, we add this method and do not just rely on getSender.
	 * 
	 * @return
	 */
	public TransitContact getTrackerContact() {
		return tracker;
	}

	/**
	 * The document that is to be streamed (this is just a meta-container used
	 * in the application). We assume that it has 50 byte payload size, as it
	 * must at least contain some kind of ID and the current playback position.
	 * 
	 * @return
	 */
	public StreamingDocument getStreamingDocument() {
		return streamingDocument;
	}

	@Override
	public long getSize() {
		/*
		 * 50 byte payload in terms of the streamingDocument-Meta-Container.
		 * This is NOT the full video file, therefore getSize would not work!
		 */
		return super.getSize() + tracker.getTransmissionSize() + 50;
	}

	@Override
	public String toString() {
		return "JOIN REPLY neighbors:" + getNeighbors().toString();
	}

}
