/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.models;

import java.util.BitSet;
import java.util.Map;

import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;

/**
 * This estimator allows a node to provide information about chunk
 * availabilities. This masks some functionality of the PeerManager and is used
 * for layer switching decisions.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 30.05.2012
 */
public interface AvailabilityEstimator {

	/*
	 * TODO
	 */

	/**
	 * This has to return a BitSet of available blocks for each neighbor.
	 * 
	 * @return
	 */
	public Map<UniqueID, BitSet> getAvailableBlocksByAllNeighbors();

}
