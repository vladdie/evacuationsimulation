/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.common.UniqueID;
import de.tudarmstadt.maki.simonstrator.api.operation.Operation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.PeriodicPiggybackListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.ConnectionMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.ConnectionMessage.ConnectionMessageType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.NodeInfoMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.OfferNeighborhoodMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SetConnectionDirectionMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SetConnectionDirectionMessageReply;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.SetConnectionDirectionMessageReply.Response;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.messages.TransitMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.RequestState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.operations.ConnectionControlOperation;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.operations.SwitchConnectionDirectionOperation;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * This component manages connections to a subset of all contacts that are
 * available in our routing table. The number of concurrent connections depends
 * on the state of the node and the demands of the scheduler.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 10.06.2012
 */
public class TransitConnectionManager implements TransitConnectionListener,
		OperationCallback<TransitConnection> {

	/**
	 * Container for all connections
	 */
	private Map<UniqueID, TransitConnection> allConnections = new LinkedHashMap<UniqueID, TransitConnection>();

	private CopyOnWriteArrayList<TransitConnection> allConnectionsThreadsafe = new CopyOnWriteArrayList<TransitConnection>();

	/**
	 * Reserved connections, e.g. in order to guarantee, that no connection is
	 * open if we need one.
	 */
	private Map<ConnectionDirection, Boolean> reservedConnections = new HashMap<ConnectionDirection, Boolean>();

	/**
	 * Endpoint ID to ConnectionControlOperation
	 */
	private Map<UniqueID, ConnectionControlOperation> ccops = new LinkedHashMap<UniqueID, ConnectionControlOperation>();

	private TransitNode node;
	
	private TransitContact trackerContact;

	private List<TransitConnectionListener> connectionListeners = new LinkedList<TransitConnectionListener>();

	private final TimeoutSet<TransitContact> justClosed = new TimeoutSet<TransitContact>(
			Time.MINUTE);

	private final Comparator<TransitConnection> COMP_LASTBLOCK_NEWEST_FIRST = new Comparator<TransitConnection>() {
		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			return (int) Math.signum(o2.getLastBlockTransferredTimestamp()
					- o1.getLastBlockTransferredTimestamp());
		}
	};

	private final Comparator<TransitConnection> COMP_LOAD = new Comparator<TransitConnection>() {
		@Override
		public int compare(TransitConnection o1, TransitConnection o2) {
			return o1.getEndpoint().getLoad() - o2.getEndpoint().getLoad();
		}
	};

	/**
	 * Create the connection manager as part of the
	 * {@link TransitNeighborhoodManager}
	 * 
	 * @param node
	 */
	public TransitConnectionManager(TransitNode node) {
		this.node = node;
		reservedConnections.put(ConnectionDirection.IN, false);
		reservedConnections.put(ConnectionDirection.OUT, false);
		
		node.getMessageHandler().addPiggybackListener(
				new ExchangeNeighborhood(node.getSettings().getTime(
						TransitTimes.NEIGHBORHOOD_EXCHANGE_INTERVAL)));
	}

	/**
	 * The node
	 * 
	 * @return
	 */
	protected TransitNode getNode() {
		return node;
	}

	/**
	 * Negotiates a new connection with the given contact.
	 * 
	 * @param to
	 */
	private void openIncomingConnection(TransitContact to) {

		ConnectionControlOperation cop = ccops.get(to.getNodeID());

		if (cop == null || cop.isStopped()) {
			TransitConnection con = new TransitConnection(
					node.getLocalOverlayContact(), to, ConnectionDirection.IN,
					this);
			assert !allConnections.containsKey(to.getNodeID());
			allConnections.put(to.getNodeID(), con);
			allConnectionsThreadsafe.add(con);
			cop = new ConnectionControlOperation(node, this, con);
			ccops.put(to.getNodeID(), cop);
			// cop.startWithDelay(allConnections.size() * 50
			// * Time.MILLISECOND);
			cop.start();
			cop.doOpenConnection();
		} else {
			throw new AssertionError();
		}
	}

	/**
	 * Dispatcher for incoming {@link ConnectionMessage}s. These messages are
	 * forwarded to the correct {@link ConnectionControlOperation}.
	 * 
	 * @param msg
	 */
	public void receivedConnectionMessage(ConnectionMessage msg,
			TransitReplyHandler replyTo) {
		TransitContact sender = msg.getSenderContact();
		ConnectionControlOperation cop = ccops.get(sender.getNodeID());
		if (cop == null || cop.isStopped()) {
			if (msg.getMessageType() == ConnectionMessageType.CLOSE
					|| msg.getMessageType() == ConnectionMessageType.PING) {
				return;
			}
			assert msg.getMessageType() == ConnectionMessageType.OPEN;
			assert !allConnections.containsKey(sender.getNodeID());
			TransitConnection con = new TransitConnection(
					node.getLocalOverlayContact(), sender,
					ConnectionDirection.OUT, this);
			allConnections.put(sender.getNodeID(), con);
			allConnectionsThreadsafe.add(con);
			cop = new ConnectionControlOperation(node, this, con);
			ccops.put(sender.getNodeID(), cop);
			cop.start();
		}
		cop.receive(msg, replyTo);
	}
	
	
	public void setTrackerContact(TransitContact trackerContact) {
		this.trackerContact = trackerContact;
	}
	
	public TransitContact getTrackerContact() {
		return trackerContact;
	}

	public void receivedSetConnectionDirectionMsg(
			SetConnectionDirectionMessage msg, TransitReplyHandler replyTo) {

		TransitContact sender = msg.getSenderContact();
		ConnectionControlOperation cop = ccops.get(sender.getNodeID());

		if (cop == null || cop.isStopped()) {
			SetConnectionDirectionMessageReply deny = new SetConnectionDirectionMessageReply(
					node.getLocalOverlayContact(),
					Response.CANNOT_CHANGE_DIRECTION);
			replyTo.reply(deny);
			return;
		}
		
		TransitConnection connectionToSwitch = cop.getConnection();
		ConnectionDirection switchToDirection = msg.getSwitchToDirection();
		
		/* Check if the connection is reserved by the optimization manager. */
		if (node.getOptimizationManager().isReserved(connectionToSwitch)) {
			SetConnectionDirectionMessageReply deny = new SetConnectionDirectionMessageReply(
					node.getLocalOverlayContact(),
					Response.CANNOT_CHANGE_DIRECTION);
			replyTo.reply(deny);
			return;
		}
		
		/* Check if we can open a connection in the new direction. */
		if (!mayOpenNewConnection(switchToDirection)) {
			/*
			 * Since no capacity left: check if we can cut an unimportant
			 * connection.
			 */
			boolean cut = cutUnimportantConnection(switchToDirection);
			if (!cut) {
				/* We give up. Cannot switch direction. */
				SetConnectionDirectionMessageReply deny = new SetConnectionDirectionMessageReply(
						node.getLocalOverlayContact(),
						Response.CANNOT_CHANGE_DIRECTION);
				replyTo.reply(deny);
				return;
			}
		}
		
		/* We can switch direction. */
		connectionToSwitch.setDirection(switchToDirection);
		
		SetConnectionDirectionMessageReply ok = new SetConnectionDirectionMessageReply(
				node.getLocalOverlayContact(),
				Response.OK);
		replyTo.reply(ok);
		

	}

	/**
	 * Called for every payload message (video block), checks if the connection
	 * is open at all or if the source might have missed a close-message
	 * 
	 * @param from
	 */
	public void receivedPayloadMessage(TransitContact from) {
		TransitConnection co = getConnection(from);
		if (co == null) {
			return;
		}
		if (co.getState() != ConnectionState.OPEN) {
			ccops.get(from.getNodeID()).closeConnection();
		}
	}

	/**
	 * Updates the RTT-Estimation of the given connection
	 * 
	 * @param newRtt
	 */
	public void updateRttEstimation(TransitContact from, long rtt) {
		if (rtt > 0) {
			ConnectionControlOperation cop = ccops.get(from.getNodeID());
			if (cop != null) {
				cop.updateRttEstimation(rtt);
				cop.getConnection().getEndpoint().update(from);
			}
		}
	}
	
	public void changeDirection(TransitConnection connection) {
		SwitchConnectionDirectionOperation op = new SwitchConnectionDirectionOperation(node, connection, null);
		op.scheduleImmediately();
	}

	@Override
	public void calledOperationFailed(Operation<TransitConnection> op) {
		throw new AssertionError("This operation is not allowed to fail!");
	}

	@Override
	public void calledOperationSucceeded(Operation<TransitConnection> op) {
		if (op instanceof ConnectionControlOperation) {
			ConnectionControlOperation cop = (ConnectionControlOperation) op;
			if (cop.isStopped()) {
				/*
				 * Removes the connection from the list of all connections.
				 */
				TransitConnection connection = allConnections
						.remove(((ConnectionControlOperation) op)
								.getConnection().getEndpoint().getNodeID());
				assert connection != null;
				allConnectionsThreadsafe.remove(connection);
				justClosed.addNow(connection.getEndpoint());
				ccops.remove(connection.getEndpoint().getNodeID());
			}
		}
	}

	/**
	 * Returns a connection object for the given contact, can be used to
	 * aggregate bandwidth statistics etc. inside the {@link TransitConnection}
	 * 
	 * @param to
	 * @return a {@link TransitConnection} or null, if the contact is not
	 *         connected to this node
	 */
	public TransitConnection getConnection(TransitContact to) {
		return allConnections.get(to.getNodeID());
	}

	/**
	 * Generic way to query for a type of connections. Any combination of
	 * arguments can be null to not filter for this state.
	 * 
	 * @param direction
	 * @param connectionState
	 * @param scheduleState
	 * @param requestState
	 * @return
	 */
	public List<TransitConnection> getConnections(
			ConnectionDirection direction, ConnectionState connectionState,
			ScheduleState scheduleState, RequestState requestState) {
		LinkedList<TransitConnection> result = new LinkedList<TransitConnection>();
		for (TransitConnection connection : allConnections.values()) {
			if ((connectionState == null || connection.getState() == connectionState)
					&& (direction == null || connection.getDirection() == direction)
					&& (scheduleState == null || connection.getScheduleState() == scheduleState)
					&& (requestState == null || connection.getRequestState() == requestState)) {
				result.add(connection);
			}
		}
		return result;
	}

	/**
	 * Shortcut if you need to access all connections
	 * 
	 * @return
	 */
	public List<TransitConnection> getAllConnections() {
		return allConnectionsThreadsafe;
	}

	private boolean mayReserve(ConnectionDirection direction) {
		return reservedConnections.get(direction).booleanValue() == false;
	}

	public boolean reserve(ConnectionDirection direction) {
		if (!mayReserve(direction)) {
			return false;
		}

		reservedConnections.put(direction, true);
		return true;
	}

	public void unreserve(ConnectionDirection direction) {
		reservedConnections.put(direction, false);
	}

	/**
	 * Called by the {@link ConnectionControlOperation} on an incoming
	 * open-request.
	 * 
	 * @param cop
	 * @return
	 */
	public boolean mayOpen(ConnectionControlOperation cop) {
		assert cop.getResult().getDirection() == ConnectionDirection.OUT;
		int currentConnections = getConnections(ConnectionDirection.OUT,
				ConnectionState.OPEN, null, null).size();
		
		/* Is there a reserved connection in this direction ? */
		int reserved = 0;
		if (reservedConnections.get(ConnectionDirection.OUT)) {
			reserved = 1;
		}
		
		if (currentConnections + reserved >= node.getSettings().getParam(
				TransitParams.CONNECTION_MAX_OUTGOING)) {
			return false;
		}
		return true;
	}

	/**
	 * Checks if a new connection in the given direction may be opened.
	 * 
	 * @param direction
	 * @return
	 */
	public boolean mayOpenNewConnection(ConnectionDirection direction) {
		int currentConnections = getConnections(direction,
				ConnectionState.OPEN, null, null).size();
		
		/* Is there a reserved connection in this direction ? */
		int reserved = 0;
		if (reservedConnections.get(direction)) {
			reserved = 1;
		}
		
		if (currentConnections + reserved >= node
				.getSettings()
				.getParam(
						(direction == ConnectionDirection.OUT ? TransitParams.CONNECTION_MAX_OUTGOING
								: TransitParams.CONNECTION_MAX_INCOMING))) {
			return false;
		}
		return true;
	}
	
	public void closeConnection(TransitContact to) {
		ConnectionControlOperation cop = ccops.get(to.getNodeID());
		if (cop!=null && !cop.isStopped()) {
			cop.closeConnection();
		}
	}

	/**
	 * Cuts a connection in the given direction if it is unimportant. Currently,
	 * an unimportant connection is a connection which does not participate in a
	 * flow or in a request and from its last transferred block timestamp is
	 * oldest.
	 * 
	 * @param direction
	 * @return true, if a connection was cut, else false.
	 */
	public boolean cutUnimportantConnection(ConnectionDirection direction) {
		List<TransitConnection> connections = getConnections(direction,
				ConnectionState.OPEN, null, null);
		long lastBlockTransferTimestamp = Long.MAX_VALUE;
		TransitConnection connectionToCut = null;

		for (TransitConnection conn : connections) {
			ScheduleState scheduleState = conn.getScheduleState();
			RequestState reqState = conn.getRequestState();
			if (scheduleState == ScheduleState.ACTIVE
					|| scheduleState == ScheduleState.NEGOTIATING
					|| reqState == RequestState.ACTIVE) {
				/* Do not cut this connection */
				continue;
			}
			if (conn.getLastBlockTransferredTimestamp() < lastBlockTransferTimestamp) {
				lastBlockTransferTimestamp = conn
						.getLastBlockTransferredTimestamp();
				connectionToCut = conn;
			}
		}

		/* We found a connection to cut */
		if (connectionToCut != null) {
			/* Cut connection */
			ccops.get(connectionToCut.getEndpoint().getNodeID())
					.closeConnection();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Called by the MessageHandler / NeighborhoodManager as soon as a timeout
	 * is detected
	 * 
	 * @param contact
	 */
	public void timeout(TransitContact contact) {
		ConnectionControlOperation cop = ccops.get(contact.getNodeID());
		if (cop != null) {
			cop.handleTimeout();
		}
	}

	/**
	 * This method is called by the neighborhood manager and opens connections
	 * to some or all of the new contacts provided by the neighborhood manager.
	 * 
	 * @param newContacts
	 */
	public void onNewNeighbors(List<TransitContact> newContacts) {
		assert !getNode().isServer();

		for (TransitContact contact : newContacts) {
			assert !contact.equals(node.getLocalOverlayContact());
			assert getConnection(contact) == null;
		}
		newContacts.removeAll(justClosed.getUnmodifiableSet());

		List<TransitConnection> openConnections = getConnections(
				ConnectionDirection.IN, ConnectionState.OPEN, null, null);
		int opening = getConnections(ConnectionDirection.IN,
				ConnectionState.NEGOTIATING, null, null).size();

		Collections.sort(openConnections, COMP_LASTBLOCK_NEWEST_FIRST);

		long time = Time.getCurrentTime();
		int passive = 0;
		int active = 0;
		for (TransitConnection connection : openConnections) {
			if (Math.max(connection.getLastBlockTransferredTimestamp(),
					connection.getLastStateChangeTimestamp())
					+ node.getSettings().getTime(
							TransitTimes.CONNECTION_INACTIVITY_TIMEOUT) < time) {
				passive++;
			} else {
				active++;
			}
		}

		/*
		 * Close passive connection to make room for new contacts, if there are
		 * too many of them
		 */
		int i = 0;
		while (passive > node.getSettings().getParam(
				TransitParams.CONNECTION_MAX_INCOMING_INACTIVE)) {
			TransitConnection toClose = openConnections.get(openConnections
					.size() - 1 - i);
			ccops.get(toClose.getEndpoint().getNodeID()).closeConnection();
			i++;
			passive--;
		}

		/*
		 * Close the oldest passive connection with a probability of
		 * CONNECTION_INACTIVITY_KILL_PROBABILITY if no connection has already
		 * been closed
		 */
		if (i == 0
				&& passive > 0
				&& passive <= node.getSettings().getParam(
						TransitParams.CONNECTION_MAX_INCOMING_INACTIVE)) {
			int rnd = Randoms.getRandom(this).nextInt(100);
			if (rnd < node.getSettings().getParam(
					TransitParams.CONNECTION_INACTIVITY_KILL_PROBABILITY)) {
				TransitConnection toKill = openConnections.get(openConnections
						.size() - 1);
				ccops.get(toKill.getEndpoint().getNodeID())
						.closeConnection();
				passive--;
			}
		}

		/*
		 * Open new connections
		 */
		int extraBuffer = (active == 0 ? Math.max(0, 5 - passive) : 0);
		while (opening + passive + active < node.getSettings().getParam(
				TransitParams.CONNECTION_MAX_INCOMING)
				+ extraBuffer) {
			if (newContacts.isEmpty()) {
				break;
			}
			if (opening + passive + active >= node.getSettings().getParam(
					TransitParams.CONNECTION_MAX_INCOMING)) {
				break;
			}
			TransitContact to = newContacts.remove(0);
			openIncomingConnection(to);
			opening++;
		}
	}

	@Override
	public void onConnectionStateChanged(TransitConnection connection,
			ConnectionState oldState, ConnectionState newState) {

		assert (oldState == ConnectionState.CLOSING && newState == ConnectionState.CLOSED)
				|| oldState != ConnectionState.CLOSING;

		/*
		 * Dispatch to all listeners
		 */
		for (TransitConnectionListener listener : connectionListeners) {
			listener.onConnectionStateChanged(connection, oldState, newState);
		}

		if (connection.getDirection() == ConnectionDirection.IN
				&& oldState == ConnectionState.NEGOTIATING) {
			/*
			 * If we run out of open connections, use the spare-list
			 */
			int open = getConnections(ConnectionDirection.IN,
					ConnectionState.OPEN, null, null).size();
			if (open < node.getSettings().getParam(
					TransitParams.CONNECTION_MIN_INCOMING)) {
				node.getNeighborhood().requestNeighbors();
			}
		}

	}

	/**
	 * Adds a listener that is informed if connections change their state
	 * 
	 * @param listener
	 */
	public void addConnectionListener(TransitConnectionListener listener) {
		connectionListeners.add(listener);
	}

	/**
	 * Removes the specified listener from the ConnectionManager
	 * 
	 * @param listener
	 */
	public void removeConnectionListener(TransitConnectionListener listener) {
		connectionListeners.remove(listener);
	}

	/**
	 * A list of neighbors that can be sent to other nodes.
	 * 
	 * @return
	 */
	public List<TransitContact> getRandomNeighborsFor(TransitContact to) {
		List<TransitContact> neighbors = new LinkedList<TransitContact>();
		List<TransitConnection> candidates = new LinkedList<TransitConnection>(
				getConnections(null, ConnectionState.OPEN, null, null));

		if (!candidates.isEmpty()) {
			// Add some randomness
			// Collections.rotate(candidates,
			// Time.getRandom().nextInt(candidates.size()));
			Collections.sort(candidates, COMP_LOAD);

			int in = 0;
			for (TransitConnection candidate : candidates) {
				if (candidate.getEndpoint().equals(to)) {
					continue;
				}
				if (in > node.getSettings().getParam(
						TransitParams.CONNECTION_MAX_INCOMING_INACTIVE) * 2) {
					break;
				}
				neighbors.add(candidate.getEndpoint());
				in++;
			}
		}
		return neighbors;
	}

	public void receivedNodeInfo(NodeInfoMessage msg) {
		if (msg.getSenderContact()==null) {
			System.out.println("[" + getNode().getHost().getId()
					+ "]: Sender contact=null! Msg:" + msg);
			return;
		}
		if (getConnection(msg.getSenderContact())==null) {
			System.out.println("[" + getNode().getHost().getId()
					+ "]: Connection to " + msg.getSenderContact()
					+ "= null! Msg: " + msg);
			return;
		}
		
		if (msg.getNodeInfo()==null) {
			System.out.println("[" + getNode().getHost().getId()
					+ "]: NodeInfo =null! Msg:" + msg);
			return;
		}
		
		TransitConnection connection = getConnection(msg.getSenderContact());

		connection.setNodeInfo(msg.getNodeInfo());
	}

	/**
	 * Periodically piggybacks the current neighborhood
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 11.09.2012
	 */
	public class ExchangeNeighborhood extends PeriodicPiggybackListener {

		public ExchangeNeighborhood(long interval) {
			super(interval);
		}

		@Override
		protected TransitMessage whatToPiggybackPeriodically(TransitContact to) {
			List<TransitContact> neighbors = getRandomNeighborsFor(to);
			if (!neighbors.isEmpty()) {
				return new OfferNeighborhoodMessage(getNode()
						.getLocalOverlayContact(), neighbors);
			}

			return null;
		}

	}

}
