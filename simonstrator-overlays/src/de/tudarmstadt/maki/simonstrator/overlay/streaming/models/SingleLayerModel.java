package de.tudarmstadt.maki.simonstrator.overlay.streaming.models;

import java.io.Serializable;
import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.overlay.streaming.VideoModel;

public class SingleLayerModel implements VideoModel, Serializable {

	private static final long serialVersionUID = 1L;

	private static byte[] baseLayer = { 0, 0, 0 };
	private static BitSet baseLayerBitSet;
	static {
		baseLayerBitSet = new BitSet(1);
		baseLayerBitSet.set(0);
	}

	private final int chunkSize;

	/**
	 * @param chunkSize
	 *            in bytes
	 */
	public SingleLayerModel(int chunkSize) {
		this.chunkSize = chunkSize;
	}

	@Override
	public long getVideoFileSize() {
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public int getNumOfChunks() {
		return Integer.MAX_VALUE;
	}

	@Override
	public int getNumOfBlocksPerChunk() {
		return 1;
	}

	@Override
	public int getBlockSize(int blockNum) {
		return chunkSize;
	}

	@Override
	public byte getTemporalLevels() {
		return 1;
	}

	@Override
	public byte getSpatialLevels() {
		return 1;
	}

	@Override
	public byte getSnrLevels() {
		return 1;
	}

	@Override
	public byte[] getBlockLayer(int blockNum) {
		return baseLayer;
	}

	@Override
	public BitSet getLayerMask(byte[] layer) {
		return baseLayerBitSet;
	}

	@Override
	public Byte getLayerIndex(byte[] layer) {
		return 0;
	}

	@Override
	public double getLayerByteRate(int d, int t, int q) {
		// Used for IQA layer decision, not needed for single layer
		return 0;
	}

	@Override
	public int getLayerComplexity(int d, int t, int q) {
		// Used for IQA layer decision, not needed for single layer
		return 0;
	}

}
