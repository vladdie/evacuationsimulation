/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.optimization;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitNodeInfo;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.TransitOptimizationManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnection.ScheduleState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitConnectionManager;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.neighborhood.TransitContact;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.TransitFlowSchedule;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transitpp.scheduling.simple.TransitSimpleFlowManager;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

/**
 * High degree preemption optimization as defined in Lidanski MA.
 * @author Eduardo Lidanski
 * @version 1.0, 14.04.2014
 */
public class HighDegreePreemptionOptimization implements TopologyOptimization {

	private TransitNode node;

	private TransitConnectionManager connectionManager;

	private TransitSimpleFlowManager flowManager;

	/* The chosen optimization, i.e. the best possible optimization. */
	private PossibleHighDegreePreemptionOptimization chosen;

	private TransitOptimizationManager optimizationManager;

	/* Layer -> Last tried connections */
	private Map<Integer, TimeoutSet<TransitConnection>> lastTried = new HashMap<Integer, TimeoutSet<TransitConnection>>();

	private Map<Integer, TimeoutSet<TransitContact>> lastTriedParentsOfNeighbors = new HashMap<Integer, TimeoutSet<TransitContact>>();

	private int gainThreshold = 1;

	/* Which trees shall we optimize */
	private Set<Integer> optimizeTrees = new HashSet<Integer>();

	/*
	 * Which was the last tree optimized? We need this in order to not only
	 * optimize one tree, but all of them, in a Round-Robin way.
	 */
	private int lastOptimizedTree = -1;

	public HighDegreePreemptionOptimization(TransitNode node) {
		this.node = node;

		for (int i = 0; i <= 3; i++) {
			lastTried.put(i, new TimeoutSet<TransitConnection>(2 * Time.MINUTE));
			lastTriedParentsOfNeighbors.put(i, new TimeoutSet<TransitContact>(
					6 * Time.SECOND));
		}
		/*
		 * Define trees to be optimized here
		 * 
		 * FIXME[JR]: Should be changed to dynamic detection of available trees
		 * and use all of them by default.
		 */
		optimizeTrees.add(0);
		optimizeTrees.add(1);
		optimizeTrees.add(2);
		optimizeTrees.add(3);
	}

	@Override
	public boolean canOptimize() {

		this.optimizationManager = node.getOptimizationManager();

		this.connectionManager = node.getNeighborhood().getConnectionManager();
		this.flowManager = (TransitSimpleFlowManager) node.getScheduler()
				.getFlowManager();

		List<TransitConnection> neighbors = connectionManager.getConnections(
				null, ConnectionState.OPEN, null, null);
		List<PossibleHighDegreePreemptionOptimization> possibleOptimizations = new ArrayList<PossibleHighDegreePreemptionOptimization>();

		int[] myDepths = flowManager.getDepth();
		int[] myNumberOfChildren = flowManager.getNumberOfChildren();

		BitSet incomingFlow = flowManager.getIncomingFlow().getHealthyFlow();
		for (int i = incomingFlow.nextSetBit(0); i >= 0; i = incomingFlow
				.nextSetBit(i + 1)) {

			for (TransitConnection neighbor : neighbors) {
				TransitNodeInfo nodeInfo = neighbor.getNodeInfo();
				if (nodeInfo == null) {
					continue;
				}

				if (!optimizeTrees.contains(i)) {
					continue;
				}

				/* Only internal nodes are allowed to preempt other nodes */
				if (myNumberOfChildren[i] == 0) {
					return false;
				}

				/* Node closer to the source with less children than us. */
				if (myDepths[i] - 1 >= nodeInfo.getDepth()[i]
						&& nodeInfo.getDepth()[i] != -1
						&& nodeInfo.getChildren()[i] < myNumberOfChildren[i]) {

					TransitContact parentOfPreempted = nodeInfo.getParents()[i];
					if (parentOfPreempted == null) {
						continue;
					}
					TransitConnection connectionToParentOfPreempted = connectionManager
							.getConnection(parentOfPreempted);

					/* We just tried with this parent */
					if (lastTriedParentsOfNeighbors.get(i).contains(
							parentOfPreempted)) {
						continue;
					}

					/*
					 * We need an incoming connection from the parent of the
					 * preempted node.
					 */
					if (connectionToParentOfPreempted == null) {
						List<TransitContact> asList = new ArrayList<TransitContact>();
						asList.add(parentOfPreempted);
						lastTriedParentsOfNeighbors.get(i).addNow(
								parentOfPreempted);
						connectionManager.onNewNeighbors(asList);
						continue;
					}

					/*
					 * Is the connection to the parent of the neighbor in the
					 * right direction? If not: send a correct-direction message
					 * and keep trying with other nodes. But only if we are not
					 * sending a flow to this neighbor.
					 */
					if (connectionToParentOfPreempted.getDirection() == ConnectionDirection.OUT) {
						if (!flowManager.getOutgoingFlow().isChild(
								connectionToParentOfPreempted.getEndpoint(), i)) {
							lastTriedParentsOfNeighbors.get(i).addNow(
									parentOfPreempted);
							connectionManager
									.changeDirection(connectionToParentOfPreempted);
						}
						continue;
					}

					/*
					 * Are we receiving (another) flow from the parent of this
					 * neighbor? If yes, we cannot ask for a flow from this
					 * neighbor.
					 * 
					 * FIXME[JR]: This is due to the fact that in transit you
					 * cannot ask for more layers if you are already receiving
					 * some layers from a peer. This may be changed in future.
					 * For now: we cannot jump to this node.
					 */
					if (connectionToParentOfPreempted.getDirection() == ConnectionDirection.IN
							&& (connectionToParentOfPreempted
									.getScheduleState() == ScheduleState.ACTIVE)
							|| connectionToParentOfPreempted.getScheduleState() == ScheduleState.NEGOTIATING) {
						continue;
					}

					/*
					 * We are already sending a flow to this node. Because of
					 * this, the node cannot issue a flow request from us. See
					 * comment above.
					 */
					if (neighbor.getDirection() == ConnectionDirection.OUT
							&& (neighbor.getScheduleState() == ScheduleState.ACTIVE)
							|| neighbor.getScheduleState() == ScheduleState.NEGOTIATING) {
						continue;
					}

					/* No change of parent if we preempt */
					TransitConnection actualParent = flowManager
							.getIncomingFlow().getConnectionForFlow(i);
					if (neighbor.getEndpoint().getNodeID()
							.equals(actualParent.getEndpoint().getNodeID())) {
						continue;
					}

					/* We just tried with this one. */
					if (lastTried.get(i).contains(neighbor)) {
						continue;
					}

					/* We can preempt this node */
					PossibleHighDegreePreemptionOptimization possible = new PossibleHighDegreePreemptionOptimization(
							neighbor, connectionToParentOfPreempted, i,
							myDepths[i], nodeInfo.getDepth()[i],
							myNumberOfChildren[i], nodeInfo.getChildren()[i]);
					if (possible.getGain() >= gainThreshold) {
						possibleOptimizations.add(possible);
					}

				}
			}
		}

		if (possibleOptimizations.isEmpty()) {
			return false;
		}

		/*
		 * Now we have a list of possible optimizations. Choose the best one.
		 * LayerID->Possible OptimizationsAction
		 */
		Map<Integer, PossibleHighDegreePreemptionOptimization> best = new HashMap<Integer, PossibleHighDegreePreemptionOptimization>();
		for (PossibleHighDegreePreemptionOptimization possible : possibleOptimizations) {
			Integer layer = possible.getLayer();
			double gain = possible.getGain();
			if (!best.containsKey(layer)) {
				best.put(layer, possible);
				continue;
			}

			if (gain > best.get(layer).getGain()) {
				best.put(layer, possible);
			}
		}

		chooseBest(best);
		if (chosen != null) {
			return true;

		} else {
			return false;
		}
	}

	private void chooseBest(
			Map<Integer, PossibleHighDegreePreemptionOptimization> actions) {

		double bestGain = -1;

		Iterator<Integer> it = actions.keySet().iterator();
		while (it.hasNext()) {
			Integer next = it.next();
			PossibleOptimizationAction action = actions.get(next);
			if (action.getGain() > bestGain) {
				bestGain = action.getGain();
			}
		}

		chosen = null;

		boolean didChose = false;
		int counter = (lastOptimizedTree + 1) % 4;
		while (!didChose) {
			if (actions.containsKey(counter)
					&& actions.get(counter).getGain() == bestGain) {
				chosen = actions.get(counter);
				lastOptimizedTree = counter;
				didChose = true;
			}
			counter = (counter + 1) % 4;
		}

	}

	@Override
	public long doOptimize() {

		TransitConnection actualParent = flowManager.getIncomingFlow()
				.getConnectionForFlow(chosen.getLayer());

		if (actualParent == null) {
			return 0;
		}

		if (actualParent != null && actualParent.getEndpoint() == null) {
			return 0;
		}

		if (chosen.getTargetConnection() == null) {
			return 0;
		}
		if (chosen.getTargetConnection().getNodeInfo() == null) {
			return 0;
		}

		if (TransitOptimizationManager.DEBUG) {
			System.out.println("["
 + node.getHost().getId()
					+ "] "
					+ Time.getFormattedTime(Time.getCurrentTime())
					+ " Doing optimization: node ["
 + node.getHost().getId()
					+ "] (parent "
					+ flowManager.getIncomingFlow()
							.getConnectionForFlow(chosen.getLayer())
							.getEndpoint().getNodeID()
					+ ") tries to preempt "
					+ chosen.getTargetConnection().getEndpoint().getNodeID()
					+ "("
					+ chosen.getLayer()
					+ "), Gain: "
					+ chosen.getGain()
					+ " with parent "
					+ chosen.getParentOfPreempted().getEndpoint().getNodeID());

		}

		/* We request the flow from the parent of the chosen connection. */
		TransitConnection parentOfPreempted = chosen.getParentOfPreempted();
		TransitContact replaceChild = chosen.getTargetConnection()
				.getEndpoint();
		int layer = chosen.getLayer();
		// because we send 3 messages.
		long calculatedRTT = (long)Math.ceil(chosen.getTargetConnection()
				.getCurrentRTTEstimation() * 1.5);

		optimizationManager.setDoubleParents(actualParent, parentOfPreempted,
				layer);

		lastTriedParentsOfNeighbors.get(layer).addNow(
				parentOfPreempted.getEndpoint());

		BitSet requestBitSet = new BitSet();
		requestBitSet.set(layer);
		lastTried.get(layer).addNow(parentOfPreempted);
		TransitFlowSchedule request = new TransitFlowSchedule(requestBitSet);
		flowManager.doRequestReplaceChild(request, parentOfPreempted,
				replaceChild, calculatedRTT);
		// flowManager.doRequest(request, parentOfPreempted);

		long wait = actualParent.getCurrentRTTEstimation()
				+ parentOfPreempted.getCurrentRTTEstimation();

		return wait;

	}

	@Override
	public long onFlowReply(TransitConnection conn) {

		if (chosen == null) {
			optimizationManager.setActualOptimization(null);
			return 0; // when we manually set the
					// HighDegreePreemptionOptimization, e.g. with
					// optimizationManager.setActualOptimizationHighDegreePreemption()
		}

		TransitConnection targetConnection = chosen.getTargetConnection();

		targetConnection.setDirection(ConnectionDirection.OUT);
		flowManager
				.doSendRequestFromMeCommand(
						new TransitFlowSchedule(chosen.getSchedule()),
						targetConnection);

		optimizationManager.setActualOptimization(null);

		// return targetConnection.getCurrentRTTEstimation()*2;

		// optimizationManager.setActualOptimization(null);
		//
		return Time.MINUTE;

	}

	@Override
	public long onFlowDeny(TransitConnection conn) {
		optimizationManager.setActualOptimization(null);

		return 0;
	}
}
