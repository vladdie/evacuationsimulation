/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit;

import java.util.BitSet;

import de.tudarmstadt.maki.simonstrator.api.component.overlay.IPeerStatusListener;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitBlockMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.messages.TransitFlowMessage;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.scheduling.TransitScheduler;

/**
 * Interface for the public part of the FlowManager in Transit. The FlowManager
 * is responsible for the timely distribution of newly available video blocks.
 * It can access the {@link TransitRequestManager} via the
 * {@link TransitScheduler} to start requests for blocks that were not
 * transmitted in the LivePhase.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 04.07.2012
 */
public interface TransitFlowManager extends IPeerStatusListener {

	/*
	 * TODO
	 */

	/**
	 * Called once every time the {@link PlayOperation} is executed. This is the
	 * speed at which one chunk is consumed. This will of course also be called
	 * if there is a stall.
	 */
	public void tick();

	/**
	 * Tells the FlowManager to request a given flow out of all available
	 * packets (basically, this is called whenever the layer changes). The
	 * FlowManager will split this mask across multiple Connections.
	 * 
	 * @param mask
	 *            the overall mask that is requested.
	 */
	public void requestFlow(BitSet mask);

	/**
	 * Received a video block (live). This method should forward the message and
	 * increment the hop-count accordingly!
	 * 
	 * @param blockMessage
	 * @param replyTo
	 *            optional reply handler
	 */
	public void receivedLiveBlock(TransitBlockMessage blockMessage, TransitReplyHandler replyTo, boolean duplicate);

	/**
	 * Received a protocol message for the FlowManager.
	 * 
	 * @param message
	 * @param replyTo
	 */
	public void receivedFlowMessage(TransitFlowMessage message, TransitReplyHandler replyTo);

	/**
	 * Returns true, if the incoming Flow is healthy.
	 * 
	 * @return
	 */
	public boolean isHealthy();

}
