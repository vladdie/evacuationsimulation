/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Time;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent;
import de.tudarmstadt.maki.simonstrator.api.component.overlay.OverlayComponent.PeerStatus;
import de.tudarmstadt.maki.simonstrator.api.operation.Operation;
import de.tudarmstadt.maki.simonstrator.api.operation.OperationCallback;
import de.tudarmstadt.maki.simonstrator.api.operation.Operations;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitMessageHandler.TransitReplyHandler;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitNode;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitParams;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.TransitSettings.TransitTimes;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionDirection;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitConnection.ConnectionState;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood.TransitContact.ContactType;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.operations.JoinTrackerOperation;
import de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.plugins.pulltoken.PullToken;
import de.tudarmstadt.maki.simonstrator.util.timeoutcollections.TimeoutSet;

// TODO: Auto-generated Javadoc
/**
 * Management of the neighborhood, i.e. the signaling layer in transit.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 08.06.2012
 */
public class TransitNeighborhoodManager implements TransitNeighborhood {

	/** The connection manager. */
	private final TransitConnectionManager connectionManager;

	/** The node. */
	private final TransitNode node;

	/** The tracker. */
	private TransitContact tracker;

	/** The not reachable. */
	private final TimeoutSet<TransitContact> notReachable = new TimeoutSet<TransitContact>(Time.SECOND);

	/** The recent contacts. */
	private final TimeoutSet<TransitContact> recentContacts = new TimeoutSet<TransitContact>(Time.SECOND);

	/** The allow two way connections. */
	private final boolean ALLOW_TWO_WAY_CONNECTIONS;

	/** The neighborhood shuffling strategy. */
	protected final int NEIGHBORHOOD_SHUFFLING_STRATEGY;

	/** The neighborhood shuffling interval. */
	protected final long NEIGHBORHOOD_SHUFFLING_INTERVAL;

	/** The min connections to retain. */
	protected final int MIN_CONNECTIONS_TO_RETAIN;

	/** The periodic neighborhood shuffler. */
	private PeriodicNeighborhoodShuffler periodicNeighborhoodShuffler;

	/** The shuffle strategy by load. */
	final public int SHUFFLE_STRATEGY_BY_LOAD = 1;

	/** The shuffle strategy by recent bytes. */
	final public int SHUFFLE_STRATEGY_BY_RECENT_BYTES = 2;

	/** The shuffle strategy by incentive. */
	final public int SHUFFLE_STRATEGY_BY_INCENTIVE = 3;

	/**
	 * Instantiates a new transit neighborhood manager.
	 * 
	 * @param node
	 *            the node
	 */
	public TransitNeighborhoodManager(TransitNode node) {
		this.connectionManager = new TransitConnectionManager(node);
		this.node = node;
		this.ALLOW_TWO_WAY_CONNECTIONS = node.getSettings().getParam(
		        TransitParams.CONNECTION_ALLOW_TWO_WAY_DATA_TRANSFER) > 0;
		this.NEIGHBORHOOD_SHUFFLING_INTERVAL = node.getSettings().getTime(TransitTimes.NEIGHBORHOOD_SHUFFLING_INTERVAL);
		this.NEIGHBORHOOD_SHUFFLING_STRATEGY = node.getSettings().getParam(
		        TransitParams.NEIGHBORHOOD_SHUFFLING_STRATEGY);
		this.MIN_CONNECTIONS_TO_RETAIN = node.getSettings().getParam(TransitParams.CONNECTION_MIN_INCOMING);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tud.kom.p2psim.impl.overlay.IPeerStatusListener#peerStatusChanged(
	 * de.tud.kom.p2psim.api.overlay.OverlayNode,
	 * de.tud.kom.p2psim.api.overlay.OverlayNode.PeerStatus)
	 */
	@Override
	public void peerStatusChanged(OverlayComponent source, PeerStatus peerStatus) {
		switch (peerStatus) {
		case PRESENT:
		case TO_JOIN:
			this.periodicNeighborhoodShuffler = new PeriodicNeighborhoodShuffler();
			break;

		case ABSENT:
			if (periodicNeighborhoodShuffler != null) {
				periodicNeighborhoodShuffler.stop();
			}
			break;

		default:
			throw new AssertionError("Unknown PeerStatus.");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitNeighborhood
	 * #receivedNeighborhood(de.tudarmstadt.maki.simonstrator
	 * .overlay.streaming.transit.neighborhood.TransitContact, java.util.List,
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.
	 * TransitMessageHandler.TransitReplyHandler)
	 */
	@Override
	public void receivedNeighborhood(TransitContact from, List<TransitContact> neighborhood, TransitReplyHandler replyTo) {

		if (node.isServer()) {
			return;
		}

		List<TransitContact> contacts = new LinkedList<TransitContact>();
		for (TransitContact transitContact : neighborhood) {
			if (transitContact.equals(node.getLocalOverlayContact())) {
				continue;
			}
			if (notReachable.contains(transitContact)) {
				continue;
			}
			if (connectionManager.getConnection(transitContact, ConnectionDirection.IN) != null) {
				continue;
			}
			if (!ALLOW_TWO_WAY_CONNECTIONS
			        && connectionManager.getConnection(transitContact, ConnectionDirection.OUT) != null) {
				continue;
			}
			if (contacts.contains(transitContact)) {
				continue;
			}
			contacts.add(transitContact);
			recentContacts.addNow(transitContact);
		}
		if (!contacts.isEmpty()) {
			connectionManager.onNewNeighbors(contacts);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitNeighborhood#requestNeighbors()
	 */
	@Override
	public void requestNeighbors() {

		new JoinTrackerOperation(getNode(), new OperationCallback<TransitContact>() {

			@Override
			public void calledOperationFailed(Operation<TransitContact> op) {
				// TODO Auto-generated method stub

			}

			@Override
			public void calledOperationSucceeded(Operation<TransitContact> op) {
				// TODO Auto-generated method stub

			}
		});

		/*
		 * We do not support active requests, but we call onNewNeighbors with
		 * some of the previously unused contacts.
		 */
		List<TransitContact> contacts = new LinkedList<TransitContact>();
		for (TransitContact contact : recentContacts.getUnmodifiableSet()) {
			assert !contact.equals(node.getLocalOverlayContact());
			if (notReachable.contains(contact)) {
				continue;
			}
			if (connectionManager.getConnection(contact, ConnectionDirection.IN) != null) {
				continue;
			}
			contacts.add(contact);
		}
		if (!contacts.isEmpty()) {
			connectionManager.onNewNeighbors(contacts);
		}
	}

	/**
	 * For callback-access.
	 * 
	 * @return the node
	 */
	protected TransitNode getNode() {
		return node;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitNeighborhood
	 * #messageArrived(de.tudarmstadt.maki.simonstrator.overlay
	 * .streaming.transit.neighborhood.TransitContact, long, long)
	 */
	@Override
	public void messageArrived(TransitContact from, long rtt, long messageSize) {
		connectionManager.updateRttEstimation(from, rtt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitNeighborhood
	 * #timeout(de.tudarmstadt.maki.simonstrator.overlay.streaming
	 * .transit.neighborhood.TransitContact)
	 */
	@Override
	public void timeout(TransitContact contact) {
		notReachable.addNow(contact);
		recentContacts.remove(contact);
		connectionManager.timeout(contact);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitNeighborhood#getTracker()
	 */
	@Override
	public TransitContact getTracker() {
		return tracker;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitNeighborhood
	 * #forceInjectNewNeighbor(de.tudarmstadt.maki.simonstrator
	 * .overlay.streaming.transit.neighborhood.TransitContact, long)
	 */
	@Override
	public void forceInjectNewNeighbor(TransitContact senderContact, PullToken pullToken) {
		TransitConnection inConnection = getConnectionManager().getConnection(senderContact, ConnectionDirection.IN);
		if (inConnection == null) {
			closeNeighborConnectionsWithStrategy(SHUFFLE_STRATEGY_BY_RECENT_BYTES, MIN_CONNECTIONS_TO_RETAIN - 1);
			inConnection = getConnectionManager().getOrOpenConnection(senderContact);
		}
		inConnection.setPullToken(pullToken);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitNeighborhood
	 * #forceRemoveNeighbor(de.tudarmstadt.maki.simonstrator.
	 * overlay.streaming.transit.neighborhood.TransitContact)
	 */
	@Override
	public void forceRemoveNeighbor(TransitContact badHost) {
		TransitConnection inConnection = getConnectionManager().getConnection(badHost, ConnectionDirection.OUT);
		if (inConnection != null) {
			getConnectionManager().closeConnection(inConnection);
		}
	}

	/**
	 * Shuffle neighborhood adapter. First contact tracker for new contacts.
	 * Then shuffle.
	 * 
	 * @param shuffleMode
	 *            the shuffle mode SHUFFLE_STRATEGY_BY_LOAD: by load, kick last
	 *            X percent SHUFFLE_STRATEGY_BY_RECENT_BYTES: by recently
	 *            transferred bytes, kick last X percent
	 *            SHUFFLE_STRATEGY_BY_INCENTIVE: ask Incentive Manager
	 */
	public void shuffleNeighborhood(final int shuffleMode) {

		if (getNode().isServer() == true)
			return;

		/*
		 * Request new contacts from tracker so that we don't have a problem
		 * when closing connections.
		 */
		JoinTrackerOperation op = new JoinTrackerOperation(node, true, new JoinTrackerCallback(
		        new OperationCallback<Object>() {

			        @Override
			        public void calledOperationFailed(Operation<Object> op) {
				        // None. Thanks
			        }

			        @Override
			        public void calledOperationSucceeded(Operation<Object> op) {
				        shuffleNeighborhoodReal(shuffleMode, MIN_CONNECTIONS_TO_RETAIN);
			        }
		        }));

		op.scheduleImmediately();

	}

	/**
	 * Shuffle neighborhood real.
	 * 
	 * @param shuffleMode
	 *            the shuffle mode
	 */
	void shuffleNeighborhoodReal(int shuffleMode, int minConnectionsToRetai) {
		/* First: Close. */
		this.closeNeighborConnectionsWithStrategy(shuffleMode, minConnectionsToRetai);

		/* Then: Reopen. */
		this.requestNeighbors();
	}

	/**
	 * Close neighbor connections with strategy.
	 * 
	 * @param shuffleMode
	 *            the shuffle mode
	 * @param minConnectionsToRetai
	 *            the min connections to retai
	 */
	private void closeNeighborConnectionsWithStrategy(int shuffleMode, int minConnectionsToRetai) {
		/*
		 * Shuffle neighborhood with one of the following strategies:
		 * SHUFFLE_STRATEGY_BY_LOAD: by load, kick last X percent
		 * SHUFFLE_STRATEGY_BY_RECENT_BYTES: by recently transferred bytes, kick
		 * last X percent SHUFFLE_STRATEGY_BY_INCENTIVE: ask Incentive Manager
		 */
		final List<TransitConnection> inConnections = new LinkedList<TransitConnection>(
		        connectionManager.getConnections(ConnectionDirection.IN, ConnectionState.OPEN, null, null));

		switch (shuffleMode) {
		case SHUFFLE_STRATEGY_BY_LOAD:
			Collections.sort(inConnections, new Comparator<TransitConnection>() {
				@Override
				public int compare(TransitConnection o1, TransitConnection o2) {
					return Short.valueOf(o1.getEndpoint().getLoad()).compareTo(
					        Short.valueOf(o2.getEndpoint().getLoad()));
				}
			});
			for (int i = inConnections.size() - 1; i > minConnectionsToRetai; i--) {
				connectionManager.closeConnection(inConnections.get(i));
			}
			break;

		case SHUFFLE_STRATEGY_BY_RECENT_BYTES:
			Collections.sort(inConnections, new Comparator<TransitConnection>() {
				@Override
				public int compare(TransitConnection o1, TransitConnection o2) {
					return Long.valueOf(o2.getConnectionStatistics().getRecentlyBytesTransferred()).compareTo(
					        Long.valueOf(o1.getConnectionStatistics().getRecentlyBytesTransferred()));
				}
			});
			for (int i = inConnections.size() - 1; i >= minConnectionsToRetai; i--) {
				connectionManager.closeConnection(inConnections.get(i));
			}
			break;

		case SHUFFLE_STRATEGY_BY_INCENTIVE:
			List<TransitConnection> sortedConnections = getNode().getIncentiveManager()
			        .sortConnectionsByIncentiveForRequesting(inConnections);
			for (int i = sortedConnections.size() - 1; i > minConnectionsToRetai; i--) {
				connectionManager.closeConnection(sortedConnections.get(i));
			}
			break;

		default:
			/* Unknown! */
			return;
		}
	}

	/**
	 * Set the tracker, this is done as a result of the.
	 * 
	 * @param tracker
	 *            the new tracker {@link JoinTrackerOperation} inside the
	 *            {@link JoinTrackerCallback}.
	 */
	protected void setTracker(TransitContact tracker) {
		assert tracker.getType() == ContactType.TRACKER;
		this.tracker = tracker;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitNeighborhood#getConnectionManager()
	 */
	@Override
	public TransitConnectionManager getConnectionManager() {
		return connectionManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.tudarmstadt.maki.simonstrator.overlay.streaming.transit.neighborhood
	 * .TransitNeighborhood
	 * #connectToTracker(de.tud.kom.p2psim.api.common.OperationCallback)
	 */
	@Override
	public int connectToTracker(OperationCallback<Object> callback) {
		JoinTrackerOperation op = new JoinTrackerOperation(node, new JoinTrackerCallback(callback));
		op.scheduleImmediately();
		return op.getOperationID();
	}

	/**
	 * The Class PeriodicNeighborhoodShuffler.
	 */
	private class PeriodicNeighborhoodShuffler implements EventHandler {

		/** The stopped. */
		private boolean stillRunning = true;

		/**
		 * Instantiates a new periodic neighborhood shuffler.
		 */
		public PeriodicNeighborhoodShuffler() {
			if (NEIGHBORHOOD_SHUFFLING_INTERVAL > 0 && NEIGHBORHOOD_SHUFFLING_STRATEGY > 0 && !getNode().isServer()) {
				scheduleNextShuffleAction();
			}

		}

		/**
		 * Schedule shuffle action.
		 */
		private void scheduleNextShuffleAction() {
			Event.scheduleWithDelay(NEIGHBORHOOD_SHUFFLING_INTERVAL, this, null, 0);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * de.tud.kom.p2psim.api.simengine.SimulationEventHandler#eventOccurred
		 * (de.tud.kom.p2psim.impl.simengine.SimulationEvent)
		 */
		@Override
		public void eventOccurred(Object se, int type) {
			if (stillRunning) {

				// (1) Exchange Neighborhood.
				shuffleNeighborhood(NEIGHBORHOOD_SHUFFLING_STRATEGY);

				// (2) Go to sleep and reschedule for future.
				scheduleNextShuffleAction();
			}
		}

		/**
		 * Stop.
		 */
		public void stop() {
			this.stillRunning = false;
		}

	}

	/**
	 * This callback is passed to the {@link JoinTrackerOperation} and might be
	 * extended lateron to support retries. For now, we just inform the
	 * application.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 29.05.2012
	 */
	public class JoinTrackerCallback implements OperationCallback<TransitContact> {

		/** The app callback. */
		private final OperationCallback<Object> appCallback;

		/**
		 * Instantiates a new join tracker callback.
		 * 
		 * @param appCallback
		 *            the app callback
		 */
		public JoinTrackerCallback(OperationCallback<Object> appCallback) {
			this.appCallback = appCallback;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * de.tud.kom.p2psim.api.common.OperationCallback#calledOperationFailed
		 * (de.tud.kom.p2psim.api.common.Operation)
		 */
		@Override
		public void calledOperationFailed(Operation<TransitContact> op) {
			Operations.createEmptyFailingOperation(getNode(), appCallback).scheduleImmediately();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * de.tud.kom.p2psim.api.common.OperationCallback#calledOperationSucceeded
		 * (de.tud.kom.p2psim.api.common.Operation)
		 */
		@Override
		public void calledOperationSucceeded(Operation<TransitContact> op) {
			JoinTrackerOperation joinOp = (JoinTrackerOperation) op;
			getNode().setPeerStatus(PeerStatus.PRESENT);
			setTracker(op.getResult());
			receivedNeighborhood(op.getResult(), joinOp.getInitialContacts(), null);
			Operations.createEmptyOperation(getNode(), appCallback).scheduleImmediately();
		}
	}

}
