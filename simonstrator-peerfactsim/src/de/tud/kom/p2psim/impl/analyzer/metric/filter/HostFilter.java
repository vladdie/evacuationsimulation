/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.analyzer.metric.filter;

import java.util.List;

import de.tud.kom.p2psim.api.common.SimHost;
import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

/**
 * Filters metrics based on a host ID, a list of host IDs, or a group name
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Oct 11, 2016
 */
public abstract class HostFilter extends AbstractFilter<MetricValue<?>> {

	protected String description = "";

	public HostFilter(String name, String description) {
		super(name);
		this.description = description;
	}

	@Override
	protected String getNameForDerivedMetric(List<Metric<?>> inputs) {
		assert inputs.size() == 1;
		return inputs.get(0).getName() + getPostfix();
	}

	@Override
	protected void onInitialize(List<Metric<?>> incomingMetrics) {
		for (Metric metric : incomingMetrics) {
			if (metric.isOverallMetric()) {
				continue;
			}
			createDerivedMetric(metric, false, metric.getUnit(),
					description + " of " + metric.getName(), false);
		}
	}

	@Override
	public void onStop() {
		// nothing to do
	}

	@Override
	protected MetricValue<?> getDerivedMetricValueFor(Metric<?> derivedMetric,
			List<Metric<?>> inputs, Host host) {
		assert inputs.size() == 1;
		assert host != null;
		Metric<?> input = inputs.get(0);
		if (input.isOverallMetric()) {
			throw new AssertionError(
					"Only available for per-host input metrics.");
		}
		/*
		 * filter hosts, return null for irrelevant hosts.
		 */
		if (isAllowed(host)) {
			return new HostFilteredMetricValue(
					input.getPerHostMetric(host.getId()));
		} else {
			return null;
		}
	}

	/**
	 * Computes statistics such as svg, sum, std...
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 08.08.2012
	 */
	private class HostFilteredMetricValue implements MetricValue {

		private final MetricValue input;

		public HostFilteredMetricValue(MetricValue input) {
			this.input = input;
		}

		@Override
		public Object getValue() {
			return input.getValue();
		}

		@Override
		public boolean isValid() {
			return input.isValid();
		}

	}

	protected abstract boolean isAllowed(Host host);

	protected abstract String getPostfix();

	/**
	 * Filter by a given Host group name
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, Oct 11, 2016
	 */
	public static class GroupName extends HostFilter {

		private String group;

		@XMLConfigurableConstructor({ "group" })
		public GroupName(String group) {
			super(group, "Only " + group);
			this.group = group;
		}

		@Override
		protected boolean isAllowed(Host host) {
			SimHost sHost = (SimHost) host;
			return group.equals(sHost.getProperties().getGroupID());
		}

		@Override
		protected String getPostfix() {
			return "_" + group;
		}

	}

}
