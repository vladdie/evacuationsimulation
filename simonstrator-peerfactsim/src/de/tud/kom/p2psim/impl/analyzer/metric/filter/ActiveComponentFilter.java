/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.analyzer.metric.filter;

import java.util.List;

import de.tudarmstadt.maki.simonstrator.api.Host;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric;
import de.tudarmstadt.maki.simonstrator.api.common.metric.Metric.MetricValue;
import de.tudarmstadt.maki.simonstrator.api.component.ComponentNotAvailableException;
import de.tudarmstadt.maki.simonstrator.api.component.LifecycleComponent;
import de.tudarmstadt.maki.simonstrator.api.util.XMLConfigurableConstructor;

/**
 * Filter a metric usign a {@link LifecycleComponent}s isActive method.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Feb 7, 2017
 */
public class ActiveComponentFilter extends AbstractFilter<MetricValue<?>> {

	private Class<? extends LifecycleComponent> compClass;
	
	private String description;
	
	private String prefix;
	
	@SuppressWarnings("unchecked")
	@XMLConfigurableConstructor({ "clazz", "prefix" })
	public ActiveComponentFilter(String clazz, String prefix) {
		super("active");
		this.prefix = prefix;
		description = "only active "+clazz.substring(clazz.lastIndexOf(".") + 1);
		// Find class
		try {
			compClass = (Class<? extends LifecycleComponent>) Class.forName(clazz);
		} catch (ClassNotFoundException e) {
			throw new AssertionError();
		}
	}

	@Override
	protected String getNameForDerivedMetric(List<Metric<?>> inputs) {
		assert inputs.size() == 1;
		return prefix + "_" + inputs.get(0).getName();
	}

	@Override
	protected void onInitialize(List<Metric<?>> incomingMetrics) {
		for (Metric metric : incomingMetrics) {
			if (metric.isOverallMetric()) {
				continue;
			}
			createDerivedMetric(metric, false, metric.getUnit(),
					description + " of " + metric.getName(), false);
		}
	}

	@Override
	public void onStop() {
		// nothing to do
	}

	@Override
	protected MetricValue<?> getDerivedMetricValueFor(Metric<?> derivedMetric,
			List<Metric<?>> inputs, Host host) {
		assert inputs.size() == 1;
		assert host != null;
		Metric<?> input = inputs.get(0);
		if (input.isOverallMetric()) {
			throw new AssertionError(
					"Only available for per-host input metrics.");
		}
		try {
			LifecycleComponent comp = host.getComponent(compClass);
			MetricValue mvIn = input.getPerHostMetric(host.getId());
			if (mvIn != null) {
				return new ActiveHostMetricValue(input.getPerHostMetric(host.getId()), comp);
			}
		} catch (ComponentNotAvailableException e) {
			//
		}
		return null; // no derived metric
	}

	/**
	 * Computes statistics such as svg, sum, std...
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, 08.08.2012
	 */
	private class ActiveHostMetricValue implements MetricValue {

		private final MetricValue input;
		
		private final LifecycleComponent comp;

		public ActiveHostMetricValue(MetricValue input, LifecycleComponent comp) {
			this.input = input;
			this.comp = comp;
		}

		@Override
		public Object getValue() {
			return input.getValue();
		}

		@Override
		public boolean isValid() {
			return comp.isActive() && input.isValid();
		}

	}
	
}
