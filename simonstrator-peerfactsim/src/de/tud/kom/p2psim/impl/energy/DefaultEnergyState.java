/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.energy;

import de.tud.kom.p2psim.api.energy.EnergyState;

/**
 * Basic implementation of an EnergyState
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, 03.03.2012
 */
public class DefaultEnergyState implements EnergyState {

	private String name;

	private double energyConsumption;

	/**
	 * 
	 * @param name
	 *            identifier of this state
	 * @param energyConsumption
	 *            consumption in uW
	 */
	public DefaultEnergyState(String name, double energyConsumption) {
		this.name = name;
		this.energyConsumption = energyConsumption;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getEnergyConsumption() {
		return energyConsumption;
	}

	@Override
	public String toString() {
		return getName();
	}

}
