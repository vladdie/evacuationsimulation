/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.util.db.dao.metric;

import java.util.HashMap;
import java.util.Map;

import de.tud.kom.p2psim.impl.util.db.dao.DAO;
import de.tud.kom.p2psim.impl.util.db.metric.GroupMetric;
import de.tud.kom.p2psim.impl.util.db.metric.Metric;

public class GroupMetricDAO extends DAO {
	/** Cache of {@link GroupMetric} objects to avoid database lookups. */
	private static Map<Metric, Map<String, GroupMetric>> groupMetricCache = new HashMap<Metric, Map<String, GroupMetric>>();

	/**
	 * Retrieve the {@link GroupMetric} object for the given metric and group
	 * name.
	 * 
	 * If there is no matching Metric object, it is created, persisted, and
	 * cached automatically.
	 */
	public static GroupMetric lookupGroupMetric(Metric metric,
			String groupName) {
		Map<String, GroupMetric> metricMap = groupMetricCache.get(metric);

		if (metricMap == null) {
			metricMap = new HashMap<String, GroupMetric>();
			groupMetricCache.put(metric, metricMap);
		}
		
		GroupMetric gm = metricMap.get(groupName);
		
		if (gm == null) {
			gm = new GroupMetric(metric, groupName);
			metricMap.put(groupName, gm);
			addToPersistQueue(gm);
			// TODO: block commit and avoid multiple threads for commit of first
			// object definition
			commitQueue();
		}
		return gm;

	}
}
