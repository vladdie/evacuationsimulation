/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.util.db.metric;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "groupmetrics", indexes = {
		@Index(columnList = "id", name = "id"),
		@Index(columnList = "groupName", name = "groupName"),
		@Index(columnList = "metricId", name = "metricId") })
/**
 * Assign a metric measurement to a group of hosts instead of single hosts or
 * global.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Jan 30, 2017
 */
public class GroupMetric {
	
	@Id
	@GeneratedValue
	private int id;
	
	/**
	 * GroupName of the host
	 */
	@Column(length = 1023)
	private String groupName;

	@ManyToOne
	@JoinColumn(name = "metricId")
	private Metric metric;

    protected GroupMetric() {

    }

	public GroupMetric(Metric metric, String groupName) {
		this.metric = metric;
		this.groupName = groupName;
	}
	
	public String getGroupName() {
		return this.groupName;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((groupName == null) ? 0 : groupName.hashCode());
		result = prime * result + ((metric == null) ? 0 : metric.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GroupMetric other = (GroupMetric) obj;
		if (groupName == null) {
			if (other.groupName != null)
				return false;
		} else if (!groupName.equals(other.groupName))
			return false;
		if (metric == null) {
			if (other.metric != null)
				return false;
		} else if (!metric.equals(other.metric))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return "GroupMetric{" +
                "id=" + id +
                ", group=" + groupName +
                ", metric=" + metric +
                '}';
    }

    public int getId() {
        return id;
    }
}
