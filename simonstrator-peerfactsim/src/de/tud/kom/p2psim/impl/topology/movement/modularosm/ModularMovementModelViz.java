/*
 * Copyright (c) 2005-2015 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.topology.movement.modularosm;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JMenu;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import de.tud.kom.p2psim.api.topology.movement.SimLocationActuator;
import de.tud.kom.p2psim.impl.topology.PositionVector;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView;
import de.tud.kom.p2psim.impl.topology.views.VisualizationTopologyView.VisualizationInjector;
import de.tud.kom.p2psim.impl.topology.views.visualization.ui.VisualizationComponent;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;

/**
 * Visualization Component of the Attraction Points in the Modular Movement
 * Model.
 * 
 * 
 * @author Martin Hellwig, Bjoern Richerzhagen
 * @version 1.0, 02.07.2015
 */
public class ModularMovementModelViz extends JComponent
		implements VisualizationComponent {

	private ModularMovementModel movementModel;

	protected boolean showAttractionPoints = true;

	protected boolean showNodePositions = true;

	private JMenu menu;

	private final static int NODE_PAD = 2;

	private final static int ATTR_PAD = 5;

	private static Color COLOR_ATTR_POINT = Color.decode("#4A7B9D");

	public ModularMovementModelViz(ModularMovementModel model) {
		setBounds(0, 0, VisualizationInjector.getWorldX(),
				VisualizationInjector.getWorldY());
		setOpaque(true);
		setVisible(true);
		this.movementModel = model;

		menu = new JMenu("Mobility Model");
		menu.add(createCheckboxAp());
		menu.add(createCheckboxNodePositions());
	}

	private JCheckBoxMenuItem createCheckboxAp() {
		final JCheckBoxMenuItem checkBox = new JCheckBoxMenuItem(
				"show attraction points", showAttractionPoints);
		checkBox.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				showAttractionPoints = checkBox.isSelected();
				VisualizationInjector.invalidate();
			}
		});
		return checkBox;
	}

	private JCheckBoxMenuItem createCheckboxNodePositions() {
		final JCheckBoxMenuItem checkBox = new JCheckBoxMenuItem(
				"show node positions", showNodePositions);
		checkBox.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				showNodePositions = checkBox.isSelected();
				VisualizationInjector.invalidate();
			}
		});
		return checkBox;
	}

	@Override
	public void paint(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		if (showAttractionPoints) {
			for (AttractionPoint aPoint : movementModel.getAttractionPoints()) {
				Point point = ((PositionVector) aPoint).asPoint();
				// draw border
				g2.setColor(Color.BLACK);
				g2.setFont(VisualizationTopologyView.FONT_MEDIUM);
				g2.drawString(aPoint.getName(),
						VisualizationInjector.scaleValue(point.x) - ATTR_PAD,
						VisualizationInjector.scaleValue(point.y) - ATTR_PAD);
				g2.setColor(COLOR_ATTR_POINT);
				float alpha = 0.25f + (float) (aPoint.getWeight() / 2);
				g2.setComposite(AlphaComposite
						.getInstance(AlphaComposite.SRC_OVER, alpha));
				g2.fillOval(
						VisualizationInjector.scaleValue(point.x) - ATTR_PAD,
						VisualizationInjector.scaleValue(point.y) - ATTR_PAD,
						ATTR_PAD * 2 + 1, ATTR_PAD * 2 + 1);
				g2.setColor(COLOR_ATTR_POINT);
				int radius = (int) aPoint.getRadius() + ATTR_PAD;
				g2.drawOval(VisualizationInjector.scaleValue(point.x) - radius,
						VisualizationInjector.scaleValue(point.y) - radius,
						radius * 2 + 1, radius * 2 + 1);

			}
		}

		if (showNodePositions) {
			for (SimLocationActuator comp : movementModel
					.getAllLocationActuators()) {
				Point2D pt = comp.getRealPosition().asPoint();
				g2.setColor(Color.GRAY);
				g2.fillOval((int) pt.getX() - NODE_PAD,
						(int) pt.getY() - NODE_PAD, NODE_PAD * 2 + 1,
						NODE_PAD * 2 + 1);
			}
		}
	}

	@Override
	public String getDisplayName() {
		return "Mobility Model";
	}

	@Override
	public JComponent getComponent() {
		return this;
	}

	@Override
	public JMenu getCustomMenu() {
		return menu;
	}

	@Override
	public boolean isHidden() {
		return false;
	}
}
