/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.topology.movement.modularosm.transition;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import de.tud.kom.p2psim.api.scenario.ConfigurationException;
import de.tud.kom.p2psim.api.topology.movement.SimLocationActuator;
import de.tudarmstadt.maki.simonstrator.api.Event;
import de.tudarmstadt.maki.simonstrator.api.EventHandler;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;

/**
 * A simplified transition strategy taking only the weights of an AP into account.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Jan 16, 2017
 */
public class WeightedTransitionStrategy implements ITransitionStrategy, EventHandler {
	
	private Set<AttractionPoint> aps = new LinkedHashSet<>();
	
	private Map<SimLocationActuator, AttractionPoint> assignments = new LinkedHashMap<>();
	
	private Map<SimLocationActuator, AttractionPoint> lastAssignments = new LinkedHashMap<>();
	
	private List<AttractionAssignmentListener> listeners = new LinkedList<>();
	
	private long pauseTimeMin = 0;
	
	private long pauseTimeMax = 0;
	
	private Random rnd = Randoms.getRandom(WeightedTransitionStrategy.class);
	
	private final static int EVENT_PAUSE_ENDED = 1;
	
	@Override
	public AttractionPoint getAssignment(SimLocationActuator comp) {
		return assignments.get(comp);
	}
	
	@Override
	public void addAttractionAssignmentListener(
			AttractionAssignmentListener listener) {
		listeners.add(listener);
	}
	
	@Override
	public void removeAttractionAssignmentListener(
			AttractionAssignmentListener listener) {
		listeners.remove(listener);
	}

	@Override
	public void setAttractionPoints(
			Collection<AttractionPoint> attractionPoints) {
		this.aps.addAll(attractionPoints);
	}

	@Override
	public Set<AttractionPoint> getAllAttractionPoints() {
		return Collections.unmodifiableSet(aps);
	}

	@Override
	public void addComponent(SimLocationActuator ms) {
		this.assignments.put(ms, getNewAssignment(ms));
	}
	
	private AttractionPoint getNewAssignment(SimLocationActuator component) {
		double score = rnd.nextDouble();
		List<AttractionPoint> candidates = new LinkedList<>();
		for (AttractionPoint ap : aps) {
			if (ap.getWeight() >= score) {
				candidates.add(ap);
			}
		}
		if (candidates.isEmpty()) {
			candidates.addAll(aps);
		}
		AttractionPoint assignment = candidates.get(rnd.nextInt(candidates.size()));
		listeners.forEach(listener -> listener.updatedAttractionAssignment(component, assignment));
		return assignment;
	}
	
	private long getPauseTime(SimLocationActuator component) {
		return (long) (rnd.nextDouble() * (pauseTimeMax-pauseTimeMin)) + pauseTimeMin;
	}
	
	@Override
	public void eventOccurred(Object content, int type) {
		assert type == EVENT_PAUSE_ENDED;
		SimLocationActuator comp = (SimLocationActuator) content;
		if (!this.assignments.containsKey(comp)) {
			// maybe someone assigned a new AP in the meantime. ignore.
			this.assignments.put(comp, getNewAssignment(comp));
		}
	}

	@Override
	public void reachedAttractionPoint(SimLocationActuator ms) {
		this.lastAssignments.put(ms, this.assignments.remove(ms));
		Event.scheduleWithDelay(getPauseTime(ms), this, ms, EVENT_PAUSE_ENDED);
	}

	@Override
	public void updateTargetAttractionPoint(SimLocationActuator comp,
			AttractionPoint attractionPoint) {
		this.lastAssignments.put(comp, this.assignments.remove(comp));
		this.assignments.put(comp, attractionPoint);
		listeners.forEach(listener -> listener.updatedAttractionAssignment(comp, attractionPoint));
	}
	
	public void setMinPauseTime(long minPauseTime) {
		if (minPauseTime < 0) {
			throw new ConfigurationException(
					"MinPauseTime should be >= 0!");
		}
		this.pauseTimeMin = minPauseTime;
	}

	public void setMaxPauseTime(long maxPauseTime) {
		if (maxPauseTime < 0) {
			throw new ConfigurationException(
					"MaxPauseTime should be >= 0!");
		}
		this.pauseTimeMax = maxPauseTime;
	}

}
