/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.topology.views.fiveg;

import de.tud.kom.p2psim.api.linklayer.mac.MacAddress;
import de.tud.kom.p2psim.impl.topology.views.FiveGTopologyView;
import de.tud.kom.p2psim.impl.topology.views.FiveGTopologyView.CellLink;
import de.tudarmstadt.maki.simonstrator.api.component.GlobalComponent;

/**
 * Database for the {@link FiveGTopologyView} - containing a mapping of position
 * IDs to the respective link characteristics.
 * 
 * 06.10.2016 added by Nils Richerzhagen, Clemens Krug Functionality to
 * 'destroy' cells using SHIFT + Left-click. Enable/Disable using the
 * enableCellDestruction boolean.
 * 
 * @author Bjoern Richerzhagen
 * @version 1.0, Nov 5, 2015
 */
public interface FiveGTopologyDatabase extends GlobalComponent {

	/**
	 * Calculation of a segment ID based on a position (usually given in
	 * Cartesian coordinates between 0 and WORLD_SIZE)
	 * 
	 * @param x
	 *            coordinate
	 * @param y
	 *            coordinate
	 * @return
	 */
	public int getSegmentID(double x, double y);

	/**
	 * Access to the {@link Entry} for a given segmentID. To allow distinction
	 * between clouds (full latency) and cloudlets (less latency, as the backend
	 * is "shorter"), we added a boolean "isCloudlet", which may or may not be
	 * used by the database. It is expected that the link to a cloudlet should
	 * outperform the link to a cloud.
	 * 
	 * @param segmentID
	 * @return the entry. ONLY for access point databases, this might also
	 *         return null, if the given segment is not offering access point
	 *         connectivity.
	 */
	public FiveGTopologyDatabase.Entry getEntryFor(int segmentID,
			boolean isCloudlet);

	/**
	 * If wanted one can 'destroy' cells using this method. Destroy means that
	 * the cell is not available for the communication mean specified within
	 * that TopologyView.
	 * 
	 * To be set by configuration.
	 * 
	 * @param enableCellDestruction
	 */
	public void setEnableCellDestruction(boolean enableCellDestruction);

	/**
	 * Data structure for the network parameters of a given segment ID - these
	 * are directly accessed by the {@link CellLink} object on each call (so
	 * they should not perform expensive calculations!). We allow a distinction
	 * between up and download (from the client's perspective) via the provided
	 * boolean - upload means: from client to station, download means: from
	 * station to client.
	 * 
	 * @author Bjoern Richerzhagen
	 * @version 1.0, Nov 5, 2015
	 */
	public interface Entry {

		/**
		 * Static segment ID
		 * 
		 * @return
		 */
		public int getSegmentID();

		default public void onHostLeavesSegment(MacAddress hostAddr) {
			// ignore
		}

		default public void onHostEntersSegment(MacAddress hostAddr) {
			// ignore
		}

		/**
		 * Probability of a packet drop on the link - can even be 1, so that the
		 * link is virtually disconnected.
		 * 
		 * @return
		 */
		public double getDropProbability(boolean isUpload);

		/**
		 * Latency on the link (RTT/2)
		 * 
		 * @return
		 */
		public long getLatency(boolean isUpload);

		/**
		 * Bandwidth on the link
		 * 
		 * @return
		 */
		public long getBandwidth(boolean isUpload);

		/**
		 * Availability of the cell (false, if the current cell is "broken")
		 *
		 * @return
		 */
		boolean isAvailable();

		/**
		 * Set the connectivity of the cell. When set to false, the cell is no
		 * longer accepting connections and existing connections fail.
		 * 
		 * @param isAvailable
		 */
		void setAvailability(boolean isAvailable);
	}

}
