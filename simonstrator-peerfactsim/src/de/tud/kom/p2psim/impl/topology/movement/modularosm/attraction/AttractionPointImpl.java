/*
 * Copyright (c) 2005-2010 KOM – Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.topology.movement.modularosm.attraction;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import de.tud.kom.p2psim.impl.topology.PositionVector;
import de.tudarmstadt.maki.simonstrator.api.Randoms;
import de.tudarmstadt.maki.simonstrator.api.component.sensor.location.AttractionPoint;

/**
 * In the current implementation, {@link AttractionPoint}s cannot move anymore.
 * We hold a static list of attraction points to ensure that there exists only
 * one instance of each name at a time.
 * 
 * @author Christoph Muenker, Bjoern Richerzhagen
 * @version 1.0, 02.07.2013
 */
public class AttractionPointImpl extends PositionVector
		implements AttractionPoint {

	protected static Random rnd = Randoms.getRandom(AttractionPoint.class);

	private static Map<String, AttractionPointImpl> instances = new LinkedHashMap<>();

	private String name;
	
	private ArrayList<String> types = new ArrayList<>();
	private ArrayList<String> individuals = new ArrayList<>();
	private ArrayList<String> superDomains = new ArrayList<>();
	private ArrayList<String> domains = new ArrayList<>();
			
	private double weight = 0;
	
	private double radius = 0;

	public AttractionPointImpl(String name, PositionVector posVec) {
		super(posVec);
		this.name = name;
		if (getInstances().containsKey(name)) {
			throw new AssertionError("Name "+name+" already in use for an attraction point.");
		}
		getInstances().put(name, this);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	@Override
	public double getRadius() {
		return radius;
	}

	@Override
	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Override
	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public AttractionPoint clone(String newName) {
		return new AttractionPointImpl(name, this);
	}
	
	@Override
	public int getTransmissionSize() {
		return super.getTransmissionSize() + name.length() + Double.BYTES * 2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = prime * ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		AttractionPointImpl other = (AttractionPointImpl) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public static Map<String, AttractionPointImpl> getInstances() {
		return instances;
	}

	public ArrayList<String> getTypes() {
		return types;
	}

	public void setTypes(ArrayList<String> types) {
		this.types = types;
	}

	public ArrayList<String> getIndividuals() {
		return individuals;
	}

	public void setIndividuals(ArrayList<String> individuals) {
		this.individuals = individuals;
	}

	public ArrayList<String> getSuperDomains() {
		return superDomains;
	}

	public void setSuperDomains(ArrayList<String> superDomains) {
		this.superDomains = superDomains;
	}

	public ArrayList<String> getDomains() {
		return domains;
	}

	public void setDomains(ArrayList<String> domains) {
		this.domains = domains;
	}
}
