/*
 * Copyright (c) 2005-2010 KOM Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.topology.movement.modularosm.attraction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;

public class Domains {
	private static Map<String,String> domains;
	public Domains(){
		setDomains(new HashMap<>());
		this.loadDomains();
	}
	public static Map<String,String> getDomains() {
		return domains;
	}
	public static void setDomains(Map<String,String> domains) {
		Domains.domains = domains;
	}
	
	public void loadDomains(){
		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
		// m.read("file:///home/christian/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl/surfOntology-1.0.0.owl");
		m.read("file:///D:/Repository/Implementation/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		//m.read("file:///G:/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		OntClass domain = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Domain");
		ExtendedIterator<OntClass> ds = domain.listSubClasses();
		List<OntClass> domainsOntology = ds.toList();
		for(int w=0;w<domainsOntology.size();w++){
			OntClass d = domainsOntology.get(w);
			ExtendedIterator<OntResource> ins = (ExtendedIterator<OntResource>) d.listInstances(true);
			List<OntResource> domainInstances = ins.toList();
			for(int h=0;h<domainInstances.size();h++){
				this.getDomains().put(domainInstances.get(h).getURI(),d.getURI());
			}
		}
	}
}
