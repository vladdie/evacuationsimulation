/*
 * Copyright (c) 2005-2010 KOM � Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.topology.movement.modularosm.attraction;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;


public class AvailableServices {
	private static HashMap<Long, String> serviceNames = new HashMap<>();
	private String proBasePath =  System.getProperty("user.dir");
		
	public AvailableServices() {
	}
	
	public void loadServiceNames(){
		try {
			FileInputStream inputStream = new FileInputStream(proBasePath+ "/NegotiatorData/servicesNames.json");
			//FileInputStream inputStream = new FileInputStream("O:/NegotiatorData/servicesNames.json");
			String nameString = IOUtils.toString(inputStream);
			JSONObject namesData = new JSONObject(nameString);
			JSONArray allNames = namesData.getJSONArray("name");
			for(int i=0;i<allNames.length();i++){
				serviceNames.put(Long.parseLong("" +i), allNames.getString(i));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static HashMap<Long, String> getServiceNames() {
		return serviceNames;
	}

	public static void setServiceNames(HashMap<Long, String> serviceNames) {
		AvailableServices.serviceNames = serviceNames;
	}
}
