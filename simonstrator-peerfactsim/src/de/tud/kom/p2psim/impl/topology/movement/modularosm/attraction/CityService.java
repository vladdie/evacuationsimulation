package de.tud.kom.p2psim.impl.topology.movement.modularosm.attraction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ModelFactory;

public class CityService {
	private String uri;
	private ArrayList<String> domains;
	private Map<String,String> superDomains;

	public CityService() {
		uri = "";
		domains = new ArrayList<>();
		setSuperDomains(new HashMap<>());
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public ArrayList<String> getDomains() {
		return domains;
	}

	public void setDomains(ArrayList<String> domains) {
		this.domains = domains;
	}

	public Map<String,String> getSuperDomains() {
		return superDomains;
	}

	public void setSuperDomains(Map<String,String> superDomains) {
		this.superDomains = superDomains;
	}
	
	public void loadSuperDomains(){
		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
		//m.read("file:///G:/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		m.read("file:///D:/Repository/Implementation/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		for(int i=0; i<this.getDomains().size();i++){
			Individual individual = m.getIndividual(this.getDomains().get(i));
			OntClass c = individual.getOntClass(true);
			this.getSuperDomains().put(this.getDomains().get(i), c.getURI());
		}

	}
}
