/*
 * Copyright (c) 2005-2010 KOM Multimedia Communications Lab
 *
 * This file is part of PeerfactSim.KOM.
 * 
 * PeerfactSim.KOM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 * 
 * PeerfactSim.KOM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with PeerfactSim.KOM.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package de.tud.kom.p2psim.impl.topology.movement.modularosm.attraction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.iterator.ExtendedIterator;

public class GatewayDomains {
	private static ArrayList<String> domains;
	private static ArrayList<String> domainIndividuals;
	private static Map<String, String> domainsAndIndividuals;
	
	public GatewayDomains(){
		domains = new ArrayList<>();
		domainIndividuals = new ArrayList<>();
		domainsAndIndividuals = new HashMap<>();
		this.loadDomains();
		this.loadDomainIndividuals();
		this.loadDomainsAndIndividuals();
	}
	
	public void loadDomains(){
		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
		// m.read("file:///home/christian/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl/surfOntology-1.0.0.owl");
		m.read("file:///D:/Repository/Implementation/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		//m.read("file:///G:/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		OntClass domain = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Domain");
		ExtendedIterator<OntClass> ds = domain.listSubClasses();
		List<OntClass> domainsOntology = ds.toList();
		for(int w=0;w<domainsOntology.size();w++){
			OntClass d = domainsOntology.get(w);
			this.getDomains().add(d.getURI());
		}
	}
	
	public void loadDomainIndividuals() {
		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
		// m.read("file:///home/christian/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl/surfOntology-1.0.0.owl");
		m.read("file:///D:/Repository/Implementation/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		//m.read("file:///G:/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		OntClass domain = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Domain");
		ExtendedIterator<OntClass> ds = domain.listSubClasses();
		List<OntClass> domainsOntology = ds.toList();
		for(int w=0;w<domainsOntology.size();w++){
			OntClass d = domainsOntology.get(w);
			ExtendedIterator<OntResource> ins = (ExtendedIterator<OntResource>) d.listInstances();
			List<OntResource> domainInstances = ins.toList();
			for(int h=0;h<domainInstances.size();h++){
				this.getDomainIndividuals().add(domainInstances.get(h).getURI());
			}
		}
	}
	
	public void loadDomainsAndIndividuals(){
		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
		// m.read("file:///home/christian/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl/surfOntology-1.0.0.owl");
		m.read("file:///D:/Repository/Implementation/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		//m.read("file:///G:/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		OntClass domain = m.getOntClass("http://www.surf.scss.tcd.ie/cityOntology/#Domain");
		ExtendedIterator<OntClass> ds = domain.listSubClasses();
		List<OntClass> domainsOntology = ds.toList();
		for(int w=0;w<domainsOntology.size();w++){
			OntClass d = domainsOntology.get(w);
			ExtendedIterator<OntResource> ins = (ExtendedIterator<OntResource>) d.listInstances(true);
			List<OntResource> domainInstances = ins.toList();
			for(int h=0;h<domainInstances.size();h++){
				this.getDomainsAndIndividuals().put(domainInstances.get(h).getURI(),d.getURI());
			}
		}
	}
	
	public static String getDomainClass(String individual){
		String res = "";
		OntModel m = ModelFactory.createOntologyModel("http://www.w3.org/2002/07/owl#");
		// m.read("file:///home/christian/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl/surfOntology-1.0.0.owl");
		m.read("file:///D:/Repository/Implementation/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		//m.read("file:///G:/Repository/code/Data/data-generation/data/ontologies/surfOntology-1-0-0/surfOntology-1.0.0.owl");
		Individual ind = m.getIndividual(individual);
		OntClass domain = ind.getOntClass(true);
		res = domain.getURI();
		return res;
	}
	                          

	public static ArrayList<String> getDomains() {
		return domains;
	}

	public static void setDomains(ArrayList<String> domains) {
		GatewayDomains.domains = domains;
	}

	public static ArrayList<String> getDomainIndividuals() {
		return domainIndividuals;
	}

	public static void setDomainIndividuals(ArrayList<String> domainIndividuals) {
		GatewayDomains.domainIndividuals = domainIndividuals;
	}

	public static Map<String, String> getDomainsAndIndividuals() {
		return domainsAndIndividuals;
	}

	public static void setDomains_and_Individuals(
			Map<String, String> domainsAndIndividuals) {
		GatewayDomains.domainsAndIndividuals = domainsAndIndividuals;
	}
}
